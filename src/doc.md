# Portal venta seguros

#### Requerimientos
  - Alta disponibilidad
  - Soporte de navegadores IE8 y IE8 con retrocompatibilidad IE7
  - Mantenedores -> EasyAdminBundle
  - Historificación
  - Portal
    - Portal se carga por un iframe
    - Log actividad del usuarios
    - HTML responsive
      - Mobile
      - Desktop
    - Session por token
    - Comparador de seguros
    - Landing Page Administrable
      - Un slider de 3 productos administrable
      - Un cuadro dividido en 4
        - 3 cuadros para familia de seguros
        - 1 cuadro de Información
        - Son ordenables
    - Comprar del seguro
    - Mis seguros
    - Envio consolidados

#### Modelo
  - User
    - email
    - password
  - Log
    - user(token|rut)
    - ip
    - action
    - text
    - created_at
  - Category
    Accidente
    Auto
    Fraude
    Hogar
    Salud
    Vacaciones
    Vida
  - SubCategory
  - Currency
  - Client
  - Person
  - Product
    - name
    - segments
    - image_title
    - image_subtitle
    - image
    - title
    - description
    - coverage
    - coverage_not
    - howto
    - pdf_url
    - text_legal
    - button_call
    - button_contract
    - price
  - Security
      - Aceptación de mandato
      - Aceptación de coberturas y exclusiones
      - Medido de pago
      - Person
          - Fecha nacimiento
          - Address
              - Comuna
              - Ciudad
              - Teléfono
              - Email
  - DataForm
  - Content
  - Workflow
  - Activity
  - Statement
  - Authorization
  - Extra  