## Flujos como entidaes

### Auto
### Hogar

#### Flujo Genérico
  - Insurance
    - Beneficiaries
    - Product
    - Plan
    - Prima
    - Person(Pagador, Asegurado)
        - Name
        - Code
        - Birthday
        - Address
            - Community(Region, State, Community)
            - City
            - Phone
            - Cellphone
        - Email
    - Mandate
    - Coverage And Exclusions
    - PaymentMethod
    - Status

#### Flujo A
  - Insurance
    - Statement
        - Activities
        - Health
    - Authorization Health

#### Flujo B
  - Insurance
      - *Contratación a un Tercero*
      - Statement
        - Health
    - Authorization Health

#### Flujo C
  - Insurance
      - *Contratación a un Tercero*
      - Statement
        - Health
    - Authorization Health

#### Flujo D
  - Insurance
    - *Adicionales*
    - *Contratación de un tercero*

#### Flujo E
  - Insurance
    - *Adicionales*
    - *Contratación de un tercero*
  - Fecha inicio viaje
  - *Calculo automático viaje*

#### Flujo F
  - Insurance
    - *Contratación a un tercero*
    - Home

#### Flujo G
  - Insurance
    - Statement
        - Activities
        - Health
    - Authorization Health
  - *Adicionales*

#### Flujo H
  - Insurance
    - Statement
        - Activities
        - Health
    - Authorization Health
