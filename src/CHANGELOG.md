# Change Log

## [0.0.1] - 2016-03-10
### Added
- Botón de "Te Ayudamos" en ficha del producto es administrable.
- Botón "Continuar" y "Te Ayudamos" en ficha de productos sin asistencias. Alineados con texto a derecha.
- Descripción marketera de home debe estar en negrita
- Caja del nombre del seguro en carrusel debe ser más grande
- Cambiar nombre de botón "Solicitar" por "Cotizar"
- Precios debe estar con label "mensuales" y con un asterisco
- Frase legal abajo con UF.
- Precio debe quedar redondeado hacía arriba, para que último número sea 0
- Precio del seguro en la ficha, va a quedar donde antes estaba el nombre
- Agregar tooltip con ayuda para telefono.
- Lightbox para segundo paso de cotizar con que cubre y que no cubre.
- Nueva distribución de nombre y mensaje marketing en banner de ficha del producto.
- Banner home administrable

- Nombres de planes van a quedar como A,B,C…  Cuando tengan complejidad va a ir un texto pequeño que explica o un tooltip. Mauricio va a realizar una propuesta.
- En la información que se pide a los beneficiarios, se tiene que pedir la dirección (oculta por ahora)
- Formato de teléfono en información de contacto. Mismo formato que internet.
- Hay que revisar los comprobantes. En salud hay que enviar DPS en todos los productos. Enviar DPS sólo al email, el comprobante online más corto.
- Datos de Beneficiaros: En responsivo cuadros…  en internet lista.
- Hay que enviar el código de cada plan para que sean identificados.
