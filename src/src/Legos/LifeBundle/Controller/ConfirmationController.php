<?php

namespace Legos\LifeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Legos\LifeBundle\Module\Confirmation;
use Legos\LifeBundle\Module\Utils;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Legos\LifeBundle\Module\PaymentOrder;
use Legos\LifeBundle\Module\Beneficiary;

class ConfirmationController extends Controller
{
    public function indexAction(request $request)
    {
        $session = new Session();
        $main =  $this->get('session')->get('main');
        $data = $request->request;
        if ($request->isMethod('post')){
            $session->set('payment_order',$data);
            $main =  $this->get('session')->get('main');
            Utils::simulatorClean($data,$main,$session);
        }
       
        $insured = Confirmation::Person( $session->get('person'),$this->get('session')->get('payment_order'));
        $_beneficiary = Confirmation::beneficiary( $this->get('session')->get('beneficiary') );

        $allowinvalidez = Utils::ProductsAge($insured['birthday']);
        $numberPerson = Utils::getNumberPerson($insured['code']);
        $engagement = Utils::getEngagement();
        $planner = Utils::isPlanificador();
        $plannerContinue = false;
        if($planner && count($engagement) != 0) {
            $plannerContinue = true;
        }
       
        $coverage = Confirmation::Coverage( $this->get('session')->get('main'),$allowinvalidez );
        $_dps = Confirmation::Dps( $this->get('session')->get('dps' ));

        $plans = $this->get("legos_life.plan");
        $plan = $plans->plans($this->get('session')->get('main'));
        $cobertura =  floatval(str_replace('.','',$main->get('cobertura_total')));
        $obtieneoferta = Confirmation::PlanDetail($plan,$insured["birthday"], $cobertura);
       
        $confirmation = $this->get('session')->get('confirmation');
        $data_confirmation = $this->get("legos_life.solicitudventa");
        $validProduct = PaymentOrder::validProduct();
        $data_prima = PaymentOrder::DataPrima();
        $payment_confirmat =  $session->get('payment_confirmat');
        if($validProduct === true){
            if ($request->isMethod('post')){
                $session->set('payment_order',$data);
                $payment_confirmat = Confirmation::PaymentConfirmation($this->get('session')->get('payment_order'),$main->get('prima_mensual_UF'));
                $confirmation = $data_confirmation->solicitudventa($obtieneoferta,$coverage,$plan,$payment_confirmat,$insured,$session);
                $session->set('confirmation',$confirmation);
                $session->set('payment_confirmat', $payment_confirmat);
                return $this->redirect($this->generateUrl('legos_life_confirmation'));
            }
            $coverage_uf = [];
            if($coverage) {
                if(isset($coverage['vida'])) {
                    $coverage_uf['vida'] = number_format($cobertura, 0, '','.');
                }
                if(isset($coverage['invalidez'])) {
                    $coverage_uf['invalidez'] = true;
                }
                if(isset($coverage['urgencias_medicas'])) {
                    $coverage_uf['urgencias_medicas'] = '150';
                }
                if(isset($coverage['urgencias_dental'])) {
                    $coverage_uf['urgencias_dental'] = '250';
                }
            }
            
           
            $aplication_num = json_decode($confirmation,true);
            
            $em = $this->getDoctrine()->getManager();

           
            $product = $em->getRepository('AppBundle:Product')->findOneBy(['code' => 331]);
            if(method_exists($product, 'getId')) {
                if($product->getId()) {
                    $contratados = $this->get('session')->get('contratados');
                    $contratados[] = "V{$product->getId()}";
                    $contratados = array_unique($contratados);
                    $this->get('session')->set('contratados', $contratados);
                }
            }
            if($confirmation && isset($aplication_num["OUTPUT"])) {
                $primaUtag =  $main->get('prima_mensual_UF');
                $beneficiaries = beneficiary::beneficiaryArray($this->get('session')->get('beneficiary'));
                $udata = Utils::getUdata([
                    'page_path' => '/transa/productos/seguros/vida/vidaatumedida/confirmacion',
                    'page_name' => 'Vida a tu medida: Confirmación',
                    'cargas' => count($beneficiaries),
                    'prima_uf' => $primaUtag,
                    '_PENUMPER' => $numberPerson,
                ]);
                $output = $aplication_num["OUTPUT"];
                if(isset($output["NUMEROSOLICITUD"])) {
					$numero_solicitud = $output["NUMEROSOLICITUD"];
                    if(!empty($numero_solicitud) && $numero_solicitud != 0) {
                        return $this->render('LegosLifeBundle:Default:confirmation.html.twig', [
                            '_PLANNER'              => $plannerContinue,
                            '_INSURED'              => $insured,
                            '_MATRIZ_BENEFICIARIOS' => $_beneficiary,
                            '_DPS'                  => $_dps,
                            '_COVERAGE'             => $coverage_uf,
                            '_PAYMENT_CONFIRMAT'    => $payment_confirmat,
                            '_APPLICATION_NUM'      => $output["NUMEROSOLICITUD"],
                            '_UDATA'                => $udata,
                        ]);
                    }
                }
            }
        } 
        $udata['page_path'] = '/transa/productos/seguros/vida/vidaatumedida/error';
        $udata['page_name'] = 'Vida a tu medida: Error';
        return $this->render('LegosLifeBundle:Default:error.html.twig', [
            '_UDATA'  => $udata,
        ]);
    }
}
