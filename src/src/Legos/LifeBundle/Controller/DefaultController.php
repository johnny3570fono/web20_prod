<?php

namespace Legos\LifeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Legos\LifeBundle\Module\Utils;

class DefaultController extends Controller
{
   

   public function render($view, array $parameters = array(), Response $response = null) {
        
        $insured = $this->get('session')->get('person');
        $age = Utils::MaxAge($insured['birthday']);
        if ($age==false){
                return parent::render('LegosLifeBundle:Default:no_allowed.html.twig');
        }else{
                return parent::render($view, $parameters, $response);
        }

    }
}
