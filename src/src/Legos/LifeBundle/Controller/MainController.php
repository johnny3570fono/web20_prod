<?php

namespace Legos\LifeBundle\Controller;

use Legos\LifeBundle\Controller\DefaultController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Legos\LifeBundle\Module\Confirmation;
use Legos\LifeBundle\Module\Utils;
use Legos\LifeBundle\Module\Main;

class MainController extends DefaultController {
   
    public function indexAction(Request $request) {
      
      $session = new Session();
      $coverage = "3000";
      $insured = Confirmation::Person( $session->get('person'),$this->get('session')->get('payment_order'));
      $allowinvalidez = Utils::ProductsAge($insured['birthday']);
      $productsSimulator = Utils::valueProductInitial();
      $checkeds = Main::checkedsProducts($this->get('session')->get('main'));
      $uf = Utils::getUf(Utils::getRut($insured["code"]));
      $numberPerson = Utils::getNumberPerson($insured['code']);
      
     
      if(!empty($this->get('session')->get('main'))){
          $coverage = $this->get('session')->get('main')->get('cobertura_total');
      }
      if(is_array($checkeds)) {
        $primaUtag = (@$productsSimulator['_LIFE'] * ($coverage / 1000));
        if(isset($checkeds['disability']) && $checkeds['disability']) {
          $primaUtag = (@$productsSimulator['_DISABILITY'] * ($coverage / 1000)) + $primaUtag;
        }
        if(isset($checkeds['medical']) && $checkeds['medical']) {
          $primaUtag = @$productsSimulator['_MEDICAL_URGENCY'] + $primaUtag;
        }
        if(isset($checkeds['dental']) && $checkeds['dental']) {
          $primaUtag = @$productsSimulator['_MEDICAL_DENTAL'] + $primaUtag;
        }
        $primaUtag = $primaUtag;
      }

      $udata = Utils::getUdata([
        'page_path' => '/transa/productos/seguros/vida/vidaatumedida/personalizacion',
        'page_name' => 'Vida a tu medida: Personalización',
        'prima_uf' => $primaUtag,
        '_PENUMPER' => $numberPerson,
      ]);

      return $this->render('LegosLifeBundle:Default:main.html.twig', [
               '_MEDICAL_URGENCY'=> $productsSimulator['_MEDICAL_URGENCY'],
               '_MEDICAL_DENTAL' => $productsSimulator['_MEDICAL_DENTAL'],
               '_CLP_UF'         => $uf,
               '_COVERAGE'       => $coverage,
               '_VIDA'           => $productsSimulator['_LIFE'],
               '_INVALIDEZ'      => $productsSimulator['_DISABILITY'],
               '_ALLOWINVALIDEZ' => $allowinvalidez,
               '_NAME'           => $insured["name"] ." ". $insured["lastname1"],
               '_CHECKEDS'       => $checkeds,
               '_UDATA'          => $udata,
      ]);
    }
}
