<?php

namespace Legos\LifeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Legos\LifeBundle\Module\Utils;
use Legos\LifeBundle\Module\Dps;
use Legos\LifeBundle\Module\Beneficiary;


class DpsController extends Controller
{
    public function indexAction(request $request)
    {
        $session = new Session();
        $data = $request->request;
        $dataSession = array();
        $insured = $session->get('person');
        $uf = Utils::getUf(Utils::getRut($insured["code"]));
        $numberPerson = Utils::getNumberPerson($insured['code']);
        $main =  $this->get('session')->get('main');
        if ($request->isMethod('post')){
              $session->set('beneficiary',$data);
              Utils::simulatorClean($data,$main,$session);
        }

        if (!empty($this->get('session')->get('dps'))){
          $dataSession = Dps::getDps( $this->get('session')->get('dps') );
        }
        $template = 'LegosLifeBundle:Default:dps.html.twig';
        $products = beneficiary::mainProducts($session->get('main',$data));
        
      
        if($cobertura = $main->get('cobertura_total') ){
          $cobertura = floatval(str_replace('.','',$cobertura));
          if($cobertura <= 3000) {
              if($this->get('twig')->getLoader()->exists('LegosLifeBundle:Default:dps_simple.html.twig')) {
                $template = 'LegosLifeBundle:Default:dps_simple.html.twig' ;
              }
          }
        }
        
        $beneficiaries = beneficiary::beneficiaryArray($this->get('session')->get('beneficiary') );
        $primaUtag =  $data->get('prima_mensual_UF');
        $udata = Utils::getUdata([
            'page_path' => '/transa/productos/seguros/vida/vidaatumedida/dps',
            'page_name' => 'Vida a tu medida: DPS',
            'cargas'    => count($beneficiaries),
            'prima_uf' => $primaUtag,
            '_PENUMPER' => $numberPerson,
        ]);
  
        return $this->render($template, [
                '_CLP_UF'      => $uf,
                '_VIDA'        => $this->get('session')->get('main')->get('vida_valor'),
                '_NAME'        => $insured["name"] ." ". $insured["lastname1"],
                '_DATA_PRIMA'  => array(
                      'uf'  => $data->get('prima_mensual_UF'),
                      'clp' => $data->get('prima_mensual_cl'),
                ),
                '_DATA_SESSION'=> $dataSession,
                '_PRODUCTS'    => $products,
                '_UDATA'       => $udata,

        ]);
    }
}
