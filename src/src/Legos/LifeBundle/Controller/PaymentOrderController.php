<?php

namespace Legos\LifeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Legos\LifeBundle\Module\Confirmation;
use Legos\LifeBundle\Module\PaymentOrder;
use Legos\LifeBundle\Module\Utils;
use Legos\LifeBundle\Module\Beneficiary;

class PaymentOrderController extends Controller
{
    public function indexAction(request $request)
    {
        $session = new Session();
        $data = $request->request;

        if ($request->isMethod('post')){
                $session->set('dps',$data);
                $main =  $this->get('session')->get('main');
                Utils::simulatorClean($data,$main,$session);
        }

        $life = $this->get('session')->get('main')->get('vida_valor');
        $insured = Confirmation::Person( $session->get('person'));
        $uf = Utils::getUf(Utils::getRut($insured["code"]));
        $numberPerson = Utils::getNumberPerson($insured['code']);
        $creditCards = PaymentOrder::creditCards( $session->get('person'));
        $data_prima = PaymentOrder::DataPrima();
        $regins = PaymentOrder::WSC_Commune(Utils::getRut($session->get('rut')));
        $products = beneficiary::mainProducts($session->get('main',$data));
        $beneficiaries = beneficiary::beneficiaryArray($this->get('session')->get('beneficiary') );
        $primaUtag =  $data->get('prima_mensual_UF');
        $udata = Utils::getUdata([
            'page_path' => '/transa/productos/seguros/vida/vidaatumedida/pago',
            'page_name' => 'Vida a tu medida: Pago',
            'cargas'    => count($beneficiaries),
            'prima_uf'  => $primaUtag,
            '_PENUMPER' => $numberPerson,
        ]);
  

        return $this->render('LegosLifeBundle:Default:payment_order.html.twig', [
                         '_NAME'         => $insured["name"] ." ". $insured["lastname1"],
                         '_REGIONS_DATA' => "",
                         '_CLP_UF'       => $uf,
                         '_VIDA'         => $life,
                         '_CHECKBOX'     => true,
                         '_PAYMENT_DATA' => $data_prima,
                         '_INSURED'      => $insured,
                         '_CARDS'        => $creditCards,
                         '_DATA_PRIMA'   => $data_prima,
                         '_REGIONS'      => json_decode($regins),
                         '_PRODUCTS'     => $products,
                         '_UDATA'         => $udata,
        				]);
    }
}
