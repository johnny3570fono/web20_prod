<?php

namespace Legos\LifeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\MasterRequest;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Legos\LifeBundle\Module\Utils;
use Legos\LifeBundle\Module\Beneficiary;
use Legos\LifeBundle\Module\Confirmation;
use Legos\LifeBundle\Module\Dps;
use Legos\LifeBundle\Module\Main;


class BeneficiaryController extends Controller
{
    public function indexAction(request $request)
    {

        $session = new Session();
        $_beneficiary = array();
        $products = array();
        $data = $request->request;

        if ($request->isMethod('post')){
              $session->set('main',$data);
        }
        if (!empty($this->get('session')->get('beneficiary'))){
            $_beneficiary = beneficiary::beneficiaryArray( $this->get('session')->get('beneficiary') );
        }

        $life = $this->get('session')->get('main')->get('vida_valor');
        $products = beneficiary::mainProducts($session->get('main',$data));
        $insured = Confirmation::Person( $session->get('person'),$this->get('session')->get('payment_order'));
      
        $uf = Utils::getUf(Utils::getRut($insured["code"]));
        $plans = $this->get("legos_life.plan");
        $plan = $plans->plans($this->get('session')->get('main'));
        $numberPerson = Utils::getNumberPerson($insured['code']);
        $primaUtag =  $data->get('prima_mensual_UF');
        
        $udata = Utils::getUdata([
            'page_path' => '/transa/productos/seguros/vida/vidaatumedida/beneficiarios',
            'page_name' => 'Vida a tu medida: Beneficiarios',
            'prima_uf' => $primaUtag,
            '_PENUMPER' => $numberPerson,
        ]);


        return $this->render('LegosLifeBundle:Default:beneficiary.html.twig', [
                '_CLP_UF'        => $uf,
                '_VIDA'          => $life,
                '_NAME'          => $insured["name"] ." ". $insured["lastname1"],
                '_DATA_PRIMA'    => [
                  'uf' => $data->get('prima_mensual_UF'),
                  'clp' => $data->get('prima_mensual_cl'),
                ],
                '_BENEFICIARIES' => $_beneficiary,
                '_PRODUCTS'      => $products,
                '_UDATA'         => $udata
        ]);
    }
}
