<?php
namespace Legos\LifeBundle\Module;

  class PaymentOrder{

    function creditCards($insured,$start = 0, $end = 4){

            $cards = array_unique($insured['creditCards'], SORT_REGULAR);
            $creditCards = array();
            $array_card = array();

            foreach ($cards as $key => $value) {
                if( $value->id!=00){
                  $str = $value->cardNumber;

                  $len = strlen($str);
                  $cardnumber = str_repeat('*',4)." ".substr($str, -4);
                }else{
                  $cardnumber = $value->cardNumber;
                }
                $array_card = array(
                  "name" => $value->name,
                  "cardNumber" => $cardnumber,
                  "numCardNumber" => $value->cardNumber,
                  "id" => $value->id,
                  "idprod" => $value->idprod
                );
                array_push($creditCards,$array_card);
            }
          
             return $creditCards;
    }

    function DataPrima(){

            $data_prima = array(
                "uf"  => $this->get('session')->get('main')->get('prima_mensual_UF'),
                "clp" => $this->get('session')->get('main')->get('prima_mensual_cl')
            );

            return $data_prima;
    }

    function WSC_Commune($rut){

        $array_json  = array(
              "CANAL" => "ABC",
              "RUTCLIENTE" => "$rut"
        );

        $data_commune = $this->get("legos_life.commune");
        $commune = $data_commune->commune(json_encode($array_json));

        return $commune;


    }

    function validProduct(){

        $productsSimulator = Utils::valueProductInitial();
        $coverageTotal = floatval(str_replace('.','',$this->get('session')->get('main')->get('cobertura_total')));
        $calculateLife = 0;
        $calculateDisability = 0;
        $calculateUrgency = 0;
        $calculateDental = 0;

       if(!empty($this->get('session')->get('main')->get('vida_valor'))){
            $calculateLife = $productsSimulator["_LIFE"]*($coverageTotal / 1000 );
       }
       if(!empty($this->get('session')->get('main')->get('invalidez_valor'))) {
            $calculateDisability = $productsSimulator["_DISABILITY"] * ($coverageTotal / 1000 );
       }
       if(!empty($this->get('session')->get('main')->get('urgencias_medicas_valor'))) {
            $calculateUrgency = $productsSimulator["_MEDICAL_URGENCY"];
       }
       if(!empty($this->get('session')->get('main')->get('urgencias_dental_valor'))) {
            $calculateDental = $productsSimulator["_MEDICAL_DENTAL"];
       }

       $total =  floatval($calculateLife + $calculateDisability + $calculateUrgency + $calculateDental);
       $totalSession = floatval($this->get('session')->get('main')->get('prima_mensual_UF'));

       if(abs(($total-$totalSession)/$totalSession) < 0.00001){
            return true;
       }

       return false;
    }

}
?>
