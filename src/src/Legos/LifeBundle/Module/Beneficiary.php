<?php

namespace Legos\LifeBundle\Module;
use \Datetime;

class Beneficiary{

        function beneficiaryArray($beneficiary){
              $i=1;
              $json_benef = array();
              $benef_all = array();

                    while ($i <= 5) {
                        if(!empty($beneficiary->get('beneficiaryName_'.$i.''))) {
                          $json_benef = array(
                            "names" => $beneficiary->get('beneficiaryName_'.$i.''),
                            "surnames" => $beneficiary->get('beneficiarySurname_'.$i.''),
                            "rut" => $beneficiary->get('beneficiaryRUT_'.$i.''),
                            "cellphone" => $beneficiary->get('beneficiaryTel_'.$i.''),
                            "mail" => $beneficiary->get('beneficiaryEmail_'.$i.''),
                            "percentage" => (int)$beneficiary->get('beneficiaryINP_'.$i.'')

                          );
                          array_push($benef_all,$json_benef);

                        }
                      $i++;
                    }


              return $benef_all;
        }

        function mainProducts($main){

                $products = [];
                $simulator = array("invalidez","urgencias_medicas","urgencias_dental");

                foreach ($simulator as $value) {
                    if(!empty($main->get($value.'_id')) && !empty($main->get($value.'_nombre'))){
                      $product = array();
                      $product["nombre"] = $main->get($value.'_nombre');
                      $product["id"] = $main->get($value.'_id');
                      $product["product"] = $value;
                      if(!empty($main->get($value.'_valor')) && $main->get($value.'_valor') != 0) {
                        $product["valor"] = $main->get($value.'_valor');
                      }
                      array_push($products,$product);
                    }
                }

                return $products;
        }
}
 ?>
