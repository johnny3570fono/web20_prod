<?php

namespace Legos\LifeBundle\Module;
use \Datetime;

class Dps{

        function getDps($dataSession) {
            $diseases = array();

            foreach ($dataSession as $key => $value) {
                if ((preg_match("/enfermedades_/", $key)) || (preg_match("/otra_enfermedad/", $key))|| 
                (preg_match("/ninguna_enfermedad/", $key))) {
                    array_push($diseases,$key);
                }
            }

            $array = array(
             "diseases" => array(
                 "checkbox" => $diseases,
                 "input" => $dataSession->get('descripcion_enfermedades'),
             ),
             "coverage" => $dataSession->get('incluir_cobertura'),
             "imc"=> array(
                 "height" => $dataSession->get('estatura'),
                 "width" => $dataSession->get('peso'),
             ),
             "accordanceDeclaration" => true,
             "laboralActivity" => $dataSession->get('actividad_laboral'),
             "sportDeclaration" => $dataSession->get('practica_actividades'),
             "additionalBackground" => array(
                 "mother" => array(
                     "input" => $dataSession->get('madre_viva'),
                     "response" => array(
                         "fecha_muerte_madre" => $dataSession->get('fecha_muerte_madre'),
                         "causa_muerte_madre" => $dataSession->get('causa_muerte_madre'),
                     ),
                   ),
                 "father" => array(
                     "input" => $dataSession->get('padre_vivo'),
                     "response" => array(
                         "fecha_muerte_padre" => $dataSession->get('fecha_muerte_padre'),
                         "causa_muerte_padre" => $dataSession->get('causa_muerte_padre'),
                     )
                 ),
                 "disease" => array(
                     "input" => $dataSession->get('pregunta_afeccion_lesion_enfermedad'),
                     "response" => array(
                         "descripcion_afeccion_enfermedad_lesion" => $dataSession->get('descripcion_afeccion_enfermedad_lesion'),
                     )
                 ),
                 "lastFiveYears" => array(
                     "input" => $dataSession->get('pregunta_tratamiento_medico'),
                     "response" => array(
                         "descripcion_tratamiento_medico" => $dataSession->get('descripcion_tratamiento_medico'),
                     )
                 ),
                 "surgery" => array(
                     "input" => $dataSession->get('pregunta_intervencion_quirurgica'),
                     "response" => array(
                         "descripcion_intervencion_quirurgica" => $dataSession->get('descripcion_intervencion_quirurgica'),
                     )
                 ),
                 "highPressure" => array(
                     "input" => $dataSession->get('pregunta_presion_alta'),
                 ),
                 "drug" => array(
                     "input" => $dataSession->get('pregunta_drogas'),
                 ),
                 "smoker" => array(
                     "input" => $dataSession->get('pregunta_fumador'),
                     "response" => array(
                         "descripcion_fumador" => $dataSession->get('descripcion_fumador'),
                     )
                 )
             )
          );

          return $array;

        }
}
 ?>
