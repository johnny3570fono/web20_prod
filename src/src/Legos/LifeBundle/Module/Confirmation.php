<?php
namespace Legos\LifeBundle\Module;
use Symfony\Component\HttpFoundation\Session\Session;

  class Confirmation{

    function beneficiary($beneficiary){
          $i=1;
          $json_benef = array();
          $benef_all = array();
          $matriz_benf = array();

          if(!empty($beneficiary->get('beneficiaryName_1'))) {

                while ($i <= 5) {
                      $lastname = Utils::Lastname( $beneficiary->get('beneficiarySurname_'.$i.''));
                      $json_benef = array(
                                       "TIPOBENEFICIARIO" => "3",
                                       "SECUENCIAL" => "$i",
                                       "NOMBRES" => $beneficiary->get('beneficiaryName_'.$i.''),
                                       "APEPATERNO" => $lastname["father_lname"],
                                       "APEMATERNO"=> $lastname["mother_lname"],
                                       "CODPARENTESCO" => "2",
                                       "PORCENTAJE" => $beneficiary->get('beneficiaryINP_'.$i.''),
                                       "EMAIL" => $beneficiary->get('beneficiaryEmail_'.$i.''),
                                       "TELEF" => $beneficiary->get('beneficiaryTel_'.$i.'')

                      );
                  $i++;
                  array_push($benef_all,$json_benef);
                }
                foreach ($benef_all as $key => $value) {
                    if ($value['NOMBRES']==""){
                      unset($benef_all[$key]);
                    }
                }


                $matriz_benf=$benef_all;
          }

          return $matriz_benf;
    }

    function Person($insured, $payment_order = NULL){


      if(!empty($payment_order)){
            $comune = $payment_order->get('nameCommune');
            $city = $payment_order->get('nameCity');
            $region = $payment_order->get('inputRegion');
      }else{
            $comune = "";
            $city = "";
            $region = "";
      }
      $birthday = $insured['birthday'];
      $data_insured = array(
                       "name" => $insured['name'],
                       "lastname1" => $insured['lastname1'],
                       "lastname2" => $insured['lastname2'],
                       "birthday" =>  $birthday->format('d/m/Y'),
                       "address"=> $insured['address'],
                       "addressCellphone" => $insured['addressCellphone'],
                       "addressPhone" => $insured['addressPhone'],
                       "addressEmail" => $insured['addressEmail'],
                       "gene" => $insured['gene'],
                       "code" => $insured['code'],
                       "segment" => $insured['segment'],
                       "commune" => $comune,
                       "city" => $city,
                       "region" => $region

              );

        return $data_insured;
    }

    function Dps($dps){
      $detenfermadad="";
      $riesgo ="";
      $antecentes = "No";
      if(($dps->get('madre_viva')=="no") || ($dps->get('padre_vivo')=="no")){
                  $antecentes = "Si";
      }
      foreach ($dps as $key => $value) {
          if ((preg_match("/enfermedades_/", $key)) || (preg_match("/otra_enfermedad/", $key))) {
            $detenfermadad = "Si";
          }elseif (preg_match("/ninguna_enfermedad/", $key)){
            $detenfermadad = "No";
          }
          if (preg_match("/pregunta_/", $key)) {
              if($value == "S"){
                  $antecentes = "Si";
              }
          }
      }


      $riesgo = 'No';
      if (preg_match("/^No/", $dps->get('practica_actividades'))) {
          $riesgo = "No";
      }elseif (preg_match("/^Practico/", $dps->get('practica_actividades'))) {
          $riesgo = "Si";
      }


      $json_dps=array(
            "enfermedad" => $detenfermadad,
            "riesgo" => $riesgo,
            "antecentes" => $antecentes

      );

      return $json_dps;
    }

    function Coverage($main,$allowinvalidez){
          $invalidez = "";
          $urgencias_medicas = "";
          $dental = "";

          foreach ($main as $key => $value) {
              if(preg_match("/_valor/",$key)){
                $vida =  $main->get('vida_valor');
                $invalidez = $main->get('invalidez_valor');
                $urgencias_medicas = $main->get('urgencias_medicas_valor');
                $dental = $main->get('urgencias_dental_valor');
              }
          }

          if($allowinvalidez){
              $converage = array(
                    "vida" => $vida,
                    "urgencias_medicas" => $urgencias_medicas,
                    "urgencias_dental" => $dental
              );
          }else{
              $converage = array(
                    "vida" => $vida,
                    "invalidez" => $invalidez,
                    "urgencias_medicas" => $urgencias_medicas,
                    "urgencias_dental" => $dental
              );
          }


          return $converage;
    }

    function PaymentConfirmation($payment_order,$prime){

            $payment_confirmat = array(
                  "inputCreditCard" => $payment_order->get('inputCreditCard'),
                  "inputPaymentDay" => $payment_order->get('inputPaymentDay'),
                  "prime_monthly" => $prime
            );

            return $payment_confirmat;

    }

    function PlanDetail($plan, $age, $cobertura = 1000){
      $service_oferta = $this->get("legos_life.obtieneoferta");
      $oferta = json_decode($service_oferta->obtieneoferta($age),true);
      $array_assistance = [];
      $converage_plan['MATRIZ_COBERTURAS'] = [];
      $matrix_converage = self::getMatrixCoverage($oferta, $plan);
      foreach ($matrix_converage as $key => $value) { 
            $primaCob = ((floatval($cobertura) * floatval($value["MONTOBRUTO"]))/1000);
            $converage = array(
                        "PRIMACOB" => floatval(round($primaCob, 4)),
                        "CODCOBERTURA" => $value["CODIGOCOBERTURA"],
                        "CAPITAL" => $cobertura

            );
            if (array_key_exists('MATRIZ_ASISTENCIAS', $value)) {
                  $converage['MATRIZ_ASISTENCIAS'] = [];
                  $capital = 0;
                  $prima = 0;
                  foreach ($value['MATRIZ_ASISTENCIAS'] as  $key => $assistance) {
                        $capital = $assistance["CAPITAL"] + $capital;
                        $prima_assitance = ($assistance["PRIMABRUTAUF"] * $assistance["CAPITAL"]) ;
                        $converage['MATRIZ_ASISTENCIAS'][] = array(
                              "PRIMAASISTENCIA" =>  floatval(round($prima_assitance, 4)),
                              "CAPITALASISTENCIA" => $assistance["CAPITAL"],
                              "CODCOBASISTENCIA" => $assistance["CODCOBASISTENCIA"]
                        );
                        $prima = $prima_assitance + $prima;
                  }
                  $converage['CAPITAL'] = $capital;
                  $converage['PRIMACOB'] = floatval(round($prima, 4));
                  
            }
           $converage_plan['MATRIZ_COBERTURAS'][] = $converage;
      }
      return $converage_plan;
    }

    function getMatrixCoverage($oferta, $plan) {
      if(isset($oferta["OUTPUT"]) && isset($oferta["OUTPUT"]["MATRIZ_OFERTAS"])) {
            $matrix_oferta = current($oferta["OUTPUT"]["MATRIZ_OFERTAS"]);
            $matrix_planes = $matrix_oferta["MATRIZ_PLANES"];
            $keyplan = array_search($plan, array_column($matrix_planes, 'CODIGOPLAN'));
            if(isset($matrix_planes[$keyplan]["MATRIZ_COBERTURAS"])) {
                  return $matrix_planes[$keyplan]["MATRIZ_COBERTURAS"];
            }
      }
      return false;
    }


}


 ?>
