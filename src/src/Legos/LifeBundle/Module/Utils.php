<?php

namespace Legos\LifeBundle\Module;
use \Datetime;

class Utils {

        function Lastname($full_name){

          $tokens = explode(' ', trim($full_name));
          $names = array();
          $special_tokens = array('da', 'de', 'del', 'la', 'las', 'los', 'mac', 'mc', 'van', 'von', 'y', 'i', 'san', 'santa');

          $prev = "";
          foreach($tokens as $token) {
              $_token = strtolower($token);
              if(in_array($_token, $special_tokens)) {
                  $prev .= "$token ";
              } else {
                  $names[] = $prev. $token;
                  $prev = "";
              }
          }

          $num_nombres = count($names);
          $nombres = $apellidos = "";
          switch ($num_nombres) {
              case 0:
                  $nombres = '';
                  break;
              case 1:
                  $nombres = $names[0];
                  break;
              case 2:
                  $nombres    = $names[0];
                  $apellidos  = $names[1];
                  break;
              case 3:
                  $apellidos = $names[0] . ' ' . $names[1];
                  $nombres   = $names[2];
              default:
                  $apellidos = $names[0] . ' '. $names[1];
                  unset($names[0]);
                  unset($names[1]);

                  $nombres = implode(' ', $names);
                  break;
          }

          $lastname_father    = mb_convert_case($nombres, MB_CASE_TITLE, 'UTF-8');
          $lastname_mother  = mb_convert_case($apellidos, MB_CASE_TITLE, 'UTF-8');

          $array_lastname = array(
                  "father_lname" => $lastname_father,
                  "mother_lname" => $lastname_mother
          );

          return $array_lastname;
        }



        function getRut($originalRut, $incluyeDigitoVerificador = true) {

          $originalRut = trim($originalRut);

          $input		= str_split($originalRut);
          $cleanedRut	= '';
          foreach ($input as $key => $chr){

            if ((($key + 1) == count($input)) && ($incluyeDigitoVerificador)){
              if (is_numeric($chr) || ($chr == 'k') || ($chr == 'K')){
                  $cleanedRut .= strtoupper($chr);
              }else{
                  return false;
              }

            }elseif (is_numeric($chr)){
                  $cleanedRut .= $chr;
            }
          }

          if (strlen($cleanedRut) < 3){
            return false;
          }

          $rutceros = str_pad($cleanedRut, 11, "0", STR_PAD_LEFT);
          return $rutceros;
        }

        function MaxAge($birthday){
          $presentDate = new DateTime("now");
          $age = Utils::Age(date("d/m/Y", strtotime($birthday->date)));
          $interval = $birthday->diff($presentDate);
          if($age >= 70){
            $available = 0;
          }else{
             $available = 1;
          }

          return $available;
        }

        function ProductsAge($birthday){
            $presentDate = date("d-m-Y");;
            $birthday = (DateTime::createFromFormat("d/m/Y", $birthday));
            $age = Utils::Age($birthday->format('d/m/Y'));
            if($age > 60){
                $allowinvalidez = true;
            } else {
                $allowinvalidez = false;
            }
            return $allowinvalidez;
        }

        function Age($birthdayOffer){
          $getOffer = $this->get("legos_life.obtieneoferta");
          $age = json_decode($getOffer->obtieneoferta($birthdayOffer));
          return $age->OUTPUT->EDAD;
        }


        function getUf($rut,$type = "uf"){
          $arrayUf = array(
            "KEY"    => "123",
            "CANAL"  => "ABC",
            "RUT"    => $rut,
            "DIVISA" => "UF",
            "FECHA"  => date("d/m/Y")
          );
          $dataUf = $this->get("legos_life.servicelegos");
          $uf = json_decode($dataUf->getService($arrayUf,"uf"),true);
          if (array_key_exists("OUTPUT",$uf)) {
                self::setDateUF($uf["OUTPUT"]);
                return $uf["OUTPUT"]["VALORCL"];
          }
          throw new \Exception();
        }


        function setDateUF($data) {
          if(array_key_exists("FECHADIVISA",$data)) {
            $date = DateTime::createFromFormat("d/m/Y",  $data["FECHADIVISA"]);
            $month = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sept', 'Oct', 'Nov', 'Dic'];
            $date_ES = "{$date->format('d')} {$month[$date->format('n')-1]}, {$date->format('Y')}";
            $this->get('twig')->addGlobal('_DATE_UF', $date_ES);
          }
        }
        
        function lastRoute($request){

            $referer = $request->headers->get('referer');
            $lastPath = substr($referer, strpos($referer, $request->getBaseUrl()));
            $lastPath = str_replace($request->getBaseUrl(), '', $lastPath);

            $matcher = $this->get('router')->getMatcher();
            $parameters = $matcher->match($lastPath);
            $route = $parameters['_route'];

            return $route;

        }

        function simulatorClean($data,$main,$session){
            if(empty($data->get('invalidez_valor'))) {
              $main->remove('invalidez_valor');
            }
            if(empty($data->get('urgencias_medicas_valor'))) {
              $main->remove('urgencias_medicas_valor');
            }
            if(empty($data->get('urgencias_dental_valor'))) {
                $main->remove('urgencias_dental_valor');
            }
            if(!empty($data->get('prima_mensual_UF'))) {
                $main->set('prima_mensual_UF',$data->get('prima_mensual_UF'));
            }
            if(!empty($data->get('prima_mensual_cl'))) {
                $main->set('prima_mensual_cl',$data->get('prima_mensual_cl'));
            }
            $session->set('main',$main);
        }

        function sessionClean($main){
                $main->remove('invalidez_valor');
                $main->remove('urgencias_medicas_valor');
                $main->remove('urgencias_dental_valor');
                $main->set('prima_mensual_UF',$data->get('prima_mensual_UF'));
                $main->set('prima_mensual_cl',$data->get('prima_mensual_cl'));
        }

        function valueProductInitial(){
                  $insured = $this->get('session')->get('person');
                  $obtieneoferta = $this->get("legos_life.obtieneoferta");
                  $getrate = $this->get("legos_life.liferate");
                  $birthday = $insured['birthday'];
                  $oferta = json_decode($obtieneoferta->obtieneoferta($birthday->format("d/m/Y")),true);
                  $age = $oferta['OUTPUT']["EDAD"];
                  $rate = $getrate->requestrate($age);

                  $valueProduct = array(
                    '_LIFE'       => $rate['vida'],
                    '_DISABILITY' => $rate['invalidez'],
                    '_MEDICAL_URGENCY'        => 0.36,
                    '_MEDICAL_DENTAL'        => 0.38
                  );

                  return $valueProduct;

        }

      function getNumberPerson($rut) {
        $cruceprod = $this->get("legos_life.cruceproducto");;
        return $cruceprod->getPersonNumber($rut);
      }

      function getUdata($arg) {
        $default = [
          'page_path' => '',  'page_name' => '',
          'es_cliente' => 'si',
          'prima_uf' => '', '_PENUMPER' => 0,
        ];
        return array_merge($default, $arg);
      }

      function getEngagement() {
        $contrataciones =  $this->get('session')->get('contrataciones');
        return $contrataciones;
      }

      function isPlanificador() {
        $origen = $this->get('session')->get('origen');
        if($origen == 1){
          return true;
        }
        return false;
      }

      function getContracted() {
        $contratados =  $this->get('session')->get('contratados');
        return $contratados;
      }
      
      function getEngagementPlan() {
        $posicionarray =  $this->get('session')->get('posicionarray');
        return $posicionarray;
      }
}
 ?>
