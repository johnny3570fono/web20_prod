<?php

namespace Legos\LifeBundle\Services;

use Symfony\Component\HttpFoundation\Session\Session;

class Plans {

    public function plans($main){


       $invalidez = $main->get('invalidez_valor');
       $urgencias_medicas = $main->get('urgencias_medicas_valor');
       $dental = $main->get('urgencias_dental_valor');

       $key ="1";

        if (isset($dental)) {
                $key .="2";
        }
        if(isset($urgencias_medicas)) {
                $key .="3";
        }
        if(isset($invalidez)){
                $key .="4";
        }


        $array = array(
                  1 => 1,
                  12 => 2,
                  13 => 3,
                  14 => 5,
                  123 => 4,
                  124 => 6,
                  134 => 7,
                  1234 => 8
          );

         return $array[$key];

    }
}
