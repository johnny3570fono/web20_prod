<?php

namespace Legos\LifeBundle\Services;

use Symfony\Component\HttpFoundation\Session\Session;
use Legos\LifeBundle\Services\WSC_ServiceZurich;

class WSC_ValorDivisa extends WSC_ServiceZurich{
    
    public function __construct($webservice = '', $endpoint = '') {
      parent::__construct($webservice, $endpoint['WSC_ValorDivisa']);
    }

    public function getUf($rut){
        $arrayUf = [
            "KEY"    => "123",
            "CANAL"  => "ABC",
            "RUT"    => $rut,
            "DIVISA" => "UF",
            "FECHA"  => date("d/m/Y")
        ];
        $json = json_encode($arrayUf);
        return $this->requestService($json, array(CURLOPT_TIMEOUT => 120));
    }
}
