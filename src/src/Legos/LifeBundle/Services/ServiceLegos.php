<?php

namespace Legos\LifeBundle\Services;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\DependencyInjection\ContainerInterface;


class ServiceLegos extends WSC_Zurich {

    private $logger;

    public function __construct(ContainerInterface $container)  {
      parent::__construct($container);
      $this->logger = parent::getLogLegos();
    }
    
    public function getService($json, $type){
        $curl = curl_init();
        $json =  json_encode($json);
        $url = $this->getUrl($type);
        curl_setopt_array($curl, array(
          CURLOPT_PORT => "8080",
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $json,
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "cache-control: no-cache"
          ),
        ));
        $json = preg_replace('/\s+/', ' ', $json);
        $this->logger->info("[Obtiene ServiceLegos] - REQUEST: ${json} \n");
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
          echo "cURL Error #:" . $err;
          $this->logger->info("[Obtiene ServiceLegos] - ERROR: ${err} \n");
          return false;
        }
        $this->logger->info("[Obtiene ServiceLegos] - RESPONSE: ${response} \n");
        return $response;
    }

    public function getUrl($type = 'uf') {
        $callws = array(
          "commune" => "region_comuna_ws",
          "uf"      => "valor_divisa_ws"
        );
    
        if(isset($callws[$type])) {
           return $this->getUrlService($callws[$type]);
        }
      return false;
    }
}

?>
