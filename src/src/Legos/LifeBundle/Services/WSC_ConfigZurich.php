<?php
namespace Legos\LifeBundle\Services;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class WSC_ServiceZurich   {
    public function __construct($webservice = '', $endpoint) {
        $this->serviceURL =  $this->generateURL($webservice);
    }

    public function requestService($request, $header =  array(
        "Content-Type: application/json",
        "cache-control: no-cache"
      )) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->serviceURL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $request,
            CURLOPT_HTTPHEADER => $header,
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err || !$response) 
           return false;
        return $response;
    }

    public function generateURL($webservice = '', $endpoint = '' ) {
        if(isset($webservice['url']) && !$endpoint) {
            return $webservice['url'].':'.((isset($webservice['port']))? $webservice['port']: '80').'/'.$endpoint;
        }
        return false;
    }
}