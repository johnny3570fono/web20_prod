<?php
namespace Legos\LifeBundle\Services;

class WSC_ServiceZurich {

    public function __construct($webservice = '', $endpoint) {
        $this->serviceURL =  $this->generateURL($webservice, $endpoint);
    }

    public function generateURL($webservice = '', $endpoint = '' ) {
        if(isset($webservice['url']) && $endpoint) {
            return $webservice['url'].':'.((isset($webservice['port']))? $webservice['port']: '80').$endpoint;
        }
        return false;
    }

    public function requestService($request, $options = array() ) {
        $default = array(
            CURLOPT_URL => $this->serviceURL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 60,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $request,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "cache-control: no-cache"
            )
        );
        $curl = curl_init();
        if(count($options)) {
            foreach($options as $key => $value) {
                $default[$key] = $value;
            }
        }
        curl_setopt_array($curl, $default);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err || !$response) 
           return false;
        return $response;
    }
}