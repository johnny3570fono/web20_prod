<?php

namespace Legos\LifeBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Session\Session;
use Legos\LifeBundle\Module\Utils;
use Legos\LifeBundle\Module\PaymentOrder;


class WSC_Creasolicitudventa extends WSC_Zurich {

    private $logger;
    private $session;
    public function __construct(ContainerInterface $container)  {
      parent::__construct($container);
      $this->logger = parent::getLogLegos();
    }
    
    public function solicitudventa($matrixconverage,$coverage, $codplan,$payment_confirmat,$insured,$session){
        $this->session = $session;
        $this->matrixconverage = $matrixconverage;
        $this->payment_confirmat = $payment_confirmat;
        $this->insured = $insured;
        $this->main = $session->get('main');
        $this->beneficiary = $session->get('beneficiary');
        $this->dps = $session->get('dps');
        $this->payment_order = $session->get('payment_order');
        $this->insured['gene'] = self::getSex();
        $propuesta = 00000001;
        $propuesta++;
        $solicitud = [
          'KEY' => '2NFYAKHZP78TJHTWXJCFB6GRLEFZNG542CGII0VDCGFCCQL5IAKWLBCOGOMQZ2HN',
          "CODIGOCANAL" => "85",
          "TIPOGENERACION" => "N",
          "NUMEROPROPUESTA" => "${propuesta}",
          "RUTEJECUTIVO" => Utils::getRut($this->insured["code"]),
          "CODSUCURSAL" => "559",
          "CODINSTITUCION" => "4",
          "TIPOSEGURO" => "V",
          "ESCALAR_PRODUCTO" => [
            "CODPRODUCTO" => "331",
            "CODPLAN" => "${codplan}"           
          ],
          "ESCALAR_ASEGURADO_PRINCIPAL" => [
            "RUTCLIENTE" => Utils::getRut($this->insured["code"]),
            "NOMASEGURADO" =>  $this->insured['name'],
            "APEPATASEGURADO" => $this->insured['lastname1'],
            "APEMATASEGURADO" => $this->insured['lastname2'],
            "FECHANACASEGURADO" => $this->insured['birthday'],
            "ESTCIVASEGURADO" => "0",
            "DIRASEGURADO" => $this->payment_order->get('direction'),
            "SEXOASEGURADO" => $this->insured['gene'],
            "CIUDADASEGURADO" =>  $this->payment_order->get('inputCity'),
            "TELEFONOASEGURADO" => $this->payment_order->get('phone'),
            "COMUNAASEGURADO" =>  $this->payment_order->get('inputCommune'),
            "EMAILASEGURADO" =>  $this->payment_order->get('email'),
          ],
          "ESCALAR_CONTRATANTE" => [
            "RUTCONTRATANTE" => Utils::getRut($insured["code"]),
            "NOMCONTRATANTE" => $this->insured['name'],
            "APEPATCONTRATANTE" => $this->insured['lastname1'],
            "APEMATCONTRATANTE" => $this->insured['lastname2'],
            "SEXOCONTRATANTE" => $this->insured['gene'],
            "ESTCIVCONTRATANTE" => "0",
            "DIRCONTRATANTE" => $insured['address'],
            "COMUNACONTRATANTE" => $this->payment_order->get('inputCommune'),
            "CIUDADCONTRATANTE" => $this->payment_order->get('inputCity'),
            "TELCONTRATANTE" =>  $this->payment_order->get('phone'),
            "EMAILCONTRATANTE" => $this->payment_order->get('email'),
          ],
          "MATRIZ_MEDIO_PAGO" => [
            self::getCreditCard()
          ],
          "MATRIZ_BENEFICIARIOS" => self::getBeneficiaries(),
          "MATRIZ_COBERTURAS" => self::getMatrixCoverage(),
          "ESCALAR_SEG_VIDA_DPS" => self::getDps()
        ];
    
        $curl = curl_init();
        $url = $this->getUrlService('crear_solicitud_ws');
        $json = json_encode(self::utf8ize($solicitud));
   
        curl_setopt_array($curl, array(
              CURLOPT_PORT => "8080",
              CURLOPT_URL => $url,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 120,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => $json,
              CURLOPT_HTTPHEADER => array(
              "Content-Type: application/json",
              "cache-control: no-cache"
              ),
        ));

        $this->logger->info("[Crear Solicitud] - REQUEST: ${json} \n");
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        if ($err || !$response) {
          $this->logger->info("[Crear Solicitud] - RESPONSE: ${err} \n");
          return false;
        } 
        $this->logger->info("[Crear Solicitud] - RESPONSE: ${response} \n");
        return $response; 
    }

    public function getBeneficiaries() {
      $beneficiary =  $this->beneficiary;
      $beneficiaryMatrix = [];
      if(!empty($beneficiary->get('beneficiaryName_1'))) {
        $i = 0;
        while ($i <= 5) {
            if( $beneficiary->get('beneficiaryName_'.$i.'') !== null && !empty( $beneficiary->get('beneficiaryName_'.$i.''))) { 
            $lastname = Utils::Lastname( $beneficiary->get('beneficiarySurname_'.$i.''));
            $beneficiaryArray = array(
                "TIPOBENEFICIARIO" => "3",
                "SECUENCIAL" => "$i",
                "NOMBRES" => $beneficiary->get('beneficiaryName_'.$i.''),
                "APEPATERNO" => $lastname["father_lname"],
                "APEMATERNO"=> $lastname["mother_lname"],
                "CODPARENTESCO" => "0",
                "PORCENTAJE" => $beneficiary->get('beneficiaryINP_'.$i.''),
                "EMAIL" => $beneficiary->get('beneficiaryEmail_'.$i.''),
                "TELEF" => $beneficiary->get('beneficiaryTel_'.$i.'')

            );
            array_push($beneficiaryMatrix, $beneficiaryArray);
          }
          $i++;
        }
      }
      return $beneficiaryMatrix;
    }

    public function getDps() {
      $dps =  $this->dps;
      $main = $this->main;
      $cobertura = floatval(str_replace([',','.'], '', $main->get('cobertura_total')));
     
      if($cobertura <= 3000) {
          $dpsMatrix = [ 
            "TIPODPS" => "S",
            "FLAGENFERMEDAD" => self::getFlagEnfermedad(),
            "DETENFERMEDAD" => self::getDetailEnfermedad()
          ];
        } else {
     
        $dpsMatrix = [
          "TIPODPS" =>  "C",
          "FLAGENFERMEDAD" => self::getFlagEnfermedad(),
          "DETENFERMEDAD" => self::getDetailEnfermedad(),
          "TIPOACTIVIDAD" => self::getTypeActivity(),
          "FLAGDEPORTE" => self::getActivity(),
          "PESO" => $dps->get('peso'),
          "ESTATURA" => $dps->get('estatura'),
          "PADECEENFERMEDAD" =>  self::getPadeceEnfermedad(),
          "DESCPADECEENFERMEDAD" => $dps->get('descripcion_afeccion_enfermedad_lesion'),
          "FLAGCONSULTAMED" => $dps->get('pregunta_tratamiento_medico'),
          "DETCONSULTAMED" => $dps->get('descripcion_tratamiento_medico'),
          "FLAGOPERADO" => $dps->get('pregunta_intervencion_quirurgica'),
          "DETOPERADO" => $dps->get('descripcion_intervencion_quirurgica'),
          "FLAGTRATAPRESION" => $dps->get('pregunta_presion_alta'),
          "FLAGDROGAS" => $dps->get('pregunta_drogas'),
          "FLAGFUMA" =>$dps->get('pregunta_fumador'),
        ];
      }
      return $dpsMatrix;
    }

    public function getFlagEnfermedad() {
      $dps = $this->dps;
      if(!empty($dps->get('ninguna_enfermedad'))) {
        return 'N';
      }
      return 'S';
    }

    public function getDetailEnfermedad() {
      $dps = $this->dps;
      $detenfermedad = '';
      if ($dps) {
        foreach ($dps as $key => $value) {
            if (preg_match("/enfermedades_/", $key) || $key === 'otra_enfermedad' ) {
              if($key === 'otra_enfermedad') {
                $disease[] = "Otra enfermedad";
              } else {
                $disease[]=$key;
              }
              $detenfermedad = implode(",",str_replace("enfermedades_", "", $disease));
            } 
        }
        if(!empty($detenfermedad)) {
          $detenfermedad = trim("${detenfermedad} ".utf8_decode($dps->get('descripcion_enfermedades')));
        }
      }
      return $detenfermedad;
    }

    public function getTypeActivity() {
      $dps = $this->dps;
      return substr($dps->get('actividad_laboral'), 0,1);
    }

    public function getActivity() {
      $dps = $this->dps;
      $actividadRiesgo = $dps->get('practica_actividades');
      if(preg_match('/^No/',$actividadRiesgo)) {
        return 'N';
      }
      return 'S';
    }

    public function getPadeceEnfermedad() {
      $dps = $this->dps;
      if($dps->get('pregunta_afeccion_lesion_enfermedad')) {
        return str_replace('si', 'S', $dps->get('pregunta_afeccion_lesion_enfermedad'));
      }
      return 'N';
    }

    public function getCreditCard() {
      $payment_order = $this->payment_order;
      $main = $this->main;
      $numberCreditCard = $payment_order->get('numberCreditCard');
      $paymentDay =  $payment_order->get('numberDayPayment');
      $total = number_format($this->getTotal(),4);
      $codViaPago = 2;
      $card_regexes = array(
        "/^4\d{12}(\d\d\d){0,1}$/" => 4,
        "/^5[12345]\d{14}$/"       => 1,
        "/^3[47]\d{13}$/"          => 9,
        "/^30[012345]\d{11}$/"     => 6,
        "/^3[68]\d{12}$/"          => 6,
      );
      $cobertura_total = floatval(str_replace([',','.'], '', $main->get('cobertura_total')));
      foreach ($card_regexes as $regex => $type) {
        if (preg_match($regex, $numberCreditCard)) {
            $codViaPago = $type;
            break;
        }
      }
      
      $creditCard = [
        "CODVIAPAGO" => $codViaPago,
        "CODBCOCTCTJT" => "37",
        "DIAPAGO" => $paymentDay,
        "PRIMAPAGAR" => $total,
        "PERIODOPAGO" => "M",
      ];
      if ($codViaPago !== 2)  {
        $cardSplit = str_split($numberCreditCard, 4);
        $creditCard["NUMTARJ1"] = $cardSplit[0];
        $creditCard["NUMTARJ2"] = $cardSplit[1];
        $creditCard["NUMTARJ3"] = $cardSplit[2];
        $creditCard["NUMTARJ4"] = $cardSplit[3];
      }  else  {
        $creditCard["CTACTE"] = $numberCreditCard;
      } 
      return $creditCard;
    }

    public function getTotal() {
      $payment_confirmat = $this->payment_confirmat;
      $matrixconverage = $this->matrixconverage;
      $matrixconverage = $matrixconverage['MATRIZ_COBERTURAS'];
      $initTotal = $payment_confirmat['prime_monthly'];
      $total = 0;
      if($matrixconverage) {
        foreach($matrixconverage as $cobertura) {
          if($cobertura['PRIMACOB'] !== 0) {
            $total = $cobertura['PRIMACOB'] + $total;
          }
        }
        if($total !== 0)
          return $total;
      }
      return $initTotal;
    }


    public function getMatrixCoverage() {
      $matrixconverage = $this->matrixconverage;
      if (isset($matrixconverage['MATRIZ_COBERTURAS'])) {
        return $matrixconverage['MATRIZ_COBERTURAS'];
      }
      return [];
    }

    public function getSex() {
      if(isset($this->insured['gene'])) {
        if($this->insured['gene'] === 'H') {
          return 'M';
        }
      }
      return 'F';
    }

    public function utf8ize( $mixed ) {
      if (is_array($mixed)) {
          foreach ($mixed as $key => $value) {
              $mixed[$key] = self::utf8ize($value);
          }
      } elseif (is_string($mixed)) {
          return @mb_convert_encoding($mixed, "UTF-8", "UTF-8");
      }
      return $mixed;
  }
}
