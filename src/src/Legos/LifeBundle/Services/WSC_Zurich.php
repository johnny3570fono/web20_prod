<?php

namespace Legos\LifeBundle\Services;

use Symfony\Bridge\Monolog\Logger;
use Monolog\Handler\FirePHPHandler; 
use Monolog\Handler\RotatingFileHandler;

class WSC_Zurich {
    private $container;
    private $logger;

    public function __construct($container) {
        $path = $container->get('kernel')->getRootDir();
        $this->container = $container;
        $this->logger = new Logger('logger');
        $this->logger->pushHandler(new RotatingFileHandler("${path}/logs/legos.log", 7, Logger::INFO));
        $this->logger->pushHandler(new FirePHPHandler());
    }

    public function getUrlService($callType = '') {
        $services = $this->container->getParameter('webservice');
        if(array_key_exists($callType, $services)) {
          return $services[$callType];
        }
        return false;
    }

    public function getLogLegos() {
        return $this->logger;
    }
}