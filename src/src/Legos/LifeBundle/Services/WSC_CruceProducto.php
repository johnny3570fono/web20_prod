<?php
namespace Legos\LifeBundle\Services;

use AppBundle\AppSoapClient;
use Symfony\Component\DependencyInjection\ContainerInterface;

class WSC_CruceProducto {
    private $container;
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }
    
    public function getPersonNumber($rut) {
        $number = 0;
        $cruceprod = $this->container->get('cruceprod_session');
        $penumper = $cruceprod->getProductEscalar($rut);
        if(isset($penumper->NUMEROPERSONA)) {
            $number = $penumper->NUMEROPERSONA;
        }
        return $number;
    }
}