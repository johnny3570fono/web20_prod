<?php

namespace Legos\LifeBundle\Services;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\DependencyInjection\ContainerInterface;


class WSC_ObtieneOferta extends WSC_Zurich {
    private $logger;

    public function __construct(ContainerInterface $container)  {
      parent::__construct($container);
      $this->logger = parent::getLogLegos();
    }
    
    public function obtieneoferta($birthday){

        $curl = curl_init();
        $json = '{
            "KEY": "00",
            "PRODUCTO": "331",
            "RELLAMADO": "0",
            "FECHANACIMIENTO": "'.$birthday.'",
            "CANAL": "53",
            "PERIODICIDAD": "M"
        }';
        $url = $this->getUrlService('obtiene_oferta_ws');
        curl_setopt_array($curl, array(
          CURLOPT_PORT => "8080",
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $json,
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "cache-control: no-cache"
          ),
        ));

        $json = preg_replace('/\s+/', ' ', $json);
        $this->logger->info("[Obtiene Oferta] - REQUEST: ${json} \n");
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          $this->logger->info("[Obtiene Oferta] - ERROR: ${json} \n");
          return "cURL Error #:" . $err;
        } 
        $this->logger->info("[Obtiene Oferta] - RESPONSE: ${response} \n");
        return $response;
    }


    
}
