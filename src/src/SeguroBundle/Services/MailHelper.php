<?php

namespace SeguroBundle\Services;
use SeguroBundle\Entity\PublicInsurance;

class MailHelper
{
    protected $mailer;
    protected $templating;
    private $from;
 
    public function __construct(\Swift_Mailer $mailer, $templating)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
    }

    public function sendEmail( $from, $to, $subject = '', $body, $cco )
    {
        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($from)
            ->setTo($to)
            ->setBody($body);            
		$message->setBcc($cco);
		$message->setContentType("text/html");
	
		$submited = $this->mailer->send( $message );
    }
   
    public function correoNotificacion( $insurance, $uf, $parametros, $plantilla, $proposal = null )
    {
        $subject = $parametros['subject'];
    	$from = $parametros['from'];
        $cco = $parametros['cco']; 
    	$to = $insurance->getClient()->getAddressEmail();
        $arregloCorreo = [ 'insurance' => $insurance, 'uf' => $uf, 'proposal' => $proposal ];
       
        $body = $this->templating->render('SeguroBundle:Mail:'.$plantilla, $arregloCorreo);
        
    	$this->sendEmail($from, $to, $subject, $body, $cco);
    }
   
    public function sendEmailA($to, $path,$parametros)
    {
		$message = \Swift_Message::newInstance()
		->setFrom($parametros['from'])
		->setTo($to)
		->setSubject($parametros['subject'])
		->setBody('Reporte de Ventas Diarias')
		->attach(\Swift_Attachment::fromPath($path));

		$message->setBcc($parametros['cco']);
		$message->setContentType("text/html");
		//
		$this->mailer->send($message);
    }
	
    public function correoPrueba($parametros,$to)
    {
        $subject = $parametros['subject'];
        $from = $parametros['from'];
        $cco=$parametros['cco'];
        $body = 'Correo de Prueba ';
         
        $this->sendEmail($from, $to,$subject,$body,$cco);
    }
    

    public function correoAsesor($arregloPresentacion,$to,$parametros,$uf,$nombre,$menus,$resumen)
    {
		$subject = $parametros['subject'];
		$from = $parametros['from'];
		$cco=''; 
		$body = $this->templating->render('::Correo/correoAsesor.txt.twig', array(
			'arregloPresentacion'=>$arregloPresentacion,
			'nombre'=>$nombre,
			'uf'=>$uf,
			'menus'=>$menus,
			'resumen'=>$resumen));
      
   
  
		//$this->sendEmail($from, $to,$subject,$body,$cco);
    }
}