<?php
namespace SeguroBundle\Services;

use SeguroBundle\Helper\Utils;

class TwigFormat extends \Twig_Extension
{
	public function getFilters(){
        return array(
            new \Twig_SimpleFilter('format_rut', array($this, 'formRut')),
			new \Twig_SimpleFilter('format_phone', array($this, 'formPhone')),
			new \Twig_SimpleFilter('format_cardnumber', array($this, 'formatCardNumber')),
        );
    }

    public function formRut( $rut ) 
	{
		if( !empty( $rut ) ) {
			$rutorig = str_replace(".", "", $rut);
			$rutorig = str_replace("-", "", $rut);
			
			//separamos rut
			$rut = substr($rutorig, 0, strlen($rutorig) - 1);
			$dv = substr($rutorig, -1);
			
			//formateamos
			$rut = number_format( $rut, 0, ".", ".")."-".$dv;
			
			return $rut;
		}
    }
	
	public function formPhone( $phone ) 
	{
		if( !empty( $phone ) ) {
			$code = substr($phone, 0, 2);
			$number = substr($phone, 2, 12);
			
			return $code."-".$number;
		}
    }
	
	public function getName(){
		 return 'twigformat';
	}
	
	public function formatCardNumber( $cardnumber, $maskingCharacter = 'X' ) 
	{
		if( !empty( $cardnumber ) ) {
			$cardnumber = Utils::formatCreditCard( $cardnumber );
			return $cardnumber;
		}
    }
	
}
?>