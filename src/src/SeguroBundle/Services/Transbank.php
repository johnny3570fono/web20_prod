<?php

namespace SeguroBundle\Services;

class Transbank
{
	private $transbank;
	
	private $rootpath;
	
	private $webpay; 
	
	public function __construct($rootpath){
		require_once( $rootpath.'/../vendor/transbank/webpay.php');
		require_once($rootpath.'/../vendor/transbank/certificates/cert-normal.php');
		//require_once($rootpath.'/../vendor/transbank/certificates/cert-patpass.php');
		$this->certificate = $certificate;
		$this->rootpath = $rootpath;
		$configuration = new \Configuration();

		$configuration->setEnvironment($this->certificate['environment']);
		$configuration->setCommerceCode($this->certificate['commerce_code']);
		$configuration->setPrivateKey($this->certificate['private_key']);
		$configuration->setPublicCert($this->certificate['public_cert']);
		$configuration->setWebpayCert($this->certificate['webpay_cert']);
		$this->webpay = new \Webpay($configuration);
	}
	
	public function generatePay( $url_return = '', $url_final = '', $buyOrder, $amount){	
		$request = array(
            "amount"    => $amount,
            "buyOrder"  => $buyOrder,
            "sessionId" => uniqid(),
            "urlReturn" => $url_return,
            "urlFinal"  => $url_final,
        );
		$this->transbank = $this->getWebpay()->getNormalTransaction()->initTransaction(
			$request['amount'], $request['buyOrder'], $request['sessionId'], 
			$request['urlReturn'], $request['urlFinal']
		);
		
		return $this->transbank;
	}
	
	public function getWebpay(){
		return $this->webpay;
	}
}