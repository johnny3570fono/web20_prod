<?php

namespace SeguroBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

//entities
use SeguroBundle\Entity\PublicClient;
use SeguroBundle\Entity\PublicInsurance;

class TentadoPublicoCommand extends ContainerAwareCommand
{
    protected function configure()
    {
		$this
        ->setName('tentado:publico')
        ->setDescription('Genera reporte tentados publico')
		->addArgument(
			'date_from', InputArgument::OPTIONAL, 'Fecha de creación de los seguros a conciliar (Y-m-d)'
		)->addArgument(
            'time_from', InputArgument::OPTIONAL, 'Hora y minutos de creación de los seguros a conciliar (H:i)'
        )->addArgument(
			'date_to', InputArgument::OPTIONAL, 'Fecha hasta de creación de los seguros a conciliar (Y-m-d)'
        )->addArgument(
			'time_to', InputArgument::OPTIONAL, 'Hora y minutos hasta de creación de los seguros a conciliar (H:i)'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
		//iniciamos doctrine
		$manager = $this->getContainer()->get('doctrine')->getManager();
		
		// argumentos del comando - Fecha inicio y Fecha Final
		$date_from = $input->getArgument('date_from');
        $time_from = $input->getArgument('time_from');
		
        $date_to = $input->getArgument('date_to');
        $time_to = $input->getArgument('time_to');
		
		//filtramos por fecha de inicio
		if ( $date_from ) {
			$date_from = $this->getFecha($date_from, $time_from);
        }
		
		//filtramos por fecha de termino
		if ( $date_to ) {
			$date_to = $this->getFecha($date_to, $time_to);
        }
			
		$insurances = $manager->getRepository(PublicInsurance::class)->findInsurancesBorrador( $date_from, $date_to );
		
	
		if( count( $insurances ) ) {
			$date_file = new \DateTime('now');
			
			// AVISO!!!!: CAMBIAR POR PARAMETERS.YML
			$pathfile = "/opt/www/zurich_portal_backend/src/web/tentadosPublicos";
			$prefixfile = "TentadosPublicosCSSWEB20_";
			
			// ruta y nombre del archivo
			$filename = sprintf('%s/%s%s.txt', $pathfile, $prefixfile, $date_file->format('Ymd_Hi'));
			
			// creamos el archivo
			$i=1;
			$writer = new \AppBundle\Command\Conciliacion\ReportGenerator(fopen($filename, 'w'));
			$line[$i] = "Rut;Dv;Nombre;Apellido;Email;Telefono;Nombre producto;Fecha Visualizacion;Estado" . "\r\n";
			// $writer->writeItem($line);
			
			foreach(  $insurances as $key => $insurance ) {
				if( method_exists( $insurance->getClient(), 'getId' ) ) {
					$i++;
					
				    //$rut = trim($insurance->getClient()->getCode());
					$rut = substr(trim($insurance->getClient()->getCode()), 0, -2);
                    $dv = substr(trim($insurance->getClient()->getCode()), -1);
		 
					$name = trim($insurance->getClient()->getName());
					$lastname = trim($insurance->getClient()->getLastname1());
					$email = trim($insurance->getClient()->getAddressEmail());
					$phone = $this->emptyPhone( trim($insurance->getClient()->getAddressCellphone()) );
					$name_product = trim($insurance->getTitle());
					$created = $insurance->getCreated();
					
                    $line[$i] = ltrim($rut, "0") . ";" .$dv.";" . $name . ";". $lastname .";" . $email . ";" . $phone . ";" . $name_product . ";" . date_format($created, "Y-m-d H:i:s") . ";" . 'Sin culminar' . "\r\n";
				}
			}
			
			$writer->writeItem($line);
			 
			$output->writeln('Generación de archivo en path: "' . $filename . '" ');
		}else{
			$output->writeln('Seguros para el/los dia/s solicitado/s no existen.');
		}
    }
	
	private function getFecha($day, $hour) {
        $tz = new \DateTimeZone('America/Santiago');

        if (strtolower($day) == 'now' || !$day) {
            $fecha = new \DateTime('now', $tz);
        } else {
            if (!$hour || !preg_match('/^[0-2][0-9]:[0-5][0-9]$/', $hour)) {
                $hour = '00:00:00';
            }

            $fecha = new \DateTime($day . ' ' . $hour, $tz);
        }

        return $fecha;
    }
    
    private function emptyPhone($phone) {
        $phone = str_replace(" ","", $phone);
        
        return $phone;
    }
}