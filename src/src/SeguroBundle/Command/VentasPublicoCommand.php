<?php
namespace SeguroBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
// use AppBundle\Command\Conciliacion\ConciliacionWriter;
use SeguroBundle\Entity\PublicInsurance;
use SeguroBundle\Entity\PublicBasePerson;
use SeguroBundle\Entity\PublicCarga;
use SeguroBundle\Entity\PublicInsuranceQuestion;
// use SeguroBundle\Entity\UserActivity;
// use SeguroBundle\Entity\InformeCorreo;
// use SeguroBundle\Entity\Grupos;
// use Goutte\Client;

/**
 * Clase para extraer valores de la UF
 */
class VentasPublicoCommand extends ContainerAwareCommand {

    private $config;
    protected $defaultName;

    protected function configure() {
        parent::configure();
        $defaultName = $this->defaultName;

        $this->setName('ventas:publico')
        ->setDescription('Genera archivos de ventas publicas diarias para Zurich')
		->addArgument(
			'fecha', InputArgument::OPTIONAL, 'Fecha de creación de los seguros a conciliar (Y-m-d)'
        )->addArgument(
            'hora', InputArgument::OPTIONAL, 'Hora y minutos de creación de los seguros a conciliar (H:i)'
        )->addArgument(
            'fecha_hasta', InputArgument::OPTIONAL, 'Fecha hasta de creación de los seguros a conciliar (Y-m-d)'
        )->addArgument(
            'hora_hasta', InputArgument::OPTIONAL, 'Hora y minutos hasta de creación de los seguros a conciliar (H:i)'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $this->config = $this->getContainer()->getParameter('reportes');
        // $tipoarchivo = $this->config['ventascss'];
        // $path = $tipoarchivo['source_file'];
        // $prefix = $tipoarchivo['prefix'];
		
		// AVISO!!!!: CAMBIAR POR PARAMETERS.YML
		$path = "/opt/www/zurich_portal_backend/src/web/ventasPublicos";
        $prefix = "VentasPublicosCSSWEB20_";

        $fecha = $input->getArgument('fecha');
        $hora = $input->getArgument('hora'); //fecha inicial
        $fecha_hasta = $input->getArgument('fecha_hasta');
        $hora_hasta = $input->getArgument('hora_hasta'); //fecha inicial

        if ($fecha) {
            $fecha = $this->getFecha($input->getArgument('fecha'), $input->getArgument('hora'));
        }
        if ($fecha_hasta) {
            $fecha_hasta = $this->getFecha($input->getArgument('fecha_hasta'), $input->getArgument('hora_hasta'));
        }

        $fecha_archivo = new \DateTime('now');
        $filaname = sprintf('%s/%s%s.txt', $path, $prefix, $fecha_archivo->format('Ymd'));
        $filename = sprintf('%s%s.txt', $prefix, $fecha_archivo->format('Ymd'));

        $types = array_diff(PublicInsurance::getTypes(), ['none']);
        /* buscar datos */
        $repository = $this->getContainer()->get('doctrine')->getManager()->getRepository('SeguroBundle:PublicInsurance');
        $insurance = $repository->findInsurancesVentas($fecha, $fecha_hasta);


        //recolecion de datos
        $preguntas = '';
        $esp = ";";
        $fin = "\n\r";
        $i = 2;


        if (count($insurance) < 1) {
            $output->writeln('Seguros para el día solicitado no existen.');
        } else {
            foreach ($insurance as $datos) {
                $insurance_id = $datos->getId();

                $user_rut = $datos->getClient()->getCode();
                $user_name = $datos->getClient()->getName();
				$user_lastname1 = $datos->getClient()->getLastname1();
                $user_lastname2 = $datos->getClient()->getLastname2();
                $user_telefono = $datos->getClient()->getAddressCellphone();
                $user_email = $datos->getClient()->getAddressEmail();
                $prod_name = $datos->getTitle();
                $prod_price = $datos->getPrice();
                $prod_code = $datos->getCode();
                $prod_fecha = $datos->getCreated();
                $prod_plan = $datos->getPlan();

                $plan = $prod_plan['name'];
				
                $em = $this->getContainer()->get('doctrine')->getManager()->getRepository('SeguroBundle:PublicCarga');
                $cargas = $em->cargas($insurance_id);

                $em2 = $this->getContainer()->get('doctrine')->getManager()->getRepository('SeguroBundle:PublicBeneficiary');
                $beneficiarios = $em2->beneficiarios($insurance_id);

                //comienzo con el archivo
                $writer = new \AppBundle\Command\Conciliacion\ReportGenerator(fopen($filaname, 'w'));

                $body = '';
                $line['1'] = "Solicitud;Fecha;Seguro;Rut;Nombre;Apellido Paterno;Apellido Materno;Email;Telefono;Plan;Precio;codigo Zurich;";
                $head = '';
                for ($r = 0; $r < 5; $r++) {

                    $cab = 'Rut carga ' . $r . ';' . 'Nombre carga ' . $r . ';' . 'Apellido p. carga ' . $r . ';' . 'Apellido m. carga ' . $r . ';' .  'Parentesco ' . $r . ';' . 'Capital ' . $r . ';';
                    $head = $head . $cab;
                }

                if (count($cargas) > 0) {
                    $total = 5 - count($cargas);
                    $body = '';
                    foreach ($cargas as $key => $r) {
                        $carga_rut = $r->getCode();
                        $carga_name = $r->getName();
						$carga_lastname1 = $r->getLastname1();
						$carga_lastname2 = $r->getLastname2();
                        $carga_relationship = $r->getRelationship();

                        $body = $body . $carga_rut . ";" . $carga_name . ";" . $carga_lastname1 . ";" . $carga_lastname2 . ";" . $carga_relationship . ";" . ";";
                    }
                    if ($total < 5) {
                        for ($q = 0; $q < $total; $q++) {
                            $body = $body . ";;;;;;";
                        }
                    }
                }
                if (count($beneficiarios) > 0) {
                    $total = 5 - count($beneficiarios);
                    $body = '';

                    foreach ($beneficiarios as $key => $r) {
                        $carga_rut = $r->getCode();
                        $carga_name = $r->getName();
						$carga_lastname1 = $r->getLastname1();
                        $carga_lastname2 = $r->getLastname2();
                        $carga_relationship = $r->getRelationship();
                        $carga_capital = $r->getCapital();

                        $body = $body . $carga_rut . ";" . $carga_name . ";" . $carga_lastname1 . ";" . $carga_lastname2 . ";" . $carga_relationship . ";" . $carga_capital . ";";
                    }

                    if ($total < 5) {
                        for ($q = 0; $q < $total; $q++) {
                            $body = $body . ";;;;;;";
                        }
                    }
                }
                if (count($cargas) < 1 && count($beneficiarios) < 1) {
                    for ($q = 0; $q < 5; $q++) {
                        $body = $body . ";;;;;;";
                    }
                }

                $objProduct = $this->getContainer()->get('doctrine')->getManager()->getRepository('AppBundle:Product')->
                        findOneBy(array('id' => $datos->getProduct()->getId()));

                if ($objProduct !== NULL) {
                    $prod_temp = $datos->getProduct()->getFormTemplate();
                    $prod_viaje = $datos->getTravelDate();
                } else {
                    $prod_temp = '';
                    $prod_viaje = '';
                }

                if ($prod_temp == 'home.html.twig') {
                    $home_address = $datos->getHome()->getAddress();
                    $home_addressState = $datos->getHome()->getAddressState();
                    $home_addressCity = $datos->getHome()->getAddressCity();

                    $head = $head . 'Address;Address State;Address City;';
                    $body = $body . $home_address . ';' . $home_addressState . ';' . $home_addressCity . ';';
                } else {
                    $head = $head . 'Address;Address State;Address City;';
                    $body = $body . ';;;';
                }

                if ($prod_temp == 'vacaciones.html.twig') {
                    $head = $head . 'Dia Viaje;';
                    $body = $body . $prod_viaje . ';';
                } else {
                    $head = $head . 'Dia Viaje;';
                    $body = $body . ';';
                }
                $head = $head . 'Peso;estatura;Enfermedad;Actividad Ejemplo;Actividad Riesgo;Deportes Riesgo;Enviar Certificado Domicilio;';

                //$repo = $this->getContainer()->get('doctrine')->getManager()->getRepository('AppBundle:InsuranceQuestion');
                //$question = $repo->findBy(array('insurance' => $insurance_id));

                $peso = $datos->getPeso();
                $estatura = $datos->getEstatura();
                $enfermedad = $datos->getEnfermedad();
                $a_riesgo = $this->getPreguntas($insurance_id, 'Actividad Riesgo');
                $d_riesgo = $this->getPreguntas($insurance_id, 'Deportes Riesgo');
                $a_ejemplo = $this->getPreguntas($insurance_id, 'Actividad Ejemplo');
                $domicilio = $this->getPreguntas($insurance_id, 'Enviar Certificado Domicilio');


                $preguntas = $peso . ";" . $estatura . ";" . $enfermedad . ";" . $a_ejemplo . ";" . $a_riesgo . ";" . $d_riesgo . ";" . $domicilio;


                $line['1'] = $line['1'] . $head . $fin;

                $line[$i] = $insurance_id . ";" . date_format($prod_fecha, "Y-m-d H:i:s") . ";" . $prod_name . ";" . $user_rut . ";" . $user_name . ";" . $user_lastname1 . ";" . $user_lastname2 . ";" . $user_email . ";" . $user_telefono . ";" . $plan . ";" . $prod_price . ";" . $prod_code . ";" . $body . $preguntas . $fin;

                $writer->writeItem($line);
                $i++;
            }
            $writer->finish();
            $output->writeln('Generación de archivos en path: "' . $filaname . '" ');

            
        }
    }

    private function getFecha($day, $hour) {
        $tz = new \DateTimeZone('America/Santiago');

        if (strtolower($day) == 'now' || !$day) {
            $fecha = new \DateTime('now', $tz);
        }else {
            if (!$hour || !preg_match('/^[0-2][0-9]:[0-5][0-9]$/', $hour)) {
                $hour = '00:00:00';
            }

            $fecha = new \DateTime($day . ' ' . $hour, $tz);
        }

        return $fecha;
    }

    public function getPreguntas($insu, $quest) {
        $repo = $this->getContainer()->get('doctrine')->getManager()->getRepository('SeguroBundle:PublicInsuranceQuestion');
        $question = $repo->findBy(array('insurance' => $insu));
        $result = null;

        if (count($question) > 0) {
            foreach ($question as $r) {
                if ($r->getQuestion() == $quest)
                    $result = $r->getAnswer();
            }
        }

        return $result;
    }

}
