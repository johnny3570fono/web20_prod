<?php

namespace SeguroBundle\Command\Conciliacion;

use Ddeboer\DataImport\Writer\AbstractStreamWriter;

class ConciliacionWriter extends AbstractStreamWriter
{
    /**
     * {@inheritdoc}
     */
    public function writeItem(array $item)
    {
        fwrite($this->getStream(), implode('', $item) . "\r\n");
    }
}
