<?php

namespace SeguroBundle\Command\Conciliacion;

use Doctrine\ORM\EntityRepository;
use SeguroBundle\Entity\PublicInsurance;
use AppBundle\Entity\Educational;

class LifeGenerator extends Generator
{
   /* protected $fillers = [
        'cabecera' => [
            'rowForArchivo1' => 454,
            'rowForArchivo2' => 32,
            'rowForArchivo3' => 35,
            'rowForArchivo4' => 206,
            'rowForArchivo5' => 627,
        ],
        'totales' => [
            'rowForArchivo1' => 437,
            'rowForArchivo2' => 15,
            'rowForArchivo3' => 18,
            'rowForArchivo4' => 189,
            'rowForArchivo5' => 627,
        ]
    ];*/
     public function __construct($stream = null)
    {
        parent::__construct($stream);

        $this->data = array_merge(
            $this->data,
            [
                'codLinea' => ['003', 3],
            ]
        );
    }

    public function rowForArchivo1()
    {   


        $keys = [
            'fecVige',
            'emiteKit',
            'superClave',
            'dpsSucia',
            'filler1',
            'declara',
            'peso',
            'talla',
        ];
      
  //die();
        $keys = array_merge($this->keysForArchivo1(),$keys);
        $first = $this->padItems($this->getDataRow($keys));
        
        /*$keys = array_merge($this->keysForArchivo1(),$keys);
        return [
            $this->padItems($this->getDataRow($keys))
        ]; */

        //correccion georly torres 19/07/2017
          $curso1 = $this->padItems(
                [
                    [null, 4],
                ]);

        if($this->insurance->getProduct()->getFormTemplate() == 'life_education.html.twig' ){
            
            //$repository = $this->getDoctrine()->getRepository('AppBundle:Educational');
            if($this->insurance->getEducational()!=null){
                    $curso = $this->insurance->getEducational()->getCurso();
                    
                    $curso1 = $this->padItems(
                [
                    [$curso, 4],
                ]
            );

            }/*else{
                $curso1 = $this->padItems(
                [
                    [null, 4],
                ]
            );

            }*/
            
            $plan = $this->insurance->getPlan();
            $plan = $plan['name'];
            if($plan == 'Plan Colegio'){
                $plan = 'N';
            }elseif($plan == 'Plan Universidad'){
                $plan = 'U';   
            }else{
                $plan = 'S';
            }
            //$cant_d=strlen($curso);
           
            $datos = $this->padItems(
                [
                    [$plan, 1],
                ]
            );
            
            $lines[] = array_merge($first,$curso1,$datos);
          
         //   die();
        }else{

            $datos = $this->padItems(
                [
                    [null, 1],
                ]
            );
            
            $lines[] = array_merge($first,$curso1,$datos);

        }
        

        /*$keys = array_merge($this->keysForArchivo1(),$keys);
        $first = $this->padItems($this->getDataRow($keys));
       
        $curso = $this->padItems(
            [
                ['curs', 4],
            ]
        );

        $lines[] = array_merge($first,$curso);*/


        return $lines;
    }
    
    public function codParentesco($parentesco, $arr_parentescoCodigo) {
        
        $codparentesco = '30';     
        $matchparentesco = array_filter($arr_parentescoCodigo, function($element ) use($parentesco){
             $normalizeChars = array(
                'Š'=>'S', 'š'=>'s', 'Ð'=>'Dj','Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A',
                'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I',
                'Ï'=>'I', 'Ñ'=>'N', 'Ń'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U',
                'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'S', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a',
                'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i',
                'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ń'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u',
                'ú'=>'u', 'û'=>'u', 'ü'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', 'ƒ'=>'f',
                'ă'=>'a', 'î'=>'i', 'â'=>'a', 'ș'=>'s', 'ț'=>'t', 'Ă'=>'A', 'Î'=>'I', 'Â'=>'A', 'Ș'=>'S', 'Ț'=>'T',
            );
            $parentesco = strtr($parentesco, $normalizeChars);
            if(isset($element["parentesco"])){
                $element['parentesco'] =  strtolower($element["parentesco"]);
                $parentesco = strtolower($parentesco);
                if(strpos($parentesco, $element["parentesco"]) !== false){
                    return $element;
                }
            }
        });
        if( is_array($matchparentesco) ){
            foreach($matchparentesco as $match){
                $codparentesco = $match['codigo'];
            }
        }
        $codparentesco = str_pad($codparentesco, 2, 0, STR_PAD_LEFT); 
       
        return $codparentesco;
    }

    public function rowForArchivo2()
    {
        $lines = [];
        //datos a imprimir
        $keys = $this->keysForArchivo2();
        $first = $this->padItems($this->getDataRow($keys));
        $codeonlynumber=(int) $this->insurance->getClient()->getCodeOnlyNumber();
        $rut=$codeonlynumber.$this->insurance->getClient()->getCodeOnlyDv();
        $cant=strlen($rut);
        $total=11-$cant;
        $espacios='';
        for($i=0;$i< $total;$i++){
            $espacios.='0';
        }
        $rut=$espacios.$rut;
          

        foreach(['PAG', 'CON'] as $k => $v) {
            $person = $this->padItems(
                [
                    [$v, 3],
                    ['00', 2],
                    [$rut, 11],
                    ['00', 2],
                ]
            );

            $lines[] = array_merge($first, $person);
        }

        /*Busqueda del asegurado*/ 

        $codeonlynumberase=(int) $this->insurance->getClient()->getCodeOnlyNumber();
        $rutase=$codeonlynumberase.$this->insurance->getClient()->getCodeOnlyDv();
        /*completar con ceros a la izquierda*/
        $cant=strlen($rutase);
        $total=11-$cant;
        $espacios='';
        for($i=0;$i< $total;$i++){
            $espacios.='0';
        }
        $rutase=$espacios.$rutase;


        $personase = $this->padItems(
        [
            ['ASE', 3],
            ['00', 2],
            [$rutase, 11],
            ['00', 2],
        ]
        );
        $lines[] = array_merge($first, $personase);
      
        /**/


        $cargas=$this->insurance->getCargas();
        $correlativo=2;
        if($cargas){
            $arr_parentescoCodigo = $this->parentescoCodigo; 
            foreach ( $cargas as $carga ) {
                /*Incluir codigo e parentesco */
                $parentesco=$carga->getRelationship();
                $codparentesco = $this->codParentesco($parentesco, $arr_parentescoCodigo); 
                
                
                ///////
                $cant=strlen($correlativo);
                $total=2-$cant;
                $espacios='';
                for($i=0;$i< $total;$i++){
                    $espacios.='0';
                }
                $correlativo2=$espacios.$correlativo;            
                $codeonlynumber=(int) $carga->getCodeOnlyNumber();
                $rutcarga=$codeonlynumber.$carga->getCodeOnlyDv();
                $cant=strlen($rutcarga);
                $total=11-$cant;
                $espacios='';
                for($i=0;$i< $total;$i++){
                    $espacios.='0';
                }
                $rut=$espacios.$rutcarga;
                $person = $this->padItems(
                [
                    ['ASE', 3],
                    [$correlativo2, 2],
                    [$rut, 11],
                    [$codparentesco, 2],
                ]
                );
                $correlativo=$correlativo+1;
                $lines[] = array_merge($first, $person);
            }
        } 
		
		//beneficiarios
		$beneficiarios=$this->insurance->getBeneficiaries();
        $correlativo=2;
        if($beneficiarios){
            $arr_parentescoCodigo = $this->parentescoCodigo;
            foreach ( $beneficiarios as $beneficiario ) {

                /*Incluir codigo e parentesco */
                $parentesco=$beneficiario->getRelationship();
                $codparentesco = $this->codParentesco($parentesco, $arr_parentescoCodigo); 
                
                ///////
                $cant=strlen($correlativo);
                $total=2-$cant;
                $espacios='';
                for($i=0;$i< $total;$i++){
                    $espacios.='0';
                }
                $correlativo2=$espacios.$correlativo;            
                $codeonlynumber=(int) $beneficiario->getCodeOnlyNumber();
                $rutbeneficiario=$codeonlynumber.$beneficiario->getCodeOnlyDv();
                $cant=strlen($rutbeneficiario);
                $total=11-$cant;
                $espacios='';
                for($i=0;$i< $total;$i++){
                    $espacios.='0';
                }
                $rut=$espacios.$rutbeneficiario;
                $person = $this->padItems(
                [
                    ['BEN', 3],
                    [$correlativo2, 2],
                    [$rut, 11],
                    [$codparentesco, 2],
                ]
                );
                $correlativo=$correlativo+1;
                $lines[] = array_merge($first, $person);
            }
        } 
		
        //var_dump($this->insurance->getProduct()->getFormTemplate());
/*

        $beneficiarios=$this->insurance->getBeneficiaries();
        $correlativo=2;
        if($beneficiarios && $this->insurance->getProduct()->getFormTemplate() == 'life_education.html.twig'){
            foreach ( $beneficiarios as $beneficiario ) {

               // Incluir codigo e parentesco 
                $parentesco=$beneficiario->getRelationship();
                if(strtolower($parentesco)=='hijo'){
                    $codparentesco='02';

                }elseif(strtolower($parentesco)=='cónyuge'){
                    $codparentesco='07';

                }elseif(strtolower($parentesco)=='madre'){
                    $codparentesco='22';

                }elseif(strtolower($parentesco)=='padre'){
                    $codparentesco='23';

              /*  }elseif(strtolower($parentesco)=='nieto'){
                    $codparentesco='12';

                }elseif(strtolower($parentesco)=='hermano'){
                    $codparentesco='15';

                }elseif(strtolower($parentesco)=='sobrino'){
                    $codparentesco='11';

                }elseif(strtolower($parentesco)=='conviviente'){
                    $codparentesco='42';
                
                }else{
                    $codparentesco='30';
                }
                
                ///////
                $cant=strlen($correlativo);
                $total=2-$cant;
                $espacios='';
                for($i=0;$i< $total;$i++){
                    $espacios.='0';
                }
                $correlativo2=$espacios.$correlativo;            
                $codeonlynumber=(int) $beneficiario->getCodeOnlyNumber();
                $rutbeneficiario=$codeonlynumber.$beneficiario->getCodeOnlyDv();
                $cant=strlen($rutbeneficiario);
                $total=11-$cant;
                $espacios='';
                for($i=0;$i< $total;$i++){
                    $espacios.='0';
                }
                $rut=$espacios.$rutbeneficiario;
                $person = $this->padItems(
                [
                    ['BEN', 3],
                    [$correlativo2, 2],
                    [$rut, 11],
                    [$codparentesco, 2],
                ]
                );
                $correlativo=$correlativo+1;
                $lines[] = array_merge($first, $person);
            }
        }*/

        return $lines;
    }
     public function rowForArchivo3()
    {

        $keys = [
            'filler3',
            'codCobertura',
            'filler3',
            'codTipobene',
           
        ];

        

        $keys = array_merge($this->keysForArchivo3(),$keys);
    
        return [
            $this->padItems($this->getDataRow($keys))
        ];
    }

}
