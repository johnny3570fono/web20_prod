<?php

namespace SeguroBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use AppBundle\Entity\Product as Productos;
use AppBundle\Entity\Plan as Planes;
use AppBundle\Controller\BaseController;

use SeguroBundle\Entity\PublicInsurance;
use SeguroBundle\Entity\PublicPaiement as Pago;
use SeguroBundle\Entity\PublicProposal;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;

class TransbankController extends BaseController
{

	/**
     * @Route("/pago/fin/{slug}", requirements={"slug" = "[0-9a-zA-Z\/\-]*"}, name="transbank_endpage")
     * @ParamConverter("product", class="AppBundle:Product")
     * @Method({"GET", "POST"})
     *
     * @param Product $product
     * @param Request $request
     * @return Response
     */
    public function endAction(Productos $product, Request $request){
		$session  = $request->getSession();
		$client = (int) $session->get('client');
		$manager = $this->getDoctrine()->getManager();
		$session  = $request->getSession(); 
		$repository = $manager->getRepository(PublicInsurance::class);	
		$template = 'SeguroBundle:Voucher:'.@$product->getPlantilla()->getTemplate();
        // $request->request->get('token_ws');
		if (  $this->get('templating')->exists($template) ) {
			if( !empty($client) && isset($client) ){  
				$insurance = $repository->findOneSend(array('id' => $session->get('client')));
				if ($insurance != null || $insurance = $repository->findOneIssued(array('id' => $session->get('client')))) {
                    // $this->sentNotificationPaid( $insurance );
					$session->remove('client');
					
					$proposal = $manager->getRepository('SeguroBundle:PublicProposal')->findOneBy(array('insurance' => $insurance->getId()));
					return $this->render( $template,
						array('insurance' => $insurance, 'product'=> $product, 'proposal' => $proposal)
					); 
				}
			} 
			die();
			return $this->redirect( 
				$this->generateUrl('transbank_failpage')
			);
		}
	}	
	
    
	/**
	* @Route("/pago/rechazo", name="transbank_failpage")
	* @Method({"GET", "POST"})
	*
	* @param Request $request
	* @return Response
	*/
	public function failAction(Request $request){
		$session  = $request->getSession();  
		$client = (int) $session->get('client');

		$template = 'SeguroBundle:Front:rechazo.html.twig';	
		$manager = $this->getDoctrine()->getManager();
		$repository = $manager->getRepository(PublicInsurance::class);	
		$insurance = $repository->findOneDraft(array( 'id'=>$client ) );
        
        if (  $this->get('templating')->exists($template) ) {
			return $this->render( $template , ['insurance' => $insurance]);
		}
        
        return $this->render( 'TwigBundle:Exception:error.html.twig', ['status_code' => 'error_template'] );
	}
	
	/**
     * @Route("/pago/{slug}", requirements={"slug" = "[0-9a-zA-Z\/\-]*"}, name="transbank_returnpage")
     * @ParamConverter("product", class="AppBundle:Product")
     * @Method({"GET", "POST"})
     *
     * @param Product $product
     * @param Request $request
     * @return Response
     */
    public function returnAction(Productos $product, Request $request){
		$manager = $this->getDoctrine()->getManager();   		
		$session  = $request->getSession(); 
		$client = (int) $session->get('client');
		$token = filter_input(INPUT_POST, 'token_ws');
		$repository = $manager->getRepository(PublicInsurance::class);
		
		if( !empty($token) && !empty($client)){
			
			if($insurance = $repository->findOneDraft(array('id'=> $client ) ) ){
				
				$transbank = $this->get('seguro.transbank');
				$result = $transbank->getWebpay()->getNormalTransaction()->getTransactionResult($token);
				
				if( isset($result->detailOutput->responseCode) ){
					if ($result->detailOutput->responseCode === 0) {
						$pago = new Pago();
						$pago->setInsurance($insurance);
						$pago->setAccountingDate($result->accountingDate);
						if(isset($result->cardDetail->cardNumber)){
							$pago->setCardNumber($result->cardDetail->cardNumber);
						}
						if(isset($result->cardDetail->cardExpirationDate)){
							$pago->setCardExpirationDate($result->cardDetail->cardExpirationDate);
						}
						if(isset($result->detailOutput->authorizationCode)){
							$pago->setAuthorizationCode($result->detailOutput->authorizationCode);
						}
						if(isset($result->detailOutput->paymentTypeCode)){
							$pago->setPaymentTypeCode($result->detailOutput->paymentTypeCode);
						}
						if(isset($result->detailOutput->responseCode)){
							$pago->setResponseCode($result->detailOutput->responseCode);
						}
						if(isset($result->detailOutput->sharesNumber)){
							$pago->setSharesNumber($result->detailOutput->sharesNumber);
						}
						if(isset($result->detailOutput->amount)){
							$pago->setAmount($result->detailOutput->amount);
						}
						if(isset($result->detailOutput->commerceCode)){
							$pago->setCommerceCode($result->detailOutput->commerceCode);
						}						
						if(isset($result->transactionDate)){
							$pago->setTransactionDate($result->transactionDate);
						}
						if(isset($result->VCI)){
							$pago->setVci($result->VCI);
						}

						$insurance->setStatus(PublicInsurance::STATUS_ISSUED);
						$manager->persist($insurance);
						$manager->persist($pago);
						$manager->flush();
						
						$insurance->setPaiement($pago);
						$concentradorAPI = $this->container->get('concentrador_api');
						$response = $concentradorAPI->creaSolicitudVentaPublic($insurance);
						$proposal = null;

						try {
							if ($response != null) {
								$proposal = new PublicProposal();
								$proposal->setInsurance($insurance);
								$proposal->setFecha(\DateTime::createFromFormat("d/m/Y H:i:s", $response["FECHA"]));
								$proposal->setPrima($response["PRIMACALCULADA"]);
								$proposal->setEstado($response["ESTADOSOLICITUD"]);
								$proposal->setPoliza($response["NUMEROPOLIZA"]);
								$proposal->setCotizacion($response["NUMEROCOTIZACION"]);
								$proposal->setSolicitud($response["NUMEROSOLICITUD"]);

								$manager->persist($proposal);
								$insurance->setStatus(PublicInsurance::STATUS_SEND);
							} else {
								$insurance->setStatus(PublicInsurance::STATUS_ISSUED);
							}

							$manager->persist($insurance);
							$manager->flush();
						} catch (Exception $e) {
							$proposal = null;
							$manager = $this->getDoctrine()->getManager();
							$insurance->setStatus(PublicInsurance::STATUS_ISSUED);
							$manager->persist($insurance);
							$manager->flush();
						}
						
						//enviamos email de notificacion de compra
						if ($proposal == null ||  $concentradorAPI->mustSendEmail($insurance->getProduct())) {
							$this->sentNotificationPaid( $insurance, $product->getSlug(), $proposal);
						}
                        // die;
						
						return $this->render('SeguroBundle:Front:webpay.html.twig',  array(
								'token' => $token,
                                'product' => $product,
								'url' => $result->urlRedirection
							)
						);
					
					}
				}
				
			}
		}
		
		return $this->redirect($this->generateUrl('transbank_failpage',
			array('slug' => $product->getSlug() ))
		);
	}
	

	public function sentNotificationPaid( $insurance, $slug, $proposal = null ) 
	{
		switch($slug){
			case "vacaciones-seguras":
				$subject = "Solicitud de Seguro de Vacaciones Seguras";
				break;
			case "viaje-larga-estadia":
				$subject = "Solicitud de Seguro de Viaje Larga Estadía";
				break;
			default:
				$subject = 'Resumen de ventas Web2.0 (Publica)';
				break;
		}
        	$plantilla = $this->getProductTemplate($insurance->getProduct());
		$parametros = [
			'subject' => $subject,
			'from' => 'respaldoseguros@santanderseguros.cl',
			'cco' => 'respaldoseguros@santanderseguros.cl'
		];

		$uf = $this->getRepository('AppBundle:Uf')->findLast();
		$this->get('seguro.mail_helper')->correoNotificacion( $insurance, $uf, $parametros, $plantilla, $proposal );
		
       /*$template = 'SeguroBundle:Mail:'.$insurance->getProduct()->getFormTemplate();
         if (  $this->get('templating')->exists($template) ) {
             return $this->render( $template, [ 'insurance' => $insurance, 'uf' => $uf ] );
         }
        */
	}
}
