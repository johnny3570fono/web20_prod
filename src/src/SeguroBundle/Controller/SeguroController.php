<?php
namespace SeguroBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

use JMS\Serializer\SerializerBuilder;

use AppBundle\Controller\BaseController;

use AppBundle\Entity\Product as Productos;
use AppBundle\Entity\Plan as Planes;
use AppBundle\Entity\Community as Comunas;
use AppBundle\Entity\Segment as Segmento;

use SeguroBundle\Form\Type\ClientType;
use SeguroBundle\Form\Type\HomeType;

use SeguroBundle\Form\Type\InsuranceTravelType;
use SeguroBundle\Form\Type\InsuranceHomeType;
use SeguroBundle\Form\Type\InsuranceOncologicoType;
use SeguroBundle\Form\Type\InsuranceUrgenciaType;
use SeguroBundle\Form\Type\InsuranceCatastroficoType;
use SeguroBundle\Form\Type\InsuranceHospitalizacionType;
use SeguroBundle\Form\Type\InsuranceVacacionesType;

use SeguroBundle\Entity\PublicClient;
use SeguroBundle\Entity\PublicInsurance;
use SeguroBundle\Entity\PublicHome;
use SeguroBundle\Entity\PublicPersonRelationship;

use SeguroBundle\Controller\TransbankController;
use SeguroBundle\Entity\PublicProposal;

class SeguroController extends BaseController
{
	
	public function __construct(){
		
	}
	
	/**
     * @Route("/informacion/{slug}", requirements={"slug" = "[a-zA-Z0-9\-_\/]+"}, name="seguro_index")
     * @ParamConverter("product", class="AppBundle:Product")
     * @Method({"GET", "POST"})
     *
     * @param Product $product
     * @param Request $request
     * @return Response
     */
    public function indexAction(Productos $product, Request $request)
    {
    
		$plantilla = $this->getProductTemplate($product);
		if ( $this->get('templating')->exists('SeguroBundle:Home:'.$plantilla) ) {
			$template = 'SeguroBundle:Home:'.$plantilla;
			$seguro = $product->getId();
			$slug = $product->getSlug();
			$repository = $this->getDoctrine()->getRepository(Productos::class);
			$product = $repository->findOneBy(array('id'=>$seguro, 'isPublic' => true));
			$session = $request->getSession();
            $session->remove('client');
            $session->remove('price');
            $session->remove('insured');
            $session->remove('canal');
			if( $product ) {
				if($request->getMethod() == 'POST') {
                    if( $insured = $request->request->get('insured') ){
                       $_post_csrf_token_ajax = $request->request->get('_csrf_token_ajax');
                       $_csrf_token_ajax = $session->get('_csrf_token_ajax');
					   if(isset( $insured['plan']) && $_csrf_token_ajax == $_post_csrf_token_ajax  ){
                            $session->set('plan', (int) $insured['plan']);
                            $session->set('insured', $insured);
                            $session->set('price', (float) $insured['price']);
                           
                            return $this->redirect($this->generateUrl('seguro_formpage', array('slug' => $slug))); 
					   }
					}
                } else {
                    $canal = $request->query->get('canal') ;
                    if($request->getMethod() == 'GET' && $canal == 'chatbot') {
                        $plan =  $request->query->get('plan');
                        $price =  $request->query->get('price');
                        $viajeros = $request->query->get('viajeros');
                        $coverage = $request->query->get('coverage');
                        $dias = $request->query->get('dias');
                        $repository = $this->getDoctrine()->getRepository(Planes::class);
                        if($planOne = $repository->findOneBy(array('id'=> $plan))) {
                            $insured = [
                                'viajeros' => $viajeros,
                                'dias' => $dias,
                                'plan' => $plan,
                                'coverage' => [
                                    $plan => $coverage
                                ]
                            ];
                            $insured["price"] = $this->getPricebyFilter($planOne, $insured);
                            $session->set('plan', (int) $insured['plan']);
                            $session->set('insured', $insured);
                            $session->set('price', (float) $insured['price']);
                            $session->set('canal', $canal);

                            return $this->redirect($this->generateUrl('seguro_formpage', array('slug' => $slug , 'canal' => $canal)));
                        } 
                    }
                }

                $dias = array();
                if(method_exists($product,"getCoverages")){
                    foreach($product->getCoverages()->toArray() as $coverage){
                        $dia = (int) $coverage->getDays();
                        $dias[$dia] = $dia;
                    }
                    asort($dias); 
                }
                $_csrf_token_ajax = uniqid();
                if( $session->get('_csrf_token_ajax') ) {
                    $_csrf_token_ajax = $session->get('_csrf_token_ajax');
                }
                $session->set('_csrf_token_ajax', $_csrf_token_ajax);
				return $this->render( $template, array('product' => $product,'_csrf_token_ajax'=> $_csrf_token_ajax, 'dias'=>$dias));
			}
            
		}
       
		return $this->render( 'TwigBundle:Exception:error.html.twig', ['status_code' => 'no_public'] );
    }
	
    /**
     * @Route("/formulario/{slug}", requirements={"slug" = "[0-9a-zA-Z\/\-]*"}, name="seguro_formpage")
     * @ParamConverter("product", class="AppBundle:Product")
     * @Method({"GET", "POST"})
     *
     * @param Product $product
     * @param Request $request
     * @return Response
     */
    public function formularioAction(Productos $product, Request $request){
		$repository = $this->getDoctrine()->getRepository(Productos::class);
		$product = $repository->findOneBy(array('id'=> $product->getId(), 'isPublic' => true));
        
		if( $product ) {
			$plantilla = $this->getProductTemplate($product);
			if ( $this->get('templating')->exists('SeguroBundle:Register:'.$plantilla) ) {
				$template = 'SeguroBundle:Register:'.$plantilla;
				$id = $product->getId();
				$slug = $product->getSlug();
				$session  = $request->getSession();
				if($plan = $session->get('plan')){
					$insured = $session->get('insured');
					$repository = $this->getDoctrine()->getRepository(Planes::class);
					if( $plan = $repository->findOneBy(array('id'=> $plan,'product' => $id))){
						//validamos los filtros y el precio
                        $price = $this->getPricebyFilter($plan, $insured);
                        $cargas = $this->getCargasbyFilter($plan, $insured);
                        $rulesage = $this->getRuleAge($plan, $insured);
                        $insurance = new PublicInsurance($product, $plan);
                        $formtype = $this->getTypeForm($plantilla);
                        $form = $this->createForm($formtype, $insurance);
                        $form = $this->transFormInsured($form, $insured);
                        $form->handleRequest($request);
                        
                        $manager = $this->getDoctrine()->getManager();
                        
                        if ($form->isSubmitted() && !$this->errorInsurance($plan, $form, $session)){	
                            foreach ($form->getErrors(true) as $error){
                                $errors[] =  $error->getMessage();
                            }
                            
                            if(isset($errors)){
                                $this->get('session')->getFlashBag()->set('error', $errors);
                            }
                            if($form->isValid() && !isset($error) ){
                                if( !$form->get('client')->getData()->getInsuranceU()){
                                    $form->get('client')->getData()->setInsuranceU(0);
                                }
                                //pasamos el plan original
                                $insurance->setPlan($plan);
                                $manager->persist($insurance);
                                $manager->flush();
                                
                                $session = $request->getSession();
                                $session->set('client', $insurance->getId());
                                $canal = $session->get('canal') ;
                                $parameters = array('slug' => $slug);
                                if($canal == 'chatbot') {
                                    $parameters['canal'] = $canal;
                                }

                                return $this->redirect($this->generateUrl('seguro_checkoutpage', $parameters));
                            }
                            
                            
                        }
                        
                
                        $relationships = [];
                        if ($form->offsetExists('beneficiaries'))  {
                            $relationships = [
                                ['value' => PublicPersonRelationship::REALTIONSHIP_CHILD, 'maxapparition' => -1],
                                ['value' => PublicPersonRelationship::REALTIONSHIP_SPOUSE, 'maxapparition' => 1],
                                ['value' => PublicPersonRelationship::REALTIONSHIP_FATHER, 'maxapparition' => 1],
                                ['value' => PublicPersonRelationship::REALTIONSHIP_MOTHER, 'maxapparition' => 1],
                                ['value' => PublicPersonRelationship::REALTIONSHIP_OTHER, 'maxapparition' => -1]
                            ];
                        }

                        if ($form->offsetExists('cargas')) {

                            if ($product->getCalculoMayor() == 1) {
                                $relationships = [
                                    ['value' => PublicPersonRelationship::REALTIONSHIP_SPOUSE, 'maxapparition' => 1],
                                    ['value' => PublicPersonRelationship::REALTIONSHIP_CONVIVIENTE, 'maxapparition' => 1],
                                    ['value' => PublicPersonRelationship::REALTIONSHIP_PAREJA, 'maxapparition' => 1],
                                    ['value' => PublicPersonRelationship::REALTIONSHIP_CHILD, 'maxapparition' => 1],
                                    ['value' => PublicPersonRelationship::REALTIONSHIP_NIETO, 'maxapparition' => -1],
                                ];
                            } else {
                                $relationships = [
                                    ['value' => PublicPersonRelationship::REALTIONSHIP_CHILD, 'maxapparition' => -1],
                                    ['value' => PublicPersonRelationship::REALTIONSHIP_SPOUSE, 'maxapparition' => 1]
                                ];

                                if (trim($product->getCode()) != '258' && trim($product->getCode()) != '276') {
                                    $relationships[] = ['value' => PublicPersonRelationship::REALTIONSHIP_OTHER, 'maxapparition' => -1];
                                }
                            }
                        }
                      
                        $parentescos = $manager->getRepository('AppBundle:Parentesco')
                                        ->findBy(array("producto" => $product->getId()));
                        if ($parentescos) {
                            $relationships = array();
                            foreach ($parentescos as $value) {
                                $relationships[] = ['value' => $value->getParentesco(), 'maxapparition' =>  $value->getMaxApparition(), 'edadMinima' => $value->getEdadMinima(), 'edadMaxima' => $value->getEdadMaxima()];
                            }
                        }
                        
                        return $this->render($template,  array(
                                'plan' => $plan,
                                'relationships' => $relationships,
                                'insurance' => $insurance,
                                'rulesage' => $rulesage,
                                'form' => $form->createView(),
                                'price' => $price,
                                'cargas' => $cargas,
                                'product' => $product
                            )
                        );	
                           
                        
					}
				}
			}
			return $this->redirect($this->generateUrl('seguro_index', array('slug' => $slug))); 	
		}
		
		return $this->render( 'TwigBundle:Exception:error.html.twig', ['status_code' => 'no_public'] );
		
    }
    
    
    /**
     * @Route("/orden/{slug}", requirements={"slug" = "[0-9a-zA-Z\/\-]*"}, name="seguro_checkoutpage")
     * @ParamConverter("product", class="AppBundle:Product")
     * @Method({"GET", "POST"})
     *
     * @param Product $product
     * @param Request $request
     * @return Response
     */
    public function ordenAction(Productos $product, Request $request)
	{
		$repository = $this->getDoctrine()->getRepository(Productos::class);
		$product = $repository->findOneBy(array('id'=> $product->getId(), 'isPublic' => true));
		
		if( $product ) {
			$manager = $this->getDoctrine()->getManager();
            $plantilla = $this->getProductTemplate($product);
            $slug = $product->getSlug();
			if ( $this->get('templating')->exists('SeguroBundle:Order:'.$plantilla) ) {
				$template = 'SeguroBundle:Order:'.$plantilla;	
				$id = $product->getId();
				$session  = $request->getSession();
                $canal = $session->get('canal') ;
                $parameters = array('slug' => $slug);
                if($canal == 'chatbot') {
                    $parameters['canal'] = $canal;
                }
				if($plan = $session->get('plan') && $client = $session->get('client')){
					$plan = $manager->getRepository(Planes::class)->findOneBy(array('id'=> $plan,'product' => $id));  
					$repository = $manager->getRepository('SeguroBundle:PublicInsurance');
					if($insurance = $repository->findOneDraft(array('id'=>$session->get('client'))) ){			
						
						//if submited order
						if ( $request->isMethod('POST') && $insurance->getProduct()->getPatPass() ) {

                            $concentradorAPI = $this->container->get('concentrador_api');
                            $response = $concentradorAPI->creaSolicitudVentaPublic($insurance);
                    
                            try {
                                if ($response != null) {
                                    $proposal = new PublicProposal();
                                    $proposal->setInsurance($insurance);
                                    $proposal->setFecha(\DateTime::createFromFormat("d/m/Y H:i:s", $response["FECHA"]));
                                    $proposal->setPrima($response["PRIMACALCULADA"]);
                                    $proposal->setEstado($response["ESTADOSOLICITUD"]);
                                    $proposal->setPoliza($response["NUMEROPOLIZA"]);
                                    $proposal->setCotizacion($response["NUMEROCOTIZACION"]);
                                    $proposal->setSolicitud($response["NUMEROSOLICITUD"]);
                    
                                    $manager->persist($proposal);
                                    $insurance->setStatus(PublicInsurance::STATUS_SEND);
                                } else {
                                    $insurance->setStatus(PublicInsurance::STATUS_ISSUED);
                                }
                    
                                $manager->persist($insurance);
                                $manager->flush();
                            } catch (Exception $e) {
                                $manager = $this->getDoctrine()->getManager();
                                $insurance->setStatus(PublicInsurance::STATUS_ISSUED);
                                $manager->persist($insurance);
                                $manager->flush();
                            }
							
                           
                            //redireccionamos a comprobante
							return $this->redirect( $this->generateUrl('transbank_endpage', $parameters, true ));
						}
						
						$transbank = [];
						if( $amount = $this->ufToClp( $insurance->getPrice()) ){				
							$transbank = $this->get('seguro.transbank');
							$url_return = $this->generateUrl('transbank_returnpage', $parameters);
							$url_final = $this->generateUrl('transbank_endpage', $parameters);
                            
                            $schemaHost =  $this->getHttpHostProxy($request);
        
                            $url_return =  $schemaHost.'/'.ltrim($url_return,'/');
                            $url_final  =  $schemaHost.'/'.ltrim($url_final,'/');
							$transbank  = $transbank->generatePay($url_return, $url_final, $insurance->getId(),$amount );
							
							$transbankuri = $transbank->url;
							if( $insurance->getProduct()->getPatPass() ) {
                               
								$transbankuri = $this->generateUrl('seguro_checkoutpage', $parameters);	
							}
						}
						//print_r($product->getCoverage());
						return $this->render($template, array('product' => $product , 'insurance'=> $insurance, 'transbank' => $transbank, 'transbankuri' => $transbankuri)); 
					}
                } 
                die('---');
            }
            die('---');
			return $this->redirect($this->generateUrl('seguro_index', $parameters)); 
		}
            
        die('---');
		return $this->render( 'TwigBundle:Exception:error.html.twig', ['status_code' => 'no_public'] );
		
    }

   

	 /**
     * @Route("/planes/{slug}", requirements={"slug" = "[0-9a-zA-Z\/\-]*"}, name="seguro_plans_index")
     * @ParamConverter("product", class="AppBundle:Product")
     * @Method({"GET", "POST"})
     *
     * @param Product $product
     * @param Request $request
     * @return Response
     */
      public function plansAction(Productos $product, Request $request){
        $id = $product->getId();
        $manager =  $this->getDoctrine()->getManager();
       
        $plans = array(); 
        $repository = $manager->getRepository(Productos::class);
        $productos = $repository->findOneBy(array('id'=>$id));
        if(method_exists($productos, 'getPlans')){
            foreach($productos->getPlans() as $plan){
				$plans[$plan->getId()] = $plan->toArray();
				if(method_exists($plan, 'getTargets')){
					$plans[$plan->getId()]['target'] = $plan->getTargets()->toArray();
				}
				$plans[$plan->getId()]['single'] = true;
                $frecuencia = "mensual";
                if($plan->getProduct()){
                    if($plan->getProduct()->getCodFrecPago()){
                        if((int) $plan->getProduct()->getCodFrecPago() == 5){
                            $frecuencia= 'único';
                        } if((int) $plan->getProduct()->getCodFrecPago() == 4){ 
                            $frecuencia = 'anual';
                        }
                    }
                }
                $plans[$plan->getId()]['frecuencia'] = $frecuencia;
            }
            //Recontruir coverage
            if(method_exists($productos, 'getCoverages')){
                $count = count($productos->getCoverages());
                
                foreach($productos->getCoverages() as $coverages){
                    $coverage = $coverages->toArray();
                    if(isset($coverage['plan']['id'])){
                        if(isset($plans[$coverage['plan']['id']])){
                           $plans[$coverage['plan']['id']]['coverages'][] = $coverages->toArray();
							if(isset($coverage['price'])){
								if(!empty($coverage['price'])){
									$plans[$coverage['plan']['id']]['single'] = false;
								}
							}
                        }
                    }
                   
                }
            }
			//cambio debido a cambio de diseño
			if(count($plans) != 0) {
				$cloneplan = [];
				foreach($plans as $indice => $plan){
					$continuar = true;
					$itemplan = $plan;
					if(isset($plan['coverages'])){
						foreach($plan['coverages'] as $coverage){
							if($continuar == true){
								if(!empty(@$coverage['price'])){
									$itemplan['coverages'] = [];
									array_push($itemplan['coverages'], $coverage);
									array_push($cloneplan, $itemplan);
								} else {
									array_push($cloneplan, $itemplan);
									$continuar = false;
								}
							
							}
						}
					} else{
						array_push($cloneplan, $itemplan);
					}
				}
				$plans = $cloneplan;
			}			
        }      
      
        return new JsonResponse($plans);
      }       

      public function errorInsurance( Planes $plan, $form, $session = array()){
		$error  = true;
		// $template = $plan->getProduct()->getFormTemplate();
		$template = $this->getProductTemplate($plan->getProduct());
        $alternativas =  array( 
            'travel.html.twig' => 1,
            'hospitalizacion.html.twig' => 1 ,
            'home.html.twig' => 1 ,
            'urgencias_medicas.html.twig' => 1,
            'catastrofico.html.twig' => 2 ,
            'life.html.twig' => 2, 
            'oncologico.html.twig' =>2 ,
			'vacaciones.html.twig' => 3,
			'vacaciones_largas_fam.html.twig' => 3,
			'life_education.html.twig' => 4
        );
		
		
		if(isset($alternativas[$template])){
			$error = false;
	
			$planId = $plan->getId();
			$insured = $session->get('insured');
            $pricesession = (float) $session->get('price');
			$planes = $plan->getProduct()->getPlans(); 
			$cargas = $form->getData()->getCargas();
            $cargas = (count($cargas) == 0 ) ? $form->getData()->getBeneficiaries(): $cargas;
			$price = (float) $plan->getPrice();
            
            if($alternativas[$template] == 1){
                foreach($planes as $eplan){
                    if($eplan->getId() == $planId){
                        $price = $eplan->getPrice();
                        if ($template != 'home.html.twig') {
                            if ($template == 'hospitalizacion.html.twig') {
                                $price = $price * ( count($cargas) + 1 );
                            }
                            if ($template == 'hospitalizacion.html.twig' || $template == 'urgencias_medicas.html.twig') {
                                if (count($cargas) < $eplan->getCargasMin() || count($cargas) > $eplan->getCargasMax()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                }
                $form->getData()->setPrice($price);
            } else if($alternativas[$template] == 2){
                if ($template != 'life.html.twig' ) {
					$edad = $form->get('client')->getData()->getBirthday();
					$dateTime = new \DateTime();
					$diff = date_diff($edad, $dateTime);
					$edad = $diff->y;
                    foreach($planes as $eplan){
                        if( $eplan->getId() == $planId ){
                            foreach ($eplan->getTargets() as $key => $q) {
                                if ($edad >= $q->getStart() && $edad <= $q->getFinish() && $planId == $q->getPlanId()) {
                                    $data = $form->getData()->getQuestions();
                                    $price = $q->getPrice();
                                    if ($data[0]->getAnswer() < 0 || $data[1]->getAnswer() < 0 )
                                        $error = true;
                                }
                            }
                        }
                    }
                }
                
                $form->getData()->setPrice($price);
            } else if($alternativas[$template] == 3){
                $cover = $plan->getProduct()->getCoverages();
                $coverage = @$insured['coverage'][$plan->getId()];
                
                //$error = true;
                foreach($planes as $eplan){
                    if( $eplan->getId() == $planId ){
                        $cover = $eplan->getProduct()->getCoverages();
                       
                        foreach ($cover as $key => $r) {
                            if ($r->getPlanId() == $planId) {
                                 
                                if ($r->getId() == $coverage) {
                                    $error = false;
                                    $precio = $r->getPrice();
                                    $codplanco = $r->getCodplanco();
                                  
                                    $precio = explode(',', $precio);
                                    $codplanco = explode(',',$codplanco);
                                    if( is_array($codplanco) ){
                                        if(isset($codplanco[count($cargas)]) ){
                                            $codplanco = trim($codplanco[count($cargas)]); 
                                        } else {
                                            $codplanco = trim(end($codplanco));
                                        }
                                        if(isset($codplanco)){
                                            $form->getData()->setPri($codplanco);
                                        }
                                    }
                                    
                                    if (count($cargas) < $eplan->getCargasMin() || 
                                        count($cargas) > $eplan->getCargasMax()) {
                                       $error = true;
                                    }
                                   
                                    if($precio){
                                        if (isset($precio[count($cargas)])) {
                                            $price = (float) $precio[count($cargas)];
                                        } else {
                                            $price = (float) end($precio);
                                        }	
                                    }
                                }
                            }
                        }
                    }
                }
                $form->getData()->setPrice($price);
            }
           
            if( (float) $pricesession != (float) $price ){
               $error = true;
            }
		}
        
      
		return $error;
      }
	  
	public function coverageInProduct($product, $coverage_id){
		if(method_exists($product, 'getCoverages')){
			if( $coverages = $product->getCoverages()){
				foreach($coverages as $coverage){
					if($coverage->getId() == $coverage_id){
						return $coverage->toArray();
					}
				}
			}
		}
		return false;
	}
	
	public function getTypeForm($template = 'home.html.twig'){
		$templates = array(
			'vacaciones_largas_fam.html.twig' => new InsuranceTravelType($this->getManager()),
			'home.html.twig' => new InsuranceHomeType($this->getManager()),
			'travel.html.twig' => new InsuranceTravelType($this->getManager()),
			'oncologico.html.twig' => new InsuranceOncologicoType($this->getManager()),
			'urgencias_medicas.html.twig' => new InsuranceUrgenciaType($this->getManager()),
			'catastrofico.html.twig' => new InsuranceCatastroficoType($this->getManager()),
			'hospitalizacion.html.twig' => new InsuranceHospitalizacionType($this->getManager()),
			'vacaciones.html.twig' => new InsuranceVacacionesType($this->getManager())
		);
		if(isset($templates[$template])){
			return $templates[$template];
		}
		return new InsuranceHomeType($this->getManager());
	}
	
	public function ufToClp($price){
		if( $uf = $this->getUf()->getValue()){
			$priceUp = (int)round($uf * $price);
			while(($priceUp%10) != 0) {
				$priceUp++;
			}
			return (int) $priceUp;
		}
		return false;
    }
	
	
	public function getRuleAge(Planes $plan, $restricted = []){
		if($targets = @$plan->getTargets()->toArray()){
			if(isset($restricted['birthday']) ){
				$birthday = $restricted['birthday'];	
				if( $edad = $this->getAgebyBirthday($birthday) ){
					foreach($targets as $target){
						if ($edad >= $target->getStart() && $edad <= $target->getFinish() 
								&& $plan->getId() == $target->getPlanId()) {
							$rules = [
								'start' => $target->getStart(),
								'end' => $target->getFinish()
							];
							return $rules;
						}
					}
				}
			}
			return false;
		}
		return true;
	}
	/*
	 * @Parametro de birthday = [day][month][year]
	 * @return age (int)
	 */
	public function getAgebyBirthday($birthday = []){
		$edad = false;
		if( isset($birthday['day']) &&  isset($birthday['month'])  
			&& isset($birthday['year']) ) {
			$day =  sprintf("%02d", $birthday['day']);
			$month =  sprintf("%02d", $birthday['month']);
			$year =  sprintf("%04d", $birthday['year']);
			$birthday =new \DateTime($year.'-'.$month.'-'.$day);
			$dateTime = new \DateTime();
			$diff = date_diff($birthday, $dateTime);
			$edad = $diff->y;
		}
		return $edad;
	}
    /*
	 * @Parametro para setear parametros desde simulación
	 */
    public function transFormInsured($form, $insured){
        if(isset($insured['birthday']) ){
            if(isset($insured['birthday']['year'])){
                if(!empty($insured['birthday']['year'])){
                    $year =  $insured['birthday']['year'];
                    $form->get('client')->get('birthday')->get('year')->setData($year); 
                }
            }
            if(isset($insured['birthday']['month'])){
                if(!empty($insured['birthday']['month'])){ 
                   $month = $insured['birthday']['month'];
                   $form->get('client')->get('birthday')->get('month')->setData($month);
                }
            }
            if(isset($insured['birthday']['day'])){
                if(!empty($insured['birthday']['day'])){   
                    $day = $insured['birthday']['day'];
                    $form->get('client')->get('birthday')->get('day')->setData($day);
                    
                }
            }
        }
		return $form;
	}
    
    public function getPricebyFilter(Planes $plan, $filter){
        $price = $plan->getPrice();
        if( isset($filter['coverage'][$plan->getId()])){
			$coverage = $filter['coverage'][$plan->getId()];
			if( $coverage = $this->coverageInProduct($plan->getProduct(), $coverage )){
                if(!empty($coverage['price']) && !empty($coverage['codplanco']) ){
                    $price = $coverage['price'];
                    $multiprice = explode(',',$price);
                    if( is_array($multiprice) && isset($filter['viajeros'])){
                        $viajeros = explode(':',$filter['viajeros']);
                        if(count($viajeros)!= 0){
                            foreach($viajeros as $index){
                                $price = isset($multiprice[$index])? $multiprice[$index]: end($multiprice);
                            }
                           return $price;
                        }
                    }
                    return $price;    
                }
			} 
		} else if( isset($filter['asegurados'] )){
			$asegurados = (int) $filter['asegurados'];
			$asegurados = max($asegurados, 0);
            if(!empty($asegurados) ){
                $price = (int) ($asegurados + 1) * $plan->getPrice();
                return $price;
            }
		} else if( isset($filter['birthday']) ){
			$birthday = $filter['birthday'];
			if( $edad = $this->getAgebyBirthday($birthday) ){
				$targets = $plan->getTargets()->toArray();
				if(is_array($targets)){
					foreach($targets as $target){
						if ($edad >= $target->getStart() && $edad <= $target->getFinish() 
								&& $plan->getId() == $target->getPlanId()) {
							$price  = $target->getPrice();
                            return $price;
						}
					}
				}
			}  
		}
        return $price;
    }
    
    public function getCargasbyFilter(Planes $plan , $filter){
        $cargas = ['min' => $plan->getCargasMin(), 'max' => $plan->getCargasMax()];
        if(isset($filter['viajeros'])){
            $viajeros = explode(':', $filter['viajeros']);
            if(isset($viajeros[0])){
                $cargas = [
                    'min' => isset($viajeros[0])? (int) $viajeros[0] : (int) $plan->getCargasMin(),
                    'max' => isset($viajeros[1])? (int) $viajeros[1] : (int) $viajeros[0]
                ];
            }
        } else if (isset($filter['asegurados']) ){
            $asegurados = (int) $filter['asegurados'];
            $asegurados = max($asegurados, 0);
            $cargas =  $asegurados;
        }
        return $cargas;
    }
    
    public function getHttpHostProxy(Request $request){
        //variable para validar condiciones del proxypass
        if($request->get('debug')=='HTTP_X_FORWARDED_HOST'){
            echo '<!-- ';var_dump($_SERVER); echo '-->';
        }
        
        if( isset($_SERVER['HTTP_X_FORWARDED_SERVER'] ) || isset($_SERVER['HTTP_X_FORWARDED_HOST'] ) ){
            if(!empty($_SERVER['HTTP_X_FORWARDED_SERVER']) ){
                $schemaHost = 'https://'.$_SERVER['HTTP_X_FORWARDED_SERVER'];
            } else if( !empty($_SERVER['HTTP_X_FORWARDED_HOST']) ){
                $schemaHost = 'https://'.$_SERVER['HTTP_X_FORWARDED_HOST'];
           } 
        }
        if( ! isset($schemaHost) ){
            if(strpos( $request->getSchemeAndHttpHost(),'180.153.10.71' )  || 
                strpos( $request->getSchemeAndHttpHost(), 'homzsws10.santanderseguros.cl.bsch' ) ){
                $schemaHost = $request->getSchemeAndHttpHost();
            } else if(strpos('prodzsws10.santanderseguros.cl.bsch', $request->getSchemeAndHttpHost())){
                $schemaHost = 'https://www.santander.cl';
            } else {
                $schemaHost = $request->getSchemeAndHttpHost();
            }
        }
        return $schemaHost;
    }
} 
