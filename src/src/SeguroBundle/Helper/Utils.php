<?php
namespace SeguroBundle\Helper;

class Utils{
    function clearCardNumber( $cardNumber ){
        $cardNumber = str_replace("-", "", $cardNumber);
        return $cardNumber;
    }
    
    function formatCreditCard($cc) {
        $cc = str_replace(array('-', ' '), '', $cc);
        $cc_length = strlen($cc);
        $newCreditCard = substr($cc, -4);
    
        for ($i = $cc_length - 5; $i >= 0; $i--) {
    
            if ((($i + 1) - $cc_length) % 4 == 0)
                $newCreditCard = '-' . $newCreditCard;
    
            $newCreditCard = $cc[$i] . $newCreditCard;
        }
    
        return $newCreditCard;
    }
}