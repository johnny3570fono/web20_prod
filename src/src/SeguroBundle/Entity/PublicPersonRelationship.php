<?php

namespace SeguroBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints as AppAssert;

/**
 * @ORM\MappedSuperclass()
 */
class PublicPersonRelationship extends PublicBasePerson
{
    const REALTIONSHIP_CHILD = 'Hijo/a';
    const REALTIONSHIP_SPOUSE = 'Cónyuge';
    const REALTIONSHIP_FATHER = 'Padre';
    const REALTIONSHIP_MOTHER = 'Madre';
    const REALTIONSHIP_OTHER = 'Otro';
    const REALTIONSHIP_SOBRINO = 'Sobrino';
    const REALTIONSHIP_HERMANO = 'Hermano';
    const REALTIONSHIP_NIETO = 'Nieto/a';
    const REALTIONSHIP_CONVIVIENTE = 'Conviviente';
    const REALTIONSHIP_PAREJA = 'Pareja';

    /**
     * @var string
     *
     * @ORM\Column(name="relationship", type="string", length=32)
     */
    private $relationship;

    /**
     * Set relationship
     *
     * @param string $relationship
     * @return self
     */
    public function setRelationship($relationship)
    {
        $this->relationship = $relationship;

        return $this;
    }

    /**
     * Get relationship
     *
     * @return string
     */
    public function getRelationship()
    {
        return $this->relationship;
    }

    public function toArray()
    {
        $data = parent::toArray();

        return array_merge(
            $data,
                [
                    'relationship' => $this->getRelationship(),
                ]
        );
    }
}
