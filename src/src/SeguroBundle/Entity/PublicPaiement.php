<?php

namespace SeguroBundle\Entity;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * PublicPay
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="SeguroBundle\Entity\PublicPaiementRepository")
 */
class PublicPaiement
{
	
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	 /**
     * @var \stdClass
     *
     * @ORM\OneToOne(targetEntity="PublicInsurance", inversedBy="paiement", cascade={"persist", "remove"})
	 * @ORM\JoinColumn(name="insurance_id", referencedColumnName="id")
     */
    private $insurance;
	
	/**
     * @var string
     *
     * @ORM\Column(name="accounting_date", type="string", length=6)
     */
    private $accountingDate;
	
	/**
     * @var string
     *
     * @ORM\Column(name="card_number", type="string", nullable=true, length=6)
     */
    private $cardNumber;
	
	/**
     * @var string
     *
     * @ORM\Column(name="card_expiration", type="string", nullable=true, length=6)
     */
    private $cardExpirationDate;
	
	/**
     * @var string
     *
     * @ORM\Column(name="authorization_code", type="string", length=6)
     */
    private $authorizationCode;
	
	
	/**
     * @var string
     *
     * @ORM\Column(name="payment_typecode", type="string", length=4)
     */
    private $paymentTypeCode;
	
	
	/**
     * @var string
     *
     * @ORM\Column(name="vci", type="string", length=10)
     */
    private $vci;
	
	/**
     * @var string
     *
     * @ORM\Column(name="response_code", type="integer", length=11)
     */
    private $responseCode;
	
	/**
     * @var string
     *
     * @ORM\Column(name="shares_number", type="integer", length=11)
     */
    private $sharesNumber;
	
	
	/**
     * @var string
     *
     * @ORM\Column(name="amount", type="integer", length=11)
     */
	private $amount;
	
	
	/**
     * @var string
     *
     * @ORM\Column(name="commerce_code", type="bigint", length=11)
     */
	private $commerceCode;
	
	
	/**
     * @var string
     *
     * @ORM\Column(name="transaccion_date", type="string", length=40)
     */
	private $transactionDate;
	
	
	
	 /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(name="updated", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updated;
	
	//transactionDate
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set accountingDate
     *
     * @param string $accountingDate
     * @return PublicPay
     */
    public function setAccountingDate($accountingDate)
    {
        $this->accountingDate = $accountingDate;

        return $this;
    }

    /**
     * Get accountingDate
     *
     * @return string 
     */
    public function getAccountingDate()
    {
        return $this->accountingDate;
    }

    /**
     * Set cardNumber
     *
     * @param string $cardNumber
     * @return PublicPay
     */
    public function setCardNumber($cardNumber)
    {
        $this->cardNumber = $cardNumber;

        return $this;
    }

    /**
     * Get cardNumber
     *
     * @return string 
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * Set cardExpirationDate
     *
     * @param string $cardExpirationDate
     * @return PublicPay
     */
    public function setCardExpirationDate($cardExpirationDate)
    {
        $this->cardExpirationDate = $cardExpirationDate;

        return $this;
    }

    /**
     * Get cardExpirationDate
     *
     * @return string 
     */
    public function getCardExpirationDate()
    {
        return $this->cardExpirationDate;
    }

    /**
     * Set authorizationCode
     *
     * @param string $authorizationCode
     * @return PublicPay
     */
    public function setAuthorizationCode($authorizationCode)
    {
        $this->authorizationCode = $authorizationCode;

        return $this;
    }

    /**
     * Get authorizationCode
     *
     * @return string 
     */
    public function getAuthorizationCode()
    {
        return $this->authorizationCode;
    }

    /**
     * Set paymentTypeCode
     *
     * @param string $paymentTypeCode
     * @return PublicPay
     */
    public function setPaymentTypeCode($paymentTypeCode)
    {
        $this->paymentTypeCode = $paymentTypeCode;

        return $this;
    }

    /**
     * Get paymentTypeCode
     *
     * @return string 
     */
    public function getPaymentTypeCode()
    {
        return $this->paymentTypeCode;
    }

    /**
     * Set vci
     *
     * @param string $vci
     * @return PublicPay
     */
    public function setVci($vci)
    {
        $this->vci = $vci;

        return $this;
    }

    /**
     * Get vci
     *
     * @return string 
     */
    public function getVci()
    {
        return $this->vci;
    }

    /**
     * Set responseCode
     *
     * @param integer $responseCode
     * @return PublicPay
     */
    public function setResponseCode($responseCode)
    {
        $this->responseCode = $responseCode;

        return $this;
    }

    /**
     * Get responseCode
     *
     * @return integer 
     */
    public function getResponseCode()
    {
        return $this->responseCode;
    }

    /**
     * Set sharesNumber
     *
     * @param integer $sharesNumber
     * @return PublicPay
     */
    public function setSharesNumber($sharesNumber)
    {
        $this->sharesNumber = $sharesNumber;

        return $this;
    }

    /**
     * Get sharesNumber
     *
     * @return integer 
     */
    public function getSharesNumber()
    {
        return $this->sharesNumber;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     * @return PublicPay
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set commerceCode
     *
     * @param integer $commerceCode
     * @return PublicPay
     */
    public function setCommerceCode($commerceCode)
    {
        $this->commerceCode = $commerceCode;

        return $this;
    }

    /**
     * Get commerceCode
     *
     * @return integer 
     */
    public function getCommerceCode()
    {
        return $this->commerceCode;
    }

    /**
     * Set transactionDate
     *
     * @param string $transactionDate
     * @return PublicPay
     */
    public function setTransactionDate($transactionDate)
    {
        $this->transactionDate = $transactionDate;

        return $this;
    }

    /**
     * Get transactionDate
     *
     * @return string 
     */
    public function getTransactionDate()
    {
        return $this->transactionDate;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return PublicPay
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return PublicPay
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set insurance
     *
     * @param \SeguroBundle\Entity\PublicInsurance $insurance
     * @return PublicPay
     */
    public function setInsurance(\SeguroBundle\Entity\PublicInsurance $insurance = null)
    {
        $this->insurance = $insurance;

        return $this;
    }

    /**
     * Get insurance
     *
     * @return \SeguroBundle\Entity\PublicInsurance 
     */
    public function getInsurance()
    {
        return $this->insurance;
    }
}
