<?php

namespace SeguroBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

use AppBundle\Validator\Constraints as AppAssert;
use SeguroBundle\Entity\PublicBasePerson;

/**
 * PublicClient
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="SeguroBundle\Entity\PublicClientRepository")
 */
class PublicClient extends PublicBasePerson
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

      /**
     * @var \stdClass
     *
     * @ORM\OneToOne(targetEntity="PublicInsurance", inversedBy="client", cascade={"persist", "remove"})
	 * @ORM\JoinColumn(name="insurance_id", referencedColumnName="id")
     */
    private $insurance;

	
	/**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotNull()
     */
    // protected $name;
	
	/**
     * @var string
     *
     * @ORM\Column(name="lastname1", type="string", length=255)
     * @Assert\NotNull()
     */
    // protected $lastname1;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname2", type="string", length=255)
     * @Assert\NotNull()
     */
    // protected $lastname2;
	
	/**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=36)
     * @AppAssert\IsRun()
     * @Assert\NotNull()
     */
    // protected $code;
	
	 /**
     * @var \Date
     *
     * @ORM\Column(name="birthday", type="date", nullable=true)
     * @Assert\NotNull()
     */
    // protected $birthday;
	
	 /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     * @Assert\NotNull()
     */
    protected $address;

    /**
     * @var integer
     *
     * @ORM\Column(name="cod_state", type="integer", length=4, nullable=true)
    
     */
    private $codState;

    /**
     * @var string
     *
     * @ORM\Column(name="address_state", type="string", length=255, nullable=true)
     
     */
    private $addressState;

    /**
     * @var string
     *
     * @ORM\Column(name="address_city", type="string", length=255, nullable=true)
     
     */
    protected $addressCity;

    /**
     * @var integer
     *
     * @ORM\Column(name="cod_city", type="integer", length=4, nullable=true)
    
     */
    private $codCity;


    /**
     * @var string
     *
     * @ORM\Column(name="address_phone", type="string", length=48, nullable=true)
     */
    protected $addressPhone;
	
	/**
     * @var string
     *
     * @ORM\Column(name="address_cellphone", type="string", length=48, nullable=true)
     * @Assert\NotNull()
     */
    protected $addressCellphone;

    /**
     * @var string
     *
     * @ORM\Column(name="address_email", type="string", length=200, nullable=true)
     * @Assert\NotNull()
     * @Assert\Email()
     */
    protected $addressEmail;
	
	/**
     * @var string
     *
     * @ORM\Column(name="address_send", type="boolean", nullable=true, options={"default": true})
     * @Assert\NotNull()
     */
    protected $addressSend;
	
	/**
     * @var string
     *
     * @ORM\Column(name="insurace_u", type="boolean", nullable=false, options={"default": false})
     */
    protected $insuranceU;
	
	
	
	 /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(name="updated", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updated;
	
	
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {

        return $this->id;
    } 
 
    /**
     * Set name
     *
     * @param string $name
     * @return PublicClient
     */
    // public function setName($name)
    // {
        // $this->name = $name;

        // return $this;
    // }

    /**
     * Get name
     *
     * @return string 
     */
    // public function getName()
    // {
        // return $this->name;
    // }
	
    /**
     * Set lastname1
     *
     * @param string $lastname1
     * @return self
     */
    // public function setLastname1($lastname1)
    // {
        // $this->lastname1 = $lastname1;

        // return $this;
    // }

    /**
     * Set lastname2
     *
     * @param string $lastname2
     * @return self
     */
    // public function setLastname2($lastname2)
    // {
        // $this->lastname2 = $lastname2;

        // return $this;
    // }
	
	/**
     * Get lastname1
     *
     * @return string
     */
    // public function getLastname1()
    // {
        // return $this->lastname1;
    // }

    /**
     * Get lastname2
     *
     * @return string
     */
    // public function getLastname2()
    // {
        // return $this->lastname2;
    // }

    /**
     * Set code
     *
     * @param string $code
     * @return PublicClient
     */
    // public function setCode($code)
    // {
        // $this->code = $code;

        // return $this;
    // }

    /**
     * Get code
     *
     * @return string 
     */
    // public function getCode()
    // {
        // return $this->code;
    // }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     * @return PublicClient
     */
    // public function setBirthday($birthday)
    // {
        // $this->birthday = $birthday;

        // return $this;
    // }

    /**
     * Get birthday
     *
     * @return \DateTime 
     */
    // public function getBirthday()
    // {
        // return $this->birthday;
    // }

    /**
     * Set address
     *
     * @param string $address
     * @return PublicClient
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set codState
     *
     * @param integer $codState
     * @return PublicClient
     */
    public function setCodState($codState)
    {
        $this->codState = $codState;

        return $this;
    }

    /**
     * Get codState
     *
     * @return integer 
     */
    public function getCodState()
    {
        return $this->codState;
    }

    /**
     * Set addressState
     *
     * @param string $addressState
     * @return PublicClient
     */
    public function setAddressState($addressState)
    {
        $this->addressState = $addressState;

        return $this;
    }

    /**
     * Get addressState
     *
     * @return string 
     */
    public function getAddressState()
    {
        return $this->addressState;
    }

    /**
     * Set addressCity
     *
     * @param string $addressCity
     * @return PublicClient
     */
    public function setAddressCity($addressCity)
    {
        $this->addressCity = $addressCity;

        return $this;
    }

    /**
     * Get addressCity
     *
     * @return string 
     */
    public function getAddressCity()
    {
        return $this->addressCity;
    }

    /**
     * Set codCity
     *
     * @param integer $codCity
     * @return PublicClient
     */
    public function setCodCity($codCity)
    {
        $this->codCity = $codCity;

        return $this;
    }

    /**
     * Get codCity
     *
     * @return integer 
     */
    public function getCodCity()
    {
        return $this->codCity;
    }

    /**
     * Set addressPhone
     *
     * @param string $addressPhone
     * @return PublicClient
     */
    public function setAddressPhone($addressPhone)
    {
        $this->addressPhone = $addressPhone;

        return $this;
    }

    /**
     * Get addressPhone
     *
     * @return string 
     */
    public function getAddressPhone()
    {
        return $this->addressPhone;
    }

    /**
     * Set addressEmail
     *
     * @param string $addressEmail
     * @return PublicClient
     */
    public function setAddressEmail($addressEmail)
    {
        $this->addressEmail = $addressEmail;

        return $this;
    }

    /**
     * Get addressEmail
     *
     * @return string 
     */
    public function getAddressEmail()
    {
        return $this->addressEmail;
    }

    /**
     * Set addressSend
     *
     * @param string $addressSend
     * @return PublicClient
     */
    public function setAddressSend($addressSend)
    {
        $this->addressSend = $addressSend;

        return $this;
    }

    /**
     * Get addressSend
     *
     * @return string 
     */
    public function getAddressSend()
    {
        return $this->addressSend;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return PublicClient
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return PublicClient
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }
	
     /**
     * Getter $insurance
     *
     * @return \SeguroBundle\Entity\PublicInsurance
     */
    public function getInsurance()
    {
        return $this->insurance;
    }

    /**
     * Setter $insurance
     *
     * @param \SeguroBundle\Entity\PublicInsurance $insurance
     * @return PublicClient
     */
    public function setInsurance(\SeguroBundle\Entity\PublicInsurance $insurance = null)
    {
        $this->insurance = $insurance;

        return $this;
    }
	

    /**
     * Set insuranceU
     *
     * @param boolean $insuranceU
     * @return PublicClient
     */
    public function setInsuranceU($insuranceU)
    {
        $this->insuranceU = $insuranceU;

        return $this;
    }

    /**
     * Get insuranceU
     *
     * @return boolean 
     */
    public function getInsuranceU()
    {
        return $this->insuranceU;
    }

    
    /**
     * Set addressCellphone
     *
     * @param string $addressCellphone
     * @return PublicClient
     */
    public function setAddressCellphone($addressCellphone)
    {
        $this->addressCellphone = $addressCellphone;

        return $this;
    }

    /**
     * Get addressCellphone
     *
     * @return string 
     */
    public function getAddressCellphone()
    {
        return $this->addressCellphone;
    }
}
