<?php

namespace SeguroBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * CargaRepository
 *
 */
class PublicCargaRepository extends EntityRepository
{
	 public function cargas($id){

        $query = $this->createQueryBuilder('i')
        ->where('i.insurance = :dato')
        ->setParameter('dato', $id)
        ->getQuery()->getResult();
         
        return $query;
    }
}
