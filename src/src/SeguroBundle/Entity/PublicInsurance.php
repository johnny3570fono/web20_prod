<?php

namespace SeguroBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityRepository;


use AppBundle\Entity\Product;
use AppBundle\Entity\BaseProduct;
use AppBundle\Entity\Plan;
use AppBundle\Entity\Segment;
use AppBundle\Entity\SegmentFamily;

/**
 * PublicInsurance
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="SeguroBundle\Entity\Repository\PublicInsuranceRepository")
 */
class PublicInsurance extends BaseProduct
{
    
     private $entityManager;

    const STATUS_DRAFT = 'borrador';
    const STATUS_ISSUED = 'emitido';
    const STATUS_SEND =  'enviado';
    

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="INSURANCE_ID_SEQ")
     */
    private $id;

    /**
     * @ORM\Column(name="code", type="string", length=3)
     * @Assert\NotBlank(groups={"all"})
     */
     protected $code;
     
     /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Product") 
     */
    private $product;
    
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Segment")
     */
    private $segment;
    
     /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\SegmentFamily")
     */
    private $family;
    
    
    /**
     * @ORM\OneToOne(targetEntity="PublicClient", mappedBy="insurance", cascade={"persist", "remove"})
     */
     private $client; 
     
     /**
     * @var string
     *
     * @ORM\Column(name="plan", type="json_array", nullable=true)
     */
    private $plan;

     /**
     * @var string $pri
     * @ORM\Column(name="pri", type="string", length=5, nullable=true)
     */
    private $pri;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="travelDate", nullable=true)
     */
    private $travelDate;
    

    /**
     * @ORM\OneToOne(targetEntity="PublicHome", mappedBy="insurance", cascade={"persist", "remove"})
     **/
    private $home;
	
	
	/**
     * @ORM\OneToOne(targetEntity="PublicPaiement", mappedBy="insurance", cascade={"persist", "remove"})
     **/
    private $paiement;
	
	/**
     * @ORM\OneToOne(targetEntity="PublicPatPass", mappedBy="insurance", cascade={"persist", "remove"})
     **/
    private $patPass;
	
	/**
     * @ORM\OneToMany(
     *  targetEntity="PublicInsuranceQuestion",
     *  mappedBy="insurance",
     *  cascade={"all"}
     * )
     **/
    private $questions;
	
	
	/**
     * @ORM\OneToMany(
     *  targetEntity="PublicCarga",
     *  mappedBy="insurance",
     *  cascade={"persist", "remove"},
     *  orphanRemoval=true
     * )
     **/
    private $cargas;
	
	 /**
     * @ORM\OneToMany(
     *  targetEntity="PublicBeneficiary",
     *  mappedBy="insurance",
     *  cascade={"persist", "remove"},
     *  orphanRemoval=true
     * )
     **/
    private $beneficiaries;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="string", length=32)
     */
    private $status;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(name="updated", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updated;
	
	 


    public function __construct(Product $product, Plan $plan){
        $reflect = new \ReflectionClass($product);
        $property = $reflect->getProperties(); 
        foreach($property as $prop){
            if(method_exists($this, 'set'.$prop->getName())){
                if(method_exists($product, 'get'.$prop->getName())){
                    try{
                        $this->{'set'.$prop->getName()}( $product->{'get'.$prop->getName()}() );
                    } catch(\Exception $e){
                        //excepcion de clase llamada para el reflex
                    }
                }
            }
        }
        
		$this->beneficiaries = new \Doctrine\Common\Collections\ArrayCollection();
        $this->cargas = new \Doctrine\Common\Collections\ArrayCollection();
        $this->questions = new \Doctrine\Common\Collections\ArrayCollection();
		$this->setProduct($product);
        $this->setPlan($plan);
        $this->setStatus(self::STATUS_DRAFT);
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set plan
     *
     * @param array $plan
     * @return PublicInsurance
     */
    public function setPlan($plan)
    {
        $this->plan = $plan;

        return $this;
    }

    /**
     * Get plan
     *
     * @return array 
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * Set pri
     *
     * @param string $pri
     * @return PublicInsurance
     */
    public function setPri($pri)
    {
        $this->pri = $pri;

        return $this;
    }

    /**
     * Get pri
     *
     * @return string 
     */
    public function getPri()
    {
        return $this->pri;
    }

    /**
     * Set travelDate
     *
     * @param string $travelDate
     * @return PublicInsurance
     */
    public function setTravelDate($travelDate)
    {
        $this->travelDate = $travelDate;

        return $this;
    }

    /**
     * Get travelDate
     *
     * @return string 
     */
    public function getTravelDate()
    {
        return $this->travelDate;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return PublicInsurance
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return PublicInsurance
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return PublicInsurance
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set product
     *
     * @param \AppBundle\Entity\Product $product
     * @return PublicInsurance
     */
    public function setProduct(Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \AppBundle\Entity\Product 
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set segment
     *
     * @param \SeguroBundle\Entity\PublicSegment $segment
     * @return PublicInsurance
     */
    public function setSegment(Segment $segment = null)
    {
        $this->segment = $segment;

        return $this;
    }

    /**
     * Get segment
     *
     * @return \SeguroBundle\Entity\PublicSegment 
     */
    public function getSegment()
    {
        return $this->segment;
    }

    /**
     * Set family
     *
     * @param \SeguroBundle\Entity\PublicSegmentFamily $family
     * @return PublicInsurance
     */
    public function setFamily(SegmentFamily $family = null)
    {
        $this->family = $family;

        return $this;
    }

    /**
     * Get family
     *
     * @return \SeguroBundle\Entity\PublicSegmentFamily 
     */
    public function getFamily()
    {
        return $this->family;
    }



    /**
     * Set home
     *
     * @param \SeguroBundle\Entity\PublicHome $home
     * @return PublicInsurance
     */
    public function setHome(\SeguroBundle\Entity\PublicHome $home = null)
    {
		$home->setInsurance($this);
		
        $this->home = $home;

        return $this;
    }

    /**
     * Get home
     *
     * @return \SeguroBundle\Entity\PublicHome 
     */
    public function getHome()
    {
        return $this->home;
    }

    /**
     * Set client
     *
     * @param \SeguroBundle\Entity\PublicClient $client
     * @return PublicInsurance
     */
    public function setClient(\SeguroBundle\Entity\PublicClient $client = null)
    {
		$client->setInsurance($this);
		
        $this->client = $client;
		
        return $this;
    }

    /**
     * Get client
     *
     * @return \SeguroBundle\Entity\PublicClient 
     */
    public function getClient()
    {	
		
        return $this->client;
    }

    /**
     * Add beneficiaries
     *
     * @param \SeguroBundle\Entity\PublicBeneficiary $beneficiaries
     * @return PublicInsurance
     */
    public function addBeneficiary(\SeguroBundle\Entity\PublicBeneficiary $beneficiaries)
    {
		$beneficiaries->setInsurance($this);
		 
        $this->beneficiaries[] = $beneficiaries;


        return $this;
    }

    /**
     * Remove beneficiaries
     *
     * @param \SeguroBundle\Entity\PublicBeneficiary $beneficiaries
     */
    public function removeBeneficiary(\SeguroBundle\Entity\PublicBeneficiary $beneficiaries)
    {
        $this->beneficiaries->removeElement($beneficiaries);
    }

    /**
     * Get beneficiaries
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBeneficiaries()
    {
        return $this->beneficiaries;
    }
	
	/**
     * Tiene beneficiarios
     *
     * @return boolean
     */
    public function hasBeneficiaries()
    {
        return ($this->getBeneficiaries()->count() > 0);
    }
	
	
	public function getInstanceInsurance(){
		return $this;
	}

    /**
     * Add cargas
     *
     * @param \SeguroBundle\Entity\PublicCarga $cargas
     * @return PublicInsurance
     */
    public function addCarga(\SeguroBundle\Entity\PublicCarga $cargas)
    {
        $cargas->setInsurance($this);

        $this->cargas[] = $cargas;

        return $this;
    }

    /**
     * Remove cargas
     *
     * @param \SeguroBundle\Entity\PublicCarga $cargas
     */
    public function removeCarga(\SeguroBundle\Entity\PublicCarga $cargas)
    {
        $this->cargas->removeElement($cargas);
    }

    /**
     * Get cargas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCargas()
    {
        return $this->cargas;
    }
	
	
	/**
     * Tiene cargas
     *
     * @return boolean
     */
    public function hasCargas()
    {
        return ($this->getCargas()->count() > 0);
    }
	
	/**
     * Add questions
     *
     * @param \SeguroBundle\Entity\PublicInsuranceQuestion $questions
     * @return Insurance
     */
    public function addQuestion(\SeguroBundle\Entity\PublicInsuranceQuestion $questions)
    {
        $questions->setInsurance($this);
		
        $this->questions->add($questions);

        return $this;
    }

    /**
     * Remove questions
     *
     * @param \SeguroBundle\Entity\PublicInsuranceQuestion $questions
     */
    public function removeQuestion(\SeguroBundle\Entity\PublicInsuranceQuestion $questions)
    {
        $this->questions->removeElement($questions);
    }
	
	/**
     * Set the value of Questions
     *
     * @param mixed questions
     *
     * @return self
     */
    public function setQuestions($questions)
    {
        $this->questions = $questions;

        return $this;
    }
	
    /**
     * Get questions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    public function getAnswerByQuestion($question)
    {
        $questions = $this->getQuestions();
        $answer = null;

        foreach ($questions as $q) {
            if (strtolower($q->getQuestion()) == strtolower($question)) {
                $answer = $q->getAnswer();
            }
        }

        return $answer;
    }
    public function getEnfermedad()
    {
        $enfermedad=$this->getAnswerByQuestion('Enfermedad');

        if($enfermedad==null){
            $enfermedad=$this->getAnswerByQuestion('Emfermedad');

        }
        return $enfermedad;
        
    }

    public function getEstatura()
    {
        return $this->getAnswerByQuestion('estatura');
    }

    public function getPeso()
    {
        return $this->getAnswerByQuestion('peso');
    }

    /**
     * Set paiement
     *
     * @param \SeguroBundle\Entity\PublicPaiement $paiement
     * @return PublicInsurance
     */
    public function setPaiement(\SeguroBundle\Entity\PublicPaiement $paiement = null)
    {
        $this->paiement = $paiement;

        return $this;
    }

    /**
     * Get paiement
     *
     * @return \SeguroBundle\Entity\PublicPaiement 
     */
    public function getPaiement()
    {
        return $this->paiement;
    }

    /**
     * Set patPass
     *
     * @param \SeguroBundle\Entity\PublicPatPass $patPass
     * @return PublicInsurance
     */
    public function setPatPass(\SeguroBundle\Entity\PublicPatPass $patPass = null)
    {
	    $patPass->setInsurance($this);
		
        $this->patPass = $patPass;

        return $this;
    }

    /**
     * Get patPass
     *
     * @return \SeguroBundle\Entity\PublicPatPass 
     */
    public function getPatPass()
    {
        return $this->patPass;
    }
    
    public function getNumPropuesta()
    {

       $propuesta=$this->getId();
           $cant=strlen($propuesta);

           $total=8-$cant;
           $Id='';
           for($i=0;$i< $total;$i++){
                    $Id.='0';
            }
           $propuesta=$Id.$propuesta;
          

               
               
            return $propuesta;

    }
    
    public function getCodFuente()
    {
        // 001: Banco
        // 002: Banefe

        return '249';
    }
    
    public function getNomEje()
    {
        // Len 30
        return 'Nombre Ejecutivo';
    }
    
    //siempre tarjeta
    public function getCodMedPago()
    {
        //P = banco; T = tarjeta; E = efectivo o valevista; C = cuota comercio; Q = chequera; R = Regalo
         // $paymethod=$this->getPaymentMethod();
                   
        // if($paymethod!=null){
           // $cant=strlen($paymethod);
           // if($cant>12){
           
            // $method='T';
            // }else
            // {
            // $method='P';
            // }
        // }else{
           // $method='P';
        // }
        return 'T';
    }
	
	
}
