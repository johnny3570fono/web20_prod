<?php

namespace SeguroBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PublicHome
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="SeguroBundle\Entity\PublicHomeRepository")
 */
class PublicHome
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255)
     */
    private $address;

    /**
     * @var Insurance
     *
     * @ORM\OneToOne(targetEntity="PublicInsurance", inversedBy="home", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $insurance;

    /**
     * @var integer
     *
     * @ORM\Column(name="cod_state", type="integer", length=4, nullable=true)
     * @Assert\NotNull()
     */
    private $codState;

    /**
     * @var string
     *
     * @ORM\Column(name="address_state", type="string", length=255, nullable=true)
     * @Assert\NotNull()
     */
    private $addressState;

    /**
     * @var string
     *
     * @ORM\Column(name="address_city", type="string", length=255, nullable=true)
     * @Assert\NotNull()
     */
    protected $addressCity;

    /**
     * @var integer
     *
     * @ORM\Column(name="cod_city", type="integer", length=4, nullable=true)
     * @Assert\NotNull()
     */
    private $codCity;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Home
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * To String
     *
     * @return String
     */
    public function __toString()
    {
        return $this->getAddress() . ' ' . $this->getCodState() . ' ' . $this->getAddressState() . ' ' . $this->getAddressCity();
    }

    /**
     * Set insurance
     *
     * @param \SeguroBundle\Entity\PublicInsurance $insurance
     * @return Home
     */
    public function setInsurance(\SeguroBundle\Entity\PublicInsurance $insurance = null)
    {
        $this->insurance = $insurance;

        return $this;
    }

    /**
     * Get insurance
     *
     * @return \SeguroBundle\Entity\PublicInsurance
     */
    public function getInsurance()
    {
        return $this->insurance;
    }

    /**
     * Set addressState
     *
     * @param string $addressState
     * @return Payer
     */
    public function setAddressState($addressState)
    {
        $this->addressState = $addressState;

        return $this;
    }

    /**
     * Get addressState
     *
     * @return string
     */
    public function getAddressState()
    {
        return $this->addressState;
    }

    /**
     * Set codState
     *
     * @param integer $codState
     * @return Payer
     */
    public function setCodState($codState)
    {
        $this->codState = $codState;

        return $this;
    }

    /**
     * Get codState
     *
     * @return integer
     */
    public function getCodState()
    {
        return $this->codState;
    }


    /**
     * Set addressCity
     *
     * @param string $addressCity
     * @return Payer
     */
    public function setAddressCity($addressCity)
    {
        $this->addressCity = $addressCity;

        return $this;
    }

    /**
     * Get addressCity
     *
     * @return string
     */
    public function getAddressCity()
    {
        return $this->addressCity;
    }

    /**
     * Set codCity
     *
     * @param integer $codCity
     * @return Payer
     */
    public function setCodCity($codCity)
    {
        $this->codCity = $codCity;

        return $this;
    }

    /**
     * Get codCity
     *
     * @return cod
     */
    public function getCodCity()
    {
        return $this->codCity;
    }
}
