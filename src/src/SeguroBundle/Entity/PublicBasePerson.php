<?php

namespace SeguroBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints as AppAssert;

/**
 * @ORM\MappedSuperclass()
 */
class PublicBasePerson implements \JsonSerializable
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotNull()
     */
    protected $name;
	
	
	/**
     * @var string
     *
     * @ORM\Column(name="lastname1", type="string", length=255)
     * @Assert\NotNull()
     */
    protected $lastname1;
	
	/**
     * @var string
     *
     * @ORM\Column(name="lastname2", type="string", length=255)
     * @Assert\NotNull()
     */
    protected $lastname2;

    
    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=36)
     * @AppAssert\IsRun()
     * @Assert\NotNull()
     */
    protected $code;

    /**
     * @var \Date
     *
     * @ORM\Column(name="birthday", type="date", nullable=true)
     * @Assert\NotNull()
     */
    protected $birthday;

     

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
	
	
	/**
     * Set name
     *
     * @return string
     */
    public function setLastname1($name)
    {
		$this->lastname1 = $name;
		
        return $this->lastname1;
    }
	
	/**
     * Get name
     *
     * @return string
     */
    public function getLastname1()
    {
        return $this->lastname1;
    }
	
	/**
     * Set name
     *
     * @return string
     */
    public function setLastname2($name)
    {
        $this->lastname2= $name;
		
        return $this->lastname2;
    }
	
	/**
     * Get name
     *
     * @return string
     */
    public function getLastname2()
    {
        return $this->lastname2;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return self
     */
    public function setCode($code)
    {
        if (strlen($code) > 1) {
            $code = preg_replace('/[^0-9kK]/', '', $code);
            

            $this->code = substr($code, 0, -1) . '-' . substr($code, -1);
        } else {
            $this->code = $code;
        }

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCodeRaw()
    {
        return preg_replace('/[^0-9kK]/', '', $this->getCode());
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCodeOnlyNumber()
    {
        return substr($this->getCode(), 0, -2);
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCodeOnlyDv()
    {
        return substr($this->getCode(), -1);
    }

    /**
     * Set birthday
     *
     * @param \Date $birthday
     * @return self
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \Date
     */
    public function getBirthday()
    {
        return $this->birthday;
    }


    /**
     * To String
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getName() . ' (' . $this->getCode() . ')';
    }

   
    /**
     * Años desde la fecha de nacimiento al día de hoy.
     *
     * @return int
     */
    public function getAge()
    {
        if (!($this->getBirthday() instanceof \DateTime)) {
            return 0;
        } else {
            $today = new \DateTime();

            return $today->diff($this->getBirthday())->y;
        }
    }

   

    /**
     * Entrega un array asositivo con Column -> Value
     *
     * @return array
     */
    public function toArray()
    {
        return [
                'id' => $this->getId(),
                'name' => $this->getName(),
                'code' => $this->getCode(),
                'day' => $this->getBirthday() ? $this->getBirthday()->format('j') : null,
                'month' => $this->getBirthday() ? $this->getBirthday()->format('n') : null,
                'year' => $this->getBirthday() ? $this->getBirthday()->format('Y') : null,
        ];
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }
}
