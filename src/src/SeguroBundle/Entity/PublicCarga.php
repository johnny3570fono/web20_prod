<?php

namespace SeguroBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Carga
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="SeguroBundle\Entity\Repository\PublicCargaRepository")
 * @ORM\AttributeOverrides({
 *      @ORM\AttributeOverride(name="code",
 *          column=@ORM\Column(
 *              nullable=true
 *          )
 *      ),
 * })
 */
class PublicCarga extends PublicPersonRelationship
{
   /**
     * @ORM\ManyToOne(targetEntity="PublicInsurance", inversedBy="cargas", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $insurance;

    /**
     * Set insurance
     *
     * @param \SeguroBundle\Entity\PublicInsurance $insurance
     * @return self
     */
    public function setInsurance(\SeguroBundle\Entity\PublicInsurance $insurance = null)
    {
        $this->insurance = $insurance;

        return $this;
    }

    /**
     * Get insurance
     *
     * @return \SeguroBundle\Entity\PublicInsurance
     */
    public function getInsurance()
    {
        return $this->insurance;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lastname1
     *
     * @param string $lastname1
     * @return PublicCarga
     */
    public function setLastname1($lastname1)
    {
        $this->lastname1 = $lastname1;

        return $this;
    }

    /**
     * Get lastname1
     *
     * @return string 
     */
    public function getLastname1()
    {
        return $this->lastname1;
    }

    /**
     * Set lastname2
     *
     * @param string $lastname2
     * @return PublicCarga
     */
    public function setLastname2($lastname2)
    {
        $this->lastname2 = $lastname2;

        return $this;
    }

    /**
     * Get lastname2
     *
     * @return string 
     */
    public function getLastname2()
    {
        return $this->lastname2;
    }
}
