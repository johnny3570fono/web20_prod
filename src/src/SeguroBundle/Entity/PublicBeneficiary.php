<?php

namespace SeguroBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * PublicBeneficiary
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="SeguroBundle\Entity\PublicBeneficiaryRepository")
 */
class PublicBeneficiary extends PublicPersonRelationship
{
    /**
     * @ORM\ManyToOne(targetEntity="PublicInsurance", inversedBy="beneficiaries", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $insurance;

    /**
     * Set insurance
     *
     * @param \AppBundle\Entity\Insurance $insurance
     * @return self
     */
    public function setInsurance(\SeguroBundle\Entity\PublicInsurance $insurance = null)
    {
        $this->insurance = $insurance;

        return $this;
    }

    /**
     * Get insurance
     *
     * @return \SeguroBundle\Entity\Insurance
     */
    public function getInsurance()
    {
        return $this->insurance;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="capital", type="integer", nullable=true, options={"default"=0})
     * @Assert\NotBlank()
     * @Assert\LessThanOrEqual(value=100)
     * @Assert\GreaterThanOrEqual(value=1)
     */
    private $capital;

    /**
     * Getter $capital
     *
     * @return integer
     */
    public function getCapital()
    {
        return $this->capital;
    }

    /**
     * Setter $capital
     *
     * @param integer $capital
     * @return \SeguroBundle\Entity\Beneficiary
     */
    public function setCapital($capital)
    {
        $this->capital = $capital;

        return $this;
    }
 
    /**
     * Get the value of Age Min
     *
     * @return string
     */
    public function getAgeMin()
    {
        $ageMin=0;
        return $ageMin;
    }

    /**
     * Get the value of Age Max
     *
     * @return string
     */
    public function getAgeMax()
    {
        $AgeMax=999;
        return $AgeMax;
    }

    

    /**
     * Getter $id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Setter $id
     *
     * @param integer $id
     * @return \AppBundle\Entity\Beneficiary
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function toArray()
    {
        return array_merge(
            parent::toArray(),
            [
                'capital' => $this->getCapital(),
            ]
        );
    }
}
