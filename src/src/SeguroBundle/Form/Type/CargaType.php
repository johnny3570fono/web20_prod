<?php

namespace SeguroBundle\Form\Type;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

use SeguroBundle\Entity\PublicCarga;
use SeguroBundle\Entity\PublicPersonRelationship;
use SeguroBundle\Form\Type\RelationshipType;
/**
 * Formulario Carga
 */
class CargaType extends RelationshipType
{
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
        $builder = parent::buildForm($builder, $options);

        $builder->get('code')->setRequired(false);

        return $builder;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SeguroBundle\Entity\PublicCarga',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'carga';
    }

    /**
     * @return array
    **/
    public function getRelationships(){
        return [
            PublicPersonRelationship::REALTIONSHIP_CHILD,
            PublicPersonRelationship::REALTIONSHIP_SPOUSE,
            PublicPersonRelationship::REALTIONSHIP_OTHER,
            PublicPersonRelationship::REALTIONSHIP_CONVIVIENTE,
            PublicPersonRelationship::REALTIONSHIP_PAREJA,
            PublicPersonRelationship::REALTIONSHIP_NIETO
        ];
    }
}
