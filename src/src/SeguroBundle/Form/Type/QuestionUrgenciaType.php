<?php
namespace SeguroBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use SeguroBundle\Form\EventListener\AddPlanFieldSubscriber;
use SeguroBundle\Form\EventListener\AddOtherFormsFieldSubscriber;

use Doctrine\ORM\EntityManager;

/**
 * Formulario Insurance
 */
class QuestionUrgenciaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('actividad_ejemplo', new QuestionType(['question' => 'Actividad Ejemplo','answer'=>'no']));
        $builder->add('actividad_riesgo', new QuestionType(['question' => 'Actividad Riesgo','answer'=>'no']));
        $builder->add('deportes_riesgo', new QuestionType(['question' => 'Deportes Riesgo','answer'=>'no']));
        $builder->add('accept', new QuestionAcceptType());

        return $builder;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'questions';
    }
}
