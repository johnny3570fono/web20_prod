<?php

namespace SeguroBundle\Form\Type;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use AppBundle\Form\DataTransformer\CiudadComunaDataTransformer;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;

use SeguroBundle\Form\EventListener\AddCommunityFieldsSubscriber;

/**
 * Formulario Insured
 */
class ClientType extends BasePersonType
{
    private $states;
    private $codStates;
    private $manager;
	
	private $entityManager;


    public function __construct($states, $codStates, $yearMin = null, $yearMax = null, $manager)
    {
        parent::__construct($yearMin, $yearMax);

        $this->states = $states;
        $this->codStates = $codStates;
        $this->manager = $manager;
		$this->entityManager = $this->manager;
    }

    public function getManager()
    {
		
        return $this->manager;
    }

    public function setManager($manager)
    {
        $this->manager = $manager;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
		$related_community = $this->getRelatedCommunity();
		$builder->add('addressCity', 'choice', array(
            'required'    => true,
            'data' => 0,
            'choices'     => $this->buildCity()
        ));
		$builder->add('addressState','choice' , array(
			'required'    => true,
            'choices'     => $this->buildState(),
            'data' => 0,
			'choice_attr' => function ($choiceValue, $choiceKey, $definedValue) use ($related_community){
                 if( isset($related_community[$choiceValue])) {
                    return array('ng-if' => 'user.addressCity==\''.$related_community[$choiceValue].'\'');
                }
				return array();
			}, 
		));
        $builder->add('address', 'text', [
                'label' => 'Dirección'
            ])

            ->add('addressCellphone', 'text', [
                'label' => 'Telefono',
                //'pattern' => '\d{6}-\d{4}',
                'constraints' => [
                        new \AppBundle\Validator\Constraints\IsLength()
                ]
            ])
            ->add('addressEmail', 'email', [
                'label' => 'E-mail',
                'constraints' => [
                        new  \Symfony\Component\Validator\Constraints\Email()
                    ]
            ]);
		//$builder->add('travelDate', 'text');
		$builder->add('addressSend', 'choice', array(
		   'choices' => array(
				true => 'Si',
				false => 'No', 
			),
			
			'expanded' => true,
		));
		
		$builder->add('insuranceU', 'choice', array(
		   'choices' => array(
				true => 'Si',
				false => 'No', 
			),
			'expanded' => true,
		));
		
		
		
        $builder->addEventSubscriber(new AddCommunityFieldsSubscriber($this->getManager()));

        return parent::buildForm($builder, $options);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SeguroBundle\Entity\PublicClient',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_client';
    }
	
	public function buildCity(){
			
		$choices = array('0' => 'Ciudad o Provincia');
        $types = $this
            ->entityManager
            ->getRepository('AppBundle\Entity\Community')
            ->createQueryBuilder('es')
            ->orderBy('es.descripcion_cuidad_homologo', 'ASC')
			
            ->getQuery()
            ->getResult();

        foreach ($types as $type) {
			if($ciudad = $type->getDescripcionCuidadHomologo()){
				$choices[$ciudad] = $this->mb_ucfirst($ciudad);
			}
        }

        return $choices;
	}
	
	public function buildState(){
		$choices = array('0' => 'Comunas');
        $types = $this
            ->entityManager
            ->getRepository('AppBundle\Entity\Community')
            ->createQueryBuilder('es')
            ->orderBy('es.descripcion_comuna_homologo', 'ASC')
            ->getQuery()
            ->getResult();

        foreach ($types as $type) {
			if($ciudad = $type->getDescripcionComunaHomologo()){
				$choices[$ciudad] = $this->mb_ucfirst($type->getDescripcionComunaHomologo());
			}
        }

        return $choices;
	}
	
	public function getRelatedCommunity() {
         $related  = [];
         $communities = $this
            ->entityManager
            ->getRepository('AppBundle\Entity\Community')
            ->createQueryBuilder('es')
            ->orderBy('es.descripcion_comuna_homologo', 'ASC')
            ->getQuery()
            ->getResult();
        if($communities){
            foreach($communities as $community){
                $related[$community->getDescripcionComunaHomologo()] = $community->getDescripcionCuidadHomologo();
            }
        }
        return $related;
    }
	
	function mb_ucfirst($str){
        $words = explode(" ", $str );
        
        if( count( $words ) ) {
            foreach( $words as $word ) {
                $word = strtolower( utf8_decode( $word ) );
                
                if( $word != 'de' and $word != 'del' ) {
                    $word_a[] = mb_convert_encoding(mb_convert_case($word, MB_CASE_TITLE), "UTF-8");
                }else{
                    $word_a[] = $word;
                }
                
                
                $words  = implode(" ", $word_a);
            }
        }
        
        return $words;
        
        
        // $str = utf8_decode( $str );
		// $words = mb_convert_encoding(mb_convert_case($str, MB_CASE_TITLE), "UTF-8"); 

        // return $words;
	}

}
