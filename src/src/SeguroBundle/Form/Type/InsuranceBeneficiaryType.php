<?php

namespace SeguroBundle\Form\Type;

use Doctrine\ORM\EntityManager;

use SeguroBundle\Entity\PublicInsurance;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Validator\Constraints as Assert;


class InsuranceBeneficiaryType extends AbstractType
{
	private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$yearMin = date('Y');
        $yearMax = date('Y');
       $builder->add('beneficiaries', 'collection', array(
            'type' => new \SeguroBundle\Form\Type\BeneficiaryType($yearMin, $yearMax),
            'allow_add' => true,
            'constraints' => [
                new \AppBundle\Validator\Constraints\UniqueBeneficiaries(),
               // new \AppBundle\Validator\Constraints\InsuredNotBeneficiary(),
                new \AppBundle\Validator\Constraints\UniqueSpouse(),
                new \AppBundle\Validator\Constraints\Capital100()
            ],
            'by_reference' => false,
        ));
				
        
    }
    
    public function getName()
    {
        return 'insurance';
    }
	
}