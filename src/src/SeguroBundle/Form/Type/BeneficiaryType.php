<?php

namespace SeguroBundle\Form\Type;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;


use SeguroBundle\Form\Type\RelationshipType;

use SeguroBundle\Entity\PublicPersonRelationship;

/**
 * Formulario Beneficiary
 */
class BeneficiaryType extends RelationshipType
{    
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
        $builder = parent::buildForm($builder, $options);

        $builder->add('capital', 'number', [
            'label' => 'Capital %',
            'constraints' => [
                    new Assert\GreaterThan(array('value' => 0))
            ]
        ]);

        return $builder;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SeguroBundle\Entity\PublicBeneficiary',
            'birthday' => [
                    'yearMin' => date('Y') - 100,
                    'yearMax' => date('Y') ,
                ]
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'beneficiary';
    }

    /**
     * @return array
    **/
    public function getRelationships(){
        return [
            PublicPersonRelationship::REALTIONSHIP_CHILD,
            PublicPersonRelationship::REALTIONSHIP_SPOUSE,
            PublicPersonRelationship::REALTIONSHIP_FATHER,
            PublicPersonRelationship::REALTIONSHIP_MOTHER,
            PublicPersonRelationship::REALTIONSHIP_SOBRINO,
            PublicPersonRelationship::REALTIONSHIP_HERMANO,
            PublicPersonRelationship::REALTIONSHIP_NIETO,
            PublicPersonRelationship::REALTIONSHIP_CONVIVIENTE,
            PublicPersonRelationship::REALTIONSHIP_OTHER
        ];
    }
}
