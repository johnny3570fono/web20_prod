<?php
namespace SeguroBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use SeguroBundle\Form\EventListener\AddPlanFieldSubscriber;
use SeguroBundle\Form\EventListener\AddOtherFormsFieldSubscriber;

use Doctrine\ORM\EntityManager;

/**
 * Formulario Insurance
 */
class InsuranceTravelType extends InsuranceCargasType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder = parent::buildForm($builder, $options);
        $builder->add('price','hidden')
                ->add('pri', 'hidden');
		//$builder->add('travelDate', 'text');
        return $builder;
    }
}
