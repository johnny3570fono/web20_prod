<?php
namespace SeguroBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use SeguroBundle\Form\EventListener\AddCargasFieldSubscriber;

use Doctrine\ORM\EntityManager;

/**
 * Formulario Insurance
 */
class InsuranceCargasType extends InsuranceType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return $builder;
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder = parent::buildForm($builder, $options);
        $builder->add('price','hidden');


        $builder->addEventSubscriber(new AddCargasFieldSubscriber());

        return $builder;
    }
}
