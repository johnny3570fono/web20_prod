<?php

namespace SeguroBundle\Form\Type;

use Doctrine\ORM\EntityManager;

use SeguroBundle\Entity\PublicHome;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

use SeguroBundle\Form\EventListener\AddCommunityFieldsSubscriber;

class HomeType extends AbstractType
{
	private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $related_community = $this->getRelatedCommunity();

       
        $builder->add('address', 'text');
		
		$builder->add('address_city', 'choice', array(
            'required'    => true,
            'choices'     => $this->buildCity()
        ));
		
		$builder->add('address_state','choice' , array(
			'required'    => true,
            'choices'     => $this->buildState(),
			'choice_attr' => function ($choiceValue, $choiceKey, $definedValue) use ($related_community){
                 if( isset($related_community[$choiceValue])) {
                    return array('ng-if' => 'home.address_city==\''.$related_community[$choiceValue].'\'');
                }
				return array();
			}, 
		));
       
	    $builder->addEventSubscriber(new AddCommunityFieldsSubscriber($this->entityManager));
        
    }
    
	public function buildCity(){
			
		$choices = array('0' => 'Ciudad');
        $types = $this
            ->entityManager
            ->getRepository('AppBundle\Entity\Community')
            ->createQueryBuilder('es')
            ->orderBy('es.descripcion_cuidad_homologo', 'ASC')
			
            ->getQuery()
            ->getResult();

        foreach ($types as $type) {
			if($ciudad = $type->getDescripcionCuidadHomologo()){
				$choices[$ciudad] = $this->mb_ucfirst( $ciudad, true );
			}
        }

        return $choices;
	}
	
	public function buildState(){
		$choices = array('0' => 'Comunas');
        $types = $this
            ->entityManager
            ->getRepository('AppBundle\Entity\Community')
            ->createQueryBuilder('es')
            ->orderBy('es.descripcion_comuna_homologo', 'ASC')
            ->getQuery()
            ->getResult();

        foreach ($types as $type) {
			if($ciudad = $type->getDescripcionComunaHomologo()){
				$choices[$ciudad] = $this->mb_ucfirst( $type->getDescripcionComunaHomologo(), true );
			}
        }

        return $choices;
	}
	
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => PublicHome::class,
        ));
    }

	
	
    public function getName()
    {
        return 'home';
    }
	
    public function getRelatedCommunity() {
         $related  = [];
         $communities = $this
            ->entityManager
            ->getRepository('AppBundle\Entity\Community')
            ->createQueryBuilder('es')
            ->orderBy('es.descripcion_comuna_homologo', 'ASC')
            ->getQuery()
            ->getResult();
        if($communities){
            foreach($communities as $community){
                $related[$community->getDescripcionComunaHomologo()] = $community->getDescripcionCuidadHomologo();
            }
        }
        return $related;
    }
    
    function mb_ucfirst($str, $to_lower = false, $charset = 'utf-8'){
		$first = mb_strtoupper(mb_substr($str, 0, 1, $charset), $charset);
		$end = mb_substr($str, 1, mb_strlen($str, $charset), $charset);
		if ( $to_lower ) {
			$end = mb_strtolower($end, $charset);
		}
		
		return $first . $end;
	}
}