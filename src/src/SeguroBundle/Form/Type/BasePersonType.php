<?php

namespace SeguroBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\CallbackTransformer;

use SeguroBundle\Form\EventListener\AddBirthdayFieldSubscriber;

/**
 * Formulario BasePerson
 */
class BasePersonType extends AbstractType
{
    private $yearMin;
    private $yearMax;

    public function __construct($yearMin = null, $yearMax = null)
    {
        /*
        $this->yearMin = $yearMin ? $yearMin : date('Y') - 99;
        $this->yearMax = $yearMax ? $yearMax : date('Y');
        */
        $this->yearMin = date('Y') - 86;
        $this->yearMax = date('Y');
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @return null
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $years = range($this->yearMin, $this->yearMax);

        $builder
            ->add('name', 'text', ['label' => 'Nombre'])
			->add('lastname1', 'text', ['label' => 'Apellido Paterno'])
            ->add('lastname2', 'text', ['label' => 'Apellido Materno'])
            ->add('code', 'text', [
                'label' => 'Rut',
                'constraints' => [
                    new \AppBundle\Validator\Constraints\IsRun()
                ]
            ])->add('birthday', 'birthday', [
                'label' => 'Fecha de Nacimiento',
                'format' => 'dd-MM-yyyy',
				'empty_value' => array('year' => 'Año', 'month' => 'Mes', 'day' => 'Día'),	
                'widget' => 'choice',
                'years' => $years,
                'data' => new \DateTime('01-01-'.date("Y",strtotime("-86 year")))
            ]);

        $builder->get('code')
            ->addModelTransformer(new CallbackTransformer(
                function ($code) {
                    $code = strtolower(preg_replace('/[^0-9kK-]/', '', $code));

                    return $code;
                },
                function ($code) {
                    return preg_replace('/[^0-9kK-]/', '', $code);
                }
            ));

        return $builder;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'base_person';
    }
}
