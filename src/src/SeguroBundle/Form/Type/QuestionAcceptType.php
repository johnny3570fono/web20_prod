<?php
namespace SeguroBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulario Insurance
 */
class QuestionAcceptType extends QuestionType
{
    /**
     * {@inheritdoc}
     */
    public function __construct(array $config = [])
    {
        parent::__construct([
                'question' => 'He leído y acepto la autorización especial de salud',
                'type' => 'checkbox'
            ]);
    }
}
