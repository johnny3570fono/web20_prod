<?php
namespace SeguroBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Doctrine\ORM\EntityManager;

/**
 * Formulario Insurance
 */
class InsuranceCatastroficoType extends InsuranceCargasType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder = parent::buildForm($builder, $options);

        $builder->add('questions', new QuestionOncologicoType(), [
                'by_reference' => false
            ]);
        $builder->add('price','hidden');
    }
}
