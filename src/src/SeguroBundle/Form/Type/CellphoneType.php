<?php
namespace SeguroBundle\Form\Type;

use AppBundle\Form\AbstractTelephoneType;

/**
 * Input Teléfono celular
 */
class CellphoneType extends AbstractTelephoneType
{
    /**
     * {@inheritdoc}
     */
    public function getCodes()
    {
        return [
            '09' => '09', '07' => '07',
            '06' => '06', '08' => '08',
            '02' => '02', '32' => '32',
            '33' => '33', '34' => '34',
            '35' => '35', '41' => '41',
            '42' => '42', '43' => '43',
			'44' => '44',
            '45' => '45', '51' => '51',
            '52' => '52', '53' => '53',
            '55' => '55', '57' => '57',
            '58' => '58', '61' => '61',
            '63' => '63', '64' => '64',
            '65' => '65', '67' => '67',
            '71' => '71', '72' => '72',
            '73' => '73', '75' => '75',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'cellphone';
    }
}
