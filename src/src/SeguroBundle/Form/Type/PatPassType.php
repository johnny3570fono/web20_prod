<?php

namespace SeguroBundle\Form\Type;



use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;


/**
 * Formulario Insured
 */
class PatPassType extends AbstractType
{
   
    private $manager;
	
	private $entityManager;


    public function __construct( $manager)
    {
		$this->yearMin = date('Y');
        $this->yearMax = date('Y') + 30;
		$this->entityManager = $this->manager;
    }

    public function getManager()
    {
		
        return $this->manager;
    }

    public function setManager($manager)
    {
        $this->manager = $manager;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {	
		$years = range($this->yearMin, $this->yearMax);
		 
		$builder->add('cardNumber', 'text')
		->add('cardExpirationDate', 'date', [
                'label' => 'Fecha de Vencimiento',
                'format' => 'dd-MM-yyyy',
				'years'           => range(date('Y'), date('Y')+12),
                'days'            => array(1),
                'widget' => 'choice'
            ]);
		$builder->add('paymentDay',  'choice', [
                'label' => 'Día de cargo',
                'choices' => array('0'=>'Día de Cargo', '6'=> '6', '13'=>'13', '20'=> '20')
            ]);
        return $builder = parent::buildForm($builder, $options);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SeguroBundle\Entity\PublicPatPass',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_patpass';
    }


}
