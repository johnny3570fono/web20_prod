<?php
namespace SeguroBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulario Insurance
 */
class QuestionHeightType extends QuestionType
{
    /**
     * {@inheritdoc}
     */
    public function __construct(array $config = [])
    {
        parent::__construct(['question' => 'estatura', 'type' => 'number']);
    }
}
