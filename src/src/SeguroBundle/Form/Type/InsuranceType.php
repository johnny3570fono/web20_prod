<?php
namespace SeguroBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use SeguroBundle\Form\EventListener\AddClientFieldSubscriber;
use SeguroBundle\Form\EventListener\AddPlanFieldSubscriber;
use SeguroBundle\Form\EventListener\AddOtherFormsFieldSubscriber;
use SeguroBundle\Form\EventListener\AddPatPassFieldSubscriber;

use Doctrine\ORM\EntityManager;

/**
 * Formulario Insurance
 */
class InsuranceType extends AbstractType
{
    private $manager;

    public function __construct(EntityManager $manager)
    {
        $this->manager = $manager;
    }

    public function getManager()
    {
        return $this->manager;
    }

    public function setManager($manager)
    {
        $this->manager = $manager;
        return $this;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return $builder;
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
          ->add('price','hidden')
          ->add('pri', 'hidden');
       $builder
          ->add('traveldate','text');

        $builder->addEventSubscriber(new AddClientFieldSubscriber($this->getManager()));
        $builder->addEventSubscriber(new AddPlanFieldSubscriber());
        $builder->addEventSubscriber(new AddOtherFormsFieldSubscriber($this->getManager()));
		$builder->addEventSubscriber(new AddPatPassFieldSubscriber($this->getManager()));
        return $builder;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SeguroBundle\Entity\PublicInsurance',
            'product' => null
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'insurance';
    }
}
