<?php

namespace SeguroBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Setters de planes
 */
class AddStateFieldSubscriber implements EventSubscriberInterface
{
    /**
     * Eventos a subscribir
     *
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(FormEvents::PRE_SET_DATA => 'preSetData');
    }

    /**
     * Setter
     *
     * @param FormEvent $event
     */
    public function preSetData(FormEvent $event)
    {
        $insurance = $event->getData();
        $form = $event->getForm();

        if ($insurance
                && $insurance->getProduct()
                && count($insurance->getProduct()->getPlans()) > 0) {
            $plans = $insurance->getProduct()->getPlans();
            $choices = [];

            foreach ($plans as $plan) {
                $choices[$plan->getId()] = $plan->getName();
            }

            $form->add('plan', 'entity', [
                'class' => 'AppBundle:Plan',
                'choices' => $insurance->getProduct()->getPlans(),
            ]);
        }
    }
}
