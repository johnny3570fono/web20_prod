<?php

namespace SeguroBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Doctrine\ORM\EntityManager;
use SeguroBundle\Helper\Utils;

/**
 * Setters de planes
 */
class AddPatPassFieldSubscriber implements EventSubscriberInterface
{
	
	private $manager;

    public function __construct(EntityManager $manager)
    {
        $this->manager = $manager;
    }

    public function getManager()
    {
        return $this->manager;
    }

    public function setManager($manager)
    {
        $this->manager = $manager;
        return $this;
    }
	
	
    /**
     * Eventos a subscribir
     *
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(FormEvents::PRE_SET_DATA => 'preSetData', FormEvents::POST_SUBMIT => 'postSetData');
    }

    /**
     * Setter
     *
     * @param FormEvent $event
     */
    public function preSetData(FormEvent $event)
    {
		
        $insurance = $event->getData();
		if( $insurance->getProduct() ){
			if( $insurance->getProduct()->getPatPass() == 1 ) {
				$form = $event->getForm();
				
				$form->add('patpass', new \SeguroBundle\Form\Type\PatPassType( $this->getManager()));
			}
		}
    }
	
	public function postSetData(FormEvent $event){
		$insurance = $event->getData();
		
		if( $insurance->getProduct() ){
			if( $insurance->getProduct()->getPatPass() == 1 ) {
				$cardnumber = $insurance->getPatPass()->getCardNumber();
				$cardnumber = Utils::clearCardNumber($cardnumber);
				
				$insurance->getPatPass()->setCardNumber($cardnumber);
			}
		}
	}
}
