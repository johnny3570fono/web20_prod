<?php

namespace SeguroBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Setters de planes
 */
class AddBirthdayFieldSubscriber implements EventSubscriberInterface
{
    /**
     * Eventos a subscribir
     *
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(FormEvents::POST_SET_DATA => 'postSetData');
    }

    /**
     * Setter
     *
     * @param FormEvent $event
     */
    public function postSetData(FormEvent $event)
    {
        $person = $event->getData();
        $form = $event->getForm();

        if ($person && $person->getInsurance()) {
            $yearMin = date('Y') - $person->getInsurance()->getAgeMax();
            $yearMax = date('Y') - $person->getInsurance()->getAgeMin();

            $form->add('birthday', 'birthday', [
                'label' => 'Fecha de Nacimiento',
                'format' => 'dd-MM-yyyy',
                'widget' => 'choice',
                'years' => range($yearMin, $yearMax)
            ]);
        }
    }
}
