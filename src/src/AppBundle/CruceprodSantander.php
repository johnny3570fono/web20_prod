<?php

namespace AppBundle;

use AppBundle\AppSoapClient;

/**
 * Webservice SOAP
 */
class CruceprodSantander extends AppSoapClient
{
    public function getCruceprod($rut,$segmento)
    {


        $rut = strtoupper(str_replace(".", "", $rut)) ;
        $rut = str_replace("-", "", $rut);
        $rut = str_pad($rut, 11, "0", STR_PAD_LEFT);


        $req = ['INPUT' => ['ENTIDAD' => '0035', 'NUMEROPERSONA' => '', 'TIPODOCUMENTO' => 'R', 'RUTCLIENTE' => $rut, 'CANALACONSULTAR' => '0003', 'CRUCEACONSULTAR' => '0045', 'ESTADORELACION' => 'A']];


        $matriz = $this->JSON_HS_PNJPE337($req);


        $matriz = $matriz->OUTPUT->MATRICES->MATRIZCAPTACIONES;

        $resp = array();
        if($matriz){
            try {
                if (!is_array($matriz)) {
                    $matriz = [$matriz];
                }
                foreach ($matriz as $prod) {
                    if ($prod->GLOSAESTADO == 'VIGENTE' || $prod->GLOSASITUACION=='ACTIVA'){
                        if(($prod->PRODUCTO == '80' || $prod->PRODUCTO == '00' || $prod->PRODUCTO == '17' || $prod->PRODUCTO == '77') ){
                            if ($prod->PRODUCTO == '80'){
                                $resp[] = (object)['name' => $prod->GLOSALARGA, 'cardNumber' => $prod->NUMEROPAN, 'id' => $prod->SUBPRODUCTO, 'idprod' => $prod->PRODUCTO];
                            }
                            else{
                                $resp[] = (object)['name' => $prod->GLOSALARGA, 'cardNumber' => $prod->NUMEROCONTRATO, 'id' => $prod->SUBPRODUCTO, 'idprod' => $prod->PRODUCTO];
                            }
                        }
                    }
                }
            } catch(Exception $e) {
                $resp = [];
            }
        }

        return $resp;

    }


    public function getProductEscalar($rut){
        $rut = strtoupper(str_replace(".", "", $rut)) ;
        $rut = str_replace("-", "", $rut);
        $rut = str_pad($rut, 11, "0", STR_PAD_LEFT);
        $req = ['INPUT' => ['ENTIDAD' => '0035', 'NUMEROPERSONA' => '', 'TIPODOCUMENTO' => 'R', 'RUTCLIENTE' => $rut, 'CANALACONSULTAR' => '0003', 'CRUCEACONSULTAR' => '0045', 'ESTADORELACION' => 'A']];
        $matriz = $this->JSON_HS_PNJPE337($req);
        $response = array();
        if(isset($matriz->OUTPUT) && isset($matriz->OUTPUT->ESCALARES)) {
            $response = $matriz->OUTPUT->ESCALARES;
        }
        return $response;
    }

}
