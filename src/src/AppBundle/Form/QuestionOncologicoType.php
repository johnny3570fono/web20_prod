<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use AppBundle\Form\EventListener\AddPlanFieldSubscriber;
use AppBundle\Form\EventListener\AddOtherFormsFieldSubscriber;

use Doctrine\ORM\EntityManager;

/**
 * Formulario Insurance
 */
class QuestionOncologicoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('weight', new QuestionWeightType());
        $builder->add('height', new QuestionHeightType());
        $builder->add('disease', new QuestionDiseaseType(['answer'=>'no']));
        $builder->add('accept', new QuestionAcceptType());

        return $builder;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'questions';
    }
}
