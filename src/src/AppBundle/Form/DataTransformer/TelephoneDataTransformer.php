<?php

namespace AppBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

class TelephoneDataTransformer implements DataTransformerInterface
{
    public function reverseTransform($value)
    {
        return sprintf('%s%s', $value['code'], $value['telephone']);
    }

    public function transform($value)
    {
        $telephone = substr($value, 2);
        $code = substr($value, 0, 2);
        return [
            'telephone' => $telephone,
            'code' => $code,
        ];
    }
}
