<?php

namespace AppBundle\Form;

use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulario Payer
 */
class PayerType extends BasePersonType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
        $builder = parent::buildForm($builder, $options);

        $builder->remove('name');
        $builder->remove('lastname1');
        $builder->remove('lastname2');
        $builder->remove('code');

        $builder->add('address', 'text', [
                'label' => 'Dirección'
            ])
            ->add('addressState', 'entity', [
                'mapped' => false,
                'class' => 'AppBundle:Community',
                'property' => 'name',
                'label' => 'Comuna',
            ])
            ->add('addressCity', 'text', ['label' => 'Ciudad'])
            ->add('addressPhone', new TelephoneType($this->codesTelephone()), [
                'label' => 'Teléfono',
            ])
            ->add('addressCellphone', new CellphoneType(), ['label' => 'Celular'])
            ->add('addressEmail', 'email', ['label' => 'E-mail']);

        return $builder;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Payer'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_payer';
    }
}
