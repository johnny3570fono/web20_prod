<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use AppBundle\Entity\BaseProduct;

/**
 * Formulario Prodducto
 */
class ProductType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('family', 'entity', [
                    'class' => 'AppBundle\Entity\Family',
                    'placeholder' => ''
            ])
            ->add('type', 'choice', [
                'choices' => BaseProduct::getTypes(),
            ])
            ->add('name')
            ->add('price')
            ->add('segments', 'entity', [
                'class' => 'AppBundle:Segment',
                'choice_label' => 'name',
                'expanded' => true,
                'multiple' => true,
                'label' => 'Segments'
            ])
            ->add('redirectUrl')
            ->add('assists', 'entity', [
                'class' => 'AppBundle:Assist',
                'choice_label' => 'name',
                'expanded' => true,
                'multiple' => true,
                'label' => 'Assists'
            ])
            ->add('imageTitle', 'text')
            ->add('imageSubtitle', 'text')
            ->add('imageFile', 'file')
            ->add('title', 'text')
            ->add('marketingText', 'ckeditor', $this->getConfigEditor())
            ->add('description', 'ckeditor', $this->getConfigEditor())
            ->add('coverage', 'ckeditor', $this->getConfigEditor())
            ->add('coverageNot', 'ckeditor', $this->getConfigEditor())
            ->add('howto', 'ckeditor', $this->getConfigEditor())
            ->add('pdfFile', 'file')
            ->add('textLegal', 'ckeditor', $this->getConfigEditor())
            ->add('ageMin')
            ->add('ageMax');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Product'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_product';
    }

    /**
     * Configuracion de ckeditor
     * @return array
     */
    protected function getConfigEditor()
    {
        return [
                'config' => [
                    'toolbar' => [
                        [
                            'name'  => 'document',
                            'items' => [
                                'Source',
                                '-',
                                'Save',
                                'NewPage',
                                'DocProps',
                                'Preview',
                                'Print',
                                '-',
                                'Templates'
                            ],
                        ],
                        '/',
                        [
                            'name'  => 'basicstyles',
                            'items' => [
                                    'Bold', 'Italic', 'Underline',
                                    'Strike', 'Subscript', 'Superscript',
                                    '-', 'RemoveFormat',
                                    '-', 'NumberedList', 'BulletedList'
                                ],
                        ],
                    ],
                    'uiColor' => '#ffffff',
                ],
            ];
    }
}
