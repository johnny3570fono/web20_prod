<?php

namespace AppBundle\Form;

use Symfony\Component\OptionsResolver\OptionsResolver;

use AppBundle\Entity\Carga;

/**
 * Formulario Carga
 */
class ChildType extends CargaType
{
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
        $builder = parent::buildForm($builder, $options);

        $builder->get('code')->setRequired(false);

        $builder->add('relationship', 'hidden', [
                'data' => Carga::REALTIONSHIP_SPOUSE
            ]);

        return $builder;
    }
}
