<?php

namespace AppBundle\Form;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *	Input Hiden Relacion
 */
class RelationshipType extends BasePersonType
{
	public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
    	$builder = parent::buildForm($builder, $options);

        $builder->add('relationship', 'hidden', [
            'required' => true,
            'data' => $this->getRelationships()[0],
            'constraints' => array(
                /*new Assert\NotBlank(),
                new Assert\NotNull(),*/
                new Assert\Choice(array(
                    'choices' => $this->getRelationships()
                    )
                )
            )
        ]);
        return $builder;
    }

    /**
     * @return array
     */
    public function getRelationships()
    {
        return [];
    }

}
