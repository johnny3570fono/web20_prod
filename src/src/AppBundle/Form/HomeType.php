<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityManager;

use AppBundle\Form\EventListener\AddCommunityFieldsSubscriber;

/**
 * Formulario Home
 */
class HomeType extends AbstractType
{
    private $manager;
    private $communities;

    public function __construct(EntityManager $manager)
    {
        $this->manager = $manager;

        $dql = 'SELECT c.descripcion_comuna_homologo FROM AppBundle:Community c ORDER BY c.descripcion_comuna_homologo ASC';

        $r = $manager->createQuery($dql)->getResult();

        foreach ($r as $community) {
            $this->communities[$community['descripcion_comuna_homologo']] = $community['descripcion_comuna_homologo'];
        }
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @return null
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('address', 'text', [
                'label' => 'Dirección',
            ])
            ->add('addressState', 'choice', [
                'choices' => $this->communities,
                'placeholder' => 'Seleccione una comuna',
                'label' => 'Comuna',
            ])
            ->add('addressCity', 'hidden', [
                'label' => 'Ciudad',
            ]);

        $builder->addEventSubscriber(new AddCommunityFieldsSubscriber($this->manager));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Home',
            'communities' => []
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_home';
    }
}
