<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\CallbackTransformer;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Customer;


/**
 * Formulario Payment
 */
class PaymentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @return null
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $newcards = [];

        $cards = $options['cards'];

        foreach($cards as $card){
         
            $needle = "cuenta";
            $cuenta = strripos($card['name'], $needle);
            if($cuenta===false){
              $cardultimos4 = substr($card['cardNumber'], -4, 4);
              $newcards[$card['cardNumber']] =  '****'.$cardultimos4.' ('.$card['name'].')';

            }else{
                 $newcards[$card['cardNumber']] =  $card['cardNumber']. '('.$card['name'].')';
            }
        }
        $diascargo = $options['diacargo'];
      
        foreach ($diascargo as $dia) {

           if($dia!=0){
            $diacargo[$dia]=$dia;

           }
        }
        
        $builder
            ->add('paymentMethod', 'choice', [
                'label' => 'Medio de Pago',
                'choices' => $newcards
            ])
            ->add('paymentDay', 'choice', [
                'label' => 'Día de cargo',
                'choices' =>$diacargo,
            ])
            ->add('coverageAndExclusions', 'checkbox', [
                'label' => 'Acepto las Coberturas y Exclusiones que se me han informado',
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Insurance',
            'cards' => null,
            'diacargo'=> null
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_payment';
    }
}
