<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use AppBundle\Form\EventListener\AddInsuredFieldSubscriber;
use AppBundle\Form\EventListener\AddPlanFieldSubscriber;
use AppBundle\Form\EventListener\AddOtherFormsFieldSubscriber;

use Doctrine\ORM\EntityManager;

/**
 * Formulario Insurance
 */
class InsuranceType extends AbstractType
{
    private $manager;

    public function __construct(EntityManager $manager)
    {
        $this->manager = $manager;
    }

    public function getManager()
    {
        return $this->manager;
    }

    public function setManager($manager)
    {
        $this->manager = $manager;
        return $this;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return $builder;
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('payerEqualsInsured', 'hidden', [
                'mapped' => false,
                'data' => 1
            ])
          ->add('price','hidden')
          ->add('pri', 'hidden');
       

        $builder->addEventSubscriber(new AddInsuredFieldSubscriber($this->getManager()));
        $builder->addEventSubscriber(new AddPlanFieldSubscriber());
        $builder->addEventSubscriber(new AddOtherFormsFieldSubscriber($this->getManager()));

        return $builder;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Insurance',
            'product' => null
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'insurance';
    }
}
