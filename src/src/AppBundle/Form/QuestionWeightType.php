<?php
namespace AppBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulario Insurance
 */
class QuestionWeightType extends QuestionType
{
    /**
     * {@inheritdoc}
     */
    public function __construct(array $config = [])
    {
        parent::__construct(['question' => 'Peso', 'type' => 'number']);
    }
}
