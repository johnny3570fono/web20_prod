<?php

namespace AppBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Setter de beneficiaries
 */
class AddRelationshipFieldSubscriber implements EventSubscriberInterface
{
    /**
     * Eventos a subscribir
     *
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(FormEvents::PRE_SET_DATA => 'preSetData');
    }

    /**
     * Setter
     *
     * @param FormEvent $event
     */
    public function preSetData(FormEvent $event)
    {
        $insurance = $event->getData();
        $form = $event->getForm();

        if ($insurance && $insurance->getProduct()) {
            if ($insurance->getProduct()->isLife()
                || $insurance->getProduct()->isHealth() ) {
            }
        }
    }
}
