<?php
namespace AppBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Doctrine\ORM\EntityManager;

/**
 * AddFormFieldSubscriber
 *
 * @author Marcos Matamala <marcos@taisachile.cl>
 */
class AddInsuredFieldSubscriber implements EventSubscriberInterface
{
    private $manager;

    public function __construct(EntityManager $manager)
    {
        $this->manager = $manager;
    }

    public function getManager()
    {
        return $this->manager;
    }

    public function setManager($manager)
    {
        $this->manager = $manager;
        return $this;
    }

    /**
     * Eventos a subscribir
     *
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(FormEvents::PRE_SET_DATA => 'preSetData');
    }

    /**
     * Setter
     *
     * @param FormEvent $event
     */
    public function preSetData(FormEvent $event)
    {
        $insurance = $event->getData();
        $form = $event->getForm();

        $states = $this->getManager()->getRepository('AppBundle:Community')
            ->findAllAsArrayOfName();
            
        $codStates = $this->getManager()->getRepository('AppBundle:Community')
            ->findAllAsArrayOfCod();

        $yearMin = date('Y') - $insurance->getAgeMax();
        $yearMax = date('Y') - $insurance->getAgeMin();

        $form->add('insured', new \AppBundle\Form\InsuredType($states, $codStates, $yearMin, $yearMax, $this->getManager()) );
    }
}
