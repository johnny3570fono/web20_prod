<?php

namespace AppBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Community;

/**
 * Setters de planes
 */
class AddCommunityFieldsSubscriber implements EventSubscriberInterface
{


    private $manager;

    public function __construct(EntityManager $manager)
    {
        $this->manager = $manager;
    }


    public function getManager()
    {
        return $this->manager;
    }

    public function setManager($manager)
    {
        $this->manager = $manager;
        return $this;
    }
    /**
     * Eventos a subscribir
     *
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(FormEvents::POST_SUBMIT => 'postSetData');
    }

    /**
     * Setter
     *
     * @param FormEvent $event
     */
    public function postSetData(FormEvent $event)
    {
        $community = $event->getData();

        if($community->getAddressState() != null && trim($community->getAddressState()) != ''){

            $em = $this->getManager();
            $dql = 'SELECT c FROM AppBundle:Community c WHERE c.descripcion_comuna_homologo = :descripcion_comuna_h';

            $exists = $em->createQuery($dql)
                ->setParameter('descripcion_comuna_h', $community->getAddressState())
                ->getResult();

            if (isset($exists[0]) && $exists[0] instanceof Community) {
                $entity = $exists[0];
                $community->setCodState( $entity->getCodigoComunaHomologo() );
                //$this->setAddressCity($entity->getDescripcionComunaHomologo());
                $community->setCodCity( $entity->getCodigoCuidadHomologo() );
            }
        }
    }
}
