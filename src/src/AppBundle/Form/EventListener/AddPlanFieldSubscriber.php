<?php

namespace AppBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Setters de planes
 */
class AddPlanFieldSubscriber implements EventSubscriberInterface
{
    /**
     * Eventos a subscribir
     *
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(FormEvents::PRE_SET_DATA => 'preSetData');
    }

    /**
     * Setter
     *
     * @param FormEvent $event
     */
    public function preSetData(FormEvent $event)
    {
        $insurance = $event->getData();
        $form = $event->getForm();

        $plans = $insurance->getProduct()->getPlans();

        if ($insurance
                && $insurance->getProduct()
                && count($plans) > 0) {

            $form->add('plan', 'entity', [
                'data' => $plans->first(),
                'class' => 'AppBundle:Plan',
                'choices' => $plans,
            ]);
        }
    }
}
