<?php

namespace AppBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Setters de planes
 */
class AddCargasFieldSubscriber implements EventSubscriberInterface
{
    /**
     * Eventos a subscribir
     *
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(FormEvents::PRE_SET_DATA => 'preSetData');
    }

    /**
     * Setter
     *
     * @param FormEvent $event
     */
    public function preSetData(FormEvent $event)
    {
        $insurance = $event->getData();
        $form = $event->getForm();

        $yearMin = date('Y') - $insurance->getAgeMax();
        $yearMax = date('Y') - $insurance->getAgeMin();

        $form->add('cargas', 'collection', array(
            'type' => new \AppBundle\Form\CargaType($yearMin, $yearMax),
            'allow_add' => true,
            'constraints' => [
                new \AppBundle\Validator\Constraints\UniqueBeneficiaries(),
                new \AppBundle\Validator\Constraints\InsuredNotBeneficiary(),
                new \AppBundle\Validator\Constraints\UniqueSpouse(),
            ],
            'by_reference' => false,
        ));
    }
}
