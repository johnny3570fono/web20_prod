<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use AppBundle\Form\EventListener\AddPlanFieldSubscriber;
use AppBundle\Form\EventListener\AddOtherFormsFieldSubscriber;

use Doctrine\ORM\EntityManager;

/**
 * Formulario Insurance
 */
class InsuranceTarjetaType extends InsuranceType
{
    private $cards;

    public function __construct($entityManager, $cards)
    {
        parent::__construct($entityManager);

        $this->cards = $cards;
    }
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder = parent::buildForm($builder, $options);

        $builder->add('credit_cards', new CreditCardType($this->cards), [
                'mapped' => false,
            ]);
        $builder->add('price','hidden');

        return $builder;
    }
}
