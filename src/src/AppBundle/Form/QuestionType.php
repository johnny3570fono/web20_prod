<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Validator\Constraints\EqualTo;

/**
 * Formulario Insurance
 */
class QuestionType extends AbstractType
{
    private $config;

    public function __construct(array $config = [])
    {
        $this->config = array_merge(
            [
                    'question' => 'question',
                    'type' => 'text',
                    'answer' => null
                ],
            $config
        );
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('question', 'hidden', [
                'data' => $this->config['question'],
                'constraints' => [
                        new EqualTo(['value' => $this->config['question']])
                    ]
            ]);

        $builder->add('answer', $this->config['type'], [
                'data' => $this->config['answer']
            ]);

        return $builder;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                'data_class' => 'AppBundle\Entity\InsuranceQuestion',
                'by_reference' => false,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'questions';
    }
}
