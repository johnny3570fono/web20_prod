<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use AppBundle\Form\DataTransformer\TelephoneDataTransformer;

/**
 * Input Teléfono
 */
abstract class AbstractTelephoneType extends AbstractType
{
    /**
     * Códigos de area para teléfonos fijos o celular
     *
     * @return array Códigos
     */
    abstract public function getCodes();

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('code', 'choice', [
            'choices' => $this->getCodes(),
            'placeholder' => ''
        ])
        ->add('telephone', 'text', [
                'constraints' => [
                        new  \Symfony\Component\Validator\Constraints\Regex('/^[0-9]*$/')
                    ]
            ])
        ->addViewTransformer(new TelephoneDataTransformer());
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'label'        => false,
            'compound'     => true,
            'inherit_data' => false,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'text';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'telephone';
    }
}
