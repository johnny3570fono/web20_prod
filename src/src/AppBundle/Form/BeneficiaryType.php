<?php

namespace AppBundle\Form;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;


use AppBundle\Form\RelationshipType;
use AppBundle\Entity\PersonRelationship;

/**
 * Formulario Beneficiary
 */
class BeneficiaryType extends RelationshipType
{    
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
        $builder = parent::buildForm($builder, $options);

        $builder->add('capital', 'number', [
            'label' => 'Capital %',
            'constraints' => [
                    new Assert\GreaterThan(array('value' => 0))
            ]
        ]);

        return $builder;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Beneficiary',
            'birthday' => [
                    'yearMin' => date('Y') - 100,
                    'yearMax' => date('Y') ,
                ]
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'beneficiary';
    }

    /**
     * @return array
    **/
    public function getRelationships(){
        return [
            PersonRelationship::REALTIONSHIP_CHILD,
            PersonRelationship::REALTIONSHIP_SPOUSE,
            PersonRelationship::REALTIONSHIP_FATHER,
            PersonRelationship::REALTIONSHIP_MOTHER,
            PersonRelationship::REALTIONSHIP_SOBRINO,
            PersonRelationship::REALTIONSHIP_HERMANO,
            PersonRelationship::REALTIONSHIP_NIETO,
            PersonRelationship::REALTIONSHIP_CONVIVIENTE,
            PersonRelationship::REALTIONSHIP_OTHER,
            PersonRelationship::REALTIONSHIP_PAREJA,
        ];
    }
}
