<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Formulario Travel
 */
class TravelType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @return null
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('start', 'date', [
                'label' => 'Fecha Viaje',
                'months' => range(date('m'), 12),
                'days' => range(date('d'), 31),
                'years' => range(date('Y'), date('Y')+1),
                'format' => 'dd-MM-yyyy',
            ])
            ->add('end', 'date', [
                'label' => 'Fin Cobertura',
                'months' => range(date('m'), 12),
                'days' => range(date('d'), 31),
                'years' => range(date('Y'), date('Y')+1),
                'format' => 'dd-MM-yyyy',
            ]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_travel';
    }
}
