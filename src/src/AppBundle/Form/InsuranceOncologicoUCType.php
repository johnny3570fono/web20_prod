<?php

namespace AppBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;

/**
 * Formulario Insurance
 */
class InsuranceOncologicoUCType extends InsuranceCargasType {

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder = parent::buildForm($builder, $options);

        $builder->add('questions', new QuestionOncologicoType(), [
            'by_reference' => false
        ]);
        $builder->add('price', 'hidden');
    }

}
