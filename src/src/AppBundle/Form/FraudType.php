<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Formulario Fraud
 */
class FraudType extends InsuranceType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @return null
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder = parent::buildForm($builder, $options);
        $builder->add('price','hidden');

       $builder->add('credit_card', 'hidden');
    }
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_fraud';
    }
}
