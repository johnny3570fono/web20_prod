<?php

namespace AppBundle\Form;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

use AppBundle\Entity\Carga;
use AppBundle\Entity\PersonRelationship;
use AppBundle\Form\RelationshipType;
/**
 * Formulario Carga
 */
class CargaType extends RelationshipType
{
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
        $builder = parent::buildForm($builder, $options);

        $builder->get('code')->setRequired(false);

        return $builder;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Carga',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'carga';
    }

    /**
     * @return array
    **/
    public function getRelationships(){
        return [
            PersonRelationship::REALTIONSHIP_CHILD,
            PersonRelationship::REALTIONSHIP_SPOUSE,
            PersonRelationship::REALTIONSHIP_OTHER,
            PersonRelationship::REALTIONSHIP_CONVIVIENTE,
            PersonRelationship::REALTIONSHIP_PAREJA,
            PersonRelationship::REALTIONSHIP_NIETO,
            PersonRelationship::REALTIONSHIP_FATHER,
            PersonRelationship::REALTIONSHIP_MOTHER,
            PersonRelationship::REALTIONSHIP_SOBRINO,
            PersonRelationship::REALTIONSHIP_HERMANO
        ];
    }
}
