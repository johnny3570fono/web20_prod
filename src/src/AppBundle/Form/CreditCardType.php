<?php

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\CallbackTransformer;
use Doctrine\ORM\EntityManager;

/**
 * Formulario CreditCard
 */
class CreditCardType extends AbstractType
{
    private $cards;

    public function __construct($cards)
    {
        $this->cards = $cards;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @return null
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('cardNumber', 'hidden');

        $builder->get('cardNumber')->addModelTransformer(
            new CallbackTransformer(
                function ($original) {
                    return array_search($original, $this->cards);
                },
                function ($selected) {
                    return $this->cards[$selected]["cardNumber"];
                }
            )
        );

        return $builder;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\CreditCard'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_credit_card';
    }
}
