<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use AppBundle\Form\EventListener\AddPlanFieldSubscriber;
use AppBundle\Form\EventListener\AddOtherFormsFieldSubscriber;

use Doctrine\ORM\EntityManager;

/**
 * Formulario Insurance
 */
class InsuranceHomeType extends InsuranceType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder = parent::buildForm($builder, $options);

        $builder->add('home', new \AppBundle\Form\HomeType($this->getManager()), [
                'by_reference' => false,
            ]);
        $builder->add('price','hidden');

    }
}
