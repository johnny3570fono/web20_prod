<?php

namespace AppBundle\Form;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use AppBundle\Form\DataTransformer\CiudadComunaDataTransformer;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Form\EventListener\AddCommunityFieldsSubscriber;
use Doctrine\ORM\EntityManager;
/**
 * Formulario Insured
 */
class InsuredType extends BasePersonType
{
    private $states;
    private $codStates;
    private $manager;


    public function __construct($states, $codStates, $yearMin = null, $yearMax = null, $manager)
    {
        parent::__construct($yearMin, $yearMax);

        $this->states = $states;
        $this->codStates = $codStates;
        $this->manager = $manager;
    }

    public function getManager()
    {
        return $this->manager;
    }

    public function setManager($manager)
    {
        $this->manager = $manager;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
        $builder->add('address', 'text', [
                'label' => 'Dirección'
            ])
            ->add('addressState', 'choice', [
                'label' => 'Comuna',
                'choices' => $this->states
            ])
            ->add('addressCity', 'hidden', ['label' => 'Ciudad'])

            ->add('addressCellphone', new CellphoneType(), [
                'label' => 'Telefono',
                'pattern' => '\d{6}-\d{4}',
                'constraints' => [
                        new \AppBundle\Validator\Constraints\IsLength()
                ]
            ])
            ->add('addressEmail', 'email', [
                'label' => 'E-mail',
                'constraints' => [
                        new  \Symfony\Component\Validator\Constraints\Email()
                    ]
            ])
            ->add('isPayer', 'hidden', [
                    'mapped' => false
                ]);

        $builder->addEventSubscriber(new AddCommunityFieldsSubscriber($this->getManager()));

        return parent::buildForm($builder, $options);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Insured',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_insured';
    }
}
