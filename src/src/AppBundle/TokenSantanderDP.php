<?php

namespace AppBundle;

use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Bridge\Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;

/**
 * Webservice SOAP
 */
class TokenSantanderDP{
    
	
	/**
     * Valida Token
     *
     * @param string $key
     * @return null|string
     */
    public function ValidateTokenOB($key){

       /**
		Datos en duro para pruebas en Desarrollo. Son los parametros que se piden para 
		llamar al servicio https://viphbusi1-dpext-e.cl.bsch:9159/ValidateToken
		***/		
		$json = '{
			"idSession":"",
			"token":"'.$key.'",
			"canal":"003",
			"app":"SEGWEB2"
		}';
			
		//Curl: Crea un nuevo recurso CURL
       	$curl = curl_init();	  
		curl_setopt_array($curl, array(
			#CURLOPT_URL => "https://apiper.santander-homo.cl/appper/login",
			CURLOPT_URL => "https://vippbusi1-dpext-e.cl.bsch:9159/ValidateToken",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_CAINFO=>  "/opt/www/zurich_portal_backend/src/app/config/dp/cervalidate.cer", 
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => $json,
			CURLOPT_HTTPHEADER => array(
				"cache-control: no-cache",
				"content-type: application/json"
			),
		));
       
		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if($err){
		  //Error Curl
		   echo "ValidateTokenOB - CURL Error #:" . $err;
		   $logger->info('<br/>ValidateTokenOB - CURL Error #: <br/>'. $err);
		}else{
			
			//Toma la cadena JSON y la convierte en una variable de PHP.
			$json_result = json_decode($response, true);
			
			foreach (array($json_result) as $key=>$detalle_result) {
			   if (isset($detalle_result["ValidateTokenResponse"]["InfoType"]["codigo"])){
					$codigo=$detalle_result["ValidateTokenResponse"]["InfoType"]["codigo"];
			   }else{
				   $codigo=false;
				   break;
			   }
			}
						
			return $codigo;
		
		}
    }

   public function getGeneraDPToken(){
	   //Creamos un nuevo Logger
	   $logger = new Logger('logger'); 
        
        /******RotatingFileHandler: Logs rotativos, un fichero por día.****************************
        **Documentación: https://github.com/Seldaek/monolog/blob/07b6c1c4171a030787f2bc961272b1887032304e/src/Monolog/Handler/RotatingFileHandler.php 
        **Parametros: int $ maxFiles La cantidad máxima de archivos para mantener (0 significa ilimitado) ****************************************************
        ********************************************************/
      
        //$logger->pushHandler(new StreamHandler(__DIR__.'/../../app/logs/log_soap.log', Logger::DEBUG)); 
        $logger->pushHandler(new RotatingFileHandler(__DIR__.'/../../app/logs/log_datapower.log', 5, Logger::INFO));
        $logger->pushHandler(new FirePHPHandler());		
		/**
		Datos en duro para pruebas en Desarrollo. Son los parametros que se piden para 
		llamar al servicio "https://apiper.santander-homo.cl/appper/login" 
		***/		
		//00215215210 072142667 00031848385
		$json = ' {
			"RUTCLIENTE":"00215215210",
			"PASSWORD":"1357",
			"APP":"SEGWEB2",
			"CANAL":"003"
		}';
		
		//Curl: Crea un nuevo recurso CURL
       	$curl = curl_init();	  
		curl_setopt_array($curl, array(
			//CURLOPT_URL => "https://apiper.santander-homo.cl/appper/login",
			CURLOPT_URL => "https://apiper.santander.cl/appper/login",
			CURLOPT_RETURNTRANSFER => true,
			//CURLOPT_ENCODING => "", 
			CURLOPT_MAXREDIRS => 10, 
			//CURLOPT_TIMEOUT => 30, 
			CURLOPT_CAINFO=>  "/opt/www/zurich_portal_backend/src/app/config/dp/certoken.cer", 
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => $json,
			CURLOPT_HTTPHEADER => array(
				"cache-control: no-cache",
				"content-type: application/json"
			),
		));
       
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);

		// dump($response);
		// dump($err);
		// die;

		if($err){
		  //Error Curl
		  echo "cURL Error #:" . $err;
		  $logger->info('<br/>getGeneraDPToken - CURL Error #: <br/>'. $err);
		}else{
			
			//Toma la cadena JSON y la convierte en una variable de PHP.
			$json_result = json_decode($response, true);
		    
			foreach (array($json_result) as $detalle_result) {	
			
				if (isset($detalle_result["METADATA"]["KEY"])){
					$key=$detalle_result["METADATA"]["KEY"];
					$logger->info('<br/>Token Generado - CURL#: <br/>'. $key);										
				}else{
					$metadata=$detalle_result["METADATA"];
					$logger->info('<br/>Token NO Generado - CURL#: <br/>'. $metadata);
				}
			} 
			
			return $key;
		
		}  
	}

}