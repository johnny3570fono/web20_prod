<?php

namespace AppBundle\Twig;

use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernel;

/**
 * Extension para crear rutas con token.
 */
class TokenExtension extends \Twig_Extension
{
    private $request;
    private $router;

    /**
     * Contructor
     *
     * @param Router $router
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * Captura evento
     *
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if ($event->getRequestType() === HttpKernel::MASTER_REQUEST) {
            $this->request = $event->getRequest();
        }
    }

    /**
     * Funciones y/o Metodos a exponer en twig
     * @return array
     */
    public function getFunctions()
    {
        return array(
            'path_token' => new \Twig_Function_Method($this, 'getPath')
        );
    }

    /**
     * Entrega una url con los parametros de token y segmento
     *
     * @param type $name
     * @param type $parameters
     * @return string
     */
    public function getPath($name, $parameters = array())
    {
        $parameters = array_merge($parameters, [
            'token' => $this->request->get('token'),
        ]);

        return $this->router->generate($name, $parameters, false);
    }

    /**
     * Getter
     *
     * @return string
     */
    public function getName()
    {
        return 'token_extension';
    }
}
