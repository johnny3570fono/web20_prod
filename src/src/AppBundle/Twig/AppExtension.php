<?php

namespace AppBundle\Twig;

/**
 * {@inheritDoc}
 */
class AppExtension extends \Twig_Extension
{
    /**
     * {@inheritDoc}
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('monthly', array($this, 'monthlyprice')),
        );
    }

    /**
     * Dado un precio y una uf se entrega su multipicación en formato.
     *
     * @param decimal $price
     * @param decimal $uf
     * @return string
     */
    public function monthlyprice($price, $uf = 0)
    {
        $priceUp = (int)round($uf * $price);

        while(($priceUp%10) != 0) {
            $priceUp++;
        }

        return '$' . number_format($priceUp, 0, ',', '.');
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'app_extension';
    }
}
