<?php

namespace AppBundle;

use Vich\UploaderBundle\Naming\NamerInterface;
use Vich\UploaderBundle\Mapping\PropertyMapping;

/**
 * Description of FileNaming
 *
 * @author Marcos Matamala <marcos.matamala@taisachile.cl>
 */
class FileNaming implements NamerInterface
{
    /**
     * {@inheritDoc}
     */
    public function name($object, PropertyMapping $mapping)
    {
        $ext = '';

        if (preg_match('/product|banners/', $mapping->getMappingName())) {
            $ext = $object->getImageFile()->guessExtension();
        }

        if (preg_match('/poliza_seguro/', $mapping->getMappingName())) {
            $ext = $object->getPdfFile()->guessExtension();
        }

        if (preg_match('/segment_assist_image|segment_family_image/', $mapping->getMappingName())) {
            $ext = $object->getIconFile()->guessExtension();
        }

        return sprintf('%s.%s', uniqid(), $ext);
    }
}
