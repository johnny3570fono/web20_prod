<?php

namespace AppBundle;

/**
 * Client SOAP
 */
class AppSoapClientLGU extends \SoapClient
{
    private $key;
    private $cert;
    private $endpoint;

    public function __construct($wsdl, array $options = [])
    {

        $this->key = $options['private_key'];
             
        $this->cert = $options['cert_file'];

        if($options['endpoint']!=''){
           $this->endpoint = $options['endpoint'];  
        }
        
        parent::__construct($wsdl, $options);
    }

    public function __doRequest($request, $location, $action, $version, $one_way = NULL)
    {
        $doc = new \DOMDocument('1.0');
        $doc->loadXML($request);
       if(isset($this->endpoint)){
          $location=$this->endpoint;
     
       
          }

        $objWSSE = new \Wse\WSSESoap($doc);

        /* add Timestamp with no expiration timestamp */
        $objWSSE->addTimestamp();

        /* create new XMLSec Key using AES256_CBC and type is private key */
        $objKey = new \Wse\XMLSecurityKey(\Wse\XMLSecurityKey::RSA_SHA1, array('type' => 'private'));

        /* llave privada, si el primer parametro es un nombre de archivo o la key como string, si el primer parametro es un certificado o no  */
        $objKey->loadKey($this->key, true, false);

        /* Sign the message - also signs appropiate WS-Security items */
        $options = array("insertBefore" => FALSE);
        $objWSSE->signSoapDoc($objKey, $options);

        /* Add certificate (BinarySecurityToken) to the message */
        $token = $objWSSE->addBinaryToken(file_get_contents($this->cert));

        /* Attach pointer to Signature */
        $objWSSE->attachTokentoSig($token);

        $objKey = new \Wse\XMLSecurityKey(\Wse\XMLSecurityKey::AES128_CBC);
        $objKey->generateSessionKey();
     
      

        $siteKey = new \Wse\XMLSecurityKey(\Wse\XMLSecurityKey::RSA_OAEP_MGF1P, array('type'=>'public'));
        //$siteKey->loadKey($this->scert, TRUE, TRUE);

        $options = array("KeyInfo" => array("X509SubjectKeyIdentifier" => true));

       // $objWSSE->encryptSoapDoc($siteKey, $objKey, $options);
        
        $retVal = parent::__doRequest($objWSSE->saveXML(), $location, $action, $version);

        $doc = new \DOMDocument('1.0');
        $doc->loadXML($retVal);

        $options = array("keys" => array("private" => array("key" => $this->key, "isFile" => TRUE, "isCert" => FALSE)));
       // $objWSSE->decryptSoapDoc($doc, $options);

        return $doc->saveXML();
    }
}
