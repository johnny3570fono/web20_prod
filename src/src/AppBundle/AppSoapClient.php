<?php

namespace AppBundle;
//Gabriela 21 Nov
use Symfony\Bridge\Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler; 
//Fin 21 Nov
/**
 * Client SOAP
 */
class AppSoapClient extends \SoapClient
{
    private $key;
    private $scert;
    private $cert;
    private $endpoint;

    public function __construct($wsdl, array $options = [])
    {
        $this->key = $options['private_key'];
       

        $this->scert = $options['service_cert'];
     
        $this->cert = $options['cert_file'];
        if($options['endpoint']!=''){
           $this->endpoint = $options['endpoint'];  
        }
        
        
        parent::__construct($wsdl, $options);
    }

    public function __doRequest($request, $location, $action, $version, $one_way = NULL)
    {
        $doc = new \DOMDocument('1.0');
        $doc->loadXML($request);
        if(isset($this->endpoint)){
          $location=$this->endpoint;
     
       
          }

        //Creamos un nuevo Logger
        $logger = new Logger('logger');
        //Lo guardamos en la siguiente ruta
        $logger->pushHandler(new StreamHandler(__DIR__.'/../../app/logs/log_soap.log', Logger::DEBUG));
        $logger->pushHandler(new FirePHPHandler());
        //Fin Log
	
	//Guardamos el Request
        htmlentities($doc->saveXML());

        $logger->info('Request: <br/>'. html_entity_decode($doc->saveXML()));
        $objWSSE = new \Wse\WSSESoap($doc);

        /* add Timestamp with no expiration timestamp */
        $objWSSE->addTimestamp();

        /* create new XMLSec Key using AES256_CBC and type is private key */
        $objKey = new \Wse\XMLSecurityKey(\Wse\XMLSecurityKey::RSA_SHA1, array('type' => 'private'));

        /* llave privada, si el primer parametro es un nombre de archivo o la key como string, si el primer parametro es un certificado o no  */
        $objKey->loadKey($this->key, true, false);

        /* Sign the message - also signs appropiate WS-Security items */
        $options = array("insertBefore" => FALSE);
        $objWSSE->signSoapDoc($objKey, $options);

        /* Add certificate (BinarySecurityToken) to the message */
        $token = $objWSSE->addBinaryToken(file_get_contents($this->cert));

        /* Attach pointer to Signature */
        $objWSSE->attachTokentoSig($token);

        $objKey = new \Wse\XMLSecurityKey(\Wse\XMLSecurityKey::AES128_CBC);
        $objKey->generateSessionKey();
        
      

        $siteKey = new \Wse\XMLSecurityKey(\Wse\XMLSecurityKey::RSA_OAEP_MGF1P, array('type'=>'public'));
        $siteKey->loadKey($this->scert, TRUE, TRUE);

        $options = array("KeyInfo" => array("X509SubjectKeyIdentifier" => true));

        $objWSSE->encryptSoapDoc($siteKey, $objKey, $options);
        
            
        $retVal = parent::__doRequest($objWSSE->saveXML(), $location, $action, $version);

        $doc = new \DOMDocument('1.0');
        $doc->loadXML($retVal);

        $options = array("keys" => array("private" => array("key" => $this->key, "isFile" => TRUE, "isCert" => FALSE)));
        $objWSSE->decryptSoapDoc($doc, $options);

        //Guardamos el Response
        htmlentities($doc->saveXML());
        $logger->info('<br/> Response: <br/>'. html_entity_decode($doc->saveXML()));

        return $doc->saveXML();
    }
}
