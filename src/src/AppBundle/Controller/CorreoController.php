<?php

namespace AppBundle\Controller;

use AppBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

/**
 * Rut Controller
 */
class CorreoController extends BaseController
{
    /**
     * @Route("/", name="rut_form")
     * @Method({"GET"})
     *
     * @return Response
     */
    public function indexAction($correo)
    {
        
        $parametros=$this->getParameter('mail_option');
        $to=$correo;
      
        $this->get('mail_helper')->correoPrueba($parametros,$to);
        echo 'Se envia el correo a:'.$correo .' verifique, si no lo encuentra, revise en su bandeja de spam';
        die();
    }
}
