<?php

namespace AppBundle\Controller;

use AppBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Doctrine\Common\Collections\Criteria;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * Controlador de familias. Muestra el catalogo de productos.
 */
class FamilyController extends BaseController
{
    /**
     * @Route("/", name="homepage_family")
     * @Method({"GET"})
     *
     * @return Response
     */
    public function indexAction()
    {
        $logger = $this->get('logger');
        $logger->info(date('Y-m-d H:i:s').' - Cargando Familia Controller');
      
        $segment = $this->getUser()->getSegment();
        $logger->info(date('Y-m-d H:i:s').' - Trae segmento');

        $repo = $this->getRepository('AppBundle:SegmentFamily');

        $logger->info(date('Y-m-d H:i:s').' - Crea consulta principal');
        $query = $repo->createQueryBuilder("sf")
            ->join('sf.family', 'f')
            ->join('sf.segment', 's')
            ->where('sf.segment = :segment')
            ->andWhere('f.parent IS NULL')
            ->setParameter('segment', $segment)
            ->getQuery();

        $logger->info(date('Y-m-d H:i:s').' - trae data inicio');
        $segmentFamilies = $query->getResult();
        $logger->info(date('Y-m-d H:i:s').' - trae data fin');
        $data = new \Doctrine\Common\Collections\ArrayCollection();
        foreach ($segmentFamilies as $sf) {
            $repo = $this->getRepository('AppBundle:Family');
            $children = $repo->children($sf->getFamily());
            foreach ($children as $child) {
                $products = $child->getProducts();
                foreach ($products as $p) {
                 
                    $sf->getFamily()->getProducts()->add($p);
                }
            }

            if (count($sf->getFamily()->getProducts()) > 0) {
                $data->add($sf);
            }
        }

        $logger->info(date('Y-m-d H:i:s').' - ordena data');

        $sort = Criteria::create()->orderBy(['orderBy' => Criteria::ASC]);

        $logger->info(date('Y-m-d H:i:s').' - enviando front');

        return $this->render('::family/index.html.twig', [
            'families' => $data->matching($sort),
        ]);
       
    }
}
