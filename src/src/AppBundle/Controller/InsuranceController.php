<?php

namespace AppBundle\Controller;

use AppBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Form\InsuranceType;
use AppBundle\Form\InsuranceHomeType;
use AppBundle\Form\InsuranceFraudType;
use AppBundle\Form\InsuranceTravelType;
use AppBundle\Form\PaymentType;
use AppBundle\Entity\Insurance;
use AppBundle\Entity\CreditCard;
use AppBundle\Entity\Home;
use AppBundle\Entity\Product;
use AppBundle\Entity\BasePerson;
use AppBundle\Entity\Insured;
use AppBundle\Entity\Community;
use AppBundle\Entity\Beneficiary;
use AppBundle\Entity\Carga;
use AppBundle\Entity\PersonRelationship;
use AppBundle\Entity\Proposal;
use AppBundle\Validator\Constraints as Assert;


/**
 * Controller de Seguros
 */
class InsuranceController extends BaseController {

    protected $baseTemplate;

    public function __construct() {
        $this->baseTemplate = '::insurance/details.html.twig';
    }

    /**
     * @return string
     */
    public function getTemplate() {
        return $this->baseTemplate;
    }

    /**
     * @param string
     * @return null
     */
    public function setTemplate($newTemplateName) {
        return $this->baseTemplate = '::insurance/' . $newTemplateName;
    }

    /**
     * Funcion utilitaria para crear una instancia de un Seguro
     *
     * @return Insurance
     */
    protected function createInsurance(\AppBundle\Entity\Product $product) {
        $segment = $this->getUser()->getSegment();

        $segmentFamily = $this->getSegmentFamily($segment, $product->getFamily());

        return Insurance::createFrom($this->getUser(), $product, $segment, $segmentFamily);
    }

    /**
     * @Route("/fraud/{id}", requirements={"id" = "\d+"}, name="fraud_insurance")
     * @ParamConverter("product", class="AppBundle:Product", options={"repository_method" = "findOneFraud"})
     * @Method({"GET", "POST"})
     *
     * @param Product $product
     * @param Request $request
     * @return Response
     */
    public function fraudAction(Product $product, Request $request) {
        $session = $request->getSession();
        $session->set('idpayment', '');
        $session->set('idresume', '');
        $error = 0;
		
        //ASESOR
        /* te indica que viene del planificador si viene algo en la session */
        $origen = $session->get('origen');
        if (is_null($origen)) {
            $origen = 0;
        }

        $idplan = $session->get('idplan');
		
        if (!isset($idplan)) {
            $idplan = 0;
        }
        /**/


        //formatea edad para validaciones
        $setEdad = $product->getAgeMax() + 1;
        $product->setAgeMax($setEdad);

        if (!$product->inSegment($this->getUser()->getSegment())) {
            throw new \Exception('Segmento erroneo');
        }

        $codStates = $this->getManager()->getRepository('AppBundle:Community')
                ->findAllAsArrayOfCod();
        $insurance = $this->createInsurance($product);


        $cards = [];
        foreach ($this->getUser()->getCreditCards() as $c) {
            if ($c->idprod == 80 && trim($c->cardNumber) > '') {
                $cards[$c->id] = [
                    "cardNumber" => $c->cardNumber,
                    "name" => $c->name,
                    "id" => $c->id
                ];
            }
        }

        if (class_exists($product->getFormType())) {
            $className = $product->getFormType();
            $formType = new $className($this->getManager(), $cards);
        } else {
            $formType = new InsuranceType($this->getManager());
        }
       
		try {
            $em =  $this->getDoctrine()->getManager();
            if(method_exists($em, 'getRepository')) {
                $campaigns =  $em->getRepository('AppBundle:Campaign')->findByProductToArray($product);
            } else {
                $campaigns = false;
            }
        } catch(\Exception $e) {
            $campaigns = false;
        }

        $form = $this->createForm($formType, $insurance);
        $form->handleRequest($request);


        if ($form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();

            if ($form->offsetExists('credit_card')) {
                $credit_cards = $form->get('credit_card')->getData();

                $credicard = '';
                foreach ($this->getUser()->getCreditCards() as $c) {
                    if ($c->cardNumber == $credit_cards) {
                        $credicard = new CreditCard();
                        $credicard->setCardNumber($c->cardNumber);
                        $credicard->setName($c->name);
                    }
                }
                if ($credicard != '') {

                    $credicard->setInsurance($insurance);
                    $insurance->setCreditCard($credicard);
                    $em->persist($credicard);
                } else {
                    $error = 1;
                }
            }



            /*    $credit = $form->getData()->getPaymentMethod();
              $ok=0;
              foreach ($cards as $key => $r) {
              if($r['cardNumber'] == $credit)
              $ok = $credit;
              }
              if($ok == 0)
              $error=1;
              } */
            $plform = $form->getData()->getPlan()->getId(); // form
            $idplan = $session->get('idplan');

			$cod_producto= $insurance->getProduct()->getId(); 
			
            $plpro = $product->getPlans(); // valore del producto
            foreach ($plpro as $key => $r) {
                if ($r->getId() == $plform) {
                    $finPri = $r->getPrice();
                }
            }
            if ($finPri != $form->getData()->getPrice() || $error == 1) {
                $form->getData()->setPrice($finPri);
                $result = false;
            } else {
                $result = true;
            }

            if ($form->isValid() && $error != 1) {

                if ($form->get('payerEqualsInsured')->getData() == 1) {
                    $insurance->insuredPassDataToPayer($form->get('insured')->getData());
                }

                //corrección para edad
                $setEdad = $product->getAgeMax() - 1;
                $product->setAgeMax($setEdad);
                $insurance->setProduct($product);
                $insurance->setAgeMax($setEdad);
                // Fin corrección
                
                $insurance->setSuscripcion($product->getSuscripcion());
                $em->persist($insurance);
                $em->flush();

                $session = $request->getSession();
                $session->set('idpayment', $insurance->getId());
                $session->set('idresume', '');
		
                //            return $this->redirect($this->generateUrl('payment_insurance', ['id' => $insurance->getId()]), 301);
       
					return $this->redirect($this->generateUrl('payment_insurance').'?p='.$cod_producto.'', 301);
				//return $this->redirect($this->generateUrl('payment_insurance'), 301);
			   // return $this->redirect($this->generateUrl('payment_insurance').'?=choice?=281', 301);
            }
        }

        if ($product->getFormTemplate()) {
            $this->setTemplate($product->getFormTemplate());
        }

        $s = new \AppBundle\Form\FormErrorsSerializer();

        return $this->render($this->getTemplate(), [
                    'form' => $form->createView(),
                    'insurance' => $insurance,
                    'cards' => $cards,
                    'product' => $product,
                    'errors' => $s->serializeFormErrors($form),
                    'cover' => $this->cover($product),
                    'georly' => $codStates,
                    'idplan' => $idplan,
                    'origen' => $origen,
					'campaigns' => $campaigns
        ]);
    }

    /**
     * @Route("/details/{id}", requirements={"id" = "\d+"}, name="details_insurance")
     * @ParamConverter("product", class="AppBundle:Product")
     * @Method({"GET", "POST"})
     *
     * @param Product $product
     * @param Request $request
     * @return Response
     */
    public function detailsAction(Product $product, Request $request) {
		
//		echo $product->getFormTemplate();
//        die;
        $session = $request->getSession();
        $session->set('idpayment', '');
        $session->set('idresume', '');
        //ASESOR
        /* te indica que viene del planificador si viene algo en la session */
        $origen = $session->get('origen');
        if (is_null($origen)) {
            $origen = 0;
        }
        
        /* para saber de que plan viene del asesor de seguros */
        $idplan = $session->get('idplan');
        if (!isset($idplan)) {
            $idplan = 0;
        }
        /* FIN ASESOR */

        //formatea edad para validaciones
        $setEdad = $product->getAgeMax() + 1;
        $product->setAgeMax($setEdad);

        if (!$product->inSegment($this->getUser()->getSegment())) {
            throw new \Exception('Segmento erroneo');
        }

        $codStates = $this->getManager()->getRepository('AppBundle:Community')
                ->findAllAsArrayOfCod();

        $logger = $this->get('logger');
        $logger->info("\n\n\n");
        $logger->info(date('Y-m-d H:i:s u') . ' - Cargando Detalle');

        $em = $this->getDoctrine()->getManager();

        
        $educational = false;
        $insurance = $this->createInsurance($product);

         /** CAMPAIGN  */
        try {
            $campaigns =  $em->getRepository('AppBundle:Campaign')->findByProductToArray($product);
        } catch(\Exception $e) {
            $campaigns = false;
        }
        /** END CAMPAIGN */
        
        if (class_exists($product->getFormType())) {
            $className = $product->getFormType();
            $formType = new $className($this->getManager());
        } else {
            $formType = new InsuranceType($this->getManager());
        }

        $logger->info(date('Y-m-d H:i:s u') . ' - Creando formulario');

        $form = $this->createForm($formType, $insurance);
        $form->handleRequest($request);

        $errors = [];
        $arraycursos = [];

        $arrayrentas = [];
        if (($product->getFormTemplate()) == 'life_education.html.twig') {

            $educational = true;
            $form_educational = new Educational();
            $cursos = $this->container->get('sp.cursos')->getCursos();
            foreach ($cursos as $key => $curso) {
                $arraycursos[$key] = $curso;
            }
        }
        //echo $form->getErrorsAsString();

        if ($form->isSubmitted()) {

            foreach ($form->getErrors(true) as $error) {
                $errors[] = $error->getMessage();
            }

            $temp = $product->getFormTemplate();
            $result = $this->validacion($temp, $form, $product, $codStates);

            if ($form->isValid() && $result == true) {
            
                if ($form->get('payerEqualsInsured')->getData() == 1) {
                    $insurance->insuredPassDataToPayer($form->get('insured')->getData());
                }
                if ($educational == true) {

                    $form_educational->setPrima($form->getData()->getPrice());
                    $id_curso = $form->get('renta')->getData();
                    $renta_query = $em->getRepository('AppBundle:Renta')->findOneBy(array('id' => $id_curso));
                    $form_educational->setRenta($renta_query);
                    $form_educational->setCurso($form->get('curso')->getData());
                    $form_educational->setInsuranceId($insurance->getId());



                    $em->persist($form_educational);
                }
                if ((($product->getFormTemplate()) == 'vacaciones_largas_fam.html.twig')) {
                    $fechaact = new \DateTime;
                    $insurance->setTravelDate($fechaact->format('d-m-Y'));
                }

                //corrección para edad
                $setEdad = $product->getAgeMax() - 1;
                $product->setAgeMax($setEdad);
                $insurance->setProduct($product);
                $insurance->setAgeMax($setEdad);
                // Fin corrección
              
                $insurance->setSuscripcion($product->getSuscripcion());
                $em->persist($insurance);
                $em->flush();
                if ($educational == true) {
                    $form_educational->setInsuranceId($insurance->getId());
                    $em->persist($form_educational);
                    $em->flush();
                }

                $session = $request->getSession();

                $session->set('idpayment', $insurance->getId());
                $session->set('campaign', $request->get('campaign'));

                $session->set('idresume', '');
                $cod_producto= $insurance->getProduct()->getId(); 
               
                //            return $this->redirect($this->generateUrl('payment_insurance', ['id' => $insurance->getId()]), 301);
				return $this->redirect($this->generateUrl('payment_insurance').'?p='.$cod_producto.'', 301);

				//return $this->redirect($this->generateUrl('payment_insurance'), 301);
            }
        }

        $template = '::insurance/details.html.twig';

        if ($product->getFormTemplate()) {
            $template = '::insurance/' . $product->getFormTemplate();
        }
      
       
        

   
        $relationships = [];
        if ($form->offsetExists('beneficiaries')) {
            $relationships = [
                ['value' => PersonRelationship::REALTIONSHIP_CHILD, 'maxapparition' => -1],
                ['value' => PersonRelationship::REALTIONSHIP_SPOUSE, 'maxapparition' => 1],
                ['value' => PersonRelationship::REALTIONSHIP_FATHER, 'maxapparition' => 1],
                ['value' => PersonRelationship::REALTIONSHIP_MOTHER, 'maxapparition' => 1],
                ['value' => PersonRelationship::REALTIONSHIP_OTHER, 'maxapparition' => -1]
            ];

            /* if(trim($product->getCode()) == '294' ){
              $relationships[] = ['value' =>PersonRelationship::REALTIONSHIP_SOBRINO,'maxapparition' => 1];
              $relationships[] = ['value' =>PersonRelationship::REALTIONSHIP_HERMANO,'maxapparition' => 1];
              $relationships[] = ['value' =>PersonRelationship::REALTIONSHIP_NIETO,'maxapparition' => 1];
              $relationships[] = ['value' =>PersonRelationship::REALTIONSHIP_CONVIVIENTE,'maxapparition' => 1];
              } */
        }
		$parentescos = $em->getRepository('AppBundle:Parentesco')->findBy(array("producto" => $product->getId()));
		
        if ($form->offsetExists('cargas')) {
            
            /*If s�lo para producto Oncológico UC*/
            if ($product->getCalculoMayor() == 1) {                

                $parentescos = $em->getRepository('AppBundle:Parentesco')->findBy(array("producto" => $product->getId()));
                
                if ($parentescos) {
                    foreach ($parentescos as $value) {
                        $relationships[]  =  ['value' => $value->getParentesco(), 'maxapparition' => 1, 'edadMinima' => $value->getEdadMinima(), 'edadMaxima' => $value->getEdadMaxima()];
                    }
                }
                
            } else {
                $relationships = [
                    ['value' => PersonRelationship::REALTIONSHIP_CHILD, 'maxapparition' => -1],
                    ['value' => PersonRelationship::REALTIONSHIP_SPOUSE, 'maxapparition' => 1]
                ];

                if (trim($product->getCode()) != '258' && trim($product->getCode()) != '276') {
                    $relationships[] = ['value' => PersonRelationship::REALTIONSHIP_OTHER, 'maxapparition' => -1];
                }
            }
        }
        $parentescos = $em->getRepository('AppBundle:Parentesco')->findBy(array("producto" => $product->getId()));        
        if ($parentescos) {
            $relationships = array();            
            foreach ($parentescos as $value) {
                $relationships[] = ['value' => $value->getParentesco(), 'maxapparition' => $value->getMaxApparition(), 'edadMinima' => $value->getEdadMinima(), 'edadMaxima' => $value->getEdadMaxima()];
            }
        }

        if ($product->getFormTemplate()) {
            $this->setTemplate($product->getFormTemplate());
        }

        $s = new \AppBundle\Form\FormErrorsSerializer();
        $logger->info(date('Y-m-d H:i:s u') . ' - enviando data front');
        
        return $this->render($this->getTemplate(), [
                'form' => $form->createView(),
                'insurance' => $insurance,
                'product' => $product,
                'relationships' => $relationships,
                'errors' => $s->serializeFormErrors($form),
                'formErrors' => $errors,
                'arraycursos' => $arraycursos,
                'cover' => $this->cover($product),
                'arrayrentas' => $this->rentas(),
                'georly' => $codStates,
                'idplan' => $idplan,
                'origen' => $origen,
                'campaigns' => $campaigns
        ]);
    }

    /**
     * @Route("/payment/choice", name="payment_insurance")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function paymentAction(Request $request) {
        $error = 0;
        $session = $request->getSession();
//        die($session->get('idpayment'));
        $em = $this->getDoctrine()->getManager();
        $campaign = $request;
    

        $insurance = $em->getRepository('AppBundle:Insurance')->findOneDraft(array('id' => $session->get('idpayment')));

        $logger = $this->get('logger');
        $logger->info("\n\n\n");
        $logger->info(date('Y-m-d H:i:s u') . ' - Cargando Forma de pago');
        $cards = [];
        foreach ($this->getUser()->getCreditCards() as $c) {


            $cards[$c->id] = [
                "cardNumber" => $c->cardNumber,
                "name" => $c->name,
                "id" => $c->id
            ];
        }


//       $resultado = $this->container->get('sp.dia_cargo')->getDiaCargo();

        $resultado = array(
            'dia1' => '6',
            'dia2' => '13',
            'dia3' => '20',
        );

        $form = $this->createForm(new PaymentType(), $insurance, array('cards' => $cards, 'diacargo' => $resultado));


        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            
            $fechalast = $em->getRepository('AppBundle:Insurance')->findlast($session->get('rut'));
            
          
            //validacion dia de pago
            $diaPago = $form->getData()->getPaymentDay();
            if ($diaPago == 6 || $diaPago == 13 || $diaPago == 20) {
                $error = 0;
            } else {
                $error = 1;
            }
            $credit = $form->getData()->getPaymentMethod();
            $ok = 0;
            foreach ($cards as $key => $r) {
                if ($r['cardNumber'] == $credit)
                    $ok = $credit;
            }
            if ($ok == 0)
                $error = 1;

            if ($fechalast > '' || $error == 1) {
                $tiempolimite = $em->getRepository('AppBundle:Config')->findOneById(2)->getValue();

                if ((time() - strtotime($fechalast['updated']->format('Y-m-d H:i:s'))) < ($tiempolimite * 1)) {
                    return $this->redirect($this->generateUrl('error_insurance'), 301);
                }
            }

            if ($form->isValid() && $error != 1) {
               
                /** TODO: REALIZAR INVOCACIÓN A API VENTA */
                $concentradorAPI = $this->container->get('concentrador_api');
                $response = $concentradorAPI->creaSolicitudVenta($insurance);
                $proposal = null;
        
                try {
                    if ($response != null) {
                        $proposal = new Proposal();
                        $proposal->setInsurance($insurance);
                        $proposal->setFecha(\DateTime::createFromFormat("d/m/Y H:i:s", $response["FECHA"]));
                        $proposal->setPrima($response["PRIMACALCULADA"]);
                        $proposal->setEstado($response["ESTADOSOLICITUD"]);
                        $proposal->setPoliza($response["NUMEROPOLIZA"]);
                        $proposal->setCotizacion($response["NUMEROCOTIZACION"]);
                        $proposal->setSolicitud($response["NUMEROSOLICITUD"]);
        
                        $insurance->setStatus(Insurance::STATUS_SEND);
        
                        $em->persist($insurance);
                        $em->persist($proposal);
                    } else {
                        $insurance->setStatus(Insurance::STATUS_ISSUED);
                    }
        
                    $em->flush();
                } catch (Exception $e) {
                    $em = $this->getDoctrine()->getManager();
                    $insurance->setStatus(Insurance::STATUS_ISSUED);
                    $em->flush();
                    $proposal = null;
                }

                $uf = $this->getRepository('AppBundle:Uf')
                        ->findLast();
                $parametros = $this->getParameter('mail_option');

                if ($proposal == null || $concentradorAPI->mustSendEmail($insurance->getProduct())) {
                    $this->get('mail_helper')->correoNotificacion($insurance, $uf, $parametros, $proposal);
                }

                $session = $request->getSession();
                $session->set('idresume', $insurance->getId());

                // return $this->redirect($this->generateUrl('resume_insurance', ['id' => $insurance->getId()]), 301);
                $cod_producto= $insurance->getProduct()->getId(); 	
             
                 /** CAMPAIGN  */
                 try {
                    if($campaign = $session->get('campaign')) {
                        $rut = $session->get('rut');
                        $id =  $campaign['id'];
                        $answer =  $campaign['answer'];
                        
                        $campaign = $em->getRepository('AppBundle:Campaign')->findOneBy(['id' => $id]);
                        $entity = $em->getClassMetadata("AppBundle:CampaignRegister");
                     
                        $register = $entity->newInstance();
                        $register->setCampaign($campaign);
                        $register->setInsurance($insurance);
                        $register->setRut($rut);
                        $register->setTitle($campaign->getTitle());
                        $register->setDescription(strip_tags($campaign->getDescription()));
                        $register->setProduct($insurance->getProduct()->getName());
                        $register->setPlan($insurance->getPlan()['name']);
                        $register->setAnswer($answer);
                       
                        $em->persist($register);
                        $em->flush();
                    }
                }  catch (\Exception $e) { 
                    $logger->info(date('Y-m-d H:i:s u') . ' - Problema en guardar campaña');
                }
                /**  */
                

				return $this->redirect($this->generateUrl('resume_insurance').'?p='.$cod_producto.'', 301);
				//return $this->redirect($this->generateUrl('resume_insurance'), 301);
				
            }
        }
        $logger->info(date('Y-m-d H:i:s u') . ' - enviando data a front');

        return $this->render('::insurance/payment.html.twig', [
                    'insurance' => $insurance,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/resume/voucher", name="resume_insurance")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function resumeAction(Request $request) {
        $session = $request->getSession();
        $em = $this->getDoctrine()->getManager();
        $insurance = $em->getRepository('AppBundle:Insurance')->findOneBy(array('id' => $session->get('idresume')));
        $proposal = $em->getRepository('AppBundle:Proposal')->findOneBy(array('insurance' => $insurance->getId()));

        $planificador = '';
        $idprod = '';
        $idplan = '';
        $fraud = true;
        $continuar = true;
        /* guardar de donde proviene si es de planificador viene con un 1 */
        $origen = $session->get('origen');
        // echo 'origen'.$origen;
        // die;

        /* variable que indica cuales seguros le interesa contratar al cliente y el selecciono */
        $contrataciones = $session->get('contrataciones');

        $posicionarray = $session->get('posicionarray');
        /* contador del arreglo para la posicion del los seleccionados */
        /* te indica el id de plan en la posicion[0] */
        $idplan = $session->get('idplan');
        /**/
        /* guarda el arreglo de los productos contratados */

        if (isset($insurance)) {
            if ($session->get('contratados')) {
                $contratadoanterior = $session->get('contratados');
                $contratadoanterior[] = $insurance->getPlan()['id'];
                $contratadounicos = array_unique($contratadoanterior);
                $session->set('contratados', $contratadounicos);
            } else {
                $contradado[] = $insurance->getPlan()['id'];
                $session->set('contratados', $contradado);
            }
        }
        /**/
        // $preo = new front_Preocupaciones();
        //$form = $this->createForm(new ResultadosType(), $preo);
        //$form->handleRequest($request); 
        /* para evitar que aparezca el boton continuar si es el último seguro que viene el ciclo del planificador */
        if ($posicionarray == count($contrataciones)) {
            $continuar = false;
        }
        /* cuando presiona continuar SOLO PARA PALNIFICADOR */
        if ($request->isMethod('POST')) {
            if ($request->request->has('volver')) {
                $session->set('contratados', null);
                return $this->redirect($this->generateUrl('front_resultados'));
            }
             if ($request->request->has('continuarOtro')) {
                if ($posicionarray < count($contrataciones)) {
                    $idplan = $contrataciones[$posicionarray + 1];
                    $posicionarray++;
                    $session->set('posicionarray', $posicionarray);
                    $session->set('idplan', $idplan['id']);
                    $plan = $em->getRepository('AppBundle:Plan')->findOneBy(array('id' => $idplan));
                    $idprod = $plan->getProduct()->getId();
                    if ($plan->getProduct()->getFormType() == '\AppBundle\Form\FraudType') {
                        return $this->redirect('/web20/customers/info/fraud/' . $idprod);
                    } else {
                        return $this->redirect('/web20/customers/info/details/' . $idprod);
                    }
                } else {

                    $continuar = false;
                }
            }
        }

        return $this->render('::insurance/resume.html.twig', [
                    'insurance' => $insurance,
                    'continuar' => $continuar,
                    'planificador' => $origen,
                    'idprod' => $idprod,
                    'fraud' => $fraud,
                    'idplan' => $idplan,
                    'proposal' => $proposal
        ]);
        /* return $this->render('::Correo/correoNotificacion.txt.twig', [
          'insurance' => $insurance,
          ]);
         */
    }

    /**
     * @Route("/timeerrorinsurance", name="error_insurance")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function errorAction(Request $request) {
        return $this->render('::status/errorlimite.html.twig');
    }

}
