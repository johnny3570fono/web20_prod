<?php

namespace AppBundle\Controller;

use AppBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Session;



class LguController extends BaseController
{


	
	/**
	 * @Route("/", name="lgu")
	 * @Method({"GET"})
	 *
	 * @return Response
	 */
	public function indexAction(Request $request)
	{
		// $logger = $this->get('logger');
		// $logger->info("\n\n\n");
		// $logger->info(date('Y-m-d H:i:s u').' - Inicio');

		// $headertoken = @$_SERVER['HTTP_TOKENSSO'];
		$headertoken = @$_REQUEST['TokenSSO'];
		/*if(!isset($_REQUEST['TokenSSO'])){
			echo 'No se pudo validar el usuario, por favor intente más tarde';
			die();
		}*/

		$session = $request->getSession();
		$session->invalidate();
		$session = new Session();

		// $logger->info(date('Y-m-d H:i:s u').' - abre sesion');
		$session->set('token', $headertoken);

		// $headertoken = @$_SERVER['TOKENSSO'];

		// $logger->info(date('Y-m-d H:i:s u').' - valida token inicio');
		$token = $this->get('token_session')->IsValido($headertoken);
		// $logger->info(date('Y-m-d H:i:s u').' - valida token fin');
		/*+++++++++++++++++++++++++++++++++++++++++++*/			 

		if($token['codRespuesta']=='0000'){
			$session->set('esvalido', true);

			$validtoken=true;
			$xml = simplexml_load_string($token['key']);
		// !OJO OJO OJO con esto!!!! //
			//$xml->userid ;
			// !OJO OJO OJO con esto!!!! //
			$rut = trim($xml->userID);
			$rut = str_pad($rut, 10, "0", STR_PAD_LEFT);
			$session->set('rut', $rut);
			$coderut = $rut;

			// !OJO OJO OJO con esto!!!! //
			//$xml->segmento = 'N';
			// !OJO OJO OJO con esto!!!! //

		
			if ($xml->Segmento == 'N')
				$Segmento='personas';
			elseif ($xml->Segmento == 'S')
				$Segmento='select';
			else
				$Segmento='banefe';


			$session->set('segmento', $Segmento);
			$themeparam = '';
			// !OJO OJO OJO con esto!!!! //
			//$xml->subcanal ;
			// !OJO OJO OJO con esto!!!! //
			if ($xml->subCanal == 'P'){
				$session->set('themedisplay', 'desktop');
			}
			else{
				$session->set('themedisplay', 'mobile');
				$themeparam = '?theme=mobile';
			}

			$redirprod = '';
			if ($xml->codProd > ''){
				$themeparam = '';
				$redirprod = $this->getUrlPart($xml->codProd, $xml->codllamado);
			}

			$session->set('tiemposesion', time());

		}
		else{
			$session->set('esvalido', false);
			$validtoken=false;
		}


		//	var_dump($coderut);
		// $logger->info(date('Y-m-d H:i:s u').' - prepara redireccion');

		if($validtoken=true){
			if ($redirprod > ''){
				return $this->redirect($redirprod);
				//return $this->redirect('../customers/'.$coderut.'/'.$themeparam);
			} else{
				return $this->redirect('../customers/info/'.$themeparam);
			}
		}
		// $logger->info(date('Y-m-d H:i:s u').' - redireccion fin');

		$session->migrate();

	}

    /**
     * @return Response
     */
    public function getUrlPart($elcode, $elllamado)
    {
    	$elcode = $elcode *1;
        $repo = $this->getRepository('AppBundle:Product');

        $query = $repo->createQueryBuilder("sf")
            ->where('sf.code = :elcode')
            ->setParameter('elcode', $elcode)
            ->getQuery();

        $products = $query->getResult();

        $eltype = '';
        $elId = '';
        foreach ($products as $product) {
        	$eltype = $product->getType();
        	$elId = $product->getId();
        }

        if($elcode != '99999' && $elcode != '99998' && $elcode != '331'){
            if ($elllamado == 'M'){
                $redirprod = '../customers/info/product/'.$elId;
            }
            else{
                if ($eltype != 'fraud')
                    $eltype = 'details';
                $redirprod = '../customers/info/'.$eltype.'/'.$elId;
            }
        } else if($elcode == '331') {
			$redirprod = '../legos/life/main';
		} else if($elcode == '99998') {
			$redirprod = '../customers/aceptacion/formulario';
		} else{

           $redirprod = '../insurance';
        }

	    return $redirprod;
    }
    
}
