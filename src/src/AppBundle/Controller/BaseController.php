<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use AppBundle\Controller\RestController;
use AppBundle\Entity\Segment;
use AppBundle\Entity\Family;
use AppBundle\Entity\Product as Productos;

/**
 * BaseController
 */
class BaseController extends Controller {

    const DEFAULT_SEGMENT = 'Select';

    /**
     * Getter EntityMananer
     *
     * @return EntityManager
     */
    public function getManager() {
        return $this->getDoctrine()->getManager();
    }

    /**
     * Retorna un instanacia del repository del Entity definido en $className
     *
     * @param string $className
     * @return Doctrine\ORM\EntityRepository
     */
    public function getRepository($className) {
        return $this->getDoctrine()
                        ->getRepository($className);
    }

    /**
     * Valor en pesos de UF del día actual.
     * base de datos
     *
     * @return decimal
     */
    public function getUf() {
        return $this->getRepository('AppBundle:Uf')
                        ->findLast();
    }

    /**
     * Valor en pesos de UF del día actual.
     * base de datos
     *
     * @return decimal
     */
    public function getDesc($product, $uf) {
        return $this->getRepository('AppBundle:Product')
                        ->findDesc($product, $uf);
    }

    public function cover($product) {
        $data = [];
        $plans = $product->getPlans();
        $i = 0;

        foreach ($plans as $plan) {
            $data[$i] = [];
            $targets = $plan->getTargets();

            foreach ($targets as $target) {
                $data[$i][] = $target->toArray();
            }
            $i++;
        }

        return $data;
    }

    /**
     * Determinar por parametro si se muestra una vista mobile o no.
     * Agrega el opjeto UF a la vista.
     *
     * @param type     $view
     * @param array    $parameters
     * @param Response $response
     * @return view
     */
    public function render($view, array $parameters = array(), Response $response = null) {
    
        
        $parameters = array_merge($parameters, [
            'uf' => $this->getUf(),
        ]);

        $parameters = array_merge($parameters, [
            'instantcall' => $this->getInstantCall($parameters),
            'udata' => $this->getUtagData($parameters)
        ]);

        
        $session = $this->get('session');
        
		if ( isset( $_GET["theme"] ) ) {
			$theme = htmlentities(strip_tags( $_GET["theme"] ));
			$session->set( 'themedisplay', $theme );
		}

        $templateShortName = explode('.html.twig', $view)[0];
        $mobileName = $templateShortName . '.mobile.html.twig';
        $parameters = array_merge($parameters, [
            'themedisplay' => $session->get('themedisplay'),
        ]);

        if ($this->get('request')->get('theme', 'desktop') == 'mobile') {
            return parent::render($mobileName, $parameters, $response);
        } else {

            return parent::render($view, $parameters, $response);
        }
    }
    
    /**
     * Verifica si el producto tiene habilitado instant call
     *
     * @param $parameters
     * @return boolean
     */

    public function getInstantCall($parameters=[]) {
        try {
            if(isset($parameters['product'])) {
              
                if(method_exists($parameters['product'], 'getInstantCall')) {
                    if($parameters['product']->getInstantCall() == 'true'  ||  $parameters['product']->getInstantCall()){
                        return true;
                    }
                }
            } 
            
            if(isset($parameters['insurance'])) {
                $productId = $parameters['insurance']->getProduct()->getId();
                if(method_exists($parameters['insurance'], 'getProduct')) {
                    $product =  $this->getRepository('AppBundle:Product')->findOneBy(['id' => $productId]);
                    if($product->getInstantCall() == 'true'  ||  $product->getInstantCall()){
                        return true;
                    }
                }
            }
        } catch(\Exception $e) {
            return false;
        }
        return false;
    }



    public function getProductParameters($parameters=[]) {
        try {
            if(isset($parameters['product'])) {
               
                $productId = $parameters['product']->getId();
                $product =  $this->getRepository('AppBundle:Product')->findOneBy(['id' => $productId]);
                return $product;
            } else if(isset($parameters['insurance'])) {
                if(method_exists($parameters['insurance'], 'getProduct')) {
                    $productId = $parameters['insurance']->getProduct()->getId();
                    $product =  $this->getRepository('AppBundle:Product')->findOneBy(['id' => $productId]);
                    return $product;
                }
            } else {
                return false;
            }
        } catch(\Exception $e) {
            return false;
        }
        return false;
    }

    public function getUtagData($parameters) {
        $product = $this->getProductParameters($parameters);
        $requestName = $this->container->get('request');
        $currentRoute = $requestName->get('_route');
        $utag = [ 
            'es_cliente' => 'si',
            'page_path' => '',
            'page_name' => '',
            'template' => [
                'page_path' => '',
                'page_name' => '',
            ],
        ];
        if($product) {
            $slug = $product->getSlug();
            $type = $product->getType();
            $name = $product->getName();
            $family = $this->getFamilybyTypeEs($type);
            $codFrec = $product->getCodFrecPago();
            $prima = $product->getPrice();
            $frequency = 'mensual';
            if(isset($parameters['insurance'])) {
                $insurance = $parameters['insurance'];
                $codFrec = $insurance->getCodFrecPago();
                $prima  = $insurance->getPrice();
                $pri = $insurance->getPri();
                if(method_exists($product, 'getPlans')) {
                    if($product->getPlans()) {
                        $plan = $product->getPlans()->first();
                        if(method_exists($plan, 'getPrice')) {
                            $prima = $plan->getPrice();

                        }
                    }
                }
            
                if(method_exists($insurance, 'getPlan')){
                    if($plan = $insurance->getPlan()) {
                        if(is_array($plan)) {
                            if(isset($plan['id'])) {
                                $planId = $plan['id'];
                            }
                            if(isset($plan['name'])) {
                                $utag['plan'] = $plan['name'];
                            }
                        }
                        if(is_object($plan)) {
                            if(method_exists($plan,'getId')) {
                                $planId = $plan->getId();
                            }
                            if(method_exists($plan,'getName')) {
                                $utag['plan'] = $plan->getName();
                            }
                        }
                        if($pri && isset($planId)) {
                            $coverages =  $this->getRepository('AppBundle:Coverage')->findBy(['plan' => $planId ]);
                            foreach($coverages as $coverage) {
                                $codplanco = $coverage->getCodplanco();
                                $codes = explode(',', $codplanco);
                                if(is_array($codes)) {
                                    foreach($codes as $cod) {
                                        if(trim($cod) == $pri) {
                                            $utag['dias']  = $coverage->getDays();
                                            $utag['cobertura']  = $coverage->getCapital();
                                        }
                                    }
                                } else {
                                    if($codplanco == $pri) {
                                        $utag['dias']  = $coverage->getDays();
                                        $utag['cobertura']  = $coverage->getCapital();
                                    }
                                }
                            }
                        }
                        if(method_exists($insurance, 'getInsured')){
                            if($insurance->getInsured()->getCode() !=  $insurance->getPayer()->getCode()) {
                                $utag['seguro_titular'] = 'no';
                            } else {
                                $utag['seguro_titular'] = 'si';
                            }
                        }

                    }
                }
                if($currentRoute != 'details_insurance' && $currentRoute != 'fraud_insurance') {
                    $utag['prima_uf'] = (float) $insurance->getPrice();
                    $utag['cargas'] = count($insurance->getCargas());
                }
                $utag['page_name'] = "${name}: Personalización";
                $utag['categoria'] = $family;
            }
            if(!empty($slug)) {
                $slug = strtolower(trim(preg_replace('/[\s-]+/', '-', preg_replace('/[^A-Za-z0-9-]+/', '-', preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $slug))))), '-'));
            }
            if($currentRoute == 'details_insurance') {
                if($type == 'travel') {
                    $utag['page_name'] = "${name}: Selección Plan";
                    $utag['page_path'] = "/transa/productos/seguros/${family}/${slug}/seleccionplan";
                } else {
                    $utag['page_name'] = "${name}: Datos";
                    $utag['page_path'] = "/transa/productos/seguros/${family}/${slug}/datos";
                }
            } else if ($currentRoute == 'fraud_insurance') {
                $utag['page_name'] = "${name}: Datos";
                $utag['page_path'] = "/transa/productos/seguros/${family}/${slug}/datos";
            }  else if ($currentRoute == 'payment_insurance') {
                $utag['page_name'] = "${name}: Pago";
                $utag['page_path'] = "/transa/productos/seguros/${family}/${slug}/pago";
            } else if ($currentRoute == 'resume_insurance') {
                $utag['page_name'] = "${name}: Confirmación";
                $utag['page_path'] = "/transa/productos/seguros/${family}/${slug}/confirmacion";
            } else {
                $utag['page_path'] = "/transa/productos/seguros/${family}/${slug}/defult";
            }

            if($codFrec == "4") {
                $frequency = 'anual';
            } 
            if($codFrec == "5") {
                $frequency = 'única';
            }
            $utag['template']['page_path'] =  "/transa/productos/seguros/${family}/${slug}/#path#";
            $utag['template']['page_name'] = "$name: #name#";
            $utag['tipo_prima'] = $frequency;
        }
        return $utag;
    }

    public function getFamilybyTypeEs($type = '') {
        $families = [
            'health' => 'salud',
            'life' => 'vida',
            'home' => 'hogar',
            'travel' => 'viaje',
            'fraud' => 'fraude',
            'car' => 'auto'
        ];
        if (isset($families[$type])) {
            return $families[$type];
        }
        return 'default';
    }

    /**
     * Con un segmento y una familia entrega su realación.
     *
     * @param Segment $segment
     * @param Family $family
     * @return SegmentFamily
     */
    public function getSegmentFamily(Segment $segment, Family $family) {
        return $this->getRepository('AppBundle:SegmentFamily')->findOneBy([
                    'segment' => $segment,
                    'family' => $family,
        ]);
    }

    /**
     *
     * @param Route $route
     * @param array $parameters
     * @param boolean $referenceType
     * @return string The generated URL
     */
    public function generateUrl($route, $parameters = array(), $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH) {
        $parameters = array_merge($parameters, [
            'token' => $this->get('request')->get('token'),
            'segment_id' => $this->get('request')->get('segment_id'),
        ]);

        return parent::generateUrl($route, $parameters, $referenceType);
    }

    /* Incluye las opciones de Renta Anual para el plan Vida-Education */

    public function rentas() {
        $arrayrentas[] = '';
        $em = $this->getDoctrine()->getEntityManager();
        $dql = "SELECT renta
                FROM
                AppBundle:renta renta
                ORDER BY renta.orderby asc";
        $query = $em->createQuery($dql);
        $rentas = $query->getResult();
        foreach ($rentas as $key => $renta) {

            $planes = $renta->getPlan();
            $arrayrentas[$key]['id'] = $renta->getId();
            $arrayrentas[$key]['descripcion'] = $renta->getDescription();
            $arrayrentas[$key]['idplan'] = $planes->getId();
            $arrayrentas[$key]['orderby'] = $renta->getOrderby();
            $arrayrentas[$key]['codplanco'] = $renta->getCodplanco();
            $arrayrentas[$key]['codsp'] = $renta->getCodsp();
        }

        return $arrayrentas;
    }

    public function validacion($temp, $form, $product, $codStates) {
        if ($temp == 'travel.html.twig' || $temp == 'hospitalizacion.html.twig' || $temp == 'home.html.twig' || $temp == 'urgencias_medicas.html.twig')
            $option = 1;
        if ( $temp == 'catastrofico.html.twig' || $temp == 'life.html.twig' || $temp == 'oncologico.html.twig' 
			|| $temp == 'oncologicoUC.html.twig' || $temp == 'life_not_dps.html.twig' )
            $option = 2;
        if ($temp == 'vacaciones.html.twig' || $temp == 'vacaciones_largas_fam.html.twig')
            $option = 3;
        if ($temp == 'life_education.html.twig')
            $option = 4;
        /* if( $temp == 'vacaciones_largas_fam.html.twig')
          $option = 5; */

        $edad = $form->get('insured')->getData()->getBirthday();
        $dateTime = new \DateTime();
        $diff = date_diff($edad, $dateTime);
        $edad = $diff->y;
        $formState = $form->get('insured')->getData()->getAddressState();
        switch ($option) {
            case 1:
                $plform = $form->getData()->getPlan()->getId(); // form
                $plpro = $product->getPlans(); // valore del producto
                $cargas = $form->getData()->getCargas();
                $cargas = (count($cargas) == 0 ) ? $form->getData()->getBeneficiaries(): $cargas;
                $error = 0;
                $ok = 0;
                $formState = $form->get('insured')->getData()->getAddressState();

                if ($temp == 'home.html.twig') {
                    $formhome = $form->get('home')->getData()->getAddressState();
                    $ok2 = 0;
                    foreach ($codStates as $key => $r) {
                        if ($formhome == $key) {

                            $ok2 = 1;
                        }
                    }
                    if ($ok2 != 1) {
                        $error = 1;
                    }
                }


                foreach ($codStates as $key => $r) {
                    if ($formState == $key) {
                        $ok = 1;
                    }
                }
                if ($ok != 1) {
                    $error = 1;
                }
                foreach ($plpro as $key => $r) {
                    if ($r->getId() == $plform) {
                        $plfinal = $r;

                        if ($temp == 'hospitalizacion.html.twig' || $temp == 'urgencias_medicas.html.twig') {

                            if (count($cargas) < $r->getCargasMin() || count($cargas) > $r->getCargasMax()) {
                                $error = 1;
                            }
                        }
                    }
                }
                $finPri = $plfinal->getPrice();
                if ($temp == 'hospitalizacion.html.twig') {
                    $finPri = $finPri * ( count($cargas) + 1 );
                }

                if ($finPri != $form->getData()->getPrice() || $error == 1) {
                    $form->getData()->setPrice($finPri);
                    $result = false;
                } else {
                    $result = true;
                }
                //$cargas = $form->getData()->getCargas(); // cantidad de cargas
                //$plan = $form->getData()->getPlan()->getCargasPrice(); //valor del plan 
                return $result;
                break;
            case 2:
                $error = 0;
                $ok = 0;
                $formState = $form->get('insured')->getData()->getAddressState();

                $plform = $form->getData()->getPlan()->getId();
                $cover = $product->getPlans();

                foreach ($cover as $key => $r) {
                    foreach ($r->getTargets() as $key => $q) {
                        if ($edad >= $q->getStart() && $edad <= $q->getFinish() && $plform == $q->getPlanId()) {
                            $finPri = $q->getPrice();


                            if ($temp == 'oncologico.html.twig' || $temp == 'catastrofico.html.twig') {
                                $data = $form->getData()->getQuestions();
                                if ($data[0]->getAnswer() < 0)
                                    $error = 1;
                                if ($data[1]->getAnswer() < 0)
                                    $error = 1;
                            }
                        }
                    }
                }

                foreach ($codStates as $key => $r) {
                    if ($formState == $key) {
                        $ok = 1;
                    }
                }
                if ($ok != 1) {
                    $error = 1;
                }
                //dump($plfinal->getPrice());//dump($form->getData()->getPrice());exit;
                if ($temp == 'oncologicoUC.html.twig') {


                    if ($finPri < $form->getData()->getPrice()) {
                        $form->getData()->setPrice($form->getData()->getPrice());
                        $result = true;
                    } else {
                        $form->getData()->setPrice($finPri);
                        $result = true;
                    }
                } else {
                    if ($finPri != $form->getData()->getPrice() || $error == 1) {
                        $form->getData()->setPrice($finPri); //deberia borrarse al habilitar el result
                        $result = false;
                    } else {
                        $form->getData()->setPrice($finPri);
                        $result = true;
                    }
                }

                return $result;
                break;
            case 3:
                $plform = $form->getData()->getPlan()->getId(); // plan form
                $plpro = $product->getPlans(); // plan producto
                $cargas = count($form->getData()->getCargas()); // cargas
                $cover = $product->getCoverages(); // coberturas
                $coform = $form->getData()->getPrice(); // cober del form
                //var_dump($form->getData()->getCoverage());
                $ok = 0;
                $formState = $form->get('insured')->getData()->getAddressState();

                foreach ($codStates as $key => $r) {
                    if ($formState == $key) {
                        $ok = 1;
                    }
                }
                if ($ok != 1) {
                    $error = 1;
                }
                if ($temp == 'vacaciones.html.twig') {
                    foreach ($cover as $key => $r) {
                        if ($r->getPlaniD() == $plform) {


                            if ($r->getId() == $coform) {

                                $precio = $r->getPrice();
                                $precio = explode(',', $precio);
                             
                                $codplanco = $r->getCodplanco();
                                $codplanco = explode(',',$codplanco);
                                if( is_array($codplanco) ) {
                                    if(isset($codplanco[$cargas]) ){
                                        $codplanco = trim($codplanco[$cargas]); 
                                       
                                    } else {
                                        $codplanco = trim(end($codplanco));
                                    }
                                    if(isset($codplanco)){
                                        $form->getData()->setPri($codplanco);
                                    }
                                }

                                //var_dump($precio);
                                //echo $cargas;
                                if ($cargas <= 1) {
                                    $finPri = $precio[$cargas];
                                    //echo $plfinal;
                                } else {
                                    $finPri = $precio[2];
                                    //echo $plfinal;
                                }
                                $result = true;
                                if (count($cargas) < $r->getPlan()->getCargasMin() || count($cargas) > $r->getPlan()->getCargasMax()) {
                                    $error = 1;
                                }
                            }
                        }
                    }
                } else {
                    foreach ($cover as $key => $r) {
                        if ($r->getPlaniD() == $plform) {
                            if ($r->getPrice() == $coform) {
                                $finPri = $r->getPrice();
                                
                                $codplanco = $r->getCodplanco();
                                $codplanco = explode(',',$codplanco);
                                if( is_array($codplanco) ){
                                    if(isset($codplanco[$cargas]) ){
                                        $codplanco = trim($codplanco[$cargas]); 
                                       
                                    } else {
                                        $codplanco = trim(end($codplanco));
                                    }
                                    if(isset($codplanco)){
                                        $form->getData()->setPri($codplanco);
                                    }
                                }
                                
                                
                                $result = true;
                                if (count($cargas) < $r->getPlan()->getCargasMin() || count($cargas) > $r->getPlan()->getCargasMax()) {
                                    $error = 1;
                                }
                            }
                        }
                    }
                }


                $form->getData()->setPrice($finPri);

                return $result;
                break;
            case 4:
                //PENDIENTE
                $error = 0;
                $codcurso = 0;
                $edad = $form->get('insured')->getData()->getBirthday();
                $cursos = $this->container->get('sp.cursos')->getCursos();
                foreach ($cursos as $key => $r) {
                    if ($form->getData()->getCurso() == $r['CURCOD']) {
                        if ($form->getData()->getRenta() >= 6 && $form->getData()->getRenta() < 8) {
                            if ($form->getData()->getCurso() > 15 && $form->getData()->getCurso() < 22) {
                                $codcurso = $r['CURCOD'];
                            } else {
                                $error = 1;
                            }
                        }
                        if ($form->getData()->getRenta() >= 4 && $form->getData()->getRenta() < 6) {
                            if ($form->getData()->getCurso() > 0 && $form->getData()->getCurso() < 16) {
                                $codcurso = $r['CURCOD'];
                            } else {
                                $error = 1;
                            }
                        }
                        if ($form->getData()->getRenta() > 0 && $form->getData()->getRenta() < 4) {
                            if ($form->getData()->getCurso() >= 0 && $form->getData()->getCurso() < 22) {
                                $codcurso = $r['CURCOD'];
                            } else {
                                $error = 1;
                            }
                        }
                    }
                }

                $ok = 0;
                $formState = $form->get('insured')->getData()->getAddressState();
                foreach ($codStates as $key => $r) {
                    if ($formState == $key) {
                        $ok = 1;
                    }
                }
                if ($ok != 1) {
                    $error = 1;
                }


                if ($form->getData()->getCurso() > 21 || $form->getData()->getCurso() < 0)
                    $error = 1;
                if ($form->getData()->getRenta() > 0 && $form->getData()->getRenta() < 8) {
                    $codplan = $form->getData()->getRenta();
                } else {
                    $error = 1;
                }

                $dateTime = new \DateTime();
                $diff = date_diff($edad, $dateTime);
                $diff = $diff->y;

                if ($diff > $product->getAgeMin() && $diff < $product->getAgeMax()) {
                    $nac = $edad->format('j-m-Y');
                } else {
                    $error = 1;
                }
                $data = $form->getData()->getQuestions();

                if ($data[0]->getAnswer() < 0)
                    $error = 1;
                if ($data[1]->getAnswer() < 0)
                    $error = 1;


                //$nac = $edad->format('j-m-Y');
                //$codcurso = $form->getData()->getCurso();

                $codprod = $product->getCode();
                $fechaactual = date('j-m-Y');
                $perid = 'M';


                /*
                  echo $perid;
                  echo '<br>';
                  echo $codprod;
                  echo '<br>';
                  echo $fechaactual;
                  echo '<br>';
                  echo $nac;
                  echo '<br>';
                  echo $codcurso;
                  echo '<br>';
                  echo $codplan;
                  echo '<br>';
                  die(); */


                /* $jsonprima = json_decode($_POST['case'],true);
                  $codcurso = $jsonprima['curso'];
                  $codprod = $jsonprima['producto'];
                  $codplan = $jsonprima['renta'];
                  $nac = $jsonprima['fecha'];
                  $fechaactual = date('j-m-Y');
                  $perid='M'; */
                /* (293, '1', '28-09-1981', 'M', 6, '10-02-2017');
                  Código de producto, código plan, fch nac asegurado,periodicidad(A,M),Código curso entregado por spCursos,Fecha de proceso */
                $finPri = 0;

                if ($error != 1) {
                    $sp = $this->container->get('sp.prima')->getPrima($codprod, $codplan, $nac, $perid, $codcurso, $fechaactual);
                    if ($sp[0]['EDU_PROY_VAL'] > 0) {
                        $finPri = $sp[0]['EDU_PROY_VAL'];
                    }
                } else {
                    $error = 1;
                }



                if ($finPri != $form->getData()->getPrice() || $error == 1) {
                    $form->getData()->setPrice($finPri); //deberia borrarse al habilitar el result
                    $result = false;
                } else {
                    $form->getData()->setPrice($finPri);
                    //$form_educational->setPrima($finPri);
                    $result = true;
                }

                return $result;
                break;
            default:
                return true;
                break;
        }
    }
    
    public function getProductTemplate(Productos $product){
		if(method_exists($product, 'getPlantilla')){
			$plantilla = $product->getPlantilla();
			if(@method_exists($plantilla, 'getTemplate')){
				$plantilla = $plantilla->getTemplate();
				return $plantilla;
			}
		}
		return false;
	}

}
