<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

class AcceptanceController extends Controller {

    /**
     * @Route("/formulario", name="acceptance_form")
     * @Method({"GET", "POST"})
     *
     * @return Response
     */
    public function indexAction( Request $request) {
        $session = $request->getSession();
        $person =  $session->get('person');
        $data = $request->request;
        $acceptance = $this->container->get('acceptance');
        $polizas = $acceptance->getVenta($person['code']);
        
        if ($request->isMethod('post')){
            $session->set('acceptance', $data);
            if($polizas) {
                foreach($polizas as $poliza) {
                    if(isset($poliza->NUMSOLICITUD)) {
                        if( $poliza->NUMSOLICITUD == $data->get('poliza')) {
                            $session->set('poliza', $poliza);
                            break;
                        }
                    }
                }
                return $this->redirect($this->generateUrl('acceptance_proposal'), 301);
            } else {
                return $this->redirect($this->generateUrl('acceptance_error'), 301);
            }
        }
     
        return $this->render('::acceptance/list.html.twig', ['polizas' => $polizas]);       
    }

    /**
     * @Route("/propuesta", name="acceptance_proposal")
     * @Method({"GET", "POST"})
     *
     * @return Response
     */

     public function proposalAction(Request $request) {
        $session = $request->getSession();
        $poliza = $session->get('poliza');
        $person =  $session->get('person');
        $code = $person['code'];
        if(isset($poliza->NUMSOLICITUD) ) {
            if ($request->isMethod('post')){
                return $this->redirect($this->generateUrl('acceptance_confirmation'), 301);
            }
            return $this->render('::acceptance/proposal.html.twig', ['id' => $poliza->NUMSOLICITUD]);
        }
        return $this->redirect($this->generateUrl('acceptance_error'), 301);
     }

     
     /**
     * @Route("/propuesta/pdf/{id}", name="acceptance_pdf_proposal")
     * @Method({"GET", "POST"})
     *
     * @return Response
     */
    public function proposalPdfAction($id, Request $request) {
        $proposal = $id;
        $session = $request->getSession();
        $person =  $session->get('person');
        $acceptance = $this->container->get('acceptance');
        $person =  $session->get('person');
        $pdfBlob = $acceptance->getProposalPdf($person['code'], [
            'NUMEROREG' => $proposal,
        ]);
        if($pdfBlob) {
            $data = base64_decode($pdfBlob);
            header('Content-Type: application/pdf');
            header('Content-Disposition: filename="downloaded.pdf"');
            die($data);
        }
        return $this->render('::acceptance/error.html.twig');
    }
     

    /**
     * @Route("/confirmacion", name="acceptance_confirmation")
     * @Method({"GET", "POST"})
     *
     * @return Response
     */
    public function confirmationAction( Request $request) {
        $session = $request->getSession();
        $poliza = $session->get('poliza');
        $person =  $session->get('person');
        $acceptance = $this->container->get('acceptance');
        $email = str_replace(' ', '', @$person['addressEmail']);
       
        $venta = $acceptance->setVenta( $person['code'], [ 
            'TIPOPRODUCTO' => $poliza->TIPOPRODUCTO,
            'COMPANIA' => (($poliza->DESCFAMILIAPROD == 'VIDA') ? 'V' : 'G'),
            'NUMEROREG' => $poliza->NUMSOLICITUD,
            'EMAILCLIENTE' => $email
        ]);

        if(!$venta) {
            return $this->redirect($this->generateUrl('acceptance_error'), 301);    
        }
        return $this->render('::acceptance/confirmation.html.twig', ['poliza' => $poliza]);
    }

   /**
     * @Route("/error", name="acceptance_error")
     * @Method({"GET", "POST"})
     *
     * @return Response
     */
    public function errorAction( Request $request) {
     
        return $this->render('::acceptance/error.html.twig');       
    }



}
