<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use FOS\RestBundle\Controller\FOSRestController;

use AppBundle\Entity\Product;

class RestController extends FOSRestController
{

    /**
     * @Route("/uf.{_format}", name="rest_uf", defaults={"_format"="json"})
     */
  
    public function ufAction()
    {
        $uf = $this->getDoctrine()
            ->getRepository('AppBundle:Uf')
            ->findLast();

        $view = $this->view([
                'day' => $uf->getDay(),
                'value' => $uf->getValue()
            ]);

        return $this->handleView($view);
    }


    /**
     * @Route("/product/{id}.{_format}", name="rest_product", defaults={"_format"="json"})
     * @ParamConverter("product", class="AppBundle:Product")
     */
    public function productAction(Product $product)
    {
        $data = $this->view([
                'id' => $product->getId(),
                'price' => $product->getPrice()
            ]);

        return $this->handleView($data);
    }

    /**
     * @Route("/product/{id}/plan.{_format}", name="rest_plan", defaults={"_format"="json"})
     * @ParamConverter("product", class="AppBundle:Product")
     */
    public function planTargetAction(Product $product)
    {


      $jsonprima = json_decode($_POST['case'],true);
      $codcurso = $jsonprima['curso'];
      $codprod = $jsonprima['producto'];
      $codplan = $jsonprima['renta'];
      $nac = $jsonprima['fecha'];
      $fechaactual = date('j-m-Y');
      $perid='M';


       /* (293, '1', '05-05-1982', 'M', 4, '02-08-2016');
      Código de producto, código plan, fch nac asegurado,periodicidad(A,M),Código curso entregado por spCursos,Fecha de proceso */
        $arrayPrima =  $this->container->get('sp.prima')->getPrima($codprod,$codplan,$nac,$perid,$codcurso,$fechaactual);

        
        return $arrayPrima;

        //return 'hola';
       
       /* $data = [];
       
        $plans = $product->getPlans();

        $i = 0;
        foreach ($plans as $plan) {
            $data[$i] = [];
            $targets = $plan->getTargets();

            foreach ($targets as $target) {
                $data[$i][] = $target->toArray();

            }
            $i++;
        }

        $view = $this->view($data);*/
      //return $this->handleView($view);
    }

    /**
     * @Route("/product/{id}/plan/target/coverage.{_format}", name="rest_plan_target_coverage", defaults={"_format"="json"})
     * @ParamConverter("product", class="AppBundle:Product")
     */
    public function planTargetCoverageAction(Product $product)
    {
        $data = [];
        $plans = $product->getPlans();
        $i = 0;
        foreach ($plans as $plan) {
            $targets = $plan->getTargets();
            foreach ($targets as $target) {
                $coverages = $target->getCoverages();
                foreach ($coverages as $coverage) {
                    $data[$i] = [
                        'plan' => $plan->toArray(),
                        'target_id' => $target->getId(),
                        'start' => $target->getStart(),
                        'finish' => $target->getFinish(),
                        'price' => $target->getPrice(),
                        'name' => $coverage->getName(),
                        'capital' => $coverage->getCapital(),
                    ];
                    $i++;
                }
            }
        }

        $view = $this->view($data);

        return $this->handleView($view);
    }
    
}
