<?php

namespace AppBundle\Controller;

use AppBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class DpController extends BaseController
{
	
	/**
	 * @Route("/", name="dp")
	 * @Method({"POST"})
	 *
	 * @return Response
	 */
	public function indexAction(Request $request){
	
		$headertoken = @$_REQUEST['TokenDP'];
		$session = $request->getSession();
		$session->invalidate();
		$session = new Session();
		$themeparam = '';	  
		$session->set('token', $headertoken);
		  
		/**
		Validamos el Token recibido (El cual tiene 3 parametros {token,canal,app} 
		de entrada)
		**/
		$token = $this->get('token_dp_session')->ValidateTokenOB($headertoken);
		
		// $logger->info(date('Y-m-d H:i:s u').' - valida token fin');
		/*+++++++++++++++++++++++++++++++++++++++++++*/			 
		
		if($token=='OK'){		  
			//Para redireccionarlo 
			$validtoken=true;
		//	$session->set('esvalido_dp', true);
			$session->set('esvalido', true);
			
			$rut = @$_REQUEST['RUT'];
			$session->set('rut', $rut);
			$coderut = $rut;
			
			//Segmento 
			$Segmento='personas';
			$session->set('segmento', $Segmento);
			//Theme
			$session->set('themedisplay', 'mobile');
			//$themeparam ='';
			$themeparam = '?theme=mobile';
	        //sessión
	        $session->set('apikeyuserprovider_dp', 1); 
			$session->set('tiemposesion', time());
			//die;
		}else{
			$session->set('esvalido_dp', false);
			$validtoken=false;
		}

		// $logger->info(date('Y-m-d H:i:s u').' - prepara redireccion');

		if($validtoken==true){
			return $this->redirect('../customers/info/'.$themeparam);
		}else{
			throw new NotFoundHttpException("Page not found"); 
		}
		
		$session->migrate();

	}

    /**
     * @Route("/gettoken")
     * @Method({"GET", "POST"})
     *
     * @return Response
     */
    public function gettoken(Request $request){
		$rut="00215215210"; //Variable con un rut de prueba
		//$rut="00031848385";
		    
		$headertoken = $this->get('token_dp_session')->getGeneraDPToken(); 
		//return $this->redirect('../dp/?TokenDP='.$headertoken);
    	return $this->redirect('../dp/?TokenDP='.$headertoken. '&RUT=' . $rut);
    }

  
}