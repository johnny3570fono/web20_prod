<?php

namespace AppBundle\Controller;

use JavierEguiluz\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;
use JavierEguiluz\Bundle\EasyAdminBundle\Event\EasyAdminEvents;

use AppBundle\Form\PersonType;
use AppBundle\Form\ProductType;
use AppBundle\Entity\Product;
use AppBundle\Entity\Campaign;
use AppBundle\Entity\SegmentAssist;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpFoundation\Response;


/**
 * Administrador
 */
class AdminController extends BaseAdminController
{
    /**
     * {@inheritDoc}
     */
    public function createEntityForm($entity, array $entityProperties, $view)
    {
        if ($entity instanceof SegmentAssist) {
            $entity->setUpdateAt(date('Y-m-d H:i:s'));
        }

        $editForm = parent::createEntityForm($entity, $entityProperties, $view);

        if ($entity instanceof Product) {
            $code = $editForm->get('code');
            $editForm->add('code', 'text', [
                'constraints' => [
                    new \Symfony\Component\Validator\Constraints\Length(['max' => 3])
                ]
            ]);
        }

        return $editForm;
    }
	
	 /**
     * @Route("/command", name="admin_command")
     * @Method({"GET" , "POST"})
     * @return Response
     */
	 public function commandAction(Request $request){
		$defaultData = array('message' => 'Type your message here');
		$form = $this->createFormBuilder($defaultData)
			->add('typeCommand', 'choice', array(
				'choices'  => array(
					'TentadoCommand' => 0,
					'ReporteDiarioCommand' => 1,
					'UserCommand' => 2,
					'VentasCommand' => 3
				),
				// *this line is important*
				'choices_as_values' => true)
			)
            ->add('startDate', 'date', array(
				'placeholder' => array(
					'year' => 'Year', 'month' => 'Month', 'day' => 'Day'
				),
				'years' => range(date('Y'), date('Y') + 5),
				'format' => 'dd-MM-yyyy',
			))
			->add('endDate', 'date', array(
				'placeholder' => array(
					'year' => 'Year', 'month' => 'Month', 'day' => 'Day'
				),
				'format' => 'dd-MM-yyyy',
				'attr' => array('class' => 'form-control"')
			))
            ->add('execute', 'submit', array('label' => 'Ejecutar' , 'attr' => array('class'=>'btn btn-primary')))
            ->getForm();

		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid()) {
			$data = $form->getData();
			if(isset($data['typeCommand'])){
				$typeCommand = (int) $data['typeCommand'];
				$startDate = $data['startDate'];
				$endDate = $data['endDate'];
				$startDate = $startDate->format('Y-m-d');
				$endDate = $endDate->format('Y-m-d'); 
				if($typeCommand == 0) {
					$command = "tentado:generate";
				} else if($typeCommand == 1) {
					$command = "report:generate";
				} else if($typeCommand == 2){
					$command = "activity:generate";
				} else if($typeCommand == 3){
					$command = "ventas:generate";
				}
				//php app/console tentado:generate 2017-09-01 00:00 2017-11-30
				$kernel = $this->get('kernel');
				$application = new Application($kernel);
				$application->setAutoExit(false);
				//dump($data);
				//cambiar
				$input = new ArrayInput(array(
				   'command' => $command,
				   'fecha' => $startDate,
				   'hora' => '00:00',
				   'fecha_hasta' => $endDate,
				   'hora_hasta' => '23:59'
				));
				$output = new BufferedOutput();
				$application->run($input, $output);
				$content = $output->fetch();
				return new Response($content);
				die;
			}
			
		}
		return $this->render('::easy_admin/command/layout.html.twig', ['form' =>$form->createView()]);
	}



	protected function createListQueryBuilder($entityClass, $sortDirection, $sortField = null) {
		$queryBuilder = $this->em->createQueryBuilder()->select('entity')->from($entityClass, 'entity');
		$nameEntity = $this->entity['name'];
		
        if (null !== $sortField) {
            $queryBuilder->orderBy('entity.' . $sortField, $sortDirection);
		}
		
		if($nameEntity === 'InstantCall') {
			$repoSegment = $this->em->getRepository('AppBundle:Segment');
			$products = $repoSegment->findOneBy(array('id' => 1))->getProducts();
			$queryConditions = $queryBuilder->expr()->orX();
			foreach($products as $product){
				$queryConditions->add('entity.id = '.$product->getId());
			}
			$queryBuilder->add('where', $queryConditions);
		}

        return $queryBuilder;
	}
	
	
	public function preUpdateEntity($entity) {
		if ($entity instanceof Campaign) {
			$title = $entity->getTitle();
			$id = $entity->getId();
			$repoCampaign = $this->em->getRepository('AppBundle:Campaign');
			$campaign = $repoCampaign->findUniqueTitle($title, $id);
			if (method_exists($entity, 'setTitle')) {
				if($campaign) {
					$entity->setTitle("{$entity->getTitle()} [Duplicado #".uniqid()."]");
				}
			}
		}
		
        parent::preUpdateEntity($entity);
    }

	

	
	public function prePersistEntity($entity) {
		if ($entity instanceof Campaign) {
			$title = $entity->getTitle();
			$repoCampaign = $this->em->getRepository('AppBundle:Campaign');
			$campaign = $repoCampaign->findUniqueTitle($title);
			if (method_exists($entity, 'setTitle')) {
				if($campaign) {
					$entity->setTitle("{$entity->getTitle()} [Duplicado #".uniqid()."]");
				}
			}
		}
		
        parent::prePersistEntity($entity);
    }
}
