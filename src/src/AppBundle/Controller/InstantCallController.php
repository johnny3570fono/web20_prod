<?php

namespace AppBundle\Controller;

use AppBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;


/**
 * Rut Controller
 */
class InstantCallController extends FOSRestController {


    /**
     * @Route("/{step}", name="instant_call",  defaults={"_format"="json"})
     * @Method({"POST", "GET"})
     *
     * @return Response
     */
    public function installCallAction($step, Request $request) {
        $this->servicesInstantCall = $this->container->getParameter('instantcall');
        $session = $request->getSession();
        $person =  $session->get('person');
        $response = $this->registerInstantCall($step, $person);
   
        $view = $this->view($response);
       
        return $this->handleView($view);
       
       // return false;
    }
    
    public function getSantanderInstantCall($rut) {
        $rut = preg_replace('/[^0-9Kk]/', '', $rut);
        $rut = str_pad($rut, 11, '0', STR_PAD_LEFT);
        $curl = curl_init();
        $json = '{
            "Cabecera":{
             "HOST": {
              "USUARIO-ALT":"GAPPS2P",
              "TERMINAL-ALT":"",
              "CANAL-ID":"078"
             },
             "CanalFisico":"78",
             "CanalLogico":"74",
             "RutCliente":"'.$rut.'",
             "RutUsuario":"'.$rut.'",
             "IpCliente":"",      
             "InfoDispositivo":"Desktop"   
            },
            "rut":"'.$rut.'"
           }';
       
        curl_setopt_array($curl, array(
          CURLOPT_PORT => "17380",
          CURLOPT_URL => $this->servicesInstantCall['banco_service'],
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $json,
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "cache-control: no-cache"
          ),
        ));

        if(isset($this->servicesInstantCall['banco_cert'])) {
          curl_setopt($curl, CURLOPT_CAINFO, $this->servicesInstantCall['banco_cert']);
        }

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        $data = json_decode($response);
        if(isset($data->OUTPUT)) {
            return $data->OUTPUT;
        }
        return false;
    }


    public function registerInstantCall($step = 'INI', $person) {
        $data = $this->getSantanderInstantCall($person['code']);
        $birthday = $person['birthday'];
        $birthday_date = $birthday->format('d/m/Y');
        $tz  = new \DateTimeZone('America/Santiago');
        $age = $birthday->diff(new \DateTime())->y;
    
        if(isset($data->MATRIZ) && isset($data->ESCALARES) ) {
          
            $escalaRut = $data->ESCALARES->RUT;
            $rut = substr($escalaRut, 0, -1);
            $rut = str_pad($rut, 11, '0', STR_PAD_LEFT);
            $dv = substr($escalaRut, -1);
            $comuna = $data->ESCALARES->COMUNA_PART;
            $ciudad = $data->ESCALARES->CIUDAD_PART;
            $calle = $data->ESCALARES->CALLE_PART;
            $numero =  preg_replace('/\s+/', ' ', $data->ESCALARES->NUMERO_PART);
            $productId = $this->get("request")->get('product');
            $product = $this->getProduct((int) $productId);
            if(method_exists($product,'getName')) {
              $step = ($step === 'inicio') ? 'INI': 'FIN';
              $json = [
                'KEY' => 123,
                'CANAL' => 1,
                'RUT' => $rut,
                'DV' => $dv,
                'NOMBRE' => $person['name'],
                'APPAT' => $person['lastname1'],
                'AP_MAT' => $person['lastname2'],
                'FECHA_NACIMIENTO' => $birthday_date,
                'EDAD' => $age,
                'AFONOCOM1' => '', 'TELCOM1'=> '',
                'AFONOCOM2' => '', 'TELCOM2' => '',
                'AFONOPAR1' => '', 'TELPAR1' => '',
                'AFONOPAR2' => '', 'TELPAR2' => '',
                'AFONO11' => '', 'FONO11' => '',
                'AFONO21' => '',  'FONO21' => '',
                'COMUNA_PARTICULAR' => $comuna,
                'CIUDAD_PART' => $ciudad,
                'CALLE_PART' => $calle,
                'NUMERO_PART' => $numero,
                'MARCA_1' => "",
                'PLASTICO_1' => "",
                'MARCA_2' => "",
                'PLASTICO_2' => "",
                'PROGRAMA_2' => "",
                'MARCA_3' => "",
                'PLASTICO_3' => "",
                'PROGRAMA_3' => "",
                'CTACTE' => "",
                'NOMBRE_SEGURO' => $product->getName(),
                'PASO_INI_FIN' => $step,
              ];

              $creditCardsData = $this->getCreditCards($person['creditCards']);
              $phoneData = $this->getPhoneData($data->MATRIZ);

              $json = array_merge($json, $phoneData);
              $json = array_merge($json, $creditCardsData);
              $json = json_encode($json);

              $curl = curl_init();
              curl_setopt_array($curl, array(
                CURLOPT_PORT => "443",
                CURLOPT_URL => $this->servicesInstantCall['zurich_service'],
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $json,
                CURLOPT_HTTPHEADER => array(
                  "Content-Type: application/json",
                  "cache-control: no-cache"
                ),
              ));
              curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
              curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
              $response = curl_exec($curl);
              $response = preg_replace('/\s/', '', $response);
              $response = json_decode($response, true);
              $err = curl_error($curl);
              
              curl_close($curl);
            }
        }

        
        try {
        
          if(isset($response['OUTPUT']) && isset($response['OUTPUT']['ID']) && !empty($response['OUTPUT']['ID']) ) {
            $response = [ 
              'INFO' =>  [
                'CODERR' => 0,
                'STATUS' => 'SUCCESS',
                'ID' => $response['OUTPUT']['ID']
              ]
            ];
          } else if(isset($response['INFO'])) {
            $response = [ 
              'INFO' =>  [
                'CODERR' => $response['INFO']['CODERR'],
                'STATUS' => 'ERROR'
              ]
            ];
          } else {
            $response = [ 
              'INFO' =>  [
                'CODERR' => 999,
                'STATUS' => 'ERROR'
              ]
            ];
          }
        } catch(\Exception $e) {
         
          $response = [ 
            'INFO' =>  [
              'CODERR' => -1,
              'STATUS' => 'ERROR'
            ]
          ];
        }
       
    
        return $response;
    }
   

    public function getPhoneData($matriz) {
      $response = [];
      if($matriz) {
        $indexParticular = 1;
        $indexComercial = 1;
        $indexOther = 1;
        foreach($matriz as $data){
          if($data->TIPOTELEFONO === 'PARTICULAR' && $indexParticular <= 2) {
            $response["AFONOPAR${indexParticular}"] = $data->CODIGOTELEFONO;
            $response["TELPAR${indexParticular}"] = $data->TELEFONO;
            $indexParticular++;
          } else if($data->TIPOTELEFONO === 'COMERCIAL' && $indexComercial <= 2){
            $response["AFONOCOM${indexComercial}"] = $data->CODIGOTELEFONO;
            $response["TELCOM${indexComercial}"] = $data->TELEFONO;
            $indexComercial++;
          } else if($data->TIPOTELEFONO !== 'PARTICULAR' &&  $data->TIPOTELEFONO !== 'COMERCIAL') {
            if($indexOther <= 2) {
              $response["AFONO${indexComercial}1"] = $data->CODIGOTELEFONO;
              $response["FONO${indexComercial}1"] = $data->TELEFONO;
              $indexOther++;
            }
          }
        }
      }
      return $response;
    }


    public function getCreditCards($cards) {
       $response = [];
       if($cards) {
          $index = 1;
          foreach($cards as $card) {
            if($card->name === 'CUENTA CORRIENTE') {
              $response['CTACTE'] = $card->cardNumber;
            } else {
              if($index <= 3 ) {
                $response["MARCA_${index}"] = $this->getCardType($card->cardNumber);
                $response["PLASTICO_${index}"] = $card->name;
                $index++;
              }
            }
          }
       }
       return $response;
    }

    public function getCardType ($numberCreditCard) {
      $codViaPago = '';
      $card_regexes = array(
        "/^4\d{12}(\d\d\d){0,1}$/" => 'VISA',
        "/^5[12345]\d{14}$/"       => 'MASTERCARD',
        "/^3[47]\d{13}$/"          => 'AMEX',
        "/^30[012345]\d{11}$/"     => 'DINERS',
        "/^3[68]\d{12}$/"          => 'DINERS',
      );
      foreach ($card_regexes as $regex => $type) {
        if (preg_match($regex, $numberCreditCard)) {
            $codViaPago = $type;
            break;
        }
      }
      return $codViaPago;
    }

    public function getProduct($id){
      $em = $this->getDoctrine()->getManager();;
      $product = $em->getRepository('AppBundle:Product')->findOneBy(['id' => $id]);
      if($product) {
        return $product;
      }
      return false;
    }

}
