<?php

namespace AppBundle\Controller;

use AppBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use AppBundle\Entity\Product;

/**
 * Controller para producto.
 */
class ProductController extends BaseController
{
    /**
     * @Route("/product/{product_id}", name="show_product")
     * @Method({"GET"})
     * @ParamConverter("product", class="AppBundle:Product", options={"id" = "product_id"})
     *
     * @param Product $product
     * @return Response
     */
    public function showAction(Product $product)
    {
        if(!$product->inSegment($this->getUser()->getSegment())){
            throw new \Exception('Segmento erroneo');
        }

        // $logger = $this->get('logger');
        // $logger->info("\n\n\n");
        // $logger->info(date('Y-m-d H:i:s u').' - Cargando Product Controller');
        $name= $this->getUser()->getName()." ".$this->getUser()->getLastName1()." ".$this->getUser()->getLastName2();
        
        $code= $this->getUser()->getCode();
      
        $code=str_replace("-","",$code);
        $rut = substr($code, -4);
     
        // $logger->info(date('Y-m-d H:i:s u').' - enviando data de producto');
        return $this->render('::product/show.html.twig', [
            'product' => $product,
            'uf' => $this->getUf(),
            'fecha' =>  getdate(),
            'desc' => $this->getDesc($product,$this->getUf()),
            'name'=>$name,
            'rut'=>$rut

        ]);

    }
   

    


}
