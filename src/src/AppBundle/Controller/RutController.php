<?php

namespace AppBundle\Controller;

use AppBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

/**
 * Rut Controller
 */
class RutController extends BaseController
{
    /**
     * @Route("/", name="rut_form")
     * @Method({"GET"})
     *
     * @return Response
     */
    public function indexAction()
    {
        return $this->render('::rut/index.html.twig');
    }
}
