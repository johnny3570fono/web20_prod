<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsRun extends Constraint
{
    public $message = 'RUN "%string%" no válido.';
}
