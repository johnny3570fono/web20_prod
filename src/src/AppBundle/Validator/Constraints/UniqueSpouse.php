<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class UniqueSpouse extends Constraint
{
    public $message = 'Solo se puede defenir un cónyuge.';
}
