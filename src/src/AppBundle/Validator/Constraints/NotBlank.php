<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraints\NotBlank as SymfonyNotBlank;

/**
 * @Annotation
 * @Target({"CLASS", "ANNOTATION"})
 */
class NotBlank extends SymfonyNotBlank
{
    public $errorPath = 'iconFile';

    /**
     * {@inheritdoc}
     */
    public function getTargets()
    {
        return [self::PROPERTY_CONSTRAINT];
    }

    /**
     * Option path
     *
     * @return string
     */
    public function getDefaultOption()
    {
        return 'errorPath';
    }
}
