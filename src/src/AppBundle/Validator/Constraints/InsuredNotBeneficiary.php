<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class InsuredNotBeneficiary extends Constraint
{
    public $message = 'El asegurado no puede ser beneficiario';
}
