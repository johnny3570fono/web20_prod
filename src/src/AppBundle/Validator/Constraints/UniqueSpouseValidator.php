<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

use AppBundle\Entity\Beneficiary;

class UniqueSpouseValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        $spouse = 0;
        foreach ($value as $beneficiary) {
            if ($beneficiary->getRelationship() == Beneficiary::REALTIONSHIP_SPOUSE) {
                $spouse++;
            }
        }

        if ($spouse > 1) {
            $this->context->buildViolation($constraint->message)
                ->atPath('[1].relationship')
                ->addViolation();
        }
    }
}
