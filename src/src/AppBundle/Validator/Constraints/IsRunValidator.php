<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validador de DV de RUN.
 */
class IsRunValidator extends ConstraintValidator
{
    /**
     * Validador
     *
     * @param string     $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (is_null($value) || empty($value)) {
            return true;
        }

        $run = preg_replace('/[^0-9kK]/', '', $value);

        $dv = strtoupper(substr($run, -1));
        $run = substr($run, 0, -1);

        $s = 1;
        for ($m = 0; $run != 0; $run/=10) {
            $s = ($s + $run%10 * (9-$m++%6))%11;
        }

        $dvValid = strtoupper(chr($s ? $s + 47 : 75));

        if ($dv != $dvValid) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('%string%', $value)
                ->addViolation();
        }
    }
}
