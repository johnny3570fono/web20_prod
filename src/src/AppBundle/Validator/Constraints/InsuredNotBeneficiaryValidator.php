<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class InsuredNotBeneficiaryValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        $insured = $this->context->getRoot()->get('insured')->getData();

        $codes = [];
        foreach ($value as $beneficiary) {
            $codes[] = $beneficiary->getCode();
        }

        if (count($codes) != count(array_unique($codes))) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('%string%', $insured->getCode())
                ->atPath('[0].code')
                ->addViolation();
        }
    }
}
