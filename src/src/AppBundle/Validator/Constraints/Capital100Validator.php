<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class Capital100Validator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        $capital = 0;
        foreach ($value as $beneficiary) {
            $capital = $capital + $beneficiary->getCapital();
        }

        if (count($value) > 0 && (int)$capital != 100) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('%string%', $capital)
                ->atPath('[0].capital')
                ->addViolation();
        }
    }
}
