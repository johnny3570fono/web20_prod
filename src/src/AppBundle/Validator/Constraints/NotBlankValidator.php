<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraints\NotBlankValidator as SymfonyNotBlankValidator;

/**
 * Validador
 */
class NotBlankValidator extends SymfonyNotBlankValidator
{
    /**
     *
     * @param string                                  $value
     * @param \Symfony\Component\Validator\Constraint $constraint
     */
    public function validate($value, \Symfony\Component\Validator\Constraint $constraint)
    {
        parent::validate($value, $constraint);
    }

    /**
     * Target
     *
     * @return string
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
