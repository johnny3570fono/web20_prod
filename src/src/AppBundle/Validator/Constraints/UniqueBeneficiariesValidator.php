<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UniqueBeneficiariesValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        $codes = [];
        foreach ($value as $beneficiary) {
            $codes[] = $beneficiary->getCode();
        }

        if (count($codes) != count(array_unique($codes))) {
            $codes = array_count_values($codes);

            $repeats = [];

            foreach ($codes as $code => $num) {
                if ($num > 1) {
                    $repeats[] = $code;
                }
            }

            $this->context->buildViolation($constraint->message)
                ->setParameter('%string%', implode(', ', $repeats))
                ->atPath('[0].code')
                ->addViolation();
        }
    }
}
