<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Capital100 extends Constraint
{
    public $message = 'La suma(%string%) de los capitales de los beneficiarios debe sumar 100%';
}
