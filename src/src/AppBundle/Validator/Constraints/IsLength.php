<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsLength extends Constraint
{
    public $message = 'Tamaño invalido "%string%" no válido.';
}
