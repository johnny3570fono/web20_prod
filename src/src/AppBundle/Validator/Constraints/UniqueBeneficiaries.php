<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class UniqueBeneficiaries extends Constraint
{
    public $message = 'Los beneficiarios(%string%) no se pueden repetir.';
}
