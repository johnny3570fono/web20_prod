<?php

namespace AppBundle\Security;

use Symfony\Component\Security\Core\Authentication\SimplePreAuthenticatorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Session;

use AppBundle\Entity\Customer;
use AppBundle\Entity\Segment;

/**
 * {@inheritDoc}
 */
class ApiKeyAuthenticator implements SimplePreAuthenticatorInterface
{
    private $em;

    /**
     * {@inheritDoc}
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->em = $this->container->get('doctrine')->getManager();
    }

    /**
     * {@inheritDoc}
     */
    public function createToken(Request $request, $providerKey)
    {

        $session = $request->getSession();
 
        $apiKey = $session->get('token');
        


        if (!$apiKey) {
            throw new BadCredentialsException('Se necesita un token.');
        }

        return new PreAuthenticatedToken(
            'anon.',
            $apiKey,
            $providerKey
        );
    }

    /**
     * Para determinar quien debe manejar el token
     *
     * @param TokenInterface $token
     * @param type           $providerKey
     * @return type
     */
    public function supportsToken(TokenInterface $token, $providerKey)
    {

        return $token instanceof PreAuthenticatedToken && $token->getProviderKey() === $providerKey;
    }

    /**
     * {@inheritDoc}
     */
    public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey)
    {
        if (!$userProvider instanceof ApiKeyUserProvider) {
            throw new \InvalidArgumentException(
                sprintf(
                    'The user provider must be an instance of ApiKeyUserProvider (%s was given).',
                    get_class($userProvider)
                )
            );
        }

        $apiKey = $token->getCredentials();

        /**
         * Existe session en el banco en el servidor
         */
         //$headertoken = @$_SERVER['TOKENSSO'];

       //$user = $this->container->get('security.context')->getToken();
       
       /*************Pruebas*******/
      // $headertoken = @$_SERVER['HTTP_TOKENSSO'];
        /***********/
        $customer = $userProvider->getUsernameForApiKey($apiKey);

        if (!$customer) {
            throw new AuthenticationException('Token incorrecto');
        }

        if (!isset($customer['segment'])
                || !isset($customer['code'])
                || !isset($customer['name'])
                || !isset($customer['lastname1'])
                || !isset($customer['lastname2'])) {
            throw new AuthenticationException('Respuesta incorrecta para el token.');
        }


        //$segmentotext = $customer['segment']->__toString();
        $segmentotext = $customer['segment'];

        $segment = $this->getSegment($segmentotext);


        if (!($segment instanceof Segment)) {
            throw new AuthenticationException('Segmento no encontrado para el token.');
        }
         //$codetext = $customer['code']->__toString();
         $codetext = $customer['code'];

        $user = $this->em->getRepository('AppBundle:Customer')
                        ->findOneByCode($codetext);

        if (!$user) {
            $user = new Customer();
        }

        $user->setCode($customer['code']);
        $user->setName($customer['name']);
        $user->setLastname1($customer['lastname1']);
        $user->setLastname2($customer['lastname2']);
        $user->setAddressEmail($customer['addressEmail']);

        // $user->setAddressState($customer['addressState']);
        // $user->setCodState($customer->getCodState());
        $user->setAddress($customer['address']);
        // $user->setCodCity($customer->getCodCity());
        $user->setAddressPhone($customer['addressPhone']);
        $user->setAddressCellphone($customer['addressCellphone']);
        $user->setAddressEmail($customer['addressEmail']);
        $user->setBirthday($customer['birthday']);

        $user->setSegment($segment);
        $user->setToken($token);
        $user->setCreditCards($customer['creditCards']);

        $this->em->persist($user);
        $this->em->flush($user);

        return new PreAuthenticatedToken(
            $user,
            $token,
            $providerKey,
            $user->getRoles()
        );
    }

    /**
     * Getter para el segmento del cliente que entrega el banco
     *
     * @return $segment
     */
    protected function getSegment($segment)
    {

        return $this->em->getRepository('AppBundle:Segment')->findByName($segment);

    }

    /**
     * {@inheritDoc}
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return new Response('Authentication Failed.', 403);
    }
}
