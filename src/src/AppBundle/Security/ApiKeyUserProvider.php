<?php

namespace AppBundle\Security;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\SessionUnavailableException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Exception;

use AppBundle\Entity\Customer;

/**
 * {@inheritDoc}
 */
class ApiKeyUserProvider implements UserProviderInterface
{
    protected $container;
    private $em;
    private $router;

    /**
     * Constructor
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->em = $this->container->get('doctrine')->getManager();
    }

    /**
     * {@inheritDoc}
     */
    public function getUsernameForApiKey($apiKey)
    {

        // $logger = $this->container->get('logger');
        // $logger->info("\n\n\n");
        // $logger->info(date('Y-m-d H:i:s u').' - Llamadas a WS Inicio');

        $session = $this->container->get('session');
        $session->migrate(true, 0);

        $session->get('tiemposesion');

        $tiempolimite =  $this->em->getRepository('AppBundle:Config')->findOneById(1)->getValue();

		
        if((time() - $session->get('tiemposesion')) > ($tiempolimite*1)){
            $session->invalidate();
			
			throw new SessionUnavailableException('Ha finalizado el tiempo de la sesion.');
			
            return false;
        }else{
             $session->set('tiemposesion', time());
        
        }

		
        if ($session->get('esvalido')){
            $RutUsuario = $session->get('rut');            
            $Segmento= $session->get('segmento');
        }else{
			if(!$session->get('esvalido_dp')){
                    return false;
            }

			$tokenSession = $this->container->get('token_session');
			
			//$tokenSession = $this->container->get('token_session_dummy');
			
			// $logger->info(date('Y-m-d H:i:s u').' - Llamadas a Token Fin');
          /***********Pruebas**********++ */
              // $apiKey=$tokenSession->getGeneraToken();
           /**/

                if (!$tokenSession->isValido($apiKey)) {
                    return false;
                }
                $token = $tokenSession->isValido($apiKey);

                if($token['codRespuesta']=='0000'){
                    $validtoken=true;
                    $xml = @simplexml_load_string($token['key']);
                    $rut = substr_replace($xml->userID, '-', 10, 0);
                    $rut = str_pad($rut, 11, "0", STR_PAD_LEFT);

                    //$RutUsuario=$xml->RutUsuario;
                    $RutUsuario=$rut;

                    //$Segmento=$xml->Segmento;
                    if ($xml->Segmento == 'N')
                        $Segmento='personas';
                    elseif ($xml->Segmento == 'S')
                        $Segmento='select';
                    else
                        $Segmento='banefe';
                }else{
                    $validtoken=false;
                    return false;
                }
        }

        if(!$session->get('person')){
			
            // $logger->info(date('Y-m-d H:i:s u').' - Llamadas a person_session Inicio');
            //$person = $this->container->get('person_session_dummy');
            $person = $this->container->get('person_session');
            // $logger->info(date('Y-m-d H:i:s u').' - Llamadas a person_session Fin');
            // $logger->info(date('Y-m-d H:i:s u').' - Llamadas a cruceprod_session Inicio');
			
            $cruce = $this->container->get('cruceprod_session');
            //$cruce = $this->container->get('cruceprod_session_dummy');
            // $logger->info(date('Y-m-d H:i:s u').' - Llamadas a cruceprod_session Fin');
            //DataPower 
			/**
			** Obtenemos los datos 
			**/
        if($session->get('apikeyuserprovider_dp')){
              
                $session->get('rut');
                $RutUsuario=$session->get('rut');;
                $Segmento='personas';
                $person = $person->getUser($RutUsuario,$Segmento);                    
                $person['creditCards'] = $cruce->getCruceprod($RutUsuario,$Segmento);
                $session->set('person', $person);	
				
			
            }else{
        
                $person = $person->getUser($RutUsuario,$Segmento);       
                $person['creditCards'] = $cruce->getCruceprod($RutUsuario,$Segmento);
                $session->set('person', $person);
	
    
            }
        }
        else{
            $person = $session->get('person');   
        }


        // $logger->info(date('Y-m-d H:i:s u').' - Llamadas a WS Fin');
        return $person;
    }

    /**
     * {@inheritDoc}
     */
    public function loadUserByUsername($username)
    {
        return $username;
    }

    /**
     * {@inheritDoc}
     */
    public function refreshUser(UserInterface $user)
    {
        throw new UnsupportedUserException();
    }

    /**
     * {@inheritDoc}
     */
    public function supportsClass($class)
    {
        return 'Symfony\Component\Security\Core\User\User' === $class;
    }
 
    /**
     * {@inheritDoc}
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return new Response('Authentication Failed.', 403);
    }
}
