<?php

namespace AppBundle\TokenSantander;

/**
 * Class map para reponse de SOAP
 */
class ValidarTokenResponse
{
    public $codRespuesta = 1;
    public $codRespuestaSpecified = 2;
    public $descRespuesta = 3;
    public $descRespuestaSpecified = 4;
    public $token = 1;
    public $tokenSpecified = 3;

    public function __construct()
    {
        // dump($this);
    }
}
