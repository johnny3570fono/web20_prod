<?php

namespace AppBundle;

use AppBundle\AppSoapClient;

use Symfony\Bridge\Monolog\Logger;
/**
 * Webservice SOAP
 */
class PersonSantander extends AppSoapClient
{
     private $usuario_in;
     private $logger;
     public function __construct($wsdl, array $options = [], Logger $logger )
    {
          $this->logger = $logger;
          $this->usuario_in = $options['usuario_in'];
        
        parent::__construct($wsdl, $options);
    }

    public function getUser($rutuser,$segmento)
    {

        
        $usuario_in=$this->usuario_in;
       
       
      /*  $segment = '';
        $segments = ['personas', 'select', 'banefe'];*/
      /*  if(isset($_REQUEST['segmento']))
            $segment = @$_REQUEST['segmento'];
        else
            $segment = $segments[rand(0, 2)];*/

/*
        $req = ['INPUT' => ['RUTASEGURADO' => $user]];
        $resp = $this->Nuevos_Datos_Cliente($req);

        return [
            'name' => $resp->OUTPUT->ESCALARES->NOMASEGURADO,
            'lastname1' => $resp->OUTPUT->ESCALARES->APELLIPATASEG,
            'lastname2' => $resp->OUTPUT->ESCALARES->APELLIMATASEG,
            'birthday' => new \DateTime($resp->OUTPUT->ESCALARES->FECHANACIMIENTOASERG),
            'address' => $resp->OUTPUT->ESCALARES->DIRECASEG,
            'addressCellphone' => $resp->OUTPUT->ESCALARES->FONOCELASEGU,
            'addressPhone' => $resp->OUTPUT->ESCALARES->FONOPARASEGU,
            'addressEmail' => $resp->OUTPUT->ESCALARES->EMAILASEGU,
            'code' => '15459610-0',
            'segment' => $segment,
            'creditCards' => [
                (object)['name' => 'Visa', 'cardNumber' => '89726363874412', 'id' => 1],
                (object)['name' => 'MasterCard', 'cardNumber' => '47412896376238', 'id' => 0],
            ]
        ];
*/


        
        $user = str_replace(".", "", $rutuser);
        $user = str_replace("-", "", $user);
        $user = str_pad($user, 11, "0", STR_PAD_LEFT);
        
        $user = strtoupper($user);
        //$user = "0014096803K";
        $this->error( 'Servicio PersonSantander Variable user- ' ."$user");
        $this->error( 'Servicio PersonSantander Variable usuario_in- ' ."$usuario_in"); 
        $this->error( 'LLamado al Servicio SOAP Nuevos_Datos_Cliente_Online_Lite'); 
      
        $req = ['INPUT' => ['TXNAME_IN' => '', 'USUARIO_IN' => $usuario_in, 'TERMINAL_IN' => '', 'CANAL_IN' => '', 'FILLER_IN' => '', 'PENUMPER_IN' => '', 'PETIPDOC_IN' => 'R', 'PENUMDOC_IN' => $user]];
      $resp = $this->Nuevos_Datos_Cliente_Online_Lite($req);

        //var_dump($resp);
        //die();

        return [
            'name' => $resp->OUTPUT->ESCALARES->PENOMPE_R_OUT,
            'lastname1' => $resp->OUTPUT->ESCALARES->PEPRIAP_R_OUT,
            'lastname2' => $resp->OUTPUT->ESCALARES->PESEGAP_R_OUT,
            'birthday' => new \DateTime($resp->OUTPUT->ESCALARES->PEFECNA_R_OUT),
            'address' => $resp->OUTPUT->ESCALARES->PEDOMANT_R_OUT,
            //'addressCellphone' => $resp->OUTPUT->ESCALARES->PENUMTEL_R_OUT,
            'addressCellphone' => $resp->OUTPUT->ESCALARES->PEPRETEL_R_OUT.' '.$resp->OUTPUT->ESCALARES->PENUMTEL_R_OUT,
            //'addressPhone' => $resp->OUTPUT->ESCALARES->PENUMTEL_R_OUT,
            'addressPhone' => '',
            'addressEmail' => $resp->OUTPUT->ESCALARES->EMAIL_OUT,
            'gene' => $resp->OUTPUT->ESCALARES->PESEXPE_R_OUT,
            'code' => $rutuser,
            'segment' => $segmento
        ];





    }

   /**
     * Atajo para loggear
     *
     * @param string $message
     */
    protected function error($message)
    {
        $this->logger->error(__CLASS__ . ' - ' . $message);
    }

}
