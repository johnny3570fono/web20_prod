<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use AppBundle\Entity\Community;

class ComunaCiudadCommand extends ContainerAwareCommand
{
	protected function configure()
    {
        $this
            ->setName('data:comunaciudad')
            ->setDescription('Load al states and cities by SP')
            ->addArgument(
                'process_log',
                InputArgument::OPTIONAL,
                'True to show what the hell we are doing!'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
    	$resultado = $this->getContainer()->get('sp.comuna_ciudad')->getComunaCiudad();

    	//SELECT * FROM COMMUNITY WHERE codigo_comuna_homologo < 5;
    	if(count($resultado) == 0){
    		$output->writeln("error -  no se obtubieron comunas");
    	}else{
    		$em = $this->getContainer()->get('doctrine')->getManager();

			$output->writeln("cantidad de resultados a procesar : ".count($resultado));
			$output->writeln("procesando...");

			$actualizados = 0;
			$creados = 0;

    		for($i = 0;$i < count($resultado);$i++){
    			if($input->getArgument('process_log') == true){
    				$output->writeln("log - procesando comuna: ".$i);
    			}
    			$codigo_comuna_homologo = $resultado[$i]["CODCOM"];
    			$descripcion_comuna_homologo = utf8_encode(trim($resultado[$i]["DESCOM"]));
    			$codigo_cuidad_homologo = $resultado[$i]["CODCIU"];
    			$descripcion_cuidad_homologo = utf8_encode(trim($resultado[$i]["DESCIU"]));
    			$dql = 'SELECT c FROM AppBundle:Community c WHERE c.codigo_comuna_homologo = :codigo_comuna_h AND c.codigo_cuidad_homologo = :codigo_cuidad_h';

    			$exists = $em->createQuery($dql)
                    ->setParameter('codigo_comuna_h', $codigo_comuna_homologo)
                    ->setParameter('codigo_cuidad_h', $codigo_cuidad_homologo)
                    ->getResult();

                if (isset($exists[0]) && $exists[0] instanceof Community) {
                    $entity = $exists[0];
                    $actualizados ++;
                } else {
                    $entity = new Community();
                    $creados ++;
                }

                $entity->setCodigoComunaHomologo($codigo_comuna_homologo);
                $entity->setDescripcionComunaHomologo($descripcion_comuna_homologo);
                $entity->setCodigoCuidadHomologo($codigo_cuidad_homologo);
                $entity->setDescripcionCuidadHomologo($descripcion_cuidad_homologo);

                $em->persist($entity);
                $em->flush();
	    	}
	    	$output->writeln("proceso finalizado! - ".($i)." comunas procesadas");
	    	$output->writeln("info - ".($creados)." comunas creadas");
	    	$output->writeln("info - ".($actualizados)." comunas actualizadas");	    	
    	}
    }
}