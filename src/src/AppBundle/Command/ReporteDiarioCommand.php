<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Command\Conciliacion\ConciliacionWriter;
use AppBundle\Entity\Insurance;


use Goutte\Client;

/**
 * Clase para extraer valores de la UF
 */
class ReporteDiarioCommand extends ContainerAwareCommand {

    private $config;
    protected $defaultName;

    protected function configure() {
        parent::configure();
        $defaultName = $this->defaultName;

        $this->setName('report:generate')
                ->setDescription('Genera archivos de ventas diarias para Zurich')
                ->addArgument(
                        'fecha', InputArgument::OPTIONAL, 'Fecha de creación de los seguros a conciliar (Y-m-d)'
                )->addArgument(
                'hora', InputArgument::OPTIONAL, 'Hora y minutos de creación de los seguros a conciliar (H:i)'
        )->addArgument(
                'fecha_hasta', InputArgument::OPTIONAL, 'Fecha hasta de creación de los seguros a conciliar (Y-m-d)'
        )->addArgument(
                'hora_hasta', InputArgument::OPTIONAL, 'Hora y minutos hasta de creación de los seguros a conciliar (H:i)'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->config = $this->getContainer()->getParameter('reportes');
        $tipoarchivo = $this->config['ventas_op'];
        $path = $tipoarchivo['source_file'];
        $prefix = $tipoarchivo['prefix'];

        $fecha = $input->getArgument('fecha'); //fecha inicial
        $fecha_hasta = $input->getArgument('fecha_hasta'); // fecha hasta
        $fecha_archivo = new \DateTime('now');

        if ($fecha) {

            $fecha = $this->getFecha($input->getArgument('fecha'), $input->getArgument('hora'));
        }
        if ($fecha_hasta) {
            $fecha_hasta = $this->getFecha($input->getArgument('fecha_hasta'), $input->getArgument('hora_hasta'));
        }
        $filename = sprintf('%s%s.txt', $prefix, $fecha_archivo->format('Ymd'));
        $types = array_diff(Insurance::getTypes(), ['none']);

        /* buscar datos */
        $repository = $this->getContainer()->get('doctrine')
                ->getManager()
                ->getRepository('AppBundle:Insurance');
        $rows = $repository->findAllIssued($fecha, $fecha_hasta);
       

        /* Envio FTP Tentados */

        //$ftp_server2 = 'ftp://usr_seguros_tentados:usr_seguros_tentados@pdpto28:8085';
        $ftp_server2 = 'pdptobdbcm03.cl.bsch';

        $destination_file2 = '/VentasOPWEB20/' . $filename;
        $ftp_user = 'usr_ftp_tentwebseg';
        $ftp_pass = 'T7e5n3w9e5b1seg';

        $conn_id2 = ftp_connect($ftp_server2, '8085');
        $login_result2 = ftp_login($conn_id2, $ftp_user, $ftp_pass);

        if ((!$conn_id2) || (!$login_result2)) {
            echo "Conexión al FTP con errores!";
            echo "Intentando conectar a $ftp_server2";
            exit;
        } else {
            echo "Conectado a $ftp_server2 \n";
        }

        ftp_pasv($conn_id2, true);

        $upload = ftp_put($conn_id2, $destination_file2, $filaname, FTP_BINARY);
        $filename = sprintf('%s%s.txt', $prefix, $fecha_archivo->format('Ymd'));
        if (!$upload) {
            echo "Error al subir el archivo!";
        } else {
            echo "Archivo $filename se ha subido exitosamente a $ftp_server2 en $destination_file2 \n";
        }

        ftp_close($conn_id2);
    }

    private function getFecha($day, $hour) {
        $tz = new \DateTimeZone('America/Santiago');

        if (strtolower($day) == 'now' || !$day) {
            $fecha = new \DateTime('now', $tz);
        } else {
            if (!$hour || !preg_match('/^[0-2][0-9]:[0-5][0-9]$/', $hour)) {
                $hour = '00:00:00';
            }

            $fecha = new \DateTime($day . ' ' . $hour, $tz);
        }

        return $fecha;
    }

}
