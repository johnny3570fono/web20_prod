<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Goutte\Client;
use AppBundle\Entity\Uf;

/**
 * Clase para extraer valores de la UF
 */
class UnidadFomentoCommand extends ContainerAwareCommand
{
    const PATTERN_URL = 'http://www.sii.cl/pagina/valores/uf/uf%year%.htm';

    protected function getUrlScraping($year)
    {
        return str_replace('%year%', $year, self::PATTERN_URL);
    }

    protected function configure()
    {
        parent::configure();

        $this->setName('uf:scrap')
            ->setDescription('Rescata desde el sitio www.sii.cl los valores de la uf')
            ->addArgument(
                'year',
                InputArgument::OPTIONAL,
                '¿ Año de los valores a extraer ?'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $year = (int) $input->getArgument('year') < 1990
                ? date('Y')
                : (int) $input->getArgument('year');

        $client = new Client();
        $crawler = $client->request('GET', $this->getUrlScraping($year));

        $data = [];
        $results = $crawler->filter('div > table > tbody > tr > td')->each(function ($node) use (&$data) {
            $data[] = $node->text();
        });

        $em = $this->getContainer()->get('doctrine')->getManager();
        $day = 1;
        for ($i = 0; $i < count($data); $i = $i + 12) {
            $ufs = array_slice($data, $i, 12);

            foreach ($ufs as $month => $uf) {
                $month = $month + 1;
                $num = $this->valueToDecimal($uf);

                if (checkdate($month, $day, $year)) {
                    $date = sprintf('%04d-%02d-%02d', $year, $month, $day);

                    $dql = 'SELECT u FROM AppBundle:Uf u WHERE u.day = :day';

                    $exists = $em->createQuery($dql)
                        ->setParameter('day', new \DateTime($date))
                        ->getResult();

                    if (isset($exists[0]) && $exists[0] instanceof Uf) {
                        $entity = $exists[0];
                    } else {
                        $entity = new Uf();
                        $entity->setDay(new \DateTime($date));
                    }

                    $entity->setValue($num);
                    $em->persist($entity);
                }
            }
            $day++;
        }
        $em->flush();

        $output->writeln('Año ' . $year . ' guardado.');
    }

    private function valueToDecimal($num)
    {
        $num = str_replace('.', '', $num);

        return (float) trim(str_replace(',', '.', $num));
    }
}
