<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


use AppBundle\Entity\Uf;

class UfLoadBySpCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('uf:loadsp')
            ->setDescription('Load UF from a store procedure and do a insert in uf entity')
            ->addArgument(
                'fecha',
                InputArgument::OPTIONAL,
                'Get a uf by date [YYYY/MM/DD]'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $resultado = $this->getContainer()->get('sp.uf')->getUf( ($input->getArgument('fecha') != null ? $input->getArgument('fecha') : null)  );

        if($resultado["valor"] > 0){

            $em = $this->getContainer()->get('doctrine')->getManager();

            $parsed_date_d = date('d',strtotime($resultado["query_date"]));
            $parsed_date_m = date('m',strtotime($resultado["query_date"]));
            $parsed_date_y = date('Y',strtotime($resultado["query_date"]));

            $parsed_valor = $resultado["valor"];
            
            if (checkdate($parsed_date_m, $parsed_date_d, $parsed_date_y)) {

                $date = sprintf('%04d-%02d-%02d', $parsed_date_y, $parsed_date_m, $parsed_date_d);

                $dql = 'SELECT u FROM AppBundle:Uf u WHERE u.day = :day';

                $exists = $em->createQuery($dql)
                    ->setParameter('day', new \DateTime($date))
                    ->getResult();

                if (isset($exists[0]) && $exists[0] instanceof Uf) {
                    $entity = $exists[0];
                } else {
                    $entity = new Uf();
                    $entity->setDay(new \DateTime($date));
                }

                $entity->setValue($parsed_valor);
                $em->persist($entity);
                $em->flush();

                $output->writeln("actualizacion de uf enviada [valor:".$parsed_valor.",fecha:".$resultado["query_date"]."]");
            }else{
                $output->writeln("error -  fecha invalida " + $date);
            }
        }else{
            $output->writeln("error -  not uf found");
        }
    }
}