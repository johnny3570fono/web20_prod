<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Command\Conciliacion\ConciliacionWriter;
use AppBundle\Entity\Insurance;
use AppBundle\Entity\UserActivity;


use Goutte\Client;

/**
 * Clase para extraer valores de la UF
 */
class TentadoCommand extends ContainerAwareCommand {

    private $config;
    protected $defaultName;

    protected function configure() {
        parent::configure();
        $defaultName = $this->defaultName;

        $this->setName('tentado:generate')
                ->setDescription('Genera archivos de ventas diarias para Zurich')
                ->addArgument(
                        'fecha', InputArgument::OPTIONAL, 'Fecha de creación de los seguros a conciliar (Y-m-d)'
                )->addArgument(
                'hora', InputArgument::OPTIONAL, 'Hora y minutos de creación de los seguros a conciliar (H:i)'
        )->addArgument(
                'fecha_hasta', InputArgument::OPTIONAL, 'Fecha hasta de creación de los seguros a conciliar (Y-m-d)'
        )->addArgument(
                'hora_hasta', InputArgument::OPTIONAL, 'Hora y minutos hasta de creación de los seguros a conciliar (H:i)'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->config = $this->getContainer()->getParameter('reportes');
        $tipoarchivo = $this->config['tentadoscss'];
        $path = $tipoarchivo['source_file'];
        $prefix = $tipoarchivo['prefix'];

        $fecha = $input->getArgument('fecha');
        $hora = $input->getArgument('hora'); //fecha inicial
        $fecha_hasta = $input->getArgument('fecha_hasta');
        $hora_hasta = $input->getArgument('hora_hasta'); //fecha inicial
        $fecha_archivo = new \DateTime('now');


        if ($fecha) {
            $fecha = $this->getFecha($input->getArgument('fecha'), $input->getArgument('hora'));
        }
        if ($fecha_hasta) {
            $fecha_hasta = $this->getFecha($input->getArgument('fecha_hasta'), $input->getArgument('hora_hasta'));
        }



        $types = array_diff(Insurance::getTypes(), ['none']);
        /* buscar datos */
        $repository = $this->getContainer()->get('doctrine')->getManager()->getRepository('AppBundle:Insurance');
        $rows = $repository->tentadoBorrador($fecha, $fecha_hasta);

        $repository2 = $this->getContainer()->get('doctrine')->getManager()->getRepository('AppBundle:UserActivity');
        $rows2 = $repository2->tentadoBorradorActivity($fecha, $fecha_hasta);

        $result = array_merge($rows, $rows2);
        //Para el Correo no manda
        //Iniciamos 

        if (count($result) < 1) {
            $output->writeln('Seguros para el día solicitado no existen.');
        } else {

            $filaname = sprintf('%s/%s%s.txt', $path, $prefix, $fecha_archivo->format('Ymd_Hi'));  //con ruta path
            
            $filename = sprintf('%s%s.txt', $prefix, $fecha_archivo->format('Ymd_Hi'));   // sólo nombre de archivo

            $i = 2;
            $final = [];

            if ($rows) {
                 foreach ($rows as $row) {

                    $fechaactual = date("Ymd");
                    $fecha = $row->getCreated();
                  //  $rutcontratante = $row->getInsured()->getCode();
                    $rutcontratante = substr(trim($row->getInsured()->getCode()), 0, -2);
                    $dv = substr(trim($row->getInsured()->getCode()), -1);
                    $producto = $row->getTitle();
                    $telefono = $row->getInsured()->getAddressCellphone();
                    $nombre = $row->getInsured()->getName();
                    $apellido = $row->getInsured()->getLastName1();
                    $email = $row->getInsured()->getAddressEmail();

                    $writer = new \AppBundle\Command\Conciliacion\ReportGenerator(fopen($filaname, 'w'));

                    $line['1'] = "Rut;Dv;nombre;Apellido;Email;Telefono;Nombre producto;fecha visualizacion;Estado" . "\r\n";
                    $line[$i] = ltrim($rutcontratante, "0") . ";" .$dv. ";" . trim($nombre) . ";" . trim($apellido) . ";" . trim($email) . ";" . $this->emptyPhone($telefono) . ";" . trim($producto) . ";" . date_format($fecha, "Y-m-d;H:i:s") . ";" . 'Sin culminar' . "\r\n";                                        
               
                    $writer->writeItem($line);
                    $i++;
                }
             }
     
            $datos = [];
            foreach ($rows2 as $row) {
                //Variable del Archivo
                $fechaactual = date("Ymd");
                $fecha = $row->getCreatedAt();
              //  $rutcontratante = $row->getUsername();
                $rutcontratante = substr(trim($row->getUsername()), 0, -2);
                $dv = substr(trim($row->getUsername()), -1);
                $repository = $this->getContainer()->get('doctrine')->getManager()->getRepository('AppBundle:Customer');
                $resp = $repository->findBy(array('code' => $rutcontratante));
                foreach ($resp as $r) {
                    $telefono = $r->getAddressCellphone();
                    $nombre = $r->getName();
                    $apellido = $r->getLastName1();
                    $email = $r->getAddressEmail();
                }
                $codProd = substr($row->getUrl(), -2);
                $repository = $this->getContainer()->get('doctrine')->getManager()->getRepository('AppBundle:Product');
                $resp2 = $repository->findBy(array('id' => $codProd));
                foreach ($resp2 as $r) {
                    $producto = $r->getTitle();
                }

                $momentaneo = [
                    'rut' => $rutcontratante,
                    'dv'  => $dv,
                    'telefono' => $telefono,
                    'producto' => $producto,
                    'fecha' => $fecha,
                    'nombre' => $nombre,
                    'apellido' => $apellido,
                    'email' => $email,
                ];

                
                $datos[$i] = $momentaneo;
                $i++;
            }

            //$i++;

            foreach ($datos as $key => $r) {
                foreach ($rows as $q) {
                    if ($r['rut'] == $q->getInsured()->getCode() && $r['producto'] == $q->getTitle())
                        unset($datos[$key]);
                }
            }
            foreach ($datos as $r) {
                $writer = new \AppBundle\Command\Conciliacion\ReportGenerator(fopen($filaname, 'w'));
                if (count($rows < 1)) {
                    $line['1'] = "Rut;Dv;Nombre;Apellido;Email;Telefono;Nombre producto;Fecha Visualizacion;Hora;Estado" . "\r\n";
                }
                
                $line[] = trim(ltrim($r['rut'], "0")) . ";".$r['dv'].";" . trim($r['nombre']) . ";" . trim($r['apellido']) . ";" . trim($r['email']) . ";" . $this->emptyPhone( $r['telefono'] ) . ";" . trim($r['producto']) . ";" . date_format($r['fecha'], "Y-m-d;H:i:s") . ";" . 'Visitado' . "\r\n";
                $writer->writeItem($line);
            }

            $writer->finish();
            $output->writeln('Generación de archivos en path: "' . $filaname . '" ');




            /* generar arreglo nuevo con datos de los dos, iterar y validar, luego escribir archivo 
              varificar title y name de productos
             */

            /*             * ****************************************************************************
             * ******************************** FTP ***************************************
             * ***************************************************************************** */
            /* Envio FTP */

            /* $ftp_server = $this->config['ftp_server'];
              $ftp_user = $this->config['ftp_user'];
              $ftp_pass = $this->config['ftp_pass'];


              $destination_file = $this->config['destination_file'] . $filename;


              $conn_id = ftp_connect($ftp_server);

              $login_result = ftp_login($conn_id, $ftp_user, $ftp_pass);


              if ((!$conn_id) || (!$login_result)) {
              echo "Conexión al FTP con errores!";
              echo "Intentando conectar a $ftp_server for user $ftp_user";
              exit;
              } else {
              echo "Conectado a $ftp_server, for user $ftp_user \n";
              }


              ftp_pasv($conn_id, true);



              $upload = ftp_put($conn_id, $destination_file, $filaname, FTP_BINARY);

              if (!$upload) {
              echo "Error al subir el archivo!";
              } else {
              echo "Archivo $filename se ha subido exitosamente a $ftp_server en $destination_file \n";
              }

              ftp_close($conn_id); */

            /* Envio FTP Tentados */

            //$ftp_server2 = ' ftp://usr_seguros_tentados:usr_seguros_tentados@pdpto28:8085';
            //$destination_file2 = $this->config['destination_file'] . $filename; 

            //$ftp_server2 = '180.122.209.73';
            $ftp_server2 = 'pdptobdbcm03.cl.bsch';
			$destination_file2 = '/TentadosCSSWEB20/' . $filename;
          //  $ftp_user = 'usr_seguros_tentados';
          //  $ftp_pass = 'usr_seguros_tentados';
			$ftp_user = 'usr_ftp_tentwebseg';
			$ftp_pass = 'T7e5n3w9e5b1seg'; 

            $conn_id2 = ftp_connect($ftp_server2, '21');
            $login_result2 = ftp_login($conn_id2, $ftp_user, $ftp_pass);

            if ((!$conn_id2) || (!$login_result2)) {
                echo "Conexión al FTP con errores!";
                echo "Intentando conectar a $ftp_server2";
                exit;
            } else {
                echo "Conectado a $ftp_server2 \n";
            }


            ftp_pasv($conn_id2, true);

            $upload = ftp_put($conn_id2, $destination_file2, $filaname, FTP_BINARY);
            $filename = sprintf('%s%s.txt', $prefix, $fecha_archivo->format('Ymd'));
            if (!$upload) {
                echo "Error al subir el archivo!";
            } else {
                echo "Archivo $filename se ha subido exitosamente a $ftp_server2 en $destination_file2 \n";
            }

            ftp_close($conn_id2);
        }
    }

//Fin del Else

    private function getFecha($day, $hour) {
        $tz = new \DateTimeZone('America/Santiago');

        if (strtolower($day) == 'now' || !$day) {
            $fecha = new \DateTime('now', $tz);
        } else {
            if (!$hour || !preg_match('/^[0-2][0-9]:[0-5][0-9]$/', $hour)) {
                $hour = '00:00:00';
            }

            $fecha = new \DateTime($day . ' ' . $hour, $tz);
        }

        return $fecha;
    }

    private function emptyPhone($phone) {
        $phone = str_replace(" ","", $phone);
        
        return $phone;
    }

}
