<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Command\Conciliacion\ConciliacionWriter;
use AppBundle\Entity\Insurance;
use AppBundle\Entity\ParentescoCodigo;
use Goutte\Client;
use AppBundle\Entity\Uf;

/**
 * Clase para extraer valores de la UF
 */
class ConciliacionCommand extends ContainerAwareCommand
{
	private $config;
	protected $defaultName;


	protected function configure()
	{
		parent::configure();
		 $defaultName = $this->defaultName;

		$this->setName('conciliacion:generate')
			->setDescription('Genera archivos de concilacion para Zurich')
			->addArgument(
				'path',
				InputArgument::OPTIONAL,
				'Directorio donde crear los archivos'
			)->addArgument(
				'fecha',
				InputArgument::OPTIONAL,
				'Fecha de creación de los seguros a conciliar (Y-m-d)'
			)->addArgument(
				'hora',
				InputArgument::OPTIONAL,
				'Hora y minutos de creación de los seguros a conciliar (H:i)'
			)->addArgument(
				'fecha_hasta',
				InputArgument::OPTIONAL,
				'Fecha hasta de creación de los seguros a conciliar (Y-m-d)'
			)->addArgument(
				'hora_hasta',
				InputArgument::OPTIONAL,
				'Hora y minutos hasta de creación de los seguros a conciliar (H:i)'
			);
	}


	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$this->config = $this->getContainer()->getParameter('conciliacion');
               
        $parentesco_codigo = $this->getContainer()->get('doctrine')
                        ->getManager()->getRepository('AppBundle:ParentescoCodigo')
                        ->createQueryBuilder('e')->select('e')->getQuery()
                        ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);                                                                                                                         
		$archivosjuntos=array('life'=>'life','health'=>'health','travel'=>'travel');

		$path=$this->config['source_file'];
		$path = $this->getPath($path); 

		$fecha=$input->getArgument('fecha'); //fecha inicial

		$fecha_hasta=$input->getArgument('fecha_hasta'); // fecha hasta
		$fecha_archivo = new \DateTime('now');

		if($fecha){
			$fecha = $this->getFecha($input->getArgument('fecha'), $input->getArgument('hora'));
		}
		if($fecha_hasta){
			$fecha_hasta = $this->getFecha($input->getArgument('fecha_hasta'), $input->getArgument('hora_hasta'));
		}
		
		$types = array_diff(Insurance::getTypes(), ['none']);

		/* buscar datos */
		$repository = $this->getContainer()->get('doctrine')
							->getManager()
							->getRepository('AppBundle:Insurance');
		$inc=0;
		$arrayarchivos=array();

		foreach($types as $t) {

			if (!isset($this->config[$t])) {
				$output->writeln('Configuración para seguro tipo "' . $t . '" no definida.');
			} else {
				$typeConfig = $this->config[$t];

				if(array_key_exists($t,$archivosjuntos)){
					$inc++;
					if($inc==1){
						$rows = $repository->findAllIssuedByDateTimeAndType($fecha,$fecha_hasta, $archivosjuntos);
					}
				}else{
					$rows = $repository->findAllIssuedByDateTimeAndType($fecha,$fecha_hasta,$t);
				}
				if (count($rows) < 1) {
					$output->writeln('Seguros de tipo "' . $t . '" para el día solicitado no existen.');
				} elseif(($inc==1) or (!array_key_exists($t,$archivosjuntos))) {

					foreach ($typeConfig['files'] as $f) {
						$canDetalle=0;
						$mtoTotal=0;

						$filaname = sprintf('%s/%s%s_%s.txt', $path, $typeConfig['prefix'], $fecha_archivo->format('Ymd'), $f['num']);
						$filename = sprintf('%s%s_%s.txt', $typeConfig['prefix'], $fecha_archivo->format('Ymd'), $f['num']);


						$arrayarchivos[]=$filename;
                                                   

						$methodGenerator = $f['generate'];

						$writer = new $typeConfig['class'](fopen($filaname, 'w'));

						$writer->writeItem($writer->getCabecera($f['generate']));

						foreach ($rows as $row) {

							if($methodGenerator=='rowForArchivo3' ){
								$beneficiarios=$row->getbeneficiaries();
								foreach ($beneficiarios as $beneficiario) {
									$codinsurance=$row->getId();
									$writer->setInsurance($row);
                                    $writer->setParentescoCodigo($parentesco_codigo);                                                   
									$lines = $writer->$methodGenerator();

									foreach($lines as $line) {
										$capital=$beneficiario->getCapital();
										$capital= (string) $capital;

										if($capital!=null){
											$cant=strlen($capital);

											$total=3-$cant;
											$ceros='';
											for($i=0;$i< $total;$i++){
												$ceros.='0';
											}
											$capital=$ceros.$capital;

										}else {
											$capital='000';
										}

										
										$codeonlynumber=(int) $beneficiario->getCodeOnlyNumber();
                						$cedulabenef=$codeonlynumber.$beneficiario->getCodeOnlyDv();
										
										if($cedulabenef!=null){
											$cant=strlen($cedulabenef);

											$total=10-$cant;
											$ceros='';
											for($i=0;$i< $total;$i++){
												$ceros.='0';
											}
											$cedulabenef=$ceros.$cedulabenef;

										} else {
											$cedulabenef='0000000000';
										}

										$reemplazos = array(9 => $capital, 7 => $cedulabenef);
										$line = array_replace($line,$reemplazos);

										$writer->writeItem($line);

									}
									$canDetalle+=count($lines);
								}

							}else{
								$writer->setInsurance($row);
                                $writer->setParentescoCodigo($parentesco_codigo);                                                 
								$lines = $writer->$methodGenerator();

								foreach($lines as $line) {
									$writer->writeItem($line);
								}

								if($methodGenerator=='rowForArchivo1'){
									$mtoTotal+=$row->getPrice();
								}

								$canDetalle+=count($lines);
							}
						}

						$writer->writeItem($writer->rowForTotales($f['generate'],$canDetalle,$mtoTotal));
						$writer->finish();

						$output->writeln('Generación de archivos en path: "' . $filaname . '" ');

					}
				}
			}
		}

		$ftp_server=$this->config['ftp_server'];
		$ftp_user=$this->config['ftp_user'];
		$ftp_pass=$this->config['ftp_pass'];

		// conexión
		$conn_id = ftp_connect($ftp_server); 

		// logeo
		$login_result = ftp_login($conn_id, $ftp_user, $ftp_pass); 

		// conexión
		if ((!$conn_id) || (!$login_result)) { 
			echo "Conexión al FTP con errores!";
			echo "Intentando conectar a $ftp_server for user $ftp_user"; 
			exit; 
		} else {
			echo "Conectado a $ftp_server, for user $ftp_user \n";
		}

		// archivo a copiar/subir

                                      

                                   


		ftp_pasv($conn_id, true);
		var_dump($arrayarchivos);
		foreach($arrayarchivos as $t) {
                    


			$source_file=$this->config['source_file'].$t;
			$destination_file=$this->config['destination_file'].$t; 
                            
                                
                            
                           
                                     
                               

			$upload = ftp_put($conn_id, $destination_file, $source_file, FTP_BINARY);

			if (!$upload) {
				echo "Error al subir el archivo!";
			} else {
				echo "Archivo $t se ha subido exitosamente a $ftp_server en $destination_file \n";
                  
			}

                              
		}
		
		ftp_close($conn_id);

	}

	private function getPath($path)
	{

		if (!$path) {
			return sys_get_temp_dir();
		}

		if (!is_dir($path)) {
			throw new \Exception("Path '{$path}' no es un directorio.", 1);
		}

		if (!is_writable($path)) {
			throw new \Exception("Path '{$path}' no se puede escribir.", 1);
		}

		return realpath($path);
	}

	private function getFecha($day, $hour)
	{
		$tz = new \DateTimeZone('America/Santiago');

		if (strtolower($day) == 'now' || !$day) {
			$fecha = new \DateTime('now', $tz);
		} else {
			if (!$hour || !preg_match('/^[0-2][0-9]:[0-5][0-9]$/', $hour)) {
				$hour = '00:00:00';
			}

			$fecha = new \DateTime($day . ' ' . $hour, $tz);
		}

		return $fecha;
	}

}
