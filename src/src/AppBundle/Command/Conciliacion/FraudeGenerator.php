<?php

namespace AppBundle\Command\Conciliacion;

use AppBundle\Entity\Insurance;

class FraudeGenerator extends Generator
{

    public function __construct($stream = null)
    {
        parent::__construct($stream);

        $this->data = array_merge(
            $this->data,
            [
                'codLinea' => ['005', 3],
            ]
        );
    }

    public function rowForArchivo1()
    {
        //$keys = array_merge($this->keysForArchivo1(), ['numCtaTarjAseg']);
        $keys = $this->keysForArchivo1();

        return [
            $this->padItems($this->getDataRow($keys))
        ];
    }

    public function rowForArchivo2()
    {
        $lines = [];

        $keys = $this->keysForArchivo2();
        $first = $this->padItems($this->getDataRow($keys));

        $keys = [
            'codTipoPersona',  // [  ] Obligatorio - Cod Tipo Persona
            'corTipoPersona',  // [  ] Obligatorio - Cor Tipo Persona
            // Correlativo por tipo de persona "00"
            'rutPersona',      // [  ] Obligatorio - Rut Persona
            'codRelacion',     // [  ] Opcional - Cod Relacion
        ];

        $codeonlynumber=(int) $this->insurance->getPayer()->getCodeOnlyNumber();
        $rut=$codeonlynumber.$this->insurance->getPayer()->getCodeOnlyDv();

         $cant=strlen($rut);

           $total=11-$cant;
           $espacios='';
           for($i=0;$i< $total;$i++){
                    $espacios.='0';
            }
           $rut=$espacios.$rut;

          
        // ASE= Asegurado; CON = Contratante; PAG=Pagador; BEN = Beneficiario
        foreach(['PAG', 'CON'] as $k => $v) {
            $person = $this->padItems(
                [
                    [$v, 3],
                    ['00', 2],
                    [$rut, 11],
                    ['00', 2],
                ]
            );

            $lines[] = array_merge($first, $person);
        }
        /*Busqueda del asegurado*/ 

        $codeonlynumberase=(int) $this->insurance->getInsured()->getCodeOnlyNumber();
        $rutase=$codeonlynumberase.$this->insurance->getInsured()->getCodeOnlyDv();
        /*completar con ceros a la izquierda*/
        $cant=strlen($rutase);
        $total=11-$cant;
        $espacios='';
        for($i=0;$i< $total;$i++){
            $espacios.='0';
        }
        $rutase=$espacios.$rutase;


        $personase = $this->padItems(
        [
            ['ASE', 3],
            ['00', 2],
            [$rutase, 11],
            ['00', 2],
        ]
        );
        $lines[] = array_merge($first, $personase);

        /**/

        return $lines;
    }
    
}
