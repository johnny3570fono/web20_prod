<?php

namespace AppBundle\Command\Conciliacion;

use AppBundle\Entity\Insurance;

class HogarGenerator extends Generator
{
   /* protected $fillers = [
        'cabecera' => [
            'rowForArchivo1' => 173,
            'rowForArchivo2' => 44,
            'rowForArchivo3' => 35,
            'rowForArchivo4' => 206,
            'rowForArchivo5' => 386,
        ],
        'totales' => [
            'rowForArchivo1' => 156,
            'rowForArchivo2' => 27,
            'rowForArchivo3' => 18,
            'rowForArchivo4' => 189,
            'rowForArchivo5' => 369,
        ]
    ];*/

    public function __construct($stream = null)
    {
        parent::__construct($stream);

        $this->data = array_merge(
            $this->data,
            [
                'codLinea' => ['004', 3],
            ]
        );
    }

    public function rowForArchivo1()
    {
        $keys = $this->keysForArchivo1();

        return [
            $this->padItems($this->getDataRow($keys))
        ];
    }

    public function rowForArchivo2()
    {
        $lines = [];

        $keys = $this->keysForArchivo2();
        $first = $this->padItems($this->getDataRow($keys));

        $codeonlynumber=(int) $this->insurance->getPayer()->getCodeOnlyNumber();
        $rut=$codeonlynumber.$this->insurance->getPayer()->getCodeOnlyDv();

         $cant=strlen($rut);

           $total=11-$cant;
           $espacios='';
           for($i=0;$i< $total;$i++){
                    $espacios.='0';
            }
           $rut=$espacios.$rut;

          
        foreach(['PAG', 'CON'] as $k => $v) {
            $person = $this->padItems(
                [
                    [$v, 3],
                    ['00', 2],
                    [$rut, 11],
                    ['00', 2]
                ]
            );

            $lines[] = array_merge($first, $person);
        }

        /*Busqueda del asegurado*/ 

        $codeonlynumberase=(int) $this->insurance->getInsured()->getCodeOnlyNumber();
        $rutase=$codeonlynumberase.$this->insurance->getInsured()->getCodeOnlyDv();
        /*completar con ceros a la izquierda*/
        $cant=strlen($rutase);
        $total=11-$cant;
        $espacios='';
        for($i=0;$i< $total;$i++){
            $espacios.='0';
        }
        $rutase=$espacios.$rutase;


        $personase = $this->padItems(
        [
            ['ASE', 3],
            ['00', 2],
            [$rutase, 11],
            ['00', 2],
        ]
        );
        $lines[] = array_merge($first, $personase);

        /**/

        return $lines;
    }
     public function rowForArchivo3()
    {
        $keys = [
            'codCobertura',
            'pcjCobertura',
            'codTipobene',
        ];

        
        $keys = array_merge($this->keysForArchivo3(),$keys);
       
      
        return [
            $this->padItems($this->getDataRow($keys))
        ];
    }

    public function rowForArchivo5()
    {
        $keys = [
            'tipRegD',
            'fecEnvio',
            'numPropuesta',
            'codTipoCasa',
            'codTipoTipoCasa',
            'codMaterial',
            'rural',
            'industria',
            'agua',
            'antiguedad',
            'dirAseg',
            'codComunaAseg',
            'codCiudadAseg',
            'fonoAseg',
            /*'seguridad',
            'codSeguridad',
            'bomberos',
            'mtoAsegCont',
            'mtoAsegInm',
            'codMascota',
            'razaMascota',
            'nomMascota',
            'codMascota',
            'razaMascota',
            'nomMascota',
            'codMascota',
            'razaMascota',
            'nomMascota',
            'mtoMercaderia',
            'codGiro',
            'contacto',
            'fonoContacto',
            'dscGiro',
            'siniestro',
            'mtoSinIncendio',
            'mtoSinSismo',
            'mtoSinRobo',*/
        ];

        return [
            $this->padItems($this->getDataRow($keys))
        ];
    }

}
