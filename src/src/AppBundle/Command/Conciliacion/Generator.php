<?php

namespace AppBundle\Command\Conciliacion;

use AppBundle\Entity\Insurance;
use AppBundle\Entity\Plan;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class Generator extends ConciliacionWriter
{
    /**
     * Seguro
     */
    protected $insurance;
    protected $Plan;
    protected $parentescoCodigo;                            

    protected $fillers = [
        'cabecera' => [
            'rowForArchivo1' => 133,
            'rowForArchivo2' => 133,
            'rowForArchivo3' => 133,
            'rowForArchivo4' => 133,
            'rowForArchivo5' => 133,
        ],
        'totales' => [
            'rowForArchivo1' => 3,
            'rowForArchivo2' => 3,
            'rowForArchivo3' => 3,
            'rowForArchivo4' => 3,
            'rowForArchivo5' => 3,
        ]
    ];

    protected $data = [
        'tipoTrans'         => ['001000', 6], // Tipo Trans. "001000" X(6)
        'codConv'           => ['242', 3],  // Código de convenio BSAN, X(3)  242
        'codRamo1'          => ['  0', 3],  // Código de Ramo BSAN, X(3)
        'codRamo'           => ['000', 3],  // Código de Ramo BSAN, X(3)
        'codLinea'          => ['000', 3],  // Código de Linea BSAN, X(3), valores(002: AUTOMOTRIZ, 003: VIDA, 004: HOGAR, 005: FRAUDE, 006: CESANTIA)
        'numPoliza'         => ['000000000000', 12],
        'codMoneda'         => ['UF ', 3],
        'primaTotal'        => [null, 12],
     //   'codBancoTarjeta'   => [null, 3],   // Cod Banco/Tarjeta	N(3)
      //  'codSucursal'       => ['INBCO', 5], // Cod Sucursal
      //  'codViaContacto'    => ['EJE', 3],       // X(3)
        'rutbe'             => [null, 10],
        'rutEje'            => ['00000001', 8],
        'dvEje'             => ['9', 1],
        'codEstado'         => [null, 3],
        //'codViaContacto4'   => [' EJE', 4],
        'convenio'          => ['242', 3],   //242/
        'filler12'          => [null, 12],
        'filler'            => [null, 2],
        'filler1'           => [null, 1],
        'filler3'           => [null, 3],
        'codCobertura'      => [null, 3], /**se dijo que es porcentaje*/
        'pcjCobertura'      => [null, 6],
        'codTipobene'       => ['D', 1], // X(1) D: Directo;  C: Contingente
        'dv'                => [null, 1],
        'codDetalle'        => [null, 5],
        'mtnTotal'          => [null, 12],
        'codTipoCasa'       => [null, 3], // 001: Casa; 002: Departamento; 003: En Condominio
        'codTipoTipoCasa'   => [null, 3], // 001: Principal; 002: 2da Vivienda
        'codMaterial'       => [null, 3], // Codigo del Tipo de Material de Construcción
        'rural'             => [null, 1], // Indicador de ubicación rural
        'industria'         => [null, 1], // Indicador de ubicación cerca de Industria
        'agua'              => [null, 1], // Indicador de ubicación cerca de cursos de agua
        'antiguedad'        => [null, 4], // Antigüedad de la vivienda
        'seguridad'         => [null, 1], // Indicador de Sistema de Seguridad
        'codSeguridad'      => [null, 3], // Codigo del sistema de Seguridad
        'bomberos'          => [null, 1], // Indicador de ubicación cerca de bomberos
        'mtoAsegCont'       => [null, 5], // Monto Aseguado Contenido en UF
        'mtoAsegInm'        => [null, 5], // Monto Aseguado Inmueble en UF
        'codMascota'        => [null, 1], // Codigo de Tipo de Mascota
        'razaMascota'       => [null, 20], // Raza de la Mascota
        'nomMascota'        => [null, 20], // Nombre de la Mascota
        'mtoMercaderia'     => [null, 5], // Monto Aseguado Mercadería en UF
        'codGiro'           => [null, 3], // Codigo del Giro
        'contacto'          => [null, 50], // Persona de Contacto
        'fonoContacto'      => [null, 10], // Fono de Contacto
        'dscGiro'           => [null, 50], // Descripción del giro
        'siniestro'         => [null, 1], // Indicador de si ha tenido siniestro
        'mtoSinIncendio'    => [null, 12], // Monto del Siniestro Cobertura Incendio en Pesos
        'mtoSinSismo'       => [null, 12], // Monto del Siniestro Cobertura Sismo en Pesos
        'mtoSinRobo'        => [null, 12], // Monto del Siniestro Cobertura Robo en Pesos
        'emiteKit'	        => [null, 1],
        'superClave'        => [null, 13],
       // 'dpsSucia'          => [null, 1],
       // 'declara'           => [null, 255],
        'tipRegT'           => ['T', 1],
        'tipRegC'           => ['C', 1],
        'tipRegD'           => ['D', 1],
    ];

    /**
     * Data comun para generar el archivo 1 de los seguros
     *
     * @return array
     */
    protected function keysForArchivo1()
    {
        return [
            'tipRegD',
            'fecEnvio',
            'codConv',
            'tipoTrans',
            'codRamo1',
            'codLinea',
            'codProducto',
            'numPropuesta',
            'numPoliza',
            'codPlan',
            'fecVenta',
            'codMoneda',
            'primaCuota',
            'primaTotal',
            'codFrecPago',
            'codBancoTarjeta',
            'numMedPago',
            'codSucursal',
            'codViaContacto',
            'filler3',
            'rutEje',
            'dvEje',
            'filler12',
            'codEstado',
            'codDiaPago',
            'codViaContacto4',
            'codFuente',
            'nomEje',
            'codMedPago',
        ];
    }

    /**
     * Data comun para generar el archivo 2 de los seguros
     *
     * @return array
     */
    protected function keysForArchivo2()
    {
        return [
            'tipRegD',
            'fecEnvio',
            'codRamo',
            'codLinea',
            'codProducto',
            'numPropuesta',
        ];
    }

    /**
     * Busca $name dentro de $data para entregar su valor o verificar si
     * $name es el nombre de un metodo y retornar el resultado de su ejecucion.
     *
     */
    public function __get($name)
    {
        if (array_key_exists($name, $this->data)) {
            return $this->data[$name];
        }

        if (method_exists($this, $name)) {
            return $this->$name();
        }

        return null;
    }

    /**
     * Rellena los item
     *
     * @return array
     */
    protected function padItems(array $item)
    {
        $data = [];

        foreach($item as $v) {
            $data[] = $this->pad($v[0], $v[1]);
        }

        return $data;
    }

    /**
    * Rellena una cadena
    *
    * @return string
    */
    public function pad($value, $pad_length = 1, $pad_string = ' ')
    {
        return str_pad($value, $pad_length, $pad_string, STR_PAD_LEFT);
    }

    /**
     * Ejecuta __get para cada item del array pasado
     *
     * @return array
     */
    public function getDataRow(array $keys)
    {
        $data = [];
        foreach($keys as $k){
            $data[] = $this->$k;
        }

        return $data;
    }

    /**
     * Fec Envio
     *
     * @return string
     */
    public function fecEnvio()
    {
        return [date('Ymd'), 8];
    }



/**
     * codViaContacto
     *
     * @return string
     */
    public function codViaContacto()
    {
        $segmento=($this->insurance->getSegment());
       if ($segmento->getName='Banefe'){
        return ['CBF',3];

       }else{
        return ['EJE',3];
       }
       
         
    }


/**
     * codViaContacto4
     *
     * @return string
     */
    public function codViaContacto4()
    {
        $segmento=($this->insurance->getSegment());
       if ($segmento->getName='Banefe'){
        return [' CBF',4];

       }else{
        return [' EJE',4];
       }
       
         
    }


/**
     * via contacto
     *
     * @return string
     */
    public function codSucursal()
    {
        $segmento=($this->insurance->getSegment());
       if ($segmento->getName='Banefe'){
        return ['INBCO',5];

       }else{
        return ['INBEF',5];
       }
       
         
    }
    /**
     * nombre Ejecutivo
     *
     * @return string
     */
    public function nomEje()
    {
        $nombre='NO DISPONIBLE EN WEB';
      if($nombre!=null){
       $cant=strlen($nombre);

           $total=30-$cant;
           $espacios='';
           for($i=0;$i< $total;$i++){
                    $espacios.=' ';
            }
           $nombre=$espacios.$nombre;
               
               
            return [$nombre,30];

        }else
        {
            return [null,30];
        }
         
    }


    /**
     * Fec viaje
     *
     * @return string
     */
    public function fecVige()
    {
        $date=strtotime($this->insurance->getTravelDate());
      
             
        if(($date!=null)){
           
            return [date('Ymd', strtotime($this->insurance->getTravelDate())),8];

        }else
        {
            return [null,8];
        }
         
    }

    /**
     * prima 
     *
     * @return string
     */
    public function primaCuota()
    {
        $price=$this->insurance->getPrice();
                   
        if(($price!=null)){
           $cant=strlen($price);

           $total=12-$cant;
           $prima='';
           for($i=0;$i< $total;$i++){
                    $prima.='0';
            }
           $primaCuota=$prima.$price;
               
               
            return [$primaCuota,12];

        }else
        {
            return [null,12];
        }
    }

    /**
     *dpsSucia 
     *
     * @return string
     */
    public function dpsSucia()
    {
        $declara=$this->insurance->getEnfermedad();
       
        if(($declara==null ) or ($declara=='no')){
            return ['N',1];
        }else{

            return ['S',1];
        }
             
    }

    /**
     * Primer registro de los archivos a generar.
     *
     * @return array
     */
    public function getCabecera($filename)
    {
        $filler = 0;

        if (array_key_exists($filename, $this->fillers['cabecera'])) {
            $filler = (int)$this->fillers['cabecera'][$filename];
        }

        $keys = [
            'tipRegC',
            'fecEnvio',
            'convenio',
        ];

        $data = $this->getDataRow($keys);

        $data[] = [null, $filler];
        $data[] = ['000', 3];

        return $this->padItems($data);
    }



    /**
     * Número de Cuenta o Tarjeta Asegurada.
     *
     * @return array
     */
    public function numCtaTarjAseg()
    {
        if ($this->insurance->getCreditCard() instanceof \AppBundle\Entity\CreditCard) {
            return [$this->insurance->getCreditCard()->getCardNumber(), 20];
        }
        return [null, 20];
    }

    /**
     * Codigo Comuna del Bien Asegurado
     *
     * @return array
     */
    public function codComunaAseg()
    {
        return [$this->insurance->getHome()->getCodState(), 6];
    }

    /**
     * Codigo Ciudad del Bien Asegurado
     *
     * @return array
     */
    public function codCiudadAseg()
    {
        return [$this->insurance->getHome()->getCodCity(), 6];
    }

    /**
     * Direccion del bien Asegurado
     *
     * @return array
     */
    public function dirAseg()
    {


        return [utf8_decode($this->insurance->getHome()->getAddress()), 50];
    }

    /**
     * Código de Producto BSAN, X(3)
     *
     * @return array
     */
    public function codProducto()
    {
       $codproducto=$this->insurance->getProduct()->getCodeCorredora();
       if($codproducto==null){
        return [null, 3];
       }else{
        $codproducto = strtoupper($this->insurance->getProduct()->getCodeCorredora());
        $codproducto=utf8_decode($codproducto);
        
        return [$codproducto, 3];
        }
    }

    /**
     * Número de propuesta BSAN, N(8)
     * @return int
     */
    public function numPropuesta()
    {

        return [$this->insurance->getNumPropuesta(), 8];
    }

    /**
     * Cod Plan
     * X(3)
     */
  /*  public function codPlan()
    {
       
         $codPlan=$this->insurance->getPlan();
                          
        if(isset($codPlan['codeCorredora'])){
            $codPlan = (string)$codPlan['codeCorredora'];

           $cant=strlen($codPlan);

          $total=3-$cant;
           $plan='';
           for($i=0;$i<$total;$i++){
                    $plan.=' ';
            }

           $codPlan=$codPlan.$plan;
             
               
            return [$codPlan,3];

        }else
        {
         return [null,3];
       }

           
               
       
    }*/

        /**
     * Cod Plan
     * X(3)
     */
   public function codPlan()
    {

        $codcorredora=$this->insurance->getPri();  /*Verifica que no exista cod corredora particular para vacaciones o vida educacion*/
        if($codcorredora==null){
                $codPlan=$this->insurance->getPlan()['id'];
                $planes=$this->insurance->getProduct()->getPlans();
                 if($planes!=null and $codPlan!=null){
                 foreach ($planes as  $plan) {
                    if($codPlan==$plan->getId()){
                     $codcorredora= $plan->getCodeCorredora();
                     $codcorredora = (string)$codcorredora;

                   $cant=strlen($codcorredora);

                  $total=3-$cant;
                   $plan='';
                   for($i=0;$i<$total;$i++){
                            $plan.=' ';
                    }

                   $codcorredora=$codcorredora.$plan;
                   $codcorredora = strtoupper($codcorredora);
                   $codcorredora=utf8_decode($codcorredora);
                      return [$codcorredora,3];
                    }
                   
                    }
                 }else{
                     return [null,3];
                 }
             }else{
                $cant=strlen($codcorredora);

                  $total=3-$cant;
                   $plan='';
                   for($i=0;$i<$total;$i++){
                            $plan.=' ';
                    }

                   $codcorredora=$codcorredora.$plan;
                   $codcorredora = strtoupper($codcorredora);
                   
                   $codcorredora=utf8_decode($codcorredora);
                    return [$codcorredora,3];

             }
     }
     
    /**
     * Fec Venta N(8)
     *
     * @return array
     */
    public function fecVenta()
    {
        return [$this->insurance->getCreated()->format('Ymd'), 8];
    }



    /**
     * Código de Frecuencia de pago
     * 1: 12 Cuotas (Mensual)
     * 4: 01 Cuota (Anual )
     *
     * @return array
     */
    public function codFrecPago()
    {
        return [$this->insurance->getProduct()->getcodFrecPago(), 1];
    }

    /**
     * Num Med Pago x20
     *
     * @return array
     */
    public function codBancoTarjeta()
    {
         $paymethod=$this->insurance->getPaymentMethod();
                   
        if($paymethod!=null){
           $cant=strlen($paymethod);
           if($cant>12){
           
            return ['005',3];
            }else
            {
            return ['002',3];
        }
        }else
            {
            return ['002',3];
        }
             
    }

    /**
     * Num Med Pago x20
     *
     * @return array
     */
    public function numMedPago()
    {
         $paymethod=$this->insurance->getPaymentMethod();
             
            return [$paymethod,20];
           
    }
      

    /**
     * Código de dia de pago
     *
     */
    public function codDiaPago()
    {

        $payday=$this->insurance->getPaymentDay();
     
        if($payday!=null){
            $fistcara=substr($payday, 0,1);
            
            if($fistcara=='C'){
                return [$payday, 3];
            }elseif($payday=='8'){
                return ['C01', 3];
            }elseif($payday=='13'){
                return ['C02', 3];
            }elseif($payday=='20'){
                return ['C03', 3];
            }elseif($payday=='05'){
                return ['C04', 3];
            }elseif($payday=='02'){
                return ['C05', 3];
            }elseif($payday=='10'){
                return ['C06', 3];
            }elseif($payday=='15'){
                return ['C07', 3];
            }elseif($payday=='25'){
                return ['C08', 3];
            }elseif($payday=='06'){
                return ['C09', 3];
            }elseif($payday=='30'){
                return ['C10', 3];
            }elseif($payday=='01'){
                return ['C11', 3];
            }else{
                return ['C12', 3];
            }

        }
    }

    /**
     * Código de fuente BSAN x(3)
     *
     * @return array
     */
    public function codFuente()
    {
        return [$this->insurance->getCodFuente(), 3];
    }

    /**
     * P = banco; T = tarjeta; E = efectivo o valevista; C = cuota comercio; Q = chequera; R = Regalo
     */
    public function codMedPago()
    {
        return [$this->insurance->getCodMedPago(), 1];
    }
    public function fonoAseg()
    {
        
     $fono=substr($this->insurance->getPayer()->getAddressPhone(), 0, 10);
              $fonocel=substr($this->insurance->getPayer()->getAddressCellphone(), 0, 10);
              if(($fono==null) and ($fonocel==null)){
                    
                $fono='                   0';

                }else{
                    if($fono!=null){
                        $cant=strlen($fono);
                        $total=10-$cant;
                        $espacios='';
                        for($i=0;$i< $total;$i++){
                            $espacios.='0';
                        }
                        $fono=$espacios.$fono;
                    }else{
                        $fono=substr($this->insurance->getPayer()->getAddressCellphone(), 0, 10);
                        $cant=strlen($fono);
                        $total=10-$cant;
                        $espacios='';
                        for($i=0;$i< $total;$i++){
                            $espacios.='0';
                        }
                        $fono=$espacios.$fono;
                    }
                
              }
              return [$fono, 10];
}
    public function peso()
    {
        
        return [$this->insurance->getPeso(), 3];
    }
    public function declara()
    {
                                                            
   
                                                                        
                                  

        return [$this->insurance->getEnfermedad(), 255];
    }

    public function talla()
    {
        return [$this->insurance->getEstatura(), 3];
    }

    public function setInsurance(Insurance $insurance)
    {
        $this->insurance = $insurance;
    }

                                                          
    public function setParentescoCodigo($parentescoCodigo)
    {
        $this->parentescoCodigo = $parentescoCodigo;
    } 
                                                    
     

    public function rowForArchivo($keys, $lens)
    {
     
        return $this->padItems($this->getDataRow($keys), $lens);
    }

    public function rowToArray()
    {
        return [];
    }

    public function rowForArchivo1()
    {
        return [];
    }

    public function rowForArchivo2()
    {
        return [];
    }
     public function keysForArchivo3()
    {
         return [
            'tipRegD',
            'fecEnvio',
            'codRamo',
            'codLinea',
            'codProducto',
            'numPropuesta',
            'filler1',
            'rutbe'
        ];

        return [
            $this->padItems($this->getDataRow($keys))
        ];
    }


    public function rowForArchivo3()
    {
       return [];
    }

    public function rowForArchivo4()
    {
        $lines = [];

        $keys = [
            'tipRegD',
            'fecEnvio',
            'filler',
        ];

        $data = $this->getDataRow($keys);
      

        $first = $this->padItems($data);


        /**
            'rut',              // X10
            'apePaterno',       // X20
            'apeMaterno',       // X20
            'nombres',          // X25
            'fecNacimiento',    // Ymd
            'codSexo',          // M: Masculino;  F:Femenino
            'codEstCivil',      // C: Casado;  S:Soltero; V:Viudo; D:Divorciado;  A:Anulado
            'fono',             // X(20)
            'codCiudad',        // X(5)
            'codComuna',        // X(5)
            'direccion',        // X(80)
            'rtaMes',           // N(12,2)
         */
        $codcity=substr($this->insurance->getInsured()->getCodCity(), 0, 5);
                   
        if(($codcity!=null)){
           $cant=strlen($codcity);

           $total=5-$cant;
           $espacios='';
           for($i=0;$i< $total;$i++){
                    $espacios.='0';
            }
           $codcity=$espacios.$codcity;
              }else{
            $codcity='00000';

              } 
            $codcomuna=substr($this->insurance->getInsured()->getCodState(), 0, 5);
                   
        if(($codcomuna!=null)){
           $cant=strlen($codcomuna);

           $total=5-$cant;
           $espacios='';
           for($i=0;$i< $total;$i++){
                    $espacios.='0';
            }
           $codcomuna=$espacios.$codcomuna;
              }else{
                $codcomuna='00000';

              }
              $fono=substr($this->insurance->getInsured()->getAddressPhone(), 0, 20);
              $fonocel=substr($this->insurance->getInsured()->getAddressCellphone(), 0, 20);
              if(($fono==null) and ($fonocel==null)){
                    
                $fono='                   0';

                }else{
                    if($fono!=null){
                        $cant=strlen($fono);
                        $total=20-$cant;
                        $espacios='';
                        for($i=0;$i< $total;$i++){
                            $espacios.='0';
                        }
                        $fono=$espacios.$fono;
                    }else{
                        $fono=substr($this->insurance->getInsured()->getAddressCellphone(), 0, 20);
                        $cant=strlen($fono);
                        $total=20-$cant;
                        $espacios='';
                        for($i=0;$i< $total;$i++){
                            $espacios.='0';
                        }
                        $fono=$espacios.$fono;
                    }
                
              }
                          
            $codeonlynumber=(int) $this->insurance->getInsured()->getCodeOnlyNumber();
            $rut=$codeonlynumber.$this->insurance->getInsured()->getCodeOnlyDv();

            $cant=strlen($rut);

           $total=10-$cant;
           $espacios='';
           for($i=0;$i< $total;$i++){
                    $espacios.='0';
            }
            $rut=$espacios.$rut;

            /*comparar para traer el sexo */
            $codeonlynumber=(int) $this->insurance->getPayer()->getCodeOnlyNumber();
            $rutpayer=$codeonlynumber.$this->insurance->getPayer()->getCodeOnlyDv();

            $codeonlynumber=(int) $this->insurance->getInsured()->getCodeOnlyNumber();
            $rutinsured=$codeonlynumber.$this->insurance->getInsured()->getCodeOnlyDv();

            if($rutinsured==$rutpayer){
                $sexo=$this->insurance->getPayer()->getCodSexo();
                
            }else{
                $sexo='M';
            }

        $person = [
                [$rut, 10],
                [substr(utf8_decode($this->insurance->getInsured()->getLastname1()), 0, 20), 20],
                [substr(utf8_decode($this->insurance->getInsured()->getLastname2()), 0, 20), 20],
                [substr(utf8_decode($this->insurance->getInsured()->getName()), 0, 25), 25],
                [$this->insurance->getInsured()->getBirthday()->format('Ymd'), 8],
                [$sexo, 1],
                ['C', 1],
                [$fono, 20],
                [$codcity, 5], 
                [$codcomuna, 5],
                [substr(utf8_decode($this->insurance->getInsured()->getAddress()), 0, 80), 80],
                ['000000000000', 12]
            ];

          

        $lines[] = array_merge($first, $this->padItems($person));
      

		//CARGAS
		$output = [];
		$cargas=$this->insurance->getCargas();
		if($cargas){
			foreach ( $cargas as $carga ) {
				$codeonlynumber=(int) $carga->getCodeOnlyNumber();
				$rutcarga=$codeonlynumber.$carga->getCodeOnlyDv();
				$cant=strlen($rutcarga);
				$total=10-$cant;
				$espacios='';
				for($i=0;$i< $total;$i++){
					$espacios.='0';
				}
				$rut=$espacios.$rutcarga;
				$person = $this->padItems(
					[
						[$rut, 10],
						[substr(utf8_decode($carga->getLastname1()), 0, 20), 20],
						[substr(utf8_decode($carga->getLastname2()), 0, 20), 20],
						[substr(utf8_decode($carga->getName()), 0, 25), 25],
						[$carga->getBirthday()->format('Ymd'), 8],
						['', 1],
						['', 1],
						['', 20],
						['0000', 5], 
						['00000', 5],
						['', 80],
						['000000000000', 12]
					]
				);

				$lines[] = array_merge($first, $person);
			}
			//return $lines;
			//var_dump($lines);
			//die;
			//$lines[] = array_merge($first, $this->padItems($person));
			//$lines;
		}
		
		//BENEFICIARIOS
		$beneficiarios = $this->insurance->getBeneficiaries();
		if($beneficiarios){
			foreach ( $beneficiarios as $beneficiario ) {
				$codeonlynumber=(int) $beneficiario->getCodeOnlyNumber();
				$rutcarga=$codeonlynumber.$beneficiario->getCodeOnlyDv();
				$cant=strlen($rutcarga);
				$total=10-$cant;
				$espacios='';
				for($i=0;$i< $total;$i++){
					$espacios.='0';
				}
				$rut=$espacios.$rutcarga;
				$person = $this->padItems(
					[
						[$rut, 10],
						[substr(utf8_decode($beneficiario->getLastname1()), 0, 20), 20],
						[substr(utf8_decode($beneficiario->getLastname2()), 0, 20), 20],
						[substr(utf8_decode($beneficiario->getName()), 0, 25), 25],
						[$beneficiario->getBirthday()->format('Ymd'), 8],
						['', 1],
						['', 1],
						['', 20],
						['0000', 5], 
						['00000', 5],
						['', 80],
						['000000000000', 12]
					]
				);

				$lines[] = array_merge($first, $person);
			}
			//return $lines;
			//$output[] = $lines;
		}
		
		//var_dump($output);
		//die;

		if( count( $lines ) ) {
			return $lines;
		}
	}

    public function rowForArchivo5()
    {
        return [];
    }

    public function rowForTotales($filename,$canDetalle,$mtoTotal)
    {
        $filler = 0;
        
        if (array_key_exists($filename, $this->fillers['totales'])) {
           
            $filler = (int)$this->fillers['totales'][$filename];
        }

        $keys = [
            'tipRegT',
            'fecEnvio',
            'codConv',
        ];

        $data = $this->getDataRow($keys);

        $data[] = [$canDetalle, 5];

        if($mtoTotal!=0){
            $data[] = [$mtoTotal, 12];
        }else{
            $data[] = [null, 12];
        }
        
        $data[] = [null, $filler];


        return $this->padItems($data);
    }
}
