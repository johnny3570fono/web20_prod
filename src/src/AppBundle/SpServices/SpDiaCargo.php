<?php
namespace AppBundle\SpServices;

/**
 * Service Store Procedure SpDiaCargo
 */
class SpDiaCargo
{
	private $spConnection;

    public function __construct($connection)
    {
    	$this->spConnection = $connection;
    }
    

    /**
     * Funcion que busca los días de cobro
     *
     * @param string
     * @return array
     */
    public function getDiaCargo()
    {
		$connection = $this->spConnection->getWrappedConnection();

        $ppcdia1 = 0;
        $ppcdia2 = 0;
        $ppcdia3 = 0;
        $ppcdia4 = 0;
        $ppcdia5 = 0;


        $conn = oci_connect( $this->spConnection->getUsername(), $this->spConnection->getPassword(),  $this->spConnection->getParams()["host"].'/'.$this->spConnection->getParams()["dbname"]);

        $stid = oci_parse($conn, "begin portalzs.pkg_ventaneox.sp_dia_cargo(:ptiponegociobv, :ptipopolizabv, :ptipocobranzabv, :ppcdia1bv, :ppcdia2bv, :ppcdia3bv, :ppcdia4bv, :ppcdia5bv); end;");
        oci_bind_by_name($stid, ":ptiponegociobv", $ptiponegocio, 32);
        oci_bind_by_name($stid, ":ptipopolizabv", $ptipopoliza, 32);
        oci_bind_by_name($stid, ":ptipocobranzabv", $ptipocobranza, 32);
        oci_bind_by_name($stid, ":ppcdia1bv", $ppcdia1, 32 );
        oci_bind_by_name($stid, ":ppcdia2bv", $ppcdia2, 32 );
        oci_bind_by_name($stid, ":ppcdia3bv", $ppcdia3, 32 );
        oci_bind_by_name($stid, ":ppcdia4bv", $ppcdia4, 32 );
        oci_bind_by_name($stid, ":ppcdia5bv", $ppcdia5, 32 );

        $ptiponegocio = 'V';
        $ptipopoliza = 'I';
        $ptipocobranza = 'P';

        oci_execute($stid);
        oci_free_statement($stid);
        oci_close($conn);
        return ["dia1" => $ppcdia1, "dia2" => $ppcdia2, "dia3" => $ppcdia3, "dia4" => $ppcdia4, "dia5" => $ppcdia5];
    }
}