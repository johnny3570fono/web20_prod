<?php
namespace AppBundle\SpServices;


/**
 * Service Store Procedure SpClasificacionSegmento
 */
class SpClasificacionSegmento
{

	private $spConnection;

    public function __construct($connection)
    {
    	$this->spConnection = $connection;
    }

    /**
     * Funcion privada que valida un rut y devuelve el rut sin puntos ni digito verificador para exito y un false para error
     *
     * @param string
     * @return boolean or string
     */
    private function getRut($rut){
    	if(strpos($rut,"-")==false){
	        $RUT[0] = substr($rut, 0, -1);
	        $RUT[1] = substr($rut, -1);
	    }else{
	        $RUT = explode("-", trim($rut));
	    }
	    $elRut = str_replace(".", "", trim($RUT[0]));
	    $factor = 2;
	    $suma = 0;
	    for($i = strlen($elRut)-1; $i >= 0; $i--):
	        $factor = $factor > 7 ? 2 : $factor;
	        $suma += $elRut{$i}*$factor++;
	    endfor;
	    $resto = $suma % 11;
	    $dv = 11 - $resto;
	    if($dv == 11){
	        $dv=0;
	    }else if($dv == 10){
	        $dv="k";
	    }else{
	        $dv=$dv;
	    }
	   if($dv == trim(strtolower($RUT[1]))){
	       return $elRut;
	   }else{
	       return false;
	   }
    }


    /**
     * Funcion que llama a un SP
     *
     * @param string
     * @return boolean or string
     */
    public function getSegmento($rut = "")
    {
    	$rut = $this->getRut($rut);
    	dump($rut);
    	if($rut == false){
    		return "rut inválido";
    	}

    	$conn = oci_connect( $this->spConnection->getUsername(), $this->spConnection->getPassword(),  $this->spConnection->getParams()["host"].'/'.$this->spConnection->getParams()["dbname"]);

    	$curs = oci_new_cursor($conn);
		$rutPersona = $rut;
		$stid = oci_parse($conn, "begin ALTAVIDA.SP_GES_CSF_SEG(:rutbv,:cursbv); end;");
		oci_bind_by_name($stid, ":cursbv", $curs, -1, OCI_B_CURSOR);
		oci_bind_by_name($stid, ":rutbv", $rutPersona);
		oci_execute($stid);

		oci_execute($curs);  // Execute the REF CURSOR like a normal statement id
		$datos_persona = [];
		while (($row = oci_fetch_array($curs, OCI_ASSOC+OCI_RETURN_NULLS)) != false) {
		    $datos_persona = $row;
		}
		oci_free_statement($stid);
		oci_close($conn);
	
		return $datos_persona;
    }
}