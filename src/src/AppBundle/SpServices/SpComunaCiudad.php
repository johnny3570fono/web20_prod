<?php
namespace AppBundle\SpServices;

/**
 * Service Store Procedure SpComunaCiudad
 */
class SpComunaCiudad
{
	private $spConnection;

    public function __construct($connection)
    {
    	$this->spConnection = $connection;
    }

    /**
     * Funcion que entrega un arreglo con el listado de comunas y cuidades
     *
     * @return array
     */
    public function getComunaCiudad()
    {
        $conn = oci_connect( $this->spConnection->getUsername(), $this->spConnection->getPassword(),  $this->spConnection->getParams()["host"].'/'.$this->spConnection->getParams()["dbname"], 'WE8ISO8859P15');

    	$curs = oci_new_cursor($conn);

		$stid = oci_parse($conn, "begin ALTAVIDA.SP_GES_CNA_CDD_CMN(:cursbv); end;");
		oci_bind_by_name($stid, ":cursbv", $curs, -1, OCI_B_CURSOR);
		oci_execute($stid);

		oci_execute($curs);  // Execute the REF CURSOR like a normal statement id
		$array_comunas = [];
		while (($row = oci_fetch_array($curs, OCI_ASSOC+OCI_RETURN_NULLS)) != false) {
		    array_push($array_comunas , $row);		    
		}
		oci_free_statement($stid);
		oci_close($conn);
        
		return $array_comunas;
    }
}