<?php
namespace AppBundle\SpServices;

/**
 * Service Store Procedure SpCurestudio 
 */
class SpCursos
{
	private $spConnection;

    public function __construct($connection)
    {
    	$this->spConnection = $connection;
    }
    

    /**
     * Funcion que busca el valor del curso obteniendo la siguiente salida: Código Curso, Descripción curso,años remanentes de estudio, años no universitarios, años universitarios
     *
     * @return array
     */
     public function getCursos()
    {
        $conn = oci_connect( $this->spConnection->getUsername(), $this->spConnection->getPassword(),  $this->spConnection->getParams()["host"].'/'.$this->spConnection->getParams()["dbname"]);

        $curs = oci_new_cursor($conn);

        $stid = oci_parse($conn, "begin ALTAVIDA.SP_GES_CURESTUDIO(:cursbv); end;");
        oci_bind_by_name($stid, ":cursbv", $curs, -1, OCI_B_CURSOR);
        oci_execute($stid);
        oci_execute($curs);  // Execute the REF CURSOR like a normal statement id

        $arrayCursos = [];
        while (($row = oci_fetch_array($curs, OCI_ASSOC+OCI_RETURN_NULLS)) != false){
          //  $arrayCursos = $row;
            array_push($arrayCursos , $row);
        }
        oci_free_statement($stid);
        oci_close($conn);

        /*$arrayCursos[0]=array("codcurso" => '0', "descurso" => '',"remanente" => 0 ,"anosnouniver" => 0,"anosuniver" => 0);
        $arrayCursos[1]=array("codcurso" => '1', "descurso" => 'Playgroup',"remanente" => 15 ,"anosnouniver" => 15,"anosuniver" => 6);
        $arrayCursos[2]=array("codcurso" => '2', "descurso" => 'PreKinder',"remanente" => 14 ,"anosnouniver" => 14,"anosuniver" => 6);
        $arrayCursos[3]=array("codcurso" => '3', "descurso" => 'Kinder',"remanente" => 13 ,"anosnouniver" => 13,"anosuniver" => 6);
        $arrayCursos[4]=array("codcurso" => '4', "descurso" => 'Primero Basico',"remanente" => 12 ,"anosnouniver" => 12,"anosuniver" => 6);
        $arrayCursos[5]=array("codcurso" => '5', "descurso" => 'Segundo Basico',"remanente" => 11 ,"anosnouniver" => 11,"anosuniver" => 6);
        $arrayCursos[6]=array("codcurso" => '6', "descurso" => 'Tercero Basico',"remanente" => 10,"anosnouniver" => 10,"anosuniver" => 6);
        $arrayCursos[7]=array("codcurso" => '6', "descurso" => 'Cuarto Basico',"remanente" => 9,"anosnouniver" => 9,"anosuniver" => 6);
        $arrayCursos[8]=array("codcurso" => '6', "descurso" => 'Quinto Basico',"remanente" => 8,"anosnouniver" => 8,"anosuniver" => 6);
        $arrayCursos[9]=array("codcurso" => '6', "descurso" => 'Sexto Basico',"remanente" => 7,"anosnouniver" => 7,"anosuniver" => 6);
        $arrayCursos[10]=array("codcurso" => '6', "descurso" => 'Septimo Basico',"remanente" => 6,"anosnouniver" => 6,"anosuniver" => 6);
        $arrayCursos[11]=array("codcurso" => '6', "descurso" => 'Octavo Basico',"remanente" => 5,"anosnouniver" => 5,"anosuniver" => 6);
        $arrayCursos[12]=array("codcurso" => '6', "descurso" => 'Primero Medio',"remanente" => 4,"anosnouniver" => 4,"anosuniver" => 6);
        $arrayCursos[13]=array("codcurso" => '6', "descurso" => 'Segundo Medio',"remanente" => 3,"anosnouniver" => 3,"anosuniver" => 6);
        $arrayCursos[14]=array("codcurso" => '6', "descurso" => 'Tercero Medio',"remanente" => 2,"anosnouniver" => 2,"anosuniver" => 6);
        $arrayCursos[15]=array("codcurso" => '6', "descurso" => 'Cuarto Medio',"remanente" => 1,"anosnouniver" => 1,"anosuniver" => 6);
        $arrayCursos[16]=array("codcurso" => '6', "descurso" => 'Univ.Faltan 6',"remanente" => 6,"anosnouniver" => 0,"anosuniver" => 6);
        $arrayCursos[17]=array("codcurso" => '6', "descurso" => 'Univ.Faltan 5',"remanente" => 5,"anosnouniver" => 0,"anosuniver" => 5);
        $arrayCursos[18]=array("codcurso" => '6', "descurso" => 'Univ.Faltan 4',"remanente" => 4,"anosnouniver" => 0,"anosuniver" => 4);
        $arrayCursos[19]=array("codcurso" => '6', "descurso" => 'Univ.Faltan 3',"remanente" => 3,"anosnouniver" => 0,"anosuniver" => 3);
        $arrayCursos[20]=array("codcurso" => '6', "descurso" => 'Univ.Faltan 2',"remanente" => 2,"anosnouniver" => 0,"anosuniver" => 2);
        $arrayCursos[21]=array("codcurso" => '6', "descurso" => 'Univ.Faltan 1',"remanente" => 1,"anosnouniver" => 0,"anosuniver" => 1);*/


       /*return ["codcurso" => $codcurso, "descurso" => $descurso,"remanente" => $remanente,"anosnouniver" => $anosnouniver,"anosuniver" => $anosuniver];*/
        return $arrayCursos;
    }
}