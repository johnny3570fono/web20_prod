<?php
namespace AppBundle\SpServices;

/**
 * Service Store Procedure SpPrima
 */
class SpPrima
{
	private $spConnection;

    public function __construct($connection)
    {
    	$this->spConnection = $connection;
    }
    

    /**
     * Funcion que busca la proyección de la prima anual. Con datos de entrada: código de Producto, Código de plan, fecha nacimiento asegurado, periodicidad, código curso, fecha de proceso. y los datos de salida: Años póliza, prima UF
 
     * @param string
     * @return array
     */
    public function getPrima($codprod,$codplan,$nac,$perid,$codcurso,$fechaactual)
    {

      /*       (293, '1', '05-05-1982', 'M', 4, '02-08-2016');
      Código de producto, código plan, fch nac asegurado,periodicidad(A,M),Código curso entregado por spCursos,Fecha de proceso */
        $connection = $this->spConnection->getWrappedConnection();
        $conn = oci_connect( $this->spConnection->getUsername(), $this->spConnection->getPassword(),  $this->spConnection->getParams()["host"].'/'.$this->spConnection->getParams()["dbname"]);
      // $codprod='293';

       //$codplan='1';
        $nac_trans= date_create($nac);
        $nac= date_format($nac_trans, 'd-M-Y');
       // $nac='02-JAN-2011';
    //   $perid='A';
       // $codcurso=4;
           // $fechaactual='02-JAN-2011';
       
        $fechaactual_trans= date_create($fechaactual);
        //var_dump($fechaactual);
        //var_dump($fechaactual_trans);
        //die();
        $fechaactual= date_format($fechaactual_trans, 'd-M-Y');       
        
        $curs = oci_new_cursor($conn);
        

        $stid = oci_parse($conn, "begin ALTAVIDA.SP_GES_PROY_EDU(:codprod, :codplan, :nac, :perid, :codcurso, :fechaactual,:cursbv); end;");
       
        oci_bind_by_name($stid, ":codprod", $codprod);
        oci_bind_by_name($stid, ":codplan", $codplan);
        oci_bind_by_name($stid, ":nac", $nac);
        oci_bind_by_name($stid, ":perid", $perid);
        oci_bind_by_name($stid, ":codcurso", $codcurso);
        oci_bind_by_name($stid, ":fechaactual", $fechaactual);
        oci_bind_by_name($stid, ":cursbv", $curs, -1, OCI_B_CURSOR);
       
        oci_execute($stid);

        
        oci_execute($curs);  // Execute the REF CURSOR like a normal statement id
        $arrayPrima = [];
        while (($row = oci_fetch_array($curs, OCI_ASSOC+OCI_RETURN_NULLS)) != false) {
             array_push($arrayPrima , $row);
        }
        oci_free_statement($stid);
        oci_close($conn);
       /* $arrayPrima[0]=array("EDU_PROY_NUM" => '1', "EDU_PROY_VAL" => '0,5453');
        $arrayPrima[1]=array("EDU_PROY_NUM" => '1', "EDU_PROY_VAL" => '0,5285');
        $arrayPrima[2]=array("EDU_PROY_NUM" => '1', "EDU_PROY_VAL" => '0,5117');
        $arrayPrima[3]=array("EDU_PROY_NUM" => '1', "EDU_PROY_VAL" => '0,4949');
        $arrayPrima[4]=array("EDU_PROY_NUM" => '1', "EDU_PROY_VAL" => '0,4781');
        $arrayPrima[5]=array("EDU_PROY_NUM" => '1', "EDU_PROY_VAL" => '0,4613');
        $arrayPrima[6]=array("EDU_PROY_NUM" => '1', "EDU_PROY_VAL" => '0,4445');
        $arrayPrima[7]=array("EDU_PROY_NUM" => '1', "EDU_PROY_VAL" => '0,4277');
        $arrayPrima[8]=array("EDU_PROY_NUM" => '1', "EDU_PROY_VAL" => '0,4109');
        $arrayPrima[9]=array("EDU_PROY_NUM" => '1', "EDU_PROY_VAL" => '0,3941');*/

        return $arrayPrima;
    }

}