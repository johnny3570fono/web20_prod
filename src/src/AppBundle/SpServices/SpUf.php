<?php
namespace AppBundle\SpServices;

/**
 * Service Store Procedure SpClasificacionSegmento
 */
class SpUf
{
	private $spConnection;

    public function __construct($connection)
    {
    	$this->spConnection = $connection;
    }
    

    /**
     * Funcion que busca el valor de la uf para el dia solicitado , sino se entrega la fecha asume la del sistema (YYYY/MM/DD)
     *
     * @param string
     * @return array
     */
    public function getUf($day = "")
    {
    	$fecha = date("Y/m/d",strtotime($day));
    	$date = date_parse($day);

		if ($date["error_count"] == 0 && checkdate($date["month"], $date["day"], $date["year"]))
		    $fecha = date("Y/m/d",strtotime($day));
		else
		    $fecha = date("Y/m/d");

		$connection = $this->spConnection->getWrappedConnection();

		$sql = 'begin ALTAVIDA.SP_GES_CNA_DTS_UF(:fechaUFbv,:valorUFbv); end;';

		$stmt = $connection->prepare($sql);

		$valorUF = 0;

		$stmt->bindParam(":fechaUFbv",$fecha,\PDO::PARAM_STR);
		$stmt->bindParam(":valorUFbv",$valorUF,\PDO::PARAM_STR,16);

        echo $fecha;
		
		$stmt->execute();

        return ["valor" => $valorUF, "query_date" => $fecha];
    }
}