<?php

namespace AppBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Bundle Portal Zurich Santander
 */
class AppBundle extends Bundle
{
}
