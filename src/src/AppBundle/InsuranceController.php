<?php

namespace AppBundle\Controller;

use AppBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

use AppBundle\Form\InsuranceType;
use AppBundle\Form\InsuranceHomeType;
use AppBundle\Form\InsuranceFraudType;
use AppBundle\Form\InsuranceTravelType;
use AppBundle\Form\PaymentType;
use AppBundle\Entity\Insurance;
use AppBundle\Entity\CreditCard;
use AppBundle\Entity\Home;
use AppBundle\Entity\Product;
use AppBundle\Entity\BasePerson;
use AppBundle\Entity\Insured;
use AppBundle\Entity\Community;
use AppBundle\Entity\Beneficiary;
use AppBundle\Entity\Carga;
use AppBundle\Entity\PersonRelationship;
use AppBundle\Entity\Educational;
use AppBundle\Validator\Constraints as Assert;

/**
 * Controller de Seguros
 */
class InsuranceController extends BaseController
{
    protected $baseTemplate;

    public function __construct()
    {
        $this->baseTemplate = '::insurance/details.html.twig';
    }

    /**
     * @return string
     */
    public function getTemplate(){
        return $this->baseTemplate;
    }

    /**
     * @param string
     * @return null
     */
    public function setTemplate($newTemplateName)
    {
        return $this->baseTemplate = '::insurance/'.$newTemplateName;
    }

    /**
     * Funcion utilitaria para crear una instancia de un Seguro
     *
     * @return Insurance
     */
    protected function createInsurance(\AppBundle\Entity\Product $product)
    {
        $segment = $this->getUser()->getSegment();

        $segmentFamily = $this->getSegmentFamily($segment, $product->getFamily());

        return Insurance::createFrom($this->getUser(), $product, $segment, $segmentFamily);
    }


    /**
     * @Route("/fraud/{id}", requirements={"id" = "\d+"}, name="fraud_insurance")
     * @ParamConverter("product", class="AppBundle:Product", options={"repository_method" = "findOneFraud"})
     * @Method({"GET", "POST"})
     *
     * @param Product $product
     * @param Request $request
     * @return Response
     */
    public function fraudAction(Product $product, Request $request)
    {
        $session = $request->getSession();
        $session->set('idpayment', '');
        $session->set('idresume', '');

        if(!$product->inSegment($this->getUser()->getSegment())){
            throw new \Exception('Segmento erroneo');
        }

        $codStates = $this->getManager()->getRepository('AppBundle:Community')
            ->findAllAsArrayOfCod();
        $insurance = $this->createInsurance($product);

        $cards = [];
        foreach ($this->getUser()->getCreditCards() as $c) {
            if($c->idprod == 80 && trim($c->cardNumber) > ''){
                $cards[$c->id] = [
                        "cardNumber" => $c->cardNumber,
                        "name" => $c->name,
                        "id" => $c->id
                    ];
            }
        }


        if (class_exists($product->getFormType())) {
            $className = $product->getFormType();

            $formType = new $className($this->getManager(), $cards);
        } else {
            $formType = new InsuranceType($this->getManager());

        }
        $form = $this->createForm($formType, $insurance);
        $form->handleRequest($request);
        
         if ($form->isSubmitted() ) {

            $em = $this->getDoctrine()->getManager();

            if ($form->get('payerEqualsInsured')->getData() == 1) {
                $insurance->insuredPassDataToPayer($form->get('insured')->getData());
            }
      if ($form->offsetExists('credit_card')) {
               $credit_cards = $form->get('credit_card')->getData();
            
                foreach ($this->getUser()->getCreditCards() as $c) {
                    if ($c->cardNumber == $credit_cards) {
                      $credicard= new CreditCard();
                      $credicard->setCardNumber($c->cardNumber);
                 
                      $credicard->setName($c->name);
                    }
                }
                $credicard->setInsurance($insurance);

            $insurance->setCreditcard($credicard);
            
            $em->persist($credicard);
          }
            $insurance->setSuscripcion($product->getSuscripcion());
          
            $em->persist($insurance);
            
            $em->flush();

            $session = $request->getSession();
            $session->set('idpayment', $insurance->getId());
            $session->set('idresume', '');

//            return $this->redirect($this->generateUrl('payment_insurance', ['id' => $insurance->getId()]), 301);
            return $this->redirect($this->generateUrl('payment_insurance'), 301);
        }

        if ($product->getFormTemplate()) {
            $this->setTemplate($product->getFormTemplate());
        }

        $s = new \AppBundle\Form\FormErrorsSerializer();

        return $this->render($this->getTemplate(), [
            'form' => $form->createView(),
            'insurance' => $insurance,
            'cards' => $cards,
            'product' => $product,
            'errors' => $s->serializeFormErrors($form),
            'cover' =>  $this->cover($product),
            'georly' => $codStates

        ]);
    }

    /**
     * @Route("/details/{id}", requirements={"id" = "\d+"}, name="details_insurance")
     * @ParamConverter("product", class="AppBundle:Product")
     * @Method({"GET", "POST"})
     *
     * @param Product $product
     * @param Request $request
     * @return Response
     */
    public function detailsAction(Product $product, Request $request)
    {
        $session = $request->getSession();
        $session->set('idpayment', '');
        $session->set('idresume', '');

        if(!$product->inSegment($this->getUser()->getSegment())){
            throw new \Exception('Segmento erroneo');
        }

        $codStates = $this->getManager()->getRepository('AppBundle:Community')
            ->findAllAsArrayOfCod();

        // $logger = $this->get('logger');
        // $logger->info("\n\n\n");
        // $logger->info(date('Y-m-d H:i:s u').' - Cargando Detalle');

        $em = $this->getDoctrine()->getManager();
        $educational=false;
        $insurance = $this->createInsurance($product);

        if (class_exists($product->getFormType())) {

            $className = $product->getFormType();
            $formType = new $className($this->getManager());

        } else {

            $formType = new InsuranceType($this->getManager());
        }
        // $logger->info(date('Y-m-d H:i:s u').' - Creando formulario');
        $form = $this->createForm($formType, $insurance);
        
        $form->handleRequest($request);


		$errors = [];
		$arraycursos=[];

         $arrayrentas=[];
         if(($product->getFormTemplate())=='life_education.html.twig'){

            $educational=true;
            $form_educational = new Educational();
            $cursos=$this->container->get('sp.cursos')->getCursos();
            foreach ($cursos as $key => $curso) {
               $arraycursos[$key]=$curso;
            }
        }
           //echo $form->getErrorsAsString();

           
        if ($form->isSubmitted()){  
            foreach ($form->getErrors(true) as $error){
                $errors[] =  $error->getMessage();
            }
            if($educational==true){

                $form_educational->setPrima(json_decode($form->get('primaanual')->getData()));
                $id_curso=$form->get('renta')->getData();
                $renta_query = $em->getRepository('AppBundle:Renta')->findOneBy(array('id'=>$id_curso));
                $form_educational->setRenta($renta_query);
                $form_educational->setCurso($form->get('curso')->getData());


                $em->persist($form_educational);

            }

            if ($form->isValid()) {
                  
                if ($form->get('payerEqualsInsured')->getData() == 1) {
                    $insurance->insuredPassDataToPayer($form->get('insured')->getData());
                }
                 $insurance->setSuscripcion($product->getSuscripcion());

                //edad asegurado
             //   print_r($this->getUser()->getBirthday());
               // print_r($product->getAgeMin());

                /*$ed = $this->getUser()->getAge();
                $edad = 'f';
                if($ed > $product->getAgeMin() && $ed < $product->getAgeMax()){
                    $edad = 't';
                }*/
               

                $em->persist($insurance);

                $em->flush();
                $session = $request->getSession();
                $session->set('idpayment', $insurance->getId());
                $session->set('idresume', '');

    //            return $this->redirect($this->generateUrl('payment_insurance', ['id' => $insurance->getId()]), 301);
                return $this->redirect($this->generateUrl('payment_insurance'), 301);

			}
		}

        $template = '::insurance/details.html.twig';

        if ($product->getFormTemplate()) {
            $template = '::insurance/' . $product->getFormTemplate();
        }

        $relationships = [];
        if ($form->offsetExists('beneficiaries')){
            $relationships = [
                ['value' =>PersonRelationship::REALTIONSHIP_CHILD,'maxapparition' => -1],
                ['value' =>PersonRelationship::REALTIONSHIP_SPOUSE,'maxapparition' => 1],
                ['value' =>PersonRelationship::REALTIONSHIP_FATHER,'maxapparition' => 1],
                ['value' =>PersonRelationship::REALTIONSHIP_MOTHER,'maxapparition' => 1],
                ['value' =>PersonRelationship::REALTIONSHIP_OTHER,'maxapparition' => -1]
            ];
        }

        if ($form->offsetExists('cargas')) {
            $relationships = [
                ['value' =>PersonRelationship::REALTIONSHIP_CHILD,'maxapparition' => -1],
                ['value' =>PersonRelationship::REALTIONSHIP_SPOUSE,'maxapparition' => 1]
            ];
        }

        if ($product->getFormTemplate()) {
            $this->setTemplate($product->getFormTemplate());
        }

        $s = new \AppBundle\Form\FormErrorsSerializer();
        // $logger->info(date('Y-m-d H:i:s u').' - enviando data front');

        return $this->render($this->getTemplate(), [
            'form' => $form->createView(),
            'insurance' => $insurance,
            'product' => $product,
            'relationships' => $relationships,
            'errors' => $s->serializeFormErrors($form),
			'formErrors'=>$errors,
            'arraycursos'=>$arraycursos,
            'cover' =>  $this->cover($product),
            'arrayrentas'=>$this->rentas(),
            'georly' => $codStates


        ]);
    }

    /**
     * @Route("/payment/choice", name="payment_insurance")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function paymentAction( Request $request)
    {

        $session = $request->getSession();
//        die($session->get('idpayment'));
        $em = $this->getDoctrine()->getManager();

        $insurance = $em->getRepository('AppBundle:Insurance')->findOneDraft(array('id'=>$session->get('idpayment')));

        // $logger = $this->get('logger');
        // $logger->info("\n\n\n");
        // $logger->info(date('Y-m-d H:i:s u').' - Cargando Forma de pago');
        $cards = [];
        foreach ($this->getUser()->getCreditCards() as $c) {
            $cards[$c->id] = [
                    "cardNumber" => $c->cardNumber,
                    "name" => $c->name,
                    "id" => $c->id
                ];
        }
//        $resultado = $this->container->get('sp.dia_cargo')->getDiaCargo();
        $resultado = array(
                        'dia1' =>  '6',
                        'dia2' => '13',
                        'dia3' => '20',
                    );

        $form = $this->createForm(new PaymentType(), $insurance, array('cards'=>$cards,'diacargo'=>$resultado));


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $fechalast = $em->getRepository('AppBundle:Insurance')->findlast($session->get('rut'));
            if($fechalast > ''){
                $tiempolimite = $em->getRepository('AppBundle:Config')->findOneById(2)->getValue();

                if((time() - strtotime($fechalast['updated']->format('Y-m-d H:i:s'))) < ($tiempolimite*1)){
                    return $this->redirect($this->generateUrl('error_insurance'), 301);
                }

            }


            $em = $this->getDoctrine()->getManager();

            $insurance->setStatus(Insurance::STATUS_ISSUED);

            
            $em->persist($insurance);
            $em->flush();
            
            $uf=$this->getRepository('AppBundle:Uf')
            ->findLast();
            $parametros=$this->getParameter('mail_option');

            $this->get('mail_helper')->correoNotificacion($insurance,$uf,$parametros);

            $session = $request->getSession();
            $session->set('idresume', $insurance->getId());

           // return $this->redirect($this->generateUrl('resume_insurance', ['id' => $insurance->getId()]), 301);
            return $this->redirect($this->generateUrl('resume_insurance'), 301);
        }
        // $logger->info(date('Y-m-d H:i:s u').' - enviando data a front');

        return $this->render('::insurance/payment.html.twig', [
            'insurance' => $insurance,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/resume/voucher", name="resume_insurance")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function resumeAction(Request $request)
    {
        $session = $request->getSession();
//        die($session->get('idpayment'));
        $em = $this->getDoctrine()->getManager();
        $insurance = $em->getRepository('AppBundle:Insurance')->findOneBy(array('id'=>$session->get('idresume')));

        // $logger = $this->get('logger');
        // $logger->info("\n\n\n");
        // $logger->info(date('Y-m-d H:i:s u').' - mostrando resumen');
        return $this->render('::insurance/resume.html.twig', [
            'insurance' => $insurance,
        ]);
       /*  return $this->render('::Correo/correoNotificacion.txt.twig', [
            'insurance' => $insurance,
        ]);*/
        
        
    }

    /**
     * @Route("/timeerrorinsurance", name="error_insurance")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function errorAction(Request $request)
    {
        return $this->render('::status/errorlimite.html.twig');
    }

}
