<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CampaignRegister
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class CampaignRegister
{


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="Insurance")
     * @ORM\JoinColumn(name="insurance_id", referencedColumnName="id")
     **/
    private $insurance;


    /**
     * @var string
     *
     * @ORM\Column(name="rut", type="string", nullable=true, length=20)
     */
    private $rut;
    
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", nullable=true, length=150)
     */
    private $title;
    
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", nullable=true)
     */
    private $description;
    
    /**
     * @var string
     *
     * @ORM\Column(name="answer", type="string", nullable=true)
     */
    private $answer;


    /**
     * @var string
     *
     * @ORM\Column(name="product_name", type="string", nullable=true)
     */
    private $product;

    /**
     * @var string
     *
     * @ORM\Column(name="plan_name", type="string", nullable=true)
     */
    private $plan;

    /**
     * @ORM\ManyToOne(targetEntity="Campaign", inversedBy="campaign")
     * @ORM\JoinColumn(name="campaign_id", referencedColumnName="id", nullable=false)
     */
    private $campaign;
    

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;


     /**
     * @ORM\Column(name="updated", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updated;
   
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set insurance
     *
     * @param \AppBundle\Entity\Insurance $insurance
     */
    public function setInsurance(\AppBundle\Entity\Insurance $insurance = null)
    {
        $this->insurance = $insurance;

        return $this;
    }

    /**
     * Get insurance
     *
     * @return \AppBundle\Entity\Insurance
     */
    public function getInsurance()
    {
        return $this->insurance;
    }

    /**
     * Get rut
     *
     * @return integer
     */
    public function getRut()
    {
        return $this->rut;
    }

    /**
     * Set the value of Rut
     *
     * @param string rut
     *
     * @return self
     */
    public function setRut($rut)
    {
        $this->rut = $rut;

        return $this;
    }



    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of title
     *
     * @param string title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @param string description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }



     /**
     * Get answer
     *
     * @return integer
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set the value of answer
     *
     * @param string answer
     *
     * @return self
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;

        return $this;
    }


    /**
     * Get product
     *
     * @return integer
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set the value of product
     *
     * @param string product
     *
     * @return self
     */
    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }


    /**
     * Get plan
     *
     * @return integer
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * Set the value of plan
     *
     * @param string plan
     *
     * @return self
     */
    public function setPlan($plan)
    {
        $this->plan = $plan;

        return $this;
    }

   
    /**
     * Set plan
     *
     * @param \AppBundle\Entity\Plan $plan
     */
    public function setCampaign(\AppBundle\Entity\Campaign $campaign)
    {
        $this->campaign = $campaign;
    }

    /**
     * Get plan
     *
     * @return \AppBundle\Entity\Campaign
     */
    public function getCampaign()
    {
        return $this->campaign;
    }

    /**
     * Setter $created
     *
     * @param \DateTime $created
     * @return \AppBundle\Entity\Insurance
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    
    /**
     * Getter Updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    

    /**
     * Setter $updated
     *
     * @param \DateTime $updated
     * @return \AppBundle\Entity\Insurance
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;

        return $this;
    }

}
