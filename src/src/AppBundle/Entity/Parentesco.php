<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Parentesco
 *
 * @ORM\Table()  
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\ParentescoRepository")
 */
class Parentesco {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank(groups={"all"})     
     */
    private $parentesco;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(groups={"all"})     
     */
    private $edadMinima;

    /**
     * @ORM\Column(type="integer")     
     * @Assert\NotBlank(groups={"all"})
     */
    private $edadMaxima;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Product")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private $producto;
    
    /**
     * @ORM\Column(type="integer", nullable=true)     
     */
    private $maxApparition;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set parentesco
     *
     * @param string $parentesco
     * @return Parentesco
     */
    public function setParentesco($parentesco) {
        $this->parentesco = $parentesco;

        return $this;
    }

    /**
     * Get parentesco
     *
     * @return string 
     */
    public function getParentesco() {
        return $this->parentesco;
    }

    /**
     * Set edadMinima
     *
     * @param string $edadMinima
     * @return Parentesco
     */
    public function setEdadMinima($edadMinima) {
        $this->edadMinima = $edadMinima;

        return $this;
    }

    /**
     * Get edadMinima
     *
     * @return integer 
     */
    public function getEdadMinima() {
        return $this->edadMinima;
    }

    /**
     * Set edadMaxima
     *
     * @param string $edadMaxima
     * @return Parentesco
     */
    public function setEdadMaxima($edadMaxima) {
        $this->edadMaxima = $edadMaxima;

        return $this;
    }

    /**
     * Get edadMaxima
     *
     * @return integer 
     */
    public function getEdadMaxima() {
        return $this->edadMaxima;
    }

    /**
     * Getter $producto
     *
     * @return \AppBundle\Entity\Product
     */
    public function getProducto() {
        return $this->producto;
    }

    /**
     * Setter $producto
     *
     * @param \AppBundle\Entity\Product $producto
     * @return Parentesco
     */
    public function setProducto(\AppBundle\Entity\Product $producto = null) {
        $this->producto = $producto;

        return $this;
    }
    
    /**
     * Set maxApparition
     *
     * @param string $maxApparition
     * @return Parentesco
     */
    public function setMaxApparition($maxApparition) {
        $this->maxApparition = $maxApparition;

        return $this;
    }

    /**
     * Get $maxApparition
     *
     * @return integer 
     */
    public function getMaxApparition() {
        return $this->maxApparition;
    }

}
