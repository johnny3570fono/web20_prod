<?php

namespace AppBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;

use Doctrine\ORM\Mapping as ORM;

/**
 * Customer
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\CustomerRepository")
 */
class Customer extends PersonAddress implements UserInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Segment
     *
     * @ORM\ManyToOne(targetEntity="Segment")
     */
    private $segment;

    /**
     * @var credit_cards
    
     */
    private $creditCards;

      /**
     * @var string
     *
     * @ORM\Column(name="codsexo", type="string", length=1, nullable=true)
     */
    protected $codSexo;


    /**
     * Set codsexo
     *
     * @param string $codsexo
     * @return Payer
     */
    public function setCodSexo($codSexo)
    {
        $this->codSexo = $codSexo;

        return $this;
    }

    /**
     * Get codsexo
     *
     * @return codsexo
     */
    public function getCodSexo()
    {
        return $this->codSexo;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

   /**
     * Set segment
     *
     * @param \AppBundle\Entity\Segment $segment
     * @return Home
     */
    public function setSegment(\AppBundle\Entity\Segment $segment = null)
    {
        $this->segment = $segment;

        return $this;
    }

    /**
     * Get segment
     *
     * @return \AppBundle\Entity\Segment
     */
    public function getSegment()
    {
        return $this->segment;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }



    /**
     * {@inheritdoc}
     */
    public function isAccountNonExpired()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function isAccountNonLocked()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function isCredentialsNonExpired()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function isEnabled()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function serialize()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function unserialize($serialized)
    {

    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function getPassword()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function getRoles()
    {
        return array('ROLE_USER');
    }

    /**
     * {@inheritdoc}
     */
    public function getSalt()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function getUsername()
    {
        return $this->getName();
    }

    /**
     * Get the value of Credit Cards
     *
     * @return credit_cards
     */
    public function getCreditCards()
    {
        return $this->creditCards;
    }

    /**
     * Set the value of Credit Cards
     *
     * @param credit_cards creditCards
     *
     * @return self
     */
    public function setCreditCards(array $creditCards)
    {
        $this->creditCards = $creditCards;

        return $this;
    }
}
