<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints as AppAssert;

/**
 * @ORM\MappedSuperclass()
 */
class PersonAddress extends BasePerson
{
    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     * @Assert\NotNull()
     */
    protected $address;

    /**
     * @var integer
     *
     * @ORM\Column(name="cod_state", type="integer", length=4, nullable=true)
     * @Assert\NotNull()
     */
    private $codState;

    /**
     * @var string
     *
     * @ORM\Column(name="address_state", type="string", length=255, nullable=true)
     * @Assert\NotNull()
     */
    private $addressState;

    /**
     * @var string
     *
     * @ORM\Column(name="address_city", type="string", length=255, nullable=true)
     * @Assert\NotNull()
     */
    protected $addressCity;

    /**
     * @var integer
     *
     * @ORM\Column(name="cod_city", type="integer", length=4, nullable=true)
     * @Assert\NotNull()
     */
    private $codCity;


    /**
     * @var string
     *
     * @ORM\Column(name="address_phone", type="string", length=48, nullable=true)
     */
    protected $addressPhone;

    /**
     * @var string
     *
     * @ORM\Column(name="address_cellphone", type="string", length=48, nullable=true)
     * @Assert\NotNull()
     */
    protected $addressCellphone;

    /**
     * @var string
     *
     * @ORM\Column(name="address_email", type="string", length=200, nullable=true)
     * @Assert\NotNull()
     * @Assert\Email()
     */
    protected $addressEmail;

   
    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Payer
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Set addressState
     *
     * @param string $addressState
     * @return Payer
     */
    public function setAddressState($addressState)
    {
        $this->addressState = $addressState;

        return $this;
    }

    /**
     * Get addressState
     *
     * @return string
     */
    public function getAddressState()
    {
        return $this->addressState;
    }

    /**
     * Set codState
     *
     * @param integer $codState
     * @return Payer
     */
    public function setCodState($codState)
    {
        $this->codState = $codState;

        return $this;
    }

    /**
     * Get codState
     *
     * @return integer
     */
    public function getCodState()
    {
        return $this->codState;
    }  


    /**
     * Set addressCity
     *
     * @param string $addressCity
     * @return Payer
     */
    public function setAddressCity($addressCity)
    {
        $this->addressCity = $addressCity;

        return $this;
    }

    /**
     * Get addressCity
     *
     * @return string
     */
    public function getAddressCity()
    {
        return $this->addressCity;
    }

    /**
     * Set codCity
     *
     * @param integer $codCity
     * @return Payer
     */
    public function setCodCity($codCity)
    {
        $this->codCity = $codCity;

        return $this;
    }

    /**
     * Get codCity
     *
     * @return cod
     */
    public function getCodCity()
    {
        return $this->codCity;
    }


    /**
     * Set addressPhone
     *
     * @param string $addressPhone
     * @return Payer
     */
    public function setAddressPhone($addressPhone)
    {
        $this->addressPhone = $addressPhone;

        return $this;
    }

    /**
     * Get addressPhone
     *
     * @return string
     */
    public function getAddressPhone()
    {
        return $this->addressPhone;
    }

    /**
     * Get addressCellphone
     *
     * @return string
     */
    public function getAddressCellphone()
    {
        return $this->addressCellphone;
    }

    /**
     * Set addressCellphone
     *
     * @param string $addressCellphone
     * @return Payer
     */
    public function setAddressCellphone($addressCellphone)
    {
        $this->addressCellphone = $addressCellphone;

        return $this;
    }

    /**
     * Set addressEmail
     *
     * @param string $addressEmail
     * @return Payer
     */
    public function setAddressEmail($addressEmail)
    {
        $this->addressEmail = $addressEmail;

        return $this;
    }

    /**
     * Get addressEmail
     *
     * @return string
     */
    public function getAddressEmail()
    {
        return $this->addressEmail;
    }
}
