<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Coverage
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\CoverageRepository")
 */
class Coverage implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="capital", type="decimal", scale=3)
     */
    private $capital;

    /**
     * @var integer
     *
     * @ORM\Column(name="days", type="integer" , nullable=true, options={"default" = 0})
     */
    private $days;

    /**
     * @ORM\ManyToOne(targetEntity="Product")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     **/
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity="Plan")
     * @ORM\JoinColumn(name="plan_id", referencedColumnName="id")
     **/
    private $plan;

    /**
     *
     * @ORM\Column(name="price", type="string",  nullable=true, length=255)
     */
    private $price;

    /**
     *
     * @ORM\Column(name="CODPLANCO", type="string",  nullable=true, length=10)
     */
    private $codplanco;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Coverage
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }


    public function setCodplanco($codplanco)
    {
        $this->codplanco = $codplanco;

        return $this;
    }

    /**
     * Get codPlanCo
     *
     * @return string
     */
    public function getCodplanco()
    {
        return $this->codplanco;
    }

    /**
     * Set capital
     *
     * @param string $capital
     * @return Coverage
     */
    public function setCapital($capital)
    {
        $this->capital = $capital;

        return $this;
    }

    /**
     * Get capital
     *
     * @return string
     */
    public function getCapital()
    {
        return $this->capital;
    }

    /**
     * Set product
     *
     * @param \AppBundle\Entity\Product $product
     * @return Coverage
     */
    public function setProduct(\AppBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \AppBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set plan
     *
     * @param \AppBundle\Entity\Plan $plan
     * @return Coverage
     */
    public function setPlan(\AppBundle\Entity\Plan $plan)
    {
        $this->plan = $plan;

        return $this;
    }

    /**
     * Get plan
     *
     * @return \AppBundle\Entity\Plan
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * Set days
     *
     * @param integer $days
     * @return Coverage
     */
    public function setDays($days)
    {
        $this->days = $days;

        return $this;
    }

    /**
     * Get days
     *
     * @return integer
     */
    public function getDays()
    {
        return $this->days;
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return $this->toArray();
    }

    /**
     * To array
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'capital' => $this->getCapital(),
            'days' => $this->getDays(),
            'price' => $this->getPrice(),
            'codplanco' => $this->getCodplanco(),
            'plan' => ( ($this->getPlan() != null) ? $this->getPlan()->toArray() : []) ,

        ];
    }

    /**
     * Get the value of CodeProducto
     *
     * @return string
     */
    public function getCodeProducto()
    {
        return $this->getProduct()->getId();
    }

    /**
     * Get the value of SegmentoProducto
     *
     * @return string
     */
    public function getSegmentoProducto()
    {
        return $this->getProduct()->getSegments();
    }

    /**
     * Get the value of ID de Plan
     *
     * @return string
     */
    public function getPlanId()
    {
        return $this->getPlan()->getId();
    }

}
