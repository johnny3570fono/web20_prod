<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Proposal
 *
 * @ORM\Table(name="proposal")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\ProposalRepository")
 */
class Proposal
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Insurance
     *
     * @ORM\OneToOne(targetEntity="Insurance")
     * @ORM\JoinColumn(name="insurance_id", referencedColumnName="id")
     */
    private $insurance;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @var int
     *
     * @ORM\Column(name="solicitud", type="integer")
     */
    private $solicitud;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=255)
     */
    private $estado;

    /**
     * @var float
     *
     * @ORM\Column(name="prima", type="float", precision=4, scale=10)
     */
    private $prima;

    /**
     * @var int
     *
     * @ORM\Column(name="poliza", type="integer")
     */
    private $poliza;

    /**
     * @var string
     *
     * @ORM\Column(name="cotizacion", type="integer")
     */
    private $cotizacion;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set solicitud
     *
     * @param int $solicitud
     * @return Proposal
     */
    public function setSolicitud($solicitud)
    {
        $this->solicitud = $solicitud;

        return $this;
    }

    /**
     * Get solicitud
     *
     * @return int
     */
    public function getSolicitud()
    {
        return $this->solicitud;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Proposal
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Proposal
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set prima
     *
     * @param float $prima
     * @return Proposal
     */
    public function setPrima($prima)
    {
        $this->prima = $prima;

        return $this;
    }

    /**
     * Get prima
     *
     * @return int
     */
    public function getPrima()
    {
        return $this->prima;
    }

    /**
     * Set poliza
     *
     * @param int $poliza
     * @return Proposal
     */
    public function setPoliza($poliza)
    {
        $this->poliza = $poliza;

        return $this;
    }

    /**
     * Get poliza
     *
     * @return int
     */
    public function getPoliza()
    {
        return $this->poliza;
    }

    /**
     * Set cotizacion
     *
     * @param int $cotizacion
     * @return Proposal
     */
    public function setCotizacion($cotizacion)
    {
        $this->cotizacion = $cotizacion;

        return $this;
    }

    /**
     * Get cotizacion
     *
     * @return int
     */
    public function getCotizacion()
    {
        return $this->cotizacion;
    }

    /**
     * To String
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getSolicitud();
    }

    /**
     * Set insurance
     *
     * @param \AppBundle\Entity\Insurance $insurance
     * @return Proposal
     */
    public function setInsurance(\AppBundle\Entity\Insurance $insurance = null)
    {
        $this->insurance = $insurance;

        return $this;
    }

    /**
     * Get insurance
     *
     * @return \AppBundle\Entity\Insurance
     */
    public function getInsurance()
    {
        return $this->insurance;
    }
}
