<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TargetAudienceCoverage
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\TargetAudienceCoverageRepository")
 */
class TargetAudienceCoverage implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var TargetAudience
     *
     * @ORM\ManyToOne(targetEntity="TargetAudience")
     */
    private $targetAudience;

    /**
     * @var string
     *
     * @ORM\Column(name="capital", type="decimal", precision=8, scale=2)
     */
    private $capital;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_by", type="integer")
     */
    private $orderBy;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set targetAudience
     *
     * @param \stdClass $targetAudience
     * @return TargetAudienceCoverage
     */
    public function setTargetAudience($targetAudience)
    {
        $this->targetAudience = $targetAudience;

        return $this;
    }

    /**
     * Get targetAudience
     *
     * @return \stdClass
     */
    public function getTargetAudience()
    {
        return $this->targetAudience;
    }

    /**
     * Set capital
     *
     * @param string $capital
     * @return TargetAudienceCoverage
     */
    public function setCapital($capital)
    {
        $this->capital = $capital;

        return $this;
    }

    /**
     * Get capital
     *
     * @return string
     */
    public function getCapital()
    {
        return $this->capital;
    }

    /**
     * Set orderBy
     *
     * @param integer $orderBy
     * @return TargetAudienceCoverage
     */
    public function setOrderBy($orderBy)
    {
        $this->orderBy = $orderBy;

        return $this;
    }

    /**
     * Get orderBy
     *
     * @return integer
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return TargetAudienceCoverage
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
     /**
     * To String
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getName();

    }


    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return $this->toArray();
    }

    /**
     * To array
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'capital' => $this->getCapital(),
        ];
    }
}
