<?php

namespace AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * SegmentRepository
 *
 */
class SegmentRepository extends EntityRepository
{
    /**
     * Buscar un segmento por su nombre.
     *
     * @param string $name
     * @return AppBundle:Segment
     */
    public function findByName($name)
    {
        $dql = 'SELECT s FROM AppBundle:Segment s WHERE LOWER(s.name) LIKE LOWER(:name)';

        return $this->getEntityManager()
            ->createQuery($dql)
            ->setParameter('name', $name)
            ->getSingleResult();
    }
}
