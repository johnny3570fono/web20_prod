<?php

namespace AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * CampaignRepository
 *
 */
class CampaignRepository extends EntityRepository
{
	 public function findUniqueTitle($title, $id = false){
        if($id) {
            $query = $this->createQueryBuilder('i')
            ->where('i.title = :title')
            ->andWhere('i.id <> (:id)')
            ->setParameter('id', $id)
            ->setParameter('title', $title)
            ->getQuery()->getResult();
        } else {
            $query = $this->createQueryBuilder('i')
            ->where('i.title = :title')
            ->setParameter('title', $title)
            ->getQuery()->getResult();
        }
        return $query;
    }

    public function findByProductToArray($product) {
        $query = $this->createQueryBuilder('u');
        $query->innerJoin('u.plans', 'g');
       
        if(method_exists($product, 'getPlans')) {
            foreach($product->getPlans() as $key => $plan){
                $query->orWhere("g.id =  :id$key" ) 
                ->setParameter("id$key",$plan->getId());    
            }
            $query->andWhere('u.startAt <= :now AND u.endAt >= :now')
            ->setParameter('now', new \DateTime('now'));
            if($result = $query->getQuery()->getResult()) {
                return array_map(function($campaign) {
                    if ($campaign) {
                        return [
                            'id' => $campaign->getId(),
                            'title' => $campaign->getTitle(),
                            'description' => $campaign->getDescription(),
                            'plans' => $campaign->getPlans()->toArray(),
                            'startAt' => $campaign->getStartAt(),
                            'endAt' => $campaign->getEndAt(),
                            
                        ];
                    }
                }, $result);
            }
        }
        return [];
    }

}
