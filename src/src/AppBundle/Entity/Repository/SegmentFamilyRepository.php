<?php

namespace AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * SegmentFamilyRepository
 *
 */
class SegmentFamilyRepository extends EntityRepository
{
    /**
     * Entregar el listado de familias asociados a un segmento.
     *
     * @param Segment $segment
     * @return DoctrineCollection
     */
    public function findAllBySegment($segment)
    {
        $dql = 'SELECT
                    f
                FROM
                    AppBundle:SegmentFamily f
                JOIN
                    f.segment s
                JOIN
                    f.family fa
                WHERE
                    f.segment = :segment
                    AND fa.parent IS NULL
                ORDER BY
                    f.orderBy ASC';

        return $this->getEntityManager()
            ->createQuery($dql)
            ->setParameter('segment', $segment)
            ->getResult();
    }

    /**
     * Entregar el listado de familias asociado aun segmento y que tengan
     * productos asociados.
     *
     * @param Segment $segment
     * @return Collection
     */
    public function findFamilyAndProductBySegment($segment)
    {
        $dql = 'SELECT
                    f
                FROM
                    AppBundle:SegmentFamily f
                JOIN
                    f.segment s
                JOIN
                    f.family fa
                JOIN
                    fa.products p
                WHERE
                    f.segment = :segment
                    AND fa.parent IS NULL
                GROUP BY f.id
                ORDER BY
                    f.orderBy ASC';

        return $this->getEntityManager()
            ->createQuery($dql)
            ->setParameter('segment', $segment)
            ->getResult();
    }
}
