<?php

namespace AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Activiad del Usuario
 */
class UserActivityRepository extends EntityRepository
{
    public function tentadoBorradorActivity(\DateTime $fecha =null,\DateTime $fecha_hasta =null){

        $fechaactual = new \DateTime('now'); // Have for example 2013-06-10 09:53:21
        $ayer = clone $fechaactual;
        $ayer->modify('-2 hours'); // Have 2013-06-11 00:00:00
        //$ayer->setTime(0, 0, 0); // Modify to 2013-06-10 00:00:00, beginning of the day
        //$fechaactual->setTime(0, 0, 0);
       


        if($fecha == null){
            $url = 'details';

            $query = $this->createQueryBuilder('i')
            ->where('i.createdAt > :fecha')
            ->andwhere('i.createdAt < :fechaactual')
            ->andwhere('i.url LIKE :url')
            ->setParameter('fecha', $ayer)
            ->setParameter('fechaactual', $fechaactual)
            ->setParameter('url', "%{$url}%")
            ->getQuery();

            $url = 'fraud';
            $query2 = $this->createQueryBuilder('i')
            ->where('i.createdAt > :fecha')
            ->andwhere('i.createdAt < :fechaactual')
            ->andwhere('i.url LIKE :url')
            ->setParameter('fecha', $ayer)
            ->setParameter('fechaactual', $fechaactual)
            ->setParameter('url', "%{$url}%")
            ->getQuery();
            
        }elseif($fecha != null && $fecha_hasta == null){
            $url = 'details';
            $param = clone $fecha;
            $desde = $param->modify('-1 day');

            $query = $this->createQueryBuilder('i')
            ->where('i.createdAt < :desde')
            //->andwhere('i.createdAt < :hasta')
            ->andwhere('i.url LIKE  :url')
            ->setParameter('desde', $fecha)
            //->setParameter('hasta', $fecha)
            ->setParameter('url', "%{$url}%")
            ->getQuery();

            $url = 'fraud';
            $query2 = $this->createQueryBuilder('i')
            ->where('i.createdAt < :desde')
            //->andwhere('i.createdAt < :hasta')
            ->andwhere('i.url LIKE  :url')
            ->setParameter('desde', $fecha)
            //->setParameter('hasta', $fecha_hasta)
            ->setParameter('url', "%{$url}%")
           
            ->getQuery();
        }else{

            $url = 'details';
            $param = clone $fecha;
            $desde = $param->modify('-1 day');

            $query = $this->createQueryBuilder('i')
            ->where('i.createdAt > :desde')
            ->andwhere('i.createdAt < :hasta')
            ->andwhere('i.url LIKE  :url')
            ->setParameter('desde', $fecha)
            ->setParameter('hasta', $fecha_hasta)
            ->setParameter('url', "%{$url}%")
            ->getQuery();

            $url = 'fraud';
            $query2 = $this->createQueryBuilder('i')
            ->where('i.createdAt > :desde')
            ->andwhere('i.createdAt < :hasta')
            ->andwhere('i.url LIKE  :url')
            ->setParameter('desde', $fecha)
            ->setParameter('hasta', $fecha_hasta)
            ->setParameter('url', "%{$url}%")
           
            ->getQuery();

        }

        $result = array_merge($query->getResult(),$query2->getResult());
              
        return $result;
    }

    public function tentadoBorrador(\DateTime $fecha =null, \DateTime $fecha_hasta =null){

        $fechaactual = new \DateTime('now'); // Have for example 2013-06-10 09:53:21
        $ayer = clone $fechaactual;
        $ayer->modify('-1 day'); // Have 2013-06-11 00:00:00
        $ayer->setTime(0, 0, 0); // Modify to 2013-06-10 00:00:00, beginning of the day
        $fechaactual->setTime(0, 0, 0);

        if($fecha == null){

             $query = $this->createQueryBuilder('i')
            ->where('i.createdAt > :fecha')
            ->andwhere('i.createdAt < :fechaactual')
            ->setParameter('fecha', $ayer)
            ->setParameter('fechaactual', $fechaactual)
            ->getQuery();
           
        }elseif($fecha != null && $fecha_hasta == null){
            
            $query = $this->createQueryBuilder('i')
            ->where('i.createdAt < :fecha')
            ->setParameter('fecha', $fecha)
            ->getQuery();          
            
        }else{

            $query = $this->createQueryBuilder('i')
            ->where('i.createdAt > :desde')
            ->andwhere('i.createdAt < :hasta')
            ->setParameter('desde', $fecha)
            ->setParameter('hasta', $fecha_hasta)
            ->getQuery();
       
        }
        return $query->getResult();
    }

}
