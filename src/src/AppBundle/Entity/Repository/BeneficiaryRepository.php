<?php

namespace AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * BeneficiaryRepository
 *
 */
class BeneficiaryRepository extends EntityRepository
{
	public function beneficiarios($id){

        $query = $this->createQueryBuilder('i')
        ->where('i.insurance = :dato')
        ->setParameter('dato', $id)
        ->getQuery()->getResult();
         
        return $query;
    }
}
