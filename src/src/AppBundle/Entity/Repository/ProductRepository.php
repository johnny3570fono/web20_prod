<?php

namespace AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * ProductRepository
 *
 */
class ProductRepository extends EntityRepository
{
    /**
     * Entrega un producto solo si el id existe y el producto es del tipo
     * indicado en el parametro $type
     *
     * @param integer $id
     * @param mixed $type
     *
     * @return \AppBundle\Entity\Product|null
     */
    public function findOneType($id, $type)
    {
        $insurance = $this->find($id);

        if ($insurance instanceof \AppBundle\Entity\Product) {
            if ($insurance->getType() == $type) {
                return $insurance;
            }
        }

        return null;
    }

    /**
     * Entrega un producto solo si el id existe y el producto es del tipo buscado.
     *
     * @param integer $id
     * @return \AppBundle\Entity\Product
     */
    public function findOneFraud($id)
    {
        return $this->findOneType($id, \AppBundle\Entity\BaseProduct::TYPE_FRAUD);
    }

    /**
     * Entrega un producto solo si el id existe y el producto es del tipo buscado.
     *
     * @param integer $id
     * @return \AppBundle\Entity\Product
     */
    public function findOneLife($id)
    {
        return $this->findOneType($id, \AppBundle\Entity\BaseProduct::TYPE_LIFE);
    }

    /**
     * Entrega un producto solo si el id existe y el producto es del tipo buscado.
     *
     * @param integer $id
     * @return \AppBundle\Entity\Product
     */
    public function findOneHealth($id)
    {
        return $this->findOneType($id, \AppBundle\Entity\BaseProduct::TYPE_HEALTH);
    }

    /**
     * Entrega un producto solo si el id existe y el producto es del tipo buscado.
     *
     * @param integer $id
     * @return \AppBundle\Entity\Product
     */
    public function findOneHome($id)
    {
        return $this->findOneType($id, \AppBundle\Entity\BaseProduct::TYPE_HOME);
    }

    /**
     * Entrega un producto solo si el id existe y el producto es del tipo buscado.
     *
     * @param integer $id
     * @return \AppBundle\Entity\Product
     */
    public function findOneTravel($id)
    {
        return $this->findOneType($id, \AppBundle\Entity\BaseProduct::TYPE_TRAVEL);
    }
     /**
     * Entrega un producto solo si el id existe y el producto es del tipo buscado.
     *
     * @param integer $id
     * @return \AppBundle\Entity\Product
     */
    public function findOneTravels($id)
    {
        return $this->findOneType($id, \AppBundle\Entity\BaseProduct::TYPE_TRAVELS);
    }
     /**
     * Entrega un producto solo si el id existe y el producto es del tipo buscado.
     *
     * @param integer $id
     * @return \AppBundle\Entity\Product
     */
    public function findDesc($product,$uf)
    {

        $str = $product->getDescription();

        $valores = explode(" ",$str);
        //var_dump($valores[32]);
        //die();
        $cant = count($valores);
        
        $valor = "";
        $print = "";
        $txt1="qwerty";

        for ($i=0; $i < $cant ; $i++) {
        
            if($valores[$i] == "UF")
            {
                $txt1 = $valores[$i+1];
                $val = (float) str_replace(',','.',trim($txt1));
                $mostrar = str_replace('.',',',$val);
                $print = $print."$".number_format($val*$uf->getValue(), 0, ",", ".")." ";
                $print =  $print."(UF ".$mostrar.")";
            }
            else
            {   
                if((float) trim($valores[$i]) > 0 && $valores[$i-1] == 'UF'){
                    

                    $eliminar = strlen($val); 
                    $print = $print.substr($valores[$i],$eliminar)." ";
                }
                else{
                    $print = $print.$valores[$i]." ";
                }
            }
    
        }
        return $print;

    }
    public function getProduct()
    {
        $dql = $this->createQueryBuilder('pr');

        $result = $dql->select('pr.name, pr.id , pl.id as idplan, pl.name as plan')
            ->join('AppBundle:Plan','pl')
            ->where('pr.id = pl.product')
            ->getQuery()
            ->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);

      
       /* $data = [];
       
        foreach ($result as $key => $r){
        $data[$key] = [
          'nombre' => $r['nombre'],
          'id' => $r['id'],
          'descripcion' => strip_tags($r['descripcion']),
        ];
      }  */    

        return $result;
    }
}
