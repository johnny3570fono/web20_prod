<?php

namespace AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * InsuranceRepository
 *
 */
class InsuranceRepository extends EntityRepository
{
    public function findAllIssuedByDateTimeAndType(\DateTime $fecha =null,\DateTime $fecha_hasta=null, $type)
    {

        $fechaactual = new \DateTime('now'); // Have for example 2013-06-10 09:53:21
        $ayer = clone $fechaactual;
        $ayer->modify('-1 day'); // Have 2013-06-11 00:00:00
        $ayer->setTime(0, 0, 0); // Modify to 2013-06-10 00:00:00, beginning of the day
       
        $fechaactual->setTime(0, 0, 0);

         if (($fecha==null) and ($fecha_hasta==null)){
          
            $query = $this->createQueryBuilder('i')
            ->where('i.status = :status')
            ->andWhere('i.type in (:type)')
            ->andWhere('i.created >= :ayer')
            ->andWhere('i.created <= :fechaactual')
            ->setParameter('status', \AppBundle\Entity\Insurance::STATUS_ISSUED)
            ->setParameter('type', $type)
            ->setParameter('fechaactual', $fechaactual)
            ->setParameter('ayer', $ayer)
            ->getQuery();
        
        }elseif($fecha!=null){
            if($fecha_hasta==null){
              
                $query = $this->createQueryBuilder('i')
                ->where('i.status = :status')
                ->andWhere('i.type in (:type)')
                ->andWhere('i.created < :fecha')
                ->setParameter('status', \AppBundle\Entity\Insurance::STATUS_ISSUED)
                ->setParameter('type', $type)
                ->setParameter('fecha', $fecha)
                ->getQuery();
        }else{
            if($fecha>$fecha_hasta){
                $aux=$fecha;
                $fecha=$fecha_hasta;
                $fecha_hasta=$aux;
            }
            $query = $this->createQueryBuilder('i')
                ->where('i.status = :status')
                ->andWhere('i.type in (:type)')
                ->andwhere('i.created >= :fechadesde')
                ->andWhere('i.created <= :fechahasta')
                ->setParameter('status', \AppBundle\Entity\Insurance::STATUS_ISSUED)
                ->setParameter('type', $type)
                ->setParameter('fechahasta', $fecha_hasta)
                ->setParameter('fechadesde', $fecha)
                ->getQuery();

        }
        
    }

        return $query->getResult();
    }

  
    


    /**
     * Entrega un seguro solo si el id existe y el seguro se encuentra con estado
     * borrador.
     *
     * @param integer $id
     * @return \AppBundle\Entity\Insurance
     */
    public function findOneDraft($id)
    {
        return $this->findOneStatus($id, \AppBundle\Entity\Insurance::STATUS_DRAFT);
    }

    /**
     * Entrega un seguro solo si el id existe y el seguro está emitido.
     *
     * @param integer $id
     * @return \AppBundle\Entity\Insurance
     */
    public function findOneIssued($id)
    {
        return $this->findOneStatus($id, \AppBundle\Entity\Insurance::STATUS_ISSUED);
    }

    /**
     * Entrega un seguro solo si el id existe y el seguro esta en el que se
     * entrega en el parametro $status;
     *
     * @param integer $id
     * @param mixed $status
     *
     * @return \AppBundle\Entity\Insurance
     */
    public function findOneStatus($id, $status)
    {
        $insurance = $this->find($id);

        if ($insurance instanceof \AppBundle\Entity\Insurance) {
            if ($insurance->getStatus() == $status) {
                return $insurance;
            }
        }

        return null;
    }

    public function findlast($rut)
    {
            $em = $this->getEntityManager();
            $dql ="SELECT i.updated FROM AppBundle:Insurance i
            JOIN i.insured insu
            WHERE insu.code = :rut
            AND i.status=:status
            ORDER BY i.updated DESC";
       
        $query = $em->createQuery($dql)
        ->setParameter('rut',$rut)
        ->setParameter('status','emitido');
    
        $fechaupdate = $query->setMaxResults(1)->getOneOrNullResult();
   
        return $fechaupdate;

    }

     public function tentadoBorrador(\DateTime $fecha =null,\DateTime $fecha_hasta =null){

        $fechaactual = new \DateTime('now'); // Have for example 2013-06-10 09:53:21
        $ayer = clone $fechaactual;
        $ayer->modify('-2 hours'); // Have 2013-06-11 00:00:00
        //$ayer->setTime(0, 0, 0); // Modify to 2013-06-10 00:00:00, beginning of the day
        //$fechaactual->setTime(0, 0, 0);

        if($fecha == null){
            
            $query = $this->createQueryBuilder('i')
            ->where('i.created > :fecha')
            ->andwhere('i.created < :fechaactual')
            ->andwhere('i.status = :status')
            ->setParameter('fecha', $ayer)
            ->setParameter('fechaactual', $fechaactual)
            ->setParameter('status', \AppBundle\Entity\Insurance::STATUS_DRAFT)
            ->getQuery();
            
        }elseif($fecha != null && $fecha_hasta == null){
            
            //$param = clone $fecha;
            //$desde = $param->modify('-1 day');
            $query = $this->createQueryBuilder('i')
            ->where('i.created < :desde')
            //->andwhere('i.created < :hasta')
            ->andwhere('i.status = :status')
            ->setParameter('desde', $fecha)
           // ->setParameter('hasta', $fecha)
            ->setParameter('status', \AppBundle\Entity\Insurance::STATUS_DRAFT)
           
            ->getQuery();
        }else{

           
             $query = $this->createQueryBuilder('i')
            ->where('i.created > :fecha')
            ->andwhere('i.created < :fechaactual')
            ->andwhere('i.status = :status')
            ->setParameter('fecha', $fecha)
            ->setParameter('fechaactual', $fecha_hasta)
            ->setParameter('status', \AppBundle\Entity\Insurance::STATUS_DRAFT)
            ->getQuery();

        }
        //var_dump(count($query->getResult()));
        return $query->getResult();
    }

     public function ventasBorrador(\DateTime $fecha =null,\DateTime $fecha_hasta =null){

        $fechaactual = new \DateTime('now'); // Have for example 2013-06-10 09:53:21
        $ayer = clone $fechaactual;
        $ayer->modify('-1 day'); // Have 2013-06-11 00:00:00
        $ayer->setTime(0, 0, 0); // Modify to 2013-06-10 00:00:00, beginning of the day
        $fechaactual->setTime(0, 0, 0);

        
        if($fecha == null){
            
            $query = $this->createQueryBuilder('i')
            ->where('i.created > :fecha')
            ->andwhere('i.created < :fechaactual')
            ->andwhere('i.status = :status')
            ->setParameter('fecha', $ayer)
            ->setParameter('fechaactual', $fechaactual)
            ->setParameter('status', \AppBundle\Entity\Insurance::STATUS_ISSUED)
            ->getQuery();
            
        }elseif($fecha != null && $fecha_hasta == null){
            $param = clone $fecha;
            $desde = $param->modify('-1 day');

            $query = $this->createQueryBuilder('i')
            ->where('i.created < :desde')
            //->andwhere('i.created < :hasta')
            ->andwhere('i.status = :status')
            ->setParameter('desde', $fecha)
            //->setParameter('hasta', $fecha)
            ->setParameter('status', \AppBundle\Entity\Insurance::STATUS_ISSUED)
           
            ->getQuery();
        }else{
            
            $query = $this->createQueryBuilder('i')
            ->where('i.created > :desde')
            ->andwhere('i.created < :hasta')
            ->andwhere('i.status = :status')
            ->setParameter('desde', $fecha)
            ->setParameter('hasta', $fecha_hasta)
            ->setParameter('status', \AppBundle\Entity\Insurance::STATUS_ISSUED)
           
            ->getQuery();

        }
        //var_dump(count($query->getResult()));
        return $query->getResult();
    }
}
