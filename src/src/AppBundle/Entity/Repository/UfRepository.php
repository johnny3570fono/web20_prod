<?php

namespace AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Unidad de Fomento
 */
class UfRepository extends EntityRepository
{
    /**
     * Entrega el registro más nuevo con valor mayor a cero.
     *
     * @return Collection
     */
    public function findLast()
    {
        $today = new \DateTime('now');

        $dql = 'SELECT
                    u
                FROM
                    AppBundle:Uf u
                WHERE
                    u.day <= :day
                    AND u.value > 0
                ORDER BY u.day DESC';

        return $this->getEntityManager()
            ->createQuery($dql)
            ->setParameter('day', $today)
            ->setMaxResults(1)
            ->getSingleResult();
    }
   
}
