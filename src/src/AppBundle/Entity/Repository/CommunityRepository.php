<?php

namespace AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * CommunityRepository
 *
 */
class CommunityRepository extends EntityRepository
{
    /**
     * Entrega un array de comunas
     *
     * @return array
     */
    public function findAllAsArrayOfName()
    {
        $dql = $this->createQueryBuilder('c');

        $result = $dql->select('c.descripcion_comuna_homologo,c.descripcion_cuidad_homologo')
            ->orderBy('c.descripcion_comuna_homologo')
            ->getQuery()
            ->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);

        $data = [];

        foreach ($result as $r) {
            $data[$r['descripcion_comuna_homologo']] = $r['descripcion_comuna_homologo'];
        }

        return $data;
    }
    public function findAllAsArrayOfCod()
    {
        $dql = $this->createQueryBuilder('c');

        $result = $dql->select('c.descripcion_comuna_homologo,c.descripcion_cuidad_homologo')
            ->orderBy('c.codigo_comuna_homologo')
            ->getQuery()
            ->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);

        $data = [];

        foreach ($result as $r) {
            $data[$r['descripcion_comuna_homologo']] = $r['descripcion_cuidad_homologo'];
        }

        return $data;
    }
}
