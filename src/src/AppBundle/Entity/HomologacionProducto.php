<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HomologacionProducto
 *
 * @ORM\Table(name="homol_productos")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\HomologacionProductoRepository")
 */
class HomologacionProducto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="product_id", type="integer")
     * @ORM\Id
     */
    private $product_id;

    /**
     * @var string
     *
     * @ORM\Column(name="pri",  type="string", length=5)
     * @ORM\Id
     */
    private $pri;

    /**
     * @var string
     *
     *  @ORM\Column(name="product_code",  type="string", length=3)
     */
    private $product_code;

    /**
     * @var integer
     *
     * @ORM\Column(name="procod", type="integer")
     */
    private $procod;

    /**
     * @var string
     *
     *  @ORM\Column(name="proplncod",  type="string", length=6)
     */
    private $proplncod;

    /**
     * Set id
     *
     * @param int $product_id
     * @return HomologacionProducto
     */
    public function setId($product_id)
    {
        $this->product_id = $product_id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->product_id;
    }

    /**
     * Set pri
     *
     * @param string $pri
     * @return HomologacionProducto
     */
    public function setPri($pri)
    {
        $this->pri = $pri;

        return $this;
    }

    /**
     * Get pri
     *
     * @return string
     */
    public function getPri()
    {
        return $this->pri;
    }

    /**
     * Set product_code
     *
     * @param string $product_code
     * @return HomologacionProducto
     */
    public function setProductCode($product_code)
    {
        $this->product_code = $product_code;

        return $this;
    }

    /**
     * Get product_code
     *
     * @return string
     */
    public function getProductCode()
    {
        return $this->product_code;
    }

    /**
     * Set procod
     *
     * @param integer $procod
     * @return HomologacionProducto
     */
    public function setProCod($procod)
    {
        $this->procod = $procod;

        return $this;
    }

    /**
     * Get procod
     *
     * @return integer
     */
    public function getProCod()
    {
        return $this->procod;
    }

    /**
     * Set proplncod
     *
     * @param string $proplncod
     * @return HomologacionProducto
     */
    public function setProPlnCod($proplncod)
    {
        $this->proplncod = $proplncod;

        return $this;
    }

    /**
     * Get proplncod
     *
     * @return string
     */
    public function getProPlnCod()
    {
        return $this->proplncod;
    }
}
