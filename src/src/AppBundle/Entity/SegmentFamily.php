<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\GroupSequenceProviderInterface;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * SegmentFamily
 *
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(columns={"segment_id", "family_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\SegmentFamilyRepository")
 * @UniqueEntity(
 *      fields={"segment", "family"},
 *      message="El segmento ya tiene asociado la familia",
 *      groups={"all"}
 * )
 * @Vich\Uploadable
 * @Assert\GroupSequenceProvider
 */
class SegmentFamily implements GroupSequenceProviderInterface, \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \stdClass
     *
     * @ORM\ManyToOne(targetEntity="Segment", inversedBy="segmentFamilies", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(groups={"all"})
     */
    private $segment;

    /**
     * @var \stdClass
     *
     * @ORM\ManyToOne(targetEntity="Family", inversedBy="segmentFamilies")
     
     * @Assert\NotBlank(groups={"all"})
     */
    private $family;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=36)
     * @Assert\NotBlank(groups={"all"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     * @Assert\NotBlank(groups={"all"})
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="order_by", type="integer", nullable=true, options={"default" = 0})
     */
    private $orderBy;

    /**
     * @ORM\Column(name="icon", type="string", nullable=true)
     */
    private $icon;

    /**
     * @var File
     *
     * @Vich\UploadableField(mapping="segment_family_image", fileNameProperty="icon")
     * @Assert\File(
     *     groups={"all"},
     *     maxSize = "50k",
     *     mimeTypes = {"image/png", "image/jpeg", "image/jpg"}
     * )
     */
    private $iconFile;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return SegmentFamily
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return SegmentFamily
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get orderBy
     *
     * @return integer
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * Set orderBy
     *
     * @param integer $orderBy
     * @return integer
     */
    public function setOrderBy($orderBy)
    {
        $this->orderBy = $orderBy;

        return $this;
    }

    /**
     * Set icon
     *
     * @param string $icon
     * @return SegmentFamily
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Sets file.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $iconFile
     */
    public function setIconFile(File $iconFile = null)
    {
        $this->iconFile = $iconFile;

        if ($iconFile) {
            $this->updatedAt = new \DateTime;
        }
    }

    /**
     * Get iconFile
     *
     * @return File|\Symfony\Component\HttpFoundation\File\UploadedFile
     */
    public function getIconFile()
    {
        return $this->iconFile;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return SegmentFamily
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set segment
     *
     * @param \AppBundle\Entity\Segment $segment
     * @return SegmentFamily
     */
    public function setSegment(\AppBundle\Entity\Segment $segment = null)
    {
        $this->segment = $segment;

        return $this;
    }

    /**
     * Get segment
     *
     * @return \AppBundle\Entity\Segment
     */
    public function getSegment()
    {
        return $this->segment;
    }

    /**
     * Set family
     *
     * @param \AppBundle\Entity\Family $family
     * @return SegmentFamily
     */
    public function setFamily(\AppBundle\Entity\Family $family = null)
    {
        $this->family = $family;

        return $this;
    }

    /**
     * Get family
     *
     * @return \AppBundle\Entity\Family
     */
    public function getFamily()
    {
        return $this->family;
    }

    /**
     * Padre de la familia
     *
     * @return null|Family
     */
    public function getParent()
    {
        return $this->getFamily()->getParent();
    }

    /**
     * Los productos asociados a la familia.
     *
     * @return ArrayCollection
     */
    public function getProducts()
    {
        $family = $this->getFamily();

        $products = $family->getProducts();
        $data = new ArrayCollection();

        foreach ($products as $product) {
            $segments = $product->getSegments();
            foreach ($segments as $segment) {
                if ($segment->getId() == $this->getSegment()->getId()) {
                    $data->add($product);
                }
            }
        }

        return $data;
    }

    /**
     * To String
     *
     * @return string
     */
    public function __toString()
    {
        $name = sprintf(
            '%s - %s',
            $this->getSegment()->getName(),
            $this->getFamily()->getName()
        );

        return $name;
    }

    /**
     * {@inheritdoc}
     */
    public function getGroupSequence()
    {
        $groups = ['all'];

        if (!$this->getIcon()) {
            $groups[] = 'files';
        }

        return $groups;
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'icon' => $this->getIcon(),
        ];
    }
}
