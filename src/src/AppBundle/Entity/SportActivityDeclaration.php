<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * SportActivityDeclaration
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\SportActivityDeclarationRepository")
 */
class SportActivityDeclaration
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="weight", type="decimal", nullable=true)
     * @Assert\LessThanOrEqual(value=170)
     * @Assert\GreaterThanOrEqual(value=40)
     */
    private $weight;

    /**
     * @var string
     *
     * @ORM\Column(name="height", type="decimal", nullable=true)
     * @Assert\LessThanOrEqual(value=220)
     * @Assert\GreaterThanOrEqual(value=100)
     */
    private $height;

    /**
     * @var string
     *
     * @ORM\Column(name="sport", type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $sport;

    /**
     * @var string
     *
     * @ORM\Column(name="activity", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $activity;

    /**
     * @ORM\OneToOne(targetEntity="Insurance", inversedBy="sport", cascade={"persist", "remove"})
     **/
    private $insurance;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set weight
     *
     * @param string $weight
     * @return SportActivityDeclaration
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return string
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set height
     *
     * @param string $height
     * @return SportActivityDeclaration
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return string
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set sport
     *
     * @param string $sport
     * @return SportActivityDeclaration
     */
    public function setSport($sport)
    {
        $this->sport = $sport;

        return $this;
    }

    /**
     * Get sport
     *
     * @return string
     */
    public function getSport()
    {
        return $this->sport;
    }

    /**
     * Set activity
     *
     * @param string $activity
     * @return SportActivityDeclaration
     */
    public function setActivity($activity)
    {
        $this->activity = $activity;

        return $this;
    }

    /**
     * Get activity
     *
     * @return string
     */
    public function getActivity()
    {
        return $this->activity;
    }

    /**
     * Set insurance
     *
     * @param \AppBundle\Entity\Insurance $insurance
     * @return SportActivityDeclaration
     */
    public function setInsurance(\AppBundle\Entity\Insurance $insurance = null)
    {
        $this->insurance = $insurance;

        return $this;
    }

    /**
     * Get insurance
     *
     * @return \AppBundle\Entity\Insurance
     */
    public function getInsurance()
    {
        return $this->insurance;
    }
}
