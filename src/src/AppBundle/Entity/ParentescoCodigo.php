<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ParentescoCodigo
 *
 * @ORM\Table()  
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\ParentescoCodigoRepository")
 */
class ParentescoCodigo {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;    

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(groups={"all"})     
     */
    private $codigo;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(groups={"all"})     
     */
    private $parentesco;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }   

    /**
     * Set codigo
     *
     * @param string $codigo
     * @return Parentesco
     */
    public function setCodigo($codigo) {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return integer 
     */
    public function getCodigo() {
        return $this->codigo;
    }
    
    /**
     * Set parentesco
     *
     * @param string $parentesco
     * @return Parentesco
     */
    public function setParentesco($parentesco) {
        $this->parentesco = $parentesco;

        return $this;
    }

    /**
     * Get parentesco
     *
     * @return string 
     */
    public function getParentesco() {
        return $this->parentesco;
    }

}
