<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityRepository;

use AppBundle\Entity\PersonAddress;

/**
 * Insurance
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\InsuranceRepository")
 */
class Insurance extends BaseProduct
{

    private $entityManager;


    const STATUS_DRAFT = 'borrador';
    const STATUS_ISSUED =  'emitido';
    const STATUS_SEND =  'enviado';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="code", type="string", length=3)
     * @Assert\NotBlank(groups={"all"})
     */
     protected $code;

    /**
     * @ORM\ManyToOne(targetEntity="Product")
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity="Segment")
     */
    private $segment;

    /**
     * @ORM\ManyToOne(targetEntity="SegmentFamily")
     */
    private $family;

    /**
     * @ORM\OneToOne(targetEntity="Payer", inversedBy="insurance", cascade={"persist", "remove"})
     */
    private $payer;

    /**
     * @var string
     *
     * @ORM\Column(name="plan", type="json_array", nullable=true)
     */
    private $plan;

     /**
     * @var string $pri
     * @ORM\Column(name="pri", type="string", length=5, nullable=true)
     */
    private $pri;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="travelDate", nullable=true)
     */
    private $travelDate;

    /**
     * @ORM\OneToOne(targetEntity="Car", mappedBy="insurance")
     **/
    private $car;

    /**
     * @ORM\OneToOne(targetEntity="Home", mappedBy="insurance", cascade={"persist", "remove"})
     **/
    private $home;

    /**
     * @ORM\OneToOne(targetEntity="CreditCard", mappedBy="insurance", cascade={"persist", "remove"})
     **/
    private $creditCard;

    /**
     * @ORM\OneToMany(
     *  targetEntity="Beneficiary",
     *  mappedBy="insurance",
     *  cascade={"persist", "remove"},
     *  orphanRemoval=true
     * )
     **/
    private $beneficiaries;

    /**
     * @ORM\OneToMany(
     *  targetEntity="Carga",
     *  mappedBy="insurance",
     *  cascade={"persist", "remove"},
     *  orphanRemoval=true
     * )
     **/
    private $cargas;

    /**
     * @ORM\OneToMany(
     *  targetEntity="InsuranceQuestion",
     *  mappedBy="insurance",
     *  cascade={"all"}
     * )
     **/
    private $questions;

    /**
     * @ORM\OneToOne(targetEntity="Insured", inversedBy="insurance", cascade={"persist", "remove"})
     */
    private $insured;

    /**
     * @ORM\OneToOne(targetEntity="SportActivityDeclaration", inversedBy="insurance", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $sport;

    /**
     * @var boolean
     *
     * @ORM\Column(name="coverage_and_exclusions", type="boolean", nullable=true, options={"default"=false})
     */
    private $coverageAndExclusions;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_method", type="string", length=100, nullable=true)
     */
    private $paymentMethod;

    /**
     * @var integer
     *
     * @ORM\Column(name="payment_day", type="string", nullable=true)
     */
    private $paymentDay;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="string", length=32)
     */
    private $status;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(name="updated", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updated;

     /**
     * @var string
     *
     * @ORM\Column(name="suscripcion", type="boolean", nullable=true, options={"default": true})
     */
    protected $suscripcion;


    /**
     * @ORM\OneToOne(targetEntity="Educational", mappedBy="insurance")
     **/ 
    private $educational;
     /**
     * Set description
     *
     * @param string $suscripcion
     * @return Product
     */
    public function setSuscripcion($suscripcion)
    {
        $this->suscripcion = $suscripcion;

        return $this;
    }

    /**
     * Get suscripcion
     *
     * @return string
     */
    public function getSuscripcion()
    {
        return $this->suscripcion;
    }
 

    /*VARIABLES DUMMY **/
    private $curso;
    private $renta;
    private $primaanual;

    


      /**
     * Set primaanual
     *
     * @param string $primaanual
     */
    public function setPrimaanual($primaanual)
    {
        $this->primaanual = $primaanual;
    }

    /**
     * Get primaanual
     *
     * @return string 
     */
    public function getPrimaanual()
    {
        return $this->primaanual;
    }

      /**
     * Set curso
     *
     * @param string $curso
     */
    public function setCurso($curso)
    {
        $this->curso = $curso;
    }

    /**
     * Get curso
     *
     * @return string 
     */
    public function getCurso()
    {
        return $this->curso;
    }



      /**
     * Set renta
     *
     * @param string $renta
     */
    public function setRenta($renta)
    {
        $this->renta = $renta;
    }

    /**
     * Get renta
     *
     * @return string 
     */
    public function getRenta()
    {
        return $this->renta;
    }
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pri
     *
     * @param string $pri
     */
    public function setPri($pri)
    {
        $this->pri = $pri;
    }

    /**
     * Get pri
     *
     * @return string 
     */
    public function getPri()
    {
        return $this->pri;
    }

    /**
     * Set product
     *
     * @param \stdClass $product
     * @return Insurance
     */
    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set payer
     *
     * @param Person $payer
     * @return Insurance
     */
    public function setPayer(PersonAddress $payer)
    {
        $payer->setInsurance($this);

        $this->payer = $payer;

        return $this;
    }

    /**
     * Get payer
     *
     * @return Person
     */
    public function getPayer()
    {
        return $this->payer;
    }

    /**
     * Set coverageAndExclusions
     *
     * @param boolean $coverageAndExclusions
     * @return Insurance
     */
    public function setCoverageAndExclusions($coverageAndExclusions)
    {
        $this->coverageAndExclusions = $coverageAndExclusions;

        return $this;
    }

    /**
     * Get coverageAndExclusions
     *
     * @return boolean
     */
    public function getCoverageAndExclusions()
    {
        return $this->coverageAndExclusions;
    }

    /**
     * Set paymentMethod
     *
     * @param string $paymentMethod
     * @return Insurance
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * Get paymentMethod
     *
     * @return string
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * Getter $paymentDay
     *
     * @return integer
     */
    public function getPaymentDay()
    {
        return $this->paymentDay;
    }

    /**
     * Setter $paymentDay
     *
     * @param integer $paymentDay
     * @return \AppBundle\Entity\Insurance
     */
    public function setPaymentDay($paymentDay)
    {
        $this->paymentDay = $paymentDay;

        return $this;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Insurance
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set car
     *
     * @param \AppBundle\Entity\Car $car
     * @return Insurance
     */
    public function setCar(\AppBundle\Entity\Car $car = null)
    {
        $car->setInsurance($this);

        $this->car = $car;

        return $this;
    }

    /**
     * Get car
     *
     * @return \AppBundle\Entity\Car
     */
    public function getCar()
    {
        return $this->car;
    }

    /**
     * Set home
     *
     * @param \AppBundle\Entity\Home $home
     * @return Insurance
     */
    public function setHome(\AppBundle\Entity\Home $home = null)
    {
        $home->setInsurance($this);

        $this->home = $home;

        return $this;
    }

    /**
     * Get home
     *
     * @return \AppBundle\Entity\Home
     */
    public function getHome()
    {
        return $this->home;
    }

    /**
     * Constructor
     *
     * @param \AppBundle\Entity\Segment $segment
     * @param \AppBundle\Entity\Family $family
     * @param \AppBundle\Entity\Product $product
     * @param \AppBundle\Entity\Plan $planes
     */
    public function __construct(Segment $segment, SegmentFamily $family, Product $product)
    {
         
        $this->setSegment($segment);
        $this->setFamily($family);
        $this->setProduct($product);

        $this->beneficiaries = new \Doctrine\Common\Collections\ArrayCollection();
        $this->cargas = new \Doctrine\Common\Collections\ArrayCollection();
        $this->questions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->setStatus(self::STATUS_DRAFT);
        $this->setCoverageAndExclusions(false);
    }

    /**
     * Add beneficiaries
     *
     * @param \AppBundle\Entity\Beneficiary $beneficiaries
     * @return Insurance
     */
    public function addBeneficiary(\AppBundle\Entity\Beneficiary $beneficiaries)
    {
        $beneficiaries->setInsurance($this);

        $this->beneficiaries[] = $beneficiaries;

        return $this;
    }

    /**
     * Remove beneficiaries
     *
     * @param \AppBundle\Entity\Beneficiary $beneficiaries
     */
    public function removeBeneficiary(\AppBundle\Entity\Beneficiary $beneficiaries)
    {
        $this->beneficiaries->removeElement($beneficiaries);
    }

    /**
     * Get beneficiaries
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBeneficiaries()
    {
        return $this->beneficiaries;
    }

    /**
     * To String
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getId();
    }

    /**
     * Getter
     *
     * @return Segment
     */
    public function getSegment()
    {
        return $this->segment;
    }

    /**
     * Getter
     *
     * @return Family
     */
    public function getFamily()
    {
        return $this->family;
    }

    /**
     * Setter
     *
     * @param Segment $segment
     * @return Insurance
     */
    public function setSegment($segment)
    {
        $this->segment = $segment;

        return $this;
    }

    /**
     * Setter
     *
     * @param SegmentFamily $family
     * @return Insurance
     */
    public function setFamily($family)
    {
        $this->family = $family;

        return $this;
    }

    /**
     * Getter
     *
     * @return string
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * Setter
     *
     * @param string $plan
     * @return Insurance
     */
    public function setPlan($plan)
    {
        $this->plan = $plan;

        return $this;
    }
    /**
     * Getter
     *
     * @return date
     */
    public function getTravelDate()
    {
        return $this->travelDate;
    }

    /**
     * Setter
     *
     * @param date $date
     * @return date
     */
    public function setTravelDate($date)
    {
        $this->travelDate = $date;

        return $this;
    }

    /**
     * Getter created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Getter Updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Setter $created
     *
     * @param \DateTime $created
     * @return \AppBundle\Entity\Insurance
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Setter $updated
     *
     * @param \DateTime $updated
     * @return \AppBundle\Entity\Insurance
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Desde una entidad Insured se copian datos a la entidad Payer
     *
     * @param Insured
     * @return null
     */
    public function insuredPassDataToPayer(PersonAddress $insured)
    {
        if ($this->getPayer() instanceof PersonAddress) {
            $this->getPayer()->setAddress($insured->getAddress());
            $this->getPayer()->setAddressState($insured->getAddressState());
            $this->getPayer()->setCodState($insured->getCodState());
            $this->getPayer()->setAddressCity($insured->getAddressCity());
            $this->getPayer()->setCodCity($insured->getCodCity());
            $this->getPayer()->setAddressPhone($insured->getAddressPhone());
            $this->getPayer()->setAddressCellphone($insured->getAddressCellphone());
            $this->getPayer()->setAddressEmail($insured->getAddressEmail());
            $this->getPayer()->setBirthday($insured->getBirthday());
           // $this->getPayer()->setCodeSexo($insured->getCodeSexo());
        }
    }

    /**
     * Entrega la instacia de una clase Insurance, con los datos de un Producto
     * existente.
     *
     * @param Product       $product
     * @param Segment       $segment
     * @param SegmentFamily $family
     * @return Insurance
     */
    public static function createFrom(Customer $customer, Product $product, Segment $segment, SegmentFamily $family)
    {
        $insurance = new Insurance($segment, $family, $product);

        $payer = new Payer();

        $payer->setName($customer->getName());
        $payer->setLastname1($customer->getLastname1());
        $payer->setLastname2($customer->getLastname2());
        $payer->setCode($customer->getCode());
        $payer->setAddress($customer->getAddress());
        $payer->setAddressState($customer->getAddressState());
        $payer->setCodState($customer->getCodState());
        $payer->setAddressCity($customer->getAddressCity());
        $payer->setCodCity($customer->getCodCity());
        $payer->setAddressPhone($customer->getAddressPhone());
        $payer->setAddressCellphone($customer->getAddressCellphone());
        $payer->setAddressEmail($customer->getAddressEmail());
        $payer->setBirthday($customer->getBirthday());
        $payer->setBirthday($customer->getBirthday());
        $payer->setCodSexo($customer->getCodSexo());

        $insured = new Insured();

        $insured->setName($customer->getName());
        $insured->setLastname1($customer->getLastname1());
        $insured->setLastname2($customer->getLastname2());
        $insured->setCode($customer->getCode());
        $insured->setAddress($customer->getAddress());
        $insured->setAddressState($customer->getAddressState());
        $insured->setCodState($customer->getCodState());
        $insured->setAddressCity($customer->getAddressCity());
        $insured->setCodCity($customer->getCodCity());
        $insured->setAddressPhone($customer->getAddressPhone());
        $insured->setAddressCellphone($customer->getAddressCellphone());
        $insured->setAddressEmail($customer->getAddressEmail());
        $insured->setBirthday($customer->getBirthday());

        $insurance->setPayer($payer);
        $insurance->setInsured($insured);

        $dataMethods = [
            'codFrecPago',
            'code',
            'name',
            'imageTitle',
            'imageSubtitle',
            'image',
            'title',
            'description',
            'coverage',
            'coverageNot',
            'howto',
            'pdf',
            'textLegal',
            'marketingText',
            'price',
            'type',
            'ageMin',
            'ageMax',
        ];

        foreach ($dataMethods as $m) {
            $setMethod = sprintf('set%s', ucfirst($m));
            $getMethod = sprintf('get%s', ucfirst($m));

            $insurance->{$setMethod}($product->{$getMethod}());
        }

        return $insurance;
    }

    /**
     * Set insured
     *
     * @param \AppBundle\Entity\Insured $insured
     * @return Insurance
     */
    public function setInsured(PersonAddress $insured = null)
    {
        $insured->setInsurance($this);

        $this->insured = $insured;

        return $this;
    }

    /**
     * Get insured
     *
     * @return \AppBundle\Entity\Insured
     */
    public function getInsured()
    {
        return $this->insured;
    }

    /**
     * Add cargas
     *
     * @param \AppBundle\Entity\Carga $cargas
     * @return Insurance
     */
    public function addCarga(\AppBundle\Entity\Carga $cargas)
    {
        $cargas->setInsurance($this);

        $this->cargas[] = $cargas;

        return $this;
    }

    /**
     * Remove cargas
     *
     * @param \AppBundle\Entity\Carga $cargas
     */
    public function removeCarga(\AppBundle\Entity\Carga $cargas)
    {
        $this->cargas->removeElement($cargas);
    }

    /**
     * Get cargas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCargas()
    {
        return $this->cargas;
    }

    /**
     * Set sport
     *
     * @param \AppBundle\Entity\SportActivityDeclaration $sport
     * @return Insurance
     */
    public function setSport(\AppBundle\Entity\SportActivityDeclaration $sport = null)
    {
        $this->sport = $sport;

        return $this;
    }

    /**
     * Get sport
     *
     * @return \AppBundle\Entity\SportActivityDeclaration
     */
    public function getSport()
    {
        return $this->sport;
    }

    /**
     * Add questions
     *
     * @param \AppBundle\Entity\InsuranceQuestion $questions
     * @return Insurance
     */
    public function addQuestion(\AppBundle\Entity\InsuranceQuestion $questions)
    {
        $questions->setInsurance($this);
        $this->questions->add($questions);

        return $this;
    }

    /**
     * Remove questions
     *
     * @param \AppBundle\Entity\InsuranceQuestion $questions
     */
    public function removeQuestion(\AppBundle\Entity\InsuranceQuestion $questions)
    {
        $this->questions->removeElement($questions);
    }

    /**
     * Get questions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    public function getAnswerByQuestion($question)
    {
        $questions = $this->getQuestions();
        $answer = null;

        foreach ($questions as $q) {
            if (strtolower($q->getQuestion()) == strtolower($question)) {
                $answer = $q->getAnswer();
            }
        }

        return $answer;
    }
    public function getEnfermedad()
    {
        $enfermedad=$this->getAnswerByQuestion('Enfermedad');

        if($enfermedad==null){
            $enfermedad=$this->getAnswerByQuestion('Emfermedad');

        }
        return $enfermedad;
        
    }

    public function getEstatura()
    {
        return $this->getAnswerByQuestion('estatura');
    }

    public function getPeso()
    {
        return $this->getAnswerByQuestion('peso');
    }

    /**
     * Retorna el código de frecuencia de pago del seguro.
     *
     * @return int
     */
    public function getCodigoFrecuenciaPago()
    {
        $code = 1;
        if ($this->getType() == self::TYPE_TRAVEL) {
            $code = 4;
        }

        return $code;
    }

    public function getCodFuente()
    {
        // 001: Banco
        // 002: Banefe

        return '242';
    }

    public function getNomEje()
    {
        // Len 30
        return 'Nombre Ejecutivo';
    }

    public function getNumPropuesta()
    {

       $propuesta=$this->getId();
           $cant=strlen($propuesta);

           $total=8-$cant;
           $Id='';
           for($i=0;$i< $total;$i++){
                    $Id.='0';
            }
           $propuesta=$Id.$propuesta;
          

               
               
            return $propuesta;

    }


    public function getCodMedPago()
    {
        //P = banco; T = tarjeta; E = efectivo o valevista; C = cuota comercio; Q = chequera; R = Regalo
         $paymethod=$this->getPaymentMethod();
                   
        if($paymethod!=null){
           $cant=strlen($paymethod);
           if($cant>12){
           
            $method='T';
            }else
            {
            $method='P';
            }
        }else{
           $method='P';
        }
        return $method;
    }

    /**
     * Set the value of Id
     *
     * @param integer id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Code
     *
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set the value of Code
     *
     * @param mixed code
     *
     * @return self
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get the value of Credit Card
     *
     * @return mixed
     */
    public function getCreditCard()
    {
        return $this->creditCard;
    }

    /**
     * Set the value of Credit Card
     *
     * @param mixed creditCard
     *
     * @return self
     */
    public function setCreditCard($creditCard)
    {
        $this->creditCard = $creditCard;

        return $this;
    }

    /**
     * Set the value of Beneficiaries
     *
     * @param mixed beneficiaries
     *
     * @return self
     */
    public function setBeneficiaries($beneficiaries)
    {
        $this->beneficiaries = $beneficiaries;

        return $this;
    }

    /**
     * Set the value of Cargas
     *
     * @param mixed cargas
     *
     * @return self
     */
    public function setCargas($cargas)
    {
        $this->cargas = $cargas;

        return $this;
    }

    /**
     * Set the value of Questions
     *
     * @param mixed questions
     *
     * @return self
     */
    public function setQuestions($questions)
    {
        $this->questions = $questions;

        return $this;
    }

    /**
     * Add planes
     *
     * @param \AppBundle\Entity\Plan $planes
     * @return Insurance
     */
    public function addPlanes(\AppBundle\Entity\Plan $planes)
    {
        $planes->setInsurance($this);
        $this->planes->add($planes);

        return $this;
    }

 
    /**
     * Get planes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlanes()
    {
        $planes = $this->getPlanes();
        return $planes;
    }




    /**
     * Set educational
     *
     * @param \AppBundle\Entity\Educational $educational
     * @return Insurance
     */
    public function setEducational(\AppBundle\Entity\Educational $educational = null)
    {
        $educational->setInsurance($this);
        $this->educational = $educational;

        return $this;
    }

    /**
     * Get educational
     *
     * @return \AppBundle\Entity\Educational 
     */
    public function getEducational()
    {
        return $this->educational;
    }
}
