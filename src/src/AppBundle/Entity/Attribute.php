<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Attribute
 *
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(columns={"name", "group_attribute_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\AttributeRepository")
 */
class Attribute
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="name_value", type="string", length=100)
     */
    private $nameValue;

    /**
     * @var string
     *
     * @ORM\Column(name="field_type", type="string", length=100)
     */
    private $fieldType;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="decimal", nullable=true)
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity="GroupAttribute")
     * @ORM\JoinColumn(nullable=false)
     */
    private $groupAttribute;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Attribute
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set groupAttribute
     *
     * @param \AppBundle\Entity\GroupAttribute $groupAttribute
     * @return Attribute
     */
    public function setGroupAttribute(\AppBundle\Entity\GroupAttribute $groupAttribute)
    {
        $this->groupAttribute = $groupAttribute;

        return $this;
    }

    /**
     * Get groupAttribute
     *
     * @return \AppBundle\Entity\GroupAttribute
     */
    public function getGroupAttribute()
    {
        return $this->groupAttribute;
    }

    /**
     * Getter
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Setter
     *
     * @param type $value
     * @return \AppBundle\Entity\Attribute
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * To String
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Getter $nameValue
     *
     * @return string
     */
    public function getNameValue()
    {
        return $this->nameValue;
    }

    /**
     * Getter $fieldType
     *
     * @return string
     */
    public function getFieldType()
    {
        return $this->fieldType;
    }

    /**
     * Setter $nameValue
     *
     * @param string $nameValue
     * @return \AppBundle\Entity\Attribute
     */
    public function setNameValue($nameValue)
    {
        $this->nameValue = $nameValue;

        return $this;
    }

    /**
     * Setter $fieldType
     *
     * @param string $fieldType
     * @return \AppBundle\Entity\Attribute
     */
    public function setFieldType($fieldType)
    {
        $this->fieldType = $fieldType;

        return $this;
    }
}
