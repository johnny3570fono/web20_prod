<?php

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * Educational
 * @ORM\Entity
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\EducationalRepository")
 */
class Educational
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var prima
     *
     * @ORM\Column(name="prima", type="json_array", nullable=true)
     */
    private $prima;
 
    /**
     * @var curso
     *
     * @ORM\Column(name="curso", type="json_array", nullable=true)
     */
    private $curso;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="date_now", type="datetime")
     */
    private $dateNow;

    /**
     * @ORM\ManyToOne(targetEntity="Renta", inversedBy="Educational")
     * @ORM\JoinColumn(name="renta_id", referencedColumnName="id")
     * @return integer
    */
    protected $renta;

        /**
     * * @ORM\Column(name="insurance_id", type="integer")
     * @return integer
    */
    protected $insuranceId;

    /**
     * @var Insurance
     *
     * @ORM\OneToOne(targetEntity="Insurance", inversedBy="educational")
     * @ORM\JoinColumn(nullable=false)
     */
    private $insurance;
    /**
     * Set renta
     *
     * @param \AppBundle\Entity\Renta $renta
     */
    public function setRenta(\AppBundle\Entity\Renta $renta)
    {
        $this->renta = $renta;
    }

    /**
     * Get renta
     *
     * @return \AppBundle\Entity\Renta
     */
    public function getRenta()
    {
        return $this->renta;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of Prima 
     *
     * @return string
     */
    public function getPrima()
    {
        return $this->prima;
    }

    /**
     * Set the value of Prima
     *
     * @param string prima
    */
    public function setPrima($prima)
    {
        $this->prima = $prima;

        return $this;
    }

    /**
     * Get the value of curso 
     *
     * @return string
     */
    public function getCurso()
    {
        return $this->curso;
    }

    /**
     * Set the value of Curso
     *
     * @param string curso
     *
     * @return self
     */
    public function setCurso($curso)
    {
        $this->curso = $curso;

        return $this;
    }
    
   /**
     * Set dateNow
     *
     * @param integer $dateNow
     * @return dateNow
     */
    public function setDateNow($dateNow)
    {
        $this->dateNow = $dateNow;

        return $this;
    }

    /**
     * Get dateNow
     *
     * @return integer
     */
    public function getDateNow()
    {
        return $this->dateNow;
    }




    /**
     * Set insurance
     *
     * @param \AppBundle\Entity\Insurance $insurance
     * @return Educational
     */
    public function setInsurance(\AppBundle\Entity\Insurance $insurance)
    {
        $this->insurance = $insurance;

        return $this;
    }

    /**
     * Get insurance
     *
     * @return \AppBundle\Entity\Insurance 
     */
    public function getInsurance()
    {
        return $this->insurance;
    }

    /**
     * Set insuranceId
     *
     * @param integer $insuranceId
     * @return Educational
     */
    public function setInsuranceId($insuranceId)
    {
        $this->insuranceId = $insuranceId;

        return $this;
    }

    /**
     * Get insuranceId
     *
     * @return integer 
     */
    public function getInsuranceId()
    {
        return $this->insuranceId;
    }
}
