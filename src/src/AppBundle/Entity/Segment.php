<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Segment
 *
 * @ORM\Table(name="segment")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\SegmentRepository")
 * @UniqueEntity("name")
 */
class Segment implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=32, unique=true)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="Product", mappedBy="segments", cascade={"persist", "remove"})
     **/
    private $products;

    /**
     * @ORM\OneToMany(targetEntity="SegmentFamily", mappedBy="segment", cascade={"persist", "remove"})
     **/
    private $segmentFamilies;

    /**
     * @ORM\OneToMany(targetEntity="SegmentAssist", mappedBy="segment")
     **/
    private $segmentAssists;

    /**
     * @ORM\OneToMany(targetEntity="Banner", mappedBy="segment", cascade={"persist", "remove"})
     **/
    private $banners;

    /**
     * @ORM\OneToMany(targetEntity="Customer", mappedBy="segment")
     */
    private $customers;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->segmentFamilies = new ArrayCollection();
        $this->segmentAssists = new ArrayCollection();
        $this->products = new ArrayCollection();
        $this->customers = new ArrayCollection();
        $this->banners = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Segment
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add products
     *
     * @param \AppBundle\Entity\Product $products
     * @return Segment
     */
    public function addProduct(\AppBundle\Entity\Product $products)
    {
        $this->products[] = $products;

        return $this;
    }

    /**
     * Remove products
     *
     * @param \AppBundle\Entity\Product $products
     */
    public function removeProduct(\AppBundle\Entity\Product $products)
    {
        $this->products->removeElement($products);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Add segmentFamilies
     *
     * @param \AppBundle\Entity\SegmentFamily $segmentFamilies
     * @return Segment
     */
    public function addSegmentFamily(\AppBundle\Entity\SegmentFamily $segmentFamilies)
    {
        $this->segmentFamilies[] = $segmentFamilies;

        return $this;
    }

    /**
     * Remove segmentFamilies
     *
     * @param \AppBundle\Entity\SegmentFamily $segmentFamilies
     */
    public function removeSegmentFamily(\AppBundle\Entity\SegmentFamily $segmentFamilies)
    {
        $this->segmentFamilies->removeElement($segmentFamilies);
    }

    /**
     * Get segmentFamilies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSegmentFamilies()
    {
        return $this->segmentFamilies;
    }

    /**
     * Add segmentAssists
     *
     * @param \AppBundle\Entity\SegmentAssist $segmentAssists
     * @return Segment
     */
    public function addSegmentAssist(\AppBundle\Entity\SegmentAssist $segmentAssists)
    {
        $this->segmentAssists[] = $segmentAssists;

        return $this;
    }

    /**
     * Remove segmentAssists
     *
     * @param \AppBundle\Entity\SegmentAssist $segmentAssists
     */
    public function removeSegmentAssist(\AppBundle\Entity\SegmentAssist $segmentAssists)
    {
        $this->segmentAssists->removeElement($segmentAssists);
    }

    /**
     * Get segmentAssists
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSegmentAssists()
    {
        return $this->segmentAssists;
    }

    /**
     * To String
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
        ];
    }

    /**
     * Add customers
     *
     * @param \AppBundle\Entity\Customer $customers
     * @return Segment
     */
    public function addCustomer(\AppBundle\Entity\Customer $customers)
    {
        $this->customers[] = $customers;

        return $this;
    }

    /**
     * Remove customers
     *
     * @param \AppBundle\Entity\Customer $customers
     */
    public function removeCustomer(\AppBundle\Entity\Customer $customers)
    {
        $this->customers->removeElement($customers);
    }

    /**
     * Get customers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCustomers()
    {
        return $this->customers;
    }

    /**
     * Add banners
     *
     * @param \AppBundle\Entity\Banner $banners
     * @return Segment
     */
    public function addBanner(\AppBundle\Entity\Banner $banners)
    {
        $this->banners[] = $banners;

        return $this;
    }

    /**
     * Remove banners
     *
     * @param \AppBundle\Entity\Banner $banners
     */
    public function removeBanner(\AppBundle\Entity\Banner $banners)
    {
        $this->banners->removeElement($banners);
    }

    /**
     * Get banners
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBanners()
    {
        return $this->banners;
    }
}
