<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Assist
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\AssistRepository")
 */
class Assist extends Category
{
    /**
     * @ORM\OneToMany(targetEntity="SegmentAssist", mappedBy="assist", cascade={"persist", "remove"})
     **/
    private $segmentAssists;

    /**
     * @ORM\ManyToMany(targetEntity="Product", mappedBy="assists")
     */
    private $products;

    /**
     * To String
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->products = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add products
     *
     * @param \AppBundle\Entity\Product $products
     * @return Assist
     */
    public function addProduct(\AppBundle\Entity\Product $products)
    {
        $this->products[] = $products;

        return $this;
    }

    /**
     * Remove products
     *
     * @param \AppBundle\Entity\Product $products
     */
    public function removeProduct(\AppBundle\Entity\Product $products)
    {
        $this->products->removeElement($products);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Add segmentAssists
     *
     * @param \AppBundle\Entity\SegmentAssist $segmentAssists
     * @return Assist
     */
    public function addSegmentAssist(\AppBundle\Entity\SegmentAssist $segmentAssists)
    {
        $this->segmentAssists[] = $segmentAssists;

        return $this;
    }

    /**
     * Remove segmentAssists
     *
     * @param \AppBundle\Entity\SegmentAssist $segmentAssists
     */
    public function removeSegmentAssist(\AppBundle\Entity\SegmentAssist $segmentAssists)
    {
        $this->segmentAssists->removeElement($segmentAssists);
    }

    /**
     * Get segmentAssists
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSegmentAssists()
    {
        return $this->segmentAssists;
    }
}
