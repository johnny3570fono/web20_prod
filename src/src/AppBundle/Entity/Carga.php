<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Carga
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\CargaRepository")
 * @ORM\AttributeOverrides({
 *      @ORM\AttributeOverride(name="code",
 *          column=@ORM\Column(
 *              nullable=true
 *          )
 *      ),
 * })
 */
class Carga extends PersonRelationship
{
    /**
     * @ORM\ManyToOne(targetEntity="Insurance", inversedBy="cargas", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $insurance;

    /**
     * Set insurance
     *
     * @param \AppBundle\Entity\Insurance $insurance
     * @return self
     */
    public function setInsurance(\AppBundle\Entity\Insurance $insurance = null)
    {
        $this->insurance = $insurance;

        return $this;
    }

    /**
     * Get insurance
     *
     * @return \AppBundle\Entity\Insurance
     */
    public function getInsurance()
    {
        return $this->insurance;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
