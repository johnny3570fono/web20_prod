<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\GroupSequenceProviderInterface;
use Doctrine\Common\Collections\Criteria;

/**
 * Product
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\ProductRepository")
 * @Vich\Uploadable
 * @Assert\GroupSequenceProvider
 */
class Product extends BaseProduct implements GroupSequenceProviderInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="Segment", inversedBy="products")
     * @ORM\JoinTable(name="segment_product")
     * @Assert\Count(min="1", groups={"all"})
     * */
    private $segments;

    /**
     * @ORM\ManyToMany(targetEntity="Stament", inversedBy="products")
     * @ORM\JoinTable(name="product_stament")
     * */
    private $staments;

    /**
     * @ORM\ManyToMany(targetEntity="Activity", inversedBy="products")
     * @ORM\JoinTable(name="product_activity")
     * */
    private $activities;

    /**
     * @ORM\ManyToOne(targetEntity="Family", inversedBy="products")
     * @ORM\JoinColumn(name="family_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank(groups={"all"})
     */
    private $family;
	
	
	 /**
     * @ORM\ManyToOne(targetEntity="Plantilla", inversedBy="products")
     * @ORM\JoinColumn(name="plantilla_id", referencedColumnName="id", nullable=true)
     * */
    private $plantilla;
	

    /**
     * @ORM\ManyToMany(targetEntity="Assist", inversedBy="products")
     * @ORM\JoinTable(name="product_assist")
     * */
    private $assists;

    /**
     * @ORM\OneToMany(targetEntity="Plan", mappedBy="product", cascade={"remove", "persist"})
     * @ORM\JoinTable(name="plan")
     * @ORM\OrderBy({"orderBy" = "ASC"})
     * */
    private $plans;

    /**
     * @ORM\ManyToMany(targetEntity="Attribute", cascade={"remove"})
     * @ORM\JoinTable(name="product_attribute")
     * */
    private $attributes;

    /**
     * @ORM\OneToMany(targetEntity="Coverage", mappedBy="product", cascade={"remove","persist"})
     * */
    private $coverages;

    /**
     * @ORM\OneToMany(targetEntity="GroupProductPlan", mappedBy="product")
     * */
    private $groupProductPlan;

    /**
     * @var File
     *
     * @Vich\UploadableField(mapping="poliza_seguro", fileNameProperty="pdf")
     * @Assert\File(
     *     maxSize = "1M",
     *     mimeTypes = {"application/pdf", "application/x-pdf"},
     *     mimeTypesMessage = "Please upload a valid PDF",
     *     groups={"all"}
     * )
     */
    private $pdfFile;

    /**
     * @var File
     *
     * @Vich\UploadableField(mapping="product", fileNameProperty="image")
     * @Assert\File(
     *     maxSize = "1M",
     *     mimeTypes = {"image/jpeg", "image/png"},
     *     groups={"all"}
     * )
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="form_type", type="string", length=100, nullable=true, options={"default"="\AppBundle\Form\InsuranceType"})
     */
    protected $formType = "\AppBundle\Form\InsuranceHomeType";

    /**
     * @var string
     *
     * @ORM\Column(name="form_template", type="string", length=100, nullable=true, options={"default"="asd.html.twig"})
     */
    protected $formTemplate = "home.html.twig";
	
	
	
    /**
     * @var string
     *
     * @ORM\Column(name="id_clicktocall",  type="string", length=255, nullable=true)
     */
    protected $idClicktocall;

    /**
     * @var string
     *
     * @ORM\Column(name="url_Clicktocall", type="string", length=255, nullable=true)
     */
    protected $urlClicktocall;

    /**
     * @var string
     *
     * @ORM\Column(name="glosaclicktocall", type="string", length=255, nullable=true)
     */
    protected $glosaClicktocall;

    /**
     * @var string
     *
     * @ORM\Column(name="seguro_terceros", type="boolean", nullable=true, options={"default": true})
     */
    protected $seguroTerceros;

    /**
     * @var string
     *
     * @ORM\Column(name="Permite_cotizar", type="boolean", nullable=true, options={"default": false})
     */
    protected $permiteCotizar;

    /**
     * @var string
     *
     * @ORM\Column(name="codeCorredora", type="string", length=3,nullable=true)
     */
    private $codeCorredora;

    /**
     * @var integer
     *
     * @ORM\Column(name="orderby", type="integer", length=10,nullable=true )
     */
    private $orderBy;

    /**
     * @ORM\Column(name="calculo_mayor", type="boolean")
     */
    private $calculoMayor;
    
        /**
     * @ORM\OneToMany(targetEntity="Parentesco", mappedBy="producto")
     */
    private $parentesco;
	
	
	 /**
	 * @ORM\Column(type="integer", options={"default": 0})
	 *
	 */
    private $patPass = 0;
	
	
	 /**
	 * @ORM\Column(type="integer", options={"default": 0})
	 *
	 */
    protected $isPublic = 0;
	
	
	/**
     * @var string
     *
     * @ORM\Column(name="title_internet", type="string", nullable=true)
     */
    protected $titleInternet;
	
	/**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", nullable=true)
     */
	protected $slug;
    
    /**
     * @var string
     *
     * @ORM\Column(name="instant_call", type="boolean", nullable=true, options={"default": true})
     */
    protected $instantCall;

    
    
	/**
     * Get the value of InstantCall
     *
     * @return string
     */
    public function getInstantCall() {
        return $this->instantCall;
    }
	
    /**
     * Set the value of InstantCall
     *
     * @param string instantCall
     *
     * @return self
     */
    public function setInstantCall($instantCall) {
        $this->instantCall = $instantCall;

        return $this;
    }


	/**
     * Get the value of Slug
     *
     * @return string
     */
    public function getSlug() {
        return $this->slug;
    }
	
    /**
     * Set the value of Slug
     *
     * @param string slug
     *
     * @return self
     */
    public function setSlug($slug) {
        $this->slug = $slug;

        return $this;
    }
	
	/**
     * Get the value of TitleInternet
     *
     * @return string
     */
    public function getTitleInternet() {
        return $this->titleInternet;
    }
	
    /**
     * Set the value of TitleInternet
     *
     * @param string titleinternet
     *
     * @return self
     */
    public function setTitleInternet($titleinternet) {
        $this->titleInternet = $titleinternet;

        return $this;
    }

	
	/**
     * Get the value of IsPublic
     *
     * @return string
     */
    public function getIsPublic() {
        return $this->isPublic;
    }
	
    /**
     * Set the value of IsPublic
     *
     * @param string ispublic
     *
     * @return self
     */
    public function setIsPublic($ispublic) {
        $this->isPublic = $ispublic;

        return $this;
    }
	
    /**
     * Get the value of Codecorredora
     *
     * @return string
     */
    public function getCodeCorredora() {
        return $this->codeCorredora;
    }

    /**
     * Set the value of codeCorredora
     *
     * @param string codeCorredora
     *
     * @return self
     */
    public function setCodeCorredora($codeCorredora) {
        $this->codeCorredora = $codeCorredora;

        return $this;
    }

    /**
     * Getter $orderBy
     *
     * @return integer
     */
    public function getOrderBy() {
        return $this->orderBy;
    }

    /**
     * Setter $orderBy
     *
     * @param integer $orderBy
     * @return \AppBundle\Entity\BaseProduct
     */
    public function setOrderBy($orderBy) {
        $this->orderBy = $orderBy;

        return $this;
    }

    /**
     * Get productos que PermiteCotizar
     *
     * @return string
     */
    public function getPermiteCotizar() {
        return $this->permiteCotizar;
    }

    /**
     * Set the value of PermiteCotizar
     *
     * @param string permiteCotizar
     *
     * @return self
     */
    public function setPermiteCotizar($permiteCotizar) {
        $this->permiteCotizar = $permiteCotizar;

        return $this;
    }

    public function hasPermiteCotizar() {
        return ((boolean) $this->getPermiteCotizar() === true);
    }

    /**
     * Get the value of seguroTerceros
     *
     * @return string
     */
    public function getSeguroTerceros() {
        return $this->seguroTerceros;
    }

    /**
     * Set the value of seguroTerceros
     *
     * @param string seguroTerceros
     *
     * @return self
     */
    public function setSeguroTerceros($seguroTerceros) {
        $this->seguroTerceros = $seguroTerceros;

        return $this;
    }

    public function hasSeguroTerceros() {
        return ((boolean) $this->getSeguroTerceros() === true);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get the value of Redirect Url click to call
     *
     * @return string
     */
    public function getUrlClicktocall() {
        return $this->urlClicktocall;
    }

    /**
     * Set the value of Redirect Url click to call
     *
     * @param string urlClicktocall
     *
     * @return self
     */
    public function setUrlClicktocall($urlClicktocall) {
        $this->urlClicktocall = $urlClicktocall;

        return $this;
    }

    /**
     * Get the value of cod click to call
     *
     * @return string
     */
    public function getIdClicktocall() {
        return $this->idClicktocall;
    }

    /**
     * Set the value of cod click to call
     *
     * @param string urlClicktocall
     *
     * @return self
     */
    public function setIdClicktocall($idClicktocall) {
        $this->idClicktocall = $idClicktocall;

        return $this;
    }

    /**
     * Get the value of glosa Url click to call
     *
     * @return string
     */
    public function getGlosaClicktocall() {
        return $this->glosaClicktocall;
    }

    /**
     * Set the value of glosaClicktocall 
     *
     * @param string glosaClicktocall
     *
     * @return self
     */
    public function setGlosaClicktocall($glosaClicktocall) {
        $this->glosaClicktocall = $glosaClicktocall;

        return $this;
    }

    /**
     *
     * @return File
     */
    public function getImageFile() {
        return $this->imageFile;
    }

    /**
     *
     * @return File
     */
    public function getPdfFile() {
        return $this->pdfFile;
    }

    /**
     * Sets file.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $imageFile
     */
    public function setImageFile(File $imageFile = null) {
        $this->imageFile = $imageFile;

        if ($imageFile) {
            $this->updatedAt = new \DateTime;
        }
    }

    /**
     * Sets file.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $pdfFile
     */
    public function setPdfFile(File $pdfFile = null) {
        $this->pdfFile = $pdfFile;

        if ($pdfFile) {
            $this->updatedAt = new \DateTime;
        }
    }

    /**
     * Set family
     *
     * @param null|AppBundle\Entity\Family $family
     * @return Product
     */
    public function setFamily($family) {
        $this->family = $family;

        return $this;
    }

    /**
     * Get family
     *
     * @return \AppBundle\Entity\Family
     */
    public function getFamily() {
        return $this->family;
    }

    /**
     * Get parentFamilyName
     *
     * @return string
     */
    public function getMasterFamily() {
        return ( $this->family->getParent() ? $this->family->getParent()->getName() : $this->family->__toString() );
    }

    /**
     * Add assists
     *
     * @param \AppBundle\Entity\Assist $assists
     * @return Product
     */
    public function addAssist(\AppBundle\Entity\Assist $assists) {
        $this->assists[] = $assists;

        return $this;
    }

    /**
     * Remove assists
     *
     * @param \AppBundle\Entity\Assist $assists
     */
    public function removeAssist(\AppBundle\Entity\Assist $assists) {
        $this->assists->removeElement($assists);
    }

    /**
     * Contructor
     */
    public function __construct() {
        $this->segments = new ArrayCollection();
        $this->plans = new ArrayCollection();
        $this->attributes = new ArrayCollection();
        $this->assists = new ArrayCollection();
        $this->groupProductPlan = new ArrayCollection();
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Product
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * Add segment
     *
     * @param \AppBundle\Entity\Segment $segment
     * @return Product
     */
    public function addSegment(Segment $segment) {
        $this->segments->add($segment);
    }

    /**
     * Remove segments
     *
     * @param \AppBundle\Entity\Segment $segments
     */
    public function removeSegment(Segment $segments) {
        $this->segments->removeElement($segments);
    }

    /**
     * Get segments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSegments() {
        return $this->segments;
    }

    /**
     * Set Segment
     *
     * @param type $segments
     * @return \AppBundle\Entity\Product
     */
    public function setSegments($segments) {
        $this->segments = $segments;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getGroupSequence() {
        $groups = ['all'];

        if (!$this->getImage()) {
            $groups = array_merge($groups, ['files']);
        }

        if (!$this->getPdf()) {
            $groups = array_merge($groups, ['files']);
        }

        return $groups;
    }

    /**
     * To string
     *
     * @return string
     */
    public function __toString() {
        return $this->getName();
    }

    /**
     * Add staments
     *
     * @param \AppBundle\Entity\Stament $staments
     * @return Product
     */
    public function addStament(\AppBundle\Entity\Stament $staments) {
        $this->staments[] = $staments;

        return $this;
    }

    /**
     * Remove staments
     *
     * @param \AppBundle\Entity\Stament $staments
     */
    public function removeStament(\AppBundle\Entity\Stament $staments) {
        $this->staments->removeElement($staments);
    }

    /**
     * Get staments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStaments() {
        return $this->staments;
    }

    /**
     * Set formTemplate
     *
     * @param string $formTemplate
     * @return Insurance
     */
    public function setFormTemplate($formTemplate) {
        $this->formTemplate = $formTemplate;

        return $this;
    }

    /**
     * Get formTemplate
     *
     * @return string
     */
    public function getFormTemplate() {
        return $this->formTemplate;
    }

	
	
    /**
     * Set formType
     *
     * @param string $formType
     * @return Insurance
     */
    public function setFormType($formType) {
        $this->formType = $formType;

        return $this;
    }

    /**
     * Get formType
     *
     * @return string
     */
    public function getFormType() {
        return $this->formType;
    }

    /**
     * Add activities
     *
     * @param \AppBundle\Entity\Activity $activities
     * @return Product
     */
    public function addActivity(\AppBundle\Entity\Activity $activities) {
        $this->activities[] = $activities;

        return $this;
    }

    /**
     * Remove activities
     *
     * @param \AppBundle\Entity\Activity $activities
     */
    public function removeActivity(\AppBundle\Entity\Activity $activities) {
        $this->activities->removeElement($activities);
    }

    /**
     * Get activities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActivities() {
        return $this->activities;
    }

    /**
     * Add attributes
     *
     * @param \AppBundle\Entity\Attribute $attributes
     * @return Product
     */
    public function addAttribute(\AppBundle\Entity\Attribute $attributes) {
        $this->attributes[] = $attributes;

        return $this;
    }

    /**
     * Remove attributes
     *
     * @param \AppBundle\Entity\Attribute $attributes
     */
    public function removeAttribute(\AppBundle\Entity\Attribute $attributes) {
        $this->attributes->removeElement($attributes);
    }

    /**
     * Get attributes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAttributes() {
        return $this->attributes;
    }

    /**
     * Add plans
     *
     * @param \AppBundle\Entity\Plan $plans
     * @return Product
     */
    public function addPlan(\AppBundle\Entity\Plan $plans) {
        $this->plans[] = $plans;

        return $this;
    }

    /**
     * Remove plans
     *
     * @param \AppBundle\Entity\Plan $plans
     */
    public function removePlan(\AppBundle\Entity\Plan $plans) {
        $this->plans->removeElement($plans);
    }

    /**
     * Get plans
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlans() {
        return $this->plans;
    }

    /**
     * Get assists
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAssists() {
        return $this->assists;
    }

    /**
     * Buscar la familia segun el segmento.
     *
     * @param \AppBundle\Entity\Segment $segment
     * @return null|Segment
     */
    public function getFamilyBySegment(\AppBundle\Entity\Segment $segment) {
        $data = $this->getFamily()->getSegmentFamilies();

        foreach ($data as $item) {
            if ($item->getSegment()->getId() == $segment->getId()) {
                return $item;
            }
        }

        return null;
    }

    /**
     * Entrega las asistencias que el producto tiene asociado a un segmento.
     *
     * @param \AppBundle\Entity\Segment $segment
     * @return Collection
     */
    public function getAssistsBySegment(\AppBundle\Entity\Segment $segment) {
        $assists = new ArrayCollection();
        $data = $this->getAssists();

        foreach ($data as $item) {
            $segments = $item->getSegmentAssists();
            foreach ($segments as $segmentAssist) {
                if ($segmentAssist->getSegment()->getId() == $segment->getId()) {
                    $assists->add($segmentAssist);
                }
            }
        }

        $sort = Criteria::create();
        $sort->orderBy(array(
            'OrderBy' => Criteria::ASC
        ));

        return $assists->matching($sort);
    }

    /**
     * Indica si el producto estó en un segmento.
     *
     * @param \AppBundle\Entity\Segment $segment
     * @return boolean
     */
    public function inSegment(Segment $segment) {
        $segments = $this->getSegments();

        foreach ($segments as $s) {
            if ($s->getId() == $segment->getId()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Add insurances
     *
     * @param \AppBundle\Entity\Insurance $insurances
     * @return Product
     */
    public function addInsurance(\AppBundle\Entity\Insurance $insurances) {
        $this->insurances[] = $insurances;

        return $this;
    }

    /**
     * Remove insurances
     *
     * @param \AppBundle\Entity\Insurance $insurances
     */
    public function removeInsurance(\AppBundle\Entity\Insurance $insurances) {
        $this->insurances->removeElement($insurances);
    }

    /**
     * Get insurances
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInsurances() {
        return $this->insurances;
    }

    /**
     * Add coverages
     *
     * @param \AppBundle\Entity\Coverage $coverages
     * @return Product
     */
    public function addCoverage(\AppBundle\Entity\Coverage $coverages) {
        $this->coverages[] = $coverages;

        return $this;
    }

    /**
     * Remove coverages
     *
     * @param \AppBundle\Entity\Coverage $coverages
     */
    public function removeCoverage(\AppBundle\Entity\Coverage $coverages) {
        $this->coverages->removeElement($coverages);
    }

    /**
     * Get coverages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCoverages() {
        return $this->coverages;
    }

    /**
     * Indica si el producto tiene o no tiene plan asociados.
     *
     * @return boolean
     */
    public function hasPlan() {
        return ($this->getPlans()->count() > 0);
    }

    /**
     * Indica si el producto tiene o no tiene coberturas asociados.
     *
     * @return boolean
     */
    public function hasCoverages() {
        return ($this->getCoverages()->count() > 0);
    }

    /**
     * Indica si el primer plan del producto tiene coverages
     *
     * @return boolean
     */
    public function hasPlanTargetCoverage() {
        if ($this->hasPlan()) {
            $plan = $this->getPlans()->first();

            if ($plan->hasTargets()) {
                return $plan->getTargets()->first()->hasCoverages();
            }
        }
        return false;
    }

    /**
     * Infica si el producto tiene una descripción para su plan
     *
     * @return boolean
     */
    public function hasGroupProductPlan() {
        return ($this->getGroupProductPlan()->count() > 0);
    }

    /**
     * Indica si el producto puede tener asegurados adicionales
     *
     * @return boolean
     */
    public function includeAditionals() {
        return $this->isTravel();
    }

    /**
     * Add groupProductPlan
     *
     * @param \AppBundle\Entity\GroupProductPlan $groupProductPlan
     * @return Product
     */
    public function addgroupProductPlan(\AppBundle\Entity\GroupProductPlan $groupProductPlan) {
        $groupProductPlan->setProduct($this);
        $this->groupProductPlan[] = $groupProductPlan;

        return $this;
    }

    /**
     * Remove groupProductPlan
     *
     * @param \AppBundle\Entity\GroupProductPlan $groupProductPlan
     */
    public function removeGroupProductPlan(\AppBundle\Entity\GroupProductPlan $groupProductPlan) {
        $this->groupProductPlan->removeElement($groupProductPlan);
    }

    /**
     * Get groupProductPlan
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroupProductPlan() {
        return $this->groupProductPlan;
    }

    /**
     * Set urlcallto 
     *
     * @param string $name
     * @return Product
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set calculoMayor
     *
     * @param boolean $calculoMayor
     * @return Product
     */
    public function setCalculoMayor($calculoMayor) {
        $this->calculoMayor = $calculoMayor;

        return $this;
    }

    /**
     * Get calculoMayor
     *
     * @return boolean 
     */
    public function getCalculoMayor() {
        return $this->calculoMayor;
    }
    
        /**
     * Set parentesco
     *
     * @param \AppBundle\Entity\Parentesco $parentesco
     * @return Parentesco
     */
    public function setParentesco(\AppBundle\Entity\Parentesco $parentesco = null) {
        $this->parentesco = $parentesco;

        return $this;
    }

    /**
     * Get parentesco
     *
     * @return \AppBundle\Entity\Parentesco 
     */
    public function getParentesco() {
        return $this->parentesco;
    }


    /**
     * Set plantilla
     *
     * @param \AppBundle\Entity\Plantilla $plantilla
     * @return Product
     */
    public function setPlantilla(\AppBundle\Entity\Plantilla $plantilla = null)
    {
        $this->plantilla = $plantilla;

        return $this;
    }

    /**
     * Get plantilla
     *
     * @return \AppBundle\Entity\Plantilla 
     */
    public function getPlantilla()
    {
        return $this->plantilla;
    }

    /**
     * Add parentesco
     *
     * @param \AppBundle\Entity\Parentesco $parentesco
     * @return Product
     */
    public function addParentesco(\AppBundle\Entity\Parentesco $parentesco)
    {
        $this->parentesco[] = $parentesco;

        return $this;
    }

    /**
     * Remove parentesco
     *
     * @param \AppBundle\Entity\Parentesco $parentesco
     */
    public function removeParentesco(\AppBundle\Entity\Parentesco $parentesco)
    {
        $this->parentesco->removeElement($parentesco);
    }

    
	/**
     * Set patPass
     *
     * @param integer $patPass
     * @return Product
     */
    public function setPatPass($patPass)
    {
        $this->patPass = $patPass;

        return $this;
    }

    /**
     * Get patPass
     *
     * @return integer 
     */
    public function getPatPass()
    {
        return $this->patPass;
    }
}
