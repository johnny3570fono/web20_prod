<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HomologacionParentesco
 *
 * @ORM\Table(name="homol_parentesco")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\HomologacionParentescoRepository")
 */
class HomologacionParentesco
{
    /**
     * @var string
     *
     * @ORM\Column(name="relationship",  type="string", length=32)
     * @ORM\Id
     */
    private $relationship;

    /**
     * @var integer
     *
     * @ORM\Column(name="codparentesco", type="integer")
     * @ORM\Id
     */
    private $codparentesco;

    /**
     * Set relationship
     *
     * @param string $relationship
     * @return HomologacionParentesco
     */
    public function setRelationship($relationship)
    {
        $this->relationship = $relationship;

        return $this;
    }

    /**
     * Get relationship
     *
     * @return string
     */
    public function getRelationship()
    {
        return $this->relationship;
    }

    /**
     * Set codparentesco
     *
     * @param integer $codparentesco
     * @return HomologacionParentesco
     */
    public function setCodParentesco($codparentesco)
    {
        $this->codparentesco = $codparentesco;

        return $this;
    }

    /**
     * Get codparentesco
     *
     * @return integer
     */
    public function getCodParentesco()
    {
        return $this->codparentesco;
    }
}
