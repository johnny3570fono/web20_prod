<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Plantilla
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Plantilla
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;
	
	/**
     * @ORM\Column(name="name", type="string", length=64)
     */
    private $name;


    /**
     * @ORM\Column(name="template", type="string", length=64)
     */
    
    private $template;

    /**
     * @ORM\Column(name="formtype", type="string", length=64)
     */
    
    private $formtype;
	
	/**
     * @ORM\Column(name="formtypepublic", type="string", length=64)
     */
    
    private $formtypepublic;

   
	/**
     * @ORM\OneToMany(targetEntity="Product", mappedBy="plantilla")
     * @ORM\OrderBy({"orderBy" = "ASC"})
     */
    private $products;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set template
     *
     * @param string $template
     * @return Category
     */
    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Get template
     *
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Set formtype
     *
     * @param string $formtype
     * @return Category
     */
    public function setFormtype($template)
    {
        $this->formtype = $formtype;

        return $this;
    }

    /**
     * Get formtype
     *
     * @return string
     */
    public function getFormtype()
    {
        return $this->formtype;
    }
   
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->products = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add products
     *
     * @param \AppBundle\Entity\Product $products
     * @return Template
     */
    public function addProduct(\AppBundle\Entity\Product $products)
    {
        $this->products[] = $products;

        return $this;
    }

    /**
     * Remove products
     *
     * @param \AppBundle\Entity\Product $products
     */
    public function removeProduct(\AppBundle\Entity\Product $products)
    {
        $this->products->removeElement($products);
    }
	
	/**
     * To String
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Plantilla
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set formtypepublic
     *
     * @param string $formtypepublic
     * @return Plantilla
     */
    public function setFormtypepublic($formtypepublic)
    {
        $this->formtypepublic = $formtypepublic;

        return $this;
    }

    /**
     * Get formtypepublic
     *
     * @return string 
     */
    public function getFormtypepublic()
    {
        return $this->formtypepublic;
    }
}
