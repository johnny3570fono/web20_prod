<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Content
 *
 * @ORM\MappedSuperclass
 */
class Template
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @Assert\NotBlank
     * @ORM\Column(name="template", type="string", length=64)
     */
    
    private $template;

    /**
     * @ORM\Column(name="formtype", type="string", length=64)
     */
    
    private $formtype;

    /**
     * @ORM\Column(name="idfamilia", type="string", length=64)
     */
    
    private $idfamilia;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set template
     *
     * @param string $template
     * @return Category
     */
    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Get template
     *
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Set formtype
     *
     * @param string $formtype
     * @return Category
     */
    public function setFormtype($template)
    {
        $this->formtype = $formtype;

        return $this;
    }

    /**
     * Get formtype
     *
     * @return string
     */
    public function getFormtype()
    {
        return $this->formtype;
    }
    /**
     * Set idfamilia
     *
     * @param string $idfamilia
     * @return Category
     */
    public function setIdfamilia($template)
    {
        $this->idfamilia = $idfamilia;

        return $this;
    }

    /**
     * Get idfamilia
     *
     * @return string
     */
    public function getIdfamilia()
    {
        return $this->idfamilia;
    }
}
