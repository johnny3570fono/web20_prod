<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\MappedSuperclass()
 */
class BaseProduct2 implements \JsonSerializable
{
    const TYPE_LIFE = 'life';
    const TYPE_HEALTH = 'health';
    const TYPE_HOME = 'home';
    const TYPE_TRAVEL = 'travel';
    const TYPE_FRAUD = 'fraud';
    const TYPE_CAR = 'car';

    /**
     * @ORM\Column(name="code", type="string", length=3, unique=true)
     * @Assert\NotBlank(groups={"all"})
     * @Assert\Length(max=3)
     */
     protected $code;

     /**
      * Código de Frecuencia de pago
      * @ORM\Column(name="cod_frec_pago", type="integer")
      * @Assert\NotBlank(groups={"all"})
      */
     protected $codFrecPago;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank(groups={"all"})
     * @Assert\Length(max=255)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="text")
     * @Assert\NotBlank(groups={"all"})
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="image_title", type="text", nullable=true)
     */
    protected $imageTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="image_subtitle", type="text", nullable=true)
     */
    protected $imageSubtitle;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    protected $image;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     * @Assert\NotBlank(groups={"all"})
     */
    protected $description;

    /**
     * @var string
     *
     * @ORM\Column(name="marketing_text", type="text")
     * @Assert\NotBlank(groups={"all"})
     */
    protected $marketingText;

    /**
     * @var string
     *
     * @ORM\Column(name="coverage", type="text")
     * @Assert\NotBlank(groups={"all"})
     */
    protected $coverage;

    /**
     * @var string
     *
     * @ORM\Column(name="coverage_not", type="text")
     * @Assert\NotBlank(groups={"all"})
     */
    protected $coverageNot;

    /**
     * @var string
     *
     * @ORM\Column(name="howto", type="text")
     * @Assert\NotBlank(groups={"all"})
     */
    protected $howto;

    /**
     * @var string
     *
     * @ORM\Column(name="pdf", type="string", length=255, nullable=true)
     */
    protected $pdf;

    /**
     * @var string
     *
     * @ORM\Column(name="text_legal", type="text")
     * @Assert\NotBlank(groups={"all"})
     */
    protected $textLegal;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", precision=8, scale=2)
     * @Assert\NotBlank(groups={"all"})
     * @Assert\Type(type="float", groups={"all"})
     * @Assert\GreaterThan(value=0, groups={"all"})
     */
    protected $price;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=16)
     * @Assert\NotBlank(groups={"all"})
     * @Assert\Choice(groups={"all"}, callback={"AppBundle\Entity\BaseProduct", "getTypes"})
     */
    protected $type = 'none';

    /**
     * @var string
     *
     * @ORM\Column(name="age_min", type="integer", nullable=true, options={"default"=0})
     */
    protected $ageMin = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="age_max", type="integer", nullable=true, options={"default"=0})
     */
    protected $ageMax = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="redirect_url", type="string", nullable=true)
     */
    protected $redirectUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="redirect_url_target", type="string", nullable=true)
     */
    protected $redirectUrlTarget;

    /**
     * @var string
     *
     * @ORM\Column(name="button_help", type="boolean", nullable=true, options={"default": false})
     */
    protected $buttonHelp;

    /**
     * @var string
     *
     * @ORM\Column(name="redirect_url_mobile", type="string", nullable=true)
     */
    protected $redirectUrlMobile;

     /**
     * @var string
     *
     * @ORM\Column(name="suscripcion", type="boolean", nullable=true, options={"default": false})
     */
    protected $suscripcion;
 
     /**
     * Set suscripcion
     *
     * @param string $suscripcion
     * @return Product
     */
    public function setSuscripcion($suscripcion)
    {
        $this->suscripcion = $suscripcion;

        return $this;
    }

    /**
     * Get suscripcion
     *
     * @return string
     */
    public function getSuscripcion()
    {
        return $this->suscripcion;
    }
    
    
       /**
     * Get the value of Redirect Url Mobile
     *
     * @return string
     */
    public function getRedirectUrlMobile()
    {
        return $this->redirectUrlMobile;
    }

    /**
     * Set the value of Redirect Url Mobile
     *
     * @param string RedirectUrlMobile
     *
     * @return self
     */
    public function setRedirectUrlMobile($redirectUrlMobile)
    {
        $this->redirectUrlMobile = $redirectUrlMobile;

        return $this;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set imageTitle
     *
     * @param string $imageTitle
     * @return Product
     */
    public function setImageTitle($imageTitle)
    {
        $this->imageTitle = $imageTitle;

        return $this;
    }

    /**
     * Get imageTitle
     *
     * @return string
     */
    public function getImageTitle()
    {
        return $this->imageTitle;
    }

    /**
     * Set imageSubtitle
     *
     * @param string $imageSubtitle
     * @return Product
     */
    public function setImageSubtitle($imageSubtitle)
    {
        $this->imageSubtitle = $imageSubtitle;

        return $this;
    }

    /**
     * Get imageSubtitle
     *
     * @return string
     */
    public function getImageSubtitle()
    {
        return $this->imageSubtitle;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Product
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Product
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set coverage
     *
     * @param string $coverage
     * @return Product
     */
    public function setCoverage($coverage)
    {
        $this->coverage = $coverage;

        return $this;
    }

    /**
     * Get coverage
     *
     * @return string
     */
    public function getCoverage()
    {
        return $this->coverage;
    }

    /**
     * Set coverageNot
     *
     * @param string $coverageNot
     * @return Product
     */
    public function setCoverageNot($coverageNot)
    {
        $this->coverageNot = $coverageNot;

        return $this;
    }

    /**
     * Get coverageNot
     *
     * @return string
     */
    public function getCoverageNot()
    {
        return $this->coverageNot;
    }

    /**
     * Set howto
     *
     * @param string $howto
     * @return Product
     */
    public function setHowto($howto)
    {
        $this->howto = $howto;

        return $this;
    }

    /**
     * Get howto
     *
     * @return string
     */
    public function getHowto()
    {
        return $this->howto;
    }

    /**
     * Set pdf
     *
     * @param string $pdf
     * @return Product
     */
    public function setPdf($pdf)
    {
        $this->pdf = $pdf;

        return $this;
    }

    /**
     * Get pdf
     *
     * @return string
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * Set textLegal
     *
     * @param string $textLegal
     * @return Product
     */
    public function setTextLegal($textLegal)
    {
        $this->textLegal = $textLegal;

        return $this;
    }

    /**
     * Get textLegal
     *
     * @return string
     */
    public function getTextLegal()
    {
        return $this->textLegal;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Product
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Arreglo con los tipos de productos.
     *
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_LIFE => self::TYPE_LIFE,
            self::TYPE_CAR => self::TYPE_CAR,
            self::TYPE_FRAUD => self::TYPE_FRAUD,
            self::TYPE_HEALTH => self::TYPE_HEALTH,
            self::TYPE_HOME => self::TYPE_HOME,
            self::TYPE_TRAVEL => self::TYPE_TRAVEL,
            'none' => 'none',
        ];
    }

    /**
     * Indica si el seguro es tipo home
     *
     * @return boolean
     */
    public function isHome()
    {
        return ($this->type == self::TYPE_HOME);
    }

    /**
     * Indica si el seguro es tipo salud
     *
     * @return boolean
     */
    public function isHealth()
    {
        return ($this->type == self::TYPE_HEALTH);
    }

    /**
     * Indica si el seguro es tipo viaje
     *
     * @return boolean
     */
    public function isTravel()
    {
        return ($this->type == self::TYPE_TRAVEL);
    }

    /**
     * Indica si el seguro es tipo fraude
     *
     * @return boolean
     */
    public function isFraud()
    {
        return ($this->type == self::TYPE_FRAUD);
    }

    /**
     * Indica si el seguro es tipo auto
     *
     * @return boolean
     */
    public function isCar()
    {
        return ($this->type == self::TYPE_CAR);
    }

    /**
     * Indica si el seguro es tipo vida
     *
     * @return boolean
     */
    public function isLife()
    {
        return ($this->type == self::TYPE_LIFE);
    }

    /**
     * Get the value of Age Min
     *
     * @return string
     */
    public function getAgeMin()
    {
        return $this->ageMin;
    }

    /**
     * Get the value of Age Max
     *
     * @return string
     */
    public function getAgeMax()
    {
        return $this->ageMax;
    }

    /**
     * Set the value of Age Min
     *
     * @param string ageMin
     *
     * @return self
     */
    public function setAgeMin($ageMin)
    {
        $this->ageMin = (int)$ageMin;

        return $this;
    }

    /**
     * Set the value of Age Max
     *
     * @param string ageMax
     *
     * @return self
     */
    public function setAgeMax($ageMax)
    {
        $this->ageMax = (int)$ageMax;

        return $this;
    }

    public function toArray()
    {
        return [
                'id' => $this->getId(),
                'ageMin' => $this->getAgeMin(),
                'ageMax' => $this->getAgeMax(),
                'price' => $this->getPrice(),
        ];
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }

    /**
     * Get the value of Redirect Url
     *
     * @return string
     */
    public function getRedirectUrl()
    {
        return $this->redirectUrl;
    }

    /**
     * Set the value of Redirect Url
     *
     * @param string redirectUrl
     *
     * @return self
     */
    public function setRedirectUrl($redirectUrl)
    {
        $this->redirectUrl = $redirectUrl;

        return $this;
    }

    /**
     * Get the value of Marketing Text
     *
     * @return string
     */
    public function getMarketingText()
    {
        return $this->marketingText;
    }

    /**
     * Set the value of Marketing Text
     *
     * @param string marketingText
     *
     * @return self
     */
    public function setMarketingText($marketingText)
    {
        $this->marketingText = $marketingText;

        return $this;
    }

    /**
     * Get the value of Code
     *
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set the value of Code
     *
     * @param mixed code
     *
     * @return self
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get the value of Código de Frecuencia de pago
     *
     * @return mixed
     */
    public function getCodFrecPago()
    {
        return $this->codFrecPago;
    }

    /**
     * Set the value of Código de Frecuencia de pago
     *
     * @param mixed codFrecPago
     *
     * @return self
     */
    public function setCodFrecPago($codFrecPago)
    {
        $this->codFrecPago = $codFrecPago;

        return $this;
    }

    /**
     * Get the value of Redirect Url Target
     *
     * @return string
     */
    public function getRedirectUrlTarget()
    {
        return $this->redirectUrlTarget;
    }

    /**
     * Set the value of Redirect Url Target
     *
     * @param string redirectUrlTarget
     *
     * @return self
     */
    public function setRedirectUrlTarget($redirectUrlTarget)
    {
        $this->redirectUrlTarget = $redirectUrlTarget;

        return $this;
    }

    /**
     * Get the value of ClicktoCall
     *
     * @return string
     */
    public function getButtonHelp()
    {
        return $this->buttonHelp;
    }

    /**
     * Set the value of Botón ClicktoCall
     *
     * @param string buttonHelp
     *
     * @return self
     */
    public function setButtonHelp($buttonHelp)
    {
        $this->buttonHelp = $buttonHelp;

        return $this;
    }

    public function hasButtonHelp()
    {
        return ((boolean)$this->getButtonHelp() === true);
    }
}
