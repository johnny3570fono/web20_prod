<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Plan
 *
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(columns={"name", "product_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\PlanRepository")
 */
class Plan implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=3)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=120)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="decimal", precision=8, scale=2, nullable=true, options={"default" = 0})
     */
    private $price;

    /**
     * @var cargasMax
     *
     * @ORM\Column(name="cargas_max", type="integer", options={"default" = 0})
     */
    private $cargasMax;

    /**
     * @var cargasMin
     *
     * @ORM\Column(name="cargas_min", type="integer", options={"default" = 0})
     */
    private $cargasMin;

    /**
     * @var cargasPrice
     *
     * @ORM\Column(name="cargas_price", type="json_array", nullable=true)
     */
    private $cargasPrice;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_by", type="integer", options={"default" = 1})
     */
    private $orderBy;

    /**
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="plans")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=false)
     */
    private $product;

    /**
     * @ORM\ManyToMany(targetEntity="Attribute", cascade={"remove"})
     * @ORM\JoinTable(name="plan_attribute")
     **/
    private $attributes;

    /**
     * @ORM\OneToMany(targetEntity="TargetAudienceCoverage", mappedBy="plan" )
     * @ORM\OrderBy({"orderBy" = "ASC"})
     **/
    private $coverages;

    /**
     * @ORM\OneToMany(targetEntity="TargetAudience", cascade={"remove"}, mappedBy="plan")
     * @ORM\JoinTable(name="target_audience")
     * @ORM\OrderBy({"finish" = "ASC"})
     **/
    private $targets;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", nullable=true)
     */
    private $description;
    
     /**
     * @ORM\OneToMany(targetEntity="Renta", mappedBy="Plan" )
     */
    protected $rentas;

    /**
     * @var string
     *
     * @ORM\Column(name="codeCorredora", type="string", length=3,nullable=true)
     */
    private $codeCorredora;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->attributes = new ArrayCollection();
        $this->coverages = new ArrayCollection();
        $this->rentas = new ArrayCollection();
    }
    /**
     * Add rentas
     *
     * @param  $rentas
     */
    public function addRentas(\AppBundle\Entity\Renta $rentas)
    {
        $this->rentas[] = $rentas;
    }

    /**
     * Get rentas
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getRentas()
    {
        return $this->rentas;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Plan
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * To String
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    

    /**
     * Add attributes
     *
     * @param \AppBundle\Entity\Attribute $attributes
     * @return Plan
     */
    public function addAttribute(\AppBundle\Entity\Attribute $attributes)
    {
        $this->attributes[] = $attributes;

        return $this;
    }

    /**
     * Remove attributes
     *
     * @param \AppBundle\Entity\Attribute $attributes
     */
    public function removeAttribute(\AppBundle\Entity\Attribute $attributes)
    {
        $this->attributes->removeElement($attributes);
    }

    /**
     * Get attributes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * Set product
     *
     * @param \AppBundle\Entity\Product $product
     * @return Plan
     */
    public function setProduct(\AppBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \AppBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Getter $price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Setter $price
     *
     * @param type $price
     * @return \AppBundle\Entity\Plan
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Getter $orderBy
     *
     * @return integer
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * Setter $orderBy
     *
     * @param integer $orderBy
     * @return \AppBundle\Entity\Plan
     */
    public function setOrderBy($orderBy)
    {
        $this->orderBy = $orderBy;

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return $this->toArray();
    }

    /**
     * To array
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'code' => $this->getCode(),
            'name' => $this->getName(),
            'price' => $this->getPrice(),
            'cargasMin' => $this->getCargasMin(),
            'cargasMax' => $this->getCargasMax(),
            'cargasPrice' => $this->getCargasPrice(),
            'codeCorredora'=>$this->getCodeCorredora(),
			'order' => $this->getOrderBy()
        ];
    }

    /**
     * Add coverages
     *
     * @param \AppBundle\Entity\TargetAudienceCoverage $coverages
     * @return Plan
     */
    public function addCoverage(\AppBundle\Entity\TargetAudienceCoverage $coverages)
    {
        $this->coverages[] = $coverages;

        return $this;
    }

    /**
     * Remove coverages
     *
     * @param \AppBundle\Entity\TargetAudienceCoverage $coverages
     */
    public function removeCoverage(\AppBundle\Entity\TargetAudienceCoverage $coverages)
    {
        $this->coverages->removeElement($coverages);
    }

    /**
     * Get coverages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCoverages()
    {
        return $this->coverages;
    }

    /**
     * Tiene coberturas
     *
     * @return boolean
     */
    public function hasCoverages()
    {
        return ($this->getCoverages()->count() > 0);
    }

    /**
     * Tiene Target
     *
     * @return boolean
     */
    public function hasTargets()
    {
        return ($this->getTargets()->count() > 0);
    }

    /**
     * Add targets
     *
     * @param \AppBundle\Entity\TargetAudience $targets
     * @return Plan
     */
    public function addTarget(\AppBundle\Entity\TargetAudience $targets)
    {
        $this->targets[] = $targets;

        return $this;
    }

    /**
     * Remove targets
     *
     * @param \AppBundle\Entity\TargetAudience $targets
     */
    public function removeTarget(\AppBundle\Entity\TargetAudience $targets)
    {
        $this->targets->removeElement($targets);
    }

    /**
     * Get targets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTargets()
    {
        return $this->targets;
    }

    /**
     * Traer el tramo de edad correspondiente al valor entregado.
     *
     * @param integer $value
     * @return ArrayCollection
     */
    public function getTargetsByValue($value)
    {
        $targets = new ArrayCollection();

        if ($this->getTargets()->count() < 1) {
            return $targets;
        }

        $iteratorTargets = $this->targets->getIterator();

        $iteratorTargets->uasort(function ($a, $b) {
            return ($a->getStart() < $b->getStart()) ? -1 : 1;
        });

        $sortedTargets = new ArrayCollection(iterator_to_array($iteratorTargets));

        if ($value < $sortedTargets->first()->getStart()) {
            $targets->add($sortedTargets->first());

            return $targets;
        }

        if ($value > $sortedTargets->last()->getFinish()) {
            $targets->add($sortedTargets->last());

            return $targets;
        }

        foreach ($this->targets as $t) {
            if ($value >= $t->getStart() && $value <= $t->getFinish()) {
                $targets->add($t);
            }
        }

        return $targets;
    }

    /**
     * Get the value of cargasMax
     *
     * @return cargasMax
     */
    public function getCargasMax()
    {
        return $this->cargasMax;
        //jsonSerialize()
    }

    /**
     * Set the value of cargasMax
     *
     * @param cargasMax cargasMax
     *
     * @return self
     */
    public function setCargasMax($cargasMax)
    {
        $this->cargasMax = $cargasMax;

        return $this;
    }

    /**
     * Set the value of Attributes
     *
     * @param mixed attributes
     *
     * @return self
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;

        return $this;
    }

    /**
     * Set the value of Coverages
     *
     * @param mixed coverages
     *
     * @return self
     */
    public function setCoverages($coverages)
    {
        $this->coverages = $coverages;

        return $this;
    }

    /**
     * Set the value of Targets
     *
     * @param mixed targets
     *
     * @return self
     */
    public function setTargets($targets)
    {
        $this->targets = $targets;

        return $this;
    }

    /**
     * Set cargasMin
     *
     * @param integer $cargasMin
     * @return Plan
     */
    public function setCargasMin($cargasMin)
    {
        $this->cargasMin = $cargasMin;

        return $this;
    }

    /**
     * Get cargasMin
     *
     * @return integer
     */
    public function getCargasMin()
    {
        return $this->cargasMin;
    }

    /**
     * Set cargasPrice
     *
     * @param array $cargasPrice
     * @return Plan
     */
    public function setCargasPrice($cargasPrice)
    {
        
        $this->cargasPrice = $cargasPrice;

        return $this;
    }

    /**
     * Get cargasPrice
     *
     * @return array
     */
    public function getCargasPrice()
    {
        return $this->cargasPrice;
        
    }

    /**
     * Get the value of Code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set the value of Code
     *
     * @param string code
     *
     * @return self
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get the value of Description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of Description
     *
     * @param string description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }
 /**
     * Get the value of Codecorredora
     *
     * @return string
     */
    public function getCodeCorredora()
    {
        return $this->codeCorredora;
    }

    /**
     * Set the value of codeCorredora
     *
     * @param string codeCorredora
     *
     * @return self
     */
    public function setCodeCorredora($codeCorredora)
    {
        $this->codeCorredora = $codeCorredora;

        return $this;
    }

    /**
     * Get the value of CodeProducto
     *
     * @return string
     */
    public function getCodeProducto()
    {
        return $this->getProduct()->getId();
    }

    /**
     * Get the value of SegmentoProducto
     *
     * @return string
     */
    public function getSegmentoProducto()
    {
        return $this->getProduct()->getSegments();
    }
}
