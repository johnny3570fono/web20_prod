<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * AuthorizationHealth
 *
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(columns={"person_id", "insurance_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\AuthorizationHealthRepository")
 * @UniqueEntity(fields={"person", "insurance"})
 */
class AuthorizationHealth
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Insurance
     *
     * @ORM\ManyToOne(targetEntity="Beneficiary")
     * @ORM\JoinColumn(nullable=false)
     */
    private $person;

    /**
     * @var Insurance
     *
     * @ORM\ManyToOne(targetEntity="Insurance")
     * @ORM\JoinColumn(nullable=false)
     */
    private $insurance;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set person
     *
     * @param \AppBundle\Entity\Payer $person
     * @return AuthorizationHealth
     */
    public function setPayer(\AppBundle\Entity\Payer $person)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \AppBundle\Entity\Payer
     */
    public function getPayer()
    {
        return $this->person;
    }

    /**
     * Set insurance
     *
     * @param \AppBundle\Entity\Insurance $insurance
     * @return AuthorizationHealth
     */
    public function setInsurance(\AppBundle\Entity\Insurance $insurance)
    {
        $this->insurance = $insurance;

        return $this;
    }

    /**
     * Get insurance
     *
     * @return \AppBundle\Entity\Insurance
     */
    public function getInsurance()
    {
        return $this->insurance;
    }
}
