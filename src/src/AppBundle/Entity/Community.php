<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Community
 *
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="com_ciu", columns={"codigo_comuna_homologo", "codigo_cuidad_homologo"})})
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\CommunityRepository")
 */
class Community
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="codigo_comuna_homologo" , type="integer", length=4)
     * @Assert\NotBlank()
     */
    private $codigo_comuna_homologo;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion_comuna_homologo" , type="string", length=40)
     * @Assert\NotBlank()
     */
    private $descripcion_comuna_homologo;

    /**
     * @var integer
     *
     * @ORM\Column(name="codigo_cuidad_homologo" , type="integer", length=4)
     * @Assert\NotBlank()
     */
    private $codigo_cuidad_homologo;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion_cuidad_homologo" , type="string", length=40)
     * @Assert\NotBlank()
     */
    private $descripcion_cuidad_homologo;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo_comuna_homologo|
     *
     * @param integer $codigo_comuna_homologo
     * @return Community
     */
    public function setCodigoComunaHomologo($codigo_comuna_homologo)
    {
        $this->codigo_comuna_homologo = $codigo_comuna_homologo;

        return $this;
    }
    /**
     * Get codigo_comuna_homologo
     *
     * @return integer
     */
    public function getCodigoComunaHomologo()
    {
        return $this->codigo_comuna_homologo;
    }

    /**
     * Set descripcion_comuna_homologo
     *
     * @param string $descripcion_comuna_homologo
     * @return Community
     */
    public function setDescripcionComunaHomologo($descripcion_comuna_homologo)
    {
        $this->descripcion_comuna_homologo = $descripcion_comuna_homologo;

        return $this;
    }
    /**
     * Get descripcion_comuna_homologo
     *
     * @return string
     */
    public function getDescripcionComunaHomologo()
    {
        return $this->descripcion_comuna_homologo;
    }

    /**
     * Set codigo_cuidad_homologo
     *
     * @param integer $codigo_cuidad_homologo
     * @return Community
     */
    public function setCodigoCuidadHomologo($codigo_cuidad_homologo)
    {
        $this->codigo_cuidad_homologo = $codigo_cuidad_homologo;

        return $this;
    }
    /**
     * Get codigo_cuidad_homologo
     *
     * @return string
     */
    public function getCodigoCuidadHomologo()
    {
        return $this->codigo_cuidad_homologo;
    }
    /**
     * Set descripcion_cuidad_homologo
     *
     * @param string $descripcion_cuidad_homologo
     * @return Community
     */
    public function setDescripcionCuidadHomologo($descripcion_cuidad_homologo)
    {
        $this->descripcion_cuidad_homologo = $descripcion_cuidad_homologo;

        return $this;
    }

    /**
     * Get descripcion_cuidad_homologo
     *
     * @return string
     */
    public function getDescripcionCuidadHomologo()
    {
        return $this->descripcion_cuidad_homologo;
    }
    /**
     * Name
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }
}
