<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;


/**
 * Renta
 * @ORM\Entity
 *
 * @ORM\Table()
 */
class Renta
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

     /**
     * @var code
     *
     * @ORM\Column(name="codsp", type="integer", nullable=true)
     */
    private $codsp;

     /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", nullable=true)
     */
    private $description;
 
    /**
     * @ORM\ManyToOne(targetEntity="Plan", inversedBy="Renta")
     * @ORM\JoinColumn(name="Plan", referencedColumnName="id")
     * @return integer
     */
    protected $plan;

    /**
     * @ORM\OneToMany(targetEntity="Educational", mappedBy="Renta" )
     */
    protected $educational;

     /**
     * @var integer
     *
     * @ORM\Column(name="orderby", type="integer" , nullable=true)
     */
    private $orderby;

    /**
     *
     * @ORM\Column(name="codplanco", type="string",  nullable=true, length=10)
     */
    private $codplanco;


     public function setCodplanco($codplanco)
    {
        $this->codplanco = $codplanco;

        return $this;
    }

    /**
     * Get codPlanCo
     *
     * @return string
     */
    public function getCodplanco()
    {
        return $this->codplanco;
    }


   /**
     * Set orderby
     *
     * @param integer $orderby
     */
    public function setOrderby($orderby)
    {
        $this->orderby = $orderby;

        return $this;
    }

    /**
     * Get orderby
     *
     * @return integer
     */
    public function getOrderby()
    {
        return $this->orderby;
    }

    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->educational = new ArrayCollection();
    }
    /**
     * Add educational
     *
     * @param  $educational
     */
    public function addEducational(\AppBundle\Entity\Educational $educational)
    {
        $this->educational[] = $educational;
    }

    /**
     * Get educational
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getEducational()
    {
        return $this->educational;
    }

        
    /**
     * Set plan
     *
     * @param \AppBundle\Entity\Plan $plan
     */
    public function setPlan(\AppBundle\Entity\Plan $plan)
    {
        $this->plan = $plan;
    }

    /**
     * Get plan
     *
     * @return \AppBundle\Entity\Plan
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of Description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of Description
     *
     * @param string description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of Codsp
     *
     * @return integer
     */
    public function getCodsp()
    {
        return $this->codsp;
    }

    /**
     * Set the value of Codsp
     *
     * @param integer codsp
     *
     * @return self
     */
    public function setCodsp($codsp)
    {
        $this->codsp = $codsp;

        return $this;
    }

}
