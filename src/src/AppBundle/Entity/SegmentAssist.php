<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\GroupSequenceProviderInterface;

/**
 * SegmentAssist
 *
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(columns={"segment_id", "assist_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\SegmentFamilyRepository")
 * @UniqueEntity(
 *      fields={"segment", "assist"},
 *      message="El segmento ya tiene asociada la asistencia",
 *      groups={"all"}
 * )
 * @Vich\Uploadable
 * @Assert\GroupSequenceProvider
 */
class SegmentAssist implements GroupSequenceProviderInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \stdClass
     *
     * @ORM\ManyToOne(targetEntity="Segment", inversedBy="segmentAssists", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(groups={"all"})
     */
    private $segment;

    /**
     * @var \stdClass
     *
     * @ORM\ManyToOne(targetEntity="Assist", inversedBy="segmentAssists", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(groups={"all"})
     */
    private $assist;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=36)
     * @Assert\NotBlank(groups={"all"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=3000)
     * @Assert\NotBlank(groups={"all"})
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="order_by", type="integer", nullable=true, options={"default" = 0})
     */
    private $orderBy;

    /**
     * @var string
     *
     * @ORM\Column(name="icon", type="string", length=255, nullable=true)
     */
    private $icon;

    /**
     * @var File
     *
     * @Vich\UploadableField(mapping="segment_assist_image", fileNameProperty="icon")
     *
     * @Assert\File(
     *     groups={"all"},
     *     maxSize = "50k",
     *     mimeTypes = {"image/png", "image/jpeg", "image/jpg"}
     * )
     */
    private $iconFile;

    /**
     * @ORM\Column(type="string", nullable=true, length=36)
     * @var string
     */
    private $updateAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return SegmentAssist
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return SegmentAssist
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get orderBy
     *
     * @return integer
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * Set orderBy
     *
     * @param integer $orderBy
     * @return integer
     */
    public function setOrderBy($orderBy)
    {
        $this->orderBy = $orderBy;

        return $this;
    }

    /**
     * Set icon
     *
     * @param string $icon
     * @return SegmentAssist
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Sets file.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $iconFile
     */
    public function setIconFile(File $iconFile = null)
    {
        $this->iconFile = $iconFile;
        $this->updatedAt = date('Y-m-d H:i:s');
    }

    /**
     * Get iconFile
     *
     * @return File|\Symfony\Component\HttpFoundation\File\UploadedFile
     */
    public function getIconFile()
    {
        return $this->iconFile;
    }

    /**
     * Set updateAt
     *
     * @param \DateTime $updateAt
     * @return SegmentAssist
     */
    public function setUpdateAt($updateAt)
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
     * Get updateAt
     *
     * @return \DateTime
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }

    /**
     * Set segment
     *
     * @param \AppBundle\Entity\Segment $segment
     * @return SegmentAssist
     */
    public function setSegment(\AppBundle\Entity\Segment $segment)
    {
        $this->segment = $segment;

        return $this;
    }

    /**
     * Get segment
     *
     * @return \AppBundle\Entity\Segment
     */
    public function getSegment()
    {
        return $this->segment;
    }

    /**
     * Set assist
     *
     * @param \AppBundle\Entity\Assist $assist
     * @return SegmentAssist
     */
    public function setAssist(\AppBundle\Entity\Assist $assist)
    {
        $this->assist = $assist;

        return $this;
    }

    /**
     * Get assist
     *
     * @return \AppBundle\Entity\Assist
     */
    public function getAssist()
    {
        return $this->assist;
    }

    /**
     * To String
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * {@inheritdoc}
     */
    public function getGroupSequence()
    {
        $groups = ['all'];

        if (!$this->getIcon()) {
            $groups[] = 'files';
        }

        return $groups;
    }

    public function __construct()
    {
        $this->updatedAt = date('Y-m-d H:i:s');
    }
}
