<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints as AppAssert;

/**
 * @ORM\MappedSuperclass()
 */
class BasePerson implements \JsonSerializable
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotNull()
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname1", type="string", length=255)
     * @Assert\NotNull()
     */
    protected $lastname1;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname2", type="string", length=255)
     * @Assert\NotNull()
     */
    protected $lastname2;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=36)
     * @AppAssert\IsRun()
     * @Assert\NotNull()
     */
    protected $code;

    /**
     * @var \Date
     *
     * @ORM\Column(name="birthday", type="date", nullable=true)
     * @Assert\NotNull()
     */
    protected $birthday;

     

    /**
     * @var string
     */
    protected $token;

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return self
     */
    public function setCode($code)
    {
        if (strlen($code) > 1) {
            $code = preg_replace('/[^0-9kK]/', '', $code);

            $this->code = substr($code, 0, -1) . '-' . substr($code, -1);
        } else {
            $this->code = $code;
        }

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCodeRaw()
    {
        return preg_replace('/[^0-9kK]/', '', $this->getCode());
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCodeOnlyNumber()
    {
        return substr($this->getCode(), 0, -2);
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCodeOnlyDv()
    {
        return substr($this->getCode(), -1);
    }

    /**
     * Set birthday
     *
     * @param \Date $birthday
     * @return self
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \Date
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Get lastname1
     *
     * @return string
     */
    public function getLastname1()
    {
        return $this->lastname1;
    }

    /**
     * Get lastname2
     *
     * @return string
     */
    public function getLastname2()
    {
        return $this->lastname2;
    }

    /**
     * Set lastname1
     *
     * @param string $lastname1
     * @return self
     */
    public function setLastname1($lastname1)
    {
        $this->lastname1 = $lastname1;

        return $this;
    }

    /**
     * Set lastname2
     *
     * @param string $lastname2
     * @return self
     */
    public function setLastname2($lastname2)
    {
        $this->lastname2 = $lastname2;

        return $this;
    }

    /**
     * To String
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getFullname() . ' (' . $this->getCode() . ')';
    }

    /**
     * Entrega la concatenación del nombre y los dos apellidos.
     *
     * @return String
     */
    public function getFullname()
    {
        return sprintf('%s %s %s', $this->getName(), $this->getLastname1(), $this->getLastname2());
    }

    /**
     * Años desde la fecha de nacimiento al día de hoy.
     *
     * @return int
     */
    public function getAge()
    {
        if (!($this->getBirthday() instanceof \DateTime)) {
            return 0;
        } else {
            $today = new \DateTime();

            return $today->diff($this->getBirthday())->y;
        }
    }

    /**
     * Get the value of Token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set the value of Token
     *
     * @param string token
     *
     * @return self
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Entrega un array asositivo con Column -> Value
     *
     * @return array
     */
    public function toArray()
    {
        return [
                'id' => $this->getId(),
                'name' => $this->getName(),
                'lastname1' => $this->getLastname1(),
                'lastname2' => $this->getLastname2(),
                'code' => $this->getCode(),
                'day' => $this->getBirthday() ? $this->getBirthday()->format('j') : null,
                'month' => $this->getBirthday() ? $this->getBirthday()->format('n') : null,
                'year' => $this->getBirthday() ? $this->getBirthday()->format('Y') : null,
        ];
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }
}
