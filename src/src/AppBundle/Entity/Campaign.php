<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Campaign
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\CampaignRepository")
 */
class Campaign
{


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", type="text", nullable=true)
     */
    private $description;


     /**
     * @ORM\ManyToMany(targetEntity="Plan", inversedBy="campaign")
     * @ORM\JoinTable(name="campaign_plan")
     * @Assert\Count(min="1", groups={"all"})
     * */
    private $plans;


    /**
     * @ORM\OneToMany(targetEntity="CampaignRegister",  cascade={"remove"}, mappedBy="campaign" )
     */
    protected $registers;


    /** @ORM\Column(type="datetime", name="start_at") */
    private $startAt;


    /** @ORM\Column(type="datetime", name="end_at") */
    private $endAt;

    public function __construct() {
        $this->plans = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Banner
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Banner
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }



    /**
     * Add plan
     *
     * @param \AppBundle\Entity\Plan $plan
     * @return Product
     */
    public function addPlan(Plan $plan) {
        $this->plans->add($plan);
    }

    /**
     * Remove plans
     *
     * @param \AppBundle\Entity\Plan $plan
     */
    public function removePlan(Plan $plan) {
        $this->plans->removeElement($plan);
    }

    /**
     * Get plans
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlans() {
        
        return $this->plans;
    }

    /**
     * Set Plan
     *
     * @param type $plans
     * @return \AppBundle\Entity\Campaign
     */
    public function setPlans($plans) {
        $this->plans = $plans;

        return $this;
    }

    

    /**
     * Get registers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRegisters()
    {
        return $this->registers;
    }

    /**
     * Get startAt
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStartAt() {
        return $this->startAt;
    }

    /**
     * Set startAt
     *
     * @param type $startAt
     * @return this
     */
    public function setStartAt($startAt) {
        $this->startAt = $startAt;

        return $this;
    }

    /**
     * Get startAt
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEndAt() {
        return $this->endAt;
    }

    /**
     * Set endAt
     *
     * @param type $endAt
     * @return this
     */
    public function setEndAt($endAt) {
        $this->endAt = $endAt;

        return $this;
    }

    public function toArray() {
        return [
                'id' => $this->getId(),
                'plans' => $this->getPlans()->toArray(),
                'startAt' => $this->getStartAt(),
                'endAt' => $this->getEndAt(),
        ];
    }
    
}
