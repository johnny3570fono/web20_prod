<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Payer
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\PayerRepository")
 */
class Payer extends PersonAddress
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \stdClass
     *
     * @ORM\OneToOne(targetEntity="Insurance", inversedBy="payer", cascade={"persist", "remove"})
     */
    private $insurance;
     /**
     * @var string
     *
     * @ORM\Column(name="codsexo", type="string", length=1, nullable=true)
     */
    protected $codSexo;


    /**
     * Set codsexo
     *
     * @param string $codsexo
     * @return Payer
     */
    public function setCodSexo($codSexo)
    {
        $this->codSexo = $codSexo;

        return $this;
    }

    /**
     * Get codsexo
     *
     * @return codsexo
     */
    public function getCodSexo()
    {
        return $this->codSexo;
    }



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Getter $insurance
     *
     * @return \AppBundle\Entity\Insurance
     */
    public function getInsurance()
    {
        return $this->insurance;
    }

    /**
     * Setter $insurance
     *
     * @param \AppBundle\Entity\Insurance $insurance
     * @return Payer
     */
    public function setInsurance(\AppBundle\Entity\Insurance $insurance = null)
    {
        $this->insurance = $insurance;

        return $this;
    }
}
