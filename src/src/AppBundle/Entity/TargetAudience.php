<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;


use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * TargetAudience
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\TargetAudienceRepository")
 */
class TargetAudience implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="cstart", type="integer")
     */
    private $start;

    /**
     * @var integer
     *
     * @ORM\Column(name="cfinish", type="integer")
     */
    private $finish;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="decimal", precision=8, scale=2, nullable=true, options={"default" = 0})
     */
    private $price;

    /**
     * @var Plan
     *
     * @ORM\ManyToOne(targetEntity="Plan")
     */
    private $plan;

    /**
     * @var coverages
     *
     * @ORM\OneToMany(targetEntity="TargetAudienceCoverage", mappedBy="targetAudience")
     */
    private $coverages;
     /*
      * @Assert\Choice(groups={"all"}, callback={"\AppBundle\Entity\Product", "getProductos"})
      */
     protected $product;
  

    /**
     * Set product
     *
     * @param string $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }


    
    /**
     * Get product
     *
     * @return \AppBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }


    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return $this->toArray();
    }

    /**
     * To array
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'plan' => $this->getPlan()->toArray(),
            'start' => $this->getStart(),
            'finish' => $this->getFinish(),
            'price' => $this->getPrice(),
        ];
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set start
     *
     * @param integer $start
     * @return TargetAudience
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return integer
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set finish
     *
     * @param integer $finish
     * @return TargetAudience
     */
    public function setFinish($finish)
    {
        $this->finish = $finish;

        return $this;
    }

    /**
     * Get finish
     *
     * @return integer
     */
    public function getFinish()
    {
        return $this->finish;
    }

    /**
     * Set plan
     *
     * @param \AppBundle\Entity\Plan $plan
     * @return TargetAudience
     */
    public function setPlan(\AppBundle\Entity\Plan $plan = null)
    {
        $this->plan = $plan;

        return $this;
    }

    /**
     * Get plan
     *
     * @return \AppBundle\Entity\Plan
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return TargetAudience
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->coverages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add coverages
     *
     * @param \AppBundle\Entity\TargetAudienceCoverage $coverages
     * @return TargetAudience
     */
    public function addCoverage(\AppBundle\Entity\TargetAudienceCoverage $coverages)
    {
        $this->coverages[] = $coverages;

        return $this;
    }

    /**
     * Remove coverages
     *
     * @param \AppBundle\Entity\TargetAudienceCoverage $coverages
     */
    public function removeCoverage(\AppBundle\Entity\TargetAudienceCoverage $coverages)
    {
        $this->coverages->removeElement($coverages);
    }

    /**
     * Get coverages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCoverages()
    {
        return $this->coverages;
    }
    
    /**
     * Indica si el producto tiene o no tiene coberturas asociados.
     *
     * @return boolean
     */
    public function hasCoverages()
    {
        return ($this->getCoverages()->count() > 0);
    }

    /**
     * Get the value of ID de Plan
     *
     * @return string
     */
    public function getPlanId()
    {
        return $this->getPlan()->getId();
    }

    /**
     * Get the value of CodeProducto
     *
     * @return string
     */
    public function getCodeProducto()
    {
        return $this->getPlan()->getProduct()->getId();
    }

    /**
     * Get the value of SegmentoProducto
     *
     * @return string
     */
    public function getSegmentoProducto()
    {
        return $this->getPlan()->getProduct()->getSegments();
    }

    /**
     * Get the value of SegmentoProducto
     *
     * @return string
     */
    public function getElProducto()
    {
        return $this->getPlan()->getProduct();
    }

}
