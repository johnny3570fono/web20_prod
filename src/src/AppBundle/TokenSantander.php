<?php

namespace AppBundle;

use AppBundle\AppSoapClientLGU;
use Symfony\Component\Security\Core\SecurityContext;



/**
 * Webservice SOAP
 */
class TokenSantander extends AppSoapClientLGU
{

    /**
     * Entrega un nombre de usuario
     *
     * @param string $key
     * @return null|string
     */
    public function isValido($key)
    {
      
        $respuestas = $this->getValidarToken(['cToken' => $key]);
       
        $validtoken=false;
      foreach ($respuestas as $respuesta) {
        $codRespuesta = $respuesta->codRespuesta;
        $codRespuestaSpecified = $respuesta->codRespuestaSpecified;
        $descRespuesta = $respuesta->descRespuesta;
        $descRespuestaSpecified = $respuesta->descRespuestaSpecified;
        $key = $respuesta->token;
       
         }
       
        if($respuestas){
 return [
            'codRespuesta' => $codRespuesta,
            'codRespuestaSpecified' => $codRespuestaSpecified,
            'descRespuesta' => $descRespuesta,
            'descRespuestaSpecified' => $descRespuestaSpecified,
            'key' => $key,
            'validtoken'=>$validtoken
        ];
    }else{
      return false;
  }
    }

	public function getGeneraToken()
    {
        $req = ['cData' => '<tokenDefinition>'];

//        $req['cData'] = $req['cData'].'<userID>8521864-6</userID>';
        $req['cData'] = $req['cData'].'<userID>14717680-5</userID>';
//        $req['cData'] = $req['cData'].'<userID>14096803-k</userID>';
//        $req['cData'] = $req['cData'].'<userID>12000568-5</userID>';

        $req['cData'] = $req['cData'].'
        <RutEmpresa>21521521-0</RutEmpresa>
        <NomEmpresa>PATROLL INTERNATIONAL S.A.</NomEmpresa>
        <Contrato>000000000000016</Contrato>
        <RutUsuario>21521521-0</RutUsuario>
        <NomUsuario>José Antonio Claros Dulón</NomUsuario>
        <Rol>LALA</Rol>
        <Segmento>N</Segmento>
        <Servicio>Pago Proveedores</Servicio>
        <subCanal>P</subCanal>
        <codProd></codProd>
        <codllamado>P</codllamado>
        </tokenDefinition>';
/*
        $req = ['cData' => '<tokenDefinition>
        <userID>14096803-k</userID>
        <RutEmpresa>83314008-1</RutEmpresa>
        <NomEmpresa>PATROLL INTERNATIONAL S.A.</NomEmpresa>
        <Contrato>000000000000016</Contrato>
        <RutUsuario>14096803-k</RutUsuario>
        <NomUsuario>José Antonio Claros Dulón</NomUsuario>
        <Rol>LALA</Rol>
        <Segmento>N</Segmento>
        <Servicio>Pago Proveedores</Servicio>
        <subCanal>P</subCanal>
        <codProd></codProd>
        <codllamado>P</codllamado>
        </tokenDefinition>'];

        $req = ['cData' => '<tokenDefinition>
        <userID>12000568-5</userID>
        <RutEmpresa>83314008-1</RutEmpresa>
        <NomEmpresa>PATROLL INTERNATIONAL S.A.</NomEmpresa>
        <Contrato>000000000000016</Contrato>
        <RutUsuario>12000568-5</RutUsuario>
        <NomUsuario>José Antonio Claros Dulón</NomUsuario>
        <Rol>LALA</Rol>
        <Segmento>select</Segmento>
        <Servicio>Pago Proveedores</Servicio>
        <subCanal>P</subCanal>
</tokenDefinition>'];
*/
    
        $respuestas = $this->getGenerarToken($req);


        
        foreach ($respuestas as $respuesta) {
        $codRespuesta = $respuesta->codRespuesta;
        $codRespuestaSpecified = $respuesta->codRespuestaSpecified;
        $descRespuesta = $respuesta->descRespuesta;
        $descRespuestaSpecified = $respuesta->descRespuestaSpecified;
        $key = $respuesta->token;
        
         }
   
        return $key;
    }

    public function getGeneraTokenm()
    {   

        $req = ['cData' => '<tokenDefinition>
        <userID>14096803-k</userID>
        <RutEmpresa>83314008-1</RutEmpresa>
        <NomEmpresa>PATROLL INTERNATIONAL S.A.</NomEmpresa>
        <Contrato>000000000000016</Contrato>
        <RutUsuario>14096803-k</RutUsuario>
        <NomUsuario>José Antonio Claros Dulón</NomUsuario>
        <Rol>LALA</Rol>
        <Segmento>N</Segmento>
        <Servicio>Pago Proveedores</Servicio>
        <subCanal>M</subCanal>
        <codProd></codProd>
        <codllamado>M</codllamado>
        </tokenDefinition>'];

        $respuestas = $this->getGenerarToken($req);

        foreach ($respuestas as $respuesta) {
        $codRespuesta = $respuesta->codRespuesta;
        $codRespuestaSpecified = $respuesta->codRespuestaSpecified;
        $descRespuesta = $respuesta->descRespuesta;
        $descRespuestaSpecified = $respuesta->descRespuestaSpecified;
        $key = $respuesta->token;
        
         }
   
        return $key;
    }

}
