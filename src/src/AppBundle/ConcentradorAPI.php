<?php

namespace AppBundle;

use AppBundle\Entity\HomologacionProducto;
use AppBundle\Entity\Insurance;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use SeguroBundle\Entity\PublicInsurance;

class ConcentradorAPI
{
    /** Servicios */
    private $logger;
    private $entityManager;

    /** Propiedades */
    private $apiKey;
    private $secretKey;
    private $apiUrl;
    private $connectTimeout;
    private $requestTimeout;
    private $excludedProducts;
    private $productSendEmail;

    private $tipoSeguroVida = array("life", "travel", "health");
    private $codParentesco = array();

    public function __construct(EntityManager $entityManager, LoggerInterface $logger, $options)
    {
        $this->logger = $logger;
        $this->entityManager = $entityManager;
        $this->apiUrl = $options["apiVentaUrl"];
        $this->apiKey = $options["apiKey"];
        $this->secretKey = $options["secretKey"];
        $this->connectTimeout = isset($options["connectTimeout"]) ? $options["connectTimeout"] : 60;
        $this->requestTimeout = isset($options["requestTimeout"]) ? $options["requestTimeout"] : 60;
        $this->excludedProducts = isset($options["excludedProducts"]) && is_array($options["excludedProducts"]) ? $options["excludedProducts"] : array();
        $this->productSendEmail = isset($options["productSendEmail"]) && is_array($options["productSendEmail"]) ? $options["productSendEmail"] : array();
    }

    /**
     * Obtiene la oferta de un producto
     * @param string $product
     * @param string $periodicidad
     * @param string $fechaNacimiento
     * @param string $canal
     * @param string|null $curso
     * @return array
     */
    public function obtieneOferta($product, $periodicidad, $fechaNacimiento, $canal, $curso = null) {
        $data = array(
            "KEY"=> $this->apiKey,
	        "RELLAMADO"=> 0,
	        "PRODUCTO"=> $product,
	        "PERIODICIDAD"=> $periodicidad,
	        "FECHANACIMIENTO"=> $fechaNacimiento,
	        "CURSO"=> $curso,
	        "CANAL"=> $canal
        );

        return $this->getOutput($this->doRequest("WSC_ObtieneOferta", $data));
    }


    /**
     * Crea solicitud de venta
     * @param Insurance $insurance
     */
    public function creaSolicitudVenta(Insurance $insurance) {
        $canal = "85";

        $product = $insurance->getProduct();

        if ($this->isExcluded($product->getId())) {
            $this->logger->info("Insurance '" . $insurance->getId(). "' with product_Id '" . $product->getId() . "' is excluded and will not be sent to the API Ventas");
            return null;
        }

        $payer = $insurance->getPayer();
        $insured = $insurance->getInsured();

        $homolProducto = $this->getProductoHomologacion($product->getId(), $insurance->getPri(), $product->getCode(), $insurance->getPlan()["code"]);

        $oferta = $this->obtieneOferta($homolProducto->getProCod(), $this->getPeriodoPago($insurance->getCodFrecPago()), $this->formatDate($insured->getBirthday()), $canal, $insurance->getCurso());

        if ($oferta == null || !isset($oferta["MATRIZ_OFERTAS"])) {
            return null;
        }

        $data = array(
            "KEY"=> $this->apiKey,
            "CODIGOCANAL"=> $canal,
            "TIPOGENERACION"=> "S",
            "NUMEROPROPUESTA"=> $insurance->getId(),
            "RUTEJECUTIVO"=> $this->formatRut($payer->getCode()),
            "CODSUCURSAL"=> "559",
            "CODINSTITUCION"=> "4",
            "USUARIO" => "WEB20",
            "TIPOSEGURO"=> in_array($insurance->getType(), $this->tipoSeguroVida) ? "V" : "G",
            "CURSOVIDAEDUCA" => $insurance->getCurso(),
            "FLAGCOMPROBANTE" => $this->checkAnswer($insurance->getAnswerByQuestion('Enviar Certificado Domicilio'), "D", "E"),
            "ESCALAR_PRODUCTO"=> array(
                "CODPRODUCTO"=> $homolProducto->getProCod(),
                "CODPLAN"=> $homolProducto->getProPlnCod(),
                "FECHAVIAJE" => $this->formatDate($homolProducto->getProCod() == '336' ? $insurance->getCreated() : $insurance->getTravelDate())
            ),
            "ESCALAR_ASEGURADO_PRINCIPAL"=> array(
                "RUTCLIENTE"=> $this->formatRut($insured->getCode()),
                "NOMASEGURADO"=> $insured->getName(),
                "APEMATASEGURADO"=> $insured->getLastname1(),
                "APEPATASEGURADO"=> $insured->getLastname2(),
                "FECHANACASEGURADO"=> $this->formatDate($insured->getBirthday()),
                "SEXOASEGURADO"=> $this->getCustomer($insured->getCode())->getCodSexo(),
                "ESTCIVASEGURADO"=> "0",
                "DIRASEGURADO"=> $insured->getAddress(),
                "COMUNAASEGURADO"=> $insured->getCodState(),
                "CIUDADASEGURADO"=> $insured->getCodCity(),
                "TELEFONOASEGURADO"=> $insured->getAddressCellphone() ? $insured->getAddressCellphone() : $insured->getAddressPhone(),
                "EMAILASEGURADO"=> $insured->getAddressEmail()
            ),
            "ESCALAR_CONTRATANTE" => array(
                "RUTCONTRATANTE"=> $this->formatRut($payer->getCode()),
                "NOMCONTRATANTE"=> $payer->getName(),
                "APEPATCONTRATANTE"=> $payer->getLastname1(),
                "APEMATCONTRATANTE"=> $payer->getLastname2(),
                "SEXOCONTRATANTE"=> $this->getCustomer($payer->getCode())->getCodSexo(),
                "ESTCIVCONTRATANTE"=> "0",
                "DIRCONTRATANTE"=> $payer->getAddress(),
                "COMUNACONTRATANTE"=> $payer->getCodState(),
                "CIUDADCONTRATANTE"=> $payer->getCodCity(),
                "TELCONTRATANTE"=> $payer->getAddressCellphone() ? $payer->getAddressCellphone() : $payer->getAddressPhone(),
                "EMAILCONTRATANTE"=> $payer->getAddressEmail()
            ),
            "MATRIZ_MEDIO_PAGO" => array(),
            "MATRIZ_COBERTURAS"=> array(),
            "MATRIZ_BENEFICIARIOS"=> array()
        );

        $this->addCoberturas($data, $homolProducto->getProPlnCod(), $oferta);
        $this->addMedioPago($data, $insurance);

        if ($product->getFamily()->getRoot() == 2) {
            $this->addGeneralHogar($data, $insurance);
        }

        $this->addVidaDPS($data, $insurance);

        $idx = 1;
        $this->addBeneficiarios($data, $insurance->getCargas(), "1", $idx);
        $this->addBeneficiarios($data, $insurance->getBeneficiaries(), "3", $idx);

        return $this->getOutput($this->doRequest("WSC_creasolicitudventa", $data));
    }

    /**
     * Crea solicitud de venta para el portal publico
     * @param PublicInsurance $insurance
     */
    public function creaSolicitudVentaPublic(PublicInsurance $insurance) {
        $canal = "90";

        $product = $insurance->getProduct();

        if ($this->isExcluded($product->getId())) {
            $this->logger->info("Public insurance '" . $insurance->getId() . "' with product_Id '" . $product->getId() . "' is excluded and will not be sent to the API Ventas");
            return null;
        }

        $client = $insurance->getClient();

        $homolProducto = $this->getProductoHomologacion($product->getId(), $insurance->getPri(), $product->getCode(), $insurance->getPlan()["code"]);

        $oferta = $this->obtieneOferta($homolProducto->getProCod(), $this->getPeriodoPago($insurance->getCodFrecPago()), $this->formatDate($client->getBirthday()), $canal);

        if ($oferta == null || !isset($oferta["MATRIZ_OFERTAS"])) {
            return null;
        }

        $data = array(
            "KEY"=> $this->apiKey,
            "CODIGOCANAL"=> $canal,
            "TIPOGENERACION"=> "S",
            "NUMEROPROPUESTA"=> $insurance->getId(),
            "RUTEJECUTIVO"=> $this->formatRut($client->getCode()),
            "CODSUCURSAL"=> "559",
            "CODINSTITUCION"=> "4",
            "USUARIO" => "WEB20",
            "TIPOSEGURO"=> in_array($insurance->getType(), $this->tipoSeguroVida) ? "V" : "G",
            "FLAGCOMPROBANTE" => $this->checkAnswer($insurance->getAnswerByQuestion('Enviar Certificado Domicilio'), "D", "E"),
            "ESCALAR_PRODUCTO"=> array(
                "CODPRODUCTO"=> $homolProducto->getProCod(),
                "CODPLAN"=> $homolProducto->getProPlnCod(),
                "FECHAVIAJE" => $this->formatDate($product->getCode() == '336' ? $insurance->getCreated() : $insurance->getTravelDate(), 'd/m/Y')
            ),
            "ESCALAR_ASEGURADO_PRINCIPAL"=> array(
                "RUTCLIENTE"=> $this->formatRut($client->getCode()),
                "NOMASEGURADO"=> $client->getName(),
                "APEMATASEGURADO"=> $client->getLastname1(),
                "APEPATASEGURADO"=> $client->getLastname2(),
                "FECHANACASEGURADO"=> $this->formatDate($client->getBirthday()),
                "SEXOASEGURADO"=> "M", // NO HAY FORMA DE DETERMINARLO
                "ESTCIVASEGURADO"=> "0",
                "DIRASEGURADO"=> $client->getAddress(),
                "COMUNAASEGURADO"=> $client->getCodState(),
                "CIUDADASEGURADO"=> $client->getCodCity(),
                "TELEFONOASEGURADO"=> $client->getAddressCellphone() ? $client->getAddressCellphone() : $client->getAddressPhone(),
                "EMAILASEGURADO"=> $client->getAddressEmail()
            ),
            "ESCALAR_CONTRATANTE" => array(
                "RUTCONTRATANTE"=> $this->formatRut($client->getCode()),
                "NOMCONTRATANTE"=> $client->getName(),
                "APEPATCONTRATANTE"=> $client->getLastname1(),
                "APEMATCONTRATANTE"=> $client->getLastname2(),
                "SEXOCONTRATANTE"=> "M", // NO HAY FORMA DE DETERMINARLO
                "ESTCIVCONTRATANTE"=> "0",
                "DIRCONTRATANTE"=> $client->getAddress(),
                "COMUNACONTRATANTE"=> $client->getCodState(),
                "CIUDADCONTRATANTE"=> $client->getCodCity(),
                "TELCONTRATANTE"=> $client->getAddressCellphone() ? $client->getAddressCellphone() : $payer->getAddressPhone(),
                "EMAILCONTRATANTE"=> $client->getAddressEmail()
            ),
            "MATRIZ_MEDIO_PAGO" => array(),
            "MATRIZ_COBERTURAS"=> array(),
            "MATRIZ_BENEFICIARIOS"=> array()
        );

        $this->addCoberturas($data, $homolProducto->getProPlnCod(), $oferta);
        $this->addMedioPagoPublic($data, $insurance);

        $idx = 1;
        $this->addBeneficiarios($data, $insurance->getCargas(), "1", $idx);
        $this->addBeneficiarios($data, $insurance->getBeneficiaries(), "3", $idx);

        return $this->getOutput($this->doRequest("WSC_creasolicitudventa", $data));
    }

    /**
    * Verifica si debe enviarse el email a través de la web 2.0 en base al producto
    * @param Product $product
    * @return boolean
    */
    public function mustSendEmail($product) {
        return in_array($product->getId(), $this->productSendEmail);
    }

    /**
    * Verifica si el id de producto debe ser enviado por la API
    * @param string $product_id
    * @return boolean
    */
    private function isExcluded($product_id) {
        return in_array($product_id, $this->excludedProducts);
    }

    /**
     * Obtiene el producto/plan homologado
     * @param integer $product_id
     * @param string $plan_code
     * @param string $product_code
     * @return HomologacionProducto|null
     */
    private function getProductoHomologacion($product_id, $pri, $product_code, $plan_code) {
        $homolProducto = $this->entityManager->getRepository('AppBundle:HomologacionProducto')->findOneBy(array('product_id' => $product_id, "pri" => $pri));

        if ($homolProducto == null) {
            $homolProducto = new HomologacionProducto();
            $homolProducto->setId($product_id);
            $homolProducto->setPri($pri);
            $homolProducto->setProductCode($product_code);
            $homolProducto->setProCod($product_code);
            $homolProducto->setProPlnCod($plan_code);
        }

        return $homolProducto;
    }

    /**
     * Añade la información de las coberturas
     * @param array $data
     * @param string $codigoPlan
     * @param array $oferta
     */
    private function addCoberturas(&$data, $codigoPlan, array $oferta) {
        $ofertas = isset($oferta["MATRIZ_OFERTAS"]) && is_array($oferta["MATRIZ_OFERTAS"]) ? $oferta["MATRIZ_OFERTAS"] : NULL;
        if ($ofertas != null) {
            foreach($ofertas as $obj) {
                $planes = isset($obj["MATRIZ_PLANES"]) && is_array($obj["MATRIZ_PLANES"]) ? $obj["MATRIZ_PLANES"] : NULL;
                if ($planes != null) {
                    foreach($planes as $plan) {
                        if ($plan["CODIGOPLAN"] == $codigoPlan && isset($plan["MATRIZ_COBERTURAS"])) {
                            foreach($plan["MATRIZ_COBERTURAS"] as $cobertura) {
                                $data["MATRIZ_COBERTURAS"][] = array(
                                    "CODCOBERTURA"=> $cobertura["CODIGOCOBERTURA"],
                                    "CAPITAL"=> $cobertura["CAPITAL"],
                                    "PRIMACOB"=> $cobertura["PRIMABRUTAUF"]
                                );
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Añade la información de medios de pago
     * @param array $data
     * @param Insurance $insurance
     */
    private function addMedioPago(&$data, Insurance $insurance) {
        $codMedioPago = null;
        
        $medioPago = array(
            "CODBCOCTCTJT"=> "37",
            "DIAPAGO"=> $insurance->getPaymentDay(),
            "PRIMAPAGAR"=> $insurance->getPrice(),
            "PERIODOPAGO"=> $this->getPeriodoPago($insurance->getCodFrecPago())
        );

        if ($insurance->getCodMedPago() == "T") {
            $cardNumber = $insurance->getPaymentMethod();
            $medioPago["NUMTARJ1"] = substr($cardNumber, 0, 4);
            $medioPago["NUMTARJ2"] = substr($cardNumber, 4, 4);
            $medioPago["NUMTARJ3"] = substr($cardNumber, 8, 4);
            $medioPago["NUMTARJ4"] = substr($cardNumber, 12, 4);
            $codMedioPago = $this->getCreditCardType($cardNumber);
        } else {
            $codMedioPago = 2;
            $medioPago["CTACTE"] = $insurance->getPaymentMethod();
        }

        $medioPago["CODVIAPAGO"] = $codMedioPago;

        $data["MATRIZ_MEDIO_PAGO"][] = $medioPago;
    }

    /**
     * Añade la información de medios de pago para portal público
     * @param array $data
     * @param PublicInsurance $insurance
     */
    private function addMedioPagoPublic(&$data, PublicInsurance $insurance) {
        $product = $insurance->getProduct();
        $medioPago = array(
            "CODBCOCTCTJT"=> "37",
            "CODVIAPAGO" => "31",
            "PRIMAPAGAR"=> $insurance->getPrice(),
            "PERIODOPAGO"=> $this->getPeriodoPago($insurance->getCodFrecPago()),
            "NUMTARJ1" => "0000",
            "NUMTARJ2" => "0000",
            "NUMTARJ3" => "0000"
        );

        $cardNumber = "";
        if ($product->getPatPass() == 1) {
            $patPass = $insurance->getPatPass();
            $medioPago["DIAPAGO"] = $patPass->getPaymentDay();

            $cardNumber = $patPass->getCardNumber();
        } else {
            $payment = $insurance->getPaiement();

            $tmp = explode("T", $payment->getTransactionDate());
            $diapago = isset($tmp[0]) && is_numeric(substr($tmp[0], -2)) ? substr($tmp[0], -2) : $payment->getCreated()->format("d");

            $medioPago["DIAPAGO"] = $diapago;
            $medioPago["AUTORIZACIONPAGO"] = $payment->getAuthorizationCode();

            $cardNumber = "000000000000".$payment->getCardNumber();
        }

        $medioPago["NUMTARJ1"] = substr($cardNumber, 0, 4);
        $medioPago["NUMTARJ2"] = substr($cardNumber, 4, 4);
        $medioPago["NUMTARJ3"] = substr($cardNumber, 8, 4);
        $medioPago["NUMTARJ4"] = substr($cardNumber, 12, 4);

        $data["MATRIZ_MEDIO_PAGO"][] = $medioPago;
    }

    /**
     * Añade la información de General Hogar
     * @param array $data
     * @param Insurance $insurance
     */
    private function addGeneralHogar(&$data, Insurance $insurance) {
        $home = $insurance->getHome();
        $data["ESCALAR_SEG_GENERALES"] = array(
            "ESCALAR_BIEN_ASEGURADO" => array(
                "MATERIAASEGURADA"=> $home->getAddress() . "/" . $home->getCodState() . "/" . $home->getCodCity(),
                "CODTIPOVIVIENDA" => "",
                "CODTIPOCASA" => "1",
                "UBICACIONINDUSTRIA" => "",
                "UBICACIONCURSOAGUA" => "",
                "DIRBIENASEGURADO"=> $home->getAddress(),
                "COMUNABIENASEGURADO"=> $home->getCodState(),
                "CIUDADBIENASEGURADO"=> $home->getCodCity(),
                "TELEFBIENASEGURADO" => ""
            ),
            "MATRIZ_HOGAR_MASCOTAS" => array()
        );
    }

    /**
     * Añade la información de VIDA DPS
     * @param array $data
     * @param Insurance $insurance
     */
    private function addVidaDPS(&$data, Insurance $insurance) {

        $enfermedad = $insurance->getEnfermedad();
        $deporte1 = $insurance->getAnswerByQuestion('Deportes Riesgo');
        $deporte2 = $insurance->getAnswerByQuestion('Actividad Riesgo');
        $deporte3 = $insurance->getAnswerByQuestion('Actividad Ejemplo');
        $peso = $insurance->getPeso();
        $estatura = $insurance->getEstatura();

        $descEnfermedad = $this->checkAnswer($enfermedad, $enfermedad);

        $data["ESCALAR_SEG_VIDA_DPS"] = array(
            "TIPODPS"=> "S", // POR CONFIRMAR
            "FLAGENFERMEDAD"=> $this->checkAnswer($descEnfermedad, "S", "N"),
            "DETENFERMEDAD"=> $descEnfermedad,
            "TIPOACTIVIDAD"=> "1", // POR CONFIRMAR
            "FLAGDEPORTE"=> ($this->checkAnswer($deporte1, true, false) || $this->checkAnswer($deporte2, true, false) || $this->checkAnswer($deporte3, true, false)) ? "S" : "N",
            "DESCDEPORTE1"=> $this->checkAnswer($deporte1, $deporte1),
            "DESCDEPORTE2"=> $this->checkAnswer($deporte2, $deporte2),
            "DESCDEPORTE3"=> $this->checkAnswer($deporte3, $deporte3),
            "DESCOTROSDEPORTE"=> "",
            "ESPILOTO"=> "",
            "TIPOAVION"=> "",
            "PADECEENFERMEDAD"=> $this->checkAnswer($descEnfermedad, "S", "N"),
            "DESCPADECEENFERMEDAD"=> $descEnfermedad,
            "FLAGCONSULTAMED"=> "",
            "DETCONSULTAMED"=> "",
            "FLAGOPERADO"=> "",
            "DETOPERADO"=> "",
            "FLAGDROGAS"=> "",
            "FLAGFUMA"=> ""
        );

        if ($peso != null) {
            $data["ESCALAR_SEG_VIDA_DPS"]["PESO"] = $peso;
        }

        if ($estatura != null) {
            $data["ESCALAR_SEG_VIDA_DPS"]["ESTATURA"] = $estatura;
        }
    }

    /**
     * Añade beneficiarios a la peticion
     * @param array $data
     * @param array $collection
     * @param string $type Tipo de beneficiario
     * @param int $idx Secuencial de beneficiario
     */
    private function addBeneficiarios(&$data, $collection, $type, &$idx) {
        
        foreach($collection as $element) {
            $obj = array(
                "TIPOBENEFICIARIO"=> $type,
                "SECUENCIAL"=> $idx,
                "RUT" => $this->formatRut($element->getCode()),
                "NOMBRES"=> $element->getName(),
                "APEPATERNO"=> $element->getLastname1(),
                "APEMATERNO"=> $element->getLastname2(),
                "FECNACIMIENTO"=> $this->formatDate($element->getBirthday()),
                "CODPARENTESCO"=> $this->getParentesco($element->getRelationship())
            );

            if ($type == "3") {
                $obj["PORCENTAJE"] = $element->getCapital();
            }

            $data["MATRIZ_BENEFICIARIOS"][] = $obj;
            $idx++;
        }
    }

    /**
     * Obtiene un Customer a partir del code
     * @param string $code
     * @return Customer
     */
    private function getCustomer($code) {
        return $this->entityManager->getRepository('AppBundle:Customer')->findOneBy(array('code' => $code));
    }

    /**
     * Obtiene el código de parentesco
     * @param string $relationship
     * @return integer
     */
    private function getParentesco($relationship) {

        if (!isset($this->codParentesco[$relationship])) {
            $homolParentesco = $this->entityManager->getRepository('AppBundle:HomologacionParentesco')->findOneBy(array('relationship' => $relationship));

            if ($homolParentesco != null) {
                $this->codParentesco[$relationship] = $homolParentesco->getCodParentesco();
            } else {
                $this->codParentesco[$relationship] = "0";
            }
        }

        return isset($this->codParentesco[$relationship]) ? $this->codParentesco[$relationship] : "0";
    }

    /**
     * Obtiene el tipo de tarjeta de crédito
     * @param CreditCard $creditCard
     * @return int
     */
    private function getCreditCardType($creditCard) {
        
        $types = array(
            "1" => array('/^5[1-5]|^2(?:2(?:2[1-9]|[3-9]\d)|[3-6]\d\d|7(?:[01]\d|20))\d/'), // MASTERCARD
            "4" => array("^(4026|417500|4508|4844|491(3|7))", "^4"), // VISA
            "9" => array("^3[47]"), // AMEX
            "6" => array("^30[0-5]", "^(30[6-9]|36|38)"), // Diners
        );

        $creditCardType = null;
        foreach ($types as $type => $regexs) {
            foreach ($regexs as $regex) {
                if (preg_match($regex, $creditCard)) {
                    $creditCardType = $type;
                    break;
                }        
            }

            if ($creditCardType !== null) {
                break;
            }
        }

        return $creditCardType;
    }

    /**
     * Obtiene el período de pago
     * @param int $periodo
     * @return string
     */
    private function getPeriodoPago($periodo) {
        $codigo = null;

        switch($periodo) {
            case 1:
                $codigo = "M";
                break;
            case 4:
                $codigo = "A";
                break;
            case 5:
                $codigo = "U";
                break;
        }

        return $codigo;
    }

    /**
     * Revisa una respuesta a una pregunta
     * @param string $answer
     * @param string $yes
     * @param string $no
     * @return 
     */
    private function checkAnswer($answer, $yes, $no = "") {
        return $answer != null && $answer != "" && strtoupper($answer) != "NO" ? $yes : $no;
    }

    /**
     * Formatea rut utilizando el formado definido en el concentrador 
     * ej: 0014241810K
     * @param string $rut 
     * @return string
     */
    private function formatRut($rut) {
        return str_pad(str_replace("-", "", $rut), 10, "0", STR_PAD_LEFT);
    }

    /**
     * Formatea fecha utilizando el formato definido en el concentrados
     * ej: "01/01/2000"
     * @param \Date $date
     * @return string
     */
    private function formatDate($date, $format = 'd-m-Y') {
        if ($date != null && is_string($date)) {
            $date = date_create_from_format($format, $date);
        }
        return $date != null ? date_format($date, 'd/m/Y') : null;
    }

    /**
     * Realiza la petición HTTP
     * @param string $service Nombre de servicio
     * @param array $params Parámetros a enviar
     * @return array Respuesta del servicio
     */
    private function doRequest($service, $params) {
        $url = $this->apiUrl . "/$service";
        $data = json_encode($params);

        $headers = array(
            "Content-Type: application/json", 
            "Content-Length: " . strlen($data),
            "Api-Key: {$this->apiKey}"
        );

        if ($this->secretKey != '') {
            $headers[] = "Token: " . hash_hmac('sha256', $data , $this->secretKey);
        }

        $this->logger->info("Request to $url with data " . $data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->connectTimeout);
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->requestTimeout);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $output = curl_exec($ch);
        $this->logger->info("Response $output");
        $response = null;
        if($output === false) {
            $error = curl_error($ch);
            $this->logger->error($error);
        } else {
            $tmp = json_decode($output, true);
            if (json_last_error() == JSON_ERROR_NONE) {
                $response = $tmp;
            }
        }
        curl_close($ch);
        return $response;
    }

    /**
     * Obtiene el OUTPUT de la respuesta sólo si esta fue exitosa, en caso contrario null
     * @param array|null $response
     * @return array|null 
     */
    private function getOutput($response) {
        if ($response == null) {
            return null;
        }

        $coderr = isset($response["INFO"]) && isset($response["INFO"]["CODERR"]) ? $response["INFO"]["CODERR"] : "";

        return $coderr == "0" && $response["OUTPUT"] ? $response["OUTPUT"] : null;
    }
}