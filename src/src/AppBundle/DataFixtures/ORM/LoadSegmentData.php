<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Nelmio\Alice\Fixtures;

/**
 * LoadSegmentData
 *
 * @author Marcos Matamala <marcos.matamala@taisachile.cl>
 */
class LoadSegmentData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $options = ['providers' => [$this]];
        $objects = Fixtures::load(__DIR__.'/data.yml', $manager, $options);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 2;
    }

    /**
     * Entrega un RUT al azar.
     *
     * @return string
     */
    public function run()
    {
        $r = rand(7000000, 25000000);

        return $r . '-' . $this->dv($r);
    }

    /**
     * Determina el DV de un RUT.
     *
     * @param int $r
     * @return string
     */
    public function dv($r)
    {
        $s = 1;
        for ($m = 0; $r != 0; $r/=10) {
            $s = ($s+$r%10*(9-$m++%6))%11;
        }

        return chr($s ? $s + 47 : 75);
    }
}
