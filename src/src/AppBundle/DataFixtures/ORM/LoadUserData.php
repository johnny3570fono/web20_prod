<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Carga los usuarios por defectos del sistema
 */
class LoadUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $users = [
            'superadmin',
            'admin',
            'editor',
        ];

        foreach ($users as $name) {
            $userManager = $this->container->get('fos_user.user_manager');
            $u = $userManager->createUser();
            $u->setUsername($name);
            $u->setEmail(sprintf('%s@taisachile.cl', $name));
            $u->setPlainPassword('1.,');
            $u->setEnabled(true);
            $u->setRoles(['ROLE_ADMIN']);

            $manager->persist($u);
            $manager->flush();

            $this->addReference(sprintf('user-%s', $name), $u);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }
}
