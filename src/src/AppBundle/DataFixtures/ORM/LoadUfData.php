<?php

namespace AppBundle\DataFixtures\ORM;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nelmio\Alice\Fixtures;

/**
 * LoadUfData
 *
 * @author Marcos Matamala <marcos.matamala@taisachile.cl>
 */
class LoadUfData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

        /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        /**
         * 1990 a 2013
         */
        $objects = Fixtures::load(__DIR__.'/ufs.yml', $manager);

        /**
         * 2014 hasta date('Y')
         */
        // $kernel = $this->container->get('kernel');
        // $application = new Application($kernel);
        // $application->setAutoExit(false);
        //
        // $years = range(2014, date('Y'));
        //
        // foreach ($years as $year) {
        //     $input = new ArrayInput(['command' => 'uf:scrap', $year]);
        //
        //     $output = new BufferedOutput();
        //     $application->run($input, $output);
        // }
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 3;
    }
}
