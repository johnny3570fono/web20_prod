<?php
namespace PlanificadorBundle\Helper;

class Utils{
    function getAgesChild( $age ) {
		$age = (int) $age;
		
		$age_a = [];
		$age_a['0'] = "Menor a 1 a�o";
		$age_a['1'] = "Hasta 18 a�os";
		$age_a['2'] = "Hasta 24 a�os";
		$age_a['3'] = "M�s de 24 a�os";
		
		return utf8_encode( $age_a[$age] );
	}
	
	function convertSlug( $str, $delimiter = '-' ) {
		$slug = utf8_decode( $str );
		$slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
		
		return $slug;
	}
	
	function getPregById( $id ) {
		$id = (int) $id;
		
		$preg_a = [];
		$preg_a['103'] = "familia";
		$preg_a['105'] = "gastos";
		$preg_a['121'] = "seguros";
		$preg_a['274'] = "valor_cosas";
		
		return utf8_encode( $preg_a[$id] );
	}
	
}