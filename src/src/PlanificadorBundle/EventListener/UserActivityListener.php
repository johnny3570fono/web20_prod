<?php

namespace PlanificadorBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\Validator\Validator\ValidatorInterface;

use PlanificadorBundle\Entity\plan_UserActivity;

/**
 * Registra actividad del usuario.
 */
class UserActivityListener
{
    private $em;
    private $context;
    private $logger;
    private $validator;

    /**
     * Constructor
     *
     * @param EntityManager $entityManager
     */
    public function __construct(
        SecurityContext $context,
        EntityManager $entityManager,
        Logger $logger,
        ValidatorInterface $validator
    ) {
        $this->em = $entityManager;
        $this->context = $context;
        $this->logger = $logger;
        $this->validator = $validator;
    }

    /**
     * Registra la actividad del usuario.
     *
     * @param GetResponseEvent $event
     * @return null
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        try {
            if (!$event->isMasterRequest()
                    || !$this->context->isGranted('IS_AUTHENTICATED_FULLY')) {
                return;
            } else {
                $user = $this->context->getToken()->getUser();

                $elusername = $user->getUsername();
                if ( method_exists($user, 'getCode') )
                    $elusername = $user->getCode();

                $request  = $event->getRequest();

                $activity = new plan_UserActivity();
                $activity->setIp($request->getClientIp());
                $activity->setUrl($request->getUri());
                $activity->setUsername($elusername);
                $activity->setCreatedAt(new \DateTime('now'));

                $errors = $this->validator->validate($activity);

                if (count($errors) > 0) {
                    foreach ($errors as $e) {
                        $this->error($e->getMessage());
                    }
                } else {
                    $this->em->persist($activity);
                    $this->em->flush();
                }
            }
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }

    /**
     * Atajo para loggear
     *
     * @param string $message
     */
    protected function error($message)
    {
        $this->logger->error(__CLASS__ . ' - ' . $message);
    }
}
