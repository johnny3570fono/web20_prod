<?php

namespace PlanificadorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * plan_logConfirmacion
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="PlanificadorBundle\Entity\plan_logConfirmacionRepository")
 */
class plan_logConfirmacion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="rut", type="string", length=20)
     */
    private $rut;

    /**
     * @var integer
     *
     * @ORM\Column(name="idProduct", type="integer")
     */
    private $idProduct;

    /**
     * @var integer
     *
     * @ORM\Column(name="idPlan", type="integer")
     */
    private $idPlan;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rut
     *
     * @param string $rut
     * @return plan_logConfirmacion
     */
    public function setRut($rut)
    {
        $this->rut = $rut;

        return $this;
    }

    /**
     * Get rut
     *
     * @return string 
     */
    public function getRut()
    {
        return $this->rut;
    }

    /**
     * Set idProduct
     *
     * @param integer $idProduct
     * @return plan_logConfirmacion
     */
    public function setIdProduct($idProduct)
    {
        $this->idProduct = $idProduct;

        return $this;
    }

    /**
     * Get idProduct
     *
     * @return integer 
     */
    public function getIdProduct()
    {
        return $this->idProduct;
    }

    /**
     * Set idPlan
     *
     * @param integer $idPlan
     * @return plan_logConfirmacion
     */
    public function setIdPlan($idPlan)
    {
        $this->idPlan = $idPlan;

        return $this;
    }

    /**
     * Get idPlan
     *
     * @return integer 
     */
    public function getIdPlan()
    {
        return $this->idPlan;
    }
}
