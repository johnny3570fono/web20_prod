<?php

namespace PlanificadorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * plan_momento
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="PlanificadorBundle\Entity\plan_momentoRepository")
 * @Vich\Uploadable
 */
class plan_momento
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=500)
     */
    private $descripcion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="visible", type="boolean")
     */
    private $visible;

    /**
      * @var File
      *
      * @Vich\UploadableField(mapping="momentos", fileNameProperty="imagenTipo",nullable=true)
      * @Assert\Image(
      *     maxSize = "1M",
      *     mimeTypes = {"image/jpeg", "image/png"},
      *     maxWidth=850,
      *     minHeight=160,
      * )
      */
    private $imagen;

    /**
     * @var string
     *
     * @ORM\Column(name="imagenTipo", type="string", length=255, nullable=true)
     */
    private $imagenTipo;

    /**
       * @ORM\Column(type="datetime", nullable=true)
       *
       * @var \DateTime
    */
      private $updatedAt;
    /**
     * @var integer
     *
     * @ORM\Column(name="orderby", type="integer", length=10,nullable=true )
     */
    private $orderBy;

    private $desOrderBy;

    /**
     * @ORM\OneToMany(targetEntity="plan_mom_preo", mappedBy="idMomento")
     */
    protected $momPreo;
 
   
    public function __construct()
    {
        $this->momPreo = new \Doctrine\Common\Collections\ArrayCollection();
    }
 


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return plan_momento
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     * @return plan_momento
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean 
     */
    public function getVisible()
    {
        return $this->visible;
    }
    /**
     * Sets file.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $imagen
     *
     * @return plan_momento
     */
    public function setImagen(File $imagen = null)
    {
        $this->imagen = $imagen;
        if ($imagen) {

            $this->updatedAt = new \DateTime;
        }
    }
   

    /**
     * Get imagen
     *
     * @return  File|null
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set imagenTipo
     *
     * @param string $imagenTipo
     * @return plan_momento
     */
    public function setImagenTipo($imagenTipo)
    {
        $this->imagenTipo = $imagenTipo;

        return $this;
    }

    /**
     * Get imagenTipo
     *
     * @return string 
     */
    public function getImagenTipo()
    {
        return $this->imagenTipo;
    }



    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return plan_momento
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set orderBy
     *
     * @param integer $orderBy
     * @return plan_momento
     */
    public function setOrderBy($orderBy)
    {

        $this->orderBy = $orderBy;

        return $this;
    }

    /**
     * Get orderBy
     *
     * @return integer 
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }


    public function setDesOrderBy($desOrderBy)
    {
        $this->desOrderBy = $desOrderBy;

        return $this;
    }

    public function getDesOrderBy()
    {
        return $this->desOrderBy;
    }

    /**
     * Add momPreo
     *
     * @param \PlanificadorBundle\Entity\plan_mom_preo $momPreo
     * @return plan_momento
     */
    public function addMomPreo(\PlanificadorBundle\Entity\plan_mom_preo $momPreo)
    {
        $this->momPreo[] = $momPreo;

        return $this;
    }

    /**
     * Remove momPreo
     *
     * @param \PlanificadorBundle\Entity\plan_mom_preo $momPreo
     */
    public function removeMomPreo(\PlanificadorBundle\Entity\plan_mom_preo $momPreo)
    {
        $this->momPreo->removeElement($momPreo);
    }

    /**
     * Get momPreo
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMomPreo()
    {
        return $this->momPreo;
    }
}
