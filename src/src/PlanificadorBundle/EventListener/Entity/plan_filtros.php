<?php

namespace PlanificadorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * plan_filtros
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="PlanificadorBundle\Entity\plan_filtrosRepository")
 */
class plan_filtros
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="plan_arbol", inversedBy="filtros")
     * @ORM\JoinColumn(name="idarbol", referencedColumnName="id")
     * @return integer
     */
    private $arbol;

     /**
     * @var string
     *
     * @ORM\Column(name="json_filtro", type="json_array", nullable=true)
     */
    private $json_filtro;

    /***Variables para filtros generales sexo,segmento y edad ***/

     /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=1, nullable=true)
     */
    private $tipo;

    /**
     * @var string
     *
     * @ORM\Column(name="json_total", type="json_array", nullable=true)
     */
    private $json_total;

    /***variables para filtro para preguntas generales de gastos ***/
    private $filtro;
    private $gastos;
    /*tambien va simbolo y valor que es reutilizada en las preg generales en familia**/

    /***variables para filtro para preguntas generales de familia ***/
    private $edocivil;
    private $simbhijos;
    private $canthijos;
    private $simbolo;
    private $valor;

    /***Variables para filtros de capital**/
    private $capital;      // Titulo del capital
    private $variables;   // A,B,C variables de la formula
    private $tipoPregunta; // para filtrar preguntas gene de espe
    private $oppregunta; // para preguntas genericas mostrar los gastos (arriendo, hogar,etc) que serian los titulos de las respuestas.
    private $preguntatitulo;

    /*****************/
    /***variable para guardar el json con todo el contenido*********/
    private $choicesjson;



    /**
     * @ORM\OneToMany(targetEntity="plan_mom_preo", mappedBy="plan_filtros")
     */
    protected $momPreo;
 
   
    public function __construct()
    {
        $this->momPreo = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

   

 /**
     * Set filtro
     *
     * @param boolean $filtro
     * @return plan_filtros
     */
    public function setFiltro($filtro)
    {
        $this->filtro = $filtro;

        return $this;
    }

    /**
     * Get filtro
     *
     * @return boolean 
     */
    public function getFiltro()
    {
        return $this->filtro;
    }

    

    /**
     * Add momPreo
     *
     * @param \PlanificadorBundle\Entity\plan_mom_preo $momPreo
     * @return plan_filtros
     */
    public function addMomPreo(\PlanificadorBundle\Entity\plan_mom_preo $momPreo)
    {
        $this->momPreo[] = $momPreo;

        return $this;
    }

    /**
     * Remove momPreo
     *
     * @param \PlanificadorBundle\Entity\plan_mom_preo $momPreo
     */
    public function removeMomPreo(\PlanificadorBundle\Entity\plan_mom_preo $momPreo)
    {
        $this->momPreo->removeElement($momPreo);
    }

    /**
     * Get momPreo
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMomPreo()
    {
        return $this->momPreo;
    }

    

    /**
     * Set arbol
     *
     * @param \PlanificadorBundle\Entity\plan_arbol $arbol
     * @return plan_filtros
     */
    public function setArbol(\PlanificadorBundle\Entity\plan_arbol $arbol = null)
    {
        $this->arbol = $arbol;

        return $this;
    }

    /**
     * Get arbol
     *
     * @return \PlanificadorBundle\Entity\plan_arbol 
     */
    public function getArbol()
    {
        return $this->arbol;
    }

   
    /**
     * Set simbolo
     *
     * @param integer $simbolo
     * @return plan_filtros
     */
    public function setSimbolo($simbolo)
    {
        $this->simbolo = $simbolo;

        return $this;
    }

    /**
     * Get simbolo
     *
     * @return integer 
     */
    public function getSimbolo()
    {
        return $this->simbolo;
    }

    /**
     * Set valor
     *
     * @param string $valor
     * @return plan_filtros
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return string 
     */
    public function getValor()
    {
        return $this->valor;
    }
     /**
     * Set choicesjson
     *
     * @param array $choicesjson
     * @return plan_filtros
     */
    public function setChoicesjson($choicesjson)
    {
        $this->choicesjson = $choicesjson;

        return $this;
    }

    /**
     * Get choicesjson
     *
     * @return array 
     */
    public function getChoicesjson()
    {
        return $this->choicesjson;
    }

   
    /**
     * Get json_filtro
     *
     * @return array 
     */
    public function getJsonFiltro()
    {
        return $this->json_filtro;
    }

    /**
     * Set json_filtro
     *
     * @param integer $simbolo
     * @return plan_filtros
     */
    public function setJsonFiltro($jsonFiltro)
    {
        $this->json_filtro = $jsonFiltro;

        return $this;
    }


    /**
     * Get json_total
     *
     * @return array 
     */
    public function getJsonTotal()
    {
        return $this->json_total;
    }

    /**
     * Set json_total
     *
     * @return json_total
     */
    public function setJsonTotal($json_total)
    {
        $this->json_total = $json_total;

        return $this;
    }
    /**
     * Set gastos
     *
     * @param string $gastos
     * @return plan_filtros
     */
    public function setGastos($gastos)
    {
        $this->gastos = $gastos;

        return $this;
    }

    /**
     * Get gastos
     *
     * @return string 
     */
    public function getGastos()
    {
        return $this->gastos;
    }
     /**
     * Set tipo
     *
     * @param string $tipo
     * @return plan_filtros
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }
    /**
     * Set edocivil
     *
     * @param string $edocivil
     * @return plan_filtros
     */
    public function setEdocivil($edocivil)
    {
        $this->edocivil = $edocivil;

        return $this;
    }

    /**
     * Get edocivil
     *
     * @return string 
     */
    public function getEdocivil()
    {
        return $this->edocivil;
    }
    /**
     * Set edocivil
     *
     * @param integer $canthijos
     * @return plan_filtros
     */
    public function setSimbhijos($simbhijos)
    {
        $this->simbhijos = $simbhijos;

        return $this;
    }

    /**
     * Get simbhijos
     *
     * @return integer 
     */
    public function getSimbhijos()
    {
        return $this->simbhijos;
    }
 
     /**
     * Set canthijos
     *
     * @param integer $canthijos
     * @return plan_filtros
     */
    public function setCanthijos($canthijos)
    {
        $this->canthijos = $canthijos;

        return $this;
    }

    /**
     * Get canthijos
     *
     * @return integer 
     */
    public function getCanthijos()
    {
        return $this->canthijos;
    }
    
    /**
     * Set capital
     *
     * @param integer $capital
     * @return plan_filtros
     */
    public function setCapital($capital)
    {
        $this->capital = $capital;

        return $this;
    }

    /**
     * Get capital
     *
     * @return integer 
     */
    public function getCapital()
    {
        return $this->capital;
    }

    /**
     * Set variables
     *
     * @param string $variables
     * @return plan_filtros
     */
    public function setVariables($variables)
    {
        $this->variables = $variables;

        return $this;
    }

    /**
     * Get variables
     *
     * @return string 
     */
    public function getVariables()
    {
        return $this->variables;
    }
    /**
     * Set tipoPregunta
     *
     * @param string $tipoPregunta
     * @return plan_filtros
     */
    public function setTipoPregunta($tipoPregunta)
    {
        $this->tipoPregunta = $tipoPregunta;

        return $this;
    }

    /**
     * Get tipoPregunta
     *
     * @return string 
     */
    public function getTipoPregunta()
    {
        return $this->tipoPregunta;
    }
    /**
     * Set preguntatitulo
     *
     * @param string $tipoPregunta
     * @return plan_filtros
     */
    public function setPreguntatitulo($preguntatitulo)
    {
        $this->preguntatitulo = $preguntatitulo;

        return $this;
    }

    /**
     * Get preguntatitulo
     *
     * @return string 
     */
    public function getPreguntatitulo()
    {
        return $this->preguntatitulo;
    }
    /**
     * Set oppregunta
     *
     * @param string $oppregunta
     * @return plan_filtros
     */
    public function setOppregunta($oppregunta)
    {
        $this->oppregunta = $oppregunta;

        return $this;
    }

    /**
     * Get oppregunta
     *
     * @return string 
     */
    public function getOppregunta()
    {
        return $this->oppregunta;
    }

    
}
