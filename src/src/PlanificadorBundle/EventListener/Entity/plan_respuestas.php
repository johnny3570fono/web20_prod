<?php

namespace PlanificadorBundle\Entity;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\Mapping as ORM;

/**
 * plan_respuestas
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="PlanificadorBundle\Entity\plan_respuestasRepository")
 * @Vich\Uploadable
 */
class plan_respuestas
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idrespuesta", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idrespuesta;

    /**
     * @ORM\ManyToOne(targetEntity="plan_pregunta", inversedBy="respuestas", cascade={"persist"})
     * @ORM\JoinColumn(name="idpregunta", referencedColumnName="id")
     * @return integer
     */
    private $pregunta;
        /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=500, nullable=true)
     */
    public $titulo;
    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=500, nullable=true)
     */
    private $descripcion;
    /**
     * @var integer
     *
     * @ORM\Column(name="visible", type="integer")
     */
    public $visible;

    /**
      * @var File
      *
      * @Vich\UploadableField(mapping="respuestas", fileNameProperty="imagenTipo",nullable=true)
      * @Assert\Image(
      *     maxSize = "1M",
      *     mimeTypes = {"image/jpeg", "image/png"},
      *     maxWidth=850,
      *     minHeight=160,
      * )
      */
    private $imagen;

    /**
     * @var string
     *
     * @ORM\Column(name="imagenTipo", type="string", length=255, nullable=true)
     */
    private $imagenTipo;
      /**
       * @ORM\Column(type="datetime", nullable=true)
       *
       * @var \DateTime
    */
    private $updatedAt;
    /**
     * @var string
     *
     * @ORM\Column(name="json_respuesta", type="json_array", nullable=true)
     */
    private $json_respuesta;
    /**
     * @var string
     *
     * @ORM\Column(name="valor", type="string", length=255, nullable=true)
     */
    private $valor;
    

    //variables Dummy
    /**
     * @var integer
     *
     */
    public $desde;
     /**
     * @var integer
     *
     */
    public $hasta;

    /**
     * Get idrespuesta
     *
     * @return integer 
     */
    public function getIdrespuesta()
    {
        return $this->idrespuesta;
    }

    /**
     * Set visible
     *
     * @param integer $visible
     * @return plan_respuestas
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return integer 
     */
    public function getVisible()
    {
        return $this->visible;
    }


    /**
     * Set valor
     *
     * @param string $valor
     * @return plan_respuestas
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return string 
     */
    public function getValor()
    {
        return $this->valor;
    }
    /**
     * Sets file.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $imagen
     *
     * @return plan_preguntaGenerica
     */
    public function setImagen(File $imagen = null)
    {

        $this->imagen = $imagen;
     
        if ($imagen) {

            $this->updatedAt = new \DateTime;
        }
    }
   

    /**
     * Get imagen
     *
     * @return  File|null
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set imagenTipo
     *
     * @param string $imagenTipo
     * @return plan_preguntaGenerica
     */
    public function setImagenTipo($imagenTipo)
    {
       
        $this->imagenTipo = $imagenTipo;

        return $this;
    }

    /**
     * Get imagenTipo
     *
     * @return string 
     */
    public function getImagenTipo()
    {
        return $this->imagenTipo;
    }

    
    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return plan_respuestas
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return plan_respuestas
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }


      /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return plan_respuestas
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set pregunta
     *
     * @param \PlanificadorBundle\Entity\plan_pregunta $pregunta
     * @return plan_respuestas
     */
    public function setPregunta(\PlanificadorBundle\Entity\plan_pregunta $pregunta = null)
    {
        $this->pregunta = $pregunta;

        return $this;
    }

    /**
     * Get pregunta
     *
     * @return \PlanificadorBundle\Entity\plan_pregunta 
     */
    public function getPregunta()
    {
        return $this->pregunta;
    }

    /**
     * Set desde
     *
     * @param integer $desde
     * @return plan_respuestas
     */
    public function setDesde($desde)
    {
        $this->desde = $desde;

        return $this;
    }

    /**
     * Get desde
     *
     * @return integer 
     */
    public function getDesde()
    {
        return $this->desde;
    }

 /**
     * Set hasta
     *
     * @param integer $hasta
     * @return plan_respuestas
     */
    public function setHasta($hasta)
    {
        $this->hasta = $hasta;

        return $this;
    }

    /**
     * Get hasta
     *
     * @return integer 
     */
    public function getHasta()
    {
        return $this->hasta;
    }
     /**
     * Set json_respuesta
     *
     * @param array $jsonRespuesta
     * @return plan_respuestas
     */
    public function setJsonRespuesta($jsonRespuesta)
    {
        $this->json_respuesta = $jsonRespuesta;

        return $this;
    }

    /**
     * Get json_respuesta
     *
     * @return array 
     */
    public function getJsonRespuesta()
    {
        return $this->json_respuesta;
    }
}
