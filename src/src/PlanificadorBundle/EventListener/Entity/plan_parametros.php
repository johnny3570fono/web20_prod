<?php

namespace PlanificadorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * plan_parametros
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class plan_parametros
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="max_sel", type="integer")
     */
    private $maxSel;

    /**
     * @var integer
     *
     * @ORM\Column(name="cant_max", type="integer")
     */
    private $cantMax;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="parent", type="integer", nullable=true)
     */
    private $parent;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set maxSel
     *
     * @param integer $maxSel
     * @return plan_parametros
     */
    public function setMaxSel($maxSel)
    {
        $this->maxSel = $maxSel;

        return $this;
    }

    /**
     * Get maxSel
     *
     * @return integer 
     */
    public function getMaxSel()
    {
        return $this->maxSel;
    }

    /**
     * Set cantMax
     *
     * @param integer $cantMax
     * @return plan_parametros
     */
    public function setCantMax($cantMax)
    {
        $this->cantMax = $cantMax;

        return $this;
    }

    /**
     * Get cantMax
     *
     * @return integer 
     */
    public function getCantMax()
    {
        return $this->cantMax;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return plan_parametros
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set parent
     *
     * @param integer $parent
     * @return plan_parametros
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return integer 
     */
    public function getParent()
    {
        return $this->parent;
    }
}
