<?php

namespace PlanificadorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


class front_momentos
{
   
    private $checked;

    
    public function setChecked($checked)
    {
        $this->checked = $checked;

        return $this;
    }

    /**
     * Get checked
     *
     * @return string 
     */
    public function getChecked()
    {
        return $this->checked;
    }

}
