<?php

namespace PlanificadorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * plan_preg_prod
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="PlanificadorBundle\Entity\plan_preg_prodRepository")
 */
class plan_preg_prod
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="idProdweb", type="integer")
     */
    private $idProdweb;

    /**
     * @var integer
     *
     * @ORM\Column(name="idPlanweb", type="integer")
     */
    private $idPlanweb;

    /**
     * @var integer
     *
     * @ORM\Column(name="idPregunta", type="integer")
     */
    private $idPregunta;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idProdweb
     *
     * @param integer $idProdweb
     * @return plan_preg_prod
     */
    public function setIdProdweb($idProdweb)
    {
        $this->idProdweb = $idProdweb;

        return $this;
    }

    /**
     * Get idProdweb
     *
     * @return integer 
     */
    public function getIdProdweb()
    {
        return $this->idProdweb;
    }

    /**
     * Set idPlanweb
     *
     * @param integer $idPlanweb
     * @return plan_preg_prod
     */
    public function setIdPlanweb($idPlanweb)
    {
        $this->idPlanweb = $idPlanweb;

        return $this;
    }

    /**
     * Get idPlanweb
     *
     * @return integer 
     */
    public function getIdPlanweb()
    {
        return $this->idPlanweb;
    }

    /**
     * Set idPregunta
     *
     * @param integer $idPregunta
     * @return plan_preg_prod
     */
    public function setIdPregunta($idPregunta)
    {
        $this->idPregunta = $idPregunta;

        return $this;
    }

    /**
     * Get idPregunta
     *
     * @return integer 
     */
    public function getIdPregunta()
    {
        return $this->idPregunta;
    }
}
