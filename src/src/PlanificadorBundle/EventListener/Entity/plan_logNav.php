<?php

namespace PlanificadorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * plan_logNav
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="PlanificadorBundle\Entity\plan_logNavRepository")
 */
class plan_logNav
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="idMomento", type="integer")
     */
    private $idMomento;

    /**
     * @var integer
     *
     * @ORM\Column(name="idPreocupacion", type="integer")
     */
    private $idPreocupacion;

    /**
     * @var integer
     *
     * @ORM\Column(name="idPregunta", type="integer")
     */
    private $idPregunta;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idMomento
     *
     * @param integer $idMomento
     * @return plan_logNav
     */
    public function setIdMomento($idMomento)
    {
        $this->idMomento = $idMomento;

        return $this;
    }

    /**
     * Get idMomento
     *
     * @return integer 
     */
    public function getIdMomento()
    {
        return $this->idMomento;
    }

    /**
     * Set idPreocupacion
     *
     * @param integer $idPreocupacion
     * @return plan_logNav
     */
    public function setIdPreocupacion($idPreocupacion)
    {
        $this->idPreocupacion = $idPreocupacion;

        return $this;
    }

    /**
     * Get idPreocupacion
     *
     * @return integer 
     */
    public function getIdPreocupacion()
    {
        return $this->idPreocupacion;
    }

    /**
     * Set idPregunta
     *
     * @param integer $idPregunta
     * @return plan_logNav
     */
    public function setIdPregunta($idPregunta)
    {
        $this->idPregunta = $idPregunta;

        return $this;
    }

    /**
     * Get idPregunta
     *
     * @return integer 
     */
    public function getIdPregunta()
    {
        return $this->idPregunta;
    }
}
