<?php

namespace PlanificadorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * plan_moment_preo
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="PlanificadorBundle\Entity\plan_moment_preoRepository")
 */
class plan_moment_preo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_momento", type="integer")
     */
    private $idMomento;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_preocupacion", type="integer")
     */
    private $idPreocupacion;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idMomento
     *
     * @param integer $idMomento
     * @return plan_moment_preo
     */
    public function setIdMomento($idMomento)
    {
        $this->idMomento = $idMomento;

        return $this;
    }

    /**
     * Get idMomento
     *
     * @return integer 
     */
    public function getIdMomento()
    {
        return $this->idMomento;
    }

    /**
     * Set idPreocupacion
     *
     * @param integer $idPreocupacion
     * @return plan_moment_preo
     */
    public function setIdPreocupacion($idPreocupacion)
    {
        $this->idPreocupacion = $idPreocupacion;

        return $this;
    }

    /**
     * Get idPreocupacion
     *
     * @return integer 
     */
    public function getIdPreocupacion()
    {
        return $this->idPreocupacion;
    }
}
