<?php

namespace PlanificadorBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Command\Conciliacion\ConciliacionWriter;
use AppBundle\Entity\Insurance;
use PlanificadorBundle\Entity\Plan_Seguimiento;
use Goutte\Client;

/**
 * Clase para extraer los valores de la tabla Seguimiento en Planificador
 */
class Plan_SeguimientoCommand extends ContainerAwareCommand {

    private $config;
    protected $defaultName;

    protected function configure() {
        parent::configure();
        $defaultName = $this->defaultName;

        $this->setName('seguimiento:generate')
                ->setDescription('Genera el seguimiento de los usuarios en Planificador')
                ->addArgument(
                        'fecha', InputArgument::OPTIONAL, 'Fecha de creación del log de seguimiento en Planificador (Y-m-d)'
                )->addArgument(
                'hora', InputArgument::OPTIONAL, 'Hora y minutos de creación de Planificador (H:i)'
        )->addArgument(
                'fecha_hasta', InputArgument::OPTIONAL, 'Fecha hasta de creación de los flujos de Planificador (Y-m-d)'
        )->addArgument(
                'hora_hasta', InputArgument::OPTIONAL, 'Hora y minutos hasta de creación de los flujos de Planificador  (H:i)'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->config = $this->getContainer()->getParameter('seguimiento');
        $tipoarchivo = $this->config['trazalogcss'];
        $path = $tipoarchivo['source_file'];

		$prefix = $tipoarchivo['prefix'];

        $fecha = $input->getArgument('fecha');
        $hora = $input->getArgument('hora'); //fecha inicial
        $fecha_hasta = $input->getArgument('fecha_hasta');
        $hora_hasta = $input->getArgument('hora_hasta'); //fecha inicial

        $fecha_archivo = new \DateTime('now');
        
        if ($fecha) {
            $fecha = $this->getFecha($input->getArgument('fecha'), $input->getArgument('hora'));
        }
        if ($fecha_hasta) {
            $fecha_hasta = $this->getFecha($input->getArgument('fecha_hasta'), $input->getArgument('hora_hasta'));
        }



        $types = array_diff(Insurance::getTypes(), ['none']);
        /* Buscar Datos en Tabla Seguimiento */
        $repository = $this->getContainer()->get('doctrine')->getManager()->getRepository('PlanificadorBundle:Plan_Seguimiento');
		//Funcion fechas
		$rows = $repository->seguimientoFechas($fecha, $fecha_hasta);
        			
        //Iniciamos 
        if (count($rows) < 1) {
            $output->writeln('No existen seguimientos para el día solicitado.');
        }else{
			 $filaname = sprintf('%s/%s%s.txt', $path, $prefix, $fecha_archivo->format('Ymd'));
             $filename = sprintf('%s%s.txt', $prefix, $fecha_archivo->format('Ymd'));
             $i = 1;
			 $final = [];
			
			 foreach ($rows as $row){
					//Variables 
					$id = $row->getId();
					$session = $row->getSessionid();
					$username = $row->getUsername(); 
					$useragent = $row->getUseragent();
					$info = $row->getInfo();
					$salida = [];
					$outinfo = "";
					$outdetalle = "";
					$outvalor_cosas="";
					$datos_generales = array();
					$array_detalle=array(
	                		'id' => $id,
	                		'session'=> $session,
							'username' => $username,
							'useragent' => $useragent,
					);
					if(is_array($info)){
						$array_final = array_merge($array_detalle,$info);
					}else{
						$array_final = array_merge($array_detalle);
					}
					json_encode($array_final);
					  
					$writer = new \AppBundle\Command\Conciliacion\ReportGenerator(fopen($filaname, 'w'));
					$line[$i] = json_encode($array_final) . "\r";
					$writer->writeItem($line);
					$i++;	
				}
				$writer->finish();
				$output->writeln('Generación de archivos en path: "' . $filaname . '" ');
			} //Fin Else
    }

    private function getFecha($day, $hour) {
        $tz = new \DateTimeZone('America/Santiago');

        if (strtolower($day) == 'now' || !$day) {
            $fecha = new \DateTime('now', $tz);
        } else {
            if (!$hour || !preg_match('/^[0-2][0-9]:[0-5][0-9]$/', $hour)) {
                $hour = '00:00:00';
            }

            $fecha = new \DateTime($day . ' ' . $hour, $tz);
        }

        return $fecha;
    }

}
