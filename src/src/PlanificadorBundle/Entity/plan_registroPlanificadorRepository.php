<?php

namespace PlanificadorBundle\Entity;

use Doctrine\ORM\EntityRepository;
use PlanificadorBundle\Entity\plan_registroPlanificador;

/**
 * plan_registroPlanificadorRepository 
 */
class plan_registroPlanificadorRepository extends EntityRepository {

    /**
     * @author [aibarra]
     * @uses   [PlanificadorBundle\Controller\InicioController.php]
     * @param  [integer] $etapa
     * @return flush
     */
    public function registrarDatos($etapa, $momentos, $preocupaciones) {

        $em = $this->getEntityManager();
        $planRegistroPlanificador = new plan_registroPlanificador();
        $planRegistroPlanificador->setEtapa($etapa);
        $planRegistroPlanificador->setMomentos($momentos);
        $planRegistroPlanificador->setPreocupaciones($preocupaciones);
        $planRegistroPlanificador->setFecha(new \DateTime(date('Y-m-d H:i:s')));
        $em->persist($planRegistroPlanificador);
        $em->flush();
    }

}
