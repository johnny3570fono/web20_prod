<?php

namespace PlanificadorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * plan_tipo
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="PlanificadorBundle\Entity\plan_tipoRepository")
 */
class plan_tipo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idtipo;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="template", type="string", length=255)
     */
    private $template;

    /**
     * @ORM\OneToMany(targetEntity="plan_preguntaGenerica", mappedBy="plan_tipo")
     */
    private $preguntaGenerica;

    /**
     * @ORM\OneToMany(targetEntity="plan_pregunta", mappedBy="plan_tipo")
     */
    private $pregunta;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getIdtipo()
    {
        return $this->idtipo;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return plan_tipo
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set template
     *
     * @param string $template
     * @return plan_tipo
     */
    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Get template
     *
     * @return string 
     */
    public function getTemplate()
    {
        return $this->template;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->preguntaGenerica = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add preguntaGenerica
     *
     * @param \PlanificadorBundle\Entity\plan_preguntaGenerica $preguntaGenerica
     * @return plan_tipo
     */
    public function addPreguntaGenerica(\PlanificadorBundle\Entity\plan_preguntaGenerica $preguntaGenerica)
    {
        $this->preguntaGenerica[] = $preguntaGenerica;

        return $this;
    }

    /**
     * Remove preguntaGenerica
     *
     * @param \PlanificadorBundle\Entity\plan_preguntaGenerica $preguntaGenerica
     */
    public function removePreguntaGenerica(\PlanificadorBundle\Entity\plan_preguntaGenerica $preguntaGenerica)
    {
        $this->preguntaGenerica->removeElement($preguntaGenerica);
    }

    /**
     * Get preguntaGenerica
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPreguntaGenerica()
    {
        return $this->preguntaGenerica;
    }

    /**
     * Add pregunta
     *
     * @param \PlanificadorBundle\Entity\plan_pregunta $pregunta
     * @return plan_tipo
     */
    public function addPreguntum(\PlanificadorBundle\Entity\plan_pregunta $pregunta)
    {
        $this->pregunta[] = $pregunta;

        return $this;
    }

    /**
     * Remove pregunta
     *
     * @param \PlanificadorBundle\Entity\plan_pregunta $pregunta
     */
    public function removePreguntum(\PlanificadorBundle\Entity\plan_pregunta $pregunta)
    {
        $this->pregunta->removeElement($pregunta);
    }

    /**
     * Get pregunta
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPregunta()
    {
        return $this->pregunta;
    }
}
