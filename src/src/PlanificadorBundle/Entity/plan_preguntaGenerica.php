<?php

namespace PlanificadorBundle\Entity;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * plan_preguntaGenerica
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="PlanificadorBundle\Entity\plan_preguntaGenericaRepository")
 * @Vich\Uploadable
 */
class plan_preguntaGenerica
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=500)
     */
    private $descripcion;

    /**
     * @ORM\ManyToOne(targetEntity="plan_tipo", inversedBy="plan_preguntaGenerica")
     * @ORM\JoinColumn(name="idtipo", referencedColumnName="id")
     * @return integer
     */
    private $tipo;


    /**
     * @var boolean
     *
     * @ORM\Column(name="visible", type="boolean")
     */
    private $visible;

    /**
     * @var integer
     *
     * @ORM\Column(name="idMomento", type="integer")
     */
    private $idMomento;
    /**
      * @var File
      *
      * @Vich\UploadableField(mapping="preguntas", fileNameProperty="imagenTipo",nullable=true)
      * @Assert\Image(
      *     maxSize = "1M",
      *     mimeTypes = {"image/jpeg", "image/png"},
      *     maxWidth=850,
      *     minHeight=160,
      * )
      */
    private $imagen;

    /**
     * @var string
     *
     * @ORM\Column(name="imagenTipo", type="string", length=255, nullable=true)
     */
    private $imagenTipo;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return plan_preguntaGenerica
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

   
    /**
     * Set visible
     *
     * @param boolean $visible
     * @return plan_preguntaGenerica
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean 
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set idMomento
     *
     * @param integer $idMomento
     * @return plan_preguntaGenerica
     */
    public function setIdMomento($idMomento)
    {
        $this->idMomento = $idMomento;

        return $this;
    }

    /**
     * Get idMomento
     *
     * @return integer 
     */
    public function getIdMomento()
    {
        return $this->idMomento;
    }
     /**
     * Sets file.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $imagen
     *
     * @return plan_preguntaGenerica
     */
    public function setImagen(File $imagen = null)
    {

        $this->imagen = $imagen;
        // die();
        if ($imagen) {

            $this->updatedAt = new \DateTime;
        }
    }
   

    /**
     * Get imagen
     *
     * @return  File|null
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set imagenTipo
     *
     * @param string $imagenTipo
     * @return plan_preguntaGenerica
     */
    public function setImagenTipo($imagenTipo)
    {
       
        $this->imagenTipo = $imagenTipo;

        return $this;
    }

    /**
     * Get imagenTipo
     *
     * @return string 
     */
    public function getImagenTipo()
    {
        return $this->imagenTipo;
    }

    /**
     * Set tipo
     *
     * @param \PlanificadorBundle\Entity\plan_tipo $tipo
     * @return plan_preguntaGenerica
     */
    public function setTipo(\PlanificadorBundle\Entity\plan_tipo $tipo = null)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return \PlanificadorBundle\Entity\plan_tipo 
     */
    public function getTipo()
    {
        return $this->tipo;
    }
}
