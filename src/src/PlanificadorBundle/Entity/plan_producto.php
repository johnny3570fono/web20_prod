<?php

namespace PlanificadorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Entity\Product;
use AppBundle\Entity\Segment;
/**
 * plan_producto
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="PlanificadorBundle\Entity\plan_productoRepository")
 * @Vich\Uploadable
 */
class plan_producto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=100, nullable=true)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=1000, nullable=true)
     */
    private $descripcion;

   /**
      * @var File
      *
      * @Vich\UploadableField(mapping="alcance", fileNameProperty="imagenTipo",nullable=true)
      * @Assert\Image(
      *     maxSize = "1M",
      *     mimeTypes = {"image/jpeg", "image/png"},
      *     maxWidth=850,
      *     minHeight=160,
      * )
      */
    private $imagen;

    /**
     * @var string
     *
     * @ORM\Column(name="imagenTipo", type="string", length=255, nullable=true)
     */
    private $imagenTipo;
    /**
     * @var string
     *
     * @ORM\Column(name="cubre", type="json_array",nullable=true)
     */
    private $cubre;

    /**
     * @var string
     *
     * @ORM\Column(name="nocubre", type="json_array", nullable=true)
     */
    private $nocubre;
     /**
     * @var string
     *
     * @ORM\Column(name="informacion", type="string", length=1000, nullable=true)
     */
    private $informacion;

    /**
     * @var integer
     *
     * @ORM\Column(name="estado", type="integer")
     */
    private $estado;

      /**
       * @ORM\Column(type="datetime", nullable=true)
       *
       * @var \DateTime
    */
    private $updatedAt;
    /**
     * @var integer
     *
     * @ORM\Column(name="segmento", type="integer")
     */
    private $segmento;

     /**
     * @var string
     *
     * @ORM\Column(name="cobertura", type="json_array", nullable=true)
     */
    private $cobertura;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Product")
     * @ORM\JoinColumn(name="idproductweb", referencedColumnName="id")
     */
    private $productoweb;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Segment", inversedBy="productosplani")
     * @ORM\JoinTable(name="segment_productplani")
     */
    protected $segmentosplani;

    /*****************/
    /***variable para guardar el json con todo el contenido*********/
    private $choicesjson;

  
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return plan_producto
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return plan_producto
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set cubre
     *
     * @param string $cubre
     * @return plan_producto
     */
    public function setCubre($cubre)
    {
        $this->cubre = $cubre;

        return $this;
    }

    /**
     * Get cubre
     *
     * @return string 
     */
    public function getCubre()
    {
        return $this->cubre;
    }

    /**
     * Set nocubre
     *
     * @param string $nocubre
     * @return plan_producto
     */
    public function setNocubre($nocubre)
    {
        $this->nocubre = $nocubre;

        return $this;
    }

    /**
     * Get nocubre
     *
     * @return string 
     */
    public function getNocubre()
    {
        return $this->nocubre;
    }

    /**
     * Set estado
     *
     * @param integer $estado
     * @return plan_producto
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return integer 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return plan_producto
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Sets file.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $imagen
     *
     * @return plan_producto
     */
    public function setImagen(File $imagen = null)
    {

        $this->imagen = $imagen;
        if ($imagen) {

            $this->updatedAt = new \DateTime;
        }
    }
    /**
     * Get imagen
     *
     * @return  File|null
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set imagenTipo
     *
     * @param string $imagenTipo
     * @return plan_producto
     */
    public function setImagenTipo($imagenTipo)
    {
        $this->imagenTipo = $imagenTipo;

        return $this;
    }
    /**
     * Get imagenTipo
     *
     * @return string 
     */
    public function getImagenTipo()
    {
        return $this->imagenTipo;
    }
    /**
     * Set informacion
     *
     * @param string $informacion
     * @return plan_producto
     */
    public function setInformacion($informacion)
    {
        $this->informacion = $informacion;

        return $this;
    }

    /**
     * Get cubre
     *
     * @return string 
     */
    public function getInformacion()
    {
        return $this->informacion;
    }
     /**
     * Set estado
     *
     * @param integer $segmento
     * @return plan_producto
     */
    public function setSegmento($segmento)
    {
        $this->segmento = $segmento;

        return $this;
    }

    /**
     * Get segmento
     *
     * @return integer 
     */
    public function getSegmento()
    {
        return $this->segmento;
    }
    /**
     * Getter
     *
     * @return string
     */
    public function getCobertura()
    {
        return $this->cobertura;
    }

    /**
     * Setter
     *
     * @param string $cobertura
     * @return plan_producto
     */
    public function setCobertura($cobertura)
    {
        $this->cobertura = $cobertura;

        return $this;
    }

    /**
     * Set productoweb
     *
     * @param \AppBundle\Entity\Product $productoweb
     * @return plan_producto
     */
    public function setProductoweb(\AppBundle\Entity\Product $productoweb = null)
    {
        $this->productoweb = $productoweb;

        return $this;
    }

    /**
     * Get productoweb
     *
     * @return \AppBundle\Entity\Product 
     */
    public function getProductoweb()
    {
        return $this->productoweb;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->segmentosplani = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add segmentosplani
     *
     * @param \AppBundle\Entity\Segment $segmentosplani
     * @return plan_producto
     */
    public function addSegmentosplani(\AppBundle\Entity\Segment $segmentosplani)
    {
        $this->segmentosplani[] = $segmentosplani;

        return $this;
    }

    /**
     * Remove segmentosplani
     *
     * @param \AppBundle\Entity\Segment $segmentosplani
     */
    public function removeSegmentosplani(\AppBundle\Entity\Segment $segmentosplani)
    {
        $this->segmentosplani->removeElement($segmentosplani);
    }

    /**
     * Get segmentosplani
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSegmentosplani()
    {
        return $this->segmentosplani;
    }
        /**
     * Set choicesjson
     *
     * @param array $choicesjson
     * @return plan_producto
     */
    public function setChoicesjson($choicesjson)
    {
        $this->choicesjson = $choicesjson;

        return $this;
    }

    /**
     * Get choicesjson
     *
     * @return array 
     */
    public function getChoicesjson()
    {
        return $this->choicesjson;
    }
}
