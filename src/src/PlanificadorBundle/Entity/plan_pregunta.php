<?php

namespace PlanificadorBundle\Entity;

use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

use Doctrine\ORM\Mapping as ORM;

/**
 * plan_pregunta
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="PlanificadorBundle\Entity\plan_preguntaRepository")
 * @Vich\Uploadable
 */
class plan_pregunta
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
      /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255)
     */
    public $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=500, nullable=true)
     */
    private $descripcion;

    /**
     * @ORM\ManyToOne(targetEntity="plan_tipo", inversedBy="plan_pregunta")
     * @ORM\JoinColumn(name="idTipo", referencedColumnName="id")
     * @return integer
     */
    private $tipo;

    /**
     * @ORM\ManyToOne(targetEntity="plan_tipopreg", inversedBy="tipopreg")
     * @ORM\JoinColumn(name="pregtipo", referencedColumnName="id")
     */
    private $pregtipo;
  
  

    /**
     * @var integer
     *
     * @ORM\Column(name="visible", type="integer")
     */
    private $visible;

    /**
     * @var integer
     *
     * @ORM\Column(name="edad_min", type="integer", nullable=true)
     */
    private $edadMin;

    /**
     * @var integer
     *
     * @ORM\Column(name="edad_max", type="integer", nullable=true)
     */
    private $edadMax;

    /**
     * @var string
     *
     * @ORM\Column(name="segment", type="string", length=500, nullable=true)
     */
    private $segment;
    /**
      * @var File
      *
      * @Vich\UploadableField(mapping="preguntas", fileNameProperty="imagenTipo",nullable=true)
      * @Assert\Image(
      *     maxSize = "1M",
      *     mimeTypes = {"image/jpeg", "image/png"},
      *     maxWidth=850,
      *     minHeight=160,
      * )
      */
    private $imagen;

    /**
     * @var string
     *
     * @ORM\Column(name="imagenTipo", type="string", length=255, nullable=true)
     */
    private $imagenTipo;

     /**
     * @ORM\OneToMany(targetEntity="plan_respuestas", mappedBy="pregunta", cascade={"persist","remove"})
     */
    private $respuestas;
   //**VARIABLE DUMMY**//
    private $pregRel;
    
    /**
     * @var string
     *
     * @ORM\Column(name="json_respuesta", type="json_array", nullable=true)
     */
    private $json_respuesta;

    /**
     * @ORM\OneToMany(targetEntity="plan_mom_preo", mappedBy="plan_pregunta" )
     */
    protected $momPreo;


    /**
     * @ORM\OneToMany(targetEntity="plan_arbol", mappedBy="pregHijo")
     */
    protected $hijo;
    /**
     * @ORM\OneToMany(targetEntity="plan_arbol", mappedBy="pregPadre")
     */
    protected $padre;
     /**
     * @var integer
     *
     * @ORM\Column(name="orderby", type="integer", length=10, nullable=true )
     */
    private $orderBy=null;
     /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=1, nullable=true)
     */
    private $status;


 
   
    
    /**
     * Constructor
     */
    public function __construct()
    {
       
        $this->respuestas = new ArrayCollection();
        $this->momPreo = new ArrayCollection();
        $this->hijo = new \Doctrine\Common\Collections\ArrayCollection();
        $this->padre = new \Doctrine\Common\Collections\ArrayCollection();
    }
  

     /**
     * Set pregRel
     *
     * @param string $pregRel
     * @return plan_pregunta
     */
    public function setPregRel($pregRel)
    {
        $this->pregRel = $pregRel;

        return $this;
    }

    /**
     * Get pregRel
     *
     * @return string 
     */
    public function getPregRel()
    {
        return $this->pregRel;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return plan_pregunta
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set idTipo
     *
     * @param integer $idTipo
     * @return plan_pregunta
     */
    public function setIdTipo($idTipo)
    {
        $this->idTipo = $idTipo;

        return $this;
    }

    /**
     * Get idTipo
     *
     * @return integer 
     */
    public function getIdTipo()
    {
        return $this->idTipo;
    }

    

    /**
     * Set visible
     *
     * @param integer $visible
     * @return plan_pregunta
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return integer 
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set edadMin
     *
     * @param integer $edadMin
     * @return plan_pregunta
     */
    public function setEdadMin($edadMin)
    {
        $this->edadMin = $edadMin;

        return $this;
    }

    /**
     * Get edadMin
     *
     * @return integer 
     */
    public function getEdadMin()
    {
        return $this->edadMin;
    }

    /**
     * Set edadMax
     *
     * @param integer $edadMax
     * @return plan_pregunta
     */
    public function setEdadMax($edadMax)
    {
        $this->edadMax = $edadMax;

        return $this;
    }

    /**
     * Get edadMax
     *
     * @return integer 
     */
    public function getEdadMax()
    {
        return $this->edadMax;
    }

    /**
     * Set segment
     *
     * @param string $segment
     * @return plan_pregunta
     */
    public function setSegment($segment)
    {
        $this->segment = $segment;

        return $this;
    }

    /**
     * Get segment
     *
     * @return string 
     */
    public function getSegment()
    {
        return $this->segment;
    }
    /**
     * Sets file.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $imagen
     *
     * @return plan_preguntaGenerica
     */
    public function setImagen(File $imagen = null)
    {

        $this->imagen = $imagen;
        // die();
        if ($imagen) {

            $this->updatedAt = new \DateTime;
        }
    }
   

    /**
     * Get imagen
     *
     * @return  File|null
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set imagenTipo
     *
     * @param string $imagenTipo
     * @return plan_preguntaGenerica
     */
    public function setImagenTipo($imagenTipo)
    {
       
        $this->imagenTipo = $imagenTipo;

        return $this;
    }

    /**
     * Get imagenTipo
     *
     * @return string 
     */
    public function getImagenTipo()
    {
        return $this->imagenTipo;
    }
     


    /**
     * Set tipo
     *
     * @param \PlanificadorBundle\Entity\plan_tipo $tipo
     * @return plan_pregunta
     */
    public function setTipo(\PlanificadorBundle\Entity\plan_tipo $tipo = null)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return \PlanificadorBundle\Entity\plan_tipo 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    
  public function setRespuestas(\Doctrine\Common\Collections\Collection $respuestas)
    {
        $this->respuestas = $respuestas;
        foreach ($respuestas as $respuestas) {
            $respuestas->setPregunta($this);
        }
    }
 
    /**
     * Get respuestas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRespuestas()
    {
        return $this->respuestas;
    }
   
    /**
     * Set titulo
     *
     * @param string $titulo
     * @return plan_pregunta
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set json_respuesta
     *
     * @param array $jsonRespuesta
     * @return plan_pregunta
     */
    public function setJsonRespuesta($jsonRespuesta)
    {
        $this->json_respuesta = $jsonRespuesta;

        return $this;
    }

    /**
     * Get json_respuesta
     *
     * @return array 
     */
    public function getJsonRespuesta()
    {
        return $this->json_respuesta;
    }

     /**
     * Add respuestas
     *
     * @param \PlanificadorBundle\Entity\plan_respuestas $respuestas
     * @return plan_pregunta
     */
    public function addRespuestas(\PlanificadorBundle\Entity\plan_respuestas $respuestas)
    {
        $this->respuestas[] = $respuestas;

        return $this;
    }
    

    /**
     * Remove respuestas
     *
     * @param \PlanificadorBundle\Entity\plan_respuestas $respuestas
     */
    public function removeRespuestas(\PlanificadorBundle\Entity\plan_respuestas $respuestas)
    {
        $this->respuestas->removeElement($respuestas);
    }


    /**
     * Add momPreo
     *
     * @param \PlanificadorBundle\Entity\plan_mom_preo $momPreo
     * @return plan_pregunta
     */
    public function addMomPreo(\PlanificadorBundle\Entity\plan_mom_preo $momPreo)
    {
        $this->momPreo[] = $momPreo;

        return $this;
    }

    /**
     * Remove momPreo
     *
     * @param \PlanificadorBundle\Entity\plan_mom_preo $momPreo
     */
    public function removeMomPreo(\PlanificadorBundle\Entity\plan_mom_preo $momPreo)
    {
        $this->momPreo->removeElement($momPreo);
    }

    /**
     * Get momPreo
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMomPreo()
    {
        return $this->momPreo;
    }

    /**
     * Add hijo
     *
     * @param \PlanificadorBundle\Entity\plan_arbol $hijo
     * @return plan_pregunta
     */
    public function addHijo(\PlanificadorBundle\Entity\plan_arbol $hijo)
    {
        $this->hijo[] = $hijo;

        return $this;
    }

    /**
     * Remove hijo
     *
     * @param \PlanificadorBundle\Entity\plan_arbol $hijo
     */
    public function removeHijo(\PlanificadorBundle\Entity\plan_arbol $hijo)
    {
        $this->hijo->removeElement($hijo);
    }

    /**
     * Get hijo
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHijo()
    {
        return $this->hijo;
    }

    /**
     * Add padre
     *
     * @param \PlanificadorBundle\Entity\plan_arbol $padre
     * @return plan_pregunta
     */
    public function addPadre(\PlanificadorBundle\Entity\plan_arbol $padre)
    {
        $this->padre[] = $padre;

        return $this;
    }

    /**
     * Remove padre
     *
     * @param \PlanificadorBundle\Entity\plan_arbol $padre
     */
    public function removePadre(\PlanificadorBundle\Entity\plan_arbol $padre)
    {
        $this->padre->removeElement($padre);
    }

    /**
     * Get padre
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPadre()
    {
        return $this->padre;
    }
    /**
     * Set pregtipo
     *
     * @param string $pregtipo
     * @return plan_pregunta
     */
    public function setPregtipo($pregtipo)
    {
        $this->pregtipo = $pregtipo;

        return $this;
    }

    /**
     * Get pregtipo
     *
     * @return string 
     */
    public function getPregtipo()
    {
        return $this->pregtipo;
    }
     /**
     * Set orderBy
     *
     * @param integer $orderBy
     * @return plan_pregunta
     */
    public function setOrderBy($orderBy)
    {

        $this->orderBy = $orderBy;

        return $this;
    }

    /**
     * Get orderBy
     *
     * @return integer 
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }
     /**
     * Set status
     *
     * @param string $status
     * @return plan_pregunta
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }
}
