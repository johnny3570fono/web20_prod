<?php

namespace PlanificadorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * plan_historial
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="PlanificadorBundle\Entity\plan_historialRepository")
 */
class plan_historial
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="rut", type="string", length=20)
     */
    private $rut;

    /**
     * @var string
     *
     * @ORM\Column(name="momentos", type="blob")
     */
    private $momentos;

    /**
     * @var string
     *
     * @ORM\Column(name="preocupaciones", type="blob")
     */
    private $preocupaciones;

    /**
     * @var string
     *
     * @ORM\Column(name="respuestas", type="blob")
     */
    private $respuestas;

    /**
     * @var string
     *
     * @ORM\Column(name="seg_ofertar", type="blob")
     */
    private $seg_ofertar;

    /**
       * @ORM\Column(type="datetime", nullable=true)
       *
       * @var \DateTime
    */
      private $fecha;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return plan_historial
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set rut
     *
     * @param string $rut
     * @return plan_historial
     */
    public function setRut($rut)
    {
        $this->rut = $rut;

        return $this;
    }

    /**
     * Get rut
     *
     * @return string 
     */
    public function getRut()
    {
        return $this->rut;
    }

    /**
     * Set momentos
     *
     * @param string $momentos
     * @return plan_historial
     */
    public function setMomentos($momentos)
    {
        $this->momentos = $momentos;

        return $this;
    }

    /**
     * Get momentos
     *
     * @return string 
     */
    public function getMomentos()
    {
        return $this->momentos;
    }

    /**
     * Set preocupaciones
     *
     * @param string $preocupaciones
     * @return plan_historial
     */
    public function setPreocupaciones($preocupaciones)
    {
        $this->preocupaciones = $preocupaciones;

        return $this;
    }

    /**
     * Get preocupaciones
     *
     * @return string 
     */
    public function getPreocupaciones()
    {
        return $this->preocupaciones;
    }

    /**
     * Set respuestas
     *
     * @param string $respuestas
     * @return plan_historial
     */
    public function setRespuestas($respuestas)
    {
        $this->respuestas = $respuestas;

        return $this;
    }

    /**
     * Get respuestas
     *
     * @return string 
     */
    public function getRespuestas()
    {
        return $this->respuestas;
    }

     /**
     * Set seg_ofertar
     *
     * @param string $seg_ofertar
     * @return plan_historial
     */
    public function setSeg_ofertar($seg_ofertar)
    {
        $this->seg_ofertar = $seg_ofertar;

        return $this;
    }

    /**
     * Get seg_ofertar
     *
     * @return string 
     */
    public function getSeg_ofertar()
    {
        return $this->seg_ofertar;
    }
    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return plan_momento
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }
}
