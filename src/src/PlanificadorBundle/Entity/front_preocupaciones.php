<?php

namespace PlanificadorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


class front_preocupaciones
{
   
    private $checked;
    /**
     * @var string
     *
     * @Assert\NotNull()
     * @Assert\Email()
     */
    protected $addressEmail;
 
    
    public function setChecked($checked)
    {
        $this->checked = $checked;

        return $this;
    }

    /**
     * Get checked
     *
     * @return string 
     */
    public function getChecked()
    {
        return $this->checked;
    }
    
    /**
     * Set addressEmail
     *
     * @param string $addressEmail
     */
    public function setAddressEmail($addressEmail)
    {
        $this->addressEmail = $addressEmail;

        return $this;
    }

    /**
     * Get addressEmail
     *
     * @return string
     */
    public function getAddressEmail()
    {
        return $this->addressEmail;
    }
    
}
