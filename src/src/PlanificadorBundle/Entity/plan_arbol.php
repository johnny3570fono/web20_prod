<?php

namespace PlanificadorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * plan_arbol
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="PlanificadorBundle\Entity\plan_arbolRepository")
 */
class plan_arbol
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="plan_tipopreg", inversedBy="arbol")
     * @ORM\JoinColumn(name="tipo", referencedColumnName="id")
     */
    private $tipo;

    /**
     * @var string
     *
     * @ORM\Column(name="valor", type="string", length=255 ,nullable=true)
     */
    private $valor;

    /**
     * @ORM\ManyToOne(targetEntity="plan_preocupacion", inversedBy="arbol")
     * @ORM\JoinColumn(name="idPreocupacion", referencedColumnName="id")
     */
    private $idPreocupacion;
    /**
     * @ORM\OneToMany(targetEntity="plan_arbol", mappedBy="padre")
     */
    public $child;
 
    /**
     * @ORM\ManyToOne(targetEntity="plan_arbol", inversedBy="child")
     * @ORM\JoinColumn(name="padre_id", referencedColumnName="id")
     */
    private $padre;
    private $pregPadre;
    /**
     * @ORM\ManyToOne(targetEntity="plan_pregunta", inversedBy="hijo")
     * @ORM\JoinColumn(name="pregHijo", referencedColumnName="id")
     */
    private $pregHijo;

    /**
     * @var string
     *
     * @ORM\Column(name="producto", type="json_array", nullable=true)
     */
    private $producto;
     /**
     * @var string
     *
     * @ORM\Column(name="plan", type="json_array", nullable=true)
     */
    private $plan;
     /**
     * @var string
     *
     * @ORM\Column(name="arreglo", type="json_array", nullable=true)
     */
    private $arreglo;

    private $choices;

    private $filtrogeneral;

    private $gastos;

  

     /*****************/
    /***variable para guardar el json con todo el contenido*********/
    private $choicesjson;


    /**
     * @ORM\OneToMany(targetEntity="plan_filtros", mappedBy="arbol")
     */
    protected $filtros;

    private $choicesjsongastos;
    private $choicesjsonfamilia;
    private $choicesjsonrespuesta;
    private $choicesjsonCapital;
    private $json_total;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

     /**
     * Set gastos
     *
     * @param string $gastos
     * @return plan_filtros
     */
    public function setGastos($gastos)
    {
        $this->gastos = $gastos;

        return $this;
    }

    /**
     * Get gastos
     *
     * @return string 
     */
    public function getGastos()
    {
        return $this->gastos;
    }

    /**
     * Set tipo
     *
     * @param integer $tipo
     * @return plan_arbol
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return integer 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set valor
     *
     * @param string $valor
     * @return plan_arbol
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return string 
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set idPreocupacion
     *
     * @param \PlanificadorBundle\Entity\plan_preocupacion $idPreocupacion
     * @return plan_arbol
     */
    public function setIdPreocupacion(\PlanificadorBundle\Entity\plan_preocupacion $idPreocupacion = null)
    {
        $this->idPreocupacion = $idPreocupacion;

        return $this;
    }

    /**
     * Get idPreocupacion
     *
     * @return \PlanificadorBundle\Entity\plan_preocupacion 
     */
    public function getIdPreocupacion()
    {
        return $this->idPreocupacion;
    }

    
    /**
     * Set pregHijo
     *
     * @param \PlanificadorBundle\Entity\plan_pregunta $pregHijo
     * @return plan_arbol
     */
    public function setPregHijo(\PlanificadorBundle\Entity\plan_pregunta $pregHijo = null)
    {
        $this->pregHijo = $pregHijo;

        return $this;
    }

    /**
     * Get pregHijo
     *
     * @return \PlanificadorBundle\Entity\plan_pregunta 
     */
    public function getPregHijo()
    {
        return $this->pregHijo;
    }

    /**
     * Set producto
     *
     * @param array $producto
     * @return plan_arbol
     */
    public function setProducto($producto)
    {
        $this->producto = $producto;

        return $this;
    }

    /**
     * Get producto
     *
     * @return array 
     */
    public function getProducto()
    {
        return $this->producto;
    }

    /**
     * Set plan
     *
     * @param array $plan
     * @return plan_arbol
     */
    public function setPlan($plan)
    {
        $this->plan = $plan;

        return $this;
    }

    /**
     * Get plan
     *
     * @return array 
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * Set arreglo
     *
     * @param array $arreglo
     * @return plan_arbol
     */
    public function setArreglo($arreglo)
    {
        $this->arreglo = $arreglo;

        return $this;
    }

    /**
     * Get arreglo
     *
     * @return array 
     */
    public function getArreglo()
    {
        return $this->arreglo;
    }
     /**
     * Set choices
     *
     * @param string $choices
     * @return plan_arbol
     */
    public function setChoices($choices)
    {
        $this->choices = $choices;

        return $this;
    }

    /**
     * Get choices
     *
     * @return string 
     */
    public function getChoices()
    {
        return $this->choices;
    }

    /**
     * Get filtrogeneral
     *
     * @return array 
     */
    public function getFiltrogeneral()
    {
        return $this->filtrogeneral;
    }

    /**
     * Set filtrogeneral
     *
     * @param array $simbolo
     * @return plan_filtros
     */
    public function setFiltrogeneral($filtrogeneral)
    {
        $this->filtrogeneral = $filtrogeneral;

        return $this;
    }
    
     /**
     * Set choicesjson
     *
     * @param array $choicesjson
     * @return plan_filtros
     */
    public function setChoicesjson($choicesjson)
    {
        $this->choicesjson = $choicesjson;

        return $this;
    }

    /**
     * Get choicesjson
     *
     * @return array 
     */
    public function getChoicesjson()
    {
        return $this->choicesjson;
    }

  
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->filtros = new ArrayCollection();
        $this->child = new ArrayCollection();

    }

    /**
     * Add filtros
     *
     * @param \PlanificadorBundle\Entity\plan_filtros $filtros
     * @return plan_filtros
     */
    public function addFiltros(\PlanificadorBundle\Entity\plan_filtros $filtros)
    {
        $this->filtros[] = $filtros;

        return $this;
    }

    /**
     * Remove filtros
     *
     * @param \PlanificadorBundle\Entity\plan_filtros $filtros
     */
    public function removeFiltros(\PlanificadorBundle\Entity\plan_filtros $filtros)
    {
        $this->filtros->removeElement($filtros);
    }

    /**
     * Get filtros
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFiltros()
    {
        return $this->filtros;
    }

    /**
     * Set choicesjsongastos
     *
     * @param array $choicesjsongastos
     * @return plan_filtros
     */
    public function setChoicesjsongastos($choicesjsongastos)
    {
        $this->choicesjsongastos = $choicesjsongastos;

        return $this;
    }

    /**
     * Get choicesjsongastos
     *
     * @return array 
     */
    public function getChoicesjsongastos()
    {
        return $this->choicesjsongastos;
    }



    /**
     * Set choicesjsonfamilia
     *
     * @param array $choicesjsonfamilia
     * @return plan_filtros
     */
    public function setChoicesjsonfamilia($choicesjsonfamilia)
    {
        $this->choicesjsonfamilia = $choicesjsonfamilia;

        return $this;
    }

    /**
     * Get choicesjsonfamilia
     *
     * @return array 
     */
    public function getChoicesjsonfamilia()
    {
        return $this->choicesjsonfamilia;
    }


    /**
     * Set choicesjsonrespuesta
     *
     * @param array $choicesjsonrespuesta
     * @return plan_filtros
     */
    public function setChoicesjsonrespuesta($choicesjsonrespuesta)
    {
        $this->choicesjsonrespuesta = $choicesjsonrespuesta;

        return $this;
    }

    /**
     * Get choicesjsonrespuesta
     *
     * @return array 
     */
    public function getChoicesjsonrespuesta()
    {
        return $this->choicesjsonrespuesta;
    }
    


    /**
     * Add filtros
     *
     * @param \PlanificadorBundle\Entity\plan_filtros $filtros
     * @return plan_arbol
     */
    public function addFiltro(\PlanificadorBundle\Entity\plan_filtros $filtros)
    {
        $this->filtros[] = $filtros;

        return $this;
    }

    /**
     * Remove filtros
     *
     * @param \PlanificadorBundle\Entity\plan_filtros $filtros
     */
    public function removeFiltro(\PlanificadorBundle\Entity\plan_filtros $filtros)
    {
        $this->filtros->removeElement($filtros);
    }

    /**
     * Add child
     *
     * @param \PlanificadorBundle\Entity\plan_arbol $child
     * @return plan_arbol
     */
    public function addChild(\PlanificadorBundle\Entity\plan_arbol $child)
    {
        $this->child[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \PlanificadorBundle\Entity\plan_arbol $child
     */
    public function removeChild(\PlanificadorBundle\Entity\plan_arbol $child)
    {
        $this->child->removeElement($child);
    }

    /**
     * Get child
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getChild()
    {
        return $this->child;
    }

    /**
     * Set padre
     *
     * @param \PlanificadorBundle\Entity\plan_arbol $padre
     * @return plan_arbol
     */
    public function setPadre(\PlanificadorBundle\Entity\plan_arbol $padre = null)
    {
        $this->padre = $padre;

        return $this;
    }

    /**
     * Get padre
     *
     * @return \PlanificadorBundle\Entity\plan_arbol 
     */
    public function getPadre()
    {
        return $this->padre;
    }

    
    /**
     * Set pregPadre
     *
     * @param array $pregPadre
     * @return plan_arbol
     */
    public function setPregPadre($pregPadre)
    {
        $this->pregPadre = $pregPadre;

        return $this;
    }

    /**
     * Get pregPadre
     *
     * @return array 
     */
    public function getPregPadre()
    {
        return $this->pregPadre;
    }
    /**
     * Set choicesjsonCapital
     *
     * @param array $choicesjsonCapital
     * @return plan_filtros
     */
    public function setChoicesjsonCapital($choicesjsonCapital)
    {
        $this->choicesjsonCapital = $choicesjsonCapital;

        return $this;
    }

    /**
     * Get choicesjsonCapital
     *
     * @return array 
     */
    public function getChoicesjsonCapital()
    {
        return $this->choicesjsonCapital;
    }

    /**
     * Get json_total
     *
     * @return array 
     */
    public function getJsonTotal()
    {
        return $this->json_total;
    }

    /**
     * Set json_total
     *
     * @return json_total
     */
    public function setJsonTotal($json_total)
    {
        $this->json_total = $json_total;

        return $this;
    }
}
