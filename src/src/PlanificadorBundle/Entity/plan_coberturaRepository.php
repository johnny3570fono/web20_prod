<?php

namespace PlanificadorBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * plan_coberturaRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class plan_coberturaRepository extends EntityRepository
{

	 public function getCoberturas()
    {
        $dql = $this->createQueryBuilder('a');

        $result = $dql->select('a.id,a.nombre,a.descripcion')
            ->orderBy('a.id')
            ->getQuery()
            ->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);
 
        $data = [];
        /*foreach ($result as $r) {
            $data[$r['nombre']] = $r['nombre'];
            $data[$r['descripcion']] = $r['descripcion'];
            $data[$r['id']] = $r['id'];
        }*/
        foreach ($result as $key => $r){
        $data[$key] = [
          'nombre' => $r['nombre'],
          'id' => $r['id'],
          'descripcion' => strip_tags($r['descripcion']),
        ];
      }      
        return $data;
    }
}
