<?php

namespace PlanificadorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * plan_logResp
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="PlanificadorBundle\Entity\plan_logRespRepository")
 */
class plan_logResp
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="rut", type="string", length=30)
     */
    private $rut;

    /**
     * @var integer
     *
     * @ORM\Column(name="idPregunta", type="integer")
     */
    private $idPregunta;

    /**
     * @var integer
     *
     * @ORM\Column(name="respCliente", type="integer")
     */
    private $respCliente;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rut
     *
     * @param string $rut
     * @return plan_logResp
     */
    public function setRut($rut)
    {
        $this->rut = $rut;

        return $this;
    }

    /**
     * Get rut
     *
     * @return string 
     */
    public function getRut()
    {
        return $this->rut;
    }

    /**
     * Set idPregunta
     *
     * @param integer $idPregunta
     * @return plan_logResp
     */
    public function setIdPregunta($idPregunta)
    {
        $this->idPregunta = $idPregunta;

        return $this;
    }

    /**
     * Get idPregunta
     *
     * @return integer 
     */
    public function getIdPregunta()
    {
        return $this->idPregunta;
    }

    /**
     * Set respCliente
     *
     * @param integer $respCliente
     * @return plan_logResp
     */
    public function setRespCliente($respCliente)
    {
        $this->respCliente = $respCliente;

        return $this;
    }

    /**
     * Get respCliente
     *
     * @return integer 
     */
    public function getRespCliente()
    {
        return $this->respCliente;
    }
}
