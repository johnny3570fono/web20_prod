<?php

namespace PlanificadorBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * plan_tipoRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class plan_tipoRepository extends EntityRepository
{

	public function groupby()
    {
            $em = $this->getEntityManager();
            $dql ="SELECT p FROM PlanificadorBundle:Pregunta p
            GROUP BY p.visible";
       
        $query = $em->createQuery($dql);
        return $query;

    }
}
