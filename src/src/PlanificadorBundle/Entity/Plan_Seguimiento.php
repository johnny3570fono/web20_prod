<?php

namespace PlanificadorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Plan_Seguimiento
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="PlanificadorBundle\Entity\Plan_SeguimientoRepository")
 */
class Plan_Seguimiento
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255)
     */
	private $username;
	
	/**
     * @var text
     *
     * @ORM\Column(name="sessionid", type="string", length=255)
     */
	private $sessionid;
	
	/**
     * @var text
     *
     * @ORM\Column(name="useragent", type="text")
     */
	private $useragent;
	
	
	/**
     * @var object
     * @ORM\Column(name="info", type="object")
     */
	private $info;
	
	/**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(name="updated", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updated;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
	
	
    /**
     * Set sessionid
     *
     * @param string $sessionid
     * @return Plan_Seguimiento
     */
    public function setSessionid($sessionid)
    {
        $this->sessionid = $sessionid;

        return $this;
    }

    /**
     * Get sessionid
     *
     * @return string 
     */
    public function getSessionid()
    {
        return $this->sessionid;
    }

    /**
     * Set useragent
     *
     * @param string $useragent
     * @return Plan_Seguimiento
     */
    public function setUseragent($useragent)
    {
        $this->useragent = $useragent;

        return $this;
    }

    /**
     * Get useragent
     *
     * @return string 
     */
    public function getUseragent()
    {
        return $this->useragent;
    }

    /**
     * Set info
     *
     * @param \stdClass $info
     * @return Plan_Seguimiento
     */
    public function setInfo($info)
    {
        $this->info = $info;

        return $this;
    }

    /**
     * Get info
     *
     * @return \stdClass 
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Plan_Seguimiento
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Plan_Seguimiento
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return Plan_Seguimiento
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }
}
