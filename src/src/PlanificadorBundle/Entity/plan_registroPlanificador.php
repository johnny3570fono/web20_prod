<?php

namespace PlanificadorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * plan_registroPlanificador
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="PlanificadorBundle\Entity\plan_registroPlanificadorRepository") 
 */
class plan_registroPlanificador {

    const ETAPA_INICIO = 1;
    const ETAPA_MOMENTOS = 2;
    const ETAPA_PREOCUPACIONES = 3;
    const ETAPA_FAMILIA = 4;
    const ETAPA_GASTOS = 5;
    const ETAPA_SEGUROS = 6;
    const ETAPA_FINAL = 7;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer",  name="etapa")
     * @var integer
     * @Assert\NotBlank(message = "validation.not_blank")     
     */
    private $etapa;

    /**
     * @ORM\Column(type="integer",  name="momentos", nullable=true)
     * @var integer     
     */
    private $momentos;

    /**
     * @ORM\Column(type="integer",  name="preocupaciones", nullable=true)
     * @var integer     
     */
    private $preocupaciones;

    /**
     * @ORM\Column(type="integer",  name="estado_civil", nullable=true)
     * @var integer     
     */
    private $estadoCivil;

    /**
     * @ORM\Column(type="integer",  name="cantidad_hijos", nullable=true)
     * @var integer     
     */
    private $cantidadHijos;

    /**
     * @ORM\Column(type="integer",  name="edad_hijos", nullable=true)
     * @var integer     
     */
    private $edadHijos;

    /**
     * @ORM\Column(type="integer",  name="gastos", nullable=true)
     * @var integer     
     */
    private $gastos;

    /**
     * @ORM\Column(type="integer",  name="seguros", nullable=true)
     * @var integer     
     */
    private $seguros;

    /**
     * @var \Datetime
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set etapa
     *
     * @param integer $etapa
     * @return Etapa
     */
    public function setEtapa($etapa) {
        $this->etapa = $etapa;

        return $this;
    }

    /**
     * Get etapa
     *
     * @return integer
     */
    public function getEtapa() {
        return $this->etapa;
    }

    /**
     * Set momentos
     *
     * @param integer $momentos
     * @return Momentos
     */
    public function setMomentos($momentos) {
        $this->momentos = $momentos;

        return $this;
    }

    /**
     * Get momentos
     *
     * @return integer
     */
    public function getMomentos() {
        return $this->momentos;
    }

    /**
     * Set preocupaciones
     *
     * @param integer $preocupaciones
     * @return Preocupaciones
     */
    public function setPreocupaciones($preocupaciones) {
        $this->preocupaciones = $preocupaciones;

        return $this;
    }

    /**
     * Get Preocupaciones
     *
     * @return integer
     */
    public function getPreocupaciones() {
        return $this->preocupaciones;
    }

    /**
     * Set Estado Civil
     *
     * @param integer $estadoCivil
     * @return EstadoCivil
     */
    public function setEstadoCivil($estadoCivil) {
        $this->estadoCivil = $estadoCivil;

        return $this;
    }

    /**
     * Get Estado Civil
     *
     * @return integer
     */
    public function getEstadoCivil() {
        return $this->estadoCivil;
    }

    /**
     * Set Cantidad Hijos
     *
     * @param integer $cantidadHijos
     * @return CantidadHijos
     */
    public function setCantidadHijos($cantidadHijos) {
        $this->cantidadHijos = $cantidadHijos;

        return $this;
    }

    /**
     * Get Cantidad Hijos
     *
     * @return integer
     */
    public function getCantidadHijos() {
        return $this->cantidadHijos;
    }

    /**
     * Set Edad Hijos
     *
     * @param integer $edadHijos
     * @return EdadHijos
     */
    public function setEdadHijos($edadHijos) {
        $this->edadHijos = $edadHijos;

        return $this;
    }

    /**
     * Get Edad Hijos
     *
     * @return integer
     */
    public function getEdadHijos() {
        return $this->edadHijos;
    }

    /**
     * Set Gastos
     *
     * @param integer $gastos
     * @return Gastos
     */
    public function setGastos($gastos) {
        $this->gastos = $gastos;

        return $this;
    }

    /**
     * Get Gastos
     *
     * @return integer
     */
    public function getGastos() {
        return $this->gastos;
    }

    /**
     * Set Seguros
     *
     * @param integer $seguros
     * @return Seguros
     */
    public function setSeguros($seguros) {
        $this->seguros = $seguros;

        return $this;
    }

    /**
     * Get Seguros
     *
     * @return integer
     */
    public function getSeguros() {
        return $this->seguros;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Fecha
     */
    public function setFecha($fecha) {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha() {
        return $this->fecha;
    }

}
