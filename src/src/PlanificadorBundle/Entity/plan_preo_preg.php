<?php

namespace PlanificadorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * plan_preo_preg
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="PlanificadorBundle\Entity\plan_preo_pregRepository")
 */
class plan_preo_preg
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="idPreocupacion", type="integer")
     */
    private $idPreocupacion;

    /**
     * @var integer
     *
     * @ORM\Column(name="idPregunta", type="integer")
     */
    private $idPregunta;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idPreocupacion
     *
     * @param integer $idPreocupacion
     * @return plan_preo_preg
     */
    public function setIdPreocupacion($idPreocupacion)
    {
        $this->idPreocupacion = $idPreocupacion;

        return $this;
    }

    /**
     * Get idPreocupacion
     *
     * @return integer 
     */
    public function getIdPreocupacion()
    {
        return $this->idPreocupacion;
    }

    /**
     * Set idPregunta
     *
     * @param integer $idPregunta
     * @return plan_preo_preg
     */
    public function setIdPregunta($idPregunta)
    {
        $this->idPregunta = $idPregunta;

        return $this;
    }

    /**
     * Get idPregunta
     *
     * @return integer 
     */
    public function getIdPregunta()
    {
        return $this->idPregunta;
    }
}
