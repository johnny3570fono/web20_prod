<?php

namespace PlanificadorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * plan_mom_preo
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="PlanificadorBundle\Entity\plan_mom_preoRepository")
 */
class plan_mom_preo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="plan_filtros", inversedBy="plan_mom_preo")
     * @ORM\JoinColumn(name="id_filtros", referencedColumnName="id")
     */
    private $idFiltros;

    /**
     * @ORM\ManyToOne(targetEntity="plan_momento", inversedBy="momPreo")
     * @ORM\JoinColumn(name="id_momento", referencedColumnName="id", onDelete="CASCADE")
     */
    private $idMomento;

    /**
     * @ORM\ManyToOne(targetEntity="plan_preocupacion", inversedBy="plan_mom_preo")
     * @ORM\JoinColumn(name="id_preocupacion", referencedColumnName="id")
     */
    private $idPreocupacion;

    /**
     * @ORM\ManyToOne(targetEntity="plan_pregunta", inversedBy="plan_prguntamom_preo")
     * @ORM\JoinColumn(name="id_pregunta", referencedColumnName="id")
     */
    private $idPregunta;

    /**
    * @var string
    *
    * @ORM\Column(name="preocupacion", type="json_array", nullable=true)
    */
    private $preocupacion;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idFiltros
     *
     * @param \PlanificadorBundle\Entity\plan_filtros $idFiltros
     * @return plan_mom_preo
     */
    public function setIdFiltros(\PlanificadorBundle\Entity\plan_filtros $idFiltros = null)
    {
        $this->idFiltros = $idFiltros;

        return $this;
    }

    /**
     * Get idFiltros
     *
     * @return \PlanificadorBundle\Entity\plan_filtros 
     */
    public function getIdFiltros()
    {
        return $this->idFiltros;
    }

    /**
     * Set idMomento
     *
     * @param \PlanificadorBundle\Entity\plan_momento $idMomento
     * @return plan_mom_preo
     */
    public function setIdMomento(\PlanificadorBundle\Entity\plan_momento $idMomento = null)
    {
        $this->idMomento = $idMomento;

        return $this;
    }

    /**
     * Get idMomento
     *
     * @return \PlanificadorBundle\Entity\plan_momento 
     */
    public function getIdMomento()
    {
        return $this->idMomento;
    }

    /**
     * Set idPreocuapcion
     *
     * @param \PlanificadorBundle\Entity\plan_preocupacion $idPreocuapcion
     * @return plan_mom_preo
     */
    public function setIdPreocupacion(\PlanificadorBundle\Entity\plan_preocupacion $idPreocupacion = null)
    {
        $this->idPreocupacion = $idPreocupacion;

        return $this;
    }

    /**
     * Get idPreocuapcion
     *
     * @return \PlanificadorBundle\Entity\plan_preocupacion 
     */
    public function getIdPreocupacion()
    {
        return $this->idPreocupacion;
    }

    /**
     * Set idPregunta
     *
     * @param \PlanificadorBundle\Entity\plan_pregunta $idPregunta
     * @return plan_mom_preo
     */
    public function setIdPregunta(\PlanificadorBundle\Entity\plan_pregunta $idPregunta = null)
    {
        $this->idPregunta = $idPregunta;

        return $this;
    }

    /**
     * Get idPregunta
     *
     * @return \PlanificadorBundle\Entity\plan_pregunta 
     */
    public function getIdPregunta()
    {
        return $this->idPregunta;
    }
    /**
     * Getter
     *
     * @return string
     */
    public function getPreocupacion()
    {
        return $this->preocupacion;
    }
    /**
     * Setter
     *
     * @param string $preocupacion
     * @return plan_mom_preo
     */
    public function setPreocupacion($preocupacion)
    {
        $this->preocupacion = $preocupacion;

        return $this;
    }
}
