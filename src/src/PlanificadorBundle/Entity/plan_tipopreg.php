<?php

namespace PlanificadorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * plan_tipopreg
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="PlanificadorBundle\Entity\plan_tipopregRepository")
 */
class plan_tipopreg
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255)
     */
    private $titulo;

    /**
     * @var integer
     *
     * @ORM\Column(name="value", type="integer")
     */
    private $value;


    /**
     * @ORM\OneToMany(targetEntity="plan_tipopreg", mappedBy="pregtipo")
    */
    private $tipopreg;

    /**
     * @ORM\OneToMany(targetEntity="plan_tipopreg", mappedBy="tipo")
    */
    private $arbol;
 
   
    public function __construct()
    {
        $this->tipopreg = new \Doctrine\Common\Collections\ArrayCollection();
        $this->arbol = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return plan_tipopreg
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set value
     *
     * @param integer $value
     * @return plan_tipopreg
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return integer 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Add tipopreg
     *
     * @param \PlanificadorBundle\Entity\plan_tipopreg $tipopreg
     * @return plan_tipopreg
     */
    public function addTipopreg(\PlanificadorBundle\Entity\plan_tipopreg $tipopreg)
    {
        $this->tipopreg[] = $tipopreg;

        return $this;
    }

    /**
     * Remove tipopreg
     *
     * @param \PlanificadorBundle\Entity\plan_tipopreg $tipopreg
     */
    public function removeTipopreg(\PlanificadorBundle\Entity\plan_tipopreg $tipopreg)
    {
        $this->tipopreg->removeElement($tipopreg);
    }

    /**
     * Get tipopreg
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTipopreg()
    {
        return $this->tipopreg;
    }

    /**
     * Add arbol
     *
     * @param \PlanificadorBundle\Entity\plan_tipopreg $arbol
     * @return plan_tipopreg
     */
    public function addArbol(\PlanificadorBundle\Entity\plan_tipopreg $arbol)
    {
        $this->arbol[] = $arbol;

        return $this;
    }

    /**
     * Remove arbol
     *
     * @param \PlanificadorBundle\Entity\plan_tipopreg $arbol
     */
    public function removeArbol(\PlanificadorBundle\Entity\plan_tipopreg $arbol)
    {
        $this->arbol->removeElement($arbol);
    }

    /**
     * Get arbol
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getArbol()
    {
        return $this->arbol;
    }
}
