<?php

namespace PlanificadorBundle\Form\admin;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class FiltroEditarGastosType extends AbstractType
{
    
    public function __construct ($em)
    {
      $this->em = $em;
    }

    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
     $em = $this->em;
        
     $arregloDatos = array();

     
     $datosRespuestas = $em->getRepository('PlanificadorBundle:plan_filtros')->getTitulorespuestasgastos(); 
     
    
       foreach($datosRespuestas as $dato)
       {
 
         $arregloDatos[$dato['idrespuesta']]= $dato['titulo'];
       }
      asort($arregloDatos);
      
        $builder
                ->add('gastos', 'choice', array(
                    'label' => 'Seleccione:',
                    'choices' => $arregloDatos,           
                    'multiple'  =>false,
                    'expanded' => false,
                    'empty_value' => 'Selecciona un dato'
                    ))            
                ->add('valor', 'integer', array( 'label' => 'valor'))
                ->add('simbolo', 'entity', [
                'class' => 'PlanificadorBundle:plan_simbolo',
                               'query_builder' => function(EntityRepository $er) 
                            {
                            return $er->createQueryBuilder('u');
                        
                            }, 'property' => 'simbolo',
                               'multiple'  => false,
                               'expanded' => false 
            ] )
                
                ->add('choicesjson', 'hidden');
    
            }
    

    public function getName()
    {
        return 'filtro_form';
    }
}
