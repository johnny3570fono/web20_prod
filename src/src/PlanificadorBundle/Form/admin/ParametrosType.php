<?php

namespace PlanificadorBundle\Form\admin;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichFileType;



class ParametrosType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('max_sel', 'text',array( 'label' => 'Máx Seleccionable'))
                ->add('cant_max', 'text',array( 'label' => 'Cantidad Máxima'))
                ->add('description', 'text',array( 'label' => 'descripción'));               

    }

    public function getName()
    {
        return 'parametros_form';
    }
}
