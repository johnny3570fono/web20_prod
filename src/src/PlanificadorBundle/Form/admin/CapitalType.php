<?php

namespace PlanificadorBundle\Form\admin;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;


class CapitalType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('nombre', 'text',array( 'label' => 'Momento'))
                
                ->add('value', 'textarea',array(
                    'attr' => array('rows' => '10','cols' => '10')
                    ));
    }

    public function getName()
    {
        return 'capital_form';
    }
}
