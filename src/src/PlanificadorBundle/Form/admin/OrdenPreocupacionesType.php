<?php

namespace PlanificadorBundle\Form\admin;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichFileType;



class OrdenPreocupacionesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('orderBy','hidden', array(
                    'label' => 'Es solo un test'
                ))
                ->add('desOrderBy','hidden', array(
                    'label' => 'Es solo un test2'
                ));

    }

    public function getName()
    {
        return 'Momento_form';
    }
}
