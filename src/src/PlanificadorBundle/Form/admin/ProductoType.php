<?php

namespace PlanificadorBundle\Form\admin;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;


class ProductoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('nombre', 'text',array( 'label' => 'Momento'))
                ->add('estado', 'choice', array(
                    'label' => 'Visible',
                    'choices' => array(0 => 'No', 1 => 'Si'),
                    'multiple'  =>false,
                    'expanded' => false
                ))
                ->add('Segmentosplani', 'entity', [
                'class' => 'AppBundle:Segment',
                'choice_label' => 'name',
                'expanded' => true,
                'multiple' => true,
                'label' => 'Segments'])

                ->add('descripcion', 'ckeditor' ,array(
                    'config_name' => 'simple',
                    ))
                ->add('cubre', 'ckeditor',array(
                    'config_name' => 'simple',
                    ))
                ->add('nocubre', 'ckeditor',array(
                    'config_name' => 'simple',
                    ))
                ->add('informacion', 'ckeditor',array(
                    'config_name' => 'simple',
                    ))
               
                ->add('imagen', 'vich_image', array(
                    'required'      => false,
                    'allow_delete'  => false, // not mandatory, default is true
                    'download_link' => false,// not mandatory, default is true
                    'label' => false // not mandatory, default is true
                    ))
                ->add('imagenTipo','hidden', array(
                    'label' => 'Imagen por ahora text'
                    ))
                ->add('choicesjson','hidden', array(
                    'label' => 'Imagen por ahora text'
                    ));


    }

    public function getName()
    {
        return 'Producto_form';
    }
}
