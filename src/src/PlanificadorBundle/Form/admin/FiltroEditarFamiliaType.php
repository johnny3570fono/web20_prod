<?php

namespace PlanificadorBundle\Form\admin;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class FiltroEditarFamiliaType extends AbstractType
{
    
 /*   public function __construct ($em)
    {
      $this->em = $em;
    }*/

    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    // $em = $this->em;
        
     $arregloDatos = array();

     
    /* $repository = $em->getRepository('PlanificadorBundle:plan_simbolo'); 
     $simbolos = $repository->findAll();
    /*   foreach($simbolos as $dato)
       {
         $arregloDatos[$dato->getId()]= $dato->getSimbolo();
       }
      asort($arregloDatos);*/
    
        $builder
                
                ->add('Edocivil', 'choice', array(
                    'label' => 'Seleccione:',
                    'choices' => array('S' => 'Soltero(a)','C' => 'Casado(a)','D' => 'Divorciado(a)','V'=>'Viudo(a)'),          
                    'multiple'  =>false,
                    'expanded' => false,
                    'empty_value' => 'Selecciona un dato'
                    ))            
               /* ->add('simbhijos', 'choice', array(
                    'label' => 'Seleccione:',
                    'choices' => $arregloDatos,          
                    'multiple'  =>false,
                    'expanded' => false,
                    'empty_value' => 'Selecciona un dato'
                    )) */  
                ->add('canthijos', 'integer', array( 'label' => 'valor'))
                ->add('simbolo', 'entity', [
                'class' => 'PlanificadorBundle:plan_simbolo',
                               'query_builder' => function(EntityRepository $er) 
                            {
                            return $er->createQueryBuilder('u');
                        
                            }, 'property' => 'simbolo',
                               'multiple'  => false,
                               'expanded' => false] )
                ->add('valor', 'integer', array( 'label' => 'valor'))
                
                ->add('choicesjson', 'hidden');
    
            }
    

    public function getName()
    {
        return 'filtro';
    }
}
