<?php

namespace PlanificadorBundle\Form\admin;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class FiltroEditarCapitalType extends AbstractType
{
     
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      
        $builder
                ->add('capital', 'entity', [
                'class' => 'PlanificadorBundle:plan_capital',
                               'query_builder' => function(EntityRepository $er) 
                            {
                            return $er->createQueryBuilder('u');
                          }, 'property' => 'Nombre',
                               'multiple'  => false,
                               'expanded' => false,
                               'empty_value' => 'Selecciona un dato'
            ])
                ->add('variables', 'choice', array(
                    'label' => 'Seleccione:',
                    'choices' => array('1' => 'A','2' => 'B','3' => 'C','4'=>'D'),         
                    'multiple'  =>false,
                    'expanded' => false,
                    'empty_value' => 'Selecciona un dato'
                    ))   
                 ->add('tipoPregunta', 'choice', array(
                    'label' => 'Seleccione:',
                    'choices' => array('1' => 'Pregunta Generica','2' => 'Pregunta Especifica'),    
                    'multiple'  =>false,
                    'expanded' => false,
                    'empty_value' => 'Selecciona un dato'
                    ))   
                
                ->add('oppregunta', 'choice', array(
                    'label' => 'Seleccione:',
                    'choices' => array(),    
                    'multiple'  =>false,
                    'expanded' => false,
                    'empty_value' => 'Selecciona un dato'
                    ))   
                
                               
                ->add('choicesjson', 'hidden');
    
            }
    

    public function getName()
    {
        return 'filtro_form';
    }
}
