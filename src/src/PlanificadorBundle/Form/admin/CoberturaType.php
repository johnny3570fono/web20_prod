<?php

namespace PlanificadorBundle\Form\admin;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;


class CoberturaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('nombre', 'text',array( 'label' => 'Momento'))
                
                ->add('descripcion', 'ckeditor' ,array(
                    'config_name' => 'simple',
                    ))
                ->add('capital', 'text',array(
                   
                    ))
                ->add('imagen', 'vich_image', array(
                    'required'      => false,
                    'allow_delete'  => false, // not mandatory, default is true
                    'download_link' => false,// not mandatory, default is true
                    'label' => false // not mandatory, default is true
                    ))
                ->add('imagenTipo','hidden', array(
                    'label' => 'Imagen por ahora text'
                    ));

    }

    public function getName()
    {
        return 'Cobertura_form';
    }
}
