<?php

namespace PlanificadorBundle\Form\admin;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use PlanificadorBundle\Entity\plan_pregtipo;
use PlanificadorBundle\Form\RelationshipType;

class ArbolType extends AbstractType
{

  public function __construct ($em)
  {
      $this->em = $em;
  }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      $arregloPreguntas = $this->ArregloPreguntas();
        $builder->add('idpreocupacion', 'entity', array('label' => 'Preocupación',
                           'class' => 'PlanificadorBundle:plan_preocupacion',
                               'query_builder' => function(EntityRepository $er) 
                            {
                            return $er->createQueryBuilder('u')->OrderBy('u.id', 'ASC');
                        
                            }, 'property' => 'descripcion',
                               'multiple'  => false,
                               'expanded' => false,
                              'empty_value' => 'Selecciona un dato'
                    ))
                 ->add('tipo', 'entity', array(
                    'class' => 'PlanificadorBundle:plan_tipopreg',
                    'property' => 'titulo', 
                    'empty_value' => 'Selecciona un dato',
                    'required' => false
                
                    ))
                 ->add('pregPadre', 'choice', array(
                          'label' => 'Numero Cuenta INE: ',
                          'choices' => $arregloPreguntas,
                          'multiple'  => false,
                          'expanded' => false,
                          'empty_value' => 'No tiene Padre',
                      ))       
                 ->add('preghijo','entity', array(
                          'label' => 'Pregunta hijo',
                          'empty_value'   => 'None',
                          'required'      => false,
                          'class' => 'PlanificadorBundle:plan_pregunta',
                          'multiple'  => false,
                          'expanded' => false,
                          'property' => 'titulo',
                    ))
                 ->add('gastos','hidden')
                 ->add('choices','hidden' ,array())
                 ->add('choicesjson', 'hidden')
                 ->add('choicesjsongastos', 'hidden')
                 ->add('choicesjsonfamilia', 'hidden')
                 ->add('choicesjsonrespuesta', 'hidden')
                 ->add('choicesjsonCapital', 'hidden')
                 ->add('json_total', 'hidden');
    }
      public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PlanificadorBundle\Entity\plan_arbol',
        ));
    }


    public function getName()
    {
      return 'Arbol_form';
    }

     public function ArregloPreguntas()
   {
    $ArregloPreguntas=array();
    $preguntas = $this->em->getRepository('PlanificadorBundle:plan_arbol')->findAll();
   // $preguntas= $this->em->getRepository('PlanificadorBundle:plan_arbol')->getArbolPreg();
    
    foreach($preguntas as $pregunta)
     {
      if($pregunta->getPregHijo()){
      
       $ArregloPreguntas[$pregunta->getId()] = $pregunta->getPregHijo()->getTitulo();   
     }
     }
     
     return $ArregloPreguntas;
      
   }
}
