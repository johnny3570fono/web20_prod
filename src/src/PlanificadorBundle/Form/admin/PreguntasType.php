<?php

namespace PlanificadorBundle\Form\admin;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichFileType;
use PlanificadorBundle\Form\admin\RespuestasType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use PlanificadorBundle\Entity\plan_pregunta;
use PlanificadorBundle\Entity\plan_pregtipo;



class PreguntasType extends AbstractType
{
   /* public function __construct ($tiporesp)
    {        
                $this->tiporesp = $tiporesp;
                
    }*/
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //$tiporesp =$this->tiporesp;

        $builder->add('titulo', 'text',array( 'label' => 'titulo'))
                ->add('descripcion', 'ckeditor' ,array(
                    'config_name' => 'simple',
                    ))
                /*->add('visible', 'choice', array(
                    'label' => 'Visible',
                    'choices' => array(0 => 'No', 1 => 'Si'),
                    'multiple'  =>false,
                    'expanded' => false
                    ))*/
               
                ->add('imagen', 'vich_image', array(
                    'required'      => true,
                    'allow_delete'  => false, // not mandatory, default is true
                    'download_link' => false,// not mandatory, default is true
                    'label' => false // not mandatory, default is true
                    ))
                ->add('imagenTipo','hidden', array(
                    'label' => 'Imagen por ahora text'
                    ))
                 ->add('tipo', 'entity', array('label' => 'Tipo de Respuestas',
                           'class' => 'PlanificadorBundle:plan_tipo',
                               'query_builder' => function(EntityRepository $er) 
                            {
                            return $er->createQueryBuilder('u')->OrderBy('u.idtipo', 'ASC');
                        
                        }, 'property' => 'nombre',
                               'multiple'  => false,
                               'expanded' => false))
                 ->add('json_respuesta','hidden')
                 ->add('respuestas','collection' ,array(
                    'type' => new RespuestasType(),
                    'label'=> 'Respuestas',
                    'by_reference'=> false,
                    'allow_delete'   => true,
                    'allow_add'      => true,
                    'attr'           => array(
                    'class' => 'row respuestas'
                    )
                    ))
                  ->add('pregtipo', 'entity', array(
                    'class' => 'PlanificadorBundle:plan_tipopreg',
                    'property' => 'titulo', 
                    'empty_value' => 'Selecciona un dato', 
                    'required' => false
                    ))
                  ->add('orderBy', 'integer',array( 'label' => 'Orderby',
                    'required' => false));


    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PlanificadorBundle\Entity\plan_pregunta',
        ));
    }

    public function getName()
    {
        return 'Pregunta_form';
    }
}
