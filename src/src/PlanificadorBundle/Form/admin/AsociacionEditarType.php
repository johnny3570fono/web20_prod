<?php

namespace PlanificadorBundle\Form\admin;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;


class AsociacionEditarType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('idMomento', 'entity', array(
                    'class' => 'PlanificadorBundle:plan_momento',
                    'query_builder' => function(EntityRepository $er)  
                    {
                        return $er->createQueryBuilder('m')
                    ->leftJoin('m.momPreo', 'pm')
                    ->andWhere('pm.idMomento is null');
                    }, 
                    'property' => 'descripcion',
                    'multiple'  => false,
                    'expanded' => false,
                    'empty_value' => 'Selecciona un dato', 
                    'required' => false

                ))
                ->add('preocupacion', 'hidden',array( 'label' => 'Momento'));

                /*->add('idPreocuapcion', 'entity', array(
                    'class' => 'PlanificadorBundle:plan_preocupacion',
                    'property' => 'descripcion', 
                    'empty_value' => 'Selecciona un dato', 
                    'required' => false
                ))
                ->add('idPregunta', 'entity', array(
                    'class' => 'PlanificadorBundle:plan_pregunta',
                    'property' => 'titulo',  
                    'empty_value' => 'Selecciona un dato', 
                    'required' => false
                ));*/

    }

    public function getName()
    {
        return 'Asociacion_form';
    }
}
