<?php

namespace PlanificadorBundle\Form\admin;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichFileType;

class PreguntasGeneralesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('descripcion', 'text',array( 'label' => 'Pregunta'))
                ->add('visible', 'choice', array(
                    'label' => 'Visible',
                    'choices' => array(0 => 'No', 1 => 'Si'),
                    'multiple'  =>false,
                    'expanded' => false
                    ))
               
                ->add('imagen', 'vich_image', array(
                    'required'      => true,
                    'allow_delete'  => false, // not mandatory, default is true
                    'download_link' => false,// not mandatory, default is true
                    'label' => false // not mandatory, default is true
                    ))
                ->add('imagenTipo','hidden', array(
                    'label' => 'Imagen por ahora text'
                    ))
                 ->add('tipo', 'entity', array('label' => 'Tipo de Pregunta',
                           'class' => 'PlanificadorBundle:plan_tipo',
                               'query_builder' => function(EntityRepository $er) 
                            {
                            return $er->createQueryBuilder('u')->OrderBy('u.idtipo', 'ASC');
                        
            }, 'property' => 'nombre',
                               'multiple'  => false,
                               'expanded' => false));


    }

    public function getName()
    {
        return 'Momento_form';
    }
}
