<?php

namespace PlanificadorBundle\Form\admin;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;


class FiltroEditarType extends AbstractType
{

      
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder->add('choicesjson', 'hidden');
    
            }
     public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PlanificadorBundle\Entity\plan_filtros',
        ));

    }

    public function getName()
    {
        return 'filtro_form';
    }
}
