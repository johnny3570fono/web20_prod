<?php

namespace PlanificadorBundle\Form\admin;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use PlanificadorBundle\Entity\plan_respuestas;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;


class RespuestasType extends AbstractType
{

  /*  public function __construct ($tiporesp)
    {        
                $this->tiporesp = $tiporesp;
                
    }*/
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
            // $tiporesp =$this->tiporesp;
        $builder->add('titulo', 'text',array( 'label' => 'Titulo'))
                ->add('descripcion', 'text',array( 'label' => 'Descripcion','required' => false))
                ->add('imagen', 'vich_image', array(
                    'attr' => array('style'=>'display:none;'),
                    'required'      => false,
                    'allow_delete'  => false, // not mandatory, default is true
                    'download_link' => false,// not mandatory, default is true
                    'label' => false, // not mandatory, default is true
                    'required' => false
                    ))
                ->add('imagenTipo','hidden', array(
                    'label' => 'Imagen por ahora text'
                    ))
            
                ->add('valor', 'hidden',array( 'label' => 'Valor'))
                ->add('Desde', 'hidden',array( 'label' => 'Desde'))
                ->add('Hasta', 'hidden',array( 'label' => 'Hasta'));
    }
     public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PlanificadorBundle\Entity\plan_respuestas',
          /*  'data' => function(Options $options) {
            $class = $options['data_class'];
            return new $class;
        },*/
        ));

    }

    public function getName()
    {
        return 'respuestas_form';
    }
}
