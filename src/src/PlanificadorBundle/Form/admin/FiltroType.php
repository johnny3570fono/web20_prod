<?php

namespace PlanificadorBundle\Form\admin;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;


class FiltroType extends AbstractType
{

      
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder
                ->add('pregunta', 'entity', [
                'class' => 'PlanificadorBundle:plan_pregunta',
                               'query_builder' => function(EntityRepository $er) 
                            {
                            return $er->createQueryBuilder('u')
                            ->where('u.visible = :true')
                            ->andWhere('u.pregtipo=:especifica')
                            ->setParameter('true', '1')
                            ->setParameter('especifica', '2')
                            ->OrderBy('u.id', 'ASC');
                        
                            }, 'property' => 'titulo',
                               'multiple'  => false,
                               'expanded' => false 
            ])
                ->add('json_filtro', 'collection', array(
                'type' => new FiltroOpcionesType(),
                'label'=> 'opcionefiltro',
                    'by_reference'=> false,
                    'allow_delete'   => true,
                    'allow_add'      => true,
                    'prototype_name' =>'__opcionesfiltro__',
                    'attr'           => array(
                    'class' => 'row opcionesfiltro'
                    )
                ))
                ->add('choicesjson', 'hidden');
    
            }
    

    public function getName()
    {
        return 'filtro_form';
    }
}
