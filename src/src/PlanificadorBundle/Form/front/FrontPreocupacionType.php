<?php

namespace PlanificadorBundle\Form\front;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class FrontPreocupacionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('preocupaciones','hidden',array(
                    'required' => false,
                  ));
    }
    

    public function getName()
    {
      return 'Preocupaciones_form';
    }
}
