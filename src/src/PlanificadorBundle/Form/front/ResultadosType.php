<?php

namespace PlanificadorBundle\Form\front;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class ResultadosType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('checked','hidden',array(
                    'required' => false,
                  ))
                ->add('addressEmail', 'email', [
                'label' => 'E-mail',
                'constraints' => [
                        new  \Symfony\Component\Validator\Constraints\Email()
                    ]
                ])
                
                ;
    }
    

    public function getName()
    {
      return 'resultados_form';
    }
}
