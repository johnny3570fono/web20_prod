<?php

namespace PlanificadorBundle\Services;

class ExportExcel
{	
    private $writer;
    
	public function __construct( $rootpath ) {
		require_once( $rootpath.'/../vendor/php_xlsxwriter/xlsxwriter.class.php');
        $this->writer = new \XLSXWriter();     
	}
    
    public function export( $seguimientos ) {
        $header = array(
            'ID' => 'string',
            'Usuario' => 'string',
            'SessionID' => 'string',
            'UserAgent' => 'string',
            'Momentos_URL' => 'string',
            'Momentos' => 'string',
            'Preocupaciones_URL' => 'string',
            'Preocupaciones' => 'string',
            'Familia_URL' => 'string',
            'Familia_Estado_Civil' => 'string',
            'Familia_Cantidad_Hijos' => 'string',
            'Familia_Cantidad_Edades' => 'string',
            'Gastos_URL' => 'string',
            'Gastos' => 'string',
            'Seguros_URL' => 'string',
            'Seguros' => 'string',
            'Valores_URL' => 'string',
            'Valores' => 'string',
            'Resultado_URL' => 'string',
            'Resultado' => 'string',
            'Creado' => 'string',
            'Modificado' => 'string',
        );
        

        $rows = [];
        if( $seguimientos ) {
            foreach( $seguimientos as $key => $value ) {
                $info = $value->getInfo();
                
                $rows[$key][] = $value->getId();
                $rows[$key][] = $value->getUsername();
                $rows[$key][] = $value->getSessionid();
                $rows[$key][] = $value->getUseragent();
                
                //momentos
                if( isset($info["momentos"]) ) {
                    $rows[$key][] = $info["momentos"]["url"];
                    $rows[$key][] = (isset( $info["momentos"]["seleccionado"] )) ? implode( ";", $info["momentos"]["seleccionado"] ) : '--' ;
                    
                }else{
                    $rows[$key][] = "--";
                    $rows[$key][] = "--";
                }
                
                //preocupaciones
                if( isset($info["preocupacion"]) ) {
                    $rows[$key][] = $info["preocupacion"]["url"];
                    $rows[$key][] = (isset( $info["preocupacion"]["seleccionado"] )) ? implode( ";", $info["preocupacion"]["seleccionado"] ) : '--';
                    
                }else{
                    $rows[$key][] = "--";
                    $rows[$key][] = "--";
                }
                
                //familia
                if( isset($info["familia"]) ) {
                    $rows[$key][] = $info["familia"]["url"];
                    $rows[$key][] = $info["familia"]["estado"];
                    $rows[$key][] = $info["familia"]["cantidad_hijos"];
                    $rows[$key][] = (isset( $info["familia"]["edades"] )) ? implode( ";", $info["familia"]["edades"] ) : '--';
                    
                }else{
                    $rows[$key][] = "--";
                    $rows[$key][] = "--";
                    $rows[$key][] = "--";
                    $rows[$key][] = "--";
                }
                
                //gastos
                if( isset($info["gastos"]) ) {
                    $rows[$key][] = $info["gastos"]["url"];
                    $rows[$key][] = implode(';', array_map(function($item) {
                        $titulo = $item["titulo"];
                        $valor = (float) $item["valor"];
                        
                        return $titulo."(".$valor.")";
                    }, $info["gastos"]["valores"]));                    
                }else{
                    $rows[$key][] = "--";
                    $rows[$key][] = "--";
                }
                
                //seguros
                if( isset($info["seguros"]) ) {
                    $rows[$key][] = $info["seguros"]["url"];
                    $rows[$key][] = (isset( $info["seguros"]["seleccionado"] )) ? implode( ";", $info["seguros"]["seleccionado"] ) : '--';
                    
                }else{
                    $rows[$key][] = "--";
                    $rows[$key][] = "--";
                }
                
                //valores
                if( isset($info["valor_cosas"]) ) {
                    $rows[$key][] = $info["valor_cosas"]["url"];
                    $rows[$key][] = implode(';', array_map(function($item) {
                        $titulo = $item["titulo"];
                        $valor = (float) $item["valor"];
                        
                        return $titulo."(".$valor.")";
                    }, $info["valor_cosas"]["valores"]));                    
                }else{
                    $rows[$key][] = "--";
                    $rows[$key][] = "--";
                }
                
                //resultados
                if( isset($info["resultados"]) ) {
                    $rows[$key][] = $info["resultados"]["url"];
                    $rows[$key][] = (isset( $info["resultados"]["seleccionado"] )) ? implode( ";", $info["resultados"]["seleccionado"] ) : '--';
                    
                }else{
                    $rows[$key][] = "--";
                    $rows[$key][] = "--";
                }
                
                $rows[$key][] = $value->getCreated()->format('Y-m-d H:i:s');
                $rows[$key][] = $value->getUpdated()->format('Y-m-d H:i:s');
            }
        }
                
        // $this->writer->writeSheetHeader('Sheet1', $header);
        // foreach($rows as $row) {
            // $this->writer->writeSheetRow('Sheet1', $row);
        // }
        
        // $this->writer->writeToFile('xlsx-simple.xlsx');
        $this->writer->writeSheet( $rows, 'Sheet1', $header );
        
        header('Content-disposition: attachment;   filename="Seguimiento_'.date('YmdHis').'.xlsx"');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); 
        header('Access-Control-Allow-Methods: GET'); 
        header('Access-Control-Allow-Origin: *');
        $this->writer->writeToStdOut();
    }
}