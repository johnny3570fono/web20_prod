<?php

namespace PlanificadorBundle\Controller\admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use PlanificadorBundle\Entity\plan_preguntaGenerica;
use PlanificadorBundle\Entity\plan_pregunta;
use PlanificadorBundle\Form\admin\PreguntasGeneralesType;
use PlanificadorBundle\Form\admin\PreguntasType;
use PlanificadorBundle\Entity\plan_menu;
use PlanificadorBundle\Form\admin\ConfigType;
use PlanificadorBundle\Controller\admin\AdminController;
use PlanificadorBundle\Form\admin\PreguntasRespType;
use PlanificadorBundle\Entity\plan_respuestas;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\JsonResponse;




class PreguntasController extends AdminController
{
  // ++++++++++++++++++++++++++++++++++++++++++++++++++++ ACCIONES DE MOMENTOS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  public function EditarGenericasAction(Request $request, $id)
  {
    $em = $this->getDoctrine()->getManager();
    $menus=$this->CrearMenu();
 		
    if (isset($id)) 
		{
      $valorid = 1;
			$pregunta = $em->find('PlanificadorBundle:plan_preguntaGenerica', $id);
		}else{
			$pregunta = new plan_preguntaGenerica();
      $valorid = 0;
		}
    $form = $this->createForm(new PreguntasGeneralesType(), $pregunta);
		$form->handleRequest($request);	
    
    if ($form->isSubmitted() ){

	    	$data = $form->getData();
		    $em->persist($data);
		    $em->flush();
		    return $this->redirect($this->generateUrl('preguntasgenerica_listar'));
    }
    return $this->render('PlanificadorBundle:administrador:preguntasgenericas/preguntasgenericas.html.twig',array(
      'form'=>$form->createView(),
      'tipo'=>  $valorid,
      'menus'=>$menus
    ));
    	 
	} 
  public function ListarGenericasAction()
  {
    $menus=$this->CrearMenu();
    $repository = $this->getDoctrine()->getRepository('PlanificadorBundle:plan_preguntaGenerica');
    $preguntas = $repository->findBy(array('visible'=>1));
    $valorid = 0;

    return $this->render('PlanificadorBundle:administrador:preguntasgenericas/preguntasgenericas_list.html.twig',array(
      'preguntas'=>$preguntas,
      'tipo'=>  $valorid ,
      'menus'=>$menus
      ));
  }
  public function EliminaGenericasAction($id)
  {
    $em = $this->getDoctrine()->getEntityManager();
    $menus=$this->CrearMenu();
      $valorid = 0;
    $repository = $this->getDoctrine()->getRepository('PlanificadorBundle:plan_preguntaGenerica');
    $pregunta = $em->find('PlanificadorBundle:plan_preguntaGenerica', $id);
    
    if (!$pregunta){
      throw new NotFoundHttpException("No Existe momento Seleccionado.");
    }else{
      $pregunta->setVisible(0);
      $em->persist($pregunta);
     
      $em->flush();
  
      return $this->redirect($this->generateUrl('preguntasgenerica_listar'));            
    }
      return $this->render('PlanificadorBundle:administrador:preguntasgenericas/preguntasgenericas_list.html.twig',array(
      'preguntas'=>$preguntas,
      'tipo'=>  $valorid ,
      'menus'=>$menus
      ));
  }
  public function EditarAction(Request $request, $id)
 {
    $em = $this->getDoctrine()->getManager();
    $menus=$this->CrearMenu();
    $img = 0;
    $desde=null;
    $hasta=null;
    $valor=null;
    $tiporesp=null;
    $arregloPresentacion=array();
    $cont=0;
    $tipoPreg='';
    $em = $this->getDoctrine()->getManager();
    if (isset($id)) 
    {/***********************editando*******************************+*/
      $valorid = 1;
      $pregunta = $em->find('PlanificadorBundle:plan_pregunta', $id);
      $img = $pregunta->getImagenTipo();
      $tiporesp=$pregunta->getTipo();
      $tipoPreg=$pregunta->getPregtipo()->getId();

        foreach ($pregunta->getRespuestas() as $respuestas_obje) {
        $arregloDatos['index'] = $cont;
        $arregloDatos['idrespuesta'] = $respuestas_obje->getIdrespuesta();
        $arregloDatos['titulo'] = $respuestas_obje->getTitulo();
        $arregloDatos['descripcion'] = $respuestas_obje->getDescripcion();
        $arregloDatos['visible'] = $respuestas_obje->getVisible();
        $arregloDatos['imagen'] =$respuestas_obje->getImagenTipo();
        $valor=$respuestas_obje->getJsonRespuesta();

        $arrayvalores=$valor;
        $cont++;
        if($valor!=null){
          if ($tiporesp->getIdtipo()==2){  //rango

            if($arrayvalores['Desde'] and $arrayvalores['Hasta']){
              $arregloDatos['Desde'] =$arrayvalores['Desde'];
              $arregloDatos['Hasta'] = $arrayvalores['Hasta'];
            }else{
              $arregloDatos['Desde']='';
              $arregloDatos['Hasta']='';
            }
          }else{    
                if($arrayvalores['Valor']){
                  $arregloDatos['Valor'] =  $arrayvalores['Valor'] ;
               }else{
                  $arregloDatos['Hasta']='';
                }
          }
        }
        $arregloPresentacion[] = $arregloDatos;
      }
    }else{
      /*********************nuevo******************/
      $pregunta = new plan_pregunta();
      $valorid = 0;  
    }
    
    // Create an array of the current Address objects in the database
    $originalAnswers = new ArrayCollection();
    // Create an ArrayCollection of the current Tag objects in the database
    foreach ($pregunta->getRespuestas() as $answer) {
      $originalAnswers->add($answer);
    }
    $response = new JsonResponse();
    $form = $this->createForm(new PreguntasType(), $pregunta);
    $form->handleRequest($request); 

    if ($form->isSubmitted())
    {
      $data = $form->getData();

      foreach ($originalAnswers as $answer){
        if (false === $pregunta->getRespuestas()->contains($answer)) {
             $pregunta->removeRespuestas($answer);
            }
             //$user->getAddresses()->removeElement($answer);
            // if you wanted to delete the Address entirely, you can also do that
              $em->remove($answer);
      }
      
      if($data->getImagenTipo() == null)
        $data->setImagenTipo($img);
         
      $data->setVisible(1);
      $respuestas=$pregunta->getRespuestas();

      foreach ($respuestas as $respuesta) {
    
        if ($pregunta->getTipo()->getIdtipo()==2){ /*rango de valores*/
              $response = array(
              'Desde' => $respuesta->getDesde(),
              'Hasta' => $respuesta->getHasta()
              );
        }else{
          $response = array(
          'Valor' => $respuesta->getDesde()
          );
        }
        
    
        $respuesta->setJsonRespuesta($response);            
        $respuesta->setVisible(1);
        
        if($respuesta->getImagenTipo() == null){
            foreach ($arregloPresentacion as $datos){

              if($datos['idrespuesta']==$respuesta->getIdrespuesta())
              {
                $respuesta->setImagenTipo($datos['imagen']);
               
              }
            }
          }

        $em->persist($respuesta);
        var_dump($respuesta->getIdrespuesta());
          var_dump($respuesta->getImagen());
        
      }
      
      $em->persist($data);
      $em->flush();
        return $this->redirect($this->generateUrl('preguntas_listar'));

    }

    return $this->render('PlanificadorBundle:administrador:preguntas/preguntas.html.twig',array(
      'form'=>$form->createView(),
      'tipo'=>  $valorid,
      'menus'=>$menus,
      'img' => $img,
      'tiporesp'=>$tiporesp,
      'arregloPresentacion'=>$arregloPresentacion,
      'tipoPreg'=>$tipoPreg
    ));
       
  } 
    public function NewAction(Request $request, $id)
  {
    $em = $this->getDoctrine()->getManager();
    $menus=$this->CrearMenu();
    $img = 0;
    $valorid=0;

    $pregunta = new plan_pregunta();
     
    $form = $this->createForm(new PreguntasType(), $pregunta);

    $form->handleRequest($request); 

    if ($form->isSubmitted() ){

        $data = $form->getData();
        $data->setVisible(1);


        $respuestas=$pregunta->getRespuestas();
        foreach ($respuestas as $respuesta) {
              
               if ($pregunta->getTipo()->getIdtipo()==2){ /*rango de valores*/
                $response = array(
                'Desde' => $respuesta->getDesde(),
                'Hasta' => $respuesta->getHasta()
                );
              }else{

                  $response = array(
                    'Valor' => $respuesta->getDesde()
                  );
                 }
                $respuesta->setJsonRespuesta(json_encode($response));

               
              $respuesta->setVisible(1);
              $em->persist($respuesta);
       }
          $em->persist($data);
    
        $em->flush();
        return $this->redirect($this->generateUrl('preguntas_listar'));
    }

    return $this->render('PlanificadorBundle:administrador:preguntas/preguntas.html.twig',array(
      'form'=>$form->createView(),
      'tipo'=>  $valorid,
      'menus'=>$menus,
      'img' => $img,
    ));
       
  } 
  public function ListarAction()
  {
    $menus=$this->CrearMenu();
    $repository = $this->getDoctrine()->getRepository('PlanificadorBundle:plan_pregunta');
    $preguntas = $repository->findBy(array('visible'=>1),array('id' => 'DESC'));
    $valorid = 0;
    $eliminar=true;

    return $this->render('PlanificadorBundle:administrador:preguntas/preguntas_listar.html.twig',array(
      'preguntas'=>$preguntas,
      'tipo'=>  $valorid,
      'menus'=>$menus,
      'eliminar'=>$eliminar
      ));
  }
  public function EliminarAction($id)
  {
    $em = $this->getDoctrine()->getEntityManager();
    $menus=$this->CrearMenu();
    $valorid = 0;
    $eliminar=true;
    $repository = $this->getDoctrine()->getRepository('PlanificadorBundle:plan_pregunta');
    $pregunta = $em->find('PlanificadorBundle:plan_pregunta', $id);
    $repository = $this->getDoctrine()->getRepository('PlanificadorBundle:plan_pregunta');
    $preguntas = $repository->findBy(array('visible'=>1),array('id' => 'DESC'));
    if (!$pregunta){
      throw new NotFoundHttpException("No Existe momento Seleccionado.");
    }else{
      /*buscar en el arbol*/
      $repository_arbol = $this->getDoctrine()->getRepository('PlanificadorBundle:plan_arbol');
      $arbol = $repository_arbol->findBy(array('pregHijo'=>$id));
     
      if($arbol!=null){

        $eliminar=false;
      }
     

      /**/
        if($eliminar==true){
         
          $em->persist($pregunta);
          $em->remove($pregunta);
          
          $em->flush();
          
          return $this->redirect($this->generateUrl('preguntas_listar'));   
        }
         
    }
   
      return $this->render('PlanificadorBundle:administrador:preguntas/preguntas_listar.html.twig',array(
      'preguntas'=>$preguntas,
      'tipo'=>  $valorid,
      'menus'=>$menus,
      'eliminar'=>$eliminar
      ));
  }
  public function RelacionPregRespAction()
  {
    $em = $this->getDoctrine()->getEntityManager();
    $menus=$this->CrearMenu();
    $repository = $this->getDoctrine()->getRepository('PlanificadorBundle:plan_pregunta');
    $preguntas = $repository->findBy(array('visible'=>1));
     $valorid = 0;

    return $this->render('PlanificadorBundle:administrador:preguntas/preguntasRespuesta_listar.html.twig',array(
      'preguntas'=>$preguntas,
      'tipo'=>  $valorid ,
      'menus'=>$menus
      ));
  }
    public function PregRespMostrarAction(Request $request,$id)
  {
    $em = $this->getDoctrine()->getEntityManager();
    $menus=$this->CrearMenu();
    $repository = $this->getDoctrine()->getRepository('PlanificadorBundle:plan_pregunta');
    $pregunta = $em->find('PlanificadorBundle:plan_pregunta', $id);

   $form = $this->createForm(new PreguntasRespType(), $pregunta);
    $form->handleRequest($request); 
    
    if ($form->isSubmitted() ){
        $id = $form['pregRel']->getData();

          $respuesta = $em->find('PlanificadorBundle:plan_respuestas', $id);
      
        $respuesta->setPregunta($pregunta);
        $em->persist($respuesta);
        
      $em->flush();
    }

    return $this->render('PlanificadorBundle:administrador:preguntas/preguntasRespuesta_mostrar.html.twig',array(
      'pregunta'=>$pregunta,
      'menus'=>$menus,
      'form'=>$form->createView()
      ));
  }



}
