<?php

namespace PlanificadorBundle\Controller\admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use PlanificadorBundle\Entity\plan_menu;
use PlanificadorBundle\Form\admin\ConfigType;
use PlanificadorBundle\Controller\admin\AdminController;
use PlanificadorBundle\Form\admin\FiltroEditarType;
use PlanificadorBundle\Form\admin\FiltroType;
use PlanificadorBundle\Form\admin\FiltroOpcionesType;
use PlanificadorBundle\Entity\plan_filtros;
use PlanificadorBundle\Entity\plan_option;
use PlanificadorBundle\Form\admin\FiltroEditarGastosType;
use PlanificadorBundle\Form\admin\FiltroEditarFamiliaType; 
use PlanificadorBundle\Form\admin\FiltroEditarCapitalType;



class FiltroController extends AdminController
{
  
  public function EditarAction(Request $request, $id)
  {
    $em = $this->getDoctrine()->getManager();
  	$menus=$this->CrearMenu();
    $arregloPresentacionedad=array();
    $arregloPresentacionsegmento=array();
    $arregloPresentacionsexo=array();
    $repository = $em->getRepository('PlanificadorBundle:plan_option');
    $opciones = $repository->findAll();
    $repository = $em->getRepository('PlanificadorBundle:plan_simbolo');
    $simbolos = $repository->findAll();
    $repository = $em->getRepository('AppBundle:Segment');
    $segmentos = $repository->findAll();

    if (isset($id)) 
		{
			$filtro = $em->find('PlanificadorBundle:plan_filtros',$id);
      $valorid = 1;
		}else{
			$filtro = new plan_filtros();
      $valorid = 0;
		}
    $form = $this->createForm(new FiltroType(), $filtro);
		$form->handleRequest($request);	
    if ($form->isSubmitted() ){
	    $data = $form->getData();
      $opciones=$data->getChoicesjson();
      $jsonopciones=json_decode($opciones, true);
      $filtro->setJsonFiltro($jsonopciones);
      $filtro->setTipo('B');
	    $em->persist($data);
	    $em->flush();
	    return $this->redirect($this->generateUrl('filtro_listar'));
    }
  
       return $this->render('PlanificadorBundle:administrador:filtro/filtro_formeditar.html.twig',array(
      'form'=>$form->createView(),
      'tipo'=>  $valorid,
      'menus'=>$menus,
      'simbolos'=>$simbolos,
      'filtro'=>$filtro,
      'segmentos'=>$segmentos,
      'opciones'=>$opciones
    ));
  
    	 
	} 
  public function ListarAction()
  {
    $em = $this->getDoctrine()->getEntityManager();
    $repository = $this->getDoctrine()->getRepository('PlanificadorBundle:plan_filtros');
    $menus=$this->CrearMenu();
    $arregloPresentacion=array();
    $objetos = $repository->findAll();

    unset($arregloPresentacionsegmento);
    $arregloPresentacionsegmento=array();
    unset($arregloPresentacionedad);
    $arregloPresentacionedad=array();
    unset($arregloPresentacionsexo);
    $arregloPresentacionsexo=array();

    foreach ($objetos as $filtros) {
      $arregloDatos['index'] = $filtros->getId();
      $arregloDatos['pregunta'] = $filtros->getPregunta()->getTitulo();
     if($filtros->getTipo()=='B'){
      if($filtros->getJsonFiltro()){
          foreach ($filtros->getJsonFiltro() as $filtro) 
          {
           if($filtro['opt']==0){ /* sexo */
            $arregloPresentacionsexo[]=$filtro['valor'];                
            }elseif($filtro['opt']==1){ /*edad*/
                    $simbolo = $em->find('PlanificadorBundle:plan_simbolo', $filtro['simbolo']);
                      $arregloedad['simbolo'] = $simbolo->getSimbolo();
                      $arregloedad['edad'] =$filtro['valor'] ;
                      $arregloPresentacionedad[]=$arregloedad;

            }elseif($filtro['opt']==2){  //segmento
                      $segmento = $em->find('AppBundle:Segment', $filtro['valor']);
                      $arregloPresentacionsegmento[] = $segmento->getName();

                    
            }
          $arregloDatos['Edad']= $arregloPresentacionedad;
          $arregloDatos['Segmento']= $arregloPresentacionsegmento;
          $arregloDatos['Sexo']= $arregloPresentacionsexo;
        }  
       }
        $arregloPresentacion[] = $arregloDatos;
       
     }
      }
 
    return $this->render('PlanificadorBundle:administrador:filtro/filtro_listar.html.twig',array(
      'objetos'=>$objetos,
      'filtros'=>$arregloPresentacion,
      'filtros_objeto'=>$objetos,
      'menus'=>$menus
      ));
  }
  public function EliminarAction($id)
  {
    $em = $this->getDoctrine()->getEntityManager();
   
    $repository = $this->getDoctrine()->getRepository('PlanificadorBundle:plan_filtros');

    $objeto = $em->find('PlanificadorBundle:plan_filtros', $id);
    
    if (!$objeto){
      throw new NotFoundHttpException("No Existe objeto Seleccionado.");
    }else{
 
      $em->persist($objeto);
          $em->remove($objeto);
      $em->flush();
  
      return $this->redirect($this->generateUrl('filtro_listar'));            
    }
      
  }

   public function EditarGastosAction(Request $request, $id)
  {
    $em = $this->getDoctrine()->getManager();
    $menus=$this->CrearMenu();

    $repository = $em->getRepository('PlanificadorBundle:plan_simbolo');
    $simbolos = $repository->findAll();


    if (isset($id)) 
    {
      $filtro = $em->find('PlanificadorBundle:plan_filtros',$id);
      $valorid = 1;
    }else{
      $filtro = new plan_filtros();
      $valorid = 0;
    }
    $form = $this->createForm(new FiltroEditarGastosType($em), $filtro);
    $form->handleRequest($request); 
    if ($form->isSubmitted() ){

      $data = $form->getData();
      $opciones=$data->getChoicesjson();
      $jsonopciones=json_decode($opciones, true);
      $filtro->setJsonFiltro($jsonopciones);
      $filtro->setTipo('G');
      $em->persist($data);
      $em->flush();
      return $this->redirect($this->generateUrl('filtro_gastos_listar'));
    }

       return $this->render('PlanificadorBundle:administrador:filtro/filtro_gastos_editar.html.twig',array(
      'form'=>$form->createView(),
      'tipo'=>  $valorid,
      'menus'=>$menus,
      'simbolos'=>$simbolos,
      'filtro'=>$filtro
    ));
  
       
  } 


    public function ListarGastosAction()
  {
    $em = $this->getDoctrine()->getEntityManager();
    $repository = $this->getDoctrine()->getRepository('PlanificadorBundle:plan_filtros');
    $objetos = $repository->findAll();
    $menus=$this->CrearMenu();
    $arregloPresentacion=array();
    
    $arregloPresentacionvalor=array();
    unset($arregloPresentacionvalor);
    

    foreach ($objetos as $filtros) {
      $arregloDatos['index'] = $filtros->getId();
      $arregloDatos['pregunta'] = $filtros->getPregunta()->getTitulo();
     if($filtros->getTipo()=='G'){
          foreach ($filtros->getJsonFiltro() as $filtro) 
          {
            $idrespuesta=$filtro['opt'];
            $respuesta = $em->find('PlanificadorBundle:plan_respuestas',$idrespuesta);
            $arregloDatos['gasto']=$respuesta->getTitulo();
            $simbolo = $em->find('PlanificadorBundle:plan_simbolo', $filtro['simbolo']);
           
            
            $arregloDatos['simbolovalor']=$simbolo->getSimbolo().$filtro['valor'];
            $arregloPresentacion[] = $arregloDatos;
          
       }
     }
    }

    return $this->render('PlanificadorBundle:administrador:filtro/filtro_listarGastos.html.twig',array(
      'objetos'=>$objetos,
      'filtros'=>$arregloPresentacion,
      'filtros_objeto'=>$objetos,
      'menus'=>$menus
      ));
  }

  public function EditarFamiliarAction(Request $request, $id)
  {
    $em = $this->getDoctrine()->getManager();
    $menus=$this->CrearMenu();
    $repository = $em->getRepository('PlanificadorBundle:plan_simbolo');
    $simbolos = $repository->findAll();

    if (isset($id)) 
    {
      $filtro = $em->find('PlanificadorBundle:plan_filtros',$id);
      $valorid = 1;
    }else{
      $filtro = new plan_filtros();
      $valorid = 0;
    }
    $form = $this->createForm(new FiltroEditarFamiliaType($em), $filtro);
    $form->handleRequest($request); 
    if ($form->isSubmitted() ){
      $data = $form->getData();
      $opciones=$data->getChoicesjson();
      $jsonopciones=json_decode($opciones, true);
      $filtro->setJsonFiltro($jsonopciones);
      $filtro->setTipo('F');
      $em->persist($data);
      $em->flush();
      return $this->redirect($this->generateUrl('filtro_gastos_listar'));
    }
  
       return $this->render('PlanificadorBundle:administrador:filtro/filtro_familiar_editar.html.twig',array(
      'form'=>$form->createView(),
      'tipo'=>  $valorid,
      'menus'=>$menus,
      'simbolos'=>$simbolos,
      'filtro'=>$filtro
    ));
  
       
  } 
  public function EditarCapitalAction(Request $request,$id)
  {
      $em = $this->getDoctrine()->getManager();
      $menus=$this->CrearMenu();
      $repository = $this->getDoctrine()->getRepository('PlanificadorBundle:plan_pregunta');
      $preguntas = $repository->findAll();
      $repository = $this->getDoctrine()->getRepository('PlanificadorBundle:plan_respuestas');
      $respuestas = $repository->findAll();
      if (isset($id))
    {
      $filtro = $em->find('PlanificadorBundle:plan_filtros',$id);
      $valorid = 1;
    }else{
      $filtro = new plan_filtros();
      $valorid = 0;
    }
    $form = $this->createForm(new FiltroEditarCapitalType($em), $filtro);
    $form->handleRequest($request); 

    if ($form->isSubmitted() ){
      echo 'guardar';
      die();
    }
    
 
  return $this->render('PlanificadorBundle:administrador:filtro/filtro_capital_editar.html.twig',array(
      'form'=>$form->createView(),
      'menus'=>$menus,
      'filtro'=>$filtro,
      'preguntas'=>$preguntas,
      ));

  }
  
}
