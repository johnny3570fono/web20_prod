<?php

namespace PlanificadorBundle\Controller\admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use PlanificadorBundle\Controller\admin\AdminController;
use PlanificadorBundle\Entity\Plan_Seguimiento;
use PlanificadorBundle\Entity\plan_parametros;

class SeguimientoController extends AdminController
{
    function listarAction( $page )
    {
        $page = (int) $page;
        
        //menu
        $menus = $this->CrearMenu();
        
        //obtenemos los seguimientos
        $seguimientos = $this->getDoctrine()->getRepository('PlanificadorBundle:Plan_Seguimiento');
        
        //Conseguir todos los posts paginados
        $pageSize = 20;
        $paginator = $seguimientos->getPaginatePosts( $pageSize, $page );
        $totalItems = count( $paginator );
        $pagesCount = ceil( $totalItems / $pageSize );
        
        return $this->render('PlanificadorBundle:administrador:seguimiento/seguimiento_listar.html.twig',array(
            "menus" => $menus,
            "seguimientos" => $paginator,
            "totalItems" => $totalItems,
            "pagesCount" => $pagesCount,
            "page" => $page
        ));
    }
    
    function ViewAction( $id=0 ) 
    {
        $menus = $this->CrearMenu();
        
        $id = (int) $id;
        
        //obtenemos los seguimientos
        $seguimiento = $this->getDoctrine()->getRepository('PlanificadorBundle:Plan_Seguimiento')->find( $id );
        
        // dump( $seguimiento );
        // die;
        
        return $this->render('PlanificadorBundle:administrador:seguimiento/seguimiento_ver.html.twig',array(
            'menus' => $menus,
            'seguimiento' => $seguimiento
        ));
    }
    
    function ExportAction() {
        //inicializamos servicio
        $export = $this->get('planificador.exportexcel');
        
        //seguimientos
        $seguimientos = $this->getDoctrine()->getRepository('PlanificadorBundle:Plan_Seguimiento')->findAll();
        
        //exportamos los datos
        $export->export( $seguimientos );
        
        die;
    }
}

