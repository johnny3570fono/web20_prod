<?php

namespace PlanificadorBundle\Controller\admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use PlanificadorBundle\Entity\plan_menu;
use PlanificadorBundle\Form\admin\ConfigType;
use PlanificadorBundle\Controller\AdminController;
use PlanificadorBundle\Form\admin\RespuestasType;
use PlanificadorBundle\Entity\plan_respuestas;



class RespuestasController extends AdminController
{
  
  public function EditarAction(Request $request, $id)
  {
    $em = $this->getDoctrine()->getManager();
  		 $menus=$this->CrearMenu();
    if (isset($id)) 
		{
      $valorid = 1;
			$respuestas = $em->find('PlanificadorBundle:plan_respuestas', $id);
		}else{
			$respuestas = new plan_respuestas();
      $valorid = 0;
		}
    $form = $this->createForm(new RespuestasType(), $respuestas);
		$form->handleRequest($request);	
    
    if ($form->isSubmitted() ){

	    	$data = $form->getData();
		    $em->persist($data);
		    $em->flush();
		    return $this->redirect($this->generateUrl('respuestas_listar'));
    }
    return $this->render('PlanificadorBundle:administrador:respuestas/respuestas.html.twig',array(
      'form'=>$form->createView(),
      'tipo'=>  $valorid,
      'menus'=>$menus
    ));
    	 
	} 
  public function ListarAction()
  {
  
    $repository = $this->getDoctrine()->getRepository('PlanificadorBundle:plan_respuestas');
    $respuestas = $repository->findBy(array('visible'=>1));
    
    $valorid = 0;
    $menus=$this->CrearMenu();

    return $this->render('PlanificadorBundle:administrador:respuestas/respuestas_list.html.twig',array(
      'respuestas'=>$respuestas,
      'tipo'=>  $valorid,
      'menus'=>$menus
      ));
  }
  public function EliminarAction($id)
  {
    $em = $this->getDoctrine()->getEntityManager();
   
      $valorid = 0;
    $repository = $this->getDoctrine()->getRepository('PlanificadorBundle:plan_respuestas');
    $respuestas = $em->find('PlanificadorBundle:plan_respuestas', $id);
    
    if (!$respuestas){
      throw new NotFoundHttpException("No Existe respuesta Seleccionado.");
    }else{
      $respuestas->setVisible(0);
      $em->persist($respuestas);
     
      $em->flush();
  
      return $this->redirect($this->generateUrl('respuestas_listar'));            
    }
      
  }
  
}
