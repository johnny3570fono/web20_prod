<?php

namespace PlanificadorBundle\Controller\admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use PlanificadorBundle\Controller\admin\AdminController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use PlanificadorBundle\Entity\plan_capital;
use PlanificadorBundle\Form\admin\CapitalType;


class CapitalController extends AdminController
{
  public function indexAction(Request $request, $id)
  {
    $em = $this->getDoctrine()->getManager();
    $menus=$this->CrearMenu();
    $error='';
 		
    if (isset($id)) 
		{
      $valorid = 1;
			$capital = $em->find('PlanificadorBundle:plan_capital', $id);
      
		}else{
			$capital = new plan_capital();
      $valorid = 0;
		}

    $form = $this->createForm(new CapitalType(), $capital);
		$form->handleRequest($request);	
    
    if ($form->isSubmitted()){
	    	$data = $form->getData();
        $formula=$data->getValue();
        $valida=$this->Valida($formula);
        if($valida!=false){
        
		    $em->persist($data);
		    $em->flush();
        return $this->redirect($this->generateUrl('capital_listar'));
      }else{
        $error='Fórmula invalida';
      }
    }
    
    return $this->render('PlanificadorBundle:administrador:capital/capital.html.twig',array(
      'form'=>$form->createView(),
      'tipo'=>  $valorid,
      'menus'=>$menus,
      'error'=>$error    
    ));
    	 
	} 
  public function listarAction()
  {
    $menus=$this->CrearMenu();
    $valorid = 0;

    $repository = $this->getDoctrine()->getRepository('PlanificadorBundle:plan_capital');
    $capital = $repository->findAll();
    
    return $this->render('PlanificadorBundle:administrador:capital/capital_listar.html.twig',array(
      'capital'=>$capital,
      'tipo'=> $valorid,
      'menus'=>$menus
    ));
  }
  public function EliminarAction($id)
  {   
    $em = $this->getDoctrine()->getEntityManager();
    $menus=$this->CrearMenu();

    $repository = $this->getDoctrine()->getRepository('PlanificadorBundle:plan_capital');
    $capital = $em->find('PlanificadorBundle:plan_capital', $id);
    
    if (!$capital){
      throw new NotFoundHttpException("No Existe producto Seleccionado.");
    }else{
      $em->persist($capital);
      $em->remove($capital);
      $em->flush();
        
        return $this->redirect($this->generateUrl('capital_listar'));            
    }
      
    return $this->render('PlanificadorBundle:Admin:capital/capital_listar.html.twig',array(
      'capital'=>$capital,
      'menus'=>$menus
    ));
  }
  public function Valida($formula){

    //elimin espaios en blanco

    $formula = preg_replace ( '/\s+/' , '' , $formula ); 
    $formula=strtoupper($formula);


     $cant=strlen($formula);
     $valores=str_split($formula);
     $resultado=$valores;
     for ($i=0; $i < $cant ; $i++) {
       if (preg_match('/[A-Z]{1}/',$valores[$i])) 
        {
          //sustituimos las variables por la unidad para ver si la formula es valida
        
        $resultado[$i]='1';
         
        }
     }
      $formula=implode($resultado);
     
      
      $number = '(?:\d+(?:[.]\d+)?|pi|π)'; // What is a number
      $functions = '(?:sinh?|cosh?|tanh?|abs|acosh?|asinh?|atanh?|exp|log10|deg2rad|rad2deg|sqrt|ceil|floor|round)'; // 
      $operators = '[+\/*\^%-]'; // Allowed math operators
      $regexp = '/^(('.$number.'|'.$functions.'\s*\((?1)+\)|\((?1)+\))(?:'.$operators.'(?2))?)+$/'; // Final regexp, heavily 


    if ( preg_match ( $regexp , $formula )) 
    { 
      $formula = preg_replace('!pi|π!', 'pi()', $formula); 
      eval( '$result = ' . $formula . ';' ); 
    } 
    else 
    { 
      $result = false ; 
    } 
   
    return $result;
    
  }
  public function testAction(){
      $em = $this->getDoctrine()->getManager();
      $datos['A']='50';

      $datos['B']='50';
      var_dump($datos);
     
      $formula='((A+B)*100)/10.1';
      var_dump($formula);
      $valida=$this->Calcula($formula,$datos);
     /// $resultado = $em->getRepository('PlanificadorBundle:plan_preocupacion')->getPreo($formula,$datos);

      die();
  }

  public function Calcula($formula,$datos){

    //elimin espaios en blanco
   
    $formula = preg_replace ( '/\s+/' , '' , $formula ); 
    $formula=strtoupper($formula);

     $cant=strlen($formula);
     $valores=str_split($formula);
     $resultado=$valores;
   
     for ($i=0; $i < $cant ; $i++) {
       if (preg_match('/[A-Z]{1}/',$valores[$i])) 
        { 

          $resultado[$i]=$datos[$valores[$i]];
         
        }
     }
     
      $formula=implode($resultado);
      var_dump($formula);
            
      $number = '(?:\d+(?:[.]\d+)?|pi|π)'; // What is a number
      $functions = '(?:sinh?|cosh?|tanh?|abs|acosh?|asinh?|atanh?|exp|log10|deg2rad|rad2deg|sqrt|ceil|floor|round)'; // 
      $operators = '[+\/*\^%-]'; // Allowed math operators
      $regexp = '/^(('.$number.'|'.$functions.'\s*\((?1)+\)|\((?1)+\))(?:'.$operators.'(?2))?)+$/'; // Final regexp, heavily 


    if ( preg_match ( $regexp , $formula )) 
    { 
      $formula = preg_replace('!pi|π!', 'pi()', $formula); 
      eval( '$result = ' . $formula . ';' ); 
    } 
    else 
    { 
      $result = false ; 
    } 

    
    return $result;
    
  }
}
