<?php

namespace PlanificadorBundle\Controller\admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use PlanificadorBundle\Controller\admin\AdminController;


use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use PlanificadorBundle\Entity\plan_momento;
use PlanificadorBundle\Entity\plan_config;
use PlanificadorBundle\Entity\plan_preocupacion;
use AppBundle\Entity\Family;
use AppBundle\Entity\Category;
use PlanificadorBundle\Form\admin\MomentoType;
use PlanificadorBundle\Form\admin\PreocupacionType;
use PlanificadorBundle\Entity\plan_menu;
use PlanificadorBundle\Form\admin\ConfigType;
use PlanificadorBundle\Entity\plan_productos;
use PlanificadorBundle\Form\ProductType;


class MomentosController extends AdminController
{
  // ++++++++++++++++++++++++++++++++++++++++++++++++++++ ACCIONES DE MOMENTOS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  public function testAction(Request $request, $id)
  {
    $em = $this->getDoctrine()->getManager();
    $menus=$this->CrearMenu();
 		$img = 0;

    if (isset($id)) 
		{
      $valorid = 1;
			$momento = $em->find('PlanificadorBundle:plan_momento', $id);
      $img = $momento->getImagenTipo();
		}else{
			$momento = new plan_momento();
      $valorid = 0;
		}

    $form = $this->createForm(new MomentoType(), $momento);
		$form->handleRequest($request);	
    
    if ($form->isSubmitted() ){
      
       
	    	$data = $form->getData();
        if($data->getImagenTipo() == null)
          $data->setImagenTipo($img);
		    $em->persist($data);
		    $em->flush();
        
		    return $this->redirect($this->generateUrl('momento_listar'));
    }

    return $this->render('PlanificadorBundle:administrador:momentos/momentos.html.twig',array(
      'form'=>$form->createView(),
      'tipo'=>  $valorid,
      'menus'=>$menus,
      'img' => $img,
    ));
    	 
	} 
  public function ListMomentAction()
  {

    $menus=$this->CrearMenu();
    $repository = $this->getDoctrine()->getRepository('PlanificadorBundle:plan_momento');
    $momentos = $repository->findAll();
    $valorid = 0;

    return $this->render('PlanificadorBundle:administrador:momentos/momentos_listar.html.twig',array(
      'momentos'=>$momentos,
      'tipo'=> $valorid,
      'menus'=>$menus
      ));
  }
  public function EliminarAction($id)
  {
    $em = $this->getDoctrine()->getEntityManager();
    $menus=$this->CrearMenu();
    $repository = $this->getDoctrine()->getRepository('PlanificadorBundle:plan_momento');
    $momento = $em->find('PlanificadorBundle:plan_momento', $id);
    
    if (!$momento){
      throw new NotFoundHttpException("No Existe momento Seleccionado.");
    }else{
      $em->persist($momento);
      $em->remove($momento);
      $em->flush();
  
      return $this->redirect($this->generateUrl('momento_listar'));            
    }
      return $this->render('PlanificadorBundle:administrador:momentos/momentos_listar.html.twig',array('momentos'=>$momentos,  'menus'=>$menus));
  }


// ++++++++++++++++++++++++++++++++++++++++++++++++++++ ACCIONES DE PREOCUPACIONES ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 

}
