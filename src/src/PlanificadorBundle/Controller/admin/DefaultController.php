<?php

namespace PlanificadorBundle\Controller\admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;


class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('PlanificadorBundle:Default:index.html.twig', array('name' => $name));
    }
    public function testAction()
    {
        return $this->render('PlanificadorBundle:administrador:menu.html.twig');
    }
}
