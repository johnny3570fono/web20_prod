<?php

namespace PlanificadorBundle\Controller\admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use PlanificadorBundle\Entity\plan_preocupacion;
use PlanificadorBundle\Entity\plan_momento;

use PlanificadorBundle\Form\admin\ParametrosType;
use PlanificadorBundle\Entity\plan_menu;
use PlanificadorBundle\Form\admin\ConfigType;
use PlanificadorBundle\Entity\plan_parametros;
use PlanificadorBundle\Form\admin\OrdenMomentosType;
use PlanificadorBundle\Form\admin\OrdenPreocupacionesType;

use PlanificadorBundle\Controller\admin\AdminController;



class OpcionesController extends AdminController
{
  // ++++++++++++++++++++++++++++++++++++++++++++++++++++Opciones generales ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  public function EditarAction(Request $request, $id)
  {
    $em = $this->getDoctrine()->getManager();
    $menus=$this->CrearMenu();
 		
    if (isset($id)) 
		{
      $valorid = 1;
      $parametros = $em->find('PlanificadorBundle:plan_parametros', $id);
		}else{
      $valorid = 0;
			$parametros = new plan_parametros();
		}

    $form = $this->createForm(new ParametrosType(), $parametros);
		$form->handleRequest($request);	
    
    if ($form->isSubmitted() ){
	    	$data = $form->getData();
		    $em->persist($data);
		    $em->flush();
		    return $this->redirect($this->generateUrl('listar_opciones_generales'));
    }
    
    return $this->render('PlanificadorBundle:administrador:conf_generales/opciones_generales.html.twig',array(
      'form'=>$form->createView(),
      'tipo'=>  $valorid,
      'menus'=>$menus,
      'id'=>$id
    ));
    	 
	} 

  public function OrderbymomentosAction(Request $request)
  {

    $em = $this->getDoctrine()->getEntityManager();
    $menus=$this->CrearMenu();
  /*  $repository = $this->getDoctrine()->getRepository('PlanificadorBundle:plan_momento');

    $momentos = $repository->findAll();*/
    $parametros = new plan_momento();
    $form = $this->createForm(new OrdenMomentosType(),$parametros);
    $form->handleRequest($request);
  
    //obtiene el valores del formulario y los inserta de manera ordenada
    $orden = 1;
    if ($form->isSubmitted()){
      $data = $form->getData()->getOrderby();
     
      if(strlen($data) > 0 ){
        $data = explode(',',$data);

        for ($i=0; $i < count($data) ; $i++) { 
          $val = (int) $data[$i];
            $momentos = $em->find('PlanificadorBundle:plan_momento',$val);
            $momentos->setOrderBy($orden);
            $orden = $orden +1;
        }
        $em->persist($momentos);
        $em->flush();
      }

      $data = $form->getData()->getDesOrderby();
      if(strlen($data) > 0 ){
        $data = explode(',',$data);

        for ($i=0; $i < count($data) ; $i++) { 
          $val = (int) $data[$i];
   
            $momentos = $em->find('PlanificadorBundle:plan_momento',$val);
            $momentos->setOrderBy(0);
        }
        $em->persist($momentos);
        $em->flush();
      }

      return $this->redirect($this->generateUrl('orderbymomentos'));
    }

    $momentos=$em->getRepository('PlanificadorBundle:plan_momento')->findBy(array(), array('orderBy'=>'desc'));
    $momentosordenado= $em->getRepository('PlanificadorBundle:plan_momento')->MomentosOrdenados();
    $momentosdesordenado = $em->getRepository('PlanificadorBundle:plan_momento')->findBy(array('orderBy'=>0));

    $valorid = 0;
      return $this->render('PlanificadorBundle:administrador:conf_generales/orderByMomentos.html.twig',array(
      'form'=>$form->createView(),
      'momentosdesordenado'=>$momentosdesordenado,
      'momentosordernado'=>$momentosordenado,
      'tipo'=> $valorid,
      'menus'=>$menus
      ));
  }
 public function ListarAction()
  {

    $menus=$this->CrearMenu();
    $repository = $this->getDoctrine()->getRepository('PlanificadorBundle:plan_parametros');
    $parametros = $repository->findAll();
    $valorid = 0;

    return $this->render('PlanificadorBundle:administrador:conf_generales/listparametros.html.twig',array(
      'parametros'=>$parametros,
      'tipo'=> $valorid,
      'menus'=>$menus
      ));
  }
  public function OrderbypreocupacionAction(Request $request)
  {

    $em = $this->getDoctrine()->getEntityManager();
    $menus=$this->CrearMenu();
  /*  $repository = $this->getDoctrine()->getRepository('PlanificadorBundle:plan_momento');

    $momentos = $repository->findAll();*/
    $parametros = new plan_preocupacion();
    $form = $this->createForm(new OrdenPreocupacionesType(),$parametros);
    $form->handleRequest($request);
  
    //obtiene el valores del formulario y los inserta de manera ordenada
    $orden = 1;
    if ($form->isSubmitted()){
      $data = $form->getData()->getOrderby();
      var_dump($data);
      if(strlen($data) > 0 ){
        $data = explode(',',$data);

        for ($i=0; $i < count($data) ; $i++) { 
          $val = (int) $data[$i];
            $preocupacion = $em->find('PlanificadorBundle:plan_preocupacion',$val);
            $preocupacion->setOrderBy($orden);
            $orden = $orden +1;
        }
        $em->persist($preocupacion);
        $em->flush();
      }

      $data = $form->getData()->getDesOrderby();
      if(strlen($data) > 0 ){
        $data = explode(',',$data);

        for ($i=0; $i < count($data) ; $i++) { 
          $val = (int) $data[$i];
   
            $preocupacion = $em->find('PlanificadorBundle:plan_preocupacion',$val);
            $preocupacion->setOrderBy(0);
        }
        $em->persist($preocupacion);
        $em->flush();
      }

      return $this->redirect($this->generateUrl('orderbypreocupaciones'));
    }

    $preocupacion=$em->getRepository('PlanificadorBundle:plan_preocupacion')->findBy(array(), array('orderBy'=>'desc'));
    $preocupacionesOrdenadas= $em->getRepository('PlanificadorBundle:plan_preocupacion')->preocupacionesOrdenadas();
    $preocupacionesDesOrdenadas = $em->getRepository('PlanificadorBundle:plan_preocupacion')->findBy(array('orderBy'=>0));
    
    $valorid = 0;
      return $this->render('PlanificadorBundle:administrador:conf_generales/orderByPreocupaciones.html.twig',array(
      'form'=>$form->createView(),
      'preocupacionesDesOrdenadas'=>$preocupacionesDesOrdenadas,
      'preocupacionordenado'=>$preocupacionesOrdenadas,
      'tipo'=> $valorid,
      'menus'=>$menus
      ));
  }


}
