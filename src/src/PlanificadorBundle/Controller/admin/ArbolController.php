<?php

namespace PlanificadorBundle\Controller\admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use PlanificadorBundle\Controller\admin\AdminController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use PlanificadorBundle\Form\admin\ArbolType;
use PlanificadorBundle\Entity\plan_arbol;
use PlanificadorBundle\Entity\plan_pregunta;
use PlanificadorBundle\Entity\plan_preocupacion;
use PlanificadorBundle\Entity\plan_config;
use AppBundle\Entity\Product;
use AppBundle\Entity\plan;
use PlanificadorBundle\Entity\plan_filtros;
use PlanificadorBundle\Entity\plan_capital;




class ArbolController extends AdminController
{
  
  public function EditAction(Request $request, $id)
  {
   
    $em = $this->getDoctrine()->getManager();
    $menus=$this->CrearMenu();


    /* Inicializacion de filtros */
    $arregloDatos = array();
    $arregloDatosresp = array();
    $datosRespuestas=array();
    $arrayvariables=array();
    $arrayvalores=array();
    $datosRespuestas=array();
    $pregunta = $this->getDoctrine()->getRepository('PlanificadorBundle:plan_pregunta')->findAll();  
    $simbolos = $em->getRepository('PlanificadorBundle:plan_simbolo')->findAll();
    $segmentos = $em->getRepository('AppBundle:Segment')->findAll();
    $options = $em->getRepository('PlanificadorBundle:plan_option')->findAll();
    $edocivil = $em->getRepository('PlanificadorBundle:plan_config')->findBy(array('grupo'=>'1'));
    $datosRespuestasTitulog = $em->getRepository('PlanificadorBundle:plan_filtros')->getTitulorespuestasgastos(); 
    $capitales=$em->getRepository('PlanificadorBundle:plan_capital')->findAll();
    
    $totalgastos[] = array('idrespuesta'=>"totalg", 'titulo'=>"Total Gastos");

    $datosRespuestas= array_merge($datosRespuestasTitulog,$totalgastos);

    foreach ($capitales as $capital) {
     

        //elimin espaios en blanco
      $formula = preg_replace ( '/\s+/' , '' , $capital->getValue() ); 
      $formula=strtoupper($formula);
      $cant=strlen($formula);
      $valores=str_split($formula);
         for ($i=0; $i < $cant ; $i++) {
           if (preg_match('/[A-Z]{1}/',$valores[$i])) 
            {
              $arrayvalores['id']=$capital->getId().'-'.$valores[$i];
              $arrayvalores['idcapital']=$capital->getId();
              $arrayvalores['variable']=$valores[$i];

              if (!in_array($arrayvalores['idcapital'].'-'.$arrayvalores['variable'], $arrayvariables)){
                $arrayvariables[]=$arrayvalores;
              }
            }
            
        }
    }
    

    /* fin filtro */

    /*Query para preguntas padres*/
    $preguntasPadres= $em->getRepository('PlanificadorBundle:plan_arbol')->getArbolPreg();
    /**/

   

    foreach($datosRespuestas as $dato)
    {
      $arregloDatos[$dato['idrespuesta']]= $dato['titulo'];
    }
    
      $datosRespuestaAnt = $em->getRepository('PlanificadorBundle:plan_filtros')->getTitulorespuestas(); 
     
      $preguntasfiltro = $em->getRepository('PlanificadorBundle:plan_filtros')->getPreguntas();
      $preguntasCapital= $em->getRepository('PlanificadorBundle:plan_filtros')->getPreguntasCapital();
      
	  
	  
 

      $deco='';
      $filtro = new plan_filtros();
      $filtrog= new plan_filtros(); //filtro de gastos
      $filtrof= new plan_filtros(); //filtro de familia
      $filtroR= new plan_filtros(); //filtro de respuesta anterior
      $filtroC= new plan_filtros(); //filtro de Capital
      if (isset($id)) {
        $valorid = 1;
        $arbol = $em->find('PlanificadorBundle:plan_arbol', $id);
        /*Gauardar Idpreocupaacion por si cambia de rama */
        $IdPreocupacion = $arbol->getIdPreocupacion()->getId();
        //
        $deco = $arbol->getArreglo();
        $filtros = $arbol->getFiltros();

        foreach ($filtros as $tipo) {

            if ($tipo->getTipo()=='B'){
               $filtro=$tipo;
            }
            if($tipo->getTipo()=='G'){
              $filtrog=$tipo;
            }
            if($tipo->getTipo()=='F'){
              $filtrof=$tipo;
            }
            if($tipo->getTipo()=='R'){
              $filtroR=$tipo;
            }
            if($tipo->getTipo()=='C'){
              $filtroC=$tipo;
            }
        }
      
    }else{
      $arbol = new plan_arbol();
      $valorid = 0;
    }
       
    $form = $this->createForm(new ArbolType($em), $arbol);
    $form->handleRequest($request); 
  
    if ($form->isSubmitted()){
      
        $data = $form->getData();
        $val = $data->getChoices();
        
        $data->setArreglo(json_decode($val));
       

       
        
        $PregPadre = $data->getPregPadre();
        $Padre = $em->getRepository('PlanificadorBundle:plan_arbol')->findOneBy(array('id'=>$PregPadre));
        $data->setPadre($Padre);
        $em->persist($data);
        $Id = $data->getId();
        
        if(isset($IdPreocupacion)){
          if($data->getIdPreocupacion()->getId()!=$IdPreocupacion)
          {
            $CambioRama= $em->getRepository('PlanificadorBundle:plan_arbol')->getRama($IdPreocupacion,$Id);
            foreach ($CambioRama as $Dato) {
              $rama = $em->find('PlanificadorBundle:plan_arbol', $Dato['ID']);
              if($rama){
                $rama->setIdPreocupacion($data->getIdPreocupacion());
                $em->persist($rama);
              }
            }
            
          }
        }
        $persistfiltros=$this->PersistFiltros($em,$data,$arbol,$filtro,$filtrog,$filtrof,$filtroR,$filtroC);
       

        $em->flush();
       

        return $this->redirect($this->generateUrl('arbol_mostrar'));
    }

    $repository = $this->getDoctrine()->getRepository('AppBundle:Product');
    $productos = $repository->findAll();

    $repository = $this->getDoctrine()->getRepository('PlanificadorBundle:plan_producto');
    $prodp = $repository->findAll();

    $prodplani = [];
    foreach ($prodp as $key => $r) {
       $prodplani[$key] = [
          'nombre_pro' => $r->getNombre(),
          'id' => $r->getProductoWeb()->getId(),  
       ];
    };
   

    $padre = null;
    if($arbol->getPadre() != null){
      $padre = $arbol->getPadre()->getId();
    }
  
    $arr = '';
    if($valorid == 1){
      $arr = $arbol->getArreglo();
      $arbol = [
        'id' => $arbol->getId(),
        'padre' => $padre,
        'preg' => $arbol->getPreghijo()->getId(),  
        'tipo' => $arbol->getTipo()->getId(),
        'preo'=>$arbol->getIdPreocupacion()->getId(),
      ];
    }

    $data = [];
    foreach ($productos as $key => $r){
      $data[$key] = [
          'nombre_pro' => $r->getName(),
          'id' => $r->getId(),  
       ];
       foreach( $r->getPlans() as $key2 => $q){
        $data[$key][$key2] = [
            'nombre_plan' => $q->getName(),
            'idplan' => $q->getId(),
          ];
        }
    }

      return $this->render('PlanificadorBundle:administrador:arbol/arbol_form.html.twig',array(
      'form'=>$form->createView(),
      'tipo'=>  $valorid,
      'menus'=>$menus,
      'productos' => $data,
      'prodplani' =>$prodplani,
      'pregunta'=> $pregunta,
      'arbol'=> $arbol,
      'deco'=> $deco,
      'arr'=> $arr,
      'simbolos'=>$simbolos,
      'segmentos'=>$segmentos,
      'options'=>$options,  
      'filtro'=>$filtro,
      'gastos'=>$datosRespuestas,
      'filtrog'=>$filtrog,
      'filtrof'=>$filtrof,
      'filtroC'=>$filtroC,
      'arrayvariables'=>$arrayvariables,
      'capitales'=>$capitales,
      'edocivil'=>$edocivil,
      'anterioresResp'=>$datosRespuestaAnt,
      'filtroR'=>$filtroR,
      'preguntasfiltro'=>$preguntasfiltro,
      'PreguntasPadres'=>$preguntasPadres,
      'preguntasCapital'=>$preguntasCapital
 

     
    ));

	} 
  public function listarAction()
  {
    $menus=$this->CrearMenu();
    $repository = $this->getDoctrine()->getRepository('PlanificadorBundle:plan_arbol');
    $arboles = $repository->findAll();
	
    $valorid = 0;

    $repository = $this->getDoctrine()->getRepository('AppBundle:Product');
    $productos = $repository->findAll();
   
    return $this->render('PlanificadorBundle:administrador:arbol/arbol_list.html.twig',array(
      'arboles'=>$arboles,
      'tipo'=>  $valorid ,
      'menus'=>$menus,
      
      ));
      
  }
public function EliminarAction($id)
  {
    $em = $this->getDoctrine()->getEntityManager();

    $repository = $this->getDoctrine()->getRepository('PlanificadorBundle:plan_arbol');
    $arbol = $em->find('PlanificadorBundle:plan_arbol', $id);
    $valorid = 0;
    
    if (!$arbol){
      throw new NotFoundHttpException("No Existe arbol Seleccionado.");
    }else{
      $em->persist($arbol);
      $em->remove($arbol);
      $em->flush();
  
      return $this->redirect($this->generateUrl('arbol_mostrar'));            
    }
    return $this->render('PlanificadorBundle:administrador:arbol/arbol_listar.html.twig',array(
      'arboles'=>$arboles,
      'tipo'=>  $valorid ,
      'menus'=>$menus,
      ));
  }


  public function MostrarAction()
  { 

    $em = $this->getDoctrine()->getEntityManager();
    $menus=$this->CrearMenu();
    $repository = $this->getDoctrine()->getRepository('AppBundle:Product');
    $productos = $repository->findAll();

    // COMIENZO ARBOL
	//$arbol = $em->getRepository('PlanificadorBundle:plan_arbol');
	
    $arbolito = $em->getRepository('PlanificadorBundle:plan_arbol')
                ->getPreoG();
	
	//print_r($arbolito);
    foreach ($arbolito as $key => $q) {
      $id = (int) $q['PREO'];
      $resultado[$key] = [
          'arbolito' => $em->getRepository('PlanificadorBundle:plan_arbol')->getArbol($id),
          //'respuesta' => $em->getRepository('PlanificadorBundle:plan_arbol')->getArbol($id)->getChoicesjsonrespuesta(),
		  'preocupacion' => $em->find('PlanificadorBundle:plan_preocupacion', $id),
          'pregene' => $em->getRepository('PlanificadorBundle:plan_arbol')->getPreg($id),

      ];
	  
	
    }
	
    $cont = count($resultado)-1;
    //FIN DE ARBOL 
    
    //PRODUCTOS
    $data = [];
    foreach ($productos as $key => $r){
      $data[$key] = [
          'nombre_pro' => $r->getName(),
          'id' => $r->getId(),
       ];
       foreach( $r->getPlans() as $key2 => $q){
        $data[$key][$key2] = 
          [
            'nombre_plan' => $q->getName(),
            'idplan' => $q->getId(),
          ];
        }
    }
    //FIN DE PRODUCTOS
    $arbol = '';
    $bloqueadas=$em->getRepository('PlanificadorBundle:plan_arbol')->getCountHijos();
    foreach ($bloqueadas as $bloqueada) {
      $block[]=(int) $bloqueada['PADRE_ID'];
    }
 

       return $this->render('PlanificadorBundle:administrador:arbol/arbol.html.twig',array(
      'menus'=>$menus,
      'arbolito' =>$arbolito,
      'productos' => $data,
      'resultado'=> $resultado,
      'cont' => $cont,
      'bloqueadas'=>$block
    ));

    
  }
  public function PersistFiltros($em,$data,$arbol,$filtro,$filtrog,$filtrof,$filtroR,$filtroC)
  {
        
        $jsonopciones = $data->getChoicesjson();
        $jsonopciones=json_decode($jsonopciones, true);
        var_dump($jsonopciones);

        if(count($jsonopciones)!=0)
        {
          $filtro->setArbol($arbol);
          $filtro->setJsonFiltro($jsonopciones);
          $filtro->setTipo('B');
          $em->persist($filtro);
        }else{
            $em->remove($filtro);
        }
      
        /**/
       
        $jsonopciones = $data->getChoicesjsongastos();
        $jsonopciones=json_decode($jsonopciones, true);
         if(count($jsonopciones)!=0)
        {

          $filtrog->setArbol($arbol);
          $filtrog->setJsonFiltro($jsonopciones);
          $filtrog->setTipo('G');
          $em->persist($filtrog);
        }else{
          $em->remove($filtrog);
        }

        /***/

       
        $jsonopciones = $data->getChoicesjsonfamilia();
        $jsonopciones=json_decode($jsonopciones, true);
        if(count($jsonopciones)!=0)
        {
          $filtrof->setArbol($arbol);
          $filtrof->setJsonFiltro($jsonopciones);
          $filtrof->setTipo('F');
          $em->persist($filtrof);
        }else{
          $em->remove($filtrof);
        }

 
        /**/
        
        $jsonopciones = $data->getChoicesjsonrespuesta();
        $jsonopciones=json_decode($jsonopciones, true);
         if(count($jsonopciones)!=0)
        {
          $filtroR->setArbol($arbol);
          $filtroR->setJsonFiltro($jsonopciones);
          $filtroR->setTipo('R');
          $em->persist($filtroR);
        }else{
          $em->remove($filtroR);
        }

        /**/
       
        $jsonopciones = $data->getChoicesjsonCapital();
        $jsonopciones=json_decode($jsonopciones, true);
        $jsontotal = $data->getJsonTotal();
        $jsontotal=json_decode($jsontotal, true);
       
        
        if( count($jsonopciones)!=0 &&  (count($jsontotal)!=0))
        {
          $filtroC->setArbol($arbol);
          $filtroC->setJsonTotal($jsontotal);
          $filtroC->setJsonFiltro($jsonopciones);
          $jsonopciones = $data->getChoicesjsonCapital();
          $jsonopciones=json_decode($jsonopciones, true);
          $filtroC->setTipo('C');
          $em->persist($filtroC);
        }else{
          $em->remove($filtroC);
        }
       
      return;
  }



}
