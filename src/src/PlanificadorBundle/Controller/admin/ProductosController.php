<?php

namespace PlanificadorBundle\Controller\admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use PlanificadorBundle\Controller\admin\AdminController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use PlanificadorBundle\Entity\plan_producto;
use PlanificadorBundle\Form\admin\ProductoType;
use PlanificadorBundle\Entity\plan_cobertura;



class ProductosController extends AdminController
{
  public function indexAction(Request $request, $id, $web)
  {

    $em = $this->getDoctrine()->getManager();
    $menus=$this->CrearMenu();
 		$img = 0;
    $coberturas = '';
    if (isset($id)) 
		{
      $valorid = 1;
			$producto = $em->find('PlanificadorBundle:plan_producto', $id);
      $coberturas = $producto->getCobertura();
      $img = $producto->getImagenTipo();
		}else{


			$producto = new plan_producto();
      $valorid = 0;
      if(isset($web)){ 
        
          $productoweb = $em->find('AppBundle:Product', $web);
          $producto->setNombre($productoweb->getName()); 
          $producto->setDescripcion($productoweb->getDescription());
          $producto->setCubre($productoweb->getCoverage());
          $producto->setNoCubre($productoweb->getCoverageNot());
          $producto->setEstado(1);
          $img =$productoweb->getImage();
          
          $producto->setImagenTipo($productoweb->getImageFile());
          $producto->setProductoweb($productoweb);
          foreach ($productoweb->getSegments() as $index) {
            $producto->addSegmentosplani($index);
          }
          
      }
		}


    $form = $this->createForm(new ProductoType(), $producto);
		$form->handleRequest($request);	
    
    if ($form->isSubmitted()){

	    	$data = $form->getData();
        $cober = json_decode($data->getChoicesjson());
        $data->setCobertura($cober); 
        if(isset($web)){

          /* $origen ='/img/product/'.$img;
        echo $origen;
        $destino = __DIR__.'/img/planificador/productos/';
       copy($origen, $destino."foto_por_defecto.jpg");*/
     }
       if($data->getImagenTipo() == null)
          $data->setImagenTipo($img);

		    $em->persist($data);
		    $em->flush();

		    return $this->redirect($this->generateUrl('productos_listar'));
    }
    
    return $this->render('PlanificadorBundle:administrador:productos/producto.html.twig',array(
      'form'=>$form->createView(),
      'tipo'=>  $valorid,
      'menus'=>$menus,
      'img' => $img,
      'coberturas' => $coberturas   
    ));
    	 
	} 
  public function listarAction()
  {
    $menus=$this->CrearMenu();
   

   // $repository = $this->getDoctrine()->getRepository('PlanificadorBundle:plan_producto');
    //$productos = $repository->findAll();
    $em = $this->getDoctrine()->getEntityManager();

    $productos = $em->getRepository('PlanificadorBundle:plan_producto')
                  ->productosweb20();
    
    return $this->render('PlanificadorBundle:administrador:productos/producto_listar.html.twig',array(
      'productos'=>$productos,
      'menus'=>$menus,
      'cobertura'=>'hi'
    ));
  }
  public function EliminarAction($id)
  {   
    $em = $this->getDoctrine()->getEntityManager();
    $menus=$this->CrearMenu();

    $repository = $this->getDoctrine()->getRepository('PlanificadorBundle:plan_producto');
    $productos = $em->find('PlanificadorBundle:plan_producto', $id);
    
    if (!$productos){
      throw new NotFoundHttpException("No Existe producto Seleccionado.");
    }else{
      $em->persist($productos);
      $em->remove($productos);
      $em->flush();
        
        return $this->redirect($this->generateUrl('productos_listar'));            
    }
      
    return $this->render('PlanificadorBundle:Admin:productos/producto_listar.html.twig',array(
      'productos'=>$productos,
      'menus'=>$menus
    ));
  }
}
