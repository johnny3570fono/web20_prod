<?php

namespace PlanificadorBundle\Controller\admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use PlanificadorBundle\Entity\plan_preocupacion;
use PlanificadorBundle\Form\admin\PreocupacionesType;
use PlanificadorBundle\Entity\plan_menu;
use PlanificadorBundle\Form\admin\ConfigType;
use PlanificadorBundle\Controller\admin\AdminController;



class PreocupacionesController extends AdminController
{
  // ++++++++++++++++++++++++++++++++++++++++++++++++++++ ACCIONES DE MOMENTOS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  public function testAction(Request $request, $id)
  {
    $em = $this->getDoctrine()->getManager();
    $menus=$this->CrearMenu();
 		$img = 0;
    if (isset($id)) 
		{
      $valorid = 1;
			$preocupacion = $em->find('PlanificadorBundle:plan_preocupacion', $id);
      $img = $preocupacion->getImagenTipo();
		}else{
			$preocupacion = new plan_preocupacion();
      $valorid = 0;
		}

    $form = $this->createForm(new PreocupacionesType(), $preocupacion);
		$form->handleRequest($request);	
    
    if ($form->isSubmitted() ){
        //$form->getData()->setOrderBy(0);
	    	$data = $form->getData();
        if($data->getImagenTipo() == null)
            $data->setImagenTipo($img);
		    $em->persist($data);
		    $em->flush();

		    return $this->redirect($this->generateUrl('preocupacion_listar'));
    }

    return $this->render('PlanificadorBundle:administrador:preocupaciones/preocupaciones.html.twig',array(
      'form'=>$form->createView(),
      'tipo'=>  $valorid,
      'menus'=>$menus,
      'img' => $img,
    ));
    	 
	} 
  public function ListPreocupacionAction()
  {
    $menus=$this->CrearMenu();
    $repository = $this->getDoctrine()->getRepository('PlanificadorBundle:plan_preocupacion');
    $preocupaciones = $repository->findAll();
    $valorid = 0;

    return $this->render('PlanificadorBundle:administrador:preocupaciones/preocupaciones_listar.html.twig',array(
      'preocupacion'=>$preocupaciones,
      'tipo'=>  $valorid ,
      'menus'=>$menus
      ));
  }
  public function EliminarAction($id)
  {
    $em = $this->getDoctrine()->getEntityManager();

    $repository = $this->getDoctrine()->getRepository('PlanificadorBundle:plan_preocupacion');
    $preocupacion = $em->find('PlanificadorBundle:plan_preocupacion', $id);
    
    if (!$preocupacion){
      throw new NotFoundHttpException("No Existe momento Seleccionado.");
    }else{
      $em->persist($preocupacion);
    
      $em->remove($preocupacion);
      $em->flush();
  
      return $this->redirect($this->generateUrl('preocupacion_listar'));            
    }
      return $this->render('PlanificadorBundle:administrador:preocupaciones/preocupaciones_listar.html.twig',array('preocupacion'=>$preocupacion,  'menus'=>$preocupacion));
  }
	

// ++++++++++++++++++++++++++++++++++++++++++++++++++++ ACCIONES DE PREOCUPACIONES ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 

}
