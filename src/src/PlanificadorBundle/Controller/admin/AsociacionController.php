<?php

namespace PlanificadorBundle\Controller\admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use PlanificadorBundle\Controller\admin\AdminController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use PlanificadorBundle\Entity\plan_mom_preo;
use PlanificadorBundle\Entity\plan_momento;
use PlanificadorBundle\Entity\plan_preocupacion;
use PlanificadorBundle\Entity\plan_pregunta;
use PlanificadorBundle\Form\admin\AsociacionType;



class AsociacionController extends AdminController
{
  public function indexAction(Request $request, $id)
  {
    $em = $this->getDoctrine()->getManager();
    $menus=$this->CrearMenu();
    $preo = '';
    $momento='';
    if (isset($id)) 
    {
      $valorid = 1;
      $asociacion = $em->find('PlanificadorBundle:plan_mom_preo', $id);
      $preo = $asociacion->getPreocupacion();
      $momento = $asociacion->getidMomento();
      //echo $momento->getId();
    }else{
      $asociacion = new plan_mom_preo();
      $valorid = 0;
    }

     $preocupaciones = $em->getRepository('PlanificadorBundle:plan_preocupacion')
                      ->preocupaciones();
 		 
    $form = $this->createForm(new AsociacionType(), $asociacion);
		$form->handleRequest($request);

    if ($form->isSubmitted()){
	    	
        $data = $form->getData();
        if($momento){
          $data->setIdMomento($momento);
        }
         
		    $em->persist($data);
		    $em->flush();

		    return $this->redirect($this->generateUrl('asociacion_listar'));
    }
    
    
    return $this->render('PlanificadorBundle:administrador:asociacion/asociacion.html.twig',array(
      'form'=>$form->createView(),
      'menus'=>$menus,
      'tipo' => $valorid,
      'preocupaciones' => $preocupaciones,
      'preo' => $preo,
      'momento'=>$momento   
    ));
    	 
	} 
  public function listarAction()
  {
    $menus=$this->CrearMenu();
    $em = $this->getDoctrine()->getManager();
    $valorid = 0;
    $arregloPresentacion=array();
    $arraydatospreocupaciones=array();
    $arraypreocupaciones=array();
    $repository = $this->getDoctrine()->getRepository('PlanificadorBundle:plan_mom_preo');
    $momentos = $repository->findAll();
    foreach ($momentos as $momento) {
        
        if( $momento->getIdMomento() != null ) {
            $arregloDatos['IdAsociacion'] = $momento->getId();
            $arregloDatos['Descripcion'] = $momento->getIdMomento()->getDescripcion();
            
               
            $preocupaciones = explode(",", $momento->getPreocupacion());
            unset($arraydatospreocupaciones);
            unset($arraypreocupaciones);
            $arraydatospreocupaciones=array();
            foreach ($preocupaciones as $id) {
                if( $id != '' ) {
                    $preocupacion = $em->find('PlanificadorBundle:plan_preocupacion', $id);
                    if( $preocupacion ) {
                        $arraydatospreocupaciones['id'] = $preocupacion->getId();
                        $arraydatospreocupaciones['descripcion'] = $preocupacion->getDescripcion();
                    }
                    $arraypreocupaciones[]=$arraydatospreocupaciones;
                }
            }

            $arregloDatos['Preocupaciones']=$arraypreocupaciones;
            $arregloPresentacion[] = $arregloDatos;
        }
    }
    
    return $this->render('PlanificadorBundle:administrador:asociacion/asociacion_listar.html.twig',array(
      'momentos'=>$momentos,
      'tipo'=> $valorid,
      'menus'=>$menus,
      'arregloPresentacion'=>$arregloPresentacion
    ));
  }
  public function EliminarAction($id)
  {   
    $em = $this->getDoctrine()->getEntityManager();
    $menus=$this->CrearMenu();

    $repository = $this->getDoctrine()->getRepository('PlanificadorBundle:plan_mom_preo');
    $capital = $em->find('PlanificadorBundle:plan_mom_preo', $id);
    
    if (!$capital){
      throw new NotFoundHttpException("No Existe producto Seleccionado.");
    }else{
      $em->persist($capital);
      $em->remove($capital);
      $em->flush();
        
        return $this->redirect($this->generateUrl('asociacion_listar'));            
    }
      
    return $this->render('PlanificadorBundle:Admin:asociacion/asociacion_listar.html.twig',array(
      'capital'=>$capital,
      'menus'=>$menus
    ));
  }
}
