<?php

namespace PlanificadorBundle\Controller\admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use PlanificadorBundle\Entity\plan_momento;
use PlanificadorBundle\Entity\plan_config;
use PlanificadorBundle\Entity\plan_preocupacion;
use AppBundle\Entity\Family;
use AppBundle\Entity\Category;
use PlanificadorBundle\Form\admin\MomentoType;
use PlanificadorBundle\Entity\plan_menu;
use PlanificadorBundle\Form\admin\ConfigType;
use PlanificadorBundle\Entity\plan_productos;
use PlanificadorBundle\Form\admin\ProductType;


class AdminController extends Controller
{
  // ++++++++++++++++++++++++++++++++++++++++++++++++++++ ACCIONES DE MOMENTOS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  
	public function CrearMenu()
  {
    $em = $this->getDoctrine()->getManager();
    /* Incluir menú */
    $menuPrincipal= $em->getRepository('PlanificadorBundle:plan_menu')->Menuprincipal();

    foreach ($menuPrincipal as $menupadre){
      $Menuhijos=array();
      $MenuHijos= $em->getRepository('PlanificadorBundle:plan_menu')->MenuHijos($menupadre->getId());
      $Menudatospadre['IdPadre']=$menupadre->getId();
      $Menudatospadre['IdPadreNombre']=$menupadre->getNombre();
      $Menudatospadre['IdPadreRouting']=$menupadre->getRouting();
        
      if(isset ($MenuHijos)){
        foreach ($MenuHijos as $MenuHijo) {
            $Menudatos['IdHijo']=$MenuHijo->getId();
            $Menudatos['IdHijoRouting']=$MenuHijo->getRouting();
            $Menudatos['IdHijoParent']=$MenuHijo->getParentId();
            $Menudatos['IdHijoNombre']=$MenuHijo->getNombre();
            $Menuhijos[]=$Menudatos;
        }
        $Menudatospadre['hijos']=$Menuhijos;   
      }
    $menus[]=$Menudatospadre;

    }
    
    return $menus;
  }

// ++++++++++++++++++++++++++++++++++++++++++++++++++++ ACCIONES DE PREOCUPACIONES ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 

}
