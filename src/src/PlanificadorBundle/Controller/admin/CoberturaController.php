<?php

namespace PlanificadorBundle\Controller\admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use PlanificadorBundle\Controller\admin\AdminController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use PlanificadorBundle\Entity\plan_cobertura;
use PlanificadorBundle\Form\admin\CoberturaType;


class CoberturaController extends AdminController
{
  public function indexAction(Request $request, $id)
  {
    $em = $this->getDoctrine()->getManager();
    $menus=$this->CrearMenu();
 		$img = 0;
    if (isset($id)) 
		{
      $valorid = 1;
			$cobertura = $em->find('PlanificadorBundle:plan_cobertura', $id);
      $img = $cobertura->getImagenTipo();
		}else{
			$cobertura = new plan_cobertura();
      $valorid = 0;
		}

    $form = $this->createForm(new CoberturaType(), $cobertura);
		$form->handleRequest($request);	
    
    if ($form->isSubmitted()){
	    	$data = $form->getData();
        
        if($data->getImagenTipo() == null)
          $data->setImagenTipo($img);
		    $em->persist($data);
		    $em->flush();

		    return $this->redirect($this->generateUrl('cobertura_listar'));
    }
    
    return $this->render('PlanificadorBundle:administrador:cobertura/cobertura.html.twig',array(
      'form'=>$form->createView(),
      'tipo'=>  $valorid,
      'menus'=>$menus,
      'img' => $img      
    ));
    	 
	} 
  public function listarAction()
  {
    $menus=$this->CrearMenu();
    $valorid = 0;

    $repository = $this->getDoctrine()->getRepository('PlanificadorBundle:plan_cobertura');
    $cobertura = $repository->findAll();
    
    return $this->render('PlanificadorBundle:administrador:cobertura/cobertura_listar.html.twig',array(
      'cobertura'=>$cobertura,
      'tipo'=> $valorid,
      'menus'=>$menus
    ));
  }
  public function EliminarAction($id)
  {   
    $em = $this->getDoctrine()->getEntityManager();
    $menus=$this->CrearMenu();

    $repository = $this->getDoctrine()->getRepository('PlanificadorBundle:plan_cobertura');
    $cobertura = $em->find('PlanificadorBundle:plan_cobertura', $id);
    
    if (!$cobertura){
      throw new NotFoundHttpException("No Existe cobertura Seleccionado.");
    }else{
      $em->persist($cobertura);
      $em->remove($cobertura);
      $em->flush();
        
        return $this->redirect($this->generateUrl('cobertura_listar'));            
    }
      
    return $this->render('PlanificadorBundle:Admin:cobertura/cobertura_listar.html.twig',array(
      'cobertura'=>$cobertura,
      'menus'=>$menus
    ));
  }
}
