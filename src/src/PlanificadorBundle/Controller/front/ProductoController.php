  <?php

  namespace PlanificadorBundle\Controller\front;

  use Symfony\Component\HttpFoundation\Request;
  use Symfony\Component\HttpFoundation\Session\Session;

  use PlanificadorBundle\Form\front\FrontCheckedType;
  use PlanificadorBundle\Entity\front_momentos;
  use PlanificadorBundle\Entity\front_preocupaciones;



  class ProductoController extends FrontController
  {
    
    public function IndexAction()
    {
      return $this->render('PlanificadorBundle:front:inicio.html.twig');
  	}

    public function momentosAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();
      $repository = $em->getRepository('PlanificadorBundle:plan_momento');
      $momentos = $repository->findAll(array('orden' => 'ASC'));
      $parametros = $em->find('PlanificadorBundle:plan_parametros',1);
      $menus=$this->CrearMenuAction();
      $mom = new front_Momentos();
      $form = $this->createForm(new FrontCheckedType(), $mom);
      $form->handleRequest($request); 

     if ($form->isSubmitted()){

        $data = $form->getData()->getChecked();
        $momentos = json_decode($data , true);
        $session = $request->getSession();
        $session->set('momentos',$momentos);

          return $this->redirect($this->generateUrl('front_preocupacion'));
      }
       
       //die();
    
      
      return $this->render('PlanificadorBundle:front:momentos.html.twig', array(
        'form' => $form->createView(),
        'momentos'=>$momentos,
        'parametros'=>$parametros,
        'menus'=>$menus,
      ));
        
    }

     public function PreocupacionesAction(Request $request)
    {
     
      $em = $this->getDoctrine()->getManager();
      $parametros = $em->find('PlanificadorBundle:plan_parametros',2);
      $menus=$this->CrearMenuAction();
      $session = $request->getSession();
      $momentos=($session->get('momentos'));

      foreach ($momentos as $momento) {
        $arraymomentos[]=$momento['id'];
      }
     
      $arraypreocupaciones=array();
      $preocupaciones= $em->getRepository('PlanificadorBundle:plan_momento')->BuscarPreocupaciones($arraymomentos);
     
      foreach ($preocupaciones as $preocupacion) {
        $Idspreocupacion=explode(',', $preocupacion['preocupacion']);
        foreach ($Idspreocupacion as $value) {
          if($value!='' or array_key_exists($value, $arraypreocupaciones)){
            
            $arraypreocupaciones[$value]=$value;
          }
        }
        
      }
      $preocupaciones= $em->getRepository('PlanificadorBundle:plan_momento')->BuscarPreocupacionesEntity($arraypreocupaciones);
      $preo = new front_Preocupaciones();
      $form = $this->createForm(new FrontCheckedType(), $preo);
      $form->handleRequest($request); 

     if ($form->isSubmitted()){
        
        $data = $form->getData()->getChecked();
        $preocupaciones = json_decode($data , true);
        $session = $request->getSession();
        $session->set('preocupaciones',$preocupaciones);
        
        return $this->redirect($this->generateUrl('front_preguntas'));
      }
       
      
      return $this->render('PlanificadorBundle:front:preocupacion.html.twig',array(
        'form' => $form->createView(),
        'preocupaciones'=>  $preocupaciones,
        'parametros'=>  $parametros,
        'menus'=>$menus,
      ));

    }
   
    

  }
