<?php

namespace PlanificadorBundle\Controller\front;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use PlanificadorBundle\Entity\front_preocupaciones;
use PlanificadorBundle\Form\front\FrontCheckedType;
use PlanificadorBundle\Form\front\GenericasType;
use PlanificadorBundle\Helper\Utils;

class PreguntasController extends FrontController
{

    public function IndexAction(Request $request){
		$em = $this->getDoctrine()->getManager();
		$session = $request->getSession();
		/* inicializa algunas variables */
		$preocupaciones=($session->get('preocupaciones'));
		$arrayPregG = [];
		$merg = [];
		$arrayfinal=[];
		$arrayGenericas=array();
		$arrayEspecificas=array();
		$edades=null;
		$canthijos=null;
		$edocivils=null;
		$redirect=null;
		$redirect_t=null;
		$final_id_preg=null;
		$contador_prueba=0;
		/* fin inicializazion */
		$menus=$this->CrearMenuAction();
		/* carga de preguntas en arreglo $arrayPregG y setea la session */
		$cont = 0;
		
		$arrayPregG = array();
            
		if ( $preocupaciones != null ) {
			$arbol = $em->getRepository('PlanificadorBundle:plan_arbol')->getArbolPlano_in($preocupaciones);
            
			if( count($arbol) != 0) {
				foreach( $arbol as $ar ) {
					$arrayPregG[] = $ar;
				}
			}
            
			$session->set('arrayPregunta',$arrayPregG);
		}
		
		if( count($arrayPregG) < 1 )
			return $this->redirect($this->generateUrl('front_error'));
		

		$arrayPregG = $session->get('arrayPregunta');
        
	   /* fin de la carga de preguntas */

        foreach($arrayPregG as $key){
            if ( $key['PREGTIPO'] == '1' ) {
                //preguntas genericas
                $arrayGenericas[] = $key;
            } else {
                /* preguntas especificas y finales*/
                $arrayEspecificas[] = $key;
            }
        }

		//*ordenando las preguntas genericas por campo orderby "si existe" *//
		usort($arrayGenericas, function($a, $b) {
            return $a['ORDERBY'] - $b['ORDERBY'];
		});

		$arrayPregOrdenadas = array_merge( $arrayGenericas, $arrayEspecificas );

		
        $pregtipo = array_column( $arrayPregOrdenadas, 'PREGTIPO', array_shift(array_keys($arrayPregOrdenadas)) );
		//SUPER 14-12-2017
		// foreach ($arrayPregOrdenadas as $clave => $fila) {
			// $pregtipo[$clave] = $fila['PREGTIPO'];
		// }
        
		array_multisort($pregtipo, SORT_ASC, $arrayPregOrdenadas);
		//FIN SUPER
        
		$session->set( 'arrayPregOrd', $arrayPregOrdenadas );
		$arrayPregG = $session->get('arrayPregOrd');
		
        // carga las respuestas que existen en la session y si es que viene del boton volver
        $resp_sess = $session->get('respuestas');
        $sess_volver = $session->get('volver');
        
        // obtiene la pregunta actual del flujo		
		$arbol = $arrayPregG[$session->get('cantidad')];
		$idpregunta = $arbol['PREGHIJO'];
	
		/// SUPER 
        /*
		$arrayauxiliar = array();
		if(is_array($arrayPregOrdenadas)){ 
		  foreach($arrayPregOrdenadas as $auxiliar){
			if(!isset($arrayauxiliar[$auxiliar["PREGHIJO"]]))
			  $arrayauxiliar[$auxiliar["PREGHIJO"]] = $auxiliar;
			   $arrayauxiliar[$auxiliar["PREGHIJO"]]["IDS"][] = $auxiliar["ID"];
		  }
		}
			*/
		//$nextarray = (current(array_slice($arrayauxiliar, array_search($idpregunta, array_keys($arrayauxiliar)) + 1, 1)));
		
		// FIN SUPER

		$pregunta = $em->find('PlanificadorBundle:plan_pregunta', $idpregunta);
	//	$pregunta = $em->getRepository('PlanificadorBundle:plan_pregunta')->getPlanPregunta_id($idpregunta);

		/* pregunta si es que en la session ya se realizo la pregunta y si es que no viene de un volver, si es asi salta a la siguiente (con esto no se duplican las preguntas) */
        $array_idpreg = array();	  

        if ( isset($resp_sess) && $sess_volver == 0 ) {
            foreach ( $resp_sess as $key => $value ) {
                array_push( $array_idpreg, $value[$key]['idPreg'] );
            }
        }
	   
		if ( in_array( $pregunta->getId(), $array_idpreg ) ) {
			$new = $session->get('cantidad') + 1;
			//$session->set('cantidad',$new);
			$contador_prueba=+1;
			$redirect_t=1; 
		}
		
		$idarbol = $arbol['ID'];

		/*validando filtros retorna suma */

		$suma = $this->Filtro( $idarbol, $em, $arrayPregG, $session );
		
		$max = count($arrayPregG)-1;
		// preghijo corresponde a el id del arbol, ID corresponde el id de la pregunta en arrayPregOrdenadas
		
		//si la pregunta no aprobo los filtros o es tipo final y viene de continuar(no presiono volver) incrementa en uno la pregunta  
		 if( ($suma < 5 || $pregunta->getPregtipo()->getId() == 3) && $sess_volver == 0){
		  if($max > $session->get('cantidad')){
			$new = $session->get('cantidad') + 1;
			//$session->set('cantidad',$new);
			
			 $redirect=1;
			
		  }
		  
		  if($pregunta->getPregtipo()->getId()==3) { //Es pregunta final
			$arrayfinal[]= $pregunta->getId();
			if($max==$session->get('cantidad')){
			
				$redirect=2;
			}else{
			  if($cont< $max){
				$contador_prueba=+1;
				$cont = $session->get('cantidad')+1; 
				$redirect=1;
			  }
			}
		  }  
		}
	  
		//si la pregunta no aprobo los filtros o es tipo final y viene de volver decrementa en uno la pregunta     
		if( ($suma < 5 || $pregunta->getPregtipo()->getId() == 3) && $sess_volver == 1){
		  if($session->get('cantidad') >= 0){
			$new = $session->get('cantidad') - 1;
			
		  }
			
			$redirect=1;
			
		}
		
		// si la pregunta aprobo los filtros o no es tipo 3 busca en las respuestas existentes si no existe es porque no se realizo, asi que retrocede en una   
		if($sess_volver == 1){
		  $existe = 0;
		  foreach ($resp_sess as $key => $value){
			if($value[$key]['idarbol'] == $idarbol){
			  $existe = 1;
			}
		  }
		  if($existe == 0){
			$new = $session->get('cantidad') - 1;
			$indiceOld = $session->get('cantidad');
		
		  }
		}
	 
		//para la posicion del menu
		$posicionmenu = $this->posicion( $pregunta->getPregTipo()->getId() );
		$preo = new front_preocupaciones();
		$form = $this->createForm( new FrontCheckedType(), $preo );
		$form->handleRequest($request); 
		$sum = 1;

		
		if( $form->isSubmitted() ) {
		
		  //si presiono volver y submitea borra de la session todo hacia delante, para no duplicar respuestas de preguntas
		  if($session->get('cantidad') != 0 && $session->get('volver') == 1 ){
			array_splice($resp_sess, $session->get('cantidad'));
			$session->set('respuestas',$resp_sess);
		  }
		  
		  //setea la variable para indicar que submiteo
		  $session->set('volver',0);
          
		  /*Cuando presiona volver*/
		  if($request->request->has('volver'))
		  { 
			$countsubmit = $session->get('countsubmit');
			if($countsubmit > 0)
                $session->set('countsubmit', $countsubmit - 1);       
                $cont = $session->get('cantidad');
                $session->set('volver',1); 
			
			if( $cont == 0 )
			{ 
                /* debe volver a la preocupacion sino tiene preguntas anteriores */
                return $this->redirect($this->generateUrl('front_preocupacion'));
			} else {
                /* volver a preguntas anteriores */
                $cont = $session->get('cantidad')-$sum; 
                //$session->set('cantidad',$cont);
                //$indiceOld = $session->get('cantidad');
                //$IndiceNew = $this->buscarIndicePregunta($arrayPregOrdenadas, $indiceOld, $pregunta, false);
			
                //$session->set('cantidad',$IndiceNew);
			}
            /*fin presiona volver*/
		  }else{ /*si presiona continuar*/
	
			
			 $countsubmit = $session->get('countsubmit');
			 $session->set('countsubmit', $countsubmit + 1);
			if($session->get('cantidad') == 0){
			  $resp[][$session->get('cantidad')] = [
				'idPreg' => $pregunta->getId(),
				'respuestas'=> json_decode($form->getData()->getChecked(),true),
				'idarbol'=>$idarbol
			  ];
	  
			} else {
			  $valor = count($resp_sess);
			  $resp[][$valor] = [
				'idPreg' => $pregunta->getId(),
				'respuestas'=> json_decode($form->getData()->getChecked(),true),
				'idarbol'=>$idarbol
			  ];
			}
			
			
			if($session->get('cantidad') == 0){
		
			  $session->set('respuestas',$resp);
			}else{
			 
			  $sess = $session->get('respuestas');
			  if(isset($sess)){
			  
				$merg = array_merge($sess,$resp);
				$session->set('respuestas',$merg);
			  }else{
				$session->set('respuestas',$resp);
			  }
			}
			$tipo = $pregunta->getTipo()->getIdTipo();
			$gen = [
				'idPreg' => $pregunta->getId(),
				'respuestas'=> json_decode($form->getData()->getChecked(),true),
				'idarbol'=>$idarbol
			  ];
			 
			
			//logs
			$logs = $session->get('logs');
			
			$uri = $request->getSchemeAndHttpHost().$request->getRequestUri();
			if( isset( $gen["idPreg"] ) ) {
				$idPreg = (int) $gen["idPreg"];
				$preg = Utils::getPregById( $idPreg );
				
                // if( and isset( $gen["respuestas"] ) and count( $gen["respuestas"] )
				// reiniciamos momentos en caso que cambie la selección
				// unset( $logs["info"][$preg] );
                
                //url
				$logs["info"][$preg] = [];
                $logs["info"][$preg]["url"] = $uri;
                
                if( isset( $gen["respuestas"] ) and count( $gen["respuestas"] ) ) {
                    foreach( $gen["respuestas"] as $key => $value ) {
                        
                        // familia
                        if( $preg == 'familia' ) {
                            $estado = $value["estado"][0]["estado"];
                            $cantidad_hijos = (int) $value["cantidad"][0]["cantidad"];
                            $edades = $value["edad"];
                            
                            $logs["info"][$preg]["estado"] = $estado;
                            $logs["info"][$preg]["cantidad_hijos"] = $cantidad_hijos;						
                            if( isset( $edades ) and count( $edades ) ) {
                                foreach( $edades as $key => $edad ) {
                                    $logs["info"][$preg]["edades"][] = Utils::getAgesChild( $edad["edad"] );
                                }
                            }
                        
                        //gastos
                        }else if( $preg == 'gastos' ) {
                            $titulo = $value["titulo"];
                            $valor = $value["valor"];
                            
                            $logs["info"][$preg]["valores"][$key]["titulo"] = $titulo;
                            $logs["info"][$preg]["valores"][$key]["valor"] = $valor;
                        
                        //seguros
                        }else if( $preg == 'seguros' ) {
                            $idseguro = (int) $value["id"];
                            
                            // $respuesta = $em->getRepository('PlanificadorBundle:plan_respuestas')->find( $idseguro );
                            $respuesta = $em->getRepository('PlanificadorBundle:plan_respuestas')->BuscarSeguro( $idseguro );
                            if( $respuesta ) {					
                                $logs["info"][$preg]["seleccionado"][] = $respuesta["titulo"];
                            }
                        
                        }else if( $preg == 'valor_cosas' ) {
                            $titulo = $value["titulo"];
                            $valor = $value["valor"];
                            
                            $logs["info"][$preg]["valores"][$key]["titulo"] = $titulo;
                            $logs["info"][$preg]["valores"][$key]["valor"] = $valor;
                        }
                    }
                }
			}
            
            // dump( $logs );
            // die;
			
			$session->set('logs', $logs);
			$em->getRepository('PlanificadorBundle:Plan_Seguimiento')->registerHistory( $logs );
					
		

			if($tipo == 6){
			  $session->set('familia',$gen);
			}
			if($tipo == 5){
			  $session->set('gastos',$gen);
			}
			$posicionmenu=$this->posicion($pregunta->getPregTipo()->getId());
			
			/*if( (int) (@$nextarray["PREGTIPO"]) == 3){
			   $redirect=2;
			} else 
               */
            if($max > $session->get('cantidad')){
			  $cont = $session->get('cantidad')+$sum; 
			  //$session->set('cantidad',$cont);
			  $redirect=1;
			
			   $contador_prueba=+1;
			}else{
			  $redirect=2;
			  
			}

		  }
			//REDIRECCIONAMIENTO NUEVO 
			if( $request->request->has('volver') ) {
				$indiceOld = $session->get('cantidad');
				$IndiceNew = $this->buscarIndicePregunta($arrayPregOrdenadas, $indiceOld, $pregunta, false);
				$session->set('cantidad',$IndiceNew);
			} else {
				$indiceOld = $session->get('cantidad');
				$IndiceNew = $this->buscarIndicePregunta($arrayPregOrdenadas, $indiceOld, $pregunta);
				if(isset($arrayPregOrdenadas[$IndiceNew]['PREGTIPO'])){
					if($arrayPregOrdenadas[$IndiceNew]['PREGTIPO'] == 3){
						return $this->redirect($this->generateUrl('front_loading'));
					}
				} 
				$session->set('cantidad',$IndiceNew);
			}
			return $this->redirect($this->generateUrl('front_preguntas'));
		}
	  
		// $session->get('countsubmit');
		// preghijo corresponde a el id del arbol, ID corresponde el id de la pregunta en arrayPregOrdenadas
	
		//$session->set('cantidad');
	
		
		// if($redirect==1 or $redirect_t==1){
		 // return $this->redirect($this->generateUrl('front_preguntas'));
		// }elseif ($redirect==2) {
		   // return $this->redirect($this->generateUrl('front_loading'));
		// }


		//envia los valores a la vista de la respuesta si es que existen
		$valores = '';
        if(isset($resp_sess)){
            foreach ($resp_sess as $key => $value){
                if($value[$key]['idPreg'] == $pregunta->getId()){
                    $valores = $value[$key];
                }
            }
        }
	  
		//carga template segun tipo de pregunta
		$tipo = $pregunta->getTipo()->getIdTipo();
		$cantResp = count($pregunta->getRespuestas());
        
		if( $tipo == '6' ) {
            $edades = $em->getRepository('PlanificadorBundle:plan_config')->findBy(array('grupo'=>'4'));
            $edocivils = $em->getRepository('PlanificadorBundle:plan_config')->findBy(array('grupo'=>'1'));
            $canthijos = $em->getRepository('PlanificadorBundle:plan_config')->findBy(array('grupo'=>'3'));
		}
        
        $paramsview = [
            'form' => $form->createView(),
            'pregunta' => $pregunta,
            'menus' => $menus,
            'respuestas' => $cantResp,
            'posicionmenu' => $posicionmenu,
            'valores' => $valores
        ];
        
        if($tipo == 5 || $tipo == 2 ){
			$cantidad = 1; //cantidad maxima para seleccion por el momento preguntas tipo rango
            if($tipo == 5){
                $cantidad = count($pregunta->getRespuestas());
            }
            
            $paramsview["cantidad"] = $cantidad;
            $paramsview["tipo"] = $tipo;
            
        }else{
            $paramsview["edades"] = $edades;
            $paramsview["edocivils"] = $edocivils;
            $paramsview["canthijos"] = $canthijos;
        }
        
        return $this->render('PlanificadorBundle:front:preguntasgenerales.html.twig', $paramsview);
        
	
    }
  
    public function posicion($tipo){
        if( $tipo == 1 ){//Pregunta Generica
            $posicion=3;
        }else{
            $posicion=4;
        }
        
        return $posicion;
    }

    public function Filtro($idarbol,$em,$arrayPregG,$session){
      $repository = $em->getRepository('PlanificadorBundle:plan_filtros')->getFiltros($idarbol);
      $filtros = $repository;

      foreach ($filtros as $r) {
        if($r['TIPO'] == 'B')
          $filtro_b = $r;
        if($r['TIPO'] == 'R'){
          
          $filtro_r = $r;
          $reem = json_decode($filtro_r['JSON_FILTRO'],true);
		  if(count($reem) != 0){
           foreach ($reem as $q){
              $id_pregunta = $q['pregunta'];
            }
		  }
        }
        if($r['TIPO'] == 'F')
          $filtro_f = $r;
        if($r['TIPO'] == 'G')
          $filtro_g = $r;
        if($r['TIPO'] == 'C')
        {
          $filtro_c = $r;
        }
      }

      // TRAE LA RESPUESTA DE LOS FILTROS BASICO
      if(isset($filtro_b['JSON_FILTRO'])) {
        $param = json_decode($filtro_b['JSON_FILTRO'],true);
        $filtro_b = $em->getRepository('PlanificadorBundle:plan_pregunta')->filtrosBasicos($param,$session);
      }else{
        $filtro_b = 1;
      }
    
      // FIN RESPUESTAS FILTRO BASICO
      if(isset($filtro_r['JSON_FILTRO'])){
        $sess = $session->get('respuestas');
        $param = json_decode($filtro_r['JSON_FILTRO'],true);
      //  $filtro_r = $em->getRepository('PlanificadorBundle:plan_pregunta')->filtrosRespuesta($param,$sess); 
        $filtro_r = $em->getRepository('PlanificadorBundle:plan_pregunta')->filtrosRespuestaFinal($param,$sess,$id_pregunta,$idarbol);
      }else{
        $filtro_r = 1;
      }
  
      if(isset($filtro_f['JSON_FILTRO'])){
        
        $sess = $session->get('familia');
        $param = json_decode($filtro_f['JSON_FILTRO'],true);
        
        $filtro_f = $em->getRepository('PlanificadorBundle:plan_pregunta')->filtrosFamiliaPrueba($param,$sess,$idarbol);
      
      }else{
        $filtro_f = 1;
      }

      if(isset($filtro_g['JSON_FILTRO'])){
        $sess = $session->get('gastos');
        $param = json_decode($filtro_g['JSON_FILTRO'],true);
        $filtro_g = $em->getRepository('PlanificadorBundle:plan_pregunta')->filtrosGastos($param,$sess);
      //  die();
      }else{
        $filtro_g = 1;
      }

     // TRAE LA RESPUESTA DE LOS FILTROS CAPITAL
      if(isset($filtro_c['JSON_FILTRO'])) {
        $sess = $session->get('respuestas');
        $param = json_decode($filtro_c['JSON_FILTRO'],true);
        $total= json_decode($filtro_c['JSON_TOTAL'],true);
        foreach ($param as $r) {
          $datos_cap = $em->getRepository('PlanificadorBundle:plan_filtros')->getCapital($r['capital']);
        }
        $filtro_c = $em->getRepository('PlanificadorBundle:plan_pregunta')->filtrosCapital($param,$total,$datos_cap,$sess);
      }else{
        $filtro_c = 1;
      }

      $suma = $filtro_b + $filtro_r +$filtro_f+$filtro_g+$filtro_c;
    
//die();
    return $suma;
   }
		
	public function buscarIndicePregunta($arbol, $indiceArbol, $pregunta, $mayor = true){
		if(count($arbol) != 0){
			foreach($arbol as $indice => $pre){
				if( (int) $pre['PREGHIJO'] !== $pregunta->getId() ){
					if(  $indiceArbol > $indice && $mayor == false){
						$posicionNew = $indice;
					}else if( $indiceArbol < $indice && $mayor == true){
						$posicionNew = $indice;
						break;
					}
				}
			}
		}
		return $posicionNew;
	}

	
}