<?php

namespace PlanificadorBundle\Controller\front;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;



class FrontController extends Controller
{
  // ++++++++++++++++++++++++++++++++++++++++++++++++++++ ACCIONES DE MOMENTOS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  
  public function CrearMenuAction()
  {
    $em = $this->getDoctrine()->getManager();
    /* Incluir menú */
    $menus= $em->getRepository('PlanificadorBundle:plan_config')->findBy(array('grupo'=>'2'), array('value'=>'asc'));
   
    //$menus=$menuPrincipal->getDescription();
    return $menus;
  }
 

}
