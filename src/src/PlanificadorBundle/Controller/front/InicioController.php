<?php

namespace PlanificadorBundle\Controller\front;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use PlanificadorBundle\Form\front\FrontCheckedType;
use PlanificadorBundle\Entity\front_momentos;
use PlanificadorBundle\Entity\front_preocupaciones;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBagInterface;
use PlanificadorBundle\Entity\Plan_Seguimiento;

class InicioController extends FrontController {

    public function IndexAction(Request $request) {
        
   		//limpiando variables de session/
        $session = $request->getSession();

        $session->remove('momentos');
        $session->remove('preocupaciones');
        $session->remove('cantidad');
        $session->remove('volver');
        $session->remove('arrayPregunta');
        $session->remove('cantidad');
        $session->remove('gastos');
        $session->remove('familia');
        $session->remove('respuestas');
        $session->remove('arrayPregOrd');
        $session->remove('arrayPregunta');
        $session->remove('contrataciones');
        $session->remove('arregloPresentacion');
        $session->remove('resumen');
		$session->remove('plancontratado');
		$session->remove('logs');
		$session->set('origen', 1);
        
        $em = $this->getDoctrine()->getManager();

        $momentos = NULL;
        $preocupaciones = NULL;
		
		// iniciamos logs de seguimiento
		$logs = [];
		$person = $session->get('person');
		$session_id = uniqid();

		//insertamos historial
		$logs["username"] = $person["code"];
		$logs["sessionid"] = $session_id;
		$logs["useragent"] = $request->headers->get('User-Agent');
		$session->set('logs', $logs);
		$em->getRepository('PlanificadorBundle:Plan_Seguimiento')->registerHistory( $logs );
		

        return $this->render('PlanificadorBundle:front:inicio.html.twig');
    }

    public function momentosAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $menus = $this->CrearMenuAction();

        $session = $request->getSession();
        $volver = ($session->get('momentos'));
        if (!isset($volver)) {
            $volver = 0;
        }

        $repository = $em->getRepository('PlanificadorBundle:plan_momento');
        $momentos = $repository->MomentosOrdenados();
        $parametros = $em->find('PlanificadorBundle:plan_parametros', 1);
        
    

        $mom = new front_Momentos();
        $form = $this->createForm(new FrontCheckedType(), $mom);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            if ($request->request->has('volver')) {
                return $this->redirect($this->generateUrl('front'));
            } else {
                $data = $form->getData()->getChecked();
                $momentos = json_decode($data, true);
                $session = $request->getSession();
                $session->set('momentos', $momentos);
                
                $preocupaciones = NULL;
                
				//logs
				$logs = $session->get('logs');
				$uri = $request->getSchemeAndHttpHost().$request->getRequestUri();
                
                if( count( $momentos ) ) {
                    //obtenemos los objectos de los momentos para obtener la data
                    $momentosRS = $em->getRepository("PlanificadorBundle:plan_momento")->BuscarMomentosEntity( $momentos );

                    // reiniciamos momentos en caso que cambie la selección
                    $logs["info"]["momentos"] = [];
                    $logs["info"]["momentos"]["url"] = $uri;
                    
                    if( count( $momentosRS ) ) {
                        foreach( $momentosRS as $key => $value ) {
                            $logs["info"]["momentos"]["seleccionado"][] = $value->getDescripcion();
                        }
                    }
                }
                
                
                
				
				$session->set('logs', $logs);
				$em->getRepository('PlanificadorBundle:Plan_Seguimiento')->registerHistory( $logs );

                return $this->redirect( $this->generateUrl('front_preocupacion') );
                // return $this->redirect( $this->generateUrl('front_momentos') );
            }
        }


        return $this->render('PlanificadorBundle:front:momentos.html.twig', array(
                    'form' => $form->createView(),
                    'datos' => $momentos,
                    'parametros' => $parametros,
                    'menus' => $menus,
                    'tipo' => '0',
                    'volver' => $volver
        ));
    }

    public function PreocupacionesAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $parametros = $em->find('PlanificadorBundle:plan_parametros', 2);
        $menus = $this->CrearMenuAction();
        $volver = 0;
        $arraypreosinorden = array();
        $arraypreoTotal = array();
        $preocupacionesfinal = array();

        $session = $request->getSession();
        $momentos = ($session->get('momentos'));
        
      
       
        // $menus = $this->CrearMenuAction();
        $volver = ($session->get('preocupaciones'));
        if (!isset($volver)) {
            $volver = 0;
        }
		
		//logs
		$logs = $session->get('logs');
		
        foreach ($momentos as $momento) {
            $arraymomentos[] = $momento['id'];
        }

        $arraypreocupaciones = array();
        $preocupaciones = $em->getRepository('PlanificadorBundle:plan_momento')->BuscarPreocupaciones($arraymomentos);
		
        foreach ($preocupaciones as $preocupacion) {
            $Idspreocupacion = explode(',', $preocupacion['preocupacion']);
            foreach ($Idspreocupacion as $value) {
                if ($value != '' or array_key_exists($value, $arraypreocupaciones)) {

                    $arraypreocupaciones[$value] = $value;
                }
            }
        }
        $preocupaciones = $em->getRepository('PlanificadorBundle:plan_momento')->BuscarPreocupacionesEntity($arraypreocupaciones);
        if ($preocupaciones) {
            $preo = new front_Preocupaciones();
            $form = $this->createForm(new FrontCheckedType(), $preo);
            $form->handleRequest($request);

            if ($form->isSubmitted()) {
                if ($request->request->has('volver')) {
                    return $this->redirect($this->generateUrl('front_momentos'));
                } else {
                    $data = $form->getData()->getChecked();
                    $preocupaciones = json_decode($data, true);
                    $session = $request->getSession();
                    

					
					//logs
					$logs = $session->get('logs');
					$uri = $request->getSchemeAndHttpHost().$request->getRequestUri();
                    
                    if( count( $preocupaciones ) ) {
                        //obtenemos los objectos de los momentos para obtener la data
                        $preocupacionRS = $em->getRepository("PlanificadorBundle:plan_preocupacion")->BuscarPreocupacionEntity( $preocupaciones );
                        
                        // reiniciamos momentos en caso que cambie la selección
                        $logs["info"]["preocupacion"] = [];
                        $logs["info"]["preocupacion"]["url"] = $uri;
                    
                        if( count( $preocupacionRS ) ) {
                            foreach( $preocupacionRS as $key => $value ) {
                                $logs["info"]["preocupacion"]["seleccionado"][] = $value->getDescripcion();
                            }
                        }
                    }
                    
					$session->set('logs', $logs);
					$em->getRepository('PlanificadorBundle:Plan_Seguimiento')->registerHistory( $logs );

                    
                    /* ordenandolo por orderby de preocupaciones */
                    foreach ($preocupaciones as $preocupacion) {
                        $preocupacion = $em->find('PlanificadorBundle:plan_preocupacion', $preocupacion['id']);
                        $arrayPreocupaciones['id'] = $preocupacion->getId();
                        $arrayPreocupaciones['orderby'] = $preocupacion->getOrderby();
                        if ($preocupacion->getOrderby() != 0) {
                            $arraypreoTotal[] = $arrayPreocupaciones;
                        } else {
                            $arraypreocupacion["id"] = $preocupacion->getId();
                            $arraypreosinorden[] = $arraypreocupacion;
                        }
                    }
                    usort($arraypreoTotal, function($a, $b) {
                        return $a['orderby'] - $b['orderby'];
                    });
                    foreach ($arraypreoTotal as $value) {
                        $arraypreocupacion["id"] = $value["id"];
                        $preocupacionesfinal[] = $arraypreocupacion;
                    }
                    $preocupacionesfinal = array_merge($preocupacionesfinal, $arraypreosinorden);
                    //var_dump($preocupacionesfinal);
                    $session->set('preocupaciones', $preocupacionesfinal);

                    //inicializa la cantidad de genericas
                    $session->set('cantidad', 0);
                    $session->set('volver', 0);
                    
                    $momentos = NULL;
                    
                    
                    
                    return $this->redirect($this->generateUrl('front_preguntas'));
                }
            }
        } else {
            return $this->redirect($this->generateUrl('front_error'));
        }


        return $this->render('PlanificadorBundle:front:preocupacion.html.twig', array(
                    'form' => $form->createView(),
                    'datos' => $preocupaciones,
                    'parametros' => $parametros,
                    'menus' => $menus,
                    'tipo' => '0',
                    'volver' => $volver
        ));
    }

}
