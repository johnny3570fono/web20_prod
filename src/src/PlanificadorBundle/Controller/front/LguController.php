<?php

namespace PlanificadorBundle\Controller\front;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class LguController extends Controller
{

	/**
	 * @Route("/", name="lgu_planificador")
	 * @Method({"GET"})
	 *
	 * @return Response
	 */
	
	public function indexAction(Request $request)
	{
		
		$headertoken = @$_REQUEST['TokenSSO'];
		
		$session = $request->getSession();
		$session->invalidate();
		$session = new Session();

		$session->set('token', $headertoken);

		$token = $this->get('token_session')->IsValido($headertoken);
		
		/*+++++++++++++++++++++++++++++++++++++++++++*/			 

		if($token['codRespuesta']=='0000'){
			$session->set('esvalido', true);
			$validtoken=true;
			$xml = simplexml_load_string($token['key']);
			$rut = trim($xml->userID);
			$rut = str_pad($rut, 10, "0", STR_PAD_LEFT);
			$session->set('rut', $rut);
			$coderut = $rut;
		
			if ($xml->Segmento == 'N')
				$Segmento='personas';
			elseif ($xml->Segmento == 'S')
				$Segmento='select';
			else
				$Segmento='banefe';

			$session->set('segmento', $Segmento);
			$session->set('origen', '1'); /*planificador*/
			$themeparam = '';
			
		/*	if ($xml->subCanal == 'P'){
				$session->set('themedisplay', 'desktop');
			}
			else{
				$session->set('themedisplay', 'mobile');
				$themeparam = '?theme=mobile';
			}*/

			$redirprod = '';
			if ($xml->codProd > ''){
				$themeparam = '';

				$redirprod = $this->getUrlPart($xml->codProd, $xml->codllamado);
				
			}

			$session->set('tiemposesion', time());

		}
		else{
			$session->set('esvalido', false);
			$validtoken=false;
			var_dump($token['descRespuesta']);
			die();
		}


		if($validtoken=true){
			return $this->redirect('../insurance');
		}

		$session->migrate();
	}
	  /**
     * @Route("/gettoken")
     * @Method({"GET", "POST"})
     *
     * @return Response
     */
    public function gettokenplani(Request $request)
    {
    				
		$headertoken = $this->get('token_session')->getGeneraTokenm();
		
		return $this->redirect('../lgu-planificador/?TokenSSO='.$headertoken);
    }


}