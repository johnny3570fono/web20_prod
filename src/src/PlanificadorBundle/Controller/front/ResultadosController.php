<?php

namespace PlanificadorBundle\Controller\front;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use PlanificadorBundle\Entity\front_preocupaciones;
use PlanificadorBundle\Form\front\ResultadosType;
use PlanificadorBundle\Form\front\GenericasType;
use PlanificadorBundle\Entity\Plan_Seguimiento;

class ResultadosController extends FrontController {

    public function LoadingAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $menus = $this->CrearMenuAction();

        $session = $request->getSession();
        //$rut = $session->get('rut');
        $persons = $session->get('person');
        $genero = $persons['gene'];


		if (isset($persons)) {
            $firstName = explode( " ", $persons["name"] );
            $nombre = count( $firstName ) ? $firstName[0] : "";
        } else {
            $nombre = '';
        }


        $estado = '';
        $hijos = '';

        /* session */
        $sessionRespuesta = $session->get('respuestas');
		
        if (isset($sessionRespuesta)) {
			
            foreach ($sessionRespuesta as $respuestas) {
				$idPreg = array_column($respuestas,'idPreg');
				$familia = array_column($respuestas, 'respuestas');
				if(is_array($familia)){
					$familia = end($familia);
					
				
						$pregunta = $em->getRepository('PlanificadorBundle:plan_pregunta')->findOneBy(array('id' => $idPreg));

						if ($pregunta->getTipo()->getIdtipo() == 6) {

						
							foreach ($familia as $r) {
								foreach ($r['estado'] as $q) {
									$est_civil = $q['estado'];
								}
								foreach ($r['cantidad'] as $q) {
									$cantidad = $q['cantidad'];
								}
							}
							$hijos = (int) $cantidad;

					
							$edo = $est_civil;

							$edo = substr($edo, 0, -1);
							if ($genero) {
								if ($genero == 'H') {
									$estado = $edo . 'o';
								} else {
									$estado = $edo . 'a';
								}
							} else {
								$estado = $edo . 'o';
							}
						
					}
				}
            }
			
        }

		//echo $estado.'==='.$hijos;

        $preocupaciones = ($session->get('preocupaciones'));

        foreach ($preocupaciones as $preocupacion) {
            $Idspreocupacion = explode(',', $preocupacion['id']);
            foreach ($Idspreocupacion as $value) {
                if ($value != '' or array_key_exists($value, $arraypreocupaciones)) {

                    $arraypreocupaciones[$value] = $value;
                }
            }
        }

        $preocupaciones = $em->getRepository('PlanificadorBundle:plan_momento')->BuscarPreocupacionesEntity($arraypreocupaciones);

        foreach ($preocupaciones as $preocupacion) {
            $arrayPreocupacionesdes[] = $preocupacion->getDescripcion();
        }


        $arrayPreocupacionesdes = implode(', ', $arrayPreocupacionesdes);
        


        $momentos = ($session->get('momentos'));

        foreach ($momentos as $momento) {
            $Idsmomentos = explode(',', $momento['id']);
            foreach ($Idsmomentos as $value) {
                if ($value != '' or array_key_exists($value, $arraymomentos)) {
						$arraymomentos[$value] = $value;
                }
            }
        }
 

        $momentos = $em->getRepository('PlanificadorBundle:plan_momento')->BuscarMomentosEntity($arraymomentos);
        $gastos = null;
        $arriendo = null;
        $resumen = 'Nos contaste que ';
        // echo $hijos;
        // die;
        
        if ($estado != '') {
            $resumen .= 'estas <strong>' . $estado . '</strong>, ';
        }
        if ($hijos == 1) {
            $resumen .= 'tu familia está formada por <strong> un hijo </strong>';
        } elseif ($hijos > 0) {
            $resumen .= ' tu familia está formada por <strong>' . $hijos . ' hijo(s) .';
        }
        
        if ($arrayPreocupacionesdes and $hijos > 0) {
            $resumen .= ' Te preocupas mucho por <strong>' . $arrayPreocupacionesdes . '.</strong>';
        }elseif ($arrayPreocupacionesdes and $hijos == 0) {
            $resumen .= 'te preocupas mucho por <strong>' . $arrayPreocupacionesdes . '.</strong>';
        }
        
        if ($gastos) {
            $resumen .= 'Tienes gastos recurrentes de <strong></strong>';
        }
        if ($arriendo) {
            $resumen .= 'Estás pagando el arriendo de un <strong>departamento</strong> que podrías proteger por dentro y por fuera con un seguro a medida.';
        }


       
        $resp = [];
        foreach ($sessionRespuesta as $r) {
            foreach ($r as $q) {
                $respuestas = $q['respuestas'];
                $pregunta = $q['idPreg'];
                $tipo = $em->getRepository('PlanificadorBundle:plan_pregunta')->findOneBy(array('id' => $pregunta));
                if ($tipo->getTipo()->getIdtipo() == 6) {
                    $resp[] = [
                        'estado' => $estado,
                        'hijos' => $estado
                    ];
              
                } elseif ($tipo->getTipo()->getIdtipo() == 5) {

                    foreach ($respuestas as $z) {
                        $resp[] = [
                            'id' => $z['id'],
                            'valor' => $z['valor'],
                        ];
                    }
                } elseif ($tipo->getTipo()->getIdtipo() == 4) {

                    if (isset($respuestas)) {
                        foreach ($respuestas as $z) {
                            $resp[] = $z['id'];
                        }
                    }
                } else {
                    if (isset($respuestas)) {
                        foreach ($respuestas as $z) {
                            $resp[] = $z['id'];
                        }
                    }
                }
            }
            $datos[] = [
                'pregunta' => $pregunta,
                'id_resp' => $resp
            ];
            $resp = [];
        }

        header("refresh:1; url=resultados");

        $session->set('resumen', $resumen);


        return $this->render('PlanificadorBundle:front:loading.html.twig', array(
                    'nombre' => $nombre,
                    'resumen' => $resumen,
                    'menus' => $menus,
        ));
    }

    public function IndexAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $session = $request->getSession();
         
		if ($session->get('respuestas')) {
           
            $persons = $session->get('person');
           
            $rut = $session->get('rut');
            $arrayproductos = array();
            //$arraycoberturas = array();
            $arregloPresentacion = array();
            $arregloDatos = array();
            $ids = array();

            $uf = $em->getRepository('AppBundle:Uf')->findLast();
            $menus = $this->CrearMenuAction();

            if (isset($persons)) {
                $firstName = explode( " ", $persons["name"] );
                $nombre = count( $firstName ) ? $firstName[0] : "";
            } else {
                $nombre = '';
            }
            //$preguntasFinales= [];
            $preocupaciones = ($session->get('preocupaciones'));
          
            
            $preguntasFinales = $em->getRepository('PlanificadorBundle:plan_arbol')->getPregGeneralFinal($preocupaciones, [3]);
           

            foreach ($preguntasFinales as $value) {
                $cont = 0;
                $idArbol = $value['idarbol']; //id del arbol
                //      echo $idArbol;
                //buscarmos por el id del arbol el q
                //ORIGINAL
                
			
               $filtros = $em->getRepository('PlanificadorBundle:plan_filtros')->findBy(array('arbol' => $value['idarbol']));
			//   $filtros2 = $em->getRepository('PlanificadorBundle:plan_filtros')->filtro($value['idarbol']);

		 //$valor = $em->getRepository('PlanificadorBundle:plan_arbol')->findOneBy(array('id' => $idArbol));
                $id_pregunta = "";
                foreach ($filtros as $filtro) {
                    if ($filtro->getTipo() == 'B') {
                        $basico = $filtro->getJsonFiltro();
                    }
                    if ($filtro->getTipo() == 'R') {
                        $respuestas = $filtro->getJsonFiltro();
                        foreach ($respuestas as $respuesta) {
                            $id_pregunta = $respuesta['pregunta'];
                        }
                    }
                    if ($filtro->getTipo() == 'G') { /* gastos */
                        $gastos = $filtro->getJsonFiltro();
                    }
                    if ($filtro->getTipo() == 'F') { /* familia */
                        $familia = $filtro->getJsonFiltro();
                    }
                    if ($filtro->getTipo() == 'C') {
                        $capital = $filtro->getJsonFiltro();
                        $total = $filtro->getJsonTotal();
                    }
                }


                /* evaluand filtros */
                if (isset($capital) and count($capital)) {
                    
					 
					$capital_ids = array_column($capital,'capital');					
					$capital_unique_ids = array_unique($capital_ids);
					
				
                    $datos_cap = $em->getRepository('PlanificadorBundle:plan_filtros')->getCapital_In($capital_unique_ids);
                    
					$sess = $session->get('respuestas');
                    /*foreach ($capital as $z) {	
                        $datos_cap = $em->getRepository('PlanificadorBundle:plan_filtros')->getCapital($z['capital']);
                    }*/
					 
                    $fil_c = $em->getRepository('PlanificadorBundle:plan_pregunta')->filtrosCapital($capital, $total, $datos_cap, $sess);
                } else {
                    $fil_c = 1;
                }
                if (isset($basico)) {
                    /* echo 'basico';
                      echo '<pre>';
                      var_dump($basico);
                      echo '</pre>'; */
                    $session = $request->getSession();
                    $fil_b = $em->getRepository('PlanificadorBundle:plan_pregunta')->filtrosBasicos($basico, $session);
                } else {
                    $fil_b = 1;
                }
                //FIN RESPUESTAS q BASICO
                if (isset($respuestas)) {


                    /* echo '<pre> respuestas';
                      var_dump($respuestas);
                      echo '</pre>'; */

                    $sess = $session->get('respuestas');

                    $fil_r = $em->getRepository('PlanificadorBundle:plan_pregunta')->filtrosRespuestaFinal($respuestas, $sess, $id_pregunta, $idArbol);
                } else {

                    $fil_r = 1;
                }

                if (isset($familia)) {
                    /* echo '<pre> familia';
                      var_dump($familia);
                      echo '</pre>'; */
                    $sess = $session->get('familia');

                    $fil_f = $em->getRepository('PlanificadorBundle:plan_pregunta')->filtrosFamiliaPrueba($familia, $sess, $idArbol);
                } else {

                    $fil_f = 1;
                }
                if (isset($gastos)) {
                    $sess = $session->get('gastos');
                    $fil_g = $em->getRepository('PlanificadorBundle:plan_pregunta')->filtrosGastos($gastos, $sess);
                } else {
                    $fil_g = 1;
                }

                $suma = $fil_b + $fil_r + $fil_f + $fil_g + $fil_c;


                if ($suma > 4) {
                    //    echo 'guardados';
                    //  echo $idArbol;
                    $cont = $cont + 1;
                    if (isset($filtro)) {
                        if ($cont == count($filtro)) {
                            //   echo $idArbol; Aqui id que muestra
                            $ids[] = $idArbol;
                        }
                    }
                }
                /* }else{

                  if($valor->getPadre()){
                  $sess = $session->get('respuestas');
                  foreach ($sess as $key => $r){
                  foreach ($r as  $q) {
                  if($q['idarbol'] == $valor->getPadre()->getId())
                  $ids[]=$idArbol;
                  }
                  }
                  }
                  } */
                unset($capital);
                unset($familia);
                unset($gastos);
                unset($respuestas);
                unset($basico);
           }
            if (isset($filtro)) {
                if (count($filtro) == 0) {
                    $ids[] = $idArbol;
                }
            }

            $arboles = $em->getRepository('PlanificadorBundle:plan_producto')->getBuscarArbol($ids);
            /* Garantizando que los productos no se repitan */

            $arbol = array_column($arboles, 'arreglo');

            if ( count( $arbol ) ) {
                foreach ($arbol as $producto) {
                    if( isset( $producto[0] ) ) {
                        $value = $producto[0];
                        $arrayProductos[] = $value;

                        if ( !isset( $value['plan'] ) ) {
                            /* si no tiene plan puede ser auto con esta consulta se verifica si es o no auto */
                            $producto = $em->getRepository('AppBundle:Product')->findOneBy(array('id' => $value['producto']));
                            if ($producto) {
                                if ($producto->getRedirectUrl() != null) {
                                    unset($arregloDatos);
                                    $arregloDatos = array();
                                    $producto_planificador = $em->getRepository('PlanificadorBundle:plan_producto')->findOneBy(array('productoweb' => $producto->getId()));
                                    $arregloDatos['Id'] = $producto_planificador->getId() . '-A' . $producto->getId();
                                    $arregloDatos['Idproducto'] = $producto_planificador->getId();
                                    $arregloDatos['Idproductoweb'] = $producto->getId();
                                    $arregloDatos['Nombre'] = $producto_planificador->getNombre();
                                    $arregloDatos['Descripcion'] = $producto_planificador->getDescripcion();
                                    $arregloDatos['Price'] = 0;
                                    $arregloDatos['cubre'] = $producto_planificador->getCubre();
                                    $arregloDatos['nocubre'] = $producto_planificador->getNocubre();
                                    $arregloDatos['idplan'] = 'A' . $producto->getId();
                                    $arregloDatos['plan'] = 'Auto';
                                    $arregloDatos['imagentipo'] = $producto_planificador->getImagentipo();
                                    $arregloDatos['coberturas'] = $producto_planificador->getCobertura();
                                    // and (!array_key_exists($producto->getCode(),$tenenciacodigos))
                                    if ((!array_key_exists($producto_planificador->getId() . '-A' . $producto->getId(), $arregloPresentacion))) {

                                        $arregloPresentacion[$producto_planificador->getId() . '-A' . $producto->getId()] = $arregloDatos;
                                    }
                                } else {
                                    $menus = $this->CrearMenuAction();
                                    return $this->render('PlanificadorBundle:front:error.html.twig', array(
                                                'menus' => $menus));
                                }
                            }
                        } elseif (!array_key_exists($value['producto'] . '-' . $value['plan'], $arrayproductos)) {
                            // var_dump($value['producto']);die;
                            $plan = $em->getRepository('AppBundle:Plan')->findOneBy(array('id' => $value['plan']));
                            if ($plan) {
                                //traemos informacion del producto de planificador 
                                $idspro = $plan->getProduct()->getId();
                                $producto = $em->getRepository('PlanificadorBundle:plan_producto')->findOneBy(array('productoweb' => $idspro));
								
                                if($producto->getEstado()!=0){ //Visibilidad del Producto
									if ($producto) {

										unset($arregloDatos);
										$arregloDatos = array();
										$arregloDatos['Id'] = $producto->getId() . '-' . $plan->getId();
										$arregloDatos['Idproducto'] = $producto->getId();
										$arregloDatos['Idproductoweb'] = $idspro;
										$arregloDatos['Nombre'] = $producto->getNombre();
										$arregloDatos['Descripcion'] = $producto->getDescripcion();
										//           $arregloDatos['Price'] = $this->Precio($plan,$edad);
										$arregloDatos['Price'] = 0;
										$arregloDatos['cubre'] = $producto->getCubre();
										$arregloDatos['nocubre'] = $producto->getNocubre();
										$arregloDatos['idplan'] = $plan->getId();
										$arregloDatos['plan'] = $plan->getName();
										$arregloDatos['imagentipo'] = $producto->getImagentipo();
										$arregloDatos['coberturas'] = $producto->getCobertura();
										//&&(!array_key_exists($plan->getProduct()->getCode(),$tenenciacodigos))
										if ((!array_key_exists($producto->getId() . '-' . $plan->getId(), $arregloPresentacion))) {
											$arregloPresentacion[$producto->getId() . '-' . $plan->getId()] = $arregloDatos;
										}
									}
								}
                            }

                            $arrayproductos[$value['producto'] . '-' . $value['plan']] = $value;
                        }
                    }
                }
            }

            
            $preo = new front_Preocupaciones();
            $form = $this->createForm(new ResultadosType(), $preo);
            $form->handleRequest($request);
            if ($form->isSubmitted()) {
                // echo 'continuar';
                /* variable que si esta en true redirige la pantalla final a la del asesor pero si esta en false va a la de los productos sugeridos  */

                if ($request->request->has('Email')) {
                    $resumen = $session->get('resumen');
                    $parametros = $this->getParameter('mail_option_plani_cotiza');
                    $to = $form->getData()->getAddressEmail();

                    $this->get('mail_helper')->correoAsesor($arregloPresentacion, $to, $parametros, $uf, $nombre, $menus, $resumen);
                }

                $data = $form->getData()->getChecked();
                $contrataciones = json_decode($data, true);

                $session->set('contrataciones', $contrataciones);
                $session->set('arregloPresentacion', $arregloPresentacion);
                if ($contrataciones) {

                    $idplan = $contrataciones[0]['id'];

                    unset($contrataciones[0]);

                    $session->set('contrataciones', $contrataciones);
                    $session->set('idplan', $idplan);
                    $session->set('posicionarray', 0);
                    
                    //auto
                    // echo $idplan;
                    $cod_prod = substr($idplan, 1, 3);  //codigo producto
                    if ($idplan[0] == 'A') {
                        $producto_web = $em->getRepository('AppBundle:Product')->findOneBy(array('id' => $cod_prod));
                        if ($producto_web) {
                            if ($producto_web->getRedirectUrl() != '') {

                                return $this->redirect($producto_web->getRedirectUrl());  //si no funciona debe ser que en producto web20 la url externa esta sin http
                            }
                        }
                    } else {
                        $plan = $em->getRepository('AppBundle:Plan')->findOneBy(array('id' => $idplan));
                        $idprod = $plan->getProduct()->getId();
                        /* verificamos que el producto este click to call en el web2.0 */
                        $producto = $em->getRepository('AppBundle:Product')->findOneBy(array('id' => $idprod));
                        if ($producto->getUrlClicktocall() != '') {

                            return $this->redirect('/web20/customers/info/product/' . $idprod);
                        } else {/* si no es click to call va al producto normal */
                            if ($plan->getProduct()->getFormType() == '\AppBundle\Form\FraudType') {
                                return $this->redirect('/web20/customers/info/fraud/' . $idprod);
                            } else {

                                return $this->redirect('/web20/customers/info/details/' . $idprod);
                            }
                        }
                    }
                }
            }
            
            //Pantalla para mostrar los seguros ya contratados
            $contrataciones = $session->get('contratados');

            // if ($contrataciones != null)
                // $contrato = true;
            // if ($contrataciones == true) {
                // unset($arregloPresentacion);
                // $arregloPresentacion = array();
                // foreach ($contrataciones as $contratado) {

                    // $plan = $em->getRepository('AppBundle:Plan')->findOneBy(array('id' => $contratado));
                    // if ($plan) {
                        // traemos informacion del producto de planificador 
                        // $idspro = $plan->getProduct()->getId();
                        // $producto = $em->getRepository('PlanificadorBundle:plan_producto')->findOneBy(array('productoweb' => $idspro));

                        // if ($producto) {

                            // unset($arregloDato);
                            // $arregloDato = array();
                            // $arregloDatos['Id'] = $producto->getId() . '-' . $plan->getId();
                            // $arregloDatos['Idproducto'] = $producto->getId();
                            // $arregloDatos['Idproductoweb'] = $idspro;
                            // $arregloDatos['Nombre'] = $producto->getNombre();
                            // $arregloDatos['Descripcion'] = $producto->getDescripcion();
                            // $arregloDatos['Price'] = $this->Precio($plan, $edad);
                            // $arregloDatos['cubre'] = $producto->getCubre();
                            // $arregloDatos['nocubre'] = $producto->getNocubre();
                            // $arregloDatos['idplan'] = $plan->getId();
                            // $arregloDatos['plan'] = $plan->getName();
                            // $arregloDatos['imagentipo'] = $producto->getImagentipo();
                            // $arregloDatos['coberturas'] = $producto->getCobertura();
                            // if (!array_key_exists($producto->getId() . '-' . $plan->getId(), $arregloPresentacion)) {
                                // $arregloPresentacion[$producto->getId() . '-' . $plan->getId()] = $arregloDatos;
                            // }
                        // }
                    // }
                // }

                // return $this->render('PlanificadorBundle:front:resultadoContratados.html.twig', array(
                            // 'form' => $form->createView(),
                            // 'nombre' => $nombre,
                            // 'uf' => $uf,
                            // 'arregloPresentacion' => $arregloPresentacion,
                            // 'menus' => $menus));
            // }
            /* echo "<pre>";
              print_r($ids);
              echo "</pre>"; */
              

            $plancontratado = $session->get('plancontratado');

            // LOGS
            $uri = $request->getSchemeAndHttpHost() . $request->getRequestUri();
            $logs = $session->get('logs');
            
            //reiniciamos momentos en caso que cambie la selección
            $logs["info"]["resultados"] = [];
            $logs["info"]["resultados"]["url"] = $uri;

            if (count($arregloPresentacion)) {
                foreach ($arregloPresentacion as $key => $value) {
                    $producto = $value["Nombre"] . " - " . $value["plan"];

                    $logs["info"]["resultados"]["seleccionado"][] = $producto;
                }
            }

            $session->set('logs', $logs);
            $em->getRepository('PlanificadorBundle:Plan_Seguimiento')->registerHistory($logs);


            return $this->render('PlanificadorBundle:front:resultado.html.twig', array(
                        'form' => $form->createView(),
                        'nombre' => $nombre,
                        'uf' => $uf,
                        'ids' => $ids,
                        'plancontratado' => $plancontratado,
                        'arregloPresentacion' => $arregloPresentacion,
                        'menus' => $menus));
        }
        return $this->redirect($this->generateUrl('front'));
    }

    public function ErrorAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $session = $request->getSession();
        $rut = $session->get('rut');
        $persons = $session->get('person');


        // $responsable = $em->getRepository('AppBundle:Customer')->findOneBy(array('code'=>$rut));
        if (isset($persons)) {
            $nombre = $persons["name"];
        } else {
            $nombre = '';
        }

        $menus = $this->CrearMenuAction();
        return $this->render('PlanificadorBundle:front:error.html.twig', array(
                    'nombre' => $nombre,
                    'menus' => $menus));
    }

    public function Precio($plan, $edad) {
        $em = $this->getDoctrine()->getManager();
        $temp = $plan->getProduct()->getFormTemplate();
        // var_dump($temp);
        $cargas = 0;
        $result = '2';
        if ($temp == 'travel.html.twig' || $temp == 'hospitalizacion.html.twig' || $temp == 'home.html.twig' || $temp == 'urgencias_medicas.html.twig' || $temp == 'fraudeTarjeta.html.twig' || $temp == 'fraud.html.twig')
            $option = 1;
        if ($temp == 'catastrofico.html.twig' || $temp == 'life.html.twig' || $temp == 'oncologico.html.twig')
            $option = 2;
        if ($temp == 'vacaciones.html.twig' || $temp == 'vacaciones_largas_fam.html.twig')
            $option = 3;
        if ($temp == 'life_education.html.twig')
            $option = 4;
        $result = $plan->getPrice();

        switch ($option) {
            case 1:
                $result = $plan->getPrice();
                if ($temp == 'hospitalizacion.html.twig') {
                    $result = $result * count($cargas);
                }
                return $result;
                break;
            case 2:
                $tramoedades = $plan->getTargets();
                foreach ($tramoedades as $value) {
                    if ($edad >= $value->getStart() and $edad <= $value->getFinish()) {
                        $result = $value->getPrice();
                    }
                }
                return $result;
                break;
            case 3:
                /* se busca  el precio menor segun el plan ,  se ordenan la coverturas por el cpital menor al mayor y se tomar el menor luego se busca el menor precio en las coberturas */
                $coberturas = $em->getRepository('AppBundle:Coverage')->findBy(array('plan' => $plan), array('capital' => 'ASC'));
                if ($coberturas) {

                    $prices = explode(",", $coberturas[0]->getPrice());
                    $menor = $prices[0];
                }
                foreach ($prices as $value) {
                    if ($menor < $value) {
                        $menor = $value;
                    }
                }
                $result = $menor;

                return $result;
                break;
            case 4:
                return $result;
                break;
            default:
                return $result;
                break;
        }
        return $result;
    }

    function calcular_edad($fecha) {
        $dias = explode("-", $fecha, 3);
        $dias = mktime(0, 0, 0, $dias[1], $dias[0], $dias[2]);
        $edad = (int) ((time() - $dias) / 31556926 );

        return $edad;
    }

}
