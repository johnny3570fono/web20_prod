<?php

namespace Taisa\DummyBundle;

/**
 * Soap Dummy
 */
class SoapPersona
{

    /**
     * Función dummy
     *
     * @param string $token
     * @return array
     */
    public function Nuevos_Datos_Cliente($token)
    {
        $faker = \Faker\Factory::create('es_ES');

        $data = [
            'OUTPUT' => [
                'INFO' => [],
                'ESCALARES' => [
                        'NOMASEGURADO'          => 'Marcos',
                        'APELLIPATASEG'         => 'Matamala',
                        'APELLIMATASEG'         => 'Fernandez',
                        'FECHANACIMIENTOASERG'  => '1982-09-26',
                        'DIRECASEG'             => $faker->address,
                        'FONOCELASEGU'          => $faker->phoneNumber,
                        'FONOPARASEGU'          => $faker->phoneNumber,
                        'EMAILASEGU'            => 'asterion.alfa@gmail.com',
                        'SEXOASEGU'             => 'female',
                        'CODCONTROL'            => 15,
                    ]
            ],
        ];

        return $data;
    }

    /**
     * Función dummy
     *
     * @param string $token
     * @return array
     */
    public function Nuevos_Datos_Cliente_Online_Lite($token)
    {
        $faker = \Faker\Factory::create('es_ES');

        $data = [
            'OUTPUT' => [
                'INFO' => [],
                'ESCALARES' => [
                        'PENOMPE_R_OUT'          => 'Marcos',
                        'PEPRIAP_R_OUT'         => 'Matamala',
                        'PESEGAP_R_OUT'         => 'Fernandez',
                        'PEFECNA_R_OUT'  => '1982-09-26',
                        'PEDOMANT_R_OUT'             => $faker->address,
                        'PENUMTEL_R_OUT'          => $faker->phoneNumber,
                        'PEPRETEL_R_OUT'          => $faker->phoneNumber,
                        'EMAIL_OUT'            => 'asterion.alfa@gmail.com',
                        'SEXOASEGU'             => 'male',
                        'CODCONTROL'            => 15,
                    ]
            ],
        ];

        return $data;
    }
}
