<?php

namespace Taisa\DummyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\Response;

use Taisa\DummyBundle\SoapToken;

/**
 * Dummy Controller
 */
class DefaultController extends Controller
{
    protected $options = [
                'cache_wsdl' => WSDL_CACHE_NONE,
            ];
    /**
     * @Route("/session", name="dummy_session_service")
     *
     * @return Response
     */
    public function indexAction()
    {
        $url = $this->generateUrl('dummy_session_service_wsdl', [], true);

        $server = new \SoapServer($url, $this->options);
        $server->setObject(new SoapToken());

        $response = new Response();
        $response->headers->set('Content-Type', 'text/xml; charset=UTF-8');

        ob_start();
        $server->handle();
        $content = ob_get_clean();
        $response->setContent($content);

        return $response;
    }

    /**
     * @Route("/session/wsdl", name="dummy_session_service_wsdl")
     * @Template()
     *
     * @return Response
     */
    public function wsdlAction()
    {
        $params = [
            'endpoint' => $this->generateUrl('dummy_session_service', [], true),
            'ns' =>  $this->generateUrl('dummy_session_service', [], true)
        ];

        $response = $this->render('DummyBundle:Default:wsdl.xml.twig', $params);

        $response->headers->set('Content-Type', 'text/xml');

        return $response;
    }
}
