<?php

namespace Taisa\DummyBundle;

/**
 * Soap Dummy
 */
class SoapToken
{
    /**
     * Función dummy
     *
     * @param string $token
     * @return array
     */
    public function getValidarToken($token)
    {
        $data = [
                'codRespuesta' => 1,
                'codRespuestaSpecified' => 0,
                'descRespuesta' => 0,
                'descRespuestaSpecified' => 0,
                'token' => $token,
                'tokenSpecified' => 0,
            ];

        return $data;
    }
}
