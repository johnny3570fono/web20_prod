<?php

namespace Taisa\DummyBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Bundle Taisa
 */
class DummyBundle extends Bundle
{
}
