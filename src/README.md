## Portal Seguros Zurich Santander

### Servidor jenkins

10.69.153.10 y http://jenkins-dev.taisaplus.com/


### Servidor de test

10.69.153.11 y zurich-test.taisaplus.com

### php.ini
memory_size >= 256

### Deployment

http://rocketeer.autopergamene.eu/#/docs/rocketeer/README

### Development

vagrant plugin install vagrant-hostsupdater
vagrant up
vagrant ssh
cd /opt/www && sudo composer install

~/.bashrc
    export ORACLE_HOME=/u01/app/oracle/product/11.2.0/xe
    export ORACLE_SID=XE
    export ORACLE_BASE=/u01/app/oracle
    export PATH=$ORACLE_HOME/bin:$PATH

/etc/hosts
    127.0.0.1 oracle11g

./network/admin/listener.ora
    (SID_DESC =
      (SID_NAME = XE):
      (ORACLE_HOME = /u01/app/oracle/product/11.2.0/xe)
    )


/etc/init.d/nginx start


### Base de datos
### Oracle
    CREATE TABLESPACE portal DATAFILE 'portal_n' SIZE 200M;
    CREATE USER portal IDENTIFIED BY portal;
    GRANT CREATE SESSION, GRANT ANY PRIVILEGE TO portal;
    ALTER USER portal DEFAULT TABLESPACE portal;
    GRANT UNLIMITED TABLESPACE TO portal;
    GRANT ALL privilege to portal;

    CREATE TABLESPACE portal DATAFILE 'portal_n' SIZE 200M;
    CREATE USER ZURICH_DEV IDENTIFIED BY PASSWORD;
    GRANT CREATE SESSION, GRANT ANY PRIVILEGE TO ZURICH_DEV;
    ALTER USER ZURICH_DEV DEFAULT TABLESPACE portal;
    GRANT UNLIMITED TABLESPACE TO ZURICH_DEV;
    GRANT ALL privilege to ZURICH_DEV;

### Modelo de datos

- Un segmento tiene cero o muchas asistencias
- Un segmento tiene cero o muchas familias
- Un segmento tiene cero o muchos productos
- Una asistencia tiene cero o muchos segmentos
- Una asistencia tiene cero o muchos productos

- Un producto tiene cero o muchos segmentos
- Un producto tiene una familia
- Un producto tiene cero o muchas asistencias
- Un producto tiene cero o muchos planes
- Un producto tiene cero o muchas actividades
- Un producto tiene cero o muchos atributos
- Un producto tiene cero o muchas declaraciones

- Una familia tiene cero o muchos segmentos
- Una familia tiene cero o una familia
- Una familia tiene cero o muchos productos

- Un plan tiene cero o muchos atributos
- Un atributo tiene un grupo
- Un atributo tiene cero o muchos planes

- Un seguro tiene cero o muchos beneficiarios
- Un seguro tiene cero o un auto
- Un seguro tiene cero o un hogar
- Un seguro tiene cero o una autorización de salud
- Un seguro tiene cero o un hogar
- Un seguro tiene cero o un auto
