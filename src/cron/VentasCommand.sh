#/bin/bash!

DATE_FROM=$(date -d '24 hour ago' '+%Y-%m-%d')
TIME=$(date +'%H:%M')
DATE_TO=$(date +'%Y-%m-%d')
URL="/opt/www/zurich_portal_backend/src"

/usr/bin/php $URL/app/console ventas:generate $DATE_FROM $TIME $DATE_TO $TIME > $URL/cron/logs/VentasCommand.txt

