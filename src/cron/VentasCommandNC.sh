#/bin/bash!

DATE_FROM=$(date -d '24 hour ago' '+%Y-%m-%d 00:00')
DATE_TO=$(date +'%Y-%m-%d 00:00')
URL="/opt/www/zurich_portal_backend/src"

/usr/bin/php $URL/app/console conciliacionpublico:generate "" $DATE_FROM $DATE_TO > $URL/cron/logs/VentasCommandNC.txt
