#!/bin/bash

# Llave privada
openssl genrsa -out llave-privada.key 1024

# Crear un CSR (Certificate Signing Request)
openssl req -new -key llave-privada.key -out llave-publica.csr

# Generando el certificado SSL
openssl x509 -req -days 365 -in llave-publica.csr -signkey llave-privada.key -out certificado.crt
