{% embed "::insurance/details.html.twig" %}
    {% block javascripts %}
        {{ parent() }}
        <script src="{{ asset('assets/js/controllers/catastrofico.js') }}"></script>
    {% endblock %}
    {% block details_header %}
        {{ parent() }}
        {%  if product.hasPlan %}
            <div class="col-9 xs title">
                <p class="subtitulo">Selecciona tu plan y completa los datos requeridos</p>
            </div>
        {% endif %}
    {% endblock %}

    {% block details_steps %}
        {% if product.hasPlan %}
            {% embed "::insurance/steps.html.twig" with {inactive: false, step: 1} %}
                {% block step_declaration %}Selección de plan{% endblock %}
                {% block step_body %}
                    {% block step_button_text %}{% endblock %}
                    {{ include('partials/plan.html.twig', {plans: product.plans, price: product.price, form: form}) }}
                    {{ form_widget(form.plan, {attr: {style: 'display: none', 'ng-value': 'plan', 'ng-init': 'plan.id =' ~ product.plans.first.id }}) }}
                    <div class="bkg-gray" id="step1">
                        <div class="row">
                            <div class="col-6 xs">
                                {% if product.hasCoverages %}
                                    {{ include('partials/coverages.html.twig', {product: product}) }}
                                {% else %}
                                    <p class="subtitulo">Cobertura UF 5.000 por evento</p>
                                    {% verbatim %}
                                    <div class="box-explains" id="target-message" ng-show="plan.id > 0 && target.start > 0 && target.finish > 0">
                                        De acuerdo a los datos proporcionados, usted, se encuentra en el <strong>tramo de edad de {{target.start}} a {{target.finish}} años*</strong>. La prima se calculó automáticamente.
                                    </div>
                                    {% endverbatim %}
                                {% endif %}
                                <p class="text-black font-frutiger valor-prima" ng-show="insurance.price > 0">
                                    <strong>Valor prima mensual:</strong>
                                    <strong class="text-red">
                                        {% verbatim %}
                                            {{ insurance.price | currency:"$":"0" }} (UF {{ insurance.uf.price }})
                                        {% endverbatim %}
                                    </strong>
                                </p>
                                <div class="myrow" ng-show="plan.cargasMax > 0">
									<div class="mytd block-mobile">
                                        <span class="valor-prima">¿Deseas incluir asegurados?</span>
                                    </div>
									<div class="mytd box-asegurados">
										<span class="menos-asegurados" ng-click="lessBeneficiary()">-</span>
										<input class="cantidad-asegurados" type="text" ng-model="beneficiariesCount">
										<span class="mas-asegurados" ng-click="moreBeneficiary()">+</span>
									</div>
								</div>
                            </div>

                            <div class="col-6 xs pL30">
                                {% if form.offsetExists('insured') is not empty %}
                                    {{ include('partials/form/insured.html.twig', {form: form}) }}
                                {% endif %}
                                <hr>
                            </div>
                        </div>

                        <div class="row">
                            <p class="legal-form">
                                {%  if product.hasGroupProductPlan %}
                                    (*) {{ product.groupProductPlan.first.description }}
                                    <br><br>
                                {% endif %}
                                (1) El proponente asegurado autoriza que las comunicaciones o notificaciones que la compañía de seguro o el Banco Santander Chile envíen o deban enviar en razón de la póliza, se dirijan al correo electrónico indicado, así como a cualquier otra dirección de contacto que él hubiese entregado a dicho banco. Además a este email te enviaremos el comprobante e información del seguro.
                            </p>
                        </div>
                    </div>
                {% endblock %}
            {% endembed %}
        {% endif %}

        {% if form.offsetExists('cargas') is not empty %}
            {% embed "::insurance/steps.html.twig" with {inactive: form.cargas.vars.valid, step: 2} %}
                {% block step_declaration %}Declaración de cargas{% endblock %}
                {% block step_title %}Declaración de cargas{% endblock %}
                {% block step_subtitle %}Completa los datos requeridos para ingresar tus cargas{% endblock %}
                {% block step_attr %}ng-show="beneficiariesCount > 0"{% endblock %}
                {% block step_body_attr %}ng-show="beneficiariesCount > 0"{% endblock %}
                {% block step_body %}
                    {{ include('partials/form/cargas.html.twig', {form: form.cargas}) }}
                    <div class="box-aumento" ng-show="plan.cargasMax > 1">
                        <span class="texto">{% block add_beneficiarie %}Agregar{% endblock %}</span>
                        <span class="menos-carga" ng-click="lessBeneficiary()">-</span>
                        <span class="mas-carga" ng-click="moreBeneficiary()">+</span>
                    </div>
                    <hr>
                    <p class="text-black font-frutiger valor-prima" ng-show="beneficiariesCount > 0">
                        <strong>La prima mensual con tus cargas es:</strong>
                        <strong class="text-red monthly">
                            {% verbatim %}
                                {{ insurance.price | currency:"$":"0" }} (UF {{ insurance.uf.price }})
                            {% endverbatim %}
                        </strong>
                    </p>
                    <div class="box-explains">
                        De acuerdo a los datos proporcionados, su cónyuge es mayor que el titular. Por tanto, el valor mensual de la prima se ajustará al tramo de edad de este. Si no es correcto, cambie la fecha de nacimiento de su cónyuge.
                    </div>
                {% endblock %}

                {% block footer_step %}
                    <p class="legal-form-dos">
                        1) la Compañía calculará el monto reembolsable de cada uno de los documentos que acrediten el gasto cubierto por este seguro aplicándole los porcentajes de la tabla de coberturas de las condiciones particulares del seguro, a los montos efectivamente pagados por el asegurado, es decir, los gastos descontados, los reembolsos de Isapre / Fonasa y cualquier otro seguro. Si el total de los montos reembolsables es inferior a UF 100, no corresponderá reembolso alguno por parte de la Compañía. 2) Para los afiliados a Fonasa, este seguro reembolsará el 100% del copago de cada uno de los bonos presentados. Los gastos que sean rendidos con boletas o facturas (diferencias no cubiertas por Fonasa o prestaciones sin coberturas) serán reembolsados al 50% de
                    </p>
                {% endblock %}
            {% endembed %}
        {% endif %}

        {% if form.offsetExists('accept') is not empty %}
            {% embed "::insurance/steps.html.twig" with {inactive: form.vars.valid, step: 3, last: true} %}
                {% block step_declaration %}Declaración personal de salud{% endblock %}
                {% block step_title %}Declaración personal de salud{% endblock %}
                {% block step_body %}
                    {{ include('partials/form/health_declaration.html.twig', {form: form}) }}
                {% endblock %}
            {% endembed %}
        {% endif %}

    {% endblock %}
{% endembed %}
