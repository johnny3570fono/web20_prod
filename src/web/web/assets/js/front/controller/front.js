app.controller("plan", function($scope) {
	BaseController.call(this, $scope);

	$scope.checked = [];
	$scope.cant = $scope.checked.length;
	$scope.volver = volver;
	
	$scope.total=total;

	angular.forEach($scope.volver, function(value){
		$scope.checked.push({
			"id":value.id
		});
	})

	var datos = JSON.stringify($scope.checked);
	$("#checked_form_checked").val(datos);
	

	if(cantMax != null){
		$scope.cantMax = cantMax;
	}
	if(cantSel != null){
		$scope.cantSel = cantSel;
	}

	$scope.add = function(val){
		var i = -1;
		angular.forEach($scope.checked, function(value){
			if(value.id == val){
				i = 0;
			}
		});
		if ( i !== -1 ){
			angular.forEach($scope.checked, function(value, key){
				if(value.id == val){
					$scope.checked.splice(key,1);
				}
			});
		}else{
			if($scope.checked.length < $scope.cantSel){
				$scope.checked.push({
					"id":val
				});
			}else
			{
				newAlert("Superaste la cantidad máxima permitida para seleccionar");
			}
		}

		$scope.nom = [];
		console.log($scope.checked);
		angular.forEach($scope.checked,function(value){

			var nom = $("#"+value.id).data("nom");
				console.log(nom);
			$scope.nom.push({
				"nombre":nom
			});
		});
		//console.log($scope.nom);
		var datos = JSON.stringify($scope.checked);
		$("#checked_form_checked").val(datos);

	}
	$scope.mostrar = function(){
		$scope.cantMax=$scope.totals;
		$(".ocultar").removeAttr("style");
		$(".displayNone").css("display", "none");

	}

	$scope.alert = function(){
		newAlert("Selecciona al menos una opción");
	}
	
	$scope.continua = function() {	
		$("button.btnContinue").click();
	}
	
	$scope.volver = function() {
		$("button.btnBack").click();
	}

	$(document).on("click",".vof", function(){
		var ciclo = $(this).data("id");
		
	});
	
	
	
	function newAlert($mensaje){
		$(".error-box .error-content").html($mensaje);
		$.featherlight($(".error-box"), {closeIcon: "", otherClose: ".error-cerrar"});
		$(".featherlight-content").addClass("error-border");
	}
	function newAlert2($mensaje){
		
		$(".error-box .error-content").html($mensaje);
		$.featherlight($(".error-box"), { closeIcon: "", otherClose: ".error-cerrar"});
		$(".featherlight-content").addClass("error-border");
	}
	
	$scope.$watch("$carga",function(){
		//release 0.3
		angular.forEach($scope.checked, function(value){
			if($scope.checked.length > 0){
				$scope.mostrar();
				$("#"+value.id).children("a").addClass("str-box-active");
				$scope.nom = [];
				var nom = $("#"+value.id).data("nom");
				$scope.nom.push({
					"nombre":nom
				});
			}
		})
	})

})