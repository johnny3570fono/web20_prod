app.controller("plan", function($scope) {
	BaseController.call(this, $scope);
	
	$scope.asociacion = $("#resultados_form_checked").val();
	$scope.checked = [];
	$scope.cant = $scope.checked.length;
    
    var regmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    
	$(document).on("click",".web20",function(){
			
		$(".prod:checked").each(function() {
    		$scope.checked.push({
				"id": $(this).val()
			});
			console.log("El checkbox con valor " + $(this).val() );
    	});

		if($scope.checked.length > 0){
			var datos = JSON.stringify($scope.checked);
		    $("#resultados_form_checked").val(datos);
		   
		}else{
			event.preventDefault();
			$('.str-custom-checkbox').addClass('final-error');
			newAlert2("Por favor selecciona un seguro de tu interés");
		}
        
	})
	$("input.uEmail").val("");
    $(document).on("submit", "#frmMail", function(e){
        var inputemail = $("input.uEmail").val();
        
        if( !$.trim( inputemail ) || !regmail.test(inputemail) ) {
            e.preventDefault();
            newAlert2("Ingrese un email válido");
        }else{
            $(".modal-paso-1").hide();
            $(".modal-paso-2").show();
        }
    });
    
	$(document).on('click', '.str-custom-checkbox', function(event) {
		$('.str-custom-checkbox').removeClass('final-error');
	});

	$scope.alert = function(){
		newAlert2("sin productos seleccionados");
	}
	
	$scope.redirect = function(correo){
		console.log(correo);	
	}
	function newAlert2($mensaje){
		$(".error-box .error-content").html($mensaje);
		$.featherlight($(".error-box"), { closeIcon: '', otherClose: ".error-cerrar"});
		$(".featherlight-content").addClass("error-border");
	}
	
	function newAlert($mensaje){
		$(".error-box .error-content").html($mensaje);
		$.featherlight($(".error-box"), {closeIcon: "", otherClose: ".error-cerrar"});
		$(".featherlight-content").addClass("error-border");
	}
})
