function BaseController($scope) {

    $scope.campaigns = campaigns;
    $scope.campaign = {};
    $scope.token = token;
    $scope.product = product;
    $scope.insurance = insurance;
    $scope.georly = georly
    $scope.submit = -1;
    $scope.errors = [];
    $scope.plans = [];
    $scope.items = [];
    $scope.targets = [];
    $scope.target = { start : 18, finish: 45 };
    $scope.beneficiaries = [];
    $scope.coverages = [];
    $scope.plan = {};
    $scope.step_first = true;
    $scope.cantidad = $scope.beneficiaries.length;
   
    //console.log($scope.georly['ALGARROBO']);
    //$scope.visible = 1;
    //$scope.precio = $scope.plans.cargasPrice.split()
    //remove remove required de telefono
    $('#insurance_insured_addressPhone_telephone').removeAttr('required');
    $('#insurance_insured_addressPhone_code').removeAttr('required');

   
    if($scope.errors.global != undefined)
    {
     var valor = $('#insurance_insured_addressCellphone_telephone').val()
       
            $('#insurance_insured_addressCellphone_telephone').attr('aria-invalid','true')
            $('#insurance_insured_addressCellphone_telephone').parent('div').addClass('error')
            event.preventDefault();
        
    }else{
        $scope.errors = []
    }





     $scope.setStepFirst = function(value){
        $scope.step_first = value;

        return true;
    };

    $(document).on('click','#next1', function(){
        event.stopPropagation();
        event.preventDefault();
    })
    $(document).on('click','#next2', function(){
        event.stopPropagation();
        event.preventDefault();
    })
    $(document).on('click','#next3', function(){
        event.stopPropagation();
        event.preventDefault();
    })

    $scope.range = function(num_start, num_end){
        var nums = [];

        for (i = num_start; i <= num_end; i++) {
            nums.push(i);
        }

        return nums;
    };
    //aqui ingresa la edad s
    $scope.days = function(){
        return $scope.range(1, 31);
    };

    $scope.months = function() {
        return $scope.range(1, 12);
    };

    /*$scope.yyyy = function() {
        var d = new Date();
        var year_start = d.getFullYear() - $scope.product.ageMax;
        var year_end = d.getFullYear() - $scope.product.ageMin;

        return $scope.range(year_start, year_end);
    };*/
    $scope.yyyy = function() {
        var d = new Date();
        var year_start = d.getFullYear() - 85;
        var year_end = d.getFullYear();

        return $scope.range(year_start, year_end);
    };

    $scope.filterCoverage = function(coverage){
        return true;
    };

    $scope.findPlan = function(id){
        var plan = {};
        for (var i = 0, len = $scope.plans.length; i < len; i++) {
          if ($scope.plans[i].id == id) {
             
            plan = $scope.plans[i];
          }
        }
        return plan;
    };

    $scope.fillFields = function(){
        $('.fill').each(function(){
            $($scope).val($($scope).data('value'));
        });
    };

    $scope.emptyFields = function(){
        $('.fill').each(function(){
            $($scope).val('');
        });
    };

    $scope.moreBeneficiary = function() {
        if ($scope.beneficiaries.length < $scope.plan.cargasMax){

            $scope.beneficiaries.push({
                name: '',
                lastname1: '',
                lastname2: '',
                code: '',
                relationship: '',
                capital: 0,
                day: null,
                month: null,
                year: null
            });

            $scope.$emit("BENEFICIARY", "MORE");
            $scope.cantidad = $scope.beneficiaries.length
        }
       
    };

    $scope.lessBeneficiary = function() {
        if ($scope.beneficiaries.length > $scope.plan.cargasMin) {
            $('.cont-carga').last().remove();
            $scope.beneficiaries.pop();

            $scope.$emit("BENEFICIARY", "LESS");
            $scope.cantidad = $scope.beneficiaries.length
        }
        
        if( typeof $scope.mayorEdad === "function" ){
            $scope.mayorEdad();
        }
    };

    $scope.payBeneficiary = function() {
        $scope.$emit("BENEFICIARY", "other");
    };

    $scope.getBirthday = function() {
        var year = parseInt($('#insurance_insured_birthday_year').val());
        var month = parseInt($('#insurance_insured_birthday_month').val());
        var day = parseInt($('#insurance_insured_birthday_day').val());
		
        if( !(day)){
            var year = parseInt($('#appbundle_fraud_insured_birthday_year').val());
            var month = parseInt($('#appbundle_fraud_insured_birthday_month').val());
            var day = parseInt($('#appbundle_fraud_insured_birthday_day').val());
        }
		

        if (isNaN(year) || isNaN(month) || isNaN(day)) {
            return false;
        }


        if (year != "" && month != "" && day != "") {
			month = parseInt(month) - 1;
			year = parseInt(year);
			day = parseInt(day);
            var date_string = year + "-" + month + "-" + day;
            if ($scope.isDate(year, month, day)) {
                return new Date(year, month, day);
            }
        }

        return false;
    };
    //console.log($scope.getBirthday())
    $scope.years = function(birth) {
	
        var check = new Date();
        var milliDay = 1000 * 60 * 60 * 24; // a day in milliseconds;
        var ageInDays = (check - birth) / milliDay;
        var ageInYears =  Math.floor(ageInDays/365);
		
        return ageInYears;
    };
    $scope.isDate = function(year, month, day) {

        var invalid = new Date(year, month, day) !== "Invalid Date";
        var isnan = !isNaN(new Date(year, month, day));

        return (invalid && isnan) ? true : false;
    }

    $scope.age = function() {
        var birthday = $scope.getBirthday();
        //console.log($scope.getBirthday())
        if (birthday == false) {
            birthday = new Date();
        }
        var dia =  new Date().getDate()
        var mes = new Date().getMonth()
        var año = new Date().getFullYear()
       
        
        var age = (new Date().getFullYear() - birthday.getFullYear());
		
        if(mes < birthday.getMonth()){	
            age = age-1
           
        }else if(mes == birthday.getMonth()){
			
            if(dia < birthday.getDate()){
                age = age-1
            }

        }
        //var age = (new Date().getFullYear() - birthday.getFullYear());

        if (age < 1) {
            age = 0;
        }
        
        

        return age;
    };
    $scope.upPrice = function(price)
    {
        var p = parseInt(Math.ceil(price));
        if (p>0) {
            while ((p%10) != 0) {
                p = p + 1;
            }
        }
        return p;
    };



    $scope.updatePrice = function(price) {
        $scope.insurance.price = $scope.upPrice(price);
    };

    $scope.planToggle = function(plan_id) {
       
        $scope.beneficiaries = []
        $scope.dato = plan_id

        $scope.plan = $scope.findPlan(plan_id);
        $scope.prima_uf = parseFloat($scope.plan.price)
        $scope.prima = $scope.upPrice($scope.prima_uf * $scope.uf)
        $('#insurance_price').val($scope.prima_uf)
        $('#insurance_plan').val($scope.plan.id);
        $scope.updatePrice($scope.plan.price);
		$scope.activeCampaign($scope.plan);

    };

   

    $scope.getObjects = function(obj, key, val) {
      var objects = [];
      for (var i in obj) {
          if (!obj.hasOwnProperty(i)) continue;
          if (typeof obj[i] == 'object') {
              objects = objects.concat($scope.getObjects(obj[i], key, val));
          } else if (i == key && obj[key] == val) {
              objects = obj;
          }
      }
      return objects;
    }
    $scope.volver = function(step,next){
        $('.regular-input').removeAttr('required')
		$('.regular-select').removeAttr('required')
        var stepNum = parseInt(step);
        var nextNum = parseInt(next);
        var existStep = nextNum;
        while(existStep && $('#next' + nextNum).is(':visible') == false){
            nextNum = nextNum + 1;
            existStep = document.getElementById('next' + nextNum);
        }
        next = $('#next' + nextNum);
        if (validateSteps(stepNum)) {
            if (nextNum == 1) {
                $('#step_first_selected').show();
                $('#plans').show();
                $('#step1').show();
                $('#next'+stepNum).parent().next().slideToggle(300);
                $('div.box-declaracion.selected_plan').hide();

            } else {
                 $('div.box-declaracion.selected_plan').show();
                $('#step2').show();
                $('#next'+stepNum).parent().next().slideToggle(300);
                $('#info-declaracion').attr('style','display:block;');
            }
            
        }
		$('.regular-select').removeAttr('required')
        $('.regular-input').attr('required','required')
        
    }
    $(document).on('change', '#insurance_insured_addressState', function(event) {
        var dato = $scope.georly[$('#insurance_insured_addressState').val()]
        $('#insurance_insured_addressCity').val(dato)
        $('#ciudad').html(dato)
       
    });
    $(document).on('change', '#appbundle_fraud_insured_addressState', function(event) {
        var valor = $scope.georly[$('#appbundle_fraud_insured_addressState').val()]
        $('#appbundle_fraud_insured_addressCity').val(valor)
        $('#ciudad').html(valor)
       
    });
    $(document).on('change', '#insurance_home_addressState', function(event) {
        var dato = $scope.georly[$('#insurance_home_addressState').val()]
        $('#insurance_home_addressCity').val(dato)
        $('#ciudad2').html(dato)
        
    });
    $(document).on('click', '.button-solicitar', function(event) {
        //event.preventDefault();
       
        /* Act on the event */
    });
    

    $(document).on('click', '.btn-volver-declaracion', function(event) {
        var nextNum = parseInt($(this).data('next'));
        var existStep = document.getElementById('next' + nextNum);
        var step = null;
        while(existStep && $('#next' + nextNum).is(':visible') == false){
            nextNum = nextNum - 1;
            existStep = document.getElementById('next' + nextNum);
        }
        next = $('#next' + nextNum);
        step = parseInt(next.data('step'));
        if(typeof step !== 'undefined') {
            if(typeof utag_local !== 'undefined' && typeof utag_local.categoria !== 'undefined') {
                var categoria = utag_local.categoria;
                var path_template = utag_local.template.page_path;
                var name_template = utag_local.template.page_name;
               
                if(step == 2) {
                    if(categoria  == 'hogar') {
                        utag_local.page_path = path_template.replace("#path#", "domicilio");
                        utag_local.page_name = name_template.replace("#name#", "Domicilio");
                    } else if(categoria  == 'viaje') {
                        utag_local.page_path = path_template.replace("#path#", "asegurados");
                        utag_local.page_name = name_template.replace("#name#", "Asegurados");
                    } else {
                        utag_local.page_path = path_template.replace("#path#", "cargas");
                        utag_local.page_name = name_template.replace("#name#", "Cargas");
                    }
                } else if(step == 3) {
                    utag_local.page_path = path_template.replace("#path#", "dps");
                    utag_local.page_name = name_template.replace("#name#", "DPS");
                } else if(step == 4) {
                    utag_local.page_path = path_template.replace("#path#", "aes");
                    utag_local.page_name = name_template.replace("#name#", "AES");
                } 
               
                if(typeof utag !== 'undefined') {
                    console.log($scope);
                    if(typeof $scope.plan !== 'undefined' && typeof $scope.plan.id !== 'undefined') {
                        if( $scope.plan.name !== 'undefined') {
                            utag_local.plan = $scope.plan.name;
                        }
                    }
                    if( typeof $scope.prima_uf !== 'undefined' &&  $scope.prima_uf  !== "") {
                        utag_local.prima_uf = $scope.prima_uf;
                    }
                    if(typeof $scope.beneficiaries !== 'undefined') {
                        utag_local.cargas = $scope.beneficiaries.length;
                    }
                    if(typeof $scope.rangoDias !== 'undefined')  {
                        utag_local.dias = $scope.rangoDias;
                    }
                    if(typeof $scope.insured !== 'undefined' && $scope.insured == 1) {
                        utag_local.seguro_titular = 'si';
                    } else {
                        utag_local.seguro_titular = 'no';
                    }
                    if(typeof $scope.cober !== 'undefined') {
                        if(typeof $scope.coverages !== 'undefined') {
                            $.each($scope.coverages, function(index, item){
                                if(typeof item.id !== "undefined" && item.id == $scope.cober) {
                                    if(typeof item.days !== 'undefined') {
                                        utag_local.dias = item.days;
                                    }
                                    if(typeof item.capital !== 'undefined') {
                                        utag_local.cobertura = item.capital;
                                    }
                                }
                            })
                        }
                    }
                    if(typeof utag_local.template !== 'undefined') {
                        var template = utag_local.template;
                        delete utag_local.template;
                        utag.view(Object.assign({}, utag_local));
                        utag_local.template = {
                            page_path: template.page_path,
                            page_name: template.page_name
                        };
                    }
                }
            }
        }
    });

    $(document).on('click', '.btn-continuar', function(event) {
        var nextNum = parseInt($(this).data('next'));
        var existStep = document.getElementById('next' + nextNum);
        var step = null;
        while(existStep && $('#next' + nextNum).is(':visible') == false){
            nextNum = nextNum + 1;
            existStep = document.getElementById('next' + nextNum);
        }
        next = $('#next' + nextNum);
        step = parseInt(next.data('step'));
        if(typeof step !== 'undefined') {
            if(typeof utag_local !== 'undefined' && typeof utag_local.categoria !== 'undefined') {
               
                var categoria = utag_local.categoria;
                var path_template = utag_local.template.page_path;
                var name_template = utag_local.template.page_name;
                if(step == 2) {
                    if(categoria  == 'hogar') {
                        utag_local.page_path = path_template.replace("#path#", "domicilio");
                        utag_local.page_name = name_template.replace("#name#", "Domicilio");
                    } else if(categoria  == 'viaje') {
                        utag_local.page_path = path_template.replace("#path#", "asegurados");
                        utag_local.page_name = name_template.replace("#name#", "Asegurados");
                    } else {
                        utag_local.page_path = path_template.replace("#path#", "cargas");
                        utag_local.page_name = name_template.replace("#name#", "Cargas");
                    }
                } else if(step == 3) {
                    utag_local.page_path = path_template.replace("#path#", "dps");
                    utag_local.page_name = name_template.replace("#name#", "DPS");
                } else if(step == 4) {
                    utag_local.page_path = path_template.replace("#path#", "aes");
                    utag_local.page_name = name_template.replace("#name#", "AES");
                } 
                if(typeof utag !== 'undefined') {
                    console.log($scope);
                    if(typeof $scope.plan !== 'undefined' && typeof $scope.plan.id !== 'undefined') {
                        if( $scope.plan.name !== 'undefined') {
                            utag_local.plan = $scope.plan.name;
                        }
                    }
                    if(typeof $scope.prima_uf !== 'undefined' &&  $scope.prima_uf  !== "") {
                        utag_local.prima_uf = $scope.prima_uf;
                    }
                    if(typeof $scope.beneficiaries !== 'undefined') {
                        utag_local.cargas = $scope.beneficiaries.length;
                    }
                    if(typeof $scope.rangoDias !== 'undefined')  {
                        utag_local.dias = $scope.rangoDias;
                    }
                    if(typeof $scope.insured !== 'undefined' && $scope.insured == 1) {
                        utag_local.seguro_titular = 'si';
                    } else {
                        utag_local.seguro_titular = 'no';
                    }
                    if(typeof $scope.cober !== 'undefined') {
                        if(typeof $scope.coverages !== 'undefined') {
                            $.each($scope.coverages, function(index, item){
                                if(typeof item.id !== "undefined" && item.id == $scope.cober) {
                                    if(typeof item.days !== 'undefined') {
                                        utag_local.dias = item.days;
                                    }
                                    if(typeof item.capital !== 'undefined') {
                                        utag_local.cobertura = item.capital;
                                    }
                                }
                            })
                        }
                    }
                    if(typeof utag_local.template !== 'undefined') {
                        var template = Object.assign({}, utag_local.template);
                        delete utag_local.template;
                        utag.view(Object.assign({}, utag_local));
                        utag_local.template = {
                            page_path: template.page_path,
                            page_name: template.page_name
                        };
                    }
                }
            }
        }
    });


   /* Validación Parentescos*/
     /*Función que valida que la edad según parentesco se encuentre dentro de los rangos establecidos en el mantenedor Parentesco*/
    function validaTramo(parametros){
        var objParentesco = $scope.items;
        if (parametros !== undefined || parametros !== '') {
            var edadPersona = parametros.edad;
            var parentesco = parametros.parentesco;
           	var benfindex = parametros.indexbenef;		
            objParentesco.forEach(function (value, index) {
				
                if (value.value === parentesco) {
					
					var dia = $('#insurance_cargas_'+benfindex+'_birthday_day').val();
					var mes = $('#insurance_cargas_'+benfindex+'_birthday_month').val();
					var anio = $('#insurance_cargas_'+benfindex+'_birthday_year').val();
				
					
						if (edadPersona < value.edadMinima || edadPersona > value.edadMaxima) {
							if(dia!=""&& mes!="" && anio !=""){
								newAlert('Estimado cliente, la edad perteneciente a  ' + parentesco + ' debe estar entre ' + value.edadMinima + ' y ' + value.edadMaxima + ' años');
                                
								$('#insurance_'+benfindex+'_birthday').find('select').find('option:eq(0)').prop('selected', true);
                                if( typeof $scope.beneficiaries[benfindex].day != "undefined"){
                                    $scope.beneficiaries[benfindex].day = '';
                                }
                                if( typeof $scope.beneficiaries[benfindex].year  != "undefined"){
                                    $scope.beneficiaries[benfindex].year = '';
                                }
                                if( typeof $scope.beneficiaries[benfindex].month  != "undefined"){
                                    $scope.beneficiaries[benfindex].month = '';
                                }
								$('#insurance_'+benfindex+'_birthday').addClass('error');
							}
						}else{
								$('#insurance_'+benfindex+'_birthday').removeClass('error');
								$scope.edades.push(edadPersona);
						}   
					
				}		
            });

        }

   } 
   $scope.mayorEdad = function () {

        $scope.edades = [$scope.edad];
        
        $scope.beneficiaries.forEach(function (benef, index, data) {
			
            if (benef.day && benef.month && benef.year) {
                var dob = new Date(benef.year, benef.month - 1, benef.day);
                var today = new Date();

                var edad = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));
             //   console.dir("beneficiarios: " + parentesco);
               // console.dir("edad pariente: " + edad);

                var parametros = {edades: edad};

                benef.age = edad;

                if (benef.relationship != "") {

                    parametros = {

                        edad: edad,
                        parentesco: benef.relationship,
						indexbenef: index

                    };
                  
                    validaTramo(parametros);
                }
            }
        });

        age = Math.max.apply(null, $scope.edades);

        

    };

    $scope.selectRel = function (value, idHidden) {
        var elemento = $scope.getObjects(relationships, 'value', value);
        
		if (elemento.length > 0) {
            elemento = elemento[0];
						
            if(elemento.value === 'Hijo/a' ||  elemento.value === 'Nieto/a'){
                elemento.maxapparition = -1;
            }            
            
            if ((elemento.maxapparition !== -1) &&
                ($('select.referSelect option[value="' + elemento.value + '"]:selected').length > elemento.maxapparition)) {
                $('select.referSelect[data-person=' + idHidden + '] option:eq(0)').prop('selected', true);
                $scope.beneficiaries[idHidden].relationship = "";
            }
        }
        
        if ($scope.beneficiaries[idHidden].age != undefined) {

            var parametros = {
                parentesco: elemento.value,
                edad: $scope.beneficiaries[idHidden].age,
				indexbenef: idHidden
				
            };

            validaTramo(parametros);

        }

    };
   
    $scope.activeCampaign = function(plan) {
        $scope.campaign = false;
        if($scope.campaigns && plan.id) {
            $scope.campaigns.forEach(function(campaign){
                if (campaign.plans && ! $scope.campaign) {
                    const active =  campaign.plans.some(e => e.id == plan.id);
                    if(active) {
                       $scope.campaign = campaign;
                    }
                }
            });
        }
    };
   /* Fin Validación*/
    
}

var app = angular.module('portal', []);

app.directive("classError", function(){
  return {
    restrict: "A",
    link:function(scope, element, attrs){
        if (typeof scope.errors !== "undefined") {
            if (typeof scope.errors.fields !== "undefined") {
                if (typeof scope.errors.fields[attrs.entity] !== "undefined") {
                    if (typeof scope.errors.fields[attrs.entity][attrs.index] !== "undefined") {
                        if (typeof scope.errors.fields[attrs.entity][attrs.index][attrs.field] !== "undefined") {
                            element.addClass('error');
                        }
                    }
                }
            }
        }
    }
  }
});