$(document).ready(function() {

	var hash = window.location.hash;



	var caruselFamilias = $('.owl-carousel');

	caruselFamilias.owlCarousel({
		items:1,
		loop:false,
		center:false,
		margin:0,
		URLhashListener: true,
		autoplay: true,
		stopOnHover: true,
		// stopOnHover: true,
		// autoplayTimeout: 3500,
		// autoplayHoverPause: true,
		startPosition: 'URLHash',
		lazyLoad: true,
		smartSpeed: 500,
		dots: false,
		callbacks: true,
		nav: false,
    	autoHeight:false
	});

	caruselFamilias.on('initialized.owl.carousel',function(e){
		var sanitizeClass = e.target.className.replace(/ /gi, '.');
		$('div.' + sanitizeClass + ' div.owl-stage').css('width','100%');
		var count = $('div.' + sanitizeClass + ' div.owl-stage div.owl-item').length;
		switch(count) {
			case 4:
				$('div.' + sanitizeClass + ' div.owl-nav').css('display','none');
				break;
			case 3:
				$('div.' + sanitizeClass + ' div.owl-stage div.owl-item').css('width','33.33333%');
				$('div.' + sanitizeClass + ' div.owl-stage div.owl-item:last').css('width','33.33334%');
				$('div.' + sanitizeClass + ' div.owl-nav').css('display','none');
				break;
			case 2:
				$('div.' + sanitizeClass + ' div.owl-stage div.owl-item').css('width','50%');
				$('div.' + sanitizeClass + ' div.owl-nav').css('display','none');
				$('div.' + sanitizeClass + ' div.owl-stage').addClass('dos');
				break;
			case 1:
				$('div.' + sanitizeClass + ' div.owl-stage div.owl-item').css('width','100%');
				$('div.' + sanitizeClass + ' div.owl-nav').css('display','none');
			  break;
		}
	});

  caruselFamilias.on('changed.owl.carousel', function (e) {
    if (e.item.index != null && !$(e.target).hasClass('owl-text-select-on')) {
      $('ul.icons-buttons li a').removeClass('active');
      $('ul.icons-buttons li').eq(e.item.index).find('a').addClass('active');
    }
  });

	var carouselProductos = $('.own-carousel');

  carouselProductos.owlCarousel({
		loop:false,
		center:false,
		margin:0,
		lazyLoad: true,
		stopOnHover: true,
		smartSpeed: 500,
		dots: false,
		callbacks: true,
		nav: true,
		mouseDrag: true,
		touchDrag: true,
		pullDrag: true,
		responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:2
	        },
	        1000:{
	            items:4
	        }
	    }
	});

	window.onresize = function(){

		var windowWidth = $(this).width();

                $('.icons-buttons li a').click(function(e){
                    if (windowWidth >= 600) {
						var mydata = $(this).data();
						var mytext = (mydata.target);
						$(this).attr('href', '#' + mytext);
                    } else {
                        var mydata = $(this).data();
                        $(this).attr('href', mydata.href);
                    }
                });
	};

	var windowWidthStatic = $(window).width();

	// var myheight = $('.owl-carousel .owl-carousel').height();

	if (windowWidthStatic <= 600){
            $('.icons-buttons li a').click(function(e){
                var mydata = $(this).data();
                $(this).attr('href', mydata.href);
            });
	}else{
            $('.icons-buttons li a').click(function(e){
                var mydata = $(this).data();
                var mytext = (mydata.target);
                $(this).attr('href', '#' + mytext)
                // $('.owl-carousel .owl-carousel').css({'height': myheight});
            });
	}
});
