$(document).ready(function() {

    function owlResize(caruselFamilias) {
        console.log('entre')
        caruselFamilias.trigger('destroy.owl.carousel');
        caruselFamilias.html(caruselFamilias.find('.owl-stage-outer').html()).removeClass('owl-loaded');
        caruselFamilias.owlCarousel(caruselFamilias);
    }

	var hash = window.location.hash;

	var caruselFamilias = $('.owl-carousel');

	caruselFamilias.owlCarousel({
		items:1,
		loop:false,
		center:true,
		margin:0,
		URLhashListener: true,
		//autoplay: true,
		stopOnHover: true,
		// stopOnHover: true,
		//autoplayTimeout: 8000,
		autoplayHoverPause: true,
		startPosition: 'URLHash',
		lazyLoad: true,
		smartSpeed: 500,
		//dotsEach: true,
		callbacks: true,
		nav: false,
		//navigation: true,
    	autoHeight:false
	});
	caruselFamilias.on('initialized.owl.carousel',function(e){
		var sanitizeClass = e.target.className.replace(/ /gi, '.');
		$('div.' + sanitizeClass + ' div.owl-stage').css('width','100%');
		var count = $('div.' + sanitizeClass + ' div.owl-stage div.owl-item').length;
		switch(count) {
			case 4:
				$('div.' + sanitizeClass + ' div.owl-nav').css('display','none');
				break;
			case 3:
				$('div.' + sanitizeClass + ' div.owl-stage div.owl-item').css('width','33.33333%');
				$('div.' + sanitizeClass + ' div.owl-stage div.owl-item:last').css('width','33.33334%');
				$('div.' + sanitizeClass + ' div.owl-nav').css('display','none');
				break;
			case 2:
				$('div.' + sanitizeClass + ' div.owl-stage div.owl-item').css('width','50%');
				$('div.' + sanitizeClass + ' div.owl-nav').css('display','none');
				$('div.' + sanitizeClass + ' div.owl-stage').addClass('dos');
				break;
			case 1:
				$('div.' + sanitizeClass + ' div.owl-stage div.owl-item').css('width','100%');
				$('div.' + sanitizeClass + ' div.owl-nav').css('display','none');
			  break;
		}
	});


  caruselFamilias.on('changed.owl.carousel', function (e) {
     
     otra = $('.owl-item.active').find('div').data('val')
     otra = otra +1;
     //var hash = $('.item[data-val*="'+otra+'"]').data('hash')
     //console.log('mm:'+hash)
     if(otra > 6)
     {
        hash = $('.owl-item.active').find('div').data('hash')
        hash = hash.charAt(0).toUpperCase()+ hash.slice(1);
        if(hash == 'Productos bancarios')
            hash = "Productos Bancarios"; 
     }else
     {
        hash = $('.item[data-val*="'+otra+'"]').data('hash')
        console.log('mm:'+hash)
     }
    //hash = $('.owl-item.active').find('div').data('hash')
    hash = hash.charAt(0).toUpperCase()+ hash.slice(1);
    if(hash == 'Productos bancarios')
        hash = "Productos Bancarios";
        elemento =  $(".slick-track").children('div[data-familia*="'+hash+'"]').size();
        padre = $('.prod[data-familia*="'+hash+'"]').parent('div.slick-track').width();
        seg_widt = elemento*contenedor+30;
        //otra = $('.owl-item.active').find('div').data('val')
        if(elemento <=3)
        {
            $('.slick-track').attr('style','opacity: 1; width: '+seg_widt+'px; transform: translate3d(-'+0+'px, 0px, 0px)');
        }else
        {
            posicion = contenedor*3
            $('.slick-track').attr('style','opacity: 1; width: '+seg_widt+'px; transform: translate3d(-'+posicion+'px, 0px, 0px)');
        }
        console.log('hash:'+hash)
        console.log('elemento:'+elemento)
        console.log('seg_widt:'+seg_widt)
        console.log('posicion:'+posicion)
        console.log('contador:'+contenedor)

    if (e.item.index != null && !$(e.target).hasClass('owl-text-select-on')) {
      $('ul.icons-buttons li a').removeClass('active');
      $('ul.icons-buttons li').eq(e.item.index).find('a').addClass('active');
    }
  });

    var contador = 3
    var contenedor = $('.prod').width();
    var posicion = 0
    var otra = 0

   $(document).on('click', '.test', function(){
        //owlResize(caruselFamilias);

        $(window).trigger('resize');
        posicion = 0;
        contador = 3;
   		//AÑADE CLASE ACTIVE A OPCIONES DE FAMILIA 
   		$('.family').removeClass('active')
        $(this).addClass('active')
   		//WIDTH TOTAL DEL CAROUSEL
   		var mywidth = $('.owl-stage').width();
        //FAMILIA DEL CAROUSEL
        val = $(this).data('target')
        //WIDTH PARTICIONADO POR LA CANTIDAD DE FAMILIAS
        var total = mywidth/6
        //Posicion del elemento	
        var pos = $(this).data('posicion');
        //Transicion del elemento
        var transicion = ((pos - 1)* total)*-1 
        var estilo =  $('.owl-stage').css('transform')
        //REALIZA LA TRANSICION DEL CAROUSEL
        var posit = pos -1
        caruselFamilias.trigger('to.owl.carousel', [posit, 0]);

        
    	hash = $('.owl-item.active').find('div').data('hash')
    	hash = hash.charAt(0).toUpperCase()+ hash.slice(1);
    	if(hash == 'Productos bancarios')
    		hash = "Productos Bancarios";
    	//otra = $('.owl-item.active').find('div').data('val')
    	elemento =  $(".slick-track").children('div[data-familia*="'+hash+'"]').size();
    	padre = $('.prod[data-familia*="'+hash+'"]').parent('div.slick-track').width();
    	seg_widt = elemento*contenedor+30;
        if(elemento <=3)
        {
            $('.slick-track').attr('style','opacity: 1; width: '+seg_widt+'px; transform: translate3d(-'+0+'px, 0px, 0px)');
        }else
        {
            posicion = contenedor*3
            console.log(posicion)
            $('.slick-track').attr('style','opacity: 1; width: '+seg_widt+'px; transform: translate3d(-'+posicion+'px, 0px, 0px)');
        }
        $(window).trigger('resize');
   });

	window.onresize = function(){

		var windowWidth = $(this).width();

                $('.icons-buttons li a').click(function(e){
                    if (windowWidth >= 600) {
						var mydata = $(this).data();
						var mytext = (mydata.target);
						$(this).attr('href', '#' + mytext);
                    } else {
                        var mydata = $(this).data();
                        $(this).attr('href', mydata.href);
                    }
                });
	};

	var windowWidthStatic = $(window).width();

	if (windowWidthStatic <= 600){
			$(this).attr('href', '#')
            $('.icons-buttons li a').click(function(e){
                var mydata = $(this).data();
                //$(this).attr('href', mydata.href);
                  });
	}else{
		//$(this).attr('href', '#')
            $('.icons-buttons li a').click(function(e){
                var mydata = $(this).data();
                var mytext = (mydata.target);
                //$('.owl-item.active').children('.item').addClass('active')
                $(this).attr('href', '#' + mytext)
                //$('.owl-carousel .owl-carousel').css({'height': myheight});
            });
	}


});
/*$(document).ready(function() {
  $('#media').carousel({
    pause: true,
    interval: false,
  });
});*/