$(function() {	

	$('option').each(function(){ 
		$(this).text('('+$(this).val()+') '+$(this).text());
	});
	/******************************************************************
	**Se oculto la Barra de búsqueda de la Entity "Banner" pues *******
	**la misma generaba error al momento de la realizar consultas *****
	** 05.06.2017 *****************************************************
	**/
	var entity = $('input[name="entity"]:hidden');
	if(entity.val()=="Banner"){	    
	    $("input[type='search']").hide();
	    $("button[type='submit']").hide();	    
	}else if(entity.val()=="UF"){	    
	    $("input[type='search']").hide();
	    $("button[type='submit']").hide();	    
	}else if(entity.val()=="TramoEdades"){	    
	    $("input[type='search']").hide();
	    $("button[type='submit']").hide();	    
	}else{
		$("input[type='search']").show();
	    $("button[type='submit']").show();
	}		 
});



$(document).ready(function() {



	$(function () {
        $('#campaign_startAt').datetimepicker({
			format: 'DD/MM/YYYY HH:mm',
			locale: 'es'
		});
        $('#campaign_endAt').datetimepicker({
			format: 'DD/MM/YYYY HH:mm',
			locale: 'es',
            useCurrent: false //Important! See issue #1075
        });
        $("#campaign_startAt").on("dp.change", function (e) {
            $('#campaign_endAt').data("DateTimePicker").minDate(e.date);
        });
        $("#campaign_endAt").on("dp.change", function (e) {
            $('#campaign_startAt').data("DateTimePicker").maxDate(e.date);
        });
    });
});
