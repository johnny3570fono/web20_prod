(function($) {
	$(function() {
		var jcarousel = $('.jcarousel');

		jcarousel
			.on('jcarousel:reload jcarousel:create', function () {
				var carousel = $(this),
					width = carousel.innerWidth();

				ul = tamano = carousel.children('ul');
				tamano = carousel.children('ul').children('li').size();
				controles = carousel.siblings('.jcarousel-control-prev, .jcarousel-control-next');

				if (tamano == 1){
					ul.addClass('centro');
					width = width / 2;
					controles.hide();
				}
				else if (tamano == 2) {
					width = width / 2;
					controles.hide();
				}
				else if (tamano == 3) {
					width = width / 3;
					controles.hide();
				}
				else if (tamano <= 4) {
					width = width / 4;
					controles.hide();
				}
				else if (width >= 600) {
					width = width / 4;
				}
				else if (width >= 350) {
					width = width / 2;
				}

				carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
			})
			.jcarousel({
				wrap: 'circular'
			});

		$('.jcarousel-control-prev')
			.jcarouselControl({
				target: '-=1'
			});

		$('.jcarousel-control-next')
			.jcarouselControl({
				target: '+=1'
			});

		$('.jcarousel-pagination')
			.on('jcarouselpagination:active', 'a', function() {
				$(this).addClass('active');
			})
			.on('jcarouselpagination:inactive', 'a', function() {
				$(this).removeClass('active');
			})
			.on('click', function(e) {
				e.preventDefault();
			})
			.jcarouselPagination({
				perPage: 1,
				item: function(page) {
					return '<a href="#' + page + '">' + page + '</a>';
				}
			});
	});

})(jQuery);
