$(document).ready(function() {

	// owlCarousel inicio
	var sync1 = $("#sync1");
	var sync2 = $("#sync2");

if (sync1.size()){
	var windowWidthStatic = $(window).width();

	var cantitems = $('.icons-buttons .icon-button').size();

	sync1.owlCarousel({
		singleItem : true,
		slideSpeed : 2000,
		navigation : true,
		pagination : false,
		afterAction : syncPosition,
		responsiveRefreshRate : 200,
		loop: true,
		responsiveBaseWidth: 'body > .container',
	});

	sync2.owlCarousel({
		items : cantitems,
		itemsDesktop      : [900,cantitems],
		itemsDesktopSmall : [800,cantitems],
		itemsTablet       : [768,cantitems],
		itemsMobile       : [750,cantitems],
		pagination:false,
		responsive:true,
		afterInit : function(el){
			el.find(".owl-item").eq(0).addClass("synced");
		}
	});
 
	function syncPosition(el){
		var current = this.currentItem;
		$("#sync2")
			.find(".owl-item")
			.removeClass("synced", 2000)
			.eq(current)
			.addClass("synced", 2000)
		if($("#sync2").data("owlCarousel") !== undefined){
			center(current)
		}
	}
 
	$("#sync2").on("click", ".owl-item", function(e){
		e.preventDefault();
		var number = $(this).data("owlItem");
		sync1.trigger("owl.goTo",number);
	});
 
	function center(number){
		var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
		var num = number;
		var found = false;
		for(var i in sync2visible){
			if(num === sync2visible[i]){
				var found = true;
			}
		}
 
		if(found===false){
			if(num>sync2visible[sync2visible.length-1]){
				sync2.trigger("owl.goTo", num - sync2visible.length+2)
			}else{
				if(num - 1 === -1){
					num = 0;
				}
				sync2.trigger("owl.goTo", num);
			}
		} else if(num === sync2visible[sync2visible.length-1]){
			sync2.trigger("owl.goTo", sync2visible[1])
		} else if(num === sync2visible[0]){
			sync2.trigger("owl.goTo", num-1)
		}
		
	}

	$( "#grancarrusel" ).hover(
		function() {
			$( this ).addClass( "encarrusel" );
		}, function() {
			$( this ).removeClass( "encarrusel" );
		}
	);

	//transicion
	function playCarrusel(){
		if(!$( "#grancarrusel" ).hasClass('encarrusel')){
			$("#sync1").trigger("owl.next");
		}
		setTimeout(playCarrusel, 10000);
	}
	setTimeout(playCarrusel, 10000);

	// owlCarousel fin


	// jCarousel ini
	var jcarousel = $('.jcarousel');

	jcarousel
	.on('jcarousel:reload jcarousel:create', function () {
		var carousel = $(this),
			width = carousel.innerWidth();

		ul = carousel.children('ul');
		tamano = carousel.children('ul').children('li').size();
		controles = carousel.siblings('.jcarousel-control-prev, .jcarousel-control-next');

		if (tamano == 1){
			ul.addClass('centro');
			width = width / 1.7;
			controles.hide();
		}
		else if (tamano == 2) {
			width = width / 2;
			controles.hide();
		}
		else if (tamano == 3) {
			width = width / 3;
			controles.hide();
		}
		else if (tamano > 3) {
			width = width / 3;
			controles.show();
		}
		else if (width >= 600) {
			width = width / 3;
		}
		else if (width >= 350) {
			width = width / 2;
		}

		carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');

		//var myheight = $('.owl-carousel .owl-carousel').height();
		//console.log(myheight)

		if (windowWidthStatic <= 600){
			$('.owl-carousel').trigger('destroy.owl.carousel');
				$(this).attr('href', '#')
	            $('.icons-buttons li a').click(function(e){
	                console.log('entre')
	                var mydata = $(this).data();
	                $(this).attr('href', mydata.href);
	                  });
		}else{
			//$(this).attr('href', '#')
	            $('.icons-buttons li a').click(function(e){
	                var mydata = $(this).data();
	                var mytext = (mydata.target);
	                //$('.owl-item.active').children('.item').addClass('active')
	                $(this).attr('href', '#' + mytext)
	                //$('.owl-carousel .owl-carousel').css({'height': myheight});
	            });
		}


	})
	.jcarousel({
		wrap: 'circular'
	});

	$('.jcarousel-control-prev')
		.jcarouselControl({
			target: '-=1'
	});

	$('.jcarousel-control-next')
		.jcarouselControl({
			target: '+=1'
	});

}
else{
	sync2.show();
	sync1.hide();
	$('.icons-buttons li a').click(function(e){
	    console.log('entre')
	    var mydata = $(this).data();
	    $(this).attr('href', mydata.href);
	});

}
	
	

	// jCarousel fin

	$(".single").slick({
        dots: true,
        autoplay: true,
  		autoplaySpeed: 3000,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1
      });

	$(".bkg-banner-simulador").show();

});

