$(document).ready(function() {

    var menuplan = $('.menu-item-plan');

    menuplan.click(function(){

        var nombreid = $(this).attr("id");
        var tab = $('.info-plan');

        menuplan.removeClass('active');
        tab.removeClass('active');

        $(this).addClass('active');
        tab.filter('[data-target="'+nombreid+'"]').addClass('active');

    });
});
