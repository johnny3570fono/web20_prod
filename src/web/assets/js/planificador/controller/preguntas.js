app.controller("plan", function($scope) {

	BaseController.call(this, $scope);
	$scope.tipoPreg=tipoPreg;

	function addTagForm($collectionHolder, $newLinkLi) {
		// Get the data-prototype explained earlier
		var prototype = $collectionHolder.data("prototype");

		// get the new index
		var index = $collectionHolder.data("index");

		// Replace '__name__' in the prototype's HTML to
		// instead be a number based on how many items we have
		var newForm = prototype.replace(/__name__/g, index);

		// increase the index with one for the next item
		$collectionHolder.data("index", index + 1);

		// Display the form in the page in an li, before the "Add a tag" link li
		var $newFormLi = $('<div id=resp class="col-md-3 well"></div>');
		// also add a remove button, just for this example
		if ($("select[id=Pregunta_form_tipo]").val() != 1){
			$newFormLi.append('<button type="button" class="close remove-tag">&times;</button>');
		}
		$newFormLi.append(newForm);


		if ($("select[id=Pregunta_form_tipo]").val() == 2){// rango

			$newFormLi.append('<div id="rango" class="myrow"> <div class="mytd datos-persona-item"> <label >Desde:</label> </div> <p>hi</p><input type="text" name="Pregunta_form[respuestas]['+index+'][Desde]"> </div>');
			$newFormLi.append('<div id=""rango class="myrow"> <div class="mytd datos-persona-item"> <label >Hasta:</label> </div>  <input type="text" name="Pregunta_form[respuestas]['+index+'][Hasta]"> </div>');

		}else{  // Seleccion simple ,multiple y true/false  
			$("#Pregunta_form_respuestas_5_imagen_file").attr('style','display:none')
			$newFormLi.append('<div class="col-sm-4 fileinput fileinput-new" data-provides="fileinput">'+
				'<label>Imagen</label>'+	
				'<div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"></div>'+
				'<div id="Pregunta_form_respuestas_'+index+'_imagen_file">'+
					'<span class="btn btn-default btn-file">'+
						'<span class="fileinput-new">Seleccionar </span>'+
						'<span class="fileinput-exists">Cambiar</span>'+
						'<div class="mytd datos-persona-item"></div>'+
				'<input id="Pregunta_form_respuestas_imagen_file" type="file" name="Pregunta_form[respuestas]['+index+'][imagen][file]"></span>'+
				'<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Eliminar</a></div>');

			$newFormLi.append('<div id="valor" class="myrow"> <div class="mytd datos-persona-item"> <label >Valor:</label><p>hi2</p> </div>  <input type="text" name="Pregunta_form[respuestas]['+index+'][Desde]"> </div>');
		}
		
		//$newFormLi = $('<div/>',{class:'col-md-12'}).append($newFormLi);
		console.log($newFormLi);
		$newLinkLi.before($newFormLi);
		// handle the removal, just for this example
		$(".remove-tag").click(function(e) {
			e.preventDefault();
			$(this).parent().remove();
			return false;
		});
	}

// handle the removal, just for this example
	$(".remove-tag").click(function(e) {
		e.preventDefault();
		$(this).parent().remove();
		return false;
	});

	var $collectionHolder;
	// setup an "add a tag" link
	var $addTagLink = $('<div id="add"><a href="#" class="btn btn-xs btn-info add_tag_link gris  text-align: right;">Añadir una Respuesta</a></div>');
	
	var $newLinkLi = $("<section></section>").append($addTagLink);

	jQuery(document).ready(function() {
		// Get the ul that holds the collection of tags
		$collectionHolder = $("div.respuestas");

		// add the "add a tag" anchor and li to the tags ul
		$collectionHolder.append($newLinkLi);

		// count the current form inputs we have (e.g. 2), use that as the new
		// index when inserting a new item (e.g. 2)
		$collectionHolder.data("index", $collectionHolder.find(":input").length);

		if ($("select[id=Pregunta_form_tipo]").val() == 2){// rango 
			$("#rango").show();
			$("#add").show();
			$("#valor").hide();
		}else if ($("select[id=Pregunta_form_tipo]").val() == 3){//seleccion
			$("#rango").hide();
			$("#add").show();
			$("#valor").show();
			 
		}else if ($("select[id=Pregunta_form_tipo]").val() == 1){
		  //  
			var value= $("input:text[id=Pregunta_form_titulo]").val();

			if (!value){
				addTagForm($collectionHolder, $newLinkLi);  // true andfalse
				addTagForm($collectionHolder, $newLinkLi);
			}

			$("#add").hide();
			$("#rango").hide();
			$("#valor").show();

		}else{
			$("#rango").hide();
			$("#add").show();
			$("#valor").show();
		}

		$addTagLink.on("click", function(e) {
			// prevent the link from creating a "#" on the URL
			e.preventDefault();

			// add a new tag form (see next code block)
			addTagForm($collectionHolder, $newLinkLi);
		});

		$("#Pregunta_form_tipo").change(function(){
			$("div").remove("#resp");
			if ($("select[id=Pregunta_form_tipo]").val() == 1){
				addTagForm($collectionHolder, $newLinkLi);
				addTagForm($collectionHolder, $newLinkLi);
				$("#add").hide();
				$("#rango").hide();
				$("#valor").show();
					
			}else{
				$("#add").show();
			}
		});

	});

});