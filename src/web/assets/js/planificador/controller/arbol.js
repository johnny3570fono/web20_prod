app.controller('plan', function($scope, $http) {
	BaseController.call(this, $scope);
	$scope.content = false;
	$scope.filtro=filtro;
	$scope.filtrog=filtrog; 
	$scope.filtrof=filtrof;
	$scope.filtroR=filtroR;
	$scope.filtroC=filtroC;
	$scope.totales=totalC;
	$scope.arrayvariables=arrayvariables;
	$scope.tiporesp=tiporesp;
	$scope.productos = productos;
	$scope.prodplani = prodplani;
	if(arr.length > 0){$scope.choices = arr;}else{$scope.choices = [];}
	
	$scope.arbol = arbol;
	$scope.padre = $scope.arbol.padre
	$scope.gen = $scope.arbol.tipo
	$scope.hijo = $scope.arbol.preg
	$scope.preo = $scope.arbol.preo

  	if($scope.filtro!=null){
      $scope.opciones = filtro;
    }
    if($scope.filtrog!=null){
      $scope.opcionesg = filtrog;
    }
     if($scope.filtrof!=null){
      $scope.opcionesf = filtrof;
    }
    if($scope.filtroR!=null){
      $scope.opcionesR = filtroR;
    }
    if($scope.filtroC!=null){
      $scope.capital=$scope.filtroC[0].capital;  
      $scope.opcionesC = filtroC;
    
      /*angular.forEach($scope.arrayvariables, function(value, key){
        if(value.idcapital == $scope.capital){
          $scope.opcionesC.push({
          "capital":value.idcapital,
          "variable":value.variable
          })
        }
      }) */
     
      
    }
	if($scope.totales!=null){
    	$scope.totalesC=totalC;

	}

  	$scope.addNewChoice = function(){
	    var newItemNo = $scope.choices.length+1;
	    $scope.choices.push({
	      'id':newItemNo,
	      'producto': $scope.product,
	      'plan': $scope.plans
	    });
	};
	$scope.removeChoice = function() {
		var lastItem = $scope.choices.length-1;
		    $scope.choices.splice(lastItem);
	 };
/* filtro basico */
  $scope.addNewFiltro = function($val) {
    if($val==1){
      if($scope.opciones==null){
        $scope.opciones = [{id: 1}];
      }else{
        var newItemFiltro = $scope.opciones.length+1;
        $scope.opciones.push({'id':newItemFiltro});
      }
    }else if($val==2){
      if($scope.opcionesg==null){
         $scope.opcionesg = [{id: 1}];
      }else{
        var newItemFiltro = $scope.opcionesg.length+1;
        $scope.opcionesg.push({'id':newItemFiltro});
      }
    }else if($val==3){
      if($scope.opcionesf==null){
        $scope.opcionesf = [{id: 1}];
      }else{
        var newItemFiltro = $scope.opcionesf.length+1;
        $scope.opcionesf.push({'id':newItemFiltro});
      }
      }else if($val==4){
        if($scope.opcionesR==null){
          $scope.opcionesR = [{id: 1,opcion:1}];
        }else{
          var newItemFiltro = $scope.opcionesR.length+1;
          $scope.opcionesR.push({'id':newItemFiltro});
        }
      }
  
      
  };

  $scope.removeFiltro = function($val) {
    if($val==1){
      console.log($scope.opciones);
    var lastItemFiltro = $scope.opciones.length-1;
    $scope.opciones.splice(lastItemFiltro);
  }else if($val==2){
    var lastItemFiltro = $scope.opcionesg.length-1;
    $scope.opcionesg.splice(lastItemFiltro);

    }else if($val==3){ 
    var lastItemFiltro = $scope.opcionesf.length-1;
    $scope.opcionesf.splice(lastItemFiltro);
    }else if($val==4){ 
    var lastItemFiltro = $scope.opcionesR.length-1;
    $scope.opcionesR.splice(lastItemFiltro);
    }
  
  };


    $scope.change = function(){   
      $scope.opcionesC = [];    
      angular.forEach($scope.arrayvariables, function(value, key){
        if(value.idcapital == $scope.capital){
          $scope.opcionesC.push({
          "capital":value.idcapital,
          "variable":value.variable
          })
        }
      }) 
      $scope.totalesC=[];
        $scope.totalesC.push({
          "simbolototal":$scope.simbolototal
     
          })
	}
  $scope.removeCapital=function(){
    $scope.opcionesC=null;
    $scope.totalesC=null;
    $scope.capital=null;
    $scope.opcionesvalueC= JSON.stringify($scope.opcionesC);
    $scope.totalesCvalor= JSON.stringify($scope.totalesC);
   
  }

  $scope.saveChoice = function(){

	 $scope.totalesCvalor= JSON.stringify($scope.totalesC);

    $scope.opcionesvalue= JSON.stringify($scope.opciones);
    $scope.opcionesvalueg= JSON.stringify($scope.opcionesg);
    $scope.opcionesvaluef= JSON.stringify($scope.opcionesf);
    $scope.opcionesvalueR= JSON.stringify($scope.opcionesR);
    $scope.opcionesvalueC= JSON.stringify($scope.opcionesC);

    $('#Arbol_form_json_total').val($scope.totalesCvalor);
    $('#Arbol_form_choicesjson').val($scope.opcionesvalue);
    $('#Arbol_form_choicesjsongastos').val($scope.opcionesvalueg);
    $('#Arbol_form_choicesjsonfamilia').val($scope.opcionesvaluef);
    $('#Arbol_form_choicesjsonrespuesta').val($scope.opcionesvalueR);
    $('#Arbol_form_choicesjsonCapital').val($scope.opcionesvalueC);
   


     var datos = JSON.stringify($scope.choices);
     $('#Arbol_form_choices').val(datos);

	}

	
  /* fin filtro */
//incluir en caso de que los rangoas cambien a '<'' '<>' '='
/* $(document).on('change','.filtro_form_preg_ant', function(event) {
      var opt = $(this).find('option:selected')
      var tiporesp = opt.data('tipo')
      var id = $(this).find('option:selected')
      var idciclo = id.data('id')

      
      //console.log(tiporesp)

      if( $('#filtro_form_simbolo').hasClass(idciclo).toString() ){
          
          $('#filtro_form_simbolo option:selected').removeAttr("selected");
          if(tiporesp==2){

            $('.seleccion'+idciclo).attr('style','display:none')
            $('.simbolos'+idciclo).attr('style','display:block')
        }else{
            $('.simbolos'+idciclo).attr('style','display:none')
            $('.seleccion'+idciclo).attr('style','display:block')
        }
          
        
      }


      $scope.tiporesp=tiporesp;
      $scope.opcionesR[idciclo].opcion=$scope.tiporesp
      //console.log($scope.opcionesR[idciclo].opcion)
     
  });

  */

  $scope.plan= [];
  $(document).on('change', '.prod', function(event){
    var ind = $(this).data('index')

    var id = $(this).val();
    $scope.prod_id =  $(this).val();

   
      angular.forEach($scope.productos, function(value, key){
        if(value.id == id){
          $scope.planes = key;
        }
      })
    $scope.choices[ind].producto = id;
    //$scope.$emit("pln", "final");
  });
  /*$scope.$on("pln", function(event, message){
        //$scope.visible = 0;
        if (message == 'final'){
          $scope.prod_id;
         }
        
    });*/

  // -------- PLANES -----
  angular.forEach($scope.productos, function(r, key){
    angular.forEach(r,function(q,key){
      if (q.idplan != undefined) {
      $scope.plan.push({
          "id":q.idplan,
          "prod":r.id,
          "nom":q.nombre_plan
      })
    }
    })
  })
  $(document).on('change','.pln', function(event) {
      var ind= $(this).data('index');
      var id = $(this).val();
      $scope.choices[ind].plan = id
      
      var datos = JSON.stringify($scope.choices);
     $('#Arbol_form_choices').val(datos);
  });
    

})
.directive("loadFirst", function(){
  return {
    restrict: "A",
    link:function(scope, iElem, iAttrs){
      $scope.butt = 1
    }
  }
});
