app.controller("plan", function($scope) {
	BaseController.call(this, $scope);

	$scope.asociacion = preo.split(",");

	console.log($scope.preo);

	$scope.addAsoc = function(value){

		$("#"+value).addClass("verde");
		var val = value.toString();
		var i = $scope.asociacion.indexOf(val);
		if ( i !== -1 ){
			$scope.asociacion.splice( i, 1 );
			$("#"+value).removeClass("verde");
			$("#"+value).addClass("blanco");
		}
		else{
			$scope.asociacion.push(val);
			$("#"+value).addClass("verde");
			$("#"+value).removeClass("blanco");
		}
		$("#Asociacion_form_preocupacion").val($scope.asociacion);
		console.log($scope.asociacion);
	}

	$(document).ready(function(){
		angular.forEach($scope.asociacion, function(value){
			$("#"+value).addClass("verde");
		});
	});
});