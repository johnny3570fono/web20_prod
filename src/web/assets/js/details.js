$(document).ready(function() {
    var menuplan = $('.menu-item-plan');

    menuplan.click(function(){

        var nombreid = $(this).attr("id");
        var tab = $('.info-plan');

        menuplan.removeClass('active');
        tab.removeClass('active');

        $(this).addClass('active');
        tab.filter('[data-target="'+nombreid+'"]').addClass('active');

    });

    var clickDeclaracion = $('.tit-declaracion');

    clickDeclaracion.click(function(){
        $(this).toggleClass('active');
        $(this).parent().next().slideToggle(300);
    });

    $('div.box-declaracion.selected_plan').click(function(){
        $('#step_first_selected').show();
        $('#plans').show();
        $('#step1').show();
        $('div.box-declaracion.selected_plan').hide();
    });

    $(document).ready(function(){
        $('.single-item').slick();
       
    });


   /*$('button.btn-continuar').click(function(){
    var valor = $('#insurance_insured_addressCellphone_telephone').val()
        if(valor.length < 7 ){
            $('#insurance_insured_addressCellphone_telephone').attr('aria-invalid','true')
            $('#insurance_insured_addressCellphone_telephone').parent('div').addClass('error')
            console.log('hi1')
            event.preventDefault();
        }else{
            console.log('hi')
        }
         
    });*/

    var hash = window.location.hash;

    if (hash) {
        $('a[href=' + hash + ']').addClass('active');
    } else {
        $('ul.icons-buttons li').eq(0).find('a').addClass('active');
    }


    $('.own-carousel-menu, .own-carousel-menu-plan').owlCarousel({
        items : 4,
        itemsCustom: [
            [0, 1],
            [200, 1],
            [400, 2],
            [500, 3],
            [700, 4],
        ],
        responsiveBaseWidth: 'body > .container',

        navigation: true,
        navigationText : ['<div class="jcarousel-control-prev">‹</div>','<div class="jcarousel-control-next">›</div>'],
        pagination : false,

        responsive: true,
    });

    //Enable validation form
    $("#superform").validate({
        success: function(label,element) {
            $(element).parent().removeClass('error');
        },
        errorPlacement: function(error,element) {
            return true;
        },
        onfocusout:function(el,ev){
            return false;
            /*if($(el).valid()){
                $(el).parent().removeClass('error');
            }*/
        },
        highlight : function(element,errorClass){
            $(element).parent().addClass('error');
        },
        unhighlight : function(element,errorClass){
            $(element).parent().removeClass('error');
        }
    });

   //Calendario Pikaday
    var fecha = new Date()
    fecha.setMonth(fecha.getMonth() + 6)
    var dt = $("#insurance_travelDate").attr("data-days");
    fecha.setDate(fecha.getDate()-dt)
    
    var picker = new Pikaday({
            field: document.getElementById('insurance_travelDate'),
            format: 'DD-MM-YYYY',
            minDate: new Date(),
            defaultDate: new Date(),
            setDefaultDate : new Date(),
            maxDate: fecha,

            i18n : {
                previousMonth : 'Mes Anterior',
                nextMonth     : 'Siguiente Mes',
                months        : ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
                weekdays      : ['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'],
                weekdaysShort : ['Dom','Lun','Mar','Mie','Jue','Vie','Sab']
            },
            onSelect : function(){
                $("#insurance_travelDate").trigger('cambiar',[null]);
                
            },
            onClose: function(fecha) {

            },
            onOpen: function(){
                fecha = new Date()
                //picker.gotoToday(fecha)
                fecha.setMonth(fecha.getMonth() + 6)
                dt = $("#insurance_travelDate").attr("data-days");
                fecha.setDate(fecha.getDate()-dt)
            }

        });
       $(document).on('click', '.plan', function(event) {
        event.preventDefault();
        /* Act on the event */
        fecha = new Date()
        fecha.setDate(fecha.getDate())
        
        test = moment(fecha.getDate(),"DD-MM-YYYY").format("DD-MM-YYYY")
       
        $("#p.datepicker").html($("#insurance_travelDate").val(test));
       
        });
    
    $("#insurance_travelDate").on('cambiar',function(e,d){
        var dt = $("#insurance_travelDate").attr("data-days");
        if(d !== null){
            dt= d;
        }
        $("p.datepick").html(moment($("#insurance_travelDate").val(),"DD-MM-YYYY").add(dt-1, 'days').format("DD-MM-YYYY") );
    });


    //MASKS
    $(".telefono").mask('000000000');
    $(".rut").mask('XX.000.000-Z', {
        placeholder: '__.___.___-_',
        reverse : true,
        translation : {
            'X' : {
                pattern : /[0-9]/
            },
            'Z' : {
                pattern : /[0-9 | K | k]/
            }
        }
    });
    $(".peso").mask('XXX',{
        placeholder: '',
        translation : {
            'X': {
                pattern : /[0-9]/
            }
        }
    });
    $(".estatura").mask('XXX',{
        placeholder: '',
        translation : {
            'X': {
                pattern : /[0-9]/
            }
        }
    });

});

      /*function mayorEdad(id){
          console.log($('#' + id).val());
      }*/

/*********/
function showAlert(id){
    $("#"+  id).parent().removeClass('showAlert');
}
function validateSteps(step,callback){
    if($("div.info-declaracion")[step -1] ){
        var allValid = true;
        $("div.info-declaracion").eq(step -1).find('input,select').each(function(k,value){
            if(!$(value).valid()){
                allValid=false;
            }
        });
        if(typeof(callback) =='function'){
            callback(allValid);
        }else{
            return allValid;
        }
    }else{
        if(typeof(callback) =='function'){
            callback(false);
        }else{
            return false;
        }
    }
}
/*********/
$.validator.addMethod("rut", function(value, element) {
    return this.optional(element) || $.Rut.validar(value);
}, "Este campo debe ser un rut valido.");

$.validator.addMethod("capital", function(value, element) {
    var current=0;
    $(".capital").each(function(i,v){ current += parseInt($(v).val()); });
    return ( (current > 100 || current < 1) ? false : true);
}, "La suma de todos los capitales no puede ser mayor que 100.");

/*$.validator.addMethod("edad", function(value,element){
    var person = $(element).data("person");
    if($("select.referSelect[data-person=" + person + "]").val() != 'Hijo' && $("select.referSelect[data-person=" + person + "]").val() != 'Cónyuge'){
        $("select[id$='" +  person + "_birthday_year']").parent().removeClass('showAlert');
        return true;
    }else{
        var dia = $("select[id$='" +  person + "_birthday_day']").val();
        var mes = $("select[id$='" +  person + "_birthday_month']").val();
        var anio = $("select[id$='" +  person + "_birthday_year']").val();
        var parentError = $("select[id$='" +  person + "_birthday_year']").parent().hasClass('showAlert');
        var currentEdad = new Date(anio,mes,dia);
        var edad = new Date(Date.now() - currentEdad.getTime());
        edad = Math.abs(edad.getUTCFullYear() - 1970);
        switch ($("select.referSelect[data-person=" + person + "]").val() ){
            case "Hijo" :
                if((edad < 25 && edad >= 0)){
                    $("select[id$='" +  person + "_birthday_year']").parent().removeClass('showAlert');
                    return true;
                }else{
                    if(!isNaN(edad) && !parentError){
                        $("select[id$='" +  person + "_birthday_year']").parent().addClass('showAlert');
                        //alert("La edad de hijo/hija debe ser menor o igual a 24 años");
                        newAlert("La edad de hijo/hija debe ser menor o igual a 24 años");
                    }
                    return false;
                }
                //return ( (edad < 25 && edad >= 0) ? true:false );
                break;
            case "Cónyuge":
                if( (edad > 17 ) ){
                    $("select[id$='" +  person + "_birthday_year']").parent().removeClass('showAlert');
                    return true;
                }else{
                    if(!isNaN(edad) && !parentError ){
                        $("select[id$='" +  person + "_birthday_year']").parent().addClass('showAlert');
                        //alert("La edad del conyuge debe ser mayor o igual a 18 años");
                        newAlert("La edad del conyuge debe ser mayor o igual a 18 años");
                    }
                    return false;
                }
                //return( (edad > 17 ) ? true : false );
                break;
            default:
                $("select[id$='" +  person + "_birthday_year']").parent().removeClass('showAlert');
                return true;
        }
    }
}, "Rango de edad no permitido");*/
/*********/

function newAlert($mensaje){
    $('.error-box .error-content').html($mensaje);
    $.featherlight($('.error-box'), {closeIcon: '', otherClose: '.error-cerrar'});
    $('.featherlight-content').addClass('error-border');
}
function newAlert2($mensaje){
    $('.error-box .error-content').html($mensaje);
    $.featherlight($('.error-box'), { closeIcon: '', otherClose: '.error-cerrar'});
    $('.featherlight-content').addClass('error-border');
}

function setInsured(value)
{
    if (value == 1) {
        $('input[data-value]').each(function(){
            $(this).val($(this).data('value'));
        });

        $('select[data-value]').each(function(){
            $(this).val($(this).data('value'));
        });
    } else {
        $('input[data-value]').each(function(){
            $(this).val('');
            $(this).parent(".error").removeClass("error");
        });
        $('select[data-value]').each(function(){
            $(this).val('');
            $(this).parent(".error").removeClass("error");
        });
    }
    $('#insurance_payerEqualsInsured').val(value);
}


