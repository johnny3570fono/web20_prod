app.controller('details', function ($scope, $http) {

    BaseController.call(this, $scope);

    $scope.beneficiaries = beneficiaries;
    $scope.plans = plans;
    $scope.items = relationships;
    $scope.errors = errors;
    $scope.coverages = coverages;
    $scope.submit = 4
    $scope.cover = cover;
    $scope.edad = parseInt($scope.age());

    var age = $scope.age();
    $scope.plan.id = $('#plan').data('valor')
    if ($scope.cover.length > 0) {
        $scope.cover.forEach(function (element, index, data) {
            element.forEach(function (e, i, d) {
                if (e.plan.id == $scope.plan.id)
                    if (age >= e.start && age <= e.finish) {
                        $scope.updatePrice(e.price);
                        $scope.target.start = e.start;
                        $scope.target.finish = e.finish;
                        $scope.target.price = e.price;
                        $scope.prima_uf = parseFloat(e.price)
                        $scope.prima = $scope.upPrice($scope.prima_uf * $scope.uf)
                        $('#insurance_price').val($scope.prima_uf)
                    }
            });
        });
    }
    if ($scope.edad > $scope.target.finish | $scope.edad < $scope.target.start) {
        $("#insurance_insured_birthday_year").attr("required", "required");
        $("#insurance_insured_birthday_year").attr("aria-required", "true");
        $("#insurance_insured_birthday_year").attr("aria-invalid", "true");
        $scope.prima = 0;
    }
    $(document).ready(function () {
        $http.get('/customers/' + token + '/rest/product/' + product.id)
                .success(function (response, status, headers, config) {
                    $scope.targets = response;
                }).error(function (error, status, headers, config) {

        })
    })

    $('button.btn-continuar').click(function () {
        var codv = parseInt($('#insurance_insured_addressCellphone_code').val())
        console.log(codv)
        if (codv < 10) {
            var ref = 8
        } else {
            var ref = 7
        }
        var valor = $('#insurance_insured_addressCellphone_telephone').val()
        $('#insurance_insured_addressCellphone_telephone').parent('div').removeClass('error')
        $('#insurance_insured_addressPhone_telephone').removeAttr('required');
        if ($scope.age() >= $scope.product.ageMin && $scope.age() < $scope.product.ageMax && valor.length == ref) {
            $('#next1').attr('style', 'display:block;');
            var stepNum = parseInt($(this).data('step'));
            var nextNum = parseInt($(this).data('next'));
            var existStep = document.getElementById('next' + nextNum);
            while (existStep && $('#next' + nextNum).is(':visible') == false) {
                nextNum = nextNum + 1;
                existStep = document.getElementById('next' + nextNum);
            }
            next = $('#next' + nextNum);

            if (validateSteps(stepNum)) {
                if (stepNum == 1) {
                    $('#step_first_selected').hide();
                    $('#plans').hide();
                    $('#step1').slideToggle(300);
                    $('div.box-declaracion.selected_plan').show();
                    if ($scope.beneficiaries.length <= 0) {
                        $('#next2').hide();
                        $('#next3').show();
                        $('#next3').addClass('active');
                        if ($('#next3').hasClass('active'))
                        {
                            $('#info-declaracion-3').removeClass('hidden');
                            $('#info-declaracion-3').css('display', 'block');
                            //$('#next'+stepNum).removeClass('active');
                            //$('#info-declaracion-3').slideToggle(300);
                            //$('#next'+stepNum).parent().next().slideToggle(300);  
                        }
                    } else
                    {
                        $('#next2').show();
                        $('#next2').addClass('active');
                        if ($('#next2').hasClass('active'))
                        {
                            $('#info-declaracion-2').removeClass('hidden');
                            $('#info-declaracion-2').css('display', 'block');
                        }
                        $('#next3').hide();
                    }
                    $('#next4').hide();
                } else if (stepNum == 2)
                {
                    $scope.erro = 0
                    $scope.cod = true
                    if ($scope.beneficiaries.length > 0)
                    {
                        var val = 0
                        for (var i = 0; i < $scope.beneficiaries.length; i++) {
                            val = val + 1
                            for (var j = val; j < $scope.beneficiaries.length; j++) {
                                if ($scope.beneficiaries[j].code == $scope.beneficiaries[i].code)
                                {

                                    newAlert("Los rut son iguales")
                                    $scope.beneficiaries[j].code = ''
                                    event.stopPropagation();
                                    event.preventDefault();
                                    $scope.erro = $scope.erro + val
                                }
                            }
                        }
                    }
                    if (validateSteps(stepNum))
                    {
                        $('#info-declaracion-2').slideToggle(300);
                        $('#next2').removeClass('active');
                        $('#next3').show();
                        $('#next3').addClass('active');
                        $('#info-declaracion-3').slideToggle(300);
                    }
                } else if (stepNum == 3)
                {
                    if (validateSteps(stepNum))
                    {
                        $('#info-declaracion-3').slideToggle(300);
                        $('#next3').removeClass('active')
                        $('#next4').show();
                        $('#next4').addClass('active');
                        $('#info-declaracion-4').slideToggle(300);

                    }
                } else {

                    $('#next4').removeClass('active');
                    $('#info-declaracion-4').slideToggle(300);
                    $('#mensajeespera').fadeIn(300);
                }
            }
            if (stepNum == 1)
            {
                if (validateSteps(stepNum)) {
                } else
                {
                    $('#next2').hide();
                    $('#next3').hide();
                    $('#next6').hide();
                    $('#next5').hide();
                    $('#next4').hide();
                }
                $('#next5').hide();
                $('#next4').hide();
            }
        } else
        {
            if (stepNum == 1)
            {
                $('#next2').hide();
                $('#next3').hide();
                $('#next6').hide();
                $('#next5').hide();
                $('#next4').hide();
            }
            event.preventDefault()
            if ($scope.age() <= $scope.product.ageMin || $scope.age() >= $scope.product.ageMax)
                newAlert('Estimado cliente la edad para contratar el seguro debe estar entre ' + $scope.product.ageMin + ' y ' + ($scope.product.ageMax - 1) + ' años')

            if (valor.length != ref) {
                $('#insurance_insured_addressCellphone_telephone').attr('aria-invalid', 'true')
                $('#insurance_insured_addressCellphone_telephone').parent('div').addClass('error')
            }
        }
    });
    $scope.volver = function (step, next) {
        $('.regular-input').removeAttr('required')
        $('.regular-check').removeAttr('required')
        var stepNum = parseInt(step);
        var nextNum = parseInt(next);
        var existStep = nextNum;
        while (existStep && $('#next' + nextNum).is(':visible') == false) {
            nextNum = nextNum + 1;
            existStep = document.getElementById('next' + nextNum);
        }
        if (validateSteps(step))
        {
            if (step == 2)
            {
                $('#next1').attr('style', 'display:none;');
                $('#step1').slideToggle(300)
                $('#plans').slideToggle(300)
                $('#next2').slideToggle(300)
                $('#info-declaracion-2').slideToggle(300)
            }
            if (step == 3)
            {

                if ($scope.beneficiaries.length <= 0) {
                    $('#next1').attr('style', 'display:none;');
                    $('#step1').slideToggle(300)
                    $('#plans').slideToggle(300)
                    $('#next3').slideToggle(300)
                    $('#info-declaracion-3').slideToggle(300)
                } else {

                    $('#next3').removeClass('active')
                    $('#next2').attr('style', 'block;');
                    $('#next2').addClass('active')
                    $('#info-declaracion-2').slideToggle(300)
                    $('#next3').attr('style', 'display:none;');
                    $('#info-declaracion-3').slideToggle(300)
                }
            }
            if (step == 4)
            {
                $('#next4').removeClass('active')
                $('#next3').attr('style', 'block;');
                $('#next3').addClass('active')
                $('#info-declaracion-3').slideToggle(300)
                $('#next4').attr('style', 'display:none;');
                $('#info-declaracion-4').slideToggle(300)
            }

        }

        $('.regular-input').attr('required', 'required')
        $('.regular-check').attr('required', 'required')
        $('#insurance_insured_addressPhone_telephone').removeAttr('required');
        $('#insurance_insured_addressPhone_code').removeAttr('required');
    }

    $scope.ageConyuge = function (birthday) {
        var birthday = birthday;
        if (birthday == false) {
            birthday = new Date();
        }
        var age = (new Date().getFullYear() - birthday.getFullYear());

        if (age < 1) {
            age = 0;
        }
        return age;
    };
    $scope.getConyugeBirthday = function (beneficiarie) {
        var year = parseInt(beneficiarie.year);
        var month = parseInt(beneficiarie.month);
        var day = parseInt(beneficiarie.day);

        if (isNaN(year) || isNaN(month) || isNaN(day)) {
            return false;
        }
        if (year != "" && month != "" && day != "") {
            var date_string = year + "-" + month + "-" + day;

            if ($scope.isDate(year, month, day)) {
                return new Date(year, month, day);
            }
        }

        return false;
    };
    $scope.conyugeEdad = function () {
        for (var i = 0; i < $scope.beneficiaries.length; i++) {
            if ($scope.beneficiaries[i].relationship == 'Cónyuge') {
                return ($scope.ageConyuge($scope.getConyugeBirthday($scope.beneficiaries[i])));
            }
        }
    };

    $scope.selectRel = function (value, idHidden) {
        var elemento = $scope.getObjects(relationships, 'value', value);
        if (elemento.length > 0) {
            elemento = elemento[0];
            
            if ((elemento.maxapparition !== -1) &&
                    ($('select.referSelect option[value="' + elemento.value + '"]:selected').length > elemento.maxapparition)) {
                $('select.referSelect[data-person=' + idHidden + '] option:eq(0)').prop('selected', true);
                $scope.beneficiaries[idHidden].relationship = "";
            }
        }
    };
    $scope.traerCobertura = function (plan_id) {
        $scope.cober = []
        for (var i = 0; i < $scope.coverages.length; i++) {
            if ($scope.coverages[i].plan.id == plan_id)
            {
                $scope.cober.push({
                    'capital': $scope.coverages[i].capital,
                    'id': $scope.coverages[i].id,
                    'name': $scope.coverages[i].name
                })
            }
        }
    }
    $scope.planToggle = function (plan_id) {

        var dato = $scope.georly[$('#insurance_insured_addressState').val()]
        $('#insurance_insured_addressCity').val(dato)
        $('#ciudad').html(dato)
        //$('#next2').hide();
        var cargas = 0
        $scope.beneficiaries = [];
        for (var i = 0; i < $scope.plans.length; i++) {
            
            if (plan_id == $scope.plans[i].id) {
                if ($scope.plans[i].cargasMin > $scope.beneficiaries.length) {
                    cargas = parseInt($scope.plans[i].cargasMin)
                    for (var j = 0; j < cargas; j++) {
                        $scope.beneficiaries.push({
                            name: '',
                            lastname1: '',
                            lastname2: '',
                            code: '',
                            relationship: '',
                            capital: 0,
                            day: null,
                            month: null,
                            year: null
                        });
                    }
                }
            }
        }
        
        $scope.edad = $scope.age()
        $scope.traerCobertura(plan_id)
        var age = ($scope.age() < $scope.conyugeEdad() ? $scope.conyugeEdad() : $scope.age());
        var target = 'target';

        $scope.plan = $scope.findPlan(plan_id);

        $('#insurance_plan').val($scope.plan.id);
        if ($scope.cover.length > 0) {
            $scope.cover.forEach(function (element, index, data) {
                element.forEach(function (e, i, d) {
                    if (e.plan.id == $scope.plan.id) {

                        if (age >= e.start && age <= e.finish) {
                            console.log("c");
                            $scope.updatePrice(e.price);
                            $scope.target.start = e.start;
                            $scope.target.finish = e.finish;
                            $scope.target.price = e.price;
                            $scope.prima_uf = parseFloat(e.price)
                            $scope.prima = $scope.upPrice($scope.prima_uf * $scope.uf)
                            console.log($scope.prima);
                            $('#insurance_price').val($scope.prima_uf)

                        }
                    }
                });
            });
        }

        if ($scope.edad > $scope.target.finish | $scope.edad < $scope.target.start) {
            if ($("#insurance_insured_birthday_day").val() != null && $("#insurance_insured_birthday_month").val() != null && $("#insurance_insured_birthday_year").val() && null)
            {
                newAlert("La edad selecionada se encuentra fuera del rango permitido")
            }
            $("#insurance_insured_birthday_year").attr("required", "required");
            $("#insurance_insured_birthday_year").attr("aria-required", "true");
            $("#insurance_insured_birthday_year").attr("aria-invalid", "true");
            $scope.prima = 0;
        }

        $scope.activeCampaign($scope.plan);
    };
});
