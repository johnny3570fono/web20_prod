app.controller('selectCtrl',function($scope){
    $scope.items = relationships;
    $scope.relacion = null;

    //{ value : string , maxapparition: -1=infinito or >0}
    $scope.selectRel = function(value,idHidden){
      if(value == null || !value.value){
        $("input[persona=" + idHidden + "]").val('');
        return;
      }

      var elemento = getObjects(relationships,'value',value.value);
      if(elemento.length > 0){
        elemento = elemento[0];
        if( (elemento.maxapparition !== -1) &&
          ($('select.referSelect option[value="' + elemento.value + '"]:selected').length > elemento.maxapparition ) ){
          $scope.relacion = null;
          $("input[persona=" + idHidden + "]").val('');
        }else{
          $("input[persona=" + idHidden + "]").val(value.value);
        }
      }
    }

    function getObjects(obj, key, val) {
      var objects = [];
      for (var i in obj) {
          if (!obj.hasOwnProperty(i)) continue;
          if (typeof obj[i] == 'object') {
              objects = objects.concat(getObjects(obj[i], key, val));
          } else if (i == key && obj[key] == val) {
              objects = obj;
          }
      }
      return objects;
    }
});
