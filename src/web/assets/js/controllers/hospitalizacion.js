app.controller('details', function($scope, $http) {
    BaseController.call(this, $scope);
    $scope.visible = 0;
    $scope.submit = 1;
    $scope.coverages = coverages;
    $scope.items = relationships;
    $scope.plans = plans;
    $scope.beneficiaries = beneficiaries;
    $scope.errors = errors;

    $('button.btn-continuar').click(function(){
       var codv =  parseInt($('#insurance_insured_addressCellphone_code').val())
        console.log(codv)
        if (codv < 10){
            var ref = 8
        }else{
            var ref = 7
        }
        var valor = $('#insurance_insured_addressCellphone_telephone').val()
        $('#insurance_insured_addressCellphone_telephone').parent('div').removeClass('error')
        $('#insurance_insured_addressPhone_telephone').removeAttr('required');
        if( $scope.age() >= $scope.product.ageMin && $scope.age() < $scope.product.ageMax && valor.length == ref){
        var stepNum = parseInt($(this).data('step'));
        var nextNum = parseInt($(this).data('next'));
        var existStep = document.getElementById('next' + nextNum);

        while(existStep && $('#next' + nextNum).is(':visible') == false){
            nextNum = nextNum + 1;
            existStep = document.getElementById('next' + nextNum);
        }
        next = $('#next' + nextNum);
        
        if (validateSteps(stepNum)) {
            if (stepNum == 1) {

                $('#step_first_selected').hide();
                $('#plans').hide();
                $('#step1').slideToggle(300);
                $('div.box-declaracion.selected_plan').show();
                $('#next1').attr('style','display:block;');
            if($scope.beneficiaries.length <= 0){
                $('#next2').show();
                $('#next2').addClass('active');
                if($('#next2').hasClass('active'))
                {

                    $('#info-declaracion-2').removeClass('hidden');
                    $('#info-declaracion').css('display','block'); 
                    //$('#next'+stepNum).removeClass('active');
                    $('#next'+stepNum).parent().next().slideToggle(300);
                    $('#mensajeespera').fadeIn(300);  
                }
            }
            }

            $('#next2').show();
            $('#next2').removeClass('active');
            $('#info-declaracion-2').slideToggle(300);
             if (validateSteps(stepNum == 2))
                    $('#mensajeespera').fadeIn(300); 

        }

        }else
        {
            $('#next2').attr('style', 'display:none');
            event.preventDefault()
            //$('.regular-select').attr('required','required')
            if($scope.age() <= $scope.product.ageMin || $scope.age() >= $scope.product.ageMax)
                newAlert('Estimado cliente la edad para contratar el seguro debe estar entre '+$scope.product.ageMin+' y '+($scope.product.ageMax-1)+' años')

            if(valor.length != ref){
                $('#insurance_insured_addressCellphone_telephone').attr('aria-invalid','true')
                $('#insurance_insured_addressCellphone_telephone').parent('div').addClass('error')
            }
        }
    });
    $scope.setPrice = function()
    {
       /* priceForCarga = $scope.plan.price;
        for (var i = 0, len = $scope.plan.cargasPrice.length; i < len; i++) {
          if ($scope.plan.cargasPrice[i].value == $scope.beneficiaries.length) {
              priceForCarga = $scope.plan.cargasPrice[i].price;
          }
        }
        //$scope.updatePrice(priceForCarga);
        */
        $scope.pri = ($scope.beneficiaries.length +1)*$scope.plan.price
        $scope.prima = $scope.pri * $scope.uf
        $scope.prima_uf = $scope.plan.price * ($scope.beneficiaries.length+1)
        $('#insurance_price').val($scope.pri)
        
    }

    $scope.$on("BENEFICIARY", function(event, message){
        $scope.submit = 1;
        if ($scope.beneficiaries.length > 0) {
            $scope.submit = 2;
        }

        $scope.setPrice();
    });

    $scope.filterCoverage = function(coverage){
        return ($scope.plan.id == coverage.plan.id);
    };

    $scope.planToggle = function(plan_id) {

        var dato = $scope.georly[$('#insurance_insured_addressState').val()]
        $('#insurance_insured_addressCity').val(dato)
        $('#ciudad').html(dato)

        $('#next2').hide();
        $scope.plan = $scope.findPlan(plan_id);
        //entrega valor prima
        $scope.prima_uf = parseFloat($scope.plan.price)
        $scope.pri = $scope.prima_uf * ($scope.beneficiaries.length +1)
        $scope.prima = $scope.upPrice($scope.prima_uf * $scope.uf)
        $('#insurance_price').val($scope.pri)
        //console.log($scope.prima_uf)
        //fin

        $('#insurance_plan').val($scope.plan.id);

        $scope.setPrice();
        $scope.activeCampaign($scope.plan);
    };


    $scope.volver = function(step,next){
        $('.regular-input').removeAttr('required')
        $('.regular-select').removeAttr('required')
        //lugar
        var stepNum = parseInt(step);
        //paso anterior
        var nextNum = parseInt(next);
        //paso antior 2.0 xD
        var existStep = nextNum;
        //console.log($('#next' + nextNum).is(':visible') == false)    retorna false
        while(existStep && $('#next' + nextNum).is(':visible') == false){
            nextNum = nextNum + 1;
            existStep = document.getElementById('next' + nextNum);
            console.log(existStep)
        }
        if(validateSteps(step))
        {
            //$('#box-declaracion').attr('display','block');
            $('#next2').attr('style','display:none;');
            $('#info-declaracion-2').slideToggle(300)
            $('#next1').attr('style','display:none;');
            //$('#next2').addClass('active')
            $('#plans').slideToggle(300)
            $('#step1').slideToggle(300)
        }

      $('.regular-input').attr('required','required')
      $('.regular-select').attr('required','required')
      $('#insurance_insured_addressPhone_telephone').removeAttr('required');
      $('#insurance_insured_addressPhone_code').removeAttr('required');
    }
});
