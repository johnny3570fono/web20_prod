app.controller('details',['$scope','$http', function($scope, $http,$filter) {
    BaseController.call(this, $scope);

    $scope.submit = 6;
    $scope.coverages = coverages;
    $scope.plans =  plans;
    $scope.items = relationships;
    $scope.beneficiaries = beneficiaries;
    $scope.errors = errors;
    $scope.renta = rentas;
    $scope.product = product;
    $scope.curso_aseg = 1;
    $scope.cover = cover; 
    $scope.dato = $scope.plans[0].id
    var dia = $('#insurance_insured_birthday_day').val()
    var mes = $('#insurance_insured_birthday_month').val()
    var anio = $('#insurance_insured_birthday_year').val()
    $scope.fecha = dia+'-'+mes+'-'+anio
    $scope.code = code;
    $scope.traercurso = cursos

    $scope.$on("BENEFICIARY", function(event, message){
        $scope.submit = 1;
        if (message == 'MORE') {
            $scope.submit = 2;
        }
        $scope.setPrice();
    });

    $('button.btn-continuar').click(function(){

        console.log($scope.prima) 
        var codv =  parseInt($('#insurance_insured_addressCellphone_code').val())
        if (codv < 10){
            var ref = 8
        }else{
            var ref = 7
        }
        var valor = $('#insurance_insured_addressCellphone_telephone').val()
        $('#insurance_insured_addressCellphone_telephone').parent('div').removeClass('error')
        $('#insurance_insured_addressPhone_telephone').removeAttr('required');
        if( ($scope.age() >= $scope.product.ageMin && $scope.age() < $scope.product.ageMax && valor.length == ref) && ($scope.prima != undefined && $scope.prima > 0) ){
            $('#next1').attr('style','display:block;');
            var stepNum = parseInt($(this).data('step'));
            var nextNum = parseInt($(this).data('next'));
            var existStep = document.getElementById('next' + nextNum);
            while(existStep && $('#next' + nextNum).is(':visible') == false){
                nextNum = nextNum + 1;
                existStep = document.getElementById('next' + nextNum);
            }
            next = $('#next' + nextNum);

            if(validateSteps(stepNum)){

                
                $scope.val = 0
                if($scope.beneficiaries.length < 1){
                    $scope.beneficiaries.push({
                    name: '',
                    lastname1: '',
                    lastname2: '',
                    code: '',
                    relationship: '',
                    capital: 100,
                    day: null,
                    month: null,
                    year: null
                    });
                }
                
                if (stepNum == 1 && $scope.val == 0){

                    if(validateSteps(stepNum)){
                        var final = stepNum +1;
                        $('#step_first_selected').slideToggle();
                        $('#plans').slideToggle(300);
                        $('#step1').slideToggle(300);
                        $('div.box-declaracion.selected_plan').slideToggle(300);


                        $('#next'+final).addClass('active');
                        $('#next2').attr('style','display:block');
                        $('#info-declaracion-'+final).slideToggle(300);          
                    }
                }
                else if( stepNum > 1)
                {   
                    $scope.visible = 2
                    var final = stepNum;
                    $('#next'+final).removeClass('active');
                    $('#info-declaracion-'+final).slideToggle(300);
                    
                    final = stepNum+1;
                    //$('#box-declaracion'+final).slideToggle(300);
                    $('#next'+final).addClass('active');
                    $('#next'+final).attr('style','display:block;');
                    $('#box-declaracion'+final).attr('style','display:block;');
                    $('#info-declaracion-'+final).slideToggle(300);


                    if(final == 7)
                        $('#mensajeespera').fadeIn(300);
                }
               
            }
        }else
        {
            event.preventDefault()
            if($scope.age() > $scope.product.ageMin && $scope.age() < $scope.product.ageMax){
                newAlert('Estimado cliente en este momento no podemos atender su solicitud')
            }else{
                    
                if($scope.age() <= $scope.product.ageMin || $scope.age() >= $scope.product.ageMax)
                newAlert('Estimado cliente la edad para contratar el seguro debe estar entre '+$scope.product.ageMin+' y '+($scope.product.ageMax-1)+' años')

            if(valor.length != ref){
                $('#insurance_insured_addressCellphone_telephone').attr('aria-invalid','true')
                $('#insurance_insured_addressCellphone_telephone').parent('div').addClass('error')
            }
            }
        }
    
    });
$scope.volver = function(step,next){
        $('.regular-input').removeAttr('required')
        $('.regular-check').removeAttr('required')
        //lugar
        var stepNum = parseInt(step);
        //paso anterior
        var nextNum = parseInt(next);
        //paso antior 2.0 xD
        var existStep = nextNum;
        //console.log($('#next' + nextNum).is(':visible') == false)    retorna false
        while(existStep && $('#next' + nextNum).is(':visible') == false){
            nextNum = nextNum + 1;
            existStep = document.getElementById('next' + nextNum);
  
        }
        if(validateSteps(step))
        {
            if(step == 2)
            {
                $('#step_first_selected').slideToggle();
                $('#plans').slideToggle(300);
                $('#step1').slideToggle(300);
                $('div.box-declaracion.selected_plan').slideToggle(300);

                $('#next2').removeClass('active');
                $('#next2').attr('style','display:none');
                $('#info-declaracion-2').slideToggle(300); 
            }
            if(step == 3)
            {

                $('#next2').addClass('active');
                $('#step2').attr('style','display:none;');
                $('#info-declaracion-2').slideToggle(300); 
                
                $('#next3').removeClass('active')
                $('#step3').attr('style','display:block;');
                $('#next3').attr('style','display:none');
                $('#box-declaracion3').attr('style','display:none;');
                $('#info-declaracion3').attr('style','display:none;');
                $('#info-declaracion-3').attr('style','display:none;');
                
            }
             if(step == 4)
            {
                $('#next3').addClass('active');
                $('#step3').attr('style','display:none;');
                $('#info-declaracion-3').slideToggle(300); 
                
                $('#next4').removeClass('active')
                $('#step4').attr('style','display:block;');
                $('#next4').attr('style','display:none');
                $('#box-declaracion4').attr('style','display:none;');
                $('#info-declaracion4').attr('style','display:none;');
                $('#info-declaracion-4').attr('style','display:none;');

            }

            if(step == 5)
            {
                $('#next4').addClass('active');
                $('#step4').attr('style','display:none;');
                $('#info-declaracion-4').slideToggle(300); 
                
                $('#next5').removeClass('active')
                $('#step5').attr('style','display:block;');
                $('#next5').attr('style','display:none');
                $('#box-declaracion5').attr('style','display:none;');
                $('#info-declaracion5').attr('style','display:none;');
                $('#info-declaracion-5').attr('style','display:none;');


            }
            if(step == 6)
            {
                $('#next5').addClass('active');
                $('#step5').attr('style','display:none;');
                $('#info-declaracion-5').slideToggle(300); 
                
                $('#next6').removeClass('active')
                $('#step6').attr('style','display:block;');
                $('#next6').attr('style','display:none');
                $('#box-declaracion6').attr('style','display:none;');
                $('#info-declaracion6').attr('style','display:none;');
                $('#info-declaracion-6').attr('style','display:none;');


            }

        }

      $('.regular-input').attr('required','required')
      $('.regular-check').attr('required','required')
      $('#insurance_insured_addressPhone_telephone').removeAttr('required');
      $('#insurance_insured_addressPhone_code').removeAttr('required');
    }
    $scope.edadToggle = function(val){
        $scope.edad = $scope.age();
        var dia = $('#insurance_insured_birthday_day').val()
        var mes = $('#insurance_insured_birthday_month').val()
        var anio = $('#insurance_insured_birthday_year').val()
        $scope.fecha = dia+'-'+mes+'-'+anio
        var datos = { "producto": ''+$scope.code+'', "renta": ''+ $scope.rent+'', "fecha": ''+$scope.fecha+'','curso':''+$scope.curso_aseg+'' };
        var jsonstring = JSON.stringify(datos);

        $('#mensajeespera').fadeIn(200);

        $http({
        url: '../../../customers/' + token + '/rest/product/' + product.id + '/plan',
        method: "POST",
        data: $.param({'case':jsonstring}),
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (data, status, headers, config) {
            $('#mensajeespera').hide();

            if (data == ''){
                newAlert('Estimado cliente, no es posible calcular el valor cuota con los datos ingresados');
                $scope.prima = undefined
            }
            else{
                var test = data[0]['EDU_PROY_VAL'];
                $scope.prima = parseFloat(test) * $scope.uf
                $scope.prima_uf = test
                //COMPLETA LA MATRIZ DE VALORES
                $scope.pri= data;
                $('#insurance_price').val(parseFloat($scope.prima_uf))
            }
        }).error(function (data, status, headers, config) {
            $('#mensajeespera').hide();
            $scope.status = data;
            newAlert('Estimado cliente, no es posible calcular el valor cuota con los datos ingresados');
        });
 
        $('#insurance_pri').val($scope.codplanco)
        $('#insurance_price').val(parseFloat($scope.prima_uf))
        $('#insurance_curso').val($scope.curso_aseg)
        $('#insurance_renta').val($scope.rent)
        $('#insurance_plan').val($scope.plan.id);
    }
    $scope.calrenta = function(val)
    {
       
        $scope.rent = val

        var datos = { "producto": ''+$scope.code+'', "renta": ''+ $scope.rent+'', "fecha": ''+$scope.fecha+'','curso':''+$scope.curso_aseg+'' };
        var jsonstring = JSON.stringify(datos);

        $('#mensajeespera').fadeIn(200);

        $http({
        url: '../../../customers/' + token + '/rest/product/' + product.id + '/plan',
        method: "POST",
        data: $.param({'case':jsonstring}),
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (data, status, headers, config) {

            $('#mensajeespera').hide();

            if (data == ''){
                newAlert('Estimado cliente, no es posible calcular el valor cuota con los datos ingresados');
                $scope.prima = undefined
            }
            else{
            console.log(data);
            var test = data[0]['EDU_PROY_VAL'];
            //console.log(test)

            $scope.prima = parseFloat(test) * $scope.uf
            $scope.prima_uf = test
            //COMPLETA LA MATRIZ DE VALORES
            $('#insurance_price').val(parseFloat($scope.prima_uf))
            $scope.pri= data;
        }
        }).error(function (data, status, headers, config) {
            $('#mensajeespera').hide();
            newAlert('Estimado cliente, no es posible calcular el valor cuota con los datos ingresados');
            $scope.status = data;
        });        
        for (var i = 0; i < $scope.renta.length; i++){
                if($scope.rent == $scope.renta[i].id)
                {   

                    $scope.codplanco = $scope.renta[i].codplanco
                   
                }
                
        }
        /*for (var i = 0; i < $scope.renta.length; i++) {
                if($scope.dato == $scope.renta[i].idplan)
                    if($scope.renta[i].orderby == 1){
                        $scope.rent = $scope.renta[i].id
                        $scope.codplanco = $scope.renta[i].codplanco
                    }  
        }*/

        $('#insurance_pri').val($scope.codplanco)
        $('#insurance_price').val(parseFloat($scope.prima_uf))
        $('#insurance_curso').val($scope.curso_aseg)
        $('#insurance_renta').val($scope.rent)
        $('#insurance_plan').val($scope.plan.id);
    }
    $scope.edad = $scope.age();

    $scope.planToggle = function(plan_id){
      
        if(plan_id == 157 || plan_id == 154 ){
            $scope.sele = 1;
            $scope.curso_aseg = 1
        }
        if(plan_id == 228){
            $scope.sele = 16;
            $scope.curso_aseg = 16
        }
        
        $scope.dato = plan_id;
        
        //$scope.dato = plan_id; //$scope.dato = valor de plan
    
        var dato = $scope.georly[$('#insurance_insured_addressState').val()]
        $('#insurance_insured_addressCity').val(dato)
        $('#ciudad').html(dato)

        if($scope.edad != $scope.age())
        {
            var dia = $('#insurance_insured_birthday_day').val()
            var mes = $('#insurance_insured_birthday_month').val()
            var anio = $('#insurance_insured_birthday_year').val()

        }else
        {
            for (var i = 0; i < $scope.renta.length; i++) {
                if(plan_id == $scope.renta[i].idplan)
                    if($scope.renta[i].orderby == 1){
                        $scope.rent = $scope.renta[i].id
                        $scope.codplanco = $scope.renta[i].codplanco
                    }  
            }
        }
        $scope.prod = $scope.product.id;

        
        var datos = { "producto": ''+$scope.code+'', "renta": ''+ $scope.rent+'', "fecha": ''+$scope.fecha+'','curso':''+$scope.curso_aseg+'' };
        var jsonstring = JSON.stringify(datos);

        $('#mensajeespera').fadeIn(200);

        $http({
        url: '../../../customers/' + token + '/rest/product/' + product.id + '/plan',
        method: "POST",
        data: $.param({'case':jsonstring}),
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (data, status, headers, config) {
            $('#mensajeespera').hide();

            if (data == ''){
                newAlert('Estimado cliente, no es posible calcular el valor cuota con los datos ingresados');
                $scope.prima = undefined
            }
            else{
            var test = data[0]['EDU_PROY_VAL'];
            $scope.prima = parseFloat(test) * $scope.uf
            
            $scope.prima_uf = test
             $('#insurance_price').val(parseFloat($scope.prima_uf))
            //COMPLETA LA MATRIZ DE VALORES
            $scope.pri= data;
        }
        }).error(function (data, status, headers, config) {
            $('#mensajeespera').hide();
            newAlert('Estimado cliente, no es posible calcular el valor cuota con los datos ingresados');
            $scope.status = data;
        });
        //FIN AJAX
        $scope.traerCobertura(plan_id)
        $scope.plan = $scope.findPlan(plan_id);
        $('#insurance_pri').val($scope.codplanco)
        $('#insurance_price').val(parseFloat($scope.prima_uf))
        $('#insurance_curso').val($scope.curso_aseg)
        $('#insurance_renta').val($scope.rent)
        $('#insurance_plan').val($scope.plan.id);

        for(var i = 0; i < $scope.cover.length; i++) {
             res = $scope.cover[i]
             if(res.length > 0){
                for (var i = 0; i < res.length; i++) {
                    //console.log(res[i].plan.id)
                }
             }
         }
         $scope.activeCampaign($scope.plan);
    };

    
    $scope.traerCobertura = function(plan_id){
        $scope.cober = []
        for (var i = 0; i < $scope.coverages.length; i++) {
            if($scope.coverages[i].plan.id == plan_id)
            {
                    $scope.cober.push({
                        'capital': $scope.coverages[i].capital,
                        'id': $scope.coverages[i].id,
                        'name': $scope.coverages[i].name
                    
                    })
            }
        }
    }

    $scope.obtener_curso = function()
    {
        console.log($scope.curso)
        console.log($scope.code)
        console.log($scope.curso_aseg)
        console.log($scope.fecha)
        console.log($scope.rent)
        $scope.curso_aseg = $scope.curso
        var datos = { "producto": ''+$scope.code+'', "renta": ''+ $scope.rent+'', "fecha": ''+$scope.fecha+'','curso':''+$scope.curso_aseg+'' };
        var jsonstring = JSON.stringify(datos);

        $('#mensajeespera').fadeIn(200);

        $http({
        url: '../../../customers/' + token + '/rest/product/' + product.id + '/plan',
        method: "POST",
        data: $.param({'case':jsonstring}),
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (data, status, headers, config) {
            $('#mensajeespera').hide();

             if (data[0]['EDU_PROY_VAL'] == undefined){
                newAlert('Estimado cliente, no es posible calcular el valor cuota con los datos ingresados');
                $scope.prima = undefined
            }
            else{
            var test = data[0]['EDU_PROY_VAL'];
            $scope.prima = parseFloat(test) * $scope.uf
            $scope.prima_uf = test
            //COMPLETA LA MATRIZ DE VALORES
            $('#insurance_price').val(parseFloat($scope.prima_uf))
            console.log($('#insurance_price').val())
            $scope.pri= data;
            }
        }).error(function (data, status, headers, config) {
            $('#mensajeespera').hide();
            newAlert('Estimado cliente, no es posible calcular el valor cuota con los datos ingresados');
            $scope.status = data;
        });

        $('#insurance_pri').val($scope.codplanco)
        $('#insurance_price').val(parseFloat($scope.prima_uf))
        $('#insurance_curso').val($scope.curso_aseg)
        $('#insurance_renta').val($scope.rent)
        $('#insurance_plan').val($scope.plan.id);

        

    }
    $scope.matriz = function()
    {
        var datos = "<table width='100%'>"
        datos += "<th width='100%' COLSPAN=4>Datos de prima</th>"
        for (var i = 0; i < $scope.pri.length; i++) {
            datos += '<tr>'
            datos += '<td>'+$scope.pri[i]['EDU_PROY_NUM']+'° Año: </td>'
            datos += '<td>&nbsp;</td>'
            datos += '<td>&nbsp;</td>'
            datos += '<td >UF('+$scope.pri[i]['EDU_PROY_VAL']+')</td>'
            datos += '</tr>'
        }
        datos += "</table>"
        newAlert2(datos)
           
    }

//  $('#mensajeespera').fadeIn(300);

}]);
