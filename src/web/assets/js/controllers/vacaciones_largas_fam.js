app.controller('details',  [ '$scope' , function($scope, $http, $filter) {
	BaseController.call(this, $scope);

    $scope.submit = 1;
    $scope.coverages = coverages;
    $scope.plans =  plans;
    $scope.insured = 1;
    $scope.items = relationships;
	$scope.beneficiaries = beneficiaries;
    $scope.errors = errors;
    $scope.errorBeneficiary = false;
    //$scope.codplanco = codplanco;
  
    $('#insurance_price').removeAttr('required');
    $('#insurance_price').removeAttr('value');

    $scope.rangoDias = 0;

    $('button.btn-continuar').click(function(){
        var codv =  parseInt($('#insurance_insured_addressCellphone_code').val())
        if (codv < 10){
            var ref = 8
        }else{
            var ref = 7
        }
        var valor = $('#insurance_insured_addressCellphone_telephone').val()
        $('#insurance_insured_addressCellphone_telephone').parent('div').removeClass('error')
        $('#insurance_insured_addressPhone_telephone').removeAttr('required');
        if( $scope.age() >= $scope.product.ageMin && $scope.age() < $scope.product.ageMax && valor.length == ref){
            var stepNum = parseInt($(this).data('step'));
            var nextNum = parseInt($(this).data('next'));
            var existStep = document.getElementById('next' + nextNum);
            while(existStep && $('#next' + nextNum).is(':visible') == false){
                nextNum = nextNum + 1;
                existStep = document.getElementById('next' + nextNum);
            }
            next = $('#next' + nextNum);
            if(validateSteps(stepNum)){
                if(stepNum == 1)
                {
                   if($scope.beneficiaries.length > 0 )
                    {
                        $scope.submit = 2
                        $('#step_first_selected').hide();
                        $('#plans').hide();
                        $('#step1').slideToggle(300);
                        $('div.box-declaracion.selected_plan').show();
                        $scope.setCargas($scope.plan.cargasMin,$scope.plan.cargasMax);
                        $('#next2').show();
                        $('#next2').addClass('active');
                        $('#info-declaracion-2').removeClass('hidden');
                        $('#info-declaracion-2').attr('style', 'display:block;');
                    }else
                    {
                        $scope.submit = 1
                        $('#step_first_selected').hide();
                        $('#plans').hide();
                        $('#step1').slideToggle(300);
                        $('div.box-declaracion.selected_plan').show();
                        $('#next2').hide();
                    }  

                }
                if (stepNum == 2){
                    if(validateSteps(stepNum)){
                        $('#info-declaracion-2').slideToggle(300)
                        $('#next2').removeClass('active');
                        $('#mensajeespera').fadeIn(300);
                    }
                }
            }else{
                if (stepNum == 1)
                    $('#next2').attr('style', 'display:none');
            }
        }else
        {
                $('#next2').attr('style', 'display:none');
            event.preventDefault()
            //$('.regular-select').attr('required','required')
            $('#insurance_insured_addressPhone_telephone').removeAttr('required');
            if($scope.age() <= $scope.product.ageMin || $scope.age() >= $scope.product.ageMax)
                newAlert('Estimado cliente la edad para contratar el seguro debe estar entre '+$scope.product.ageMin+' y '+($scope.product.ageMax-1)+' años')

            if(valor.length != ref){
                $('#insurance_insured_addressCellphone_telephone').attr('aria-invalid','true')
                $('#insurance_insured_addressCellphone_telephone').parent('div').addClass('error')
            }
        }
    })
    
    $scope.mayorEdad = function () {
        $scope.edades = [$scope.edad];
        $scope.errorBeneficiary = false;
        $scope.beneficiaries.forEach(function (benef, index, data) {
            if (benef.day && benef.month && benef.year) {
                var dob = new Date(benef.year, benef.month - 1, benef.day);
                var today = new Date();
                var birthDate = new Date(dob);
                var age = today.getFullYear() - birthDate.getFullYear();
                var m = today.getMonth() - birthDate.getMonth();
                if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                    age = age - 1;
                }
               
                benef.age = age;
                // $scope.validateEdad(benef, index);
                
            }
        });
        age = Math.max.apply(null, $scope.edades);
    };
    
    $scope.validateEdad = function (beneficiary, index) {
        var ageMaxUnique = 23;
        var ageMax = 68;
        
        var issetMayor = $scope.beneficiaries.filter(function(item) {
            return item.age > ageMaxUnique;
        });

        if(issetMayor.length > 1 && (beneficiary.age > ageMaxUnique || beneficiary.age > ageMax)) {
            $('#insurance_'+index+'_birthday')
            .find('select').find('option:eq(0)').prop('selected', true);
            $scope.errorBeneficiary = true;
            beneficiary.age = "";
            beneficiary.day = "";
            beneficiary.year = "";
            beneficiary.month = "";
        } else {
            if(beneficiary.age > ageMax ) {
                $('#insurance_'+index+'_birthday')
                .find('select').find('option:eq(0)').prop('selected', true);
                $scope.errorBeneficiary = true;
                beneficiary.age = "";
                beneficiary.day = "";
                beneficiary.year = "";
                beneficiary.month = "";
            }
        } 
    }


    $scope.volver = function(step,next){
        $('.regular-input').removeAttr('required')
        $('.regular-check').removeAttr('required')
        //lugar
        var stepNum = parseInt(step);
        //paso anterior
        var nextNum = parseInt(next);
        //paso antior 2.0 xD
        var existStep = nextNum;
        while(existStep && $('#next' + nextNum).is(':visible') == false){
            nextNum = nextNum + 1;
            existStep = document.getElementById('next' + nextNum);
        }
       
        if(step == 2)
        {
            //$('#next1').attr('style','display:block;');
            $('.selected_plan').attr('style', 'display:none');
            $('#step1').slideToggle(300)
            $('#plans').slideToggle(300)
            $('#next2').slideToggle(300)
            $('#info-declaracion-2').slideToggle(300)
        }
        if(step == 3)
        {

            if($scope.beneficiaries.length <= 0){
                $('#next1').attr('style','display:none;');
                $('#step1').slideToggle(300)
                $('#plans').slideToggle(300)
                $('#next3').slideToggle(300)
                $('#info-declaracion-3').slideToggle(300)
            }
            else{

                $('#next3').removeClass('active')
                $('#next2').attr('style', 'block;');
                $('#next2').addClass('active')
                //$('#next2').slideToggle(300)
                $('#info-declaracion-2').slideToggle(300)
                //$('#next3').slideToggle(300)
                $('#next3').attr('style', 'display:none;');
                $('#info-declaracion-3').slideToggle(300)
            }
        }

    

      $('.regular-input').attr('required','required')
      $('.regular-check').attr('required','required')
      $('#insurance_insured_addressPhone_telephone').removeAttr('required');
      $('#insurance_insured_addressPhone_code').removeAttr('required');
    }

    $scope.filterCoverage = function(coverage){
        return ($scope.plan.id == coverage.plan.id);
    };
  

    $scope.planToggle = function(plan_id) {
        
        var dato = $scope.georly[$('#insurance_insured_addressState').val()];
       
        $('#insurance_insured_addressCity').val(dato)
        $('#ciudad').html(dato);
        
       

        if ($scope.plan.id === 'undefined' || plan_id !== $scope.plan.id) {
            $scope.setBeneficiaries(plan_id);
            $scope.traerCobertura(plan_id);
        }
        
        var age = $scope.age();
        $scope.plan = $scope.findPlan(plan_id);

        $('#insurance_plan').val($scope.plan.id);

        $scope.updatePrice($scope.plan.price);

        var cobertura = $scope.getPlanCoverage($scope.plan.id);

        $scope.rangoDias = cobertura.days;
        if($("p.datepick").html() == ''){
            $("p.datepick").html(moment().add(cobertura.days-1, 'days').format("DD-MM-YYYY"));
        }

        $("#insurance_travelDate").trigger('cambiar',[cobertura.days]);

        if ($scope.targets.length > 0){
            
            $scope.targets.forEach(function(element, index, data){
                element.forEach(function(e, i, d){
                    if (e.plan.id == $scope.plan.id) {
                        if (age >= e.start && age <= e.finish) {
                            $scope.product.price = e.price;
                            $scope.target.start = e.start;
                            $scope.target.finish = e.finish;
                        }
                    }
                });
            });
        }
        $scope.activeCampaign($scope.plan);
    };

    $scope.setBeneficiaries = function(plan_id) {
        var cargas = 0;
        $scope.beneficiaries = [];
        for (var i = 0; i < $scope.plans.length; i++){
            if (plan_id == $scope.plans[i].id){
                if($scope.plans[i].cargasMin > $scope.beneficiaries.length){
                    cargas = parseInt($scope.plans[i].cargasMin);
                    for (var j = 0; j < cargas; j++) {
                        $scope.beneficiaries.push({
                            name: '',
                            lastname1: '',
                            lastname2: '',
                            code: '',
                            relationship: '',
                            capital: 0,
                            day: null,
                            month: null,
                            year: null
                        });
                    }
                        $scope.$emit("BENEFICIARY", "MORE");
                } else {
                    $scope.$emit("BENEFICIARY", "LESS");
                }
            }
        }
    }


    $scope.$on("BENEFICIARY", function(event, message){
       
        //$scope.visible = 0;
        if (message != 'other') {
            if($scope.beneficiaries.length > 0){
                $scope.submit = 2
            }else{
                $scope.submit = 1
            }
        }
    });
    
    $scope.setCargas = function(minCargas, maxCargas){
        $scope.plan.cargasMin = minCargas;
        $scope.plan.cargasMax =  maxCargas;
        if($scope.beneficiaries.length >= 1)
        {
            $scope.submit = 2
        }else{
            $scope.submit = 1
        }
       
    	while($scope.beneficiaries.length > maxCargas){
            $scope.lessBeneficiary();
    	}
    	while($scope.beneficiaries.length < minCargas){
    		$scope.moreBeneficiary();
    	}
           if($scope.beneficiaries.length == 1){
            $scope.payBeneficiary();
        }
        $scope.checked($scope.cober)
        //Obtener el nuevo valor del plan dependiendo la cantidad de beneficiarios
        var plan = {};
        plan = $scope.findPlan($scope.plan.id);
        var precioPlan = plan.price;
        for (var i = 0, len = plan.cargasPrice.length; i < len; i++) {
          if (plan.cargasPrice[i].value == $scope.beneficiaries.length) {
              precioPlan = plan.cargasPrice[i].price;
          }
        }
        $scope.updatePrice(precioPlan);
        
    }

    $scope.getPlanCoverage = function(planId){
            for(var indice = 0; indice < $scope.coverages.length; indice ++){
            if($scope.plan.id == $scope.coverages[indice].plan.id){
                return $scope.coverages[indice];   
            }
        }
        return false;
    };
    
    $scope.traerCobertura = function(plan_id){
        var cont = 0
        for (var i = 0; i < $scope.coverages.length; i++) {
            if($scope.coverages[i].plan.id == plan_id)
            {    
                if(cont == 0 && $scope.coverages[i].capital < 1000 )
                {
                    $scope.cober = $scope.coverages[i].id
                    $scope.checked($scope.cober)
                    cont = cont +1
                }
            }
          
        }
    }

    $scope.checked = function(val)
    {
        $scope.cober = val
        $scope.precio = [];
        $scope.codplanco = [];

        angular.forEach($scope.coverages, function(value, key) {
            if($scope.cober == value.id )
            {   

                /*var sp = value.price.split(' ')
                var fn = sp.indexOf(',')
                sp.splice(fn,1)
                $scope.precio = sp*/
                $scope.precio = value.price
            }    
        });
        angular.forEach($scope.coverages, function(value, key) {
            //this.push(key + ': ' + value);
            if($scope.cober == value.id )
            {   
               /*var sp = value.codplanco.split(' ')
                var fn = sp.indexOf(',')
                sp.splice(fn,1)*/
                $scope.codplanco = value.codplanco 
            }    
        });
        //$scope.prima = $scope.precio[$scope.beneficiaries.length]*$scope.uf
        $scope.prima = $scope.precio*$scope.uf
        //$scope.prima_uf = $scope.precio[$scope.beneficiaries.length]
        $scope.prima_uf = $scope.precio
        $scope.pri = $scope.prima
        
        $('#insurance_price').val($scope.prima_uf)
        $('#insurance_pri').val($scope.codplanco)

    }
    
    $scope.upPrice = function(price)
    {
        var p = parseInt(Math.ceil(price));
        if (p>0) {
            while ((p%10) != 0) {
                p = p + 1;
            }
        }
        return p;
    };

            $scope.selectRel = function (value, idHidden) {
        var elemento = $scope.getObjects(relationships, 'value', value);
        if (elemento.length > 0) {
            elemento = elemento[0];

            if ((elemento.maxapparition !== -1) &&
                    ($('select.referSelect option[value="' + elemento.value + '"]:selected').length > elemento.maxapparition)) {
                $('select.referSelect[data-person=' + idHidden + '] option:eq(0)').prop('selected', true);
                $scope.beneficiaries[idHidden].relationship = "";
            }
        }
    };


}])
.directive("loadFirst", function(){
  return {
    restrict: "A",
    link:function(scope, iElem, iAttrs){
      scope.setCargas(0,0);
      scope.planToggle(scope.plan.id);
        if(scope.beneficiaries.length = 0)
        $scope.submit = 1
    }
  }
});
