app.controller('details', ['$scope', function($scope, $http) {
    BaseController.call(this, $scope);
    $scope.submit = 1;
    $scope.coverages = coverages;
    $scope.items = relationships;
    $scope.plans =  plans;
    $scope.beneficiaries = beneficiaries;
    $scope.errors = errors;
    $scope.visible = 0;

    //console.log($scope.plans)

    var dato = $scope.georly[$('#insurance_insured_addressState').val()]
        $('#insurance_insured_addressCity').val(dato)
        $('#ciudad').html(dato)

      $('button.btn-continuar').click(function(){
         var codv =  parseInt($('#insurance_insured_addressCellphone_code').val())
        //console.log(codv)
        if (codv < 10){
            var ref = 8
        }else{
            var ref = 7
        }
        var valor = $('#insurance_insured_addressCellphone_telephone').val()
        $('#insurance_insured_addressCellphone_telephone').parent('div').removeClass('error')
        $('#insurance_insured_addressPhone_telephone').removeAttr('required');
        if( $scope.age() >= $scope.product.ageMin && $scope.age() < $scope.product.ageMax && valor.length == ref){
        $('#next1').attr('style','display:block;');
        //$('#next3').hide();

        var stepNum = parseInt($(this).data('step'));
        var nextNum = parseInt($(this).data('next'));
        var existStep = document.getElementById('next' + nextNum);

        while(existStep && $('#next' + nextNum).is(':visible') == false){
            nextNum = nextNum + 1;
            existStep = document.getElementById('next' + nextNum);
        }
        next = $('#next' + nextNum);
        
        if (validateSteps(stepNum)) {
            if (stepNum == 1){
                $('#step_first_selected').hide();
                $('#plans').hide();
                $('#step1').slideToggle(300);
                $('div.box-declaracion.selected_plan').show();
                //$('#info-declaracion-2').attr('style', 'block;');
            if($scope.beneficiaries.length <= 0){
                $('#next3').show();
                $('#next3').addClass('active');
                if($('#next3').hasClass('active'))
                {
                    $('#info-declaracion-3').removeClass('hidden');
                    $('#info-declaracion-3').css('display','block'); 
                    //$('#next'+stepNum).removeClass('active');
                    //$('#next'+stepNum).parent().next().slideToggle(300);  
                    
                }
                $('#mensajeespera').fadeIn(300);
            }else
            {
                $('#next2').show();
                $('#next2').addClass('active');
                $('#info-declaracion-2').removeClass('hidden');
                $('#info-declaracion-2').css('display','block'); 
                
            }

            }
            else if( stepNum == 2)
            {
                    $scope.erro = 0
                    $scope.cod = true
                    if($scope.beneficiaries.length > 0)
                    {
                        for (var i = 0; i < $scope.beneficiaries.length; i++) {
                            $scope.erro = $scope.erro +  parseInt($scope.beneficiaries[i].capital)
                        }
                        if ($scope.erro != 100){
                            newAlert("La suma del capital debe ser igual a 100%")
                            for (var i = 0; i < $scope.beneficiaries.length; i++){
                                $scope.beneficiaries[i].capital = ''
                                 break;
                            }
                            event.stopPropagation();
                            event.preventDefault();
                        }
                    }
                    if($scope.erro == 100){
                    var val = 0
                    for (var i = 0; i < $scope.beneficiaries.length; i++){
                        val = val+1
                        for (var j = val; j < $scope.beneficiaries.length; j++){
                            if($scope.beneficiaries[j].code == $scope.beneficiaries[i].code)
                            {
                                
                                newAlert("Los rut son iguales")
                                $scope.beneficiaries[j].code = ''
                                event.stopPropagation();
                                event.preventDefault();
                                $scope.erro = $scope.erro + val
                                //break
                            }
                        }
                    }
                    }
                if($scope.erro == 100)
                {
                    if(validateSteps(stepNum))
                    {
                        $('#info-declaracion-2').slideToggle(300);
                        $('#next2').removeClass('active');
                        $('#next3').show();
                        $('#next3').addClass('active');
                        $('#info-declaracion-3').slideToggle(300);
                        $('#mensajeespera').fadeIn(300);
                    }
                }
            }
        }else
        {
            if(stepNum == 1)
            {
                $('#next1').hide();
                $('#next2').hide();
                $('#next3').hide();
                $('#next4').hide();
            }
        }
        }else
        {
            $('#next2').attr('style', 'display:none');
            event.preventDefault()
            //$('.regular-select').attr('required','required')
            if($scope.age() <= $scope.product.ageMin || $scope.age() >= $scope.product.ageMax)
                newAlert('Estimado cliente la edad para contratar el seguro debe estar entre '+$scope.product.ageMin+' y '+($scope.product.ageMax-1)+' años')

            if(valor.length != ref){
                $('#insurance_insured_addressCellphone_telephone').attr('aria-invalid','true')
                $('#insurance_insured_addressCellphone_telephone').parent('div').addClass('error')
            }
        }
    });
    $scope.volver = function(step,next){
        $('.regular-input').removeAttr('required')
        $('.regular-check').removeAttr('required')
        //lugar
        var stepNum = parseInt(step);
        var nextNum = parseInt(next);

        var existStep = nextNum;
        while(existStep && $('#next' + nextNum).is(':visible') == false){
            nextNum = nextNum + 1;
            existStep = document.getElementById('next' + nextNum);
            console.log(existStep)
        }
        console.log(step)
        if(validateSteps(step))
        {

            //validar para el paso 2
            if(step == 2)
            {
                $('#next1').attr('style','display:none;');
                $('#step1').slideToggle(300)
                $('#plans').slideToggle(300)
                $('#next2').slideToggle(300)
                $('#info-declaracion-2').slideToggle(300)
            }
            if(step == 3)
            {

                if($scope.beneficiaries.length <= 0){
                    $('#next1').attr('style','display:none;');
                    $('#step1').slideToggle(300)
                    $('#plans').slideToggle(300)
                    $('#next3').slideToggle(300)
                    $('#info-declaracion-3').slideToggle(300)
                }
                else{

                    $('#next3').removeClass('active')
                    $('#next2').attr('style', 'block;');
                    $('#next2').addClass('active')
                    //$('#next2').slideToggle(300)
                    $('#info-declaracion-2').slideToggle(300)
                    //$('#next3').slideToggle(300)
                    $('#next3').attr('style', 'display:none;');
                    $('#info-declaracion-3').slideToggle(300)
                }
            }
            if(step == 4)
            {
                $('#next4').removeClass('active')
                $('#next3').attr('style', 'block;');
                $('#next3').addClass('active')
                //$('#next2').slideToggle(300)
                $('#info-declaracion-3').slideToggle(300)
                //$('#next3').slideToggle(300)
                $('#next4').attr('style', 'display:none;');
                $('#info-declaracion-4').slideToggle(300)
            }
        }

      $('.regular-input').attr('required','required')
      $('.regular-check').attr('required','required')
      $('#insurance_insured_addressPhone_telephone').removeAttr('required');
      $('#insurance_insured_addressPhone_code').removeAttr('required');
    }
    $scope.filterCoverage = function(coverage){
        return ($scope.plan.id == coverage.plan.id);
    };
    
    /*$scope.capital = function()
    {
        $scope.erro = 0
        $scope.cod = true
        if($scope.beneficiaries.length > 0)
        {
            for (var i = 0; i < $scope.beneficiaries.length; i++) {
                $scope.erro = $scope.erro +  parseInt($scope.beneficiaries[i].capital)
            }
            if ($scope.erro != 100){
               // newAlert("La suma del capital debe ser igual a 100%")
                for (var i = 0; i < $scope.beneficiaries.length; i++){
                    $scope.beneficiaries[i].capital = ''
                     break;
                }
                event.stopPropagation();
                event.preventDefault();
            }
        }
        if($scope.erro == 100){
        var val = 0
        for (var i = 0; i < $scope.beneficiaries.length; i++){
            val = val+1
            for (var j = val; j < $scope.beneficiaries.length; j++){
                if($scope.beneficiaries[j].code == $scope.beneficiaries[i].code)
                {                    
                   // newAlert("Los rut son iguales")
                    $scope.beneficiaries[j].code = ''
                    event.stopPropagation();
                    event.preventDefault();
                    break
                }
            }
        }
        }
   
    }*/
    $scope.planToggle = function(plan_id) { 
        $scope.beneficiaries = [];
        $scope.dato = plan_id;
        $scope.plan = $scope.findPlan(plan_id);
        $scope.prima_uf = parseFloat($scope.plan.price)
        $scope.prima = $scope.upPrice($scope.prima_uf * $scope.uf)
        $('#insurance_price').val($scope.prima_uf)
        $('#insurance_plan').val($scope.plan.id);
        $scope.updatePrice($scope.plan.price);
		 if( $scope.plan.cargasMin > $scope.beneficiaries.length){
			for (var j = 0; j < $scope.plan.cargasMin; j++) {
				$scope.beneficiaries.push({
					name: '',
					lastname1: '',
					lastname2: '',
					code: '',
					relationship: '',
					capital: 0,
					day: null,
					month: null,
					year: null
				});
			}
            $scope.$emit("BENEFICIARY", "MORE");
            $scope.cantidad = $scope.beneficiaries.length
         }
         $scope.activeCampaign($scope.plan);
        
    };
    
    $scope.$on("BENEFICIARY", function(event, message){
       
    
        if (message != 'other') {
            if($scope.beneficiaries.length > 0){
                $scope.submit = 2
            }else{
                $scope.submit = 1
            }
        }
    });
    
        $scope.selectRel = function (value, idHidden) {
        var elemento = $scope.getObjects(relationships, 'value', value);
        if (elemento.length > 0) {
            elemento = elemento[0];

            if ((elemento.maxapparition !== -1) &&
                    ($('select.referSelect option[value="' + elemento.value + '"]:selected').length > elemento.maxapparition)) {
                $('select.referSelect[data-person=' + idHidden + '] option:eq(0)').prop('selected', true);
                $scope.beneficiaries[idHidden].relationship = "";
            }
        }
    };
    
}]);
