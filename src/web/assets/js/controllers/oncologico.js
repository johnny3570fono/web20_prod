app.controller('details', function($scope, $http) {
    BaseController.call(this, $scope);

    $scope.plans = plans;
    $scope.coverages = coverages //[];
    $scope.errors = errors;
    $scope.submit = 3
    $scope.cover = cover;
    $scope.edad = $scope.age()
   /* $http.get('/customers/' + token + '/rest/product/' + product.id + '/plan/target/coverage.json')
        .success(function(response, status, headers, config){
            $scope.coverage = response;
            console.log($scope.coverage)
        }).error(function(error, status, headers, config){
            console.log(error);
        });*/
    $scope.edad = parseInt($scope.age());
       
        var age = $scope.age();
        $scope.plan.id = $('#plan').data('valor')
        if ($scope.cover.length > 0){
            $scope.cover.forEach(function(element, index, data){
                element.forEach(function(e, i, d){
                    if (e.plan.id == $scope.plan.id) {
                     
                        if (age >= e.start && age <= e.finish) {
                       
                            $scope.updatePrice(e.price);
                            $scope.target.start = e.start;
                            $scope.target.finish = e.finish;
                            $scope.target.price = e.price;
                            $scope.prima_uf = parseFloat(e.price)
                            $scope.prima = $scope.upPrice($scope.prima_uf * $scope.uf)
                            $('#insurance_price').val($scope.prima_uf)
                            
                        }
                    }
                });
            });
    }
     if($scope.edad > $scope.target.finish | $scope.edad < $scope.target.start ){
            //newAlert("La edad selecionada se encuentra fuera del rango permitido")
            //$('#insurance_insured_birthday_year').val('');
            $("#insurance_insured_birthday_year").attr("required", "required");
            $("#insurance_insured_birthday_year").attr("aria-required", "true");
            $("#insurance_insured_birthday_year").attr("aria-invalid", "true");
            $scope.prima = 0;
            //$scope.prima_uf = 'no es posible calcular la prima :D'
        }else
        {
            
        }

    $('button.btn-continuar').click(function(){
         var codv =  parseInt($('#insurance_insured_addressCellphone_code').val())
        
        if (codv < 10){
            var ref = 8
        }else{
            var ref = 7
        }
        var valor = $('#insurance_insured_addressCellphone_telephone').val()
        $('#insurance_insured_addressCellphone_telephone').parent('div').removeClass('error')
        $('#insurance_insured_addressPhone_telephone').removeAttr('required');
        if( $scope.age() >= $scope.product.ageMin && $scope.age() < $scope.product.ageMax && valor.length == ref){
        $('#next1').attr('style','display:block;');
        //$('#next3').hide();
        var stepNum = parseInt($(this).data('step'));
        var nextNum = parseInt($(this).data('next'));
        var existStep = document.getElementById('next' + nextNum);

        while(existStep && $('#next' + nextNum).is(':visible') == false){
            nextNum = nextNum + 1;
            existStep = document.getElementById('next' + nextNum);
        }
        next = $('#next' + nextNum);
        
        if (validateSteps(stepNum)) {
            if (stepNum == 1){
                if(validateSteps(stepNum))
                { 
                    $('#info-declaracion-2').removeClass('hidden')
                    $('#step_first_selected').hide();
                    $('#plans').hide();
                    $('#step1').slideToggle(300);
                    $('div.box-declaracion.selected_plan').show();
                    $('#next2').show();
                    $('#next2').addClass('active');
                    $('#info-declaracion-2').attr('style','display:block'); 

                }
            $('#next3').hide();
            }
            else if( stepNum == 2)
            {
                    if(validateSteps(stepNum))
                    {
                        $('#info-declaracion-2').slideToggle(300);
                        $('#next2').removeClass('active');
                        $('#next3').show();
                        $('#next3').addClass('active');
                        $('#info-declaracion-3').slideToggle(300);
                    }  
            }
            else if(stepNum == 3)
            {
                if(validateSteps(stepNum))
                {   
                    $('#info-declaracion-3').slideToggle(300);
                    $('#next3').removeClass('active')
                    $('#next4').show();
                    $('#next4').addClass('active');
                    $('#info-declaracion-4').slideToggle(300);
                    $('#mensajeespera').fadeIn(300);
                } 
            }
            else {
                
                $('#next4').removeClass('active');
                $('#info-declaracion-4').slideToggle(300);

            }
        }
        else{

           if(stepNum == 1)
            {
                $('#next3').hide();
                $('#next2').hide();
                $('#next5').hide();
                $('#next4').hide();
            }
        }
        }else
        {
            $('#next2').attr('style', 'display:none');
            $('#next3').attr('style', 'display:none');
            event.preventDefault()
            if($scope.age() <= $scope.product.ageMin || $scope.age() >= $scope.product.ageMax)
                newAlert('Estimado cliente la edad para contratar el seguro debe estar entre '+$scope.product.ageMin+' y '+($scope.product.ageMax-1)+' años')

            if(valor.length != ref){
                $('#insurance_insured_addressCellphone_telephone').attr('aria-invalid','true')
                $('#insurance_insured_addressCellphone_telephone').parent('div').addClass('error')
            }
        }
    });
    
    $scope.planToggle = function(plan_id) {

        var dato = $scope.georly[$('#insurance_insured_addressState').val()]
        $('#insurance_insured_addressCity').val(dato)
        $('#ciudad').html(dato)
        //$('#next2').hide();
         $scope.traerCobertura(plan_id)
        $scope.plan = $scope.findPlan(plan_id);

        $scope.edad = $scope.age()
        console.log($scope.edad);
        $scope.dato = plan_id
        var age = $scope.age()
        $scope.traerCobertura(plan_id)

        $scope.plan = $scope.findPlan(plan_id);
        $('#insurance_plan').val($scope.plan.id);
        if ($scope.cover.length > 0){
            $scope.cover.forEach(function(element, index, data){
                element.forEach(function(e, i, d){
                    if (e.plan.id == $scope.plan.id) {
                        if (age >= e.start && age <= e.finish) {                       
                            $scope.updatePrice(e.price);
                            $scope.target.start = e.start;
                            $scope.target.finish = e.finish;
                            $scope.target.price = e.price;
                            $scope.prima_uf = parseFloat(e.price)
                            $scope.prima = $scope.upPrice($scope.prima_uf * $scope.uf)
                            $('#insurance_price').val($scope.prima_uf)
                            
                        }
                    }
                });
            });
        }
         if($scope.edad > $scope.target.finish | $scope.edad < $scope.target.start ){
            
            //$('#insurance_insured_birthday_year').val('');
            $("#insurance_insured_birthday_year").attr("required", "required");
            $("#insurance_insured_birthday_year").attr("aria-required", "true");
            $("#insurance_insured_birthday_year").attr("aria-invalid", "true");
            $scope.prima = 0;
            //$scope.prima_uf = 'no es posible calcular la prima :D'
        }
        $scope.activeCampaign($scope.plan);
    };

    $scope.traerCobertura = function(plan_id){
        $scope.cober = []
        for (var i = 0; i < $scope.coverages.length; i++) {
            if($scope.coverages[i].plan.id == plan_id)
            {
                    $scope.cober.push({
                        'capital': $scope.coverages[i].capital,
                        'name': $scope.coverages[i].name
                    
                    })
            }
        }
    }
    $scope.volver = function(step,next){
        $('.regular-input').removeAttr('required')
        $('.regular-check').removeAttr('required')
        //lugar
        var stepNum = parseInt(step);
        //paso anterior
        var nextNum = parseInt(next);
        //paso antior 2.0 xD
        var existStep = nextNum;
        //console.log($('#next' + nextNum).is(':visible') == false)    retorna false
        while(existStep && $('#next' + nextNum).is(':visible') == false){
            nextNum = nextNum + 1;
            existStep = document.getElementById('next' + nextNum);
         

        }
        if(validateSteps(step))
        {
           if(step == 2)
            {
                $('#next1').attr('style','display:none;');
                $('#step1').slideToggle(300)
                $('#plans').slideToggle(300)
                $('#next2').slideToggle(300)
                $('#info-declaracion-2').slideToggle(300)
            }
            if(step == 3)
            {

                $('#next3').removeClass('active')
                $('#next2').attr('style', 'block;');
                $('#next2').addClass('active')
                //$('#next2').slideToggle(300)
                $('#info-declaracion-2').slideToggle(300)
                //$('#next3').slideToggle(300)
                $('#next3').attr('style', 'display:none;');
                $('#info-declaracion-3').slideToggle(300)
                
            }
            if(step == 4)
            {
                $('#next4').removeClass('active')
                $('#next3').attr('style', 'block;');
                $('#next3').addClass('active')
                //$('#next2').slideToggle(300)
                $('#info-declaracion-3').slideToggle(300)
                //$('#next3').slideToggle(300)
                $('#next4').attr('style', 'display:none;');
                $('#info-declaracion-4').slideToggle(300)
            }
        }

      $('.regular-input').attr('required','required')
      $('.regular-check').attr('required','required')
      $('#insurance_insured_addressPhone_telephone').removeAttr('required');
      $('#insurance_insured_addressPhone_code').removeAttr('required');
    }
  
});
