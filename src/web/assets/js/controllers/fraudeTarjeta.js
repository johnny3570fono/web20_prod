app.controller('details',['$scope',function($scope, $http) {
	BaseController.call(this, $scope);

    $scope.submit = 1;
    $scope.coverages = coverages;
    $scope.plans =  plans;
    $scope.insured = 1;
	$scope.errors = errors;

    $(document).on('change', '#appbundle_fraud_insured_addressState', function(event) {
        var valor = $scope.georly[$('#appbundle_fraud_insured_addressState').val()]
        $('#appbundle_fraud_insured_addressCity').val(valor)
        $('#ciudad').html(valor)
       
    });
    
	$scope.filterCoverage = function(coverage){
        return ($scope.plan.id == coverage.plan.id);
    };

    $scope.planToggle = function(plan_id) {
        $scope.dato = plan_id
        $scope.plan = $scope.findPlan(plan_id);
        $scope.prima_uf = parseFloat($scope.plan.price)
        $scope.prima = $scope.upPrice($scope.prima_uf * $scope.uf)
        $scope.price = $scope.prima;
        $('#insurance_price').val($scope.prima_uf)
        $scope.updatePrice($scope.insurance.price);
        $scope.activeCampaign($scope.plan);
    };
    
        $('button.btn-continuar').click(function(e){
            var stepNum = parseInt($(this).data('step'));
        var codv =  parseInt($('#appbundle_fraud_insured_addressCellphone_code').val())
        console.log(codv)
        if (codv < 10){
            var ref = 8
        }else{
            var ref = 7
        }
        var valor = $('#appbundle_fraud_insured_addressCellphone_telephone').val()
        $('#appbundle_fraud_insured_addressCellphone_telephone').parent('div').removeClass('error')
        $('#appbundle_fraud_insured_addressCellphone_telephone').removeAttr('required');
        console.log($scope.age())
        if( $scope.age() >= $scope.product.ageMin && $scope.age() < $scope.product.ageMax && valor.length == ref){
            if(validateSteps(stepNum == 1))
                    $('#mensajeespera').fadeIn(300);
        }else
        {
            if($scope.age() <= $scope.product.ageMin || $scope.age() >= $scope.product.ageMax)
                newAlert('Estimado cliente la edad para contratar el seguro debe estar entre '+$scope.product.ageMin+' y '+($scope.product.ageMax-1)+' años')

            if(valor.length != ref){
                $('#appbundle_fraud_insured_addressCellphone_telephone').attr('aria-invalid','true')
                $('#appbundle_fraud_insured_addressCellphone_telephone').parent('div').addClass('error')
            }
            event.preventDefault();
        }
        
    })

}]);
