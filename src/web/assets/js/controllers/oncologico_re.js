app.controller('details', function($scope, $http) {
    BaseController.call(this, $scope);

    $scope.plans = plans;
    $scope.coverages = coverages //[];
    $scope.errors = errors;
    $scope.submit = 2
    $scope.cover = cover;
    $scope.edad = $scope.age()
   /* $http.get('/customers/' + token + '/rest/product/' + product.id + '/plan/target/coverage.json')
        .success(function(response, status, headers, config){
            $scope.coverage = response;
            console.log($scope.coverage)
        }).error(function(error, status, headers, config){
            console.log(error);
        });*/
    $scope.edad = parseInt($scope.age());
        console.log($scope.cover)
        var age = $scope.age();
        $scope.plan.id = $('#plan').data('valor')
        if ($scope.cover.length > 0){
            $scope.cover.forEach(function(element, index, data){
                element.forEach(function(e, i, d){
                    if (e.plan.id == $scope.plan.id) {
                     
                        if (age >= e.start && age <= e.finish) {
                       
                            $scope.updatePrice(e.price);
                            $scope.target.start = e.start;
                            $scope.target.finish = e.finish;
                            $scope.target.price = e.price;
                            $scope.prima_uf = parseFloat(e.price)
                            $scope.prima = $scope.upPrice($scope.prima_uf * $scope.uf)
                            $('#insurance_price').val($scope.prima_uf)
                            
                        }
                    }
                });
            });
    }
     if($scope.edad > $scope.target.finish | $scope.edad < $scope.target.start ){
            newAlert("La edad selecionada se encuentra fuera del rango permitido")
            $('#insurance_insured_birthday_year').val('');
            $("#insurance_insured_birthday_year").attr("required", "required");
            $("#insurance_insured_birthday_year").attr("aria-required", "true");
            $("#insurance_insured_birthday_year").attr("aria-invalid", "true");
            $scope.prima = 0;
            //$scope.prima_uf = 'no es posible calcular la prima :D'
        }else
        {
            console.log('pase')
        }

    $('button.btn-continuar').click(function(){
        //debugger
        $('#next3').hide();

        var stepNum = parseInt($(this).data('step'));
        var nextNum = parseInt($(this).data('next'));
        var existStep = document.getElementById('next' + nextNum);

        while(existStep && $('#next' + nextNum).is(':visible') == false){
            nextNum = nextNum + 1;
            existStep = document.getElementById('next' + nextNum);
        }
        next = $('#next' + nextNum);
        
        if (validateSteps(stepNum)) {
            if (stepNum == 1) {
                //$('#next1').attr('style','display:block;');
                $('#step_first_selected').hide();
                $('#plans').hide();
                $('#step1').slideToggle(300);
                $('div.box-declaracion.selected_plan').show();
                $('#next1').attr('style','display:block;');
            if($scope.beneficiaries.length <= 0){
                $('#next2').show();
                $('#next2').addClass('active');
                if($('#next2').hasClass('active'))
                {
                    $('#info-declaracion').removeClass('hidden');
                    $('#info-declaracion').css('display','block'); 
                    //$('#next'+stepNum).removeClass('active');
                    $('#next'+stepNum).parent().next().slideToggle(300); 

                }
                }
            }else
            {
                $('#next2').show();
                $('#next2').removeClass('active');
                $('#info-declaracion').slideToggle(300);
                $('#mensajeespera').fadeIn(300);
            }
        
            
        }
    });
    
    $scope.filterCoverage = function(coverage) {
        var a = $scope.age();
        var id = $scope.plan.id;
        return (a >= coverage.start && a <= coverage.finish && id == coverage.plan.id);

    }
     /*$scope.filterCoverage = function(coverage){
        return ($scope.plan.id == coverage.plan.id);
    };*/
    
    $scope.planToggle = function(plan_id) {
        $('#next2').hide();
         $scope.traerCobertura(plan_id)
        $scope.plan = $scope.findPlan(plan_id);

        $scope.edad = $scope.age()
        $scope.dato = plan_id
        var age = $scope.age()
        $scope.traerCobertura(plan_id)

        $scope.plan = $scope.findPlan(plan_id);
        $('#insurance_plan').val($scope.plan.id);
        if ($scope.cover.length > 0){
            $scope.cover.forEach(function(element, index, data){
                element.forEach(function(e, i, d){
                    if (e.plan.id == $scope.plan.id) {
                        if (age >= e.start && age <= e.finish) {                       
                            $scope.updatePrice(e.price);
                            $scope.target.start = e.start;
                            $scope.target.finish = e.finish;
                            $scope.target.price = e.price;
                            $scope.prima_uf = parseFloat(e.price)
                            $scope.prima = $scope.upPrice($scope.prima_uf * $scope.uf)
                            $('#insurance_price').val($scope.prima_uf)
                            
                        }
                    }
                });
            });
        }
         if($scope.edad > $scope.target.finish | $scope.edad < $scope.target.start ){
            //newAlert("La edad selecionada se encuentra fuera del rango permitido")
            $('#insurance_insured_birthday_year').val('');
            $("#insurance_insured_birthday_year").attr("required", "required");
            $("#insurance_insured_birthday_year").attr("aria-required", "true");
            $("#insurance_insured_birthday_year").attr("aria-invalid", "true");
            $scope.prima = 0;
            //$scope.prima_uf = 'no es posible calcular la prima :D'
        }
        $scope.activeCampaign($scope.plan);
    };

    $scope.traerCobertura = function(plan_id){
        $scope.cober = []
        for (var i = 0; i < $scope.coverages.length; i++) {
            if($scope.coverages[i].plan.id == plan_id)
            {
                    $scope.cober.push({
                        'capital': $scope.coverages[i].capital,
                        'name': $scope.coverages[i].name
                    
                    })
            }
        }
    }
    $scope.volver = function(step,next){
        $('.regular-input').removeAttr('required')
        $('.regular-check').removeAttr('required')
        //lugar
        var stepNum = parseInt(step);
        //paso anterior
        var nextNum = parseInt(next);
        //paso antior 2.0 xD
        var existStep = nextNum;
        //console.log($('#next' + nextNum).is(':visible') == false)    retorna false
        while(existStep && $('#next' + nextNum).is(':visible') == false){
            nextNum = nextNum + 1;
            existStep = document.getElementById('next' + nextNum);
            console.log(existStep)
        }
        if(validateSteps(step))
        {
            //$('#box-declaracion').attr('display','block');
            //$('#next2').attr('style','display:none;');
            //$('#info-declaracion-2').slideToggle(300)
            //$('#next1').attr('style','display:none;');
            //$('#next2').addClass('active')
            $('#next1').attr('style','display:none;');
            $('#step1').slideToggle(300)
            $('#plans').slideToggle(300)
            $('#next2').slideToggle(300)
            $('#info-declaracion').slideToggle(300)
        }

      $('.regular-input').attr('required','required')
      $('.regular-check').attr('required','required')
    }

});
