app.controller('details', ['$scope',  function($scope, $http) {
	BaseController.call(this, $scope);
	$scope.visible=0;
    $scope.submit = 2;
    $scope.coverages = coverages;
    $scope.plans =  plans;

    console.log($scope.plans);
	$scope.errors = errors;
	$scope.consultar = 1;
    $scope.idplan = idplan;

    console.log($scope.idplan)
	$scope.filterCoverage = function(coverage){
        return ($scope.plan.id == coverage.plan.id);
    };

    var dato = $scope.georly[$('#insurance_insured_addressState').val()]
        $('#insurance_insured_addressCity').val(dato)
        $('#ciudad').html(dato)
	
	$scope.setConsultaHogar = function setConsultaHogar(value)
	{
    if (value == 1) {
        $('input[data-value]').each(function(){
            $(this).val();
        });
        $('select[data-value]').each(function(){
            $(this).val();
        });
       
    } else {
        $('select[data-value]').each(function(){
            $(this).parent(".error").removeClass("error");

        });
    }
	}
    
    $('button.btn-continuar').click(function(){
        var codv =  parseInt($('#insurance_insured_addressCellphone_code').val())
        console.log(codv)
        if (codv < 10){
            var ref = 8
        }else{
            var ref = 7
        }
        var valor = $('#insurance_insured_addressCellphone_telephone').val()
        $('#insurance_insured_addressCellphone_telephone').parent('div').removeClass('error')
        $('#insurance_insured_addressPhone_telephone').removeAttr('required');
        if( $scope.age() >= $scope.product.ageMin && $scope.age() < $scope.product.ageMax && valor.length == ref){
        var stepNum = parseInt($(this).data('step'));
        var nextNum = parseInt($(this).data('next'));
        var existStep = document.getElementById('next' + nextNum);
        while(existStep && $('#next' + nextNum).is(':visible') == false){
            nextNum = nextNum + 1;
            existStep = document.getElementById('next' + nextNum);
        }
        next = $('#next' + nextNum);
        if(validateSteps(stepNum)){
            if(stepNum == 1)
            {
                    $('#step_first_selected').hide();
                    $('#plans').hide();
                    $('#step1').slideToggle(300);
                    $('div.box-declaracion.selected_plan').show();
                    $('#next2').show();
                    $('#next2').addClass('active');
                    $('#info-declaracion-2').removeClass('hidden');
                    $('#info-declaracion-2').attr('style', 'display:block;');
                    $('#box-declaracion').attr('style', 'display:block;'); 
                    $('#next1').attr('style','display:block;');
            }
            if (stepNum == 2){
                if(validateSteps(stepNum)){
                    $('#info-declaracion-2').slideToggle(300)
                    $('#next2').removeClass('active');
                    $('#mensajeespera').fadeIn(300);
                    $('#mensajeespera').fadeIn(300);
                }
            }
        }    
        }else{
            event.preventDefault()
            $('.regular-select').attr('required','required')
           if($scope.age() <= $scope.product.ageMin || $scope.age() >= $scope.product.ageMax)
                newAlert('Estimado cliente la edad para contratar el seguro debe estar entre '+$scope.product.ageMin+' y '+($scope.product.ageMax-1)+' años')

            if(valor.length != ref){
                $('#insurance_insured_addressCellphone_telephone').attr('aria-invalid','true')
                $('#insurance_insured_addressCellphone_telephone').parent('div').addClass('error')
            }
        
        }
    })
    $scope.style = function(val)
    {
      
       $scope.consultar =  1;
       if (valor == 0) {
            $scope.consultar =  val;  
           
            
        }else{
            $scope.consultar = 1;
        }
        
    }
    $scope.volver = function(step,next){
        $('.regular-input').removeAttr('required')
        $('.regular-select').removeAttr('required')
        //lugar
        var stepNum = parseInt(step);
        //paso anterior
        var nextNum = parseInt(next);
        //paso antior 2.0 xD
        var existStep = nextNum;
        //console.log($('#next' + nextNum).is(':visible') == false)    retorna false
        while(existStep && $('#next' + nextNum).is(':visible') == false){
            nextNum = nextNum + 1;
            existStep = document.getElementById('next' + nextNum);
            console.log(existStep)
        }
        if(validateSteps(step))
        {
          $('#next2').attr('style','display:none;');
          $('#info-declaracion-2').slideToggle(300)
          $('#next1').attr('style','display:none;');
          $('#plans').slideToggle(300)
          $('#step1').slideToggle(300)
        }

      $('.regular-input').attr('required','required')
      $('.regular-select').attr('required','required')
      $('#insurance_insured_addressPhone_telephone').removeAttr('required');
      $('#insurance_insured_addressPhone_code').removeAttr('required');
    }
    /* VOLVER QUE SE ENCUENTRA EN APP.JS
    $scope.volver = function(step,next){
        $('.regular-input').removeAttr('required')
        var stepNum = parseInt(step);
        var nextNum = parseInt(next);
        var existStep = nextNum;
        while(existStep && $('#next' + nextNum).is(':visible') == false){
            nextNum = nextNum + 1;
            existStep = document.getElementById('next' + nextNum);
        }
        next = $('#next' + nextNum);
        if (validateSteps(stepNum)) {
            if (nextNum == 1) {
                $('#step_first_selected').show();
                $('#plans').show();
                $('#step1').show();
                $('#next'+stepNum).parent().next().slideToggle(300);
                $('div.box-declaracion.selected_plan').hide();

            } else {
                 $('div.box-declaracion.selected_plan').show();
                $('#step2').show();
                $('#next'+stepNum).parent().next().slideToggle(300);
                $('#info-declaracion').attr('style','display:block;');
            }
            
        }
        $('.regular-input').attr('required','required')
        
    }*/

}])
.directive("loadFirst", function(){
  return {
    restrict: "A",
    link:function(scope, iElem, iAttrs){
      scope.planTogle(10);
    }
  }
});
