app.controller('details', function($scope, $http) {
	BaseController.call(this, $scope);
   
    
    $scope.coverages = coverages;
    $scope.plans =  plans
    $scope.insured = 1;
    $scope.items = relationships;
	$scope.errors = errors;



     $('button.btn-continuar').click(function(){
         var codv =  parseInt($('#insurance_insured_addressCellphone_code').val())
        if (codv < 10){
            var ref = 8
        }else{
            var ref = 7
        }
        var valor = $('#insurance_insured_addressCellphone_telephone').val()
        $('#insurance_insured_addressCellphone_telephone').parent('div').removeClass('error')
        $('#insurance_insured_addressPhone_telephone').removeAttr('required');
        if( $scope.age() >= $scope.product.ageMin && $scope.age() < $scope.product.ageMax && valor.length == ref){
        var stepNum = parseInt($(this).data('step'));
        var nextNum = parseInt($(this).data('next'));
        var existStep = document.getElementById('next' + nextNum);
        while(existStep && $('#next' + nextNum).is(':visible') == false){
            nextNum = nextNum + 1;
            existStep = document.getElementById('next' + nextNum);
        }
        next = $('#next' + nextNum);
        if(validateSteps(stepNum)){
            if(stepNum == 1)
            {
               
                if($scope.beneficiaries.length > 0 )
                {
                    $scope.submit = 2
                    $('#step_first_selected').hide();
                    $('#plans').hide();
                    $('#step1').slideToggle(300);
                    $('div.box-declaracion.selected_plan').show();
                    $scope.setCargas($scope.plan.cargasMin,$scope.plan.cargasMax);
                    $('#next2').show();
                    $('#next2').addClass('active');
                    $('#info-declaracion-2').removeClass('hidden');
                    $('#info-declaracion-2').attr('style', 'display:block;');
                }else
                {
                   
                    $scope.submit = 1
                    $('#step_first_selected').hide();
                    $('#plans').hide();
                    $('#step1').slideToggle(300);
                    $('div.box-declaracion.selected_plan').show();
                    $('#next2').hide();
                    $('#mensajeespera').fadeIn(300);
                }  
            }
             if(stepNum == 2)
            {
                 $('#next2').removeClass('active');
                 $('#info-declaracion-2').slideToggle(300);
                 $('#mensajeespera').fadeIn(300);   
            }   
        }else{
            if (stepNum == 1)
                $('#next2').attr('style', 'display:none');
           
        }
    }else
    {
        $('#next2').attr('style', 'display:none');
        event.preventDefault()
        //$('.regular-select').attr('required','required')
        if($scope.age() <= $scope.product.ageMin || $scope.age() >= $scope.product.ageMax)
                newAlert('Estimado cliente la edad para contratar el seguro debe estar entre '+$scope.product.ageMin+' y '+($scope.product.ageMax-1)+' años')

            if(valor.length != ref){
                $('#insurance_insured_addressCellphone_telephone').attr('aria-invalid','true')
                $('#insurance_insured_addressCellphone_telephone').parent('div').addClass('error')
            }
    }
    })


    $scope.mayorEdad = function () {
        $scope.edades = [$scope.edad];
        $scope.errorBeneficiary = false;
        $scope.beneficiaries.forEach(function (benef, index, data) {
            if (benef.day && benef.month && benef.year) {
                var dob = new Date(benef.year, benef.month - 1, benef.day);
                var today = new Date();
                var birthDate = new Date(dob);
                var age = today.getFullYear() - birthDate.getFullYear();
                var m = today.getMonth() - birthDate.getMonth();
                if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                    age = age - 1;
                }
                benef.age = age;
                //$scope.validateEdad(benef, index);
            }
        });
        age = Math.max.apply(null, $scope.edades);
    };
    
    $scope.validateEdad = function (beneficiary, index) {
        var ageMaxUnique = 23;
        var ageMax = 68;
        
        var issetMayor = $scope.beneficiaries.filter(function(item) {
            return item.age > ageMaxUnique;
        });

        if(issetMayor.length > 1 && (beneficiary.age > ageMaxUnique || beneficiary.age > ageMax)) {
            $('#insurance_'+index+'_birthday')
            .find('select').find('option:eq(0)').prop('selected', true);
            $scope.errorBeneficiary = true;
            beneficiary.age = "";
            beneficiary.day = "";
            beneficiary.year = "";
            beneficiary.month = "";
        } else {
            if(beneficiary.age > ageMax ) {
                $('#insurance_'+index+'_birthday')
                .find('select').find('option:eq(0)').prop('selected', true);
                $scope.errorBeneficiary = true;
                beneficiary.age = "";
                beneficiary.day = "";
                beneficiary.year = "";
                beneficiary.month = "";
            }
        } 
    }

    $scope.volver = function(step,next){
        $('.regular-input').removeAttr('required')
        $('.regular-check').removeAttr('required')
        //lugar
        var stepNum = parseInt(step);
        //paso anterior
        var nextNum = parseInt(next);
        //paso antior 2.0 xD
        var existStep = nextNum;
        //console.log($('#next' + nextNum).is(':visible') == false)    retorna false
        while(existStep && $('#next' + nextNum).is(':visible') == false){
            nextNum = nextNum + 1;
            existStep = document.getElementById('next' + nextNum);
        }
        if(validateSteps(step))
        {
            if(step == 2)
            {
               //$('#next1').attr('style','display:block;');
                $('.selected_plan').attr('style', 'display:none');
                $('#step1').slideToggle(300)
                $('#plans').slideToggle(300)
                $('#next2').slideToggle(300)
                $('#info-declaracion-2').slideToggle(300)
            }
            if(step == 3)
            {
                if($scope.beneficiaries.length <= 0){
                    $('#next1').attr('style','display:none;');
                    $('#step1').slideToggle(300)
                    $('#plans').slideToggle(300)
                    $('#next3').slideToggle(300)
                    $('#info-declaracion-3').slideToggle(300)
                }
                else{
                    $('#next3').removeClass('active')
                    $('#next2').attr('style', 'block;');
                    $('#next2').addClass('active')
                    //$('#next2').slideToggle(300)
                    $('#info-declaracion-2').slideToggle(300)
                    //$('#next3').slideToggle(300)
                    $('#next3').attr('style', 'display:none;');
                    $('#info-declaracion-3').slideToggle(300)
                }
            }


        }

      $('.regular-input').attr('required','required')
      $('.regular-check').attr('required','required')
      $('#insurance_insured_addressPhone_telephone').removeAttr('required');
      $('#insurance_insured_addressPhone_code').removeAttr('required');
    }

	$scope.$on("BENEFICIARY", function(event, message){
       
    
        if (message != 'other') {
            if($scope.beneficiaries.length > 0){
                $scope.submit = 2
            }else{
                $scope.submit = 2
            }
        }
    });

	$scope.filterCoverage = function(coverage){
        return (coverage.plan.id == $scope.plan.id);
    };

    
    $scope.setCargas = function(minCargas, maxCargas){
       // $scope.beneficiaries = []
       if($scope.beneficiaries.length > 0)
        {
            $scope.submit = 2
        }else{
            $scope.submit = 1
        }

    	$scope.plan.cargasMax = maxCargas;
    	$scope.plan.cargasMin = minCargas;

    
    
    	while($scope.beneficiaries.length > $scope.plan.cargasMax){
            
    		$scope.lessBeneficiary();
    	}

    	while($scope.beneficiaries.length < $scope.plan.cargasMin){
            $scope.moreBeneficiary();

        }
        
        if($scope.beneficiaries.length == 1){
            $scope.payBeneficiary();
        }
    }

    $scope.planToggle = function(plan_id) {

        var $radios = $('input:radio[name=cargas]');
        $radios.filter($('#1').prop('checked',true));

        var dato = $scope.georly[$('#insurance_insured_addressState').val()]
        $('#insurance_insured_addressCity').val(dato)
        $('#ciudad').html(dato)
        $scope.beneficiaries = []
        $scope.dato = plan_id
        $scope.plan = $scope.findPlan(plan_id);
        $scope.prima_uf = parseFloat($scope.plan.price)
        $scope.prima = $scope.upPrice($scope.prima_uf * $scope.uf);
        $('#insurance_price').val($scope.prima_uf)
        $('#insurance_plan').val($scope.plan.id);
        $scope.setCargas($scope.plan.cargasMin, $scope.plan.cargasMax);
        $scope.updatePrice($scope.plan.price);
        
        $scope.activeCampaign($scope.plan);

    };
    
})
.directive("loadFirst", function(){
  return {
    restrict: "A",
    link:function(scope, iElem, iAttrs){
      scope.setCargas(0,0);
    
      if(scope.plan.cargasMin = 0)
        $scope.submit = 1
    }
  }
});
