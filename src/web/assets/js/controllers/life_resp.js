app.controller('details',['$scope','$http', function($scope, $http) {
    BaseController.call(this, $scope);

    $scope.submit = 6;
    $scope.coverages = coverages;
    $scope.plans =  plans;
    $scope.items = relationships;
    $scope.beneficiaries = beneficiaries;
    $scope.errors = errors;
    $scope.cover = cover;
    
    /*$http.get('/customers/' + token + '/rest/product/' + product.id + '/plan/target/coverage.json')
        .success(function(response, status, headers, config){
            $scope.coverages = response;
        
        }).error(function(error, status, headers, config){
            console.log(error);
        });*/
       
    /*$http.get('/customers/' + token + '/rest/product/' + product.id + '/plan')
        .success(function(response, status, headers, config){
            $scope.targets = response;
        }).error(function(error, status, headers, config){
            console.log(error);
        });*/

        $scope.edad = parseInt($scope.age());
        var age = $scope.age();
        $scope.plan.id = $('#plan').data('valor')
        if ($scope.cover.length > 0){
            $scope.cover.forEach(function(element, index, data){
                element.forEach(function(e, i, d){
                    if (e.plan.id == $scope.plan.id) {
                     
                        if (age >= e.start && age <= e.finish) {
                       
                            $scope.updatePrice(e.price);
                            $scope.target.start = e.start;
                            $scope.target.finish = e.finish;
                            $scope.target.price = e.price;
                            $scope.prima_uf = parseFloat(e.price)
                            $scope.prima = $scope.upPrice($scope.prima_uf * $scope.uf)
                            $('#insurance_price').val($scope.prima_uf)
                        }
                    }
                });
            });
        }
         if($scope.edad > $scope.target.finish | $scope.edad < $scope.target.start ){
            newAlert("La edad selecionada se encuentra fuera del rango permitido")
            $('#insurance_insured_birthday_year').val('');
            $("#insurance_insured_birthday_year").attr("required", "required");
            $("#insurance_insured_birthday_year").attr("aria-required", "true");
            $("#insurance_insured_birthday_year").attr("aria-invalid", "true");
            $scope.prima = 0;
            //$scope.prima_uf = 'no es posible calcular la prima :D'
        }else
        {
            console.log('pase')
        }

   /* $scope.$on("BENEFICIARY", function(event, message){
        $scope.submit = 1;
        if (message == 'MORE') {
            $scope.submit = ;
        }*/

    $('button.btn-continuar').click(function(){
        $('#next1').attr('style','display:block;');
        //$('#next3').hide();
        var stepNum = parseInt($(this).data('step'));
        var nextNum = parseInt($(this).data('next'));
        var existStep = document.getElementById('next' + nextNum);

        while(existStep && $('#next' + nextNum).is(':visible') == false){
            nextNum = nextNum + 1;
            existStep = document.getElementById('next' + nextNum);
        }
        next = $('#next' + nextNum);
        if (validateSteps(stepNum)) {
            if (stepNum == 1){
                $('#step_first_selected').hide();
                $('#plans').hide();
                $('#step1').slideToggle(300);
                $('div.box-declaracion.selected_plan').show();

            if($scope.beneficiaries.length <= 0){
                $('#next3').show();
                $('#next3').addClass('active');
                if($('#next3').hasClass('active'))
                {
                    $('#info-declaracion-3').removeClass('hidden');
                    $('#info-declaracion').css('display','block'); 
                    //$('#next'+stepNum).removeClass('active');
                    $('#info-declaracion-3').slideToggle(300);
                    //$('#next'+stepNum).parent().next().slideToggle(300);  
                }
            }else
            {
                $('#next2').show();
                $('#next2').addClass('active');
                if($('#next2').hasClass('active'))
                {
                    $('#info-declaracion-2').removeClass('hidden');
                    $('#info-declaracion-2').css('display','block'); 

                }
                $('#next3').hide();
            }
            $('#next6').hide();
            $('#next5').hide();
            $('#next4').hide();
            
            }
            else if( stepNum == 2)
            {
                    $scope.erro = 0
                    $scope.cod = true
                    if($scope.beneficiaries.length > 0)
                    {
                        for (var i = 0; i < $scope.beneficiaries.length; i++) {
                            $scope.erro = $scope.erro +  parseInt($scope.beneficiaries[i].capital)
                        }
                        if ($scope.erro != 100){
                            newAlert("La suma del capital debe ser igual a 100%")
                            for (var i = 0; i < $scope.beneficiaries.length; i++){
                                $scope.beneficiaries[i].capital = ''
                                 break;
                            }
                            event.stopPropagation();
                            event.preventDefault();
                        }
                    }
                    if($scope.erro == 100){
                    var val = 0
                    for (var i = 0; i < $scope.beneficiaries.length; i++){
                        val = val+1
                        for (var j = val; j < $scope.beneficiaries.length; j++){
                            if($scope.beneficiaries[j].code == $scope.beneficiaries[i].code)
                            {
                                
                                newAlert("Los rut son iguales")
                                $scope.beneficiaries[j].code = ''
                                event.stopPropagation();
                                event.preventDefault();
                                $scope.erro = $scope.erro + val
                                //break
                            }
                        }
                    }
                    }
                if($scope.erro == 100)
                {
                    if(validateSteps(stepNum))
                    {
                        $('#info-declaracion-2').slideToggle(300);
                        $('#next2').removeClass('active');
                        $('#next3').show();
                        $('#next3').addClass('active');
                        $('#info-declaracion-3').slideToggle(300);
                    }
                }
            }
            else if(stepNum == 3)
            {
                if(validateSteps(stepNum))
                {   
                    $('#info-declaracion-3').slideToggle(300);
                    $('#next3').removeClass('active')
                    $('#next4').show();
                    //$('#next4').attr('style','display:block;');
                    $('#next4').addClass('active');
                    $('#info-declaracion-4').slideToggle(300);
                } 
            }
            else if(stepNum == 4)
            {
                if(validateSteps(stepNum))
                {   
                    $('#info-declaracion-4').slideToggle(300);
                    $('#next4').removeClass('active')
                    $('#next5').show();
                    //$('#next4').attr('style','display:block;');
                    $('#next5').addClass('active');
                    $('#info-declaracion-5').slideToggle(300);
                } 
            }
            else if(stepNum == 5)
            {
                if(validateSteps(stepNum))
                {   
                    $('#info-declaracion-5').slideToggle(300);
                    $('#next5').removeClass('active')
                    $('#next6').show();
                    //$('#next4').attr('style','display:block;');
                    $('#next6').addClass('active');
                    $('#info-declaracion-6').slideToggle(300);
                } 
            }
            else {
                
                $('#next6').removeClass('active');
                $('#info-declaracion-6').slideToggle(300);
            }
        }
        else
        {
            if(stepNum == 1)
            {
                if(validateSteps(stepNum))

                $('#next3').hide();
                $('#next6').hide();
                $('#next5').hide();
                $('#next4').hide();
            }
        }
    });
    $scope.volver = function(step,next){
        $('.regular-input').removeAttr('required')
        $('.regular-check').removeAttr('required')
        //lugar
        var stepNum = parseInt(step);
        //paso anterior
        var nextNum = parseInt(next);
        //paso antior 2.0 xD
        var existStep = nextNum;
        //console.log($('#next' + nextNum).is(':visible') == false)    retorna false
        while(existStep && $('#next' + nextNum).is(':visible') == false){
            nextNum = nextNum + 1;
            existStep = document.getElementById('next' + nextNum);
            console.log(existStep)
        }
        console.log(step)
        if(validateSteps(step))
        {

            //validar para el paso 2
            if(step == 2)
            {
                $('#next1').attr('style','display:none;');
                $('#step1').slideToggle(300)
                $('#plans').slideToggle(300)
                $('#next2').slideToggle(300)
                $('#info-declaracion-2').slideToggle(300)
            }
            if(step == 3)
            {

                if($scope.beneficiaries.length <= 0){
                    $('#next1').attr('style','display:none;');
                    $('#step1').slideToggle(300)
                    $('#plans').slideToggle(300)
                    $('#next3').slideToggle(300)
                    $('#info-declaracion-3').slideToggle(300)
                }
                else{

                    $('#next3').removeClass('active')
                    $('#next2').attr('style', 'block;');
                    $('#next2').addClass('active')
                    //$('#next2').slideToggle(300)
                    $('#info-declaracion-2').slideToggle(300)
                    //$('#next3').slideToggle(300)
                    $('#next3').attr('style', 'display:none;');
                    $('#info-declaracion-3').slideToggle(300)
                }
            }
            if(step == 4)
            {
                $('#next4').removeClass('active')
                $('#next3').attr('style', 'block;');
                $('#next3').addClass('active')
                //$('#next2').slideToggle(300)
                $('#info-declaracion-3').slideToggle(300)
                //$('#next3').slideToggle(300)
                $('#next4').attr('style', 'display:none;');
                $('#info-declaracion-4').slideToggle(300)
            }

            if(step == 5)
            {
                $('#next5').removeClass('active')
                $('#next4').attr('style', 'block;');
                $('#next4').addClass('active')
                //$('#next2').slideToggle(300)
                $('#info-declaracion-4').slideToggle(300)
                //$('#next3').slideToggle(300)
                $('#next5').attr('style', 'display:none;');
                $('#info-declaracion-5').slideToggle(300)
            }
            if(step == 6)
            {
                $('#next6').removeClass('active')
                $('#next5').attr('style', 'block;');
                $('#next5').addClass('active')
                //$('#next2').slideToggle(300)
                $('#info-declaracion-5').slideToggle(300)
                //$('#next3').slideToggle(300)
                $('#next6').attr('style', 'display:none;');
                $('#info-declaracion-6').slideToggle(300)
            }

        }

      $('.regular-input').attr('required','required')
      $('.regular-check').attr('required','required')
    }

    $scope.filterCoverage = function(coverage) {
        var a = $scope.age();
        var id = $scope.plan.id;
        return (a >= coverage.start && a <= coverage.finish && id == coverage.plan.id);
    }

    $scope.planToggle = function(plan_id) {

        $scope.edad = $scope.age()
        $scope.dato = plan_id
        var age = $scope.age()
        $scope.traerCobertura(plan_id)

        $scope.plan = $scope.findPlan(plan_id);
        $('#insurance_plan').val($scope.plan.id);
        if ($scope.cover.length > 0){
            $scope.cover.forEach(function(element, index, data){
                element.forEach(function(e, i, d){
                    if (e.plan.id == $scope.plan.id) {
                     
                        if (age >= e.start && age <= e.finish) {
                       
                            $scope.updatePrice(e.price);
                            $scope.target.start = e.start;
                            $scope.target.finish = e.finish;
                            $scope.target.price = e.price;
                            $scope.prima_uf = parseFloat(e.price)
                            $scope.prima = $scope.upPrice($scope.prima_uf * $scope.uf)
                            $('#insurance_price').val($scope.prima_uf)
                            
                        }
                    }
                });
            });
        }
        if($scope.edad > $scope.target.finish | $scope.edad < $scope.target.start ){
            if($("#insurance_insured_birthday_day").val() != null && $("#insurance_insured_birthday_month").val() != null && $("#insurance_insured_birthday_year").val() && null)
            {
                newAlert("La edad selecionada se encuentra fuera del rango permitido")
            }
            $('#insurance_insured_birthday_year').val('');
            $("#insurance_insured_birthday_year").attr("required", "required");
            $("#insurance_insured_birthday_year").attr("aria-required", "true");
            $("#insurance_insured_birthday_year").attr("aria-invalid", "true");
            $scope.prima = 0;
            //$scope.prima_uf = 'no es posible calcular la prima :D'
        }
        $scope.activeCampaign($scope.plan);
    };
    
    $scope.traerCobertura = function(plan_id){
        $scope.cober = []
        for (var i = 0; i < $scope.coverages.length; i++) {
            if($scope.coverages[i].plan.id == plan_id)
            {
                    $scope.cober.push({
                        'capital': $scope.coverages[i].capital,
                        'id': $scope.coverages[i].id,
                        'name': $scope.coverages[i].name
                    
                    })
            }
        }
    }
    function setInsured(val)
    {
        console.log('val:'+val)
        console.log('edad: '+$scope.age())
        if(val == 0 || $scope.age() == 0)
        {
            $scope.rango = 0
        }else
        {
            $scope.rango = 1
        }
        console.log($scope.rango)
    }


}]);
