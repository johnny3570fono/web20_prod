app.controller('details', ['$scope',  function($scope, $http) {
	BaseController.call(this, $scope);
	$scope.visible=0;
    $scope.submit = 2;
    $scope.coverages = coverages;
    $scope.plans =  plans;
	$scope.errors = errors;
	$scope.consultar = 1;
	$scope.filterCoverage = function(coverage){
        return ($scope.plan.id == coverage.plan.id);
    };
	
	$scope.setConsultaHogar = function setConsultaHogar(value)
	{
    if (value == 1) {
        $('input[data-value]').each(function(){
            $(this).val();
        });
        $('select[data-value]').each(function(){
            $(this).val();
        });
       
    } else {
        $('select[data-value]').each(function(){
            $(this).parent(".error").removeClass("error");

        });
    }
	}

      var valor = 1;   
      $scope.respuesta = 1;
      $(document).on('ready',function(){
      	$('#ver').attr('type','submit')
      })
      $(document).on('click','.consulta_hogar',function(){
      	$scope.respuesta = $(this).val();
      	if($scope.respuesta == 1)
      	{	
      		$('#ver').attr('type','submit')
      		$('#insurance_home_address').val($('#insurance_insured_address').val())
      		$('#insurance_home_addressState').val($('#insurance_insured_addressState').val())
      		$('#insurance_home_addressCity').val($('#insurance_insured_addressCity').val())
      	}
      	else
      	{
      		$('#ver').attr('type','button')
      		console.log($scope.submit)
      	}
      })

      $('button.btn-continuar').click(function(){
        //debugger
        if($('#next1').attr('style','display:none;'))
        {
          $('#next1').removeAttr('style','display:none;');
          $('#next2').removeAttr('style','display:none;');
        }

        var stepNum = parseInt($(this).data('step'));
        var nextNum = parseInt($(this).data('next'));
        var existStep = document.getElementById('next' + nextNum);

        while(existStep && $('#next' + nextNum).is(':visible') == false){
            nextNum = nextNum + 1;
            existStep = document.getElementById('next' + nextNum);
        }
        next = $('#next' + nextNum);
        
        if (validateSteps(stepNum)) {
            if (stepNum == 1) {
                $('#step_first_selected').hide();
                $('#plans').hide();
                $('#step1').slideToggle(300);
                $('div.box-declaracion.selected_plan').show();
            if($scope.respuesta == 0){

            	$('#box-declaracion').css('display','block')
                $('#next2').show();
                $('#next2').addClass('active');
                //$('#paso2').slideToggle(300);
                //$('#paso2').css('display','block');
                $('#paso2').css('display','block')
                $('#paso2').attr('style','display:block')
                if($('#next2').hasClass('active'))
                {

                    $('#info-declaracion-2').removeClass('hidden');
                    $('#info-declaracion-2').css('display','block');
                    $('#paso2').attr('style','display:block;') 
                    //$('#next'+stepNum).removeClass('active');
                    $('#next'+stepNum).parent().next().slideToggle(300);  
                }
            	}
            	else
            	{
           
            $('#insurance_home_address').val($('#insurance_insured_address').val())
      			$('#insurance_home_addressState').val($('#insurance_insured_addressState').val())
      			$('#insurance_home_addressCity').val($('#insurance_insured_addressCity').val())
            	}
            }
            if (stepNum == 2) {
            $('#paso2').attr('style','display:block')
            $('#paso2').css('display','block')
           	$('#next2').show();
            $('#next2').removeClass('active');
            $('#info-declaracion-2').slideToggle(300);
            }

            //$('#next2').show();
            //$('#next2').removeClass('active');
            //$('#info-declaracion-2').slideToggle(300);
            
        }
    });
    $scope.style = function(val)
    {
      
       $scope.consultar =  1;
       if (valor == 0) {
            $scope.consultar =  val;  
           
            
        }else{
            $scope.consultar = 1;
        }
        
    }
    $scope.volver = function(step,next){
        $('.regular-input').removeAttr('required')
        $('.regular-select').removeAttr('required')
        //lugar
        var stepNum = parseInt(step);
        //paso anterior
        var nextNum = parseInt(next);
        //paso antior 2.0 xD
        var existStep = nextNum;
        //console.log($('#next' + nextNum).is(':visible') == false)    retorna false
        while(existStep && $('#next' + nextNum).is(':visible') == false){
            nextNum = nextNum + 1;
            existStep = document.getElementById('next' + nextNum);
            console.log(existStep)
        }
        if(validateSteps(step))
        {
          $('#next2').attr('style','display:none;');
          $('#info-declaracion-2').slideToggle(300)
          $('#next1').attr('style','display:none;');
          $('#plans').slideToggle(300)
          $('#step1').slideToggle(300)
        }

      $('.regular-input').attr('required','required')
      $('.regular-select').attr('required','required')
    }
    /* VOLVER QUE SE ENCUENTRA EN APP.JS
    $scope.volver = function(step,next){
        $('.regular-input').removeAttr('required')
        var stepNum = parseInt(step);
        var nextNum = parseInt(next);
        var existStep = nextNum;
        while(existStep && $('#next' + nextNum).is(':visible') == false){
            nextNum = nextNum + 1;
            existStep = document.getElementById('next' + nextNum);
        }
        next = $('#next' + nextNum);
        if (validateSteps(stepNum)) {
            if (nextNum == 1) {
                $('#step_first_selected').show();
                $('#plans').show();
                $('#step1').show();
                $('#next'+stepNum).parent().next().slideToggle(300);
                $('div.box-declaracion.selected_plan').hide();

            } else {
                 $('div.box-declaracion.selected_plan').show();
                $('#step2').show();
                $('#next'+stepNum).parent().next().slideToggle(300);
                $('#info-declaracion').attr('style','display:block;');
            }
            
        }
        $('.regular-input').attr('required','required')
        
    }*/

}]);
