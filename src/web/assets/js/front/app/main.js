$(document).ready(function(){

	$("form").submit(function() {
							 $(this).submit(function() {
									 return false;
							 });
							 return true;
					 });
	// wizard clone títulos para la versión mobile
	var cantWizard = $(".wizard .wizard-step").size();
	var titleWizard = $(".wizard .active .wizard-text span").text();
	var dotWizard = $(".wizard .active .wizard-dot").text();

	$(".wizard-mobile .wizard-text").text(titleWizard);
	$(".wizard-mobile .size-wizard").text(cantWizard);
	$(".wizard-mobile .step").text(dotWizard);

	// cantidad máxima a seleccionar
	var limit = cantSel;

	$(document).on("click", "a.str-box", function(e){
		e.preventDefault();
		if(tipo == 2 || tipo == 3)
			limit = 1

		if($(".str-box-active").length >= limit) {
			if($(this).hasClass("str-box-active")) {
				$(this).toggleClass("str-box-active");
			}
		} else {
			$(this).toggleClass("str-box-active");
		}
	});

	$(".str-input").keypress(function(){
		if ($(this).val().length > 0) {
			$(this).parents(".str-box").removeClass("str-box-error").addClass("str-box-active");
			$(this).removeClass("error");
		}
		else {
			$(this).parents(".str-box").removeClass("str-box-active");
		}
	});


	$(".str-input").keyup(function(){

		if ($(this).val().length > 0) {
			$(this).parents(".str-box").removeClass("str-box-error").addClass("str-box-active");
			$(this).removeClass("error");
		}
		else {
			$(this).parents(".str-box").removeClass("str-box-active");
		}

	});

	// collapse resultado
	$(document).on("click", ".str-event-collapse", function(){
		$(this).toggleClass("is-active");
		$(this).parents(".str-collapse-seguro").find(".str-collapse-body").slideToggle();
	});


	$(".modal-paso-2").hide();



	$(document).on("click", "[data-toggle='modal']", function () {
		$("html, body").animate({ scrollTop: 0 }, 600);
		return false;
	});


	// slick js
	function creaCarousel() {
		$(".slider").addClass("slider-carousel");
	}

	function IniciaCarousel() {

		var $carousel = $(".slider-carousel");

		if(!$carousel.hasClass("slick-initialized")) {
			$(".slider-carousel").slick({
				centerMode: true,
				centerPadding: "30px",
				slidesToShow: 1,
				dots: true,
				infinite: false,
				arrows: false
			});
		}
	}


	function destoyCarousel() {

		var $carousel = $(".slider-carousel");

		if($carousel.hasClass("slick-initialized")) {
			$carousel.slick("unslick");
		}
	}

	$(window).on("load", function() {

		if ($(window).width() < 800) {
			creaCarousel();
			IniciaCarousel();
			swipeRemoveDots();
			removeDots();
		} else {
			destoyCarousel();
		}
	});


	$(window).on("resize", function() {

		if ($(window).width() < 800) {
			creaCarousel();
			IniciaCarousel();
			swipeRemoveDots();
			removeDots();
		} else {
			destoyCarousel();
		}
	});

	// ocultar dots mobile
	function removeDots() {
		if ($(".slick-current").hasClass("remove-dots")) {
			$(".slider .slick-dots").css("visibility", "hidden");
			$(".hidden-no-dots").hide();
		} else {
			$(".slider .slick-dots").css("visibility", "visible");
			$(".hidden-no-dots").show();
		}
	}

	function swipeRemoveDots() {
		$(".slider").on("swipe", function(event, slick){
			var	itemVisible = $(".remove-dots");

			if ($(itemVisible).hasClass("slick-current")) {
				$(".slider .slick-dots").css("visibility", "hidden");
				$(".hidden-no-dots").hide();
			}
			else {
				$(".slider .slick-dots").css("visibility", "visible");
				$(".hidden-no-dots").show();
			}
		});
	}

	// RANGO SLIDER
	var rangeSlider = function(){
		var slider = $(".range-slider"),
			range = $(".range-slider__range"),
			value = $(".range-slider__value");

		slider.each(function(){

			value.each(function(){
				var value = $(this).prev().attr("value");
				$(this).html(value);
			});

			range.on("input", function(){
				$(this).next(value).html(this.value);
			});
		});
	};

	rangeSlider();
	//FIN RANGO SLIDER

	function newAlert($mensaje){
		$(".error-box .error-content").html($mensaje);
		$.featherlight($(".error-box"), {closeIcon: ".error-icon", otherClose: ".error-close"});
		$(".featherlight-content").addClass("error-border");
	}
	function newAlert2($mensaje){
		$(".error-box .error-content").html($mensaje);
		$.featherlight($(".error-box"), { closeIcon: ".error-box .error-icon", otherClose: ".error-close"});
		$(".featherlight-content").addClass("error-border");
	}

});
