
function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");

    for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");

        if (pair[0] == variable){
            return pair[1];
        }
    }
    return(false);
}

$(document).ready(function() {

	$('.accordeon .own-carousel').each(function (){ $(this).find('.content-own:first').show() })

	$('.own-carousel .item-own .header-own').bind('click', function(e){
		$('.own-carousel .item-own').removeClass('active');
		$('.own-carousel .content-own').slideUp(300);
		$(this).parent().addClass('active');
		$(this).parent().find('.content-own').slideToggle(300);
	});

	$('.combo-box .option').click(function(){
		$(this).parent().find('.icons-buttons').slideToggle(300);
		$('.accordeon .own-carousel').each(function (){ $(this).find('.content-own:first').show() })
	});

	$('.icons-buttons li a').bind('click',function(){
		var obj = $(this).html();
		var mydata = $(this).data();
		var mytext = (mydata.target);
		$('.combo-box .option').empty().append(obj);
//		$(this).parent().parent().slideToggle(300);
		$(this).parent().parent().toggle(10);
		$('.hidden').hide();
		$('body').find('#'+mytext).slideToggle(300);

		return false;
	});
	$('#fam-info').hide();
	$('.combo-box').hide();

	$('#main-icons ul li a').click(function(){
		$('#fam-info').show();
		$('.combo-box').show();
	});

});
