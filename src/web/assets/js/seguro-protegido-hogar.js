$(document).ready(function(){
  $('.tooltip-accordeon ul li a').click(function(){
  	if($(this).hasClass('active')){
  		$(this).removeClass('active');
  		$('.tooltip-accordeon ul li .info').slideUp(300);
  	}else{
	  	$('.tooltip-accordeon ul li a.active').removeClass('active');
	  	$('.tooltip-accordeon ul li .info').css('display','none');
	  	var ele = $(this);
	  	ele.parent().find('.info').slideToggle({
				  		duration : 300,
				  		queue : true,
				  		start : function(){
				  			ele.addClass('active');
				  		}
				  	});
	 }
  });

    $('.items-seguros a').on('click', function(e) {
        e.preventDefault();
        swactive = 0;
        if ($(this).children().hasClass('is-active'))
	        swactive = 1;

        $('.items-seguros a *').removeClass('is-active')

        if(!swactive)
        	$(this).children().addClass('is-active');
    });

	$(".items-seguros li:nth-last-of-type(2n)").before('<li class="break-mobile"></li>');
   
});
