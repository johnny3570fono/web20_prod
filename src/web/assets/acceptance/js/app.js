$( document ).ready(function() {
    $('[data-toggle="collapse"]').click(function() {
        var target = $(this).data('target');
        var icon = $(this).find('.icon');
        if(icon.hasClass('icon-down')) {
            icon.removeClass('icon-down');
            icon.addClass('icon-up');
        } else {
            icon.removeClass('icon-up');
            icon.addClass('icon-down');
        }
        $(target).toggle();
    })

    $('#proposal input').change(function() {
        var btnPol = $(this).closest('form').find('[data-submit]');
        btnPol.removeAttr('disabled');
    });
    $("#proposal-terms input").change(function() {
        var checked = $("#proposal-terms input:checked");
        if(checked.length >0) {
            $('[data-submit="accept"]').removeAttr('disabled');
        } else {
            $('[data-submit="accept"]').attr('disabled', 'disabled');
        }
    });
});