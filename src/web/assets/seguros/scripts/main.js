"use strict";
$(document).on("ready", function() {
    function t() {
        setTimeout(function() {
            $(".sts-alert").fadeOut()
        }, 3e3)
    }

    function s() {
        $("html, body").animate({
            scrollTop: "100px"
        }, 600)
    }! function() {
        var t = $(".sts-wizard"),
            s = $(".sts-wizard").data("current"),
            e = $(".sts-wizard").data("pasos");
        $(t).find(".sts-current, .sts-wizard__dots").html(s), $(t).find(".sts-pasos").html(e), $(t).find(".sts-wizard__progress").css("width", 100 * s / e + "%")
    }(),
    function() {
        $(".sts-form__input").blur(function() {
            0 === $(this).val().length ? $(this).parents(".sts-form").removeClass("sts-form--valid") : $(this).parents(".sts-form").addClass("sts-form--valid")
      
        })
    }(),
    function() {
        $(document).on("click", ".sts-block--toggle__head", function(t) {
            t.preventDefault(), $(this).parents(".sts-block--toggle").toggleClass("sts-toggle--active")
        })
    }(),
    function() {
        $(document).on("click", '[data-evento="simular"]', function(t) {
            t.preventDefault();
            var s = $(this).data("text");
            $(this).find("span").text(s), $(this).parents(".sts-block__body-inner").prepend('<div class="overlay"><div class="cnt-load"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="27px" height="25px" viewBox="0 0 27 25" enable-background="new 0 0 27 25" xml:space="preserve"><path fill="#ec0000" d="M13.9,1.19c0,3.07,5.1,6.49,5.1,10c0,0,0,0.33-0.14,0.7C23.63,12.9,27,15.3,27,18.11c0,3.771-6.02,6.87-13.469,6.89h-0.15C6,25,0,22,0,18.25c0-2.81,3.66-5.08,8.07-6.29c0,1.55,5.03,6.459,5.15,8.45c0,0,0.02,0.17,0.02,0.36c0,0.101,0,0.19-0.02,0.29c1.08-0.58,1.08-2.4,1.08-2.4c0-4.3-4.88-6.22-4.88-10.45c0-1.65,0.75-2.86,1.44-3.2V6.2c0,3.07,5.28,6.51,5.28,9.32v0.92c1.23-0.489,1.23-2.79,1.23-2.79c0-3.87-4.911-6.02-4.911-10.45c0-1.65,0.76-2.86,1.44-3.2V1.19z"/></svg><div class="uil-ring-css"><div></div></div></div></div>'), setTimeout(function() {
                $(".sts-result").fadeIn(), $(".overlay").remove()
            }, 1500), $("html, body").animate({
                scrollTop: "400px"
            }, 600)
        })
    }(),
    function() {
        $(document).on("click", '[data-evento="eliminar"]', function(t) {
            t.preventDefault(), $(this).parents(".sts-block").fadeOut()
        })
    }(),
    function() {
        $(document).on("click", '[data-evento="editar"]', function(e) {
            e.preventDefault();
            var a = $(this).parents(".sts-block"),
                n = a.find(".sts-nombre").text(),
                o = a.find(".sts-rut .sts-dato").text(),
                i = a.find(".sts-parentesco").text(),
                d = a.find(".sts-capital .sts-dato").text(),
                l = a.find(".sts-dia").text(),
                r = a.find(".sts-mes").text(),
                c = a.find(".sts-anio").text();
            $(".sts-form--nombre").addClass("sts-form--valid"), $(".sts-form--nombre").find(".sts-form__input").val(n), $(".sts-form--rut").addClass("sts-form--valid"), $(".sts-form--rut").find(".sts-form__input").val(o), $('.sts-form--dia option[value="' + l + '"]').attr("selected", "selected"), $(".sts-form--dia").parents(".sts-form--select").addClass("sts-form--valid"), $('.sts-form--mes option[value="' + r + '"]').attr("selected", "selected"), $(".sts-form--mes").parents(".sts-form--select").addClass("sts-form--valid"), $('.sts-form--anio option[value="' + c + '"]').attr("selected", "selected"), $(".sts-form--anio").parents(".sts-form--select").addClass("sts-form--valid"), $('.sts-form--parentesco option[value="' + i + '"]').attr("selected", "selected"), $(".sts-form--parentesco").addClass("sts-form--valid"), $(".sts-form--capital").addClass("sts-form--valid"), $(".sts-form--capital").find(".sts-form__input").val(d), $('[data-evento="accion"]').text("Guardar").addClass("sts-guardar"), s(), $(a).fadeOut(), setTimeout(function() {
                $(a).find(".sts-alert").show()
            }, 600), t()
        })
    }(),
    function() {
        $(document).on("click", ".sts-guardar", function() {
            $(this).text("Agregar beneficiario");
            var s = $(".sts-new");
            $(s).find(".sts-form, .sts-form--select").removeClass("sts-form--valid"), $(s).find("input").val(""), $(s).find(".sts-form--select option[value='00']").attr("selected", "selected"), $(".sts-block").fadeIn(), t(), $(this).removeClass("sts-guardar")
        })
    }(),
    function() {
        $(document).on("click", '[data-evento="accion"]', function() {
            $("html, body").animate({
                scrollTop: "400px"
            }, 400), $(".sts-block.sts-hide").addClass("animated shake").removeClass("sts-hide"), t()
        })
    }(),
    function() {
        $(document).on("click", "[data-modal]", function(t) {
            t.preventDefault();
            var s = $(this).data("modal");
            $('.sts-modal[id="' + s + '"]').addClass("sts-modal-active"), $(".sts-modal-backdrop").addClass("sts-modal-active"), $("body").addClass("modal-visible")
        })
    }(),
    function() {
        $(document).on("click", '[data-modal="close"]', function(t) {
            t.preventDefault(), $(".sts-modal-active").removeClass("sts-modal-active"), $("body").removeClass("modal-visible")
        })
    }(),
    function() {
        $(".textfield").on("keyup paste", function() {
            var t = $(this),
                s = $(t).val().replace(/(<([^>]+)>)/gi, "").length;
            $(t).parents(".sts-parents").find(".counter").text(300 - s)
        })
    }(),
    function() {
        $(".sts-form--legal input").change(function() {
            1 == $(this).prop("checked") ? $(".sts-next").removeAttr("disabled") : $(".sts-next").attr("disabled", "disabled")
        })
    }(),
    function() {
        $(document).on("click", "[data-show]", function() {
            var t = $(this).data("show");
            $(this).parents(".sts-parents").find(".sts-cont").addClass("sts-hide"), $(this).parents(".sts-parents").find('.sts-cont[data-content="' + t + '"]').removeClass("sts-hide")
        })
    }(),
    function() {
        $(document).on("click", "[data-url]", function() {
            var t = $(this).data("url");
            $(".sts-btn-next").attr("href", t)
        })
    }()
});