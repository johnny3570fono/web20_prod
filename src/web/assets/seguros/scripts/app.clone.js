/*! Js para manejar seguros publicos zurich */
jQuery(document).ready(function($){
	base.run($);
});
var regmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
$.fn.getType = function(){ return this[0].tagName == "INPUT" ? this[0].type.toLowerCase() : this[0].tagName.toLowerCase(); }
var base = {
	run : function($){
        var dateToday = new Date(); 
		$('input, select').click(function(){
            
            if(! $(this).hasClass('ng-invalid-rut')){
                var invalid = $(this).closest('.sts-form--invalid');
                invalid.removeClass('sts-form--invalid').next(".sts-notice").remove();
			}
		}).keydown(function(){
			var invalid = $(this).closest('.sts-form--invalid');
			invalid.removeClass('sts-form--invalid').next(".sts-notice").remove();
		}); 
		$('#insurance_traveldate').datepicker({ onSelect: function (date) {
			$(this).removeClass('ng-invalid ng-invalid-required');
		}, minDate: dateToday });
		$(".telefono").mask('000000000');
		$(".input_cardnumber").mask('0000-0000-0000-0000');
		$(".kg, .cm, .capital").mask('000');
		$(".sts-form--radio [type=radio]").click(function(){
			var $parentContainer = $(this).closest('.sts-parents');
			var $textarea = $parentContainer.find('textarea');;
			$parentContainer.find('[type=radio]').prop('checked', false);
			$textarea.val('');
			$(this).prop('checked', true); 
			if($(this).val()=='No'){
				$textarea.val('No');
			}
		});
		$('input').each(function(){
            //console.log($(this).attr('name'));
            
			if($.trim($(this).val())!=='' && $(this).hasClass('ng-invalid') == false){
				$(this).closest('.sts-form').addClass('sts-form--valid');
			}
		});
        $('[ng-rut]').keyup(function(){
            var container =  $(this).parent(); 
            if(parseInt($(this).val().length) > 10){
               if($(this).hasClass('ng-invalid')){
                   container.removeClass('sts-form--valid');
                   container.addClass('sts-form--invalid');
               }
            }
        });
        // wizard
        if( $(".sts-current").length ) {
            var step = parseInt( $(".sts-current").text() );
            base.wizardBar( step );
        }
	},
	errorStep : function($element){
		var error = false;
	
		$element.find('input, select, textarea').each(function(i, value){
			var container = $(this).parent();
			if( $(this).attr('type') == 'email'){
				if(!regmail.test($(this).val()) ){
					error = true; 
					container.addClass('sts-form--invalid');
					$(this).addClass('ng-invalid-email');
				}
			} else if($(this).is(':radio')){ 
				if(!$(this).is(':checked')){
					if( typeof $(this).attr('name') !== "undefined"){
						var similar = '[name="'+$(this).attr('name')+'"]:checked';
						if( $(similar).length == 0){
							error = true;
							container.closest('.sts-form--inline').addClass('sts-form--invalid');
						}else{
							container.closest('.sts-form--inline').removeClass('sts-form--invalid');
						}
					}
				}
			}  else if( $(this).hasClass('ng-invalid') ){
				error = true; 
				container.addClass('sts-form--invalid');
				//elementHtml.push($(this).data('label'));
			}  else if($(this).find('option').length == 0){
				if($(this).val() == '' || parseInt($(this).val())==0){
					error = true; 
					container.addClass('sts-form--invalid');
				} 
			}  else if(parseInt($(this).val()) == 0 ) {
				error = true;
				container.addClass('sts-form--invalid');
			}
		});
        
		base.notifyError($element);
		return error;
	},
	notifyError: function($element){
		$($element).find('.sts-notice').remove();
		$($element).find('.sts-form--invalid').each(function(){
			$(this).after( $('<div/>',{
					class : 'sts-notice'
					}). html('<div class="sts-alert__text">'+
					'<p>'+$(this).data('error')+'</p>'+
				'</div>')
			);
		});
	},
	errorElement: function($element){
		var error = false;
		
		$($element).find('input, select').each(function(){
			var container = $(this).parent();
			if( $(this).hasClass('ng-invalid') ){
				error = true; 
				container.addClass('sts-form--invalid');
			}else if($(this).is(':radio') && !$(this).is(':checked')){
				var similar = '[name="'+$(this).attr('name')+'"]:checked';
				if( $(similar).length == 0){
					error = true;
					container.addClass('sts-form--invalid');
				}
			}  else if($(this).find('option').length == 0){
				
				if($(this).val() == '' || parseInt($(this).val())==0){
					error = true; 
					container.addClass('sts-form--invalid');
				} 
			} else {
				if(parseInt($(this).val()) == 0 ){
					error = true;
					if(container.closest('.sts-parent-select').length != 0){
						container.closest('.sts-parent-select').addClass('sts-form--invalid');
					}else{
						container.addClass('sts-form--invalid');
					}
				} 
			}
		});
		base.notifyError($element);
		return error;
	},
	resetElement : function($element){
		if(typeof $element != "undefined"){
			var $sts = $($element).find('input, select').closest('.sts-form--valid');
			$sts.removeClass('sts-form--valid').removeClass('sts-form--invalid');
		}
	},
	updateInputElement : function($element){
		setTimeout(function(){
			var $inputs = $($element).find('input, select');
			$('html, body').animate({scrollTop : $($element).offset().top },800);
			$inputs.removeClass('sts-form--valid').removeClass('sts-form--invalid');
			$inputs.each(function(){
				var container = $(this).parent();
				if($(this).is(':radio')){ 
					if(!$(this).is(':checked')){
						var similar = '[name="'+$(this).attr('name')+'"]:checked';
						if( $(similar).length == 0){
							container.addClass('sts-form--invalid');
						} else {
							container.addClass('sts-form--valid');
						}
					}
				}  else if($(this).find('option').length == 0){
					if($(this).val() == '' || parseInt($(this).val())==0){
						container.addClass('sts-form--invalid');
					} else {
						container.addClass('sts-form--valid');
					}
				} else {
					if(parseInt($(this).val()) == 0 ){
						container.addClass('sts-form--invalid');
					} else {
						container.addClass('sts-form--valid');
					}
				}
			});
		},200);
	},
	ngloading: function($element, $show){
		var $result = $(".sts-result");
		$show = ( typeof $show != "undefined" )?  $show : true;
		if( $show == true ){
			$result.hide();
			$($element).find(".sts-relative-loader").
			prepend('<div class="overlay"><div class="cnt-load">'+
			'<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"'+
			' x="0px" y="0px" width="27px" height="25px" viewBox="0 0 27 25"'+
			' enable-background="new 0 0 27 25" xml:space="preserve"><path fill="#ec0000" '+
			'd="M13.9,1.19c0,3.07,5.1,6.49,5.1,10c0,0,0,0.33-0.14,0.7C23.63,12.9,27,15.3,27,18.11c0,3.771-6.02,6.87-13.469'+
			',6.89h-0.15C6,25,0,22,0,18.25c0-2.81,3.66-5.08,8.07-6.29c0,1.55,5.03,6.459,5.15,8.45c0,0,0.02,0.17,0.02'+
			',0.36c0,0.101,0,0.19-0.02,0.29c1.08-0.58,1.08-2.4,1.08-2.4c0-4.3-4.88-6.22-4.88-10.45c0-1.65,0.75-2.86,'+
			'1.44-3.2V6.2c0,3.07,5.28,6.51,5.28,9.32v0.92c1.23-0.489,1.23-2.79,1.23-2.79c0-3.87-4.911-6.02-4.911-10.45c0-1.65,'+
			'0.76-2.86,1.44-3.2V1.19z"/></svg><div class="uil-ring-css"><div></div></div></div></div>');
		} else {
			
			$result.fadeIn(), $(".overlay").remove();
			
			if($result.length != 0){
				$("html, body").animate({
					scrollTop: $result.offset().top
				}, 1000);
			}
		}
	},
	jOserialize : function(data){
		return Object.keys(data).map(function(k) {
		    return encodeURIComponent(k) + '=' + encodeURIComponent(data[k])
		}).join('&');
	},
	wizardBar : function(paso){
		var t = $(".sts-wizard"),
        b = $(".sts-barra li"),
		s = paso,
		e = t.data("pasos");
        
        var i = 1;
        b.removeClass("active");
        b.each(function(k,v){
            if( i <= paso ) {
                $(v).addClass("active");
            }
            i++;
        });

		t.find(".sts-current, .sts-wizard__dots").html(s),
		t.find(".sts-pasos").html(e);
		// t.find(".sts-wizard__progress").animate({"width": 100 * s / e + "%"}); 
	}
};
 
var app = angular.module('portal', ['platanus.rut', 'credit-cards']).config(function($interpolateProvider){
    $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
});


angular.module('portal').controller("products", ['$scope', '$element', '$http', '$filter',
 function($scope,$element, $http, $filter) {
 	  $scope.plans = {};
 	  //$scope.cargas = { min:1, max:1};
 	  $scope.product = null;
	  $scope.range = false;
	  $scope.replans = [];
	  $scope.initial = function($slug) {
	  	$scope.product = $slug;
	  	$scope.getProducto($slug).success(function(){
			$('.ng-fix-hide').removeClass('ng-fix-hide');
		});
	  }

	  $scope.getProducto = function($slug){
	  	var response = $http.get(app.url + '/seguros/planes/'+$slug);
		response.success(function(data){
			$scope.plans = data;
            //console.log(data);
			
		});	
		return response;  	
	  }
	  
	  $scope.contratar = function($event, $id){
	  	 var $form = $($element);
	  	 var $input = $form.find('[name="insured[plan]"]');
        // var $inputCoverage = $form.find('[name="insured[coverage]"]');
         var $button =  $($event.target);
         var $coverage = $button.closest('.ng-scope').find('.ng-coverage');
        
       
	  	 if($input.length != 0){
	  	 	$input.val($id);
            if($coverage.length != 0){
                $coverage.prop('checked', true);
            }
	  	 	$form.submit();
	  	 } 
	  }
	  
	 $scope.getHospital = function($event, $id){
		if($scope.fields.asegurados !== 'undefined'){
			base.ngloading($element, true);
			$scope.getProducto($scope.product).success(function(plans){
				var cargas = $scope.fields.asegurados;
	 			var asegurados = parseInt(cargas) + 1;
				angular.forEach(plans, function(value, key) {
					price = parseFloat(plans[key].price.replace(",","."));
					//console.log(plans[key]);
					plans[key].price = price * asegurados;
					plans[key].price = plans[key].price.toFixed(2);
					if(!$scope.getIssetPlaninCargas(plans[key], cargas)){
						delete plans[key];
					}
				});
				$scope.plans = plans.filter(function(e){return e});
				base.ngloading($element, false);
	 		});
		}
	}

	$scope.getViajeLargo = function($event, $id){
		if($scope.fields.dias !== 'undefined' && $scope.fields.viajeros !== 'undefined'){
			base.ngloading($element, true);
			$scope.getProducto($scope.product).success(function(plans){
				var dias = $scope.fields.dias ;
				var viajeros = parseInt($scope.fields.viajeros);
				
				angular.forEach(plans, function(value, key){
					if( ! $scope.getIssetdaysCoverages(plans[key].coverages, dias) || 
                        !$scope.getIssetPlaninCargas(plans[key], viajeros) ){
						delete plans[key];
					}
				});
				base.ngloading($element, false);
				$scope.plans = plans.filter(function(e){return e});
			});
		}
		
	}
	//evaluar regla
	$scope.getViaje =  function($event, $id){
		if( $scope.fields.viajeros !== 'undefined'){
			var viajeros = $scope.fields.viajeros;                                                                
			base.ngloading($element, true);
			$scope.getProducto($scope.product).success(function(plans){
                angular.forEach(plans, function(value, key){
					if(!$scope.getIssetPlaninCargas(plans[key], viajeros)){
						delete plans[key];
					} 
				});	
                $scope.plans = plans.filter(function(e){return e});
				base.ngloading($element,false);
			});
			
		}
	
	}
	
	$scope.getOncologico = function($event, $id){
		base.ngloading($element);
		var age = $scope.getAgeToDateBirthday($scope.fields.birthday);
		$scope.getProducto($scope.product).success(function(plans){
			$scope.range = "error";
			angular.forEach(plans, function(value, key){
				var match = false;
				if(plans[key].target.length != 0){
					var targets = plans[key].target;
					angular.forEach(targets, function(v, k){
						if(targets[k].start <= age && targets[k].finish >= age){
							$scope.range = {start:targets[k].start,  end:targets[k].finish};
							plans[key].price = targets[k].price;
							match = true;
						}
					})
				}
				
		       	if ( match  == false ){ 
					delete $scope.plans[key];
				} 		
			});
			$scope.plans = plans.filter(function(e){return e});
			base.ngloading($element, false);
		});
		
	}
	
	$scope.getUrgencia = function($event, $id){
		if($scope.fields.asistencia != "undefined"){
			var asistencia = $scope.fields.asistencia;
			base.ngloading($element);
			$scope.getProducto($scope.product).success(function(plans){
				//$scope.range = "error";
				angular.forEach(plans, function(value, key){
					if(!$scope.getIssetPlaninCargas(plans[key], asistencia)){
						delete plans[key];
					} 
				});
                $scope.plans = plans.filter(function(e){return e});
				base.ngloading($element,false);
			});
		}
	}
	
	$scope.getCatastrofico = function($event, $id){
		var age = $scope.getAgeToDateBirthday($scope.fields.birthday);
		base.ngloading($element);
		$scope.range = false;
		$scope.getProducto($scope.product).success(function(plans){
			angular.forEach(plans, function(value, key){
				var match = false;
				if(plans[key].target.length != 0){
					var targets = plans[key].target;
					angular.forEach(targets, function(v, k){
						if(targets[k].start <= age && targets[k].finish >= age){
							$scope.range = {start:targets[k].start,  end:targets[k].finish};
							plans[key].price = targets[k].price;
							match = true;
						}
					})
				}
		       	(! match ) ? delete plans[key] : true;		
			});
            $scope.plans = plans.filter(function(e){return e});
			base.ngloading($element, false);
		});
	
	}
	
	$scope.getVacaciones = function($event , $id){
		if( typeof $scope.fields.viajeros !== 'undefined' && typeof $scope.fields.dias !== "undefined"){
			var viajeros = parseInt($scope.fields.viajeros);
            var dias = parseInt($scope.fields.dias);
            base.ngloading($element);
            
			$scope.getProducto($scope.product).success(function(plans){
                angular.forEach(plans, function(plan, key){
                    if(!$scope.getIssetPlaninCargas(plans[key], viajeros)){
						delete plans[key];
					}
                    angular.forEach(plan.coverages, function(coverage, keyc){
                        if(parseInt(coverage.days) !== parseInt(dias)){
                            delete plans[key];
                        }
                    });
                    
                });
                $scope.plans = plans.filter(function(e){return e});
				base.ngloading($element,false);
			});
		}
	}
	
	
	/* FUNCIONES DE NEGOCIO */
	$scope.getAgeToDateBirthday = function(birthday){
		var $day = birthday.day;
		var $month = birthday.month;
		var $year = birthday.year;
		if( typeof $day != "undefined" && typeof $month != "undefined" 
			&& typeof $year != "undefined"	){
			$day = $filter('rpad')($day);
			$month = $filter('rpad')($month - 1);
			$year = $filter('rpad')($year);
			var now = new Date();
			var birthday = new Date($year, $month, $day);
			var diff = now.getTime()- birthday.getTime();
			var age = Math.floor(diff/(1000 * 60 *60 * 24 * 365.25));
			return age;
		}
	}
	
	$scope.getPriceOnCoverage = function(coverage){
		
		var plans = $scope.plans;
		//console.log(coverage);
		var viajeros = $scope.fields.viajeros;
		
		if( typeof coverage.plan.id != 'undefined'){
			
			if( typeof $scope.plans[coverage.plan.id]!= 'undefined'){
				
				$scope.plans[coverage.plan.id].price = coverage.price;
				if(typeof viajeros != "undefined"){
					var multiprice = coverage.price.toString().split(',');
					viajeros = viajeros.toString().split(':');
					angular.forEach(viajeros, function(index, key){
						
						if( typeof multiprice[index] != "undefined"){
							$scope.plans[coverage.plan.id].price  = parseFloat(multiprice[index]);
						}
					});
					//console.log(multiprice);
				}
			}
		}
	}
	
	$scope.getPriceThisCoverage = function(coverages){
		
		if(typeof $scope.fields!= "undefined"){
			if(coverages.price != null && coverages.price != 0 && 
				typeof $scope.fields.viajeros != "undefined" ){
					
				var plans = $scope.plans;
				var viajeros = $scope.fields.viajeros;
				angular.forEach(plans, function(item, i){
					if(item.coverages != "undefined"){
						angular.forEach(item.coverages, function(coverage, c){
							if(coverage.id == coverages.id){
								if(typeof viajeros != "undefined"){
									var multiprice = coverage.price.toString().split(',');
									viajeros = viajeros.toString().split(':');
									angular.forEach(viajeros, function(index, key){
										if( typeof multiprice[index] != "undefined"){
											$scope.plans[i].price  = parseFloat(multiprice[index]);
										} else if( typeof multiprice != "undefined"){
                                            $scope.plans[i].price = parseFloat(multiprice);
                                        }
									});
									
								}
							}
						});
					}
				});
				
			}
		}
	}
  
  	$scope.getIssetdaysCoverages  = function(coverages, dias){
  		var isset = false;
  		dias = dias.split(':');
  		if( typeof dias[0] != 'undefined' &&  typeof dias[1] != 'undefined' ){
	  		angular.forEach(coverages, function(value, key){
				if(coverages[key].days > dias[0] && coverages[key].days <= dias[1] ){
					isset = true;
				}
				
	  		})
  		}
  		return isset;
  	}
	
	$scope.getIssetPlaninCargas = function(plan,opcion){
		var isset = false;
		var opcion = opcion.toString().split(':');
		if( opcion.length == 1 ){
			//console.log('opcion 1');
			if(typeof opcion[0] != "undefined"){
				if(plan.cargasMin <= parseInt(opcion[0]) && plan.cargasMax >= parseInt(opcion[0])){
					isset = true;
				}
			}
		} else if(opcion.length != 0){
			if(typeof opcion[0] != "undefined" && opcion[1] != "undefined"){
				opcion[0]  = parseInt(opcion[0]);
				opcion[1]  = parseInt(opcion[1]);
				if(opcion[0] >= plan.cargasMin && plan.cargasMax >= opcion[1]){
					isset = true;
				}
			}
		}
		return isset;
	}
	
	$scope.getIssetPlaninTargets  = function(targets, age){
  		var match = false;
		if(typeof targets != "undefined"){
			if(targets.length != 0){
				targets = plans[key].target;
				angular.forEach(targets, function(v, k){
					if(targets[k].start <= age && targets[k].finish >= age){
						plans[targets[k].plan.id].price = targets[k].price;
						match = true;
					}
				})
			}
		}
		return match;	
  	}
	
	/* FIN FUNCIONES DE NEGOCIOS */	
	
}]);

angular.module('portal').controller("beneficiary", ['$scope', '$element', '$http' ,
	function($scope,$element, $http){
		
		$scope.insurance = {id: null};
		$scope.beneficiaries = [];
		$scope.beneficiary = {};
		$scope.id = null;
		$scope.buttonEdit = false;
		$scope.error = {limit: false, conyuge: false, rut: false, capital : false};
		$scope.limit = {max: 0, min: 0};
		
		$scope.initial = function(){
			if($($element).data('max') ){
				$scope.limit.max = parseInt($($element).data('max'));
			}
			if($($element).data('min') ){
				$scope.limit.min = parseInt($($element).data('min'));
			}
			$scope.$parent.persons = $scope.beneficiaries;
		}
		
		$scope.$watch('buttonEdit', function(){
			$scope.buttonText = $scope.buttonEdit ? 'Guardar' : 'Agregar Beneficiarios';
		})
		
		
		$scope.actionBeneficiary = function(){
			($scope.buttonEdit) ? $scope.updateBeneficiary():$scope.addBeneficiary();
		}
		
		$scope.addBeneficiary = function(){
			if( ! base.errorElement($element) && !$scope.errorBeneficiary() ){
				//var count = $scope.getLength($scope.beneficiaries);
				if(typeof $scope.beneficiary != 'undefined' ){
					var beneficiary = $scope.beneficiary;
					$scope.beneficiaries.push(beneficiary);
					$("[rel=add-beneficiary]").fadeIn('normal',function(){
						setTimeout(function(){$("[rel=add-beneficiary]").fadeOut()},2000)
					});
					$scope.resetBeneficiary();
				}
			}else{
				$scope.notifyError( $element );
			}
		}
		$scope.setError = function(){
			$scope.error.limit = true;
			$scope.error.rut = $scope.issetRut($scope.beneficiary);
			$scope.error.conyuge =  $scope.issetConyuge($scope.beneficiary);
		}
		
		
		$scope.setErrorLimit = function(){
			$scope.error.limit = true;
			setTimeout(function(){
				$scope.error.limit = false;
			},2000);
		}
		
		$scope.editBeneficiary = function($id){
			$scope.buttonEdit = true;
			$scope.id = $id;
			if( typeof $scope.beneficiaries[$id] != 'undefined'){
				$scope.beneficiary = angular.copy($scope.beneficiaries[$id]);
				base.updateInputElement($element);
				
			}
		}
		
		
		
		$scope.updateBeneficiary = function(){
			$id = $scope.id;
			if( typeof $scope.beneficiaries[$id] != 'undefined'){
				if( ! base.errorElement($element)  && !$scope.errorBeneficiary($id) ){
					angular.copy($scope.beneficiary,$scope.beneficiaries[$id]);
					$scope.resetBeneficiary();
					$scope.buttonEdit = false;
                    $('html, body').animate({scrollTop : $(".beneficiario-"+$id).offset().top },800);
				}
			}	
		}
		
		$scope.resetBeneficiary = function(){
			$scope.beneficiary = {name: '',lastname1:'', lastname2:'', code:'', birthday:0,birthmonth:0,birthyear:0,relationship:0, capital:''};
			base.resetElement($element);
		}
		
		$scope.getLength = function(obj) {
			return Object.keys(obj).length;
		}
		$scope.setErrorCapital = function(){
			$scope.errorCapital = true;
			$("body, html").scrollTop(0);
			setTimeout(function(){
				$scope.errorCapital = false;
			},2000);
		}
		
		$scope.errorBeneficiary  = function($id){
			var error = false;
			$scope.error.rut = $scope.issetRut($scope.beneficiary, $id);
			$scope.error.conyuge =  $scope.issetConyuge($scope.beneficiary, $id);
			$scope.error.capital = $scope.capitalError($scope,$id);
			$scope.error.limit =  !$scope.inLimit($scope);
			if($scope.error.rut || $scope.error.conyuge || $scope.error.capital || $scope.error.limit){
				error = true;
				setTimeout(function(){
				angular.forEach($scope.error, function(value, key){
						$scope.error[key] = false;
					});
				},2000);
			}
			
			//console.log(error)
			return error;
		}
		
		$scope.issetRut = function(beneficiary, id){
			var isset = false;
			if( typeof beneficiary.code != 'undefined' ){
				var beneficiaries = angular.copy($scope.beneficiaries);
				angular.forEach(beneficiaries, function(data, key){
					if(data.code == beneficiary.code && key !== id){
						isset = true;
					}
				});
				//validamos si el  beneficiario es igual al titular
				if( typeof $scope.$parent.user.code != "undefined"){
					if($scope.$parent.user.code == beneficiary.code){
						isset = true;
					}
				}
				
				
			}
			return isset;
		}
		
		$scope.issetConyuge = function(beneficiary, id){
			var isset = false;
			var conyuge = false;
			if(typeof beneficiary.relationship != "undefined"){
				var beneficiaries = angular.copy($scope.beneficiaries);
				angular.forEach(beneficiaries, function(data, key){
					if(data.relationship == 'Cónyuge' && id !== key ){ 
						conyuge = true;
					}
				});
				if(beneficiary.relationship == 'Cónyuge' && conyuge){
					isset = true;
				}
			}
			return isset;
		}
		
		$scope.inLimit = function(){
			var limit  = true;
			if( typeof $scope.beneficiaries != "undefined"){
				if( $scope.getLength($scope.beneficiaries) > $scope.limit.max  ){
					limit = false;
				}
			}
			return limit;
		}
		$scope.capitalError = function($scope, id){
			var error = false;
			var capital = 100;
			var clientCapital = 0;
			if( typeof $scope.beneficiaries != "undefined" || typeof $scope.beneficiary != "undefined"){
				var beneficiaries = $scope.beneficiaries;
				var beneficiary =  $scope.beneficiary;
				angular.forEach(beneficiaries,  function(value, key) {
					if(id !== key || typeof id == "undefined" ){	
						clientCapital = parseInt(beneficiaries[key].capital) + clientCapital;
					}
				});
				
				if( typeof $scope.beneficiary != "undefined"){
					if(typeof $scope.beneficiary.capital != "undefined"){
						clientCapital = parseInt($scope.beneficiary.capital) + clientCapital;
					}
				} 
				error = (clientCapital > capital);
			}
			return error;
		}
	}
]);

angular.module('portal').controller("register", ['$scope', '$element', '$http' ,'$filter','$parse',
	function($scope,$element, $http, $filter, $parse){
	$scope.step = 1;
    // $scope.user = { addressCity: 0, addressState: 0 };
    // $scope.home = { addressCity: 0, addressState: 0 };
	$scope.plan = null;
	$scope.persons = {};
	$scope.rules = {age: false, error : false};
	$scope.error = { birthday: false };
	
	$scope.initial = function(plan){
		$scope.plan = plan;
		
       
        var datalocal = $scope.getLocalStorage();
        if( datalocal ) {
            if( typeof datalocal.user !== 'undefined' ) {
                $scope.user = datalocal.user;
            }
            if( typeof datalocal.home !== 'undefined' ) {
                $scope.home = datalocal.home;
            }
        }
	}
	
	$scope.selectInit = function(variable, def){
		var model = $parse(variable);
		if(typeof $scope.$eval(variable)== "undefined"){
			if(typeof def == 'undefined'){
				var def = 0;
			}
			model.assign($scope, def);
		}
	}
	
	$scope.nextStep = function(){
		var elemento = $("[data-step]:visible");
		var current = $("[data-step]:visible").data('step');
		var last = $("[data-step]:last").data('step');
		var persons = $("[data-step]:visible [data-min]");
		var next = $("[data-step]:visible").next();
		var prev = $("[data-step]:visible").prev();
        
		if( persons.length !=0){
			var limit = { max :  persons.data('max'), min :  persons.data('min') };
			var count = $scope.persons.length;
			var $child = $scope.$$childHead;
			if(limit.min <= count &&  limit.max >= count){
				// si es beneficiario, evaluamos capital
				if( typeof $child.beneficiary !== "undefined" && !$scope.capitalEvaluate($child) ){
					$child.setErrorCapital();
				} else if(last != current){
					$scope.moveStep(current);
				} else {
                    //guardamos en localStorage
                    $scope.sendLocalStorage( $scope );
                    
					$($element).submit();
				}
			} else {
				$("html, body").scrollTop(0);
				$scope.$$childHead.setErrorLimit();
			}
		} else if( ! base.errorStep(elemento)  ){
			if( $scope.isSingle(next)){
				if( next.data('step') == last){
                
                    //guardamos en localStorage
                    $scope.sendLocalStorage( $scope );
                
                    $($element).submit();
                    return true;
				}
				$scope.moveStep(current, 2);
			} else if(last != current) {
				$scope.moveStep(current);
			} else {
                //guardamos en localStorage
                $scope.sendLocalStorage( $scope );
                
				$($element).submit();
			}
		}
	}
	
    $scope.sendLocalStorage = function ( $data ) {
        var datasend = {};
        
        // usuario
        if( typeof $data.user !== 'undefined' ) {
            datasend["user"] = $data.user;
        }
        
        //home
        if( typeof $data.home !== 'undefined' ) {
            datasend["home"] = $data.home;
        }
        
        datasend = JSON.stringify( datasend );
        localStorage.setItem( "datasend", datasend );
    }
    
    $scope.getLocalStorage = function () {
        if (localStorage.getItem("datasend") !== null) {
            return JSON.parse( localStorage.getItem("datasend") );
        }
        return false;
    }
    
	$scope.backStep = function(){
		var prev = $("[data-step]:visible").prev();
		var historia = history;
		if($scope.step > 1){
			if( $scope.isSingle(prev) ){
				$scope.moveStep($scope.step, -2);
				return true;
			}
			$scope.moveStep($scope.step, -1);
		} else{
			historia.back(1);
		}
	}
	/*
	$scope.limitEvaluate = function($elemento){
		var limit = { max : $elemento.data('max'), min : $elemento.data('min') };
		var count = $scope.persons.length;
		if( limit.min <=  count &&  limit.max >= count){
			if(limit.min == 0 && limit.max == 0){
				return 'submit';
			}
			return true;
		}
		return false;
	}
	*/
	$scope.capitalEvaluate = function($child){
		var capital = 100;
		var clientCapital = 0;
		if( typeof $child.beneficiaries != "undefined" || typeof $child.beneficiary != "undefined"){
			var beneficiaries = $child.beneficiaries;
			var beneficiary =  $child.beneficiary;
			angular.forEach(beneficiaries,  function(value, key) {
				clientCapital = parseInt(beneficiaries[key].capital) + clientCapital;
			});
			
			if( typeof $child.beneficiary != "undefined" ){
				if(typeof $child.beneficiary.capital != "undefined" ){
					if($child.beneficiary.capital != ""){
						clientCapital = parseInt($child.beneficiary.capital) + clientCapital;
					}
				}
			}
		}
		//console.log(clientCapital+"---"+capital);
		return (clientCapital == capital);
	}
	
	$scope.moveStep = function (current, move){
		$scope.step = current + ( (typeof move == 'undefined')? 1 : move);
		//console.log($scope.step);
		base.wizardBar($scope.step);
		$("html, body").scrollTop(0);
	}
	
	$scope.isSingle = function($elemento){
		var $MinMax = $elemento.find('[data-min]');
		
		if( parseInt($MinMax.data('min')) == 0 && parseInt($MinMax.data('max')) == 0){
			return true;
		}
		return false;
	};
	
	$scope.isAgePlan = function(){
		var age = $($element).data('age');
		$scope.error.birthday = false;
		
		if(typeof age != "undefined"){
			range = age.toString().split(':')
			age = $scope.getAgeToDateBirthday($scope.user.birthday);
			
			if( typeof range[0] != 'undefined' && typeof range[1] != 'undefined' ){
				if(range[0] > age || range[1] < age){
					$scope.rules.age = true;
					$scope.rules.error = true;
				} else {
					$scope.rules.age = false;
					$scope.rules.error = false;
				}
			}
		}else{
			var age = $scope.getAgeToDateBirthday($scope.user.birthday);
			if( age < 18 ) {
				$scope.error.birthday = true;
			}
		}
	}
	
	$scope.getAgeToDateBirthday = function(birthday){
		var $day = birthday.day;
		var $month = birthday.month;
		var $year = birthday.year;
		if( typeof $day != "undefined" && typeof $month != "undefined" 
			&& typeof $year != "undefined"	){
			$day = $filter('rpad')($day);
			$month = $filter('rpad')($month - 1);
			$year = $filter('rpad')($year);
			var now = new Date();
			var birthday = new Date($year, $month, $day);
			var diff = now.getTime()- birthday.getTime();
			var age = Math.floor(diff/(1000 * 60 *60 * 24 * 365.25));
			return age;
		}
	}

}]);

angular.module('portal').directive('shakeAnimate', function(){
	return {
		link: function(scope,elemento){
			$nuevo = $(elemento);
			//$nuevo.addClass('animated shake');
			$('html, body').animate({scrollTop : $nuevo.offset().top },800);
		}
	}
});
angular.module('portal').directive('alertScroll', function(){
	return {
		link: function(scope,elemento){
			$nuevo = $(elemento);
			//$nuevo.addClass('animated shake');
			$('html, body').animate({scrollTop : $nuevo.offset().top },800);
		}
	}
});
angular.module('portal').directive('buttonSimulador', function () {
    return {
		link: function (scope, element, attrs) {
			$(element).click(function(){
				var title = attrs.buttonSimulador;
				if( title != ""){
					$(element).text(title);
				}
			});
		}
	}
});

angular.module('portal').directive('onlyText', function() {
    function link(scope, elem, attrs, ngModel) {
        ngModel.$parsers.push(function(viewValue) {
          var reg = /^[^`~!@#$%\^&*()_+={}|[\]\\:';"<>?,./1-9!¡°¨]*$/;
          if (viewValue.match(reg)) {
            return viewValue;
          }
          var transformedValue = ngModel.$modelValue;
          ngModel.$setViewValue(transformedValue);
          ngModel.$render();
          return transformedValue;
        });
    }

    return {
        restrict: 'A',
        require: 'ngModel',
        link: link
    };
});

angular.module('portal').directive('onlyAlphanumeric', function() {
    function link(scope, elem, attrs, ngModel) {
        ngModel.$parsers.push(function(viewValue) {
          var reg = /^[^`~!@$%\^&*()_+={}|[\]\\:';"<>?,./!¡¨]*$/;
          if (viewValue.match(reg)) {
            return viewValue;
          }
          var transformedValue = ngModel.$modelValue;
          ngModel.$setViewValue(transformedValue);
          ngModel.$render();
          return transformedValue;
        });
    }

    return {
        restrict: 'A',
        require: 'ngModel',
        link: link
    };
});

/* INICIO DE CONTROLADOR DE CARGAS */
angular.module('portal').controller("carga", ['$scope', '$element', '$http' ,
	function($scope,$element, $http){
		$scope.cargas = [];
		$scope.carga = {};
		$scope.id = null;
		$scope.buttonEdit = false;
		$scope.limit = {min: 0, max:0 };
		$scope.error = { rut: false, limit: false, conyuge: false};
		
		$scope.initial = function(){
			if($($element).data('max') ){
				$scope.limit.max = parseInt($($element).data('max'));
			}
			if($($element).data('min') ){
				$scope.limit.min = parseInt($($element).data('min'));
			}
			$scope.$parent.persons = $scope.cargas;
		}
		
		$scope.$watch('buttonEdit', function(){
			$scope.buttonText = $scope.buttonEdit ? 'Guardar' : 'Agregar Carga';
		})
		
		$scope.actionCarga = function(){
			($scope.buttonEdit) ? $scope.updateCarga():$scope.addCarga();
		}
		
		$scope.addCarga = function(){
			
			 if( ! base.errorElement($element) && !$scope.errorCarga()){
				var count = $scope.getLength($scope.cargas) + 1;
				$scope.error = {rut: false, conyuge: false};
				if( count > $scope.limit.max ){
					$scope.setErrorLimit();
					$scope.resetCarga();
				} else if(typeof $scope.carga != 'undefined'){
					$scope.cargas.push($scope.carga);
					$("[rel=add-carga]").fadeIn('normal',function(){
						setTimeout(function(){$("[rel=add-carga]").fadeOut()},2000)
					});
					$scope.resetCarga();
				
				}
			}
		}
        
		$scope.resetCarga = function(){
			$scope.carga = {name: '',lastname1:'', lastname2:'', code:'', birthday:0,birthmonth:0,birthyear:0,relationship:0};
			base.resetElement($element);
		}
		
		$scope.editCarga = function($id){
			$scope.buttonEdit = true;
			$scope.id = $id;
			if( typeof $scope.cargas[$id] != 'undefined'){
				$scope.carga = angular.copy($scope.cargas[$id]);
				base.updateInputElement($element);
				
			}
		}
		
		$scope.updateCarga = function(){
			$id = $scope.id;
			if( typeof $scope.cargas[$id] != 'undefined'){
				if( ! base.errorElement($element)  && !$scope.errorCarga($id) ){
					angular.copy($scope.carga,$scope.cargas[$id]);
					$scope.resetCarga();
					$scope.buttonEdit = false;
                    $('html, body').animate({scrollTop : $(".carga-"+$id).offset().top },800);
				}
			}
		}
		
		$scope.getLength = function(obj) {
			return Object.keys(obj).length;
		}
		
		
		$scope.errorCarga = function($id){
			var error = false;
			$scope.error.rut = $scope.issetRut($scope.carga,$id);
			$scope.error.conyuge =  $scope.issetConyuge($scope.carga, $id);
			if($scope.error.rut || $scope.error.conyuge){
				error = true;
			}
			setTimeout(function(){
				angular.forEach($scope.error, function(value, key){
					$scope.error[key] = false;
				});
			},2000);
			return error;
		}
		
		$scope.issetRut = function(carga, id){
			var isset = false;
			if( typeof carga.code != 'undefined' ){
				var cargas = angular.copy($scope.cargas);
				angular.forEach(cargas, function(data, key){
					if(data.code == carga.code && id !== key){
						isset = true;
					}
				});
				// validamos el usuario titular
				if( typeof $scope.$parent.user.code != "undefined"){
					if($scope.$parent.user.code == carga.code){
						isset = true;
					}
				}
			}
			return isset;
		}
		
		$scope.issetConyuge = function(carga, id){
			var isset = false;
			var conyuge = false;
			if(typeof carga.relationship != "undefined"){
				var cargas = angular.copy($scope.cargas);
				angular.forEach(cargas, function(data, key){
					if(data.relationship == 'Cónyuge' && id !== key ){ 
						conyuge = true;
					}
				});
				if(carga.relationship == 'Cónyuge' && conyuge){
					isset = true;
				}
			}
			return isset;
		}
		
		$scope.setErrorLimit = function(){
			$scope.error.limit = true;
			setTimeout(function(){
				$scope.error.limit = false;
			},2000);
		}
		
		
	}
]);

angular.module('portal').directive('cargas', function(){
	return {
		link: function(scope,elemento){
			$nuevo = $(elemento);
			$nuevo.addClass('animated shake');
			$('html, body').animate({scrollTop : $nuevo.offset().top },800);
		}
	}
});
/* FIN CONTROLADOR CARGAS */

/* INICIO CONTROLADOR HOGAR */
angular.module('portal').controller("hogar", ['$scope', '$element', '$http' ,
	function($scope,$element, $http){
		$scope.initial = function(){
		}
		
		$scope.saveHome = function($event){
			if(base.errorElement($element)){
				$event.preventDefault();
			}
		}
	}
]);
//register
angular.module('portal').filter('toCommaFloat', [
function() { // should be altered to suit your needs
    return function(input) {
    	input = parseFloat(input);
    	var ret=(input)?input.toString().trim().replace(".",","):null;
        return ret;
    };
}]);

angular.module('portal').filter('toUf', [function() {
    return function(input) {
    	var decimals = 0;
		if( typeof input != "undefined"){
			if( input != null && input != ""){
				amount = input.toString().trim(); // por si pasan un numero en vez de un string
				amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto
				amount =  parseInt(Math.round(app.uf * amount));
				decimals = decimals || 0; // por si la variable no fue fue pasada

				// si no es un numero o es igual a cero retorno el mismo cero
				if (isNaN(amount) || amount === 0) 
					return parseFloat(0).toFixed(decimals);

				amount = '' + amount.toFixed(decimals);
				while( (parseInt(amount) % 10) != 0){
					amount++;
				}
				
				var amount = ''+ amount;
				var amount_parts = amount.split('.'),
					regexp = /(\d+)(\d{3})/;

				while (regexp.test(amount_parts[0]))
					amount_parts[0] = amount_parts[0].replace(regexp, '$1' + '.' + '$2');

				amount = amount_parts.join('.');
				return amount;
			}
		}
	    return input;
    	
    };
}]);

angular.module('portal').filter('orderObjectBy', function(){
 return function(input, attribute) {
    if (!angular.isObject(input)) return input;

    var array = [];
    for(var objectKey in input) {
        array.push(input[objectKey]);
    }
    
    array.sort(function(a, b){
        a = parseInt(a[attribute]);
        b = parseInt(b[attribute]);
        return a - b;
    });
    return array;
 }
});
angular.module('portal').filter('orderObjectByAlpha', function(){
 return function(input, attribute) {
    if (!angular.isObject(input)) return input;

    var array = [];
    for(var objectKey in input) {
        array.push(input[objectKey]);
    }

    array.sort(function(a, b){
		var nameA = a[attribute].toLowerCase(), nameB = b[attribute].toLowerCase();
		if (nameA < nameB)
			return -1;
		if (nameA > nameB)
			return 1;
		return 0;
    });
    return array;
 }
});
angular.module('portal').filter('rpad', function(){
	return function(input, n){
		if(input=== undefined){
			input = "";
		}
		if(input.length >= n){
			return input;
		}
		var zeros = "0".repeat(n);
		return (zeros + input).slice(-1*n);
	}	
});


angular.module('portal').filter('Oblength', function() {
    return function(items) {
        return Object.keys(items).length;
    };
});
angular.module('portal').filter('empty', function () {
	var bar; 
	return function (obj) {
		for (bar in obj) {
			if (obj.hasOwnProperty(bar)) {
				return false;
			}
		}
		return true;
	};
});