/*! Js para manejar seguros publicos zurich */


jQuery(document).ready(function($){
	base.run($);
    var originalAddClassMethod = $.fn.addClass;
    $.fn.addClass = function(){
        var result = originalAddClassMethod.apply( this, arguments );
        $(this).trigger('onClass');
        return result;
    }
   
 
});


jQuery(window).unload(function() {
   
    base.scrollIframe({scrollTop: 0 });
  
});

var regmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
$.fn.getType = function(){ return this[0].tagName == "INPUT" ? this[0].type.toLowerCase() : this[0].tagName.toLowerCase(); }


var base = {
	run : function($){
        var dateToday = new Date(); 
       
		$('input, select, textarea').click(function(){
            
            if(! $(this).hasClass('ng-invalid-rut')){
                var invalid = $(this).closest('.sts-form--invalid');
                invalid.removeClass('sts-form--invalid').next(".sts-notice").remove();
			}
		}).keydown(function(){
			var invalid = $(this).closest('.sts-form--invalid');
			invalid.removeClass('sts-form--invalid').next(".sts-notice").remove();
		}); 
       
		$('#insurance_traveldate').datepicker({ onSelect: function (date) {
            $(this).removeClass('ng-invalid ng-invalid-required');
            var $scope = angular.element($('[ng-controller="register"]')).scope();           
            $scope.user.travelDate = $(this).val();
            $scope.$apply();
		}, minDate: dateToday });
        
        $(".sts-modal").bind("onClass",function(){
            $modal = $(".sts-modal");
            $modalcontent = $('.sts-modal__content');
            $heightDiff = $modalcontent.innerHeight() - $modalcontent.height();
            $offsetTop = $modalcontent.offset().top;
            $offsetMidle = ($modalcontent.offset().top/2);
            $contentidle = ($modalcontent.innerHeight()/2)
            $modalTop =  ($offsetTop + $contentidle) - $offsetMidle;
            base.scrollIframe({
                scrollTop: $modalTop,
                timeAnimate: 800,
                animate:true
            })
        });
        
        
		$(".telefono").mask('000000000');
		$(".input_cardnumber").mask('0000-0000-0000-0000');
		$(".kg, .cm, .capital").mask('000');
		$(".sts-form--radio [type=radio]").click(function(){
			var $parentContainer = $(this).closest('.sts-parents');
			var $textarea = $parentContainer.find('textarea');;
			$parentContainer.find('[type=radio]').prop('checked', false);
			$textarea.val('');
			$(this).prop('checked', true); 
			if($(this).val()=='No'){
				$textarea.val('No');
			}
		});
		$('input').each(function(){
			if($.trim($(this).val())!=='' && $(this).hasClass('ng-invalid') == false){
				$(this).closest('.sts-form').addClass('sts-form--valid');
			}
		});
       
        $('[ng-rut]').keyup(function(){
          
            var container =  $(this).parent(); 
           
            if(parseInt($(this).val().length) > 10){
               if($(this).hasClass('ng-invalid')){
                    container.removeClass('sts-form--valid');
                    container.addClass('sts-form--invalid');
                    if( container.next('.sts-notice').length == 0){
                        container.after( $('<div/>',{
                                class : 'sts-notice'
                                }). html('<div class="sts-alert__text">'+
                                '<p>'+container.data('error')+'</p>'+
                            '</div>')
                        );
                    }
               }
            }
        });
        
        //click al boton eliminar
        $(document).on('click','a[rel="delete-cargas"]', function(e){
            e.preventDefault();
            $.featherlight("#delete-cargas",{
                targetAttr: 'href',
                variant: 'sts-modal-carga',
                closeIcon: '',
                afterOpen: function(event){
                    $fatherlight = $('.featherlight');
                    $fathercontent = $('.featherlight-content');
                    $fatherTop = parseInt($fatherlight.offset().top + $fatherlight.height()/2) - $fathercontent.height();
                    base.scrollIframe({
                        scrollTop: $fatherTop,
                        timeAnimate: 800,
                        animate:true
                    })
                }
            });
        });
        
        //click al aceptar boton de eliminar carga
        $(document).on("click", "[rel='delete-carga']", function(e){
            e.preventDefault();
            var $scope = angular.element($('[ng-controller="carga"]')).scope();
            $scope.cargas.splice($scope.carga.delete, 1);
            $scope.$apply();
        });
        //click al boton eliminar
        $(document).on('click','a[rel="delete-beneficiaries"]', function(e){
            e.preventDefault();
            $.featherlight("#delete-beneficiaries",{
                targetAttr: 'href',
                variant: 'sts-modal-carga',
                closeIcon: '',
                afterOpen: function(event){
                    $fatherlight = $('.featherlight');
                    $fathercontent = $('.featherlight-content');
                    $fatherTop = parseInt($fatherlight.offset().top + $fatherlight.height()/2) - $fathercontent.height();
                    base.scrollIframe({
                        scrollTop: $fatherTop,
                        timeAnimate: 800,
                        animate:true
                    })
                }
            });
           
        });
        //click al aceptar boton de eliminar beneficiario
        $(document).on("click", "[rel='delete-beneficiary']", function(e){
            e.preventDefault();
            var $scope = angular.element($('[ng-controller="beneficiary"]')).scope();
           
            $scope.beneficiaries.splice($scope.beneficiary.delete, 1);
            $scope.$apply();
            
            //$compile($scope.cargas);
        });
        // wizard
        if( $(".sts-current").length ) {
            var step = parseInt( $(".sts-current").text() );
            base.wizardBar( step );
        }
        
        $(document).on( "click", ".sts-form--calendar .str-icon-calendario", function(e){
            $(this).parent().find("input[type='text']").focus();
            
            e.preventDefault();
        });
	},
	errorStep : function($element){

		var error = false;
	
		$element.find('input, select, textarea').each(function(i, value){
			var container = $(this).parent();
			if( $(this).attr('type') == 'email'){
				if(!regmail.test($(this).val()) ){
					error = true; 
					container.addClass('sts-form--invalid');
					$(this).addClass('ng-invalid-email');
				}
			} else if($(this).is(':radio')){ 
				if(!$(this).is(':checked')){
					if( typeof $(this).attr('name') !== "undefined"){
						var similar = '[name="'+$(this).attr('name')+'"]:checked';
						if( $(similar).length == 0){
							error = true;
							container.closest('.sts-form--inline').addClass('sts-form--invalid');
						}else{
							container.closest('.sts-form--inline').removeClass('sts-form--invalid');
						}
					}
				}
			}  else if( $(this).hasClass('ng-invalid') ){
				error = true; 
				if(container.closest('.sts-parent-select').length != 0){
					container.closest('.sts-parent-select').addClass('sts-form--invalid');
				} else{
					container.addClass('sts-form--invalid');
				}
			}  else if($(this).find('option').length == 0){
				if($(this).val() == '' || parseInt($(this).val())==0){
					error = true; 
					container.addClass('sts-form--invalid');
				} 
			}  else if(parseInt($(this).val()) == 0 ) {
				error = true;
				container.addClass('sts-form--invalid');
			}
		});
        
		base.notifyError($element);
		return error;
	},
	notifyError: function($element){
		$($element).find('.sts-notice').remove();
		$($element).find('.sts-form--invalid').each(function(){
            var error = $(this).data('error');
            
            if( $(this).find(':input').hasClass('ng-valid-required') ) {
                if( typeof $(this).data('invalid') !== 'undefined' ) {
                    error = $(this).data('invalid');
                }
            }
            
			$(this).after( $('<div/>',{
					class : 'sts-notice'
					}). html('<div class="sts-alert__text">'+
					'<p>'+error+'</p>'+
				'</div>')
			);
		});
	},
	errorElement: function($element){
		var error = false;
		
		$($element).find('input, select').each(function(){
			var container = $(this).parent();
			if( $(this).hasClass('ng-invalid') ){
				error = true; 
				container.addClass('sts-form--invalid');
			}else if($(this).is(':radio') && !$(this).is(':checked')){
				var similar = '[name="'+$(this).attr('name')+'"]:checked';
				if( $(similar).length == 0){
					error = true;
					container.addClass('sts-form--invalid');
				}
			}  else if($(this).find('option').length == 0){
				
				if($(this).val() == '' || parseInt($(this).val())==0){
					error = true; 
					container.addClass('sts-form--invalid');
				} 
			} else {
				if(parseInt($(this).val()) == 0 ){
					error = true;
					if(container.closest('.sts-parent-select').length != 0){
						container.closest('.sts-parent-select').addClass('sts-form--invalid');
					}else{
						container.addClass('sts-form--invalid');
					}
				} 
			}
		});
		base.notifyError($element);
		return error;
	},
	resetElement : function($element){
		if(typeof $element != "undefined"){
			var $sts = $($element).find('input, select').closest('.sts-form--valid');
			$sts.removeClass('sts-form--valid').removeClass('sts-form--invalid');
		}
	},
	updateInputElement : function($element){
		setTimeout(function(){
			var $inputs = $($element).find('input, select');
            base.scrollIframe({scrollTop: 0, timeAnimate: 800, animate:true });
			$inputs.removeClass('sts-form--valid').removeClass('sts-form--invalid');
			$inputs.each(function(){
				var container = $(this).parent();
				if($(this).is(':radio')){ 
					if(!$(this).is(':checked')){
						var similar = '[name="'+$(this).attr('name')+'"]:checked';
						if( $(similar).length == 0){
							container.addClass('sts-form--invalid');
						} else {
							container.addClass('sts-form--valid');
						}
					}
				}  else if($(this).find('option').length == 0){
					if($(this).val() == '' || parseInt($(this).val())==0){
						container.addClass('sts-form--invalid');
					} else {
						container.addClass('sts-form--valid');
					}
				} else {
					if(parseInt($(this).val()) == 0 ){
						container.addClass('sts-form--invalid');
					} else {
						container.addClass('sts-form--valid');
					}
				}
			});
		},200);
	},
	ngloading: function($element, $show){
		var $result = $(".sts-result");
		$show = ( typeof $show != "undefined" )?  $show : true;
		if( $show == true ){
			$result.hide();
			$($element).find(".sts-relative-loader").
            prepend('<div class="overlay"><div class="cnt-load">'+
			'<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"'+
            '     width="27px" height="25px" viewBox="14 383.5 27 25" enable-background="new 14 383.5 27 25" xml:space="preserve">'+
            '<path fill="#CA0100" d="M32.798,394.979c-0.035-0.89-0.297-1.799-0.751-2.603l-4.106-7.111c-0.314-0.541-0.524-1.118-0.646-1.712'+
            '    l-0.175,0.297c-1.014,1.764-1.014,3.931,0,5.695l3.284,5.695c1.014,1.764,1.014,3.93,0,5.696l-0.174,0.296'+
            '    c-0.123-0.593-0.332-1.171-0.646-1.711l-3.005-5.242l-1.922-3.32c-0.315-0.541-0.524-1.117-0.646-1.711l-0.175,0.297'+
            '    c-1.014,1.747-1.014,3.913-0.018,5.678l0,0l3.302,5.713c1.014,1.765,1.014,3.93,0,5.695l-0.175,0.297'+
            '    c-0.122-0.594-0.332-1.171-0.646-1.713l-4.106-7.145c-0.558-0.961-0.803-2.027-0.751-3.093C17.04,396.132,14,398.56,14,401.391'+
            '    c0,3.931,5.87,7.109,13.12,7.109s13.12-3.179,13.12-7.109C40.24,398.56,37.2,396.132,32.798,394.979z"/>'+
            '</svg><div class="uil-ring-css"><div></div></div></div></div>');
            
		} else {
			
			$result.fadeIn(), $(".overlay").remove();
			
			if($result.length != 0){
                base.scrollIframe({
                    scrollTop: $result.offset().top,
                    timeAnimate: 1000,
                    animate:true 
                });
			}
		}
	},
	jOserialize : function(data){
		return Object.keys(data).map(function(k) {
		    return encodeURIComponent(k) + '=' + encodeURIComponent(data[k])
		}).join('&');
	},
	wizardBar : function(paso){
		var t = $(".sts-wizard"),
        b = $(".sts-barra li"),
		s = paso,
		e = t.data("pasos");
        
        var i = 1;
        b.removeClass("active");
        b.each(function(k,v){
            if( i <= paso ) {
                $(v).addClass("active");
            }
            i++;
        });

		t.find(".sts-current, .sts-wizard__dots").html(s),
		t.find(".sts-pasos").html(e);
	},
    scrollIframe : function(frameOption){
        var originalFrame  = {
                scrollTop: 0,
                animate: false,
                timeAnimate: 800
            };
      
        var dataFrame = parentFrame  = {
            frameScroll : $.extend({},originalFrame,frameOption)
        }
        
        if( typeof dataFrame.frameScroll != "undefined"){  
            var frameScroll = dataFrame.frameScroll;
            if(frameScroll.animate == true){
                $('html,body').animate({scrollTop: frameScroll.scrollTop },
                    ( (typeof frameScroll.timeAnimate != "undefined") ? frameScroll.timeAnimate : 0)
                );
            } else {
               $('html,body').scrollTop(frameScroll.scrollTop); 
            }
        } else {
            $('html,body').scrollTop(frameScroll.scrollTop);
        }
        parent.postMessage(dataFrame, '*');  
    }
};

base.scrollIframe({scrollTop: 0 });
 
var app = angular.module('portal', ['platanus.rut', 'credit-cards']).config(function($interpolateProvider){
    $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
});
app.run(function ($rootScope, $window) {
    
    $rootScope.sendLocalStorage = function ( $data ) {
        var lstorage = $rootScope.getLocalStorage();
        var datasend = (  !lstorage )? {} : lstorage;
        // usuario
        if( typeof $data.user !== 'undefined' ) {
            datasend["user"] = angular.copy($data.user);
        }
      
        if( typeof $data.home !== 'undefined' ) {
            datasend["home"] = angular.copy($data.home);
        }
		if($data.step !== 'undefined'){
			datasend["step"] = angular.copy($data.step);
		}
        
        if($data.question !== 'undefined'){
            datasend["question"] = angular.copy($data.question);
        }
        if( typeof $data.$$childHead !== "undefined" ){
			if( typeof $data.$$childHead.beneficiaries !== 'undefined' ) {
				datasend["beneficiaries"] = angular.copy($data.$$childHead.beneficiaries);
			}
			if( typeof $data.$$childHead.cargas !== 'undefined' ) {
				datasend["cargas"] = angular.copy($data.$$childHead.cargas);
			}
		}

        datasend = JSON.stringify( datasend );
        localStorage.setItem( "datasend", datasend );
    }
    
    $rootScope.getLocalStorage = function () {
	
        if (localStorage.getItem("datasend") !== null) {
            return JSON.parse( localStorage.getItem("datasend") );
        }
        return false;
    }
});

angular.module('portal').controller("products", ['$scope', '$element', '$http', '$filter',
 function($scope,$element, $http, $filter) {
 	  $scope.plans = {};
 	  $scope.product = null;
      $scope.dias = [];
	  $scope.range = false;
	  $scope.replans = [];
	  $scope.initial = function($slug) {
	  	$scope.product = $slug;
	  	$scope.getProducto($slug).success(function(){
			$('.ng-fix-hide').removeClass('ng-fix-hide');
		});
		window.localStorage.removeItem("datasend");
		
	  }

	  $scope.getProducto = function($slug){
	  	var response = $http.get(app.url + '/seguros/planes/'+$slug);
		response.success(function(data){
			$scope.plans = data;
		});	
		return response;  	
	  }
	  
	  $scope.contratar = function($event, $id, $price){
	  	 var $form = $($element);
	  	 var $input = $form.find('[name="insured[plan]"]');
         var $inputPrice = $form.find('[name="insured[price]"]');
         var $button =  $($event.target);
         var $coverage = $button.closest('.ng-scope').find('.ng-coverage');
        
	  	 if($input.length != 0){
	  	 	$input.val($id);
            $inputPrice.val($price);
            if($coverage.length != 0){
                $coverage.prop('checked', true);
            }
	  	 	$form.submit();
	  	 } 
	  }
	  
	 $scope.getHospital = function($event, $id){
		if($scope.fields.asegurados !== 'undefined'){
			base.ngloading($element, true);
			$scope.getProducto($scope.product).success(function(plans){
                var asegurados = parseInt($scope.fields.asegurados);
				var cargas = ( asegurados <= 0 || isNaN(asegurados))?  0 : asegurados ;
	 			var asegurados = parseInt(cargas) + 1;
				angular.forEach(plans, function(value, key) {
					price = parseFloat(plans[key].price.replace(",","."));
					plans[key].price = price * asegurados;
					plans[key].price = plans[key].price.toFixed(2);
				});
				$scope.plans = plans.filter(function(e){return e});
				base.ngloading($element, false);
	 		});
		}
	}

	$scope.getViajeLargo = function($event, $id){
		if($scope.fields.dias !== 'undefined' && $scope.fields.viajeros !== 'undefined'){
			base.ngloading($element, true);
			$scope.getProducto($scope.product).success(function(plans){
				var dias = $scope.fields.dias ;
				var viajeros = parseInt($scope.fields.viajeros);
				
				angular.forEach(plans, function(value, key){
					if( ! $scope.getIssetdaysCoverages(plans[key].coverages, dias) || 
                        !$scope.getIssetPlaninCargas(plans[key], viajeros) ){
						delete plans[key];
					}
				});
				base.ngloading($element, false);
				$scope.plans = plans.filter(function(e){return e});
			});
		}
		
	}
	//evaluar regla
	$scope.getViaje =  function($event, $id){
		if( $scope.fields.viajeros !== 'undefined'){
			var viajeros = $scope.fields.viajeros;                                                                
			base.ngloading($element, true);
			$scope.getProducto($scope.product).success(function(plans){
             
                angular.forEach(plans, function(value, key){
                    console.log(plans);
					if(!$scope.getIssetPlaninCargas(plans[key], viajeros)){
						//delete plans[key];
					} 
				});	
                $scope.plans = plans.filter(function(e){return e});
                  /**/
				base.ngloading($element,false);
			});
			
		}
	
	}
	
	$scope.getOncologico = function($event, $id){
		base.ngloading($element);
		var age = $scope.getAgeToDateBirthday($scope.fields.birthday);
		$scope.getProducto($scope.product).success(function(plans){
			$scope.range = "error";
			angular.forEach(plans, function(value, key){
				var match = false;
				if(plans[key].target.length != 0){
					var targets = plans[key].target;
					angular.forEach(targets, function(v, k){
						if(targets[k].start <= age && targets[k].finish >= age){
							$scope.range = {start:targets[k].start,  end:targets[k].finish};
							plans[key].price = targets[k].price;
							match = true;
						}
					})
				}
				
		       	if ( match  == false ){ 
					delete $scope.plans[key];
				} 		
			});
			$scope.plans = plans.filter(function(e){return e});
			base.ngloading($element, false);
		});
		
	}
	
	$scope.getUrgencia = function($event, $id){
		if($scope.fields.asistencia != "undefined"){
			var asistencia = $scope.fields.asistencia;
			base.ngloading($element);
			$scope.getProducto($scope.product).success(function(plans){
				//$scope.range = "error";
				angular.forEach(plans, function(value, key){
					if(!$scope.getIssetPlaninCargas(plans[key], asistencia)){
						delete plans[key];
					} 
				});
                $scope.plans = plans.filter(function(e){return e});
				base.ngloading($element,false);
			});
		}
	}
	
	$scope.getCatastrofico = function($event, $id){
		var age = $scope.getAgeToDateBirthday($scope.fields.birthday);
		base.ngloading($element);
		$scope.range = false;
		$scope.getProducto($scope.product).success(function(plans){
			angular.forEach(plans, function(value, key){
				var match = false;
				if(plans[key].target.length != 0){
					var targets = plans[key].target;
					angular.forEach(targets, function(v, k){
						if(targets[k].start <= age && targets[k].finish >= age){
							$scope.range = {start:targets[k].start,  end:targets[k].finish};
							plans[key].price = targets[k].price;
							match = true;
						}
					})
				}
		       	(! match ) ? delete plans[key] : true;		
			});
            $scope.plans = plans.filter(function(e){return e});
			base.ngloading($element, false);
		});
	
	}
	
	$scope.getVacaciones = function($event , $id){
      
		if( typeof $scope.fields.viajeros !== 'undefined' && typeof $scope.fields.dias !== "undefined"){
			var viajeros = parseInt($scope.fields.viajeros);
            var dias = parseInt($scope.fields.dias);
            base.ngloading($element);
			$scope.getProducto($scope.product).success(function(plans){
                angular.forEach(plans, function(plan, key){
                    if(!$scope.getIssetPlaninCargas(plans[key], viajeros)){
						delete plans[key];
					}
                    angular.forEach(plan.coverages, function(coverage, keyc){
                        if(parseInt(coverage.days) !== parseInt(dias)){
                            delete plans[key];
                        }
                    });
                    
                });
                $scope.plans = plans.filter(function(e){return e});
				base.ngloading($element,false);
			});
		}
	}
	
	
	/* FUNCIONES DE NEGOCIO */
	$scope.getAgeToDateBirthday = function(birthday){
		var $day = birthday.day;
		var $month = birthday.month;
		var $year = birthday.year;
		if( typeof $day != "undefined" && typeof $month != "undefined" 
			&& typeof $year != "undefined"	){
			$day = $filter('rpad')($day);
			$month = $filter('rpad')($month - 1);
			$year = $filter('rpad')($year);
			var now = new Date();
			var birthday = new Date($year, $month, $day);
			var diff = now.getTime()- birthday.getTime();
			var age = Math.floor(diff/(1000 * 60 *60 * 24 * 365.25));
			return age;
		}
	}
	
	$scope.getPriceOnCoverage = function(coverage){
		
		var plans = $scope.plans;
		var viajeros = $scope.fields.viajeros;
		
		if( typeof coverage.plan.id != 'undefined'){
			
			if( typeof $scope.plans[coverage.plan.id]!= 'undefined'){
				
				$scope.plans[coverage.plan.id].price = coverage.price;
				if(typeof viajeros != "undefined"){
					var multiprice = coverage.price.toString().split(',');
					viajeros = viajeros.toString().split(':');
					angular.forEach(viajeros, function(index, key){
						if( typeof multiprice[index] != "undefined"){
							$scope.plans[coverage.plan.id].price  = parseFloat(multiprice[index]);
						} else if( typeof multiprice != "undefined"){
                            $scope.plans[coverage.plan.id].price = parseFloat(multiprice.slice(-1)[0]);
                        }
					});
				}
			}
		}
	}
	
	$scope.getPriceThisCoverage = function(coverages){
		
		if(typeof $scope.fields!= "undefined"){
            
			if(coverages.price != null && coverages.price != 0 && 
				typeof $scope.fields.viajeros != "undefined" ){
					
				var plans = $scope.plans;
				var viajeros = $scope.fields.viajeros;
               
				angular.forEach(plans, function(item, i){
					if(item.coverages != "undefined"){
						angular.forEach(item.coverages, function(coverage, c){
                           
							if(coverage.id == coverages.id && coverage.codplanco != null ){
								if(typeof viajeros != "undefined"  && coverage.codplanco != "" ){
                                    
									var multiprice = coverage.price.toString().split(',');
									viajeros = viajeros.toString().split(':');
                                 
									angular.forEach(viajeros, function( value, index){
										if( typeof multiprice[value] != "undefined"){
											$scope.plans[i].price  = parseFloat(multiprice[value]);
										} else if( typeof multiprice != "undefined"){
                                            $scope.plans[i].price = parseFloat(multiprice.slice(-1)[0]);
                                        }
									});
									
								}
							}
						});
					}
				});
				
			}
		}
	}
  
  	$scope.getIssetdaysCoverages  = function(coverages, dias){
  		var isset = false;
  		dias = dias.split(':');
  		if( typeof dias[0] != 'undefined' &&  typeof dias[1] != 'undefined' ){
	  		angular.forEach(coverages, function(value, key){
				if(coverages[key].days > dias[0] && coverages[key].days <= dias[1] ){
					isset = true;
				}
				
	  		})
  		}
  		return isset;
  	}
	
	$scope.getIssetPlaninCargas = function(plan,opcion){
		var isset = false;
		var opcion = opcion.toString().split(':');
       
		if( opcion.length == 1 ){
			
			if(typeof opcion[0] != "undefined"){
				if(plan.cargasMin <= parseInt(opcion[0]) && plan.cargasMax >= parseInt(opcion[0])){
					isset = true;
				}
			}
		} else if(opcion.length != 0){
			if(typeof opcion[0] != "undefined" && opcion[1] != "undefined"){
				opcion[0]  = parseInt(opcion[0]);
				opcion[1]  = parseInt(opcion[1]);
				if(opcion[0] >= plan.cargasMin && plan.cargasMax >= opcion[1]){
					isset = true;
				}
			}
		}
		return isset;
	}
	
	$scope.getIssetPlaninTargets  = function(targets, age){
  		var match = false;
		if(typeof targets != "undefined"){
			if(targets.length != 0){
				targets = plans[key].target;
				angular.forEach(targets, function(v, k){
					if(targets[k].start <= age && targets[k].finish >= age){
						plans[targets[k].plan.id].price = targets[k].price;
						match = true;
					}
				})
			}
		}
		return match;	
  	}
	
	/* FIN FUNCIONES DE NEGOCIOS */	
	
}]);

angular.module('portal').controller("beneficiary", ['$scope', '$element', '$http' , '$parse',
	function($scope,$element, $http, $parse){
		
		$scope.insurance = {id: null};
		$scope.beneficiaries = [];
		$scope.beneficiary = {};
		$scope.id = null;
		$scope.buttonEdit = false;
		$scope.error = {limit: false, conyuge: false, rut: false, capital : false, maxapparition: false};
		$scope.limit = {max: 0, min: 0};
        
		$scope.selectInit = function(variable, def){
            var model = $parse(variable);
            if(typeof $scope.$eval(variable)== "undefined"){
                if(typeof def == 'undefined'){
                    var def = 0;
                }
                model.assign($scope, def);
            }
        }
		$scope.initial = function(){
            var datalocal = $scope.getLocalStorage();
            if( datalocal ) {
                if( typeof datalocal.beneficiaries !== 'undefined' ) {
                    $scope.beneficiaries = datalocal.beneficiaries;
                }
            }
			if($($element).data('max') ){
				$scope.limit.max = parseInt($($element).data('max'));
			}
			if($($element).data('min') ){
				$scope.limit.min = parseInt($($element).data('min'));
			}
			$scope.$parent.persons = $scope.beneficiaries;
		}
		
		$scope.$watch('buttonEdit', function(){
			$scope.buttonText = $scope.buttonEdit ? 'GUARDAR' : 'Agregar beneficiarios';
		})
		
		
		$scope.actionBeneficiary = function(){
			($scope.buttonEdit) ? $scope.updateBeneficiary():$scope.addBeneficiary();
		}
		
		$scope.addBeneficiary = function(){
			if( ! base.errorElement($element) && !$scope.errorBeneficiary() ){
				if(typeof $scope.beneficiary != 'undefined' ){
					var beneficiary = $scope.beneficiary;
					$scope.beneficiaries.push(beneficiary);
					$("[rel=add-beneficiary]").fadeIn('normal',function(){
						setTimeout(function(){$("[rel=add-beneficiary]").fadeOut()},2000)
					});
					$scope.resetBeneficiary();
				}
			}else{
				base.notifyError( $element );
			}
		}
		$scope.setError = function(){
			$scope.error.limit = true;
			$scope.error.rut = $scope.issetRut($scope.beneficiary);
			$scope.error.conyuge =  $scope.issetConyuge($scope.beneficiary);
            $scope.error.maxapparition =  $scope.inMaxapparition($scope.beneficiary);
		}
		
		
		$scope.setErrorLimit = function(){
			$scope.error.limit = true;
			setTimeout(function(){
				$scope.error.limit = false;
			},2000);
		}
		
		$scope.editBeneficiary = function($id){
			$scope.buttonEdit = true;
			$scope.id = $id;
			if( typeof $scope.beneficiaries[$id] != 'undefined'){
				$scope.beneficiary = angular.copy($scope.beneficiaries[$id]);
				base.updateInputElement($element);
				
			}
		}
        
		$scope.deleteBeneficiary = function($index){
            
            if(typeof $index !== "undefined"){
                $scope.beneficiary.delete = $index;
            }
        }
		
		
		$scope.updateBeneficiary = function(){
			$id = $scope.id;
			if( typeof $scope.beneficiaries[$id] != 'undefined'){
				if( ! base.errorElement($element)  && !$scope.errorBeneficiary($id) ){
					angular.copy($scope.beneficiary,$scope.beneficiaries[$id]);
					$scope.resetBeneficiary();
					$scope.buttonEdit = false;
                    base.scrollIframe({
                            scrollTop: $(".beneficiario-"+$id).offset().top, 
                            timeAnimate : 800 
                    });
				}
			}	
		}
		
		$scope.resetBeneficiary = function(){
			$scope.beneficiary = {name: '',lastname1:'', lastname2:'', code:'', birthday:0,birthmonth:0,birthyear:0,relationship:0, capital:''};
			base.resetElement($element);
		}
		
		$scope.getLength = function(obj) {
			return Object.keys(obj).length;
		}
		$scope.setErrorCapital = function(){
			$scope.errorCapital = true;
            base.scrollIframe({scrollTop:0});
			// $("body, html").scrollTop(0);
			setTimeout(function(){
				$scope.errorCapital = false;
			},2000);
		}
		
		$scope.errorBeneficiary  = function($id){
			var error = false;
			$scope.error.rut = $scope.issetRut($scope.beneficiary, $id);
			//$scope.error.conyuge =  $scope.issetConyuge($scope.beneficiary, $id);
			$scope.error.capital = $scope.capitalError($scope,$id);
			$scope.error.limit =  !$scope.inLimit($scope);
            $scope.error.maxapparition = $scope.inMaxapparition($scope.beneficiary, $id);
            //|| $scope.error.conyuge 
			if($scope.error.rut || $scope.error.capital || 
                $scope.error.limit || $scope.error.maxapparition ){
				error = true;
				setTimeout(function(){
				angular.forEach($scope.error, function(value, key){
						$scope.error[key] = false;
					});
				},2000);
			}
			
			return error;
		}
		
		$scope.issetRut = function(beneficiary, id){
			var isset = false;
			if( typeof beneficiary.code != 'undefined' ){
				var beneficiaries = angular.copy($scope.beneficiaries);
				angular.forEach(beneficiaries, function(data, key){
					if(data.code == beneficiary.code && key !== id){
						isset = true;
					}
				});
				//validamos si el  beneficiario es igual al titular
				if( typeof $scope.$parent.user.code != "undefined"){
					if($scope.$parent.user.code == beneficiary.code){
						isset = true;
					}
				}
				
				
			}
			return isset;
		}
		
		$scope.issetConyuge = function(beneficiary, id){
			var isset = false;
			var conyuge = false;
			if(typeof beneficiary.relationship != "undefined"){
				var beneficiaries = angular.copy($scope.beneficiaries);
				angular.forEach(beneficiaries, function(data, key){
					if(data.relationship == 'Cónyuge' && id !== key ){ 
						conyuge = true;
					}
				});
				if(beneficiary.relationship == 'Cónyuge' && conyuge){
					isset = true;
				}
			}
			return isset;
		}
		
        $scope.inMaxapparition = function(beneficiary, id){
            var error = false;
            var maxapparition = false;
            console.log(beneficiary.maxapparition);
            if(typeof beneficiary.maxapparition != "undefined"){
                var beneficiaries = angular.copy($scope.beneficiaries);
                angular.forEach(beneficiaries, function(data, key){
                    if( data.maxapparition == '1'&& beneficiary.relationship == data.relationship && id !== key){
                        error = true;
                    }
                });
               
            }
            return error;
        };
        
		$scope.inLimit = function(){
			var limit  = true;
			if( typeof $scope.beneficiaries != "undefined"){
				if( $scope.getLength($scope.beneficiaries) > $scope.limit.max  ){
					limit = false;
				}
			}
			return limit;
		}
		$scope.capitalError = function($scope, id){
			var error = false;
			var capital = 100;
			var clientCapital = 0;
			if( typeof $scope.beneficiaries != "undefined" || typeof $scope.beneficiary != "undefined"){
				var beneficiaries = $scope.beneficiaries;
				var beneficiary =  $scope.beneficiary;
				angular.forEach(beneficiaries,  function(value, key) {
					if(id !== key || typeof id == "undefined" ){	
						clientCapital = parseInt(beneficiaries[key].capital) + clientCapital;
					}
				});
				
				if( typeof $scope.beneficiary != "undefined"){
					if(typeof $scope.beneficiary.capital != "undefined"){
						clientCapital = parseInt($scope.beneficiary.capital) + clientCapital;
					}
				} 
				error = (clientCapital > capital);
			}
			return error;
		}
	}
]);

angular.module('portal').controller("register", ['$scope', '$element', '$http' ,'$filter','$parse',
	function($scope,$element, $http, $filter, $parse){
	$scope.step = 1;
   
	$scope.plan = null;
	$scope.persons = {};
	$scope.rules = {age: false, error : false};
	$scope.error = { birthday: false };
	
	$scope.initial = function(plan){
		$scope.plan = plan;
        var datalocal = $scope.getLocalStorage();
        if( datalocal ) {
            if( typeof datalocal.user !== 'undefined' ) {
                $scope.user = datalocal.user;
            }
            if( typeof datalocal.home !== 'undefined' ) {
                $scope.home = datalocal.home;
            }
			if( typeof datalocal.step !== 'undefined' ) {
                $scope.step = datalocal.step;
            }
            if( typeof datalocal.question !== "undefined"){
                $scope.question = datalocal.question;
            }
            
        }
	}
	
	$scope.selectInit = function(variable, def){
		var model = $parse(variable);
		if(typeof $scope.$eval(variable)== "undefined"){
			if(typeof def == 'undefined'){
				var def = 0;
			}
			model.assign($scope, def);
		}
	}
	
	$scope.nextStep = function(){
		var elemento = $("[data-step]:visible");
		var current = $("[data-step]:visible").data('step');
		var last = $("[data-step]:last").data('step');
		var persons = $("[data-step]:visible [data-min]");
		var next = $("[data-step]:visible").next();
		var prev = $("[data-step]:visible").prev();
        
		if( persons.length !=0){
			var limit = { max :  persons.data('max'), min :  persons.data('min') };
			var count = $scope.persons.length;
			var $child = $scope.$$childHead;
			if(limit.min <= count &&  limit.max >= count){
				// si es beneficiario, evaluamos capital
				if( typeof $child.beneficiary !== "undefined" && !$scope.capitalEvaluate($child) ){
					$child.setErrorCapital();
				} else if(last != current){
					$scope.moveStep(current);
				} else {
                    //guardamos en localStorage
                    $scope.sendLocalStorage( $scope );
					$($element).submit();
				}
			} else {
                base.scrollIframe({ scrollTop: 0 });
				$scope.$$childHead.setErrorLimit();
			}
		} else if( ! base.errorStep(elemento)  ){
			if( $scope.isSingle(next)){
				if( next.data('step') == last){
                    //guardamos en localStorage
                    $scope.sendLocalStorage( $scope );
                    $($element).submit();
                    return true;
				}
				$scope.moveStep(current, 2);
			} else if(last != current) {
				$scope.moveStep(current);
			} else {
                //guardamos en localStorage
                $scope.sendLocalStorage( $scope );
				$($element).submit();
			}
		}
	}
	
    
    
	$scope.backStep = function(){
		var prev = $("[data-step]:visible").prev();
		var historia = history;
		if($scope.step > 1){
			if( $scope.isSingle(prev) ){
				$scope.moveStep($scope.step, -2);
				return true;
			}
			$scope.moveStep($scope.step, -1);
		} else{
			historia.back(1);
		}
	}
	
	$scope.capitalEvaluate = function($child){
		var capital = 100;
		var clientCapital = 0;
		if( typeof $child.beneficiaries != "undefined" || typeof $child.beneficiary != "undefined"){
			var beneficiaries = $child.beneficiaries;
			var beneficiary =  $child.beneficiary;
			angular.forEach(beneficiaries,  function(value, key) {
				clientCapital = parseInt(beneficiaries[key].capital) + clientCapital;
			});
			/*
			if( typeof $child.beneficiary != "undefined" ){
				if(typeof $child.beneficiary.capital != "undefined" ){
					if($child.beneficiary.capital != ""){
						clientCapital = parseInt($child.beneficiary.capital) + clientCapital;
					}
				}
			}*/
		}
		
		return (clientCapital == capital);
	}
	
	$scope.moveStep = function (current, move){
		$scope.step = current + ( (typeof move == 'undefined')? 1 : move);
		
		base.wizardBar($scope.step);
		$scope.sendLocalStorage($scope);
        base.scrollIframe({scrollTop: 0});
	}
	
	$scope.isSingle = function($elemento){
		var $MinMax = $elemento.find('[data-min]');
		
		if( parseInt($MinMax.data('min')) == 0 && parseInt($MinMax.data('max')) == 0){
			return true;
		}
		return false;
	};
	
	$scope.isAgePlan = function(){
		var age = $($element).data('age');
		$scope.error.birthday = false;
		
		if(typeof age != "undefined"){
			range = age.toString().split(':')
			age = $scope.getAgeToDateBirthday($scope.user.birthday);
			
			if( typeof range[0] != 'undefined' && typeof range[1] != 'undefined' ){
				if(range[0] > age || range[1] < age){
					$scope.rules.age = true;
					$scope.rules.error = true;
				} else {
					$scope.rules.age = false;
					$scope.rules.error = false;
				}
			}
		}else{
			var age = $scope.getAgeToDateBirthday($scope.user.birthday);
			if( age < 18 ) {
				$scope.error.birthday = true;
			}
		}
	}
	
	$scope.getAgeToDateBirthday = function(birthday){
		var $day = birthday.day;
		var $month = birthday.month;
		var $year = birthday.year;
		if( typeof $day != "undefined" && typeof $month != "undefined" 
			&& typeof $year != "undefined"	){
			$day = $filter('rpad')($day);
			$month = $filter('rpad')($month - 1);
			$year = $filter('rpad')($year);
			var now = new Date();
			var birthday = new Date($year, $month, $day);
			var diff = now.getTime()- birthday.getTime();
			var age = Math.floor(diff/(1000 * 60 *60 * 24 * 365.25));
			return age;
		}
	}

}]);

angular.module('portal').directive('shakeAnimate', function(){
	return {
		link: function(scope,elemento){
			$nuevo = $(elemento);
            base.scrollIframe({
                 scrollTop:  $nuevo.offset().top,
                 timeAnimate: 800,
                 animate:true 
            });
		}
	}
});
angular.module('portal').directive('alertScroll', function(){
	return {
		link: function(scope,elemento){
			$nuevo = $(elemento);
            base.scrollIframe({
                scrollTop : $nuevo.offset().top,
                timeAnimate: 800,
                animate:true 
            });
		}
	}
});
angular.module('portal').directive('buttonSimulador', function () {
    return {
		link: function (scope, element, attrs) {
			$(element).click(function(){
				var title = attrs.buttonSimulador;
				if( title != ""){
					$(element).text(title);
				}
			});
		}
	}
});

angular.module('portal').directive('onlyText', function() {
    function link(scope, elem, attrs, ngModel) {
        ngModel.$parsers.push(function(viewValue) {
          var reg = /^[^`~!@#$%\^&*()_+={}|[\]\\:';"<>?,./1-9!¡°¨]*$/;
          if (viewValue.match(reg)) {
            return viewValue;
          }
          var transformedValue = ngModel.$modelValue;
          ngModel.$setViewValue(transformedValue);
          ngModel.$render();
          return transformedValue;
        });
    }

    return {
        restrict: 'A',
        require: 'ngModel',
        link: link
    };
});

angular.module('portal').directive('onlyAlphanumeric', function() {
    function link(scope, elem, attrs, ngModel) {
        ngModel.$parsers.push(function(viewValue) {
          var reg = /^[^`~!@$%\^&*()_+={}|[\]\\:';"<>?,./!¡¨]*$/;
          if (viewValue.match(reg)) {
            return viewValue;
          }
          var transformedValue = ngModel.$modelValue;
          ngModel.$setViewValue(transformedValue);
          ngModel.$render();
          return transformedValue;
        });
    }

    return {
        restrict: 'A',
        require: 'ngModel',
        link: link
    };
});

/* INICIO DE CONTROLADOR DE CARGAS */
angular.module('portal').controller("carga", ['$scope', '$element', '$http' ,'$parse',
	function($scope,$element, $http, $parse){
		$scope.cargas = [];
		$scope.carga = {};
		$scope.id = null;
		$scope.buttonEdit = false;
		$scope.limit = {min: 0, max:0 };
		$scope.error = { rut: false, limit: false, conyuge: false, maxapparition: false };
		$scope.selectInit = function(variable, def){
            var model = $parse(variable);
            if(typeof $scope.$eval(variable)== "undefined"){
                if(typeof def == 'undefined'){
                    var def = 0;
                }
                model.assign($scope, def);
            }
        }
		$scope.initial = function(){
            var datalocal = $scope.getLocalStorage();
            if( datalocal ) {
                if( typeof datalocal.cargas !== 'undefined' ) {
                    $scope.cargas = datalocal.cargas;
                }
            }
			if($($element).data('max') ){
				$scope.limit.max = parseInt($($element).data('max'));
			}
			if($($element).data('min') ){
				$scope.limit.min = parseInt($($element).data('min'));
			}
			$scope.$parent.persons = $scope.cargas;
		}
		
		$scope.$watch('buttonEdit', function(){
			$scope.buttonText = $scope.buttonEdit ? 'GUARDAR' : 'Agregar carga';
		})
		
		$scope.actionCarga = function(){
			($scope.buttonEdit) ? $scope.updateCarga():$scope.addCarga();
		}
		
		$scope.addCarga = function(){
			
			 if( ! base.errorElement($element) && !$scope.errorCarga()){
				var count = $scope.getLength($scope.cargas) + 1;
				$scope.error = {rut: false, conyuge: false};
				if( count > $scope.limit.max ){
					$scope.setErrorLimit();
					$scope.resetCarga();
				} else if(typeof $scope.carga != 'undefined'){
					$scope.cargas.push($scope.carga);
					$("[rel=add-carga]").fadeIn('normal',function(){
						setTimeout(function(){$("[rel=add-carga]").fadeOut()},2000)
					});
					$scope.resetCarga();
				
				}
			}
		}
        
		$scope.resetCarga = function(){
			$scope.carga = {name: '',lastname1:'', lastname2:'', code:'', birthday:0,birthmonth:0,birthyear:0,relationship:0};
			base.resetElement($element);
		}
		
		$scope.editCarga = function($id){
			$scope.buttonEdit = true;
			$scope.id = $id;
			if( typeof $scope.cargas[$id] != 'undefined'){
				$scope.carga = angular.copy($scope.cargas[$id]);
				base.updateInputElement($element);
				
			}
		}
      
        $scope.deleteCarga = function($index){
           
            if(typeof $index !== "undefined"){
                $scope.carga.delete = $index;
            }
        }
		
		$scope.updateCarga = function(){
			$id = $scope.id;
			if( typeof $scope.cargas[$id] != 'undefined'){
				if( ! base.errorElement($element)  && !$scope.errorCarga($id) ){
					angular.copy($scope.carga,$scope.cargas[$id]);
					$scope.resetCarga();
					$scope.buttonEdit = false;
                    base.scrollIframe({
                        scrollTop :  $(".carga-"+$id).offset().top ,
                        timeAnimate: 800,
                        animate:true 
                    });
                    
                    // $scope.sendLocalStorage($scope);
				}
			}
		}
		
		$scope.getLength = function(obj) {
			return Object.keys(obj).length;
		}
		
		
		$scope.errorCarga = function($id){
			var error = false;
			$scope.error.rut = $scope.issetRut($scope.carga,$id);
			//$scope.error.conyuge =  $scope.issetConyuge($scope.carga, $id);
            $scope.error.maxapparition = $scope.inMaxapparition($scope.carga, $id);
            // || $scope.error.conyuge
			if($scope.error.rut || $scope.error.maxapparition){
				error = true;
			}
			setTimeout(function(){
				angular.forEach($scope.error, function(value, key){
					$scope.error[key] = false;
				});
			},2000);
			return error;
		}
		
		$scope.issetRut = function(carga, id){
			var isset = false;
			if( typeof carga.code != 'undefined' ){
				var cargas = angular.copy($scope.cargas);
				angular.forEach(cargas, function(data, key){
					if(data.code == carga.code && id !== key){
						isset = true;
					}
				});
				// validamos el usuario titular
				if( typeof $scope.$parent.user.code != "undefined"){
					if($scope.$parent.user.code == carga.code){
						isset = true;
					}
				}
			}
			return isset;
		};
		
		$scope.issetConyuge = function(carga, id){
			var isset = false;
			var conyuge = false;
			if(typeof carga.relationship != "undefined"){
				var cargas = angular.copy($scope.cargas);
				angular.forEach(cargas, function(data, key){
					if(data.relationship == 'Cónyuge' && id !== key ){ 
						conyuge = true;
					}
				});
				if(carga.relationship == 'Cónyuge' && conyuge){
					isset = true;
				}
			}
			return isset;
		};
		
        $scope.inMaxapparition = function(carga, id){
            var error = false;
            var maxapparition = false;
            if(typeof carga.maxapparition != "undefined"){
                var cargas = angular.copy($scope.cargas);
                angular.forEach(cargas, function(data, key){
                    if( data.maxapparition == '1'&& carga.relationship == data.relationship && id !== key){
                        error = true;
                    }
                });
               
            }
            return error;
        };
        
		$scope.setErrorLimit = function(){
			$scope.error.limit = true;
			setTimeout(function(){
				$scope.error.limit = false;
			},2000);
		}
		
		
	}
]);

angular.module('portal').directive('cargas', function(){
	return {
		link: function(scope,elemento){
			$nuevo = $(elemento);
			$nuevo.addClass('animated shake');
            base.scrollIframe({
                scrollTop : $nuevo.offset().top ,
                timeAnimate: 800,
                animate:true 
            });
		}
	}
});
angular.module('portal').directive('relationship',function(){
    return function(scope,element,attrs){
        var relationships = [];
        angular.forEach( element.find('option'),function( item,idx){ 
            relationships[item.value] = item.getAttribute('maxapparition');
        });
        scope.$watchGroup(['beneficiary.relationship', 'carga.relationship'],function(){
            if( typeof scope.carga !== "undefined"){
                scope.carga.maxapparition =  relationships[scope.carga.relationship];
            } else if(typeof scope.beneficiary !== "undefined"){
                scope.beneficiary.maxapparition = relationships[scope.beneficiary.relationship];
            }
        });
       
         
    }
});
/* FIN CONTROLADOR CARGAS */

/* INICIO CONTROLADOR HOGAR */
angular.module('portal').controller("hogar", ['$scope', '$element', '$http' ,
	function($scope,$element, $http){
		$scope.initial = function(){
		}
		
		$scope.saveHome = function($event){
			if(base.errorElement($element)){
				$event.preventDefault();
			}
		}
	}
]);
//register
angular.module('portal').filter('toCommaFloat', [
function() { // should be altered to suit your needs
    return function(input) {
    	input = parseFloat(input).toFixed(2);
    	var ret=(input)?input.toString().trim().replace(".",","):null;
        return ret;
    };
}]);

angular.module('portal').filter('toUf', [function() {
    return function(input) {
    	var decimals = 0;
		if( typeof input != "undefined"){
			if( input != null && input != ""){
				amount = input.toString().trim(); // por si pasan un numero en vez de un string
				amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto
				amount =  parseInt(Math.round(app.uf * amount));
				decimals = decimals || 0; // por si la variable no fue fue pasada

				// si no es un numero o es igual a cero retorno el mismo cero
				if (isNaN(amount) || amount === 0) 
					return parseFloat(0).toFixed(decimals);

				amount = '' + amount.toFixed(decimals);
				while( (parseInt(amount) % 10) != 0){
					amount++;
				}
				
				var amount = ''+ amount;
				var amount_parts = amount.split('.'),
					regexp = /(\d+)(\d{3})/;

				while (regexp.test(amount_parts[0]))
					amount_parts[0] = amount_parts[0].replace(regexp, '$1' + '.' + '$2');

				amount = amount_parts.join('.');
				return amount;
			}
		}
	    return input;
    	
    };
}]);

angular.module('portal').filter('orderObjectBy', function(){
 return function(input, attribute) {
    if (!angular.isObject(input)) return input;

    var array = [];
    for(var objectKey in input) {
        array.push(input[objectKey]);
    }
    
    array.sort(function(a, b){
        a = parseInt(a[attribute]);
        b = parseInt(b[attribute]);
        return a - b;
    });
    return array;
 }
});
angular.module('portal').filter('orderObjectByAlpha', function(){
 return function(input, attribute) {
    if (!angular.isObject(input)) return input;

    var array = [];
    for(var objectKey in input) {
        array.push(input[objectKey]);
    }

    array.sort(function(a, b){
		var nameA = a[attribute].toLowerCase(), nameB = b[attribute].toLowerCase();
		if (nameA < nameB)
			return -1;
		if (nameA > nameB)
			return 1;
		return 0;
    });
    return array;
 }
});
// angular.module('portal').filter('rpad', function(){
	// return function(input, n){
		// if(input=== undefined){
			// input = "";
		// }
		// if(input.length >= n){
			// return input;
		// }
		// var zeros = "0".repeat(n);
		// return (zeros + input).slice(-1*n);
	// }	
// });

angular.module('portal').filter('rpad', function(){
    return function (input, n) {
        if(input=== undefined){
			input = "";
		}
        var my_string = '' + input;
        while (my_string.length < n) {
            my_string =  '0' + my_string;
        }

        return my_string;

    }
});

angular.module('portal').filter('Oblength', function() {
    return function(items) {
        return Object.keys(items).length;
    };
});
angular.module('portal').filter('empty', function () {
	var bar; 
	return function (obj) {
		for (bar in obj) {
			if (obj.hasOwnProperty(bar)) {
				return false;
			}
		}
		return true;
	};
});